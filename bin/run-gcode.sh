#!/usr/bin/bash
# full list ll-lps_anchor/ ll-lps_automation/ ll-lps_factory_test_service/ ll-lps_host_programmer/ ll-lps_mcu/ ll-lps_monitor/ ll-lps_prod_test/ ll-lps_ranging/ ll-lps_regression/ ll-lps_toolkit/
XDIRS="\
 ll-lps_mcu/ \
 ll-lps_monitor/ \
 ll-lps_prod_test/
 ll-lps_ranging/ \
 ll-lps_regression/ \
 ll-lps_automation/ \
 ll-lps_cli/ \
 ll-lps_factory_test_service/ \
 ll-lps_host_programmer/ \
 \
"

test_only=false
if [ ! -z "$1" ]
then
    if [ "$1" == "-t" ]
    then
        test_only=true
        echo test only
    fi
fi
TDOCS=/home/eo/src/zainar/dfhub/zh_02_py_docs/docs 
for D in $XDIRS
do
    echo work $D
    for F in `find ${D}  -name *.py`
    do
        date
        echo working on $F
        TF=${TDOCS}/${F}.md
        echo $TF
        echo target is $F
        if [ "${TF}" -nt "${F}" ]
        then
            echo ${TF} newer than ${F} -- SKIPPING
        else
            echo process ${F}
            if $test_only
            then
                #ls -l ${TF} ${F}
                continue
            else
                gcode_analyze.py -t ${TDOCS} -x $F || (echo sleep 300; sleep 300)
            fi
            echo sleep 60
            sleep 60
        fi
    done
done
