#!/usr/bin/bash
# full list ll-lps_anchor/ ll-lps_automation/ ll-lps_factory_test_service/ ll-lps_host_programmer/ ll-lps_mcu/ ll-lps_monitor/ ll-lps_prod_test/ ll-lps_ranging/ ll-lps_regression/ ll-lps_toolkit/
XDIRS="\
 ll-lps_mcu/ \
 ll-lps_monitor/ \
 ll-lps_prod_test/
 ll-lps_ranging/ \
 ll-lps_regression/ \
 ll-lps_automation/ \
 ll-lps_anchor/ \
 ll-lps_cli/ \
 ll-lps_factory_test_service/ \
 ll-lps_host_programmer/ \
 \
"

echo $dd
exit 1

SHOWALL=false
FG_FIND=
FG_DIR=
use_filter=false

for arg in $*
do
  case $arg in
      -a) SHOWALL=true
          shift
          ;;
      -f) FG_DIR=$2
          FG_FIND=${@:3}
          use_filter=true
          shift
          shift
          shift
          ;;
      -?) echo
          echo 'Usage: find-code.sh [-a|-f]'
          echo '    interate accross all dirs, unless -f passed'
          echo '    -a: show all statuses  '
          echo '    -f: <dir> <find-wild> for the find argument rather than sequencing  '
          exit 1
          ;;
  esac
done

SHOWALL=false
if [ "$1" == "-a" ]
then
    SHOWALL=true
fi
#Echo SHOWALL:  $SHOWALL 
TDOCS=${PWD}/zh_02_py_docs/docs

# usage fgcode_check_file target_dir source_file
function fgcode_check_file()
{
    echo 1:$1
    echo 2:$2
    TARGET_MD=${1}/${2}.md
    echo TARGET_MD:${TARGET_MD}
    if [ ! -e ${TARGET_MD} ]
    then
        echo NEED: ${2}
        #Echo NEED: ${TARGET_MD}
        return 0
    fi
    echo not exist
    if ! $SHOWALL
    then
        #Echo $SHOWALL
        #Echo CONT
        return 0
    fi
    if [ "${TARGET_MD}" -nt "${2}" ]
    then
        echo NEWER:${TARGET_MD}
    else
        echo OLDER:${TARGET_MD}
    fi
}

# fgcode_check_dirs find-dir target-dir find-expresion 
function fgcode_check_dirs()
{
    #Echo ${1} ${2} ${@:3}
    for F in `find ${1}  -name ${@:3}`
    do
        fgcode_check_file ${2} ${F}
    done
    return 0
}

if ! $use_filter
then
    for D in $XDIRS
    do
        fgcode_check_dirs  ${D} ${TDOCS} *.py
    done
else
    #Echo FG_DIR:${FG_DIR} 
    #Echo FG_FIND:${FG_FIND}
    #Echo find ${FG_DIR}  ${FG_FIND}
    if [ -z "${FG_FIND}" ]
    then
        FG_FIND="-name *.py"
    fi
    for F in `find ${FG_DIR}  ${FG_FIND}`
    do
        #echo FG:${F}
        #echo FG_DIR:${FG_DIR}
        #echo TDOCS:${TDOCS}
        #fgcode_check_file ${FG_DIR} ${F}
        TF=${TDOCS}/${F}.md
        #echo TF:$TF
        if [ ! -e ${TF} ]
        then
            echo fNEED: ${F}
            continue
        fi
        if [ "${TF}" -nt "${F}" ]
        then
            continue
            echo fNEWER:${TF}
        else
            echo fOLDER:${TTF}
        fi
    done
fi
exit 0

