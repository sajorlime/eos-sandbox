
#!/usr/bin/bash
if [ -f "$1" ]
then
    SDCARD=$1
else
    SDCARD=`ls -t *.sdcard | head -1`
fi
echo $SDCARD
#sudo parted -s dey-image-qt-xwayland-ccimx8mn-dvk-20240701211004.rootfs.sdcard unit B print
sudo parted -s $SDCARD unit B print
EXT4=`sudo parted -s $SDCARD unit B print | /usr/bin/grep ext4`
echo $EXT4
EXT40=$(echo $EXT4 | awk  '{print $2}' | sed 's/B$//')

echo $EXT40
#sudo parted -s $SDCARD unit B print | /usr/bin/grep ext4 | awk '{print $2}' | sed 's/B$//'
sudo mount -o offset=$EXT40 $SDCARD sdmount
