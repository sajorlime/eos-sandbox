export OMP_NUM_THREADS=1
export OPENBLAS_NUM_THREADS=1
export EMP=1
OUTFILE=output.txt
PROFILE=''
typeset -i run
typeset -i repeat
run=0
repeat=1
DATA_DATE=20210818
for arg in $*
do
    case $arg in
    -i) INDIR=$2
        shift
        shift
        ;;
    -d) DATA_DATE=$2
        DD=`ls ${DATA_DATE}`
        shift
        shift
        ;;
    -n) 
        unset OMP_NUM_THREADS
        unset OPENBLAS_NUM_THREADS
        EMP=0
        shift
        ;;
    -o) OUTFILE=$2
        shift
        shift
        ;;
    -p) PROFILE="-m cProfile -s cumtime "
        shift
        ;;
    -r) repeat=$2
        echo set repeat, $repeat
        shift
        shift
        ;;
    -t) 
        OMP_NUM_THREADS=1
        OPENBLAS_NUM_THREADS=1
        shift
        ;;
    -?) 
        echo cmd.sh [-o logfile] [-n] [-?]
        echo -n don''t disable numpy multi-threading
        echo -? print this message
        shift
        exit 1
        ;;
    esac
done

echo outfile: ${OUTFILE}
echo OMP_NUM_THREADS: $OMP_NUM_THREADS
echo OPENBLAS_NUM_THREADS: $OPENBLAS_NUM_THREADS
echo EMP: $EMP

DATA_ROOT=/home/emilio/src/data
pushd ${DATA_ROOT} > /dev/null
DD=`ls ${DATA_DATE}`
popd > /dev/null
DATA_PATH=/home/emilio/src/data/${DATA_DATE}/${DD}
#echo $DATA_PATH

while [ $run -lt $repeat ]
do
    python3 ${PROFILE} \
        distanceComputationModule.py \
        --node_path=${DATA_PATH}/node_characteristics.csv \
        --dist_offset_path=${DATA_PATH}/distanceOffset.csv \
        --outfile_pattern_path=${DATA_PATH}/outfile_\(.*\).txt \
        --tagTDOAOnly=-1 --verboseMode=0 --distComputeMethod=1 --do_kpi=3 \
        --outfile_pattern_max_num=500 --plot_metrics=0 --output_dist_format=1 \
        --nodePermuationDist=0 --out_tdoa_type=2 --use_TDOA_geom=1 --mvdn=1 \
        --enable_multi_process=${EMP} \
        --default_yaml_file_path=/home/emilio/tmp/dc_config_templates/DC_defaults.yml > \
        ${OUTFILE}
    run=run+1
done

