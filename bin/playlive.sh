#!/usr/bin/env bash
# script to run distanceComputerModule.py with parameters

# set the base name for the output file
OUT_ROOT=/tmp/outdata
OUTBASE=dcmlive
PROFILE=''
DATA_DATE=20210818
PROFILE_ARG=''
SEND_CMD=echo
ALL_FILES=false

for arg in $*
do
    case $arg in
    -a) ALL_FILES=true
        shift
        ;;
    -c) SEND_CMD=cat
        shift
        ;;
    -d) DATA_DATE=$2
        shift
        shift
        ;;
    -o) OUTBASE=$2
        shift
        shift
        ;;
    -t) 
        OMP_NUM_THREADS=1
        OPENBLAS_NUM_THREADS=1
        shift
        ;;
    -?) 
        echo playlive.sh [-c] [-d datedir] [-n] [-o basename]  [-p] [-r R] [-t] [-?]
        echo "    -c use cat as send command, defaults to echo"
        echo "    -o set basename of log file, /tmp/outdata/BASENAME"
        echo "       file gets /tmp/outdata/BASENAME-R.txt"
        echo "       BASENAME defaults to: \"${OUTBASE}\""
        echo "    -? print this message"
        shift
        exit 1
        ;;
    esac
done

OUT_PATH=${OUT_ROOT}/${DATA_DATE}
echo ${OUT_PATH}
mkdir -p ${OUT_PATH}
OUTFILE_ROOT=${OUT_PATH}/${OUTBASE}

echo outbase: ${OUTFILE_ROOT}
echo OMP_NUM_THREADS: $OMP_NUM_THREADS
echo OPENBLAS_NUM_THREADS: $OPENBLAS_NUM_THREADS
echo EMP: $EMP

DATA_ROOT=/home/emilio/src/data
pushd ${DATA_ROOT} > /dev/null
DD=`ls ${DATA_DATE}`
popd > /dev/null
DATA_PATH=/home/emilio/src/data/${DATA_DATE}/${DD}

LOGFILE=${OUTFILE_ROOT}.txt
echo logfile: ${LOGFILE}
OFLIST=`ls -1tr ${DATA_PATH}/outfile_*.txt`
NODEFILE=${DATA_PATH}/node_characteristics.csv
for OFILE in $OFLIST
do
    echo type \<enter\> to send  ${OFILE}, 'q' to quit
    #echo AF: ${ALL_FILES}
    if ! ${ALL_FILES}
    then
        read line
        #echo line:${line}
        if [[ "${line}" == *"q"* ]]
        then
            break
        fi
    fi
    echo send w/${SEND_CMD} $OFILE to /tmp/readout_server.znr
    cat ${NODEFILE} > /tmp/read_node_states.znr
    ${SEND_CMD} ${OFILE} > /tmp/readout_server.znr
done

