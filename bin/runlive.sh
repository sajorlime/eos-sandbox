#!/usr/bin/env bash
# script to run distanceComputerModule.py with parameters

# set the base name for the output file
OUT_ROOT=/tmp/outdata
OUTBASE=dcmlive
PROFILE=''
DATA_DATE=20210818
PROFILE_ARG=''

DATA_ROOT=/home/emilio/src/data
pushd ${DATA_ROOT} > /dev/null
DD=`ls ${DATA_DATE}`
popd > /dev/null
DATA_PATH=/home/emilio/src/data/${DATA_DATE}/${DD}

for arg in $*
do
    case $arg in
    -d) DATA_DATE=$2
        shift
        shift
        ;;
    -o) OUTBASE=$2
        shift
        shift
        ;;
    -?) 
        echo runlive.sh [-c] [-d datedir] [-n] [-o basename]  [-p] [-r R] [-t] [-?]
        echo "    -c use cat as send command, defaults to echo"
        echo "    -o set basename of log file, /tmp/outdata/BASENAME"
        echo "       file gets /tmp/outdata/BASENAME-R.txt"
        echo "       BASENAME defaults to: \"${OUTBASE}\""
        echo "    -? print this message"
        shift
        exit 1
        ;;
    esac
done

OUT_PATH=${OUT_ROOT}/${DATA_DATE}
echo ${OUT_PATH}
mkdir -p ${OUT_PATH}
OUTFILE_ROOT=${OUT_PATH}/${OUTBASE}


LOGFILE=${OUTFILE_ROOT}.txt
echo logfile: ${LOGFILE}
python3 ${PROFILE} \
    distanceComputationModule.py \
    --dist_offset_path=${DATA_PATH}/distanceOffset.csv \
    --node_path=${DATA_PATH}/node_characteristics.csv \
    --outfile_fifo=dummy \
    --tagTDOAOnly=-1 \
    --verboseMode=0 \
    --distComputeMethod=0\
    --do_kpi=0 \
    --outfile_pattern_max_num=500 \
    --plot_metrics=0 \
    --output_dist_format=1 \
    --nodePermuationDist=0 \
    --out_tdoa_type=2  \
    --use_TDOA_geom=0 \
    --mvdn=1 \
    --enable_multi_process=0 \
    ${PROFILE_ARG} \
    --default_yaml_file_path=/home/emilio/tmp/dc_config_templates/DC_defaults-live.yml \
    &

sleep 3
cat ${DATA_PATH}/node_characteristics.csv > /tmp/read_node_states.znr


