#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>


typedef uint16_t mb_addr;
typedef uint16_t mb_val;

struct mb_loc {
    mb_addr _addr;
    mb_val _value;
};

typedef struct mb_loc mb_loc; 

uint8_t _rh_contiguous = 17;
uint8_t _rh_clast = 29;
uint8_t _rh_limit = 101;
mb_loc _rh_map[] = {
    // some contiguous registers
    {2, 2},
    {3, 3},
    {5, 5},
    {7, 7},
    {11, 11},
    {13, 13},
    {17, 17},
    {18, 18},
    {19, 19},
    {20, 20},
    {21, 21},
    {22, 22},
    {23, 23},
    {24, 24},
    {25, 25},
    {26, 26},
    {27, 27},
    {28, 28},
    {29, 29},
    {31, 31},
    {37, 37},
    {43, 43},
    {47, 47},
    {53, 53},
    {57, 57},
    {59, 59},
    {61, 61},
    {71, 71},
    {73, 73},
    {79, 79},
    {83, 83},
    {89, 89},
    {97, 97},
    {101, 101},
};

uint16_t return_loc(mb_addr addr, int top, int bottom)
{
    //printf("[%d,%d]: ", top, bottom);
    if (addr < _rh_map[top]._addr)
        return 0xFF; // illegal address
    if (_rh_map[top]._addr == addr)
        return _rh_map[top]._value;
    top++;
    if (_rh_map[bottom]._addr == addr)
        return _rh_map[bottom]._value;
    if (addr > _rh_map[bottom]._addr)
        return 0xFF; // illegal address
    bottom--;
    int mid = ((bottom - top) / 2) + 1;
    if (mid == 0)
        return 0xFF; // illegal address
    if (_rh_map[mid]._addr == addr)
        return _rh_map[mid]._value;
    if (addr < _rh_map[mid]._addr)
        return return_loc(addr, top, mid);
    return return_loc(addr, mid, bottom);

}

// return value for address
uint16_t loc_search(mb_addr addr)
{
# if 0
    if ((addr < 2) || (addr > 101)) {
        printf("address out of range\n");
        return -1;
    }
# endif
    int end = sizeof(_rh_map) / sizeof(mb_loc) - 1;
    return return_loc(addr, 0, end);
}


int main(int argc, char *argv[])
{
    int opt;
    int loc = 0;
    int limit = 102;

    // put ':' in the starting of the
    while((opt = getopt(argc, argv, ":se")) != -1)
    {
        printf("OPT:%c ", opt);
        switch(opt)
        {
        case 's':
            loc = atoi(optarg);
            break;
        case 'e':
            limit = atoi(optarg);
            break;
        }
    }

    for (int i = loc; i < limit; i++) {
        //printf("%d:", i);
        uint16_t v = loc_search(i);
        if (v == (uint16_t) i)
            printf("%04X == %d\n", v, i);
        //if (v != (uint16_t) i)
        //    printf("%04X != %d\n", v, i);
        //printf(" > 0x%04X\n", v);
    }

    return 0;
}

