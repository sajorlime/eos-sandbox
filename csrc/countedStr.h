/* 
// 
// $Id: //Source/Common/tcp/tracer.h#5 $ 
// 
// Copyright (c) 1999-2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: countedStr.h
// 
// Creation Date: 02/14/00
// 
// Last Modified: 
// 
// Author: Emil Rojas
// 
// Description: 

   countedStr provides an in-place counted string that is not null terminated
   and provides the operators necessary for map and other STL classes.

//  
*/ 

# ifndef __COUNTEDSTR_H__
# define __COUNTEDSTR_H__

# include "dctype.h"
# include "dotcastns.h"
# include <string>
# include <string.h>
# include <dcalgorithm.h>

// should I have the ability to print this?
//# include <ostream>

__BEGIN_DC_NAME_SPACE

class CCountedStr 
{
    const char * m_pStr;
    size_t m_size;
public:
    CCountedStr (const char * pStr = 0, size_t s = 0)
        : m_pStr (pStr),
          m_size (s)
    {
    }

    CCountedStr (const CCountedStr & rCS)
        : m_pStr (rCS.m_pStr),
          m_size (rCS.m_size)
    {
    }

    void set (const char * pStr, size_t s)
    {
        m_pStr = pStr;
        m_size = s;
    }

    void set (const CCountedStr & rCS)
    {
        m_pStr = rCS.m_pStr;
        m_size = rCS.m_size;
    }

    /*
        size returns the size of the string.
    */
    size_t size (void) const {return m_size;}
    
    /*
        copy will copy the string to the passed in parameter.
        This is the only way to read the string value directly.
    */
    void copy (std::string & rDest) const
    {
        rDest.assign (m_pStr, m_size);
    }

    inline bool operator== (const CCountedStr & rT) const
    {
        size_t s = std::min (m_size, rT.m_size);
        int b = strncmp (m_pStr, rT.m_pStr, s);
        if (b == 0)
            return true;
        return false;
    }

    inline bool operator< (const CCountedStr & rT) const
    {
        size_t s = std::min (m_size, rT.m_size);
        int b = strncmp (m_pStr, rT.m_pStr, s);
        if (b < 0)
            return true;
        return false;
    }

    inline bool operator> (const CCountedStr & rT) const
    {
        return rT < *this;
    }

    inline bool operator<= (const CCountedStr & rT) const
    {
        size_t s = std::min (m_size, rT.m_size);
        int b = strncmp (m_pStr, rT.m_pStr, s);
        if (b <= 0)
            return true;
        return false;
    }

    inline bool operator>= (const CCountedStr & rT) const
    {
        return rT <= *this;
    }

};


__END_DC_NAME_SPACE

# endif
