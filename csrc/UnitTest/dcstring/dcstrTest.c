/*
// 
// $Id:$ 
// 
// Copyright (c) 2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: stri.cpp
// 
// Creation Date: 
// 
// Last Modified: 
// 
// Author: Emil Rojas
// 
// Description: 
// 
//  
*/ 

# include <stdio.h>

# include <string.h>

# include "dcstring.h"



int main (int argc, char ** argv)
{
    char * pR;
    int d;

    if (argc < 3)
    {
        printf ("usage: stri needle haystack\n");
        return -1;
    }
    d = strncasecmp (argv[1], argv[2], 10000);
    printf ("[%s (%d) %s] :", 
                 argv[1],
                 d,
                 argv[2]
               );

    pR = stristr (argv[1], argv[2]);
    if (pR)
    {
        printf ("%s found at [%d], in %s, %d\n",
                 pR,
                 pR - argv[2],
                 argv[2],
                 d
                );
        return -1;
    }
    else
    {
        printf ("%s not found  in %s, %d\n",
                 argv[1],
                 argv[2],
                 d
               );
    }

    return 0;
}

