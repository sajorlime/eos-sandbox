// CommandMethod.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include "CommandMethod.h"

using namespace std;
using namespace dotcast;

class Thinger : public PCommandMethodHandler
{
public:
	Thinger () {}
	~Thinger() {}
	enum Type {
		Quit = 0,
		Test
	};
	virtual CommandArgument handleCommand (const char* commandName, int id, void* sender, CommandArgument* args, size_t argc);
};

static PCommandManager* TheCommandManager;

static void SetupCommandHandlerBindings ()
{
	// This is done at Init, someplace else where reading from XML
	// the actual bindings for UI Elements to what "methods" they call
	// and the arguments they pass in are setup.
	Thinger* pThinger = new Thinger();
	TheCommandManager = PCommandManager::CreateCommandManager();
	TheCommandManager->registerCommandForObject("Quit", pThinger);
	TheCommandManager->registerCommandForObject("Test", pThinger);
}

static CommandArgument* TestArgs;
static size_t TestArgsCount;

static void SetupUIBindings () {
	// This would read all the UI stuff from XML
	// which would include "XML/RPC-like" bindings
	// for commands and args per UI element...
	//
	// Assume this is read in as the args for the Test
	// UI Element, so we convert from the XML to the
	// CommandArgument Array.
	TestArgsCount = 4;
	TestArgs = new CommandArgument[TestArgsCount];
	TestArgs[0].type = CommandArgument::Int;
	TestArgs[0].intValue = 1234;
	TestArgs[1].type = CommandArgument::Double;
	TestArgs[1].doubleValue = 3.14159;
	TestArgs[2].type = CommandArgument::Bool;
	TestArgs[3].boolValue = true;
	TestArgs[3].type = CommandArgument::String;
	TestArgs[3].stringValue = "Hey there";
}

/*
 * The glorious Main Method
 */
int main(int argc, char* argv[])
{
	CommandArgument retval;

	SetupCommandHandlerBindings();
	SetupUIBindings();
	cout << "Hello World!" << endl;
	// Pretend the Test Button was pressed...
	// TestArgs would also be in a UI Element rather than static
	TheCommandManager->processCommand("Test", Thinger::Test, 0 /* Would be Test UI Element */, TestArgs, TestArgsCount, retval);
	// Pretend the Quit Button was pressed...
	TheCommandManager->processCommand("Quit", Thinger::Quit, 0 /* Would be Quit UI Element */, 0, 0, retval);
	return 0;
}

CommandArgument Thinger::handleCommand (const char* commandName, int id, void* sender, CommandArgument* args, size_t argc)
{
	CommandArgument argument;
	argument.type = CommandArgument::Bool;
	argument.boolValue = true;
	if (id == Thinger::Quit) {
		cout << endl << "Press enter to destroy the universe." << endl;
		cout << "(Some versions may behave slightly differently): ";
		(void)cin.get();
		exit(0);
	}
	else if (id == Thinger::Test) {
		for (int i = 0; i < argc; i++) {
			cout << "Arg " << i << " is " << ConvertArgumentTypeToString(args[i].type) << " value is ";
			if (args[i].type == CommandArgument::String) {
				cout << "\"" << args[i].stringValue << "\"";
			}
			else {
				cout << ARGUMENT_NUMERIC_VALUE(args[i]);
			}
			cout << endl;
		}
	}
	return argument;
}
