
#ifdef _WIN32
#pragma warning(disable: 4786)
#endif //_WIN32

////#define _NOTRACING

#include "Tracer.h"

#include <iostream>
#include <strstream>
using namespace std;
using namespace dotcast;


//
// App to demonstrate CTracer object functionality.

static int trLevel = 0;
static int outputDev = 0;

char fileName[] = "output.txt";
void setTraceOutput();
void setOutputDevice();
void doTrace1();

int main(int argc, char *argv[])
{
	cout << "TraceLevel: (0-4)";
	cin >> trLevel;
	cout << "Output Device (0=default, 1=none, 2=file): ";
	cin >> outputDev;
	cin.ignore(1000, '\n'); // Eat any stray characters, and newline

	setTraceOutput();
	setOutputDevice();

	doTrace1();
	cout << endl << "Press enter to destroy the universe." << endl;
	cout << "(Some versions may behave slightly differently): ";
	(void)cin.get();
	return 0;
}


void setTraceOutput()
{
	CTracer::setTraceLevel("Proxy", trLevel);
}

void setOutputDevice()
{
	switch(outputDev) {
	case 0:
		CTracer::setOutputStdout();
		break;
	case 1:
		CTracer::setOutputNone();
		cout << "Output off\n";
		break;
	case 2:
		CTracer::setOutputFile(fileName);
		cout << "Output appended to: " << fileName << endl;
		break;
	}
}


void doTrace1()
{
	//
	// printf:
	int xxx=1;
	CTracer::dprintf("Hello World: %d\n", xxx); 
	CTracer::pprintf("IDotbox", 4, "Hello World: %d\n", xxx); 
	CTracer::pprintf("Proxy", 0, "Proxy - tracelevel0: %d\n", xxx++); 
	CTracer::pprintf("Proxy", 1, "Proxy - tracelevel1: %d\n", xxx++); 
	CTracer::pprintf("Proxy", 2, "Proxy - tracelevel2: %d\n", xxx++); 
	CTracer::pprintf("Proxy", 3, "Proxy - tracelevel3: %d\n", xxx++); 
	CTracer::pprintf("Proxy", 4, "Proxy - tracelevel4: %d\n", xxx++);
	CTracer::pprintf("Proxy", 4, "Proxy - a verbose message: %d\n", xxx++);
	CTracer::pprintf("Proxy", 4, "Proxy - another verbose message: %d\n", xxx++);
	CTracer::pprintf("Proxy", 4, "Proxy - yet another verbose message: %d\n", xxx++);

	//
	// ostream
	CTracer::stream("Proxy", 0) << "Proxy - ostream tracelevel0 " << xxx++ << endl;
	CTracer::stream("Proxy", 1) << "Proxy - ostream tracelevel1 " << xxx++ << endl;
	CTracer::stream("Proxy", 2) << "Proxy - ostream tracelevel2 " << xxx++ << endl;
	CTracer::stream("Proxy", 3) << "Proxy - ostream tracelevel3 " << xxx++ << endl;
	CTracer::stream("Proxy", 4) << "Proxy - ostream tracelevel4 " << xxx++ << endl;
	for (int i=0; i<4; i++) {
		CTracer::stream("Proxy", 4) << 'a' << 'b' << endl;
	}

	CTracer::stream("IDotbox", 0) << "IDotbox - ostream tracelevel0 " << xxx++ << endl;
	CTracer::stream("IDotbox", 1) << "IDotbox - ostream tracelevel1 " << xxx++ << endl;
	CTracer::stream("IDotbox", 2) << "IDotbox - ostream tracelevel2 " << xxx++ << endl;


	CTracer::pprintf("Proxy", CTracer::et_fatal, "Proxy - et_fatal message: %d\n", xxx++);
	CTracer::pprintf("Proxy", CTracer::et_alert, "Proxy - et_alert message: %d\n", xxx++);
	CTracer::pprintf("Proxy", CTracer::et_critical, "Proxy - et_critical message: %d\n", xxx++);
	CTracer::pprintf("Proxy", CTracer::et_error, "Proxy - et_error message: %d\n", xxx++);
	CTracer::pprintf("Proxy", CTracer::et_warning, "Proxy - et_warning message: %d\n", xxx++);
	CTracer::pprintf("Proxy", CTracer::et_notice, "Proxy - et_notice message: %d\n", xxx++);
	CTracer::pprintf("Proxy", CTracer::et_info, "Proxy - et_info message: %d\n", xxx++);
	CTracer::pprintf("Proxy", CTracer::et_debug, "Proxy - et_debug message: %d\n", xxx++);
	CTracer::pprintf("Proxy", CTracer::et_verbose, "Proxy - et_verbose message: %d\n", xxx++);
}

