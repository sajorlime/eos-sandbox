/*
 * $Id: SimpleException.h,v 1.1 1998/02/08 18:26:15 bruce Exp $
 * $Source: /export2/Repositories/bdnswlibs/misc/SimpleException.h,v $
 *
 * Copyright (c) 1998 BDN Software Inc.
 * All rights reserved.
 */

#ifndef _SIMPLEEXCEPTION_H__
#define _SIMPLEEXCEPTION_H__
#pragma once

// VC++ 5.0 does not implement exception specifications. So disable the
// warning.
#pragma warning( disable : 4290 )

#include <windows.h>
#include "common/PrimitiveDefs.h"

class SimpleException
{

protected:

  enum {
    ExceptionMsgBufSize = 32,
  } ;

  LPVOID msg ;
  uint32 errorcode ;


public:

  SimpleException() { msg = NULL; errorcode = 0 ; }

  SimpleException(uint32 code) {
    msg = NULL ;
    errorcode = code ;
  }

  uint32 getErrorCode() { return errorcode ; }
  
  virtual ~SimpleException() {
    clearMessage() ;
  }

  virtual const char* name() { return "SimpleException" ; }


  virtual void clearMessage() {
    if(msg){
      LocalFree(msg) ;
      msg = NULL ;
    }
  }

  virtual const char* getMessage() {
    char codebuf[16] ;
    clearMessage() ;
    const char *args[2] ;
    args[0] = name() ;
    args[1] = itoa(errorcode,codebuf,10) ;
    FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER |
		   FORMAT_MESSAGE_ARGUMENT_ARRAY |
		   FORMAT_MESSAGE_FROM_STRING,
		   "%1 (code = %2)",
		   0,
		   0,
		   (LPTSTR) &msg,    
		   ExceptionMsgBufSize,    
		   (char **)args) ;

    return (const char *)msg ;
  }

} ;

#endif
