/*
// 
// $Id:$ 
// 
// Copyright (c) 2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: mapit.cpp
// 
// Creation Date: 2/10/00
// 
// Last Modified: 
// 
// Author: Emil Rojas
// 
// Description: 
//  This file creates common memory mapping utilies for for unix and windows.
//  
*/ 


# include "mmap.h"

# include <sys/mman.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>



CMMap::CMMap (const char * pFileName,
              EMemoryAccess access,
              EMemoryView view,
              bool create) throw (XMMap)
    :
        m_access (access),
        m_view (view)
{
    // gnuc may not be a good definitions for this since mmap may not exist under window
    // even though Win32 is not defined
    uint32 mappingAccess;
    uint32 viewAccess;
    uint32 createAccess = 0;

    if (create)
        createAccess = O_CREAT;

    switch (m_access)
    {
    case EMA_ReadOnly:
        createAccess |= O_RDONLY;
        mappingAccess = PROT_READ;
        viewAccess = MAP_PRIVATE;
        break;
    case EMA_ReadWrite:
        createAccess |= O_RDWR;
        mappingAccess = PROT_READ | PROT_WRITE;
        break;
    default:
        throw XMMap ("Illegal Memory Access type");
    }
    
    switch (m_view)
    {
    case EMV_Private:
        viewAccess = MAP_PRIVATE;
        break;
    case EMV_Shared:
        viewAccess = MAP_SHARED;
        break;
    default:
        throw XMMap ("Illegal Memory View type");
    }


    m_handle = open (pFileName, createAccess);


    if (m_handle < 0)
    {
        throw XMMap ("Failed to create file");
    }

    m_size =  lseek (m_handle, 0, SEEK_END);
    lseek (m_handle, 0, SEEK_SET);

    //printf ("mmap (0, m_size:%x, m_handle:%d\n", m_size, m_handle);
    m_pV = mmap (0, m_size, mappingAccess, viewAccess, m_handle, 0);
    if (!m_pV)
    {
        throw XMMap ("Failed to memory map file");
    }
}


CMMap::CMMap (const char * pFileName,
              size_t fileSize,
              EMemoryView view
             ) throw (XMMap)
    :
        m_access (EMA_ReadWrite),
        m_view (view)
{
    // gnuc may not be a good definitions for this since mmap may not exist under window
    // even though Win32 is not defined
    uint32 mappingAccess;
    uint32 viewAccess;

    switch (m_access)
    {
    case EMA_ReadWrite:
        mappingAccess = PROT_READ | PROT_WRITE;
        break;
    default:
        throw XMMap ("Illegal Memory Access type");
    }
    
    switch (m_view)
    {
    case EMV_Private:
        viewAccess = MAP_PRIVATE;
        break;
    case EMV_Shared:
        viewAccess = MAP_SHARED;
        break;
    default:
        throw XMMap ("Illegal Memory View type");
    }


    m_handle = open (pFileName, O_CREAT | O_RDWR | O_TRUNC, 0666);

    if (m_handle < 0)
    {
        throw XMMap ("Failed to create file");
    }

    m_size = lseek (m_handle, fileSize, SEEK_SET);
    write (m_handle, &m_size, sizeof (m_size));
    lseek (m_handle, 0, SEEK_SET);

    if (m_size != fileSize)
    {
        throw XMMap ("Failed to makefile propersize");
    }

    //printf ("mmap (0, m_size:%x, m_handle:%d\n", m_size, m_handle);
    m_pV = mmap (0, m_size, mappingAccess, viewAccess, m_handle, 0);
    if (!m_pV)
    {
        throw XMMap ("Failed to memory map file");
    }
}


CMMap::~CMMap ()
{
# if 0
    printf ("~CMMAP %p of %d\n", m_pV, m_size); fflush (stdout);
# endif
   // msync (m_pV, m_size, MS_SYNC);
    munmap (m_pV, m_size);
    close (m_handle);
}



