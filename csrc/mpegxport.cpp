



# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <errno.h>

# include <string.h>

# include "argsget.h"

const char * g_pUsage =
    "xportcnt [-t xportfile] [-n non-xportfile] [-bytes N] [-pids P] file\n"
    "Counts the number of transport packets\n"
    "\tif defines xportfile is just the transport packet data\n"
    "\tif defines non-xportfile is just the non-transport packet data\n"
    "\tif defines N when N bytes are read the programe exits with the current counts and data\n"
    "\tif defines P when P packets are read the programe exits with the current counts and data\n"
    ;

char g_getT[128] = "-t";
char g_getN[128] = "-n";

char g_getBytes[128] = "-bytes";
char g_getPids[128] = "-pids";


void pusage (void)
{
    printf (g_pUsage);
}


int nxwrite (int64 pos, int fd, uint8 * pData, int size)
{
    if (size <= 64) // if less than 64 then we don't care.
        return write (fd, (char *) pData, size);

    uint8 * pChk;
    int cnt;
    int wcnt;
    int isize = size;
    char buf[40];

    for (cnt = 0, pChk = pData; cnt < size; cnt++)
    {
        if (*pChk++ != 0)
        {
            if (cnt > 64)
            {
                sprintf (buf, "EPR ZERO%04x  %06x%08x", cnt, (unsigned) (pos >> 32), (unsigned) pos);
                wcnt = write (fd, buf, 32);

                cnt &= ~(4 - 1);
                size -= cnt;
                pos += cnt;
                pData += cnt;
                pChk = pData;
                cnt = 0;

                if (wcnt != 32)
                    return wcnt;
            }
            if (size > 32) // write in 32 byte chunks
            {
                bool allASCII = true;

                wcnt = 32;
                /*
                    Skip all 32 byte chunks that are completely printable strings
                */
                for (int i = 0; i < wcnt; i++)
                {
                    uint8 b = pData[i];
                    if (b == 0)
                        continue;
                
                    if (b < 0x20) // slace
                    {
                        if (b == 0xa || b == 0x13) // NL CR
                            continue;
                    }
                    else
                        if (b < 0x80)
                            continue;
                    allASCII = false;
                    break;
                }
                if (!allASCII)
                {
                    wcnt = write (fd, pData, 32);
                    if (wcnt != 32)
                        return wcnt;
                }
                pData += wcnt;
                pChk = pData;
                size -= wcnt;
                pos += wcnt;
                cnt = 0;
            }
            else
            {
                wcnt = write (fd, pData, size);
                return isize;
            }
        }
    }
    sprintf (buf, "EPR ZERO%04x%08x%08x", cnt, (unsigned) (pos >> 32), (unsigned) pos);
    wcnt = write (fd, buf, 32);
    if (wcnt != 32)
        return wcnt;

    return isize;
}


int xwrite (int64 pos, int fd, uint8 * pData, int size)
{
    return write (fd, (char *) pData, size);
}



# define XPORTLEN 188

byte g_buffer [XPORTLEN * 1024];

# define PID_WIDTH 13

int g_pids[1 << PID_WIDTH] = {0};

# define PID_MASK ((1 << PID_WIDTH) - 1)

# define XSYNC 0x47
# define XERROR (1 << 15)
# define XSTART (1 << 14)
# define XPRIORITY (1 << 13)

# define XIGNORE (XERROR)

/*
    xportcnt [-z number of xports for a xport] [-p] file
*/
int main (int argc, char ** argv)
{
    char * pFile;
    uint8 * pData;
    uint8 * pStart;
    uint8 * pEnd;
    int rcnt;
    int rfd = 0;
    int nfd = 0;
    int tfd = 0;
    uint64 tXCount = 0;
    uint64 tNCount = 0;
    uint64 tPos = 0;
    size_t oXPort = 0;
    bool inXPort = false;
    uint8 * pNXPort;
    int64 ntPos;
    int maxBytes =  (1 << 31) -1;
    int maxPids =  (1 << 31) -1;

    /*
        we need exactly two arguments the command name and the
        file-list-file
    */
    if (argc < 2)
    {
        pusage ();
        return -1;
    }

    CArguments myargs (argc, argv);

    if (myargs.intValue (g_getBytes, &rcnt))
    {
        maxBytes = rcnt;
        printf ("Max Bytes set to %d\n", maxBytes);
    }
    
    if (myargs.intValue (g_getPids, &rcnt))
    {
        maxPids = rcnt;
        printf ("Max Pids set to %d\n", maxPids);
    }
    
    if (myargs.get (g_getT, sizeof (g_getT)))
    {
        tfd = open (g_getT, O_CREAT | O_WRONLY | O_TRUNC, "w");
        if (tfd <= 0)
        {
            printf ("xportcnt failed to open %s\n", g_getT);
            return -3;
        }
    }

    if (myargs.get (g_getN, sizeof (g_getN)))
    {
        nfd = open (g_getN, O_CREAT | O_WRONLY | O_TRUNC, "w");
        if (nfd <= 0)
        {
            printf ("xportcnt failed to open %s\n", g_getN);
            return -3;
        }
    }

    myargs.adjust ();

    pFile = myargs.last ();
    if (!pFile)
    {
        printf ("xportcnt did not file argument %s\n", pFile);
        return -2;
    }

    rfd = open (pFile, O_RDONLY);
    if (rfd <= 0)
    {
        printf ("xportcnt failed to open %s\n", pFile);
        return -3;
    }

    pStart = g_buffer;

outer:
    while (1)
    {
        rcnt = read (rfd, g_buffer, sizeof (g_buffer));
        //printf ("read returned %d bytes of %d requested\n", rcnt, sizeof (g_buffer));
# if 0
        if (rcnt != sizeof (g_buffer))
        {
            printf ("xportcnt read %d\n", rcnt);
        }
# endif
        if (rcnt <= 0)
        {
            if (rcnt == 0)
                break;
            printf ("xportcnt failed read of %s with %d\n", pFile, rcnt);
            return -4;
        }
        pData = pStart;
        pEnd = pStart + rcnt;
        if (inXPort)
            pData += oXPort;

        while (pData < pEnd)
        {
            if (inXPort)
            {
                int64 xtPos = tPos;
                if ((pEnd - pData) < (4 + XPORTLEN))
                {
                    /*
                        If we have a packet at the end of the buffer we
                        want to move it to the beginining of the buffer.
                        And then read enough so that we shift the data to be
                        at the beginning of the buffer.
                    */
                    int cnt = pEnd - pData;
                    uint8 * pMove = pStart;
                    while (pData < pEnd)
                        *pMove++ = *pData++;
                    rcnt = read (rfd, g_buffer + cnt, sizeof (g_buffer) - cnt);
                    pEnd = pStart + cnt + rcnt;
                }
                uint8 sync = pData[0];
                uint32 wpid = (uint32) (pData[1] << 8);
                wpid += (uint32) (pData[2] << 0);
                uint32 pid = wpid & PID_MASK;
                
                if ((sync != XSYNC) || (wpid & XIGNORE))
                {
                    inXPort = false;
                    continue;
                }
                if (wpid & XSTART)
                {
                    printf ("Transport start for %05x @ %08x%08x\n",
                                (unsigned) (wpid & ~XSTART), (unsigned) (tPos >> 32), (unsigned) tPos);
                }
                if (tfd > 0)
                {
                    int wcnt;

                    wcnt = xwrite (xtPos, tfd, pData, XPORTLEN);
                    if (wcnt != XPORTLEN)
                    {
                        printf ("xportcnt failed to write data to %d\n", tfd);
                    }
                }
                g_pids[pid]++;
                if (maxPids-- <= 0)
                {
                    printf ("Max Pids reached in transport data\n");
                    goto allDone;
                }

                tXCount += XPORTLEN;
                tPos += XPORTLEN;
                pData += XPORTLEN;
                if ((maxBytes -= XPORTLEN) <= 0)
                {
                    printf ("Max Bytes reached in transport data\n");
                    goto allDone;
                }
            }
            else
            {
                pNXPort = pData;
                ntPos = tPos;

            findSync:
                while (*pData != XSYNC) 
                {
                    tNCount++;
                    tPos++;
                    if ((maxBytes -= 1) <= 0)
                    {
                        printf ("Max Bytes reached in findSync\n");
                        goto allDone;
                    }
                    if (++pData >= pEnd)
                    {
                        if (nfd > 0)
                        {
                            int wcnt;
                            int wsiz = pData - pNXPort;
                            wcnt = nxwrite (ntPos, nfd, pNXPort, wsiz);
                            if (wcnt != wsiz)
                            {
                                printf ("xportcnt failed to write data to %d\n", nfd);
                            }
                        }
                        goto outer;
                    }
                }

                if ((pEnd - pData) < (4 + XPORTLEN))
                {
                    /*
                        If we have a packet at the end of the buffer we
                        want to move it to the beginining of the buffer.
                        And then read enough so that we shift the data to be
                        at the beginning of the buffer.
                    */
                    int cnt = pEnd - pData;
                    uint8 * pMove = pStart;
                    while (pData < pEnd)
                        *pMove++ = *pData++;
                    tPos += cnt;;
                    rcnt = read (rfd, g_buffer + cnt, sizeof (g_buffer) - cnt);
                    pEnd = pStart + cnt + rcnt;
                }
                if (pData[XPORTLEN] != XSYNC)
                {
                    pData++;
                    tPos++;
                    goto findSync; // if next one is not a packet don't count it.  We miss the last
                }
                uint32 wpid = (uint32) (pData[1] << 8);
                wpid += (uint32) (pData[2] << 0);
                // Let's reject anything with a transport error or transport priority flags
                if (wpid & XIGNORE)
                {
                    pData++;
                    tPos++;
                    goto findSync; // this is not a transport packet
                }
                if (wpid & XSTART)
                {
                    printf ("Transport start for %05x @ %08x%08x\n",
                                (unsigned) (wpid & ~XSTART), (unsigned) (tPos >> 32), (unsigned) tPos);
                }
                inXPort = true;
                oXPort = (pData - pStart) % XPORTLEN;
                if (nfd > 0)
                {
                    int wcnt;
                    int wsiz = pData - pNXPort;
                    if (wsiz > 0)
                    {
                        wcnt = nxwrite (ntPos, nfd, pNXPort, wsiz);
                        if (wcnt != wsiz)
                        {
                            printf ("xportcnt failed to write data to %d\n", nfd);
                        }
                    }
                }
            }
        }
    }
allDone:
    
    printf ("WE DONE total XPort Count: %08x%08x\n", (unsigned) (tXCount >> 32), (unsigned) tXCount);
    printf ("WE DONE total NonXP Count: %08x%08x\n", (unsigned) (tNCount >> 32), (unsigned) tNCount);
    printf ("WE DONE total Total Count: %08x%08x\n", (unsigned) (tPos >> 32), (unsigned) tPos);

    int tpids = 0;
    for (int pid = 0; pid < (1 << PID_WIDTH); pid++)
    {
        if (g_pids[pid])
        {
            tpids += g_pids[pid];
            printf ("PID:0x%04x has %d packets\n", pid, g_pids[pid]);
        }
    }
    printf ("Total PIDS/Packets %d\n", tpids);
    return 0;
}

