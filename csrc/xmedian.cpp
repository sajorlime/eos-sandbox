
/*
 * File: median.cpp
 *
 * Implementation of a median filter.
 *
 */


# include <algorithm>
# include <cassert>
# include <cstdlib>
# include <iostream>
# include <fstream>
# include <iostream>
# include <random>
# include <string>
# include <stdio.h>

#pragma GCC diagnostic ignored "-Warray-bounds"

# define USE_STD_SORT 1

typedef uint32_t mftype;
typedef int mfindex;

void ocol(std::ostream& ofs, float x, int z)
{
    ofs << x << ", ";
}

void ocol(std::ostream& ofs, int x, int z)
{
    ofs << x << ", ";
}

// Generate some random noise from input, this is a hack for testing
// not a stotasticly meaningful value
// Args:
//  ratio: the max noise level ratio
// Return:
//   randomr, where random is [-0.5, 0.5]
float some_noise(float ratio=0.1)
{
    float f = static_cast<float>(rand()) / RAND_MAX;
    float fr = f - 0.5;
    //std::cout << ocol(std::cout, f) << ocol(fr) << std::endl;
    return ratio * fr;
}

const int mfsize = 15;
const int mfm_pos = mfsize >> 1;
// store the last mfsize values, in rotating buffer
mfindex mfl_pos = 0;
mftype mf_last[mfsize]; // value of 16 bits or less
// note wie fille both mf_last and mf_sort with a single value to start!
mftype mf_sorted[mfsize]; // value of 16 bits or less
mftype* mf_median = &mf_sorted[mfm_pos];
mftype* mf_mend = &mf_sorted[mfsize];
bool last_median_insert_was_front = false;

void show_sorted(const char*);

void mf_init(mftype sv)
{
    for (int i = 0; i < mfsize; i++) {
        mf_last[i] = sv;
        mf_sorted[i] = sv;
    }
    mfl_pos = 0;
    show_sorted("Init: ");
}

mftype get_ref(mftype y)
{
    mfindex l_pos = mfl_pos;
    mftype ref = mf_last[mfl_pos];
    mf_last[mfl_pos++] = y;
    if (mfl_pos >= mfsize)
        mfl_pos = 0;
# if 1
    mfindex i = 0;
    for (i = 0; i < mfsize; i++)
    {
        if (mf_sorted[i] == ref)
            break;
    }
    if (i == mfsize)
    {
        std::cout << "ref not found: " << ref << std::endl;
        std::cout << "l_pos: " << l_pos << std::endl;
        show_sorted("bad ref: ");
    }
    else
    {
        std::cout << "gref : " << ref
                  << " found @ " << l_pos
                  << " with y: " << y
                  << std::endl;
    }
    assert(i != mfsize);
# endif
    return ref;
}

//  0     1     2     3     4     $     6     7
// 6000, 6001, 6001, 6005, 6006, 6007, 6008, 6011,
//   8      9    10    11    12    13    14
// 60011, 6013,
//
# define DEBUG_FR 1

mftype* find_ref(const mftype ref)
{
    mftype* pref;
    if (ref < *mf_median)
    {
        mftype* pquarter = mf_median - (mfsize >> 1);
        if (ref < *pquarter)
        {
            pref = mf_sorted;
        }
        else
        {
            pref = pquarter;
        }
    }
    else
    {
        mftype* p3quarter = mf_median + (mfsize >> 1);
        if (ref < *p3quarter)
        {
            pref = mf_median;
        }
        else
        {
            pref = p3quarter;
        }
    }
    while (*pref < ref)
        pref++;
    return pref;
}

void show_sorted(const char* info="Sorted: ")
{
    std::cout << info;
    for (int l = 0; l < mfsize; l++) {
        if (l == (mfsize >> 1))
            std::cout << "**" << mf_sorted[l] << "**, ";
        else
            std::cout << mf_sorted[l] << ", ";
    }
    std::cout << std:: endl;
    std::cout << "last(" << mfl_pos << "): ";
    mfindex lpos = mfl_pos;
    for (int l = 0; l < mfsize; l++) {
        std::cout << lpos << ":" << mf_last[lpos] << ", ";
        lpos++;
        if (lpos >= mfsize)
            lpos = 0;
# if 0
        if (l == mfl_pos)
            std::cout << "**" << mf_last[l] << "**, ";
        else
            std::cout << mf_last[l] << ", ";
# endif
    }
    std::cout << std:: endl;
}

bool check_for_valid(mftype v)
{
    for (int l = 0; l < mfsize; l++)
    {
        mftype x = mf_sorted[l];

        if (x == v)
        {
# if 1
            std::cout << "cfv x(" << x << ")";
            std::cout << " == v(" << v << ")";
            std::cout << " @ " << l;
            std::cout << std::endl;
# endif
            return true;
        }
        if (x > v)
        {
            std::cout << "cfv(" << x << ")";
            std::cout << " < (" << v << ")";
            std::cout << " @ " << l;
            std::cout << std::endl;
            return false;
        }
    }
    return true;
}

bool mf_validate(bool exact=false)
{
    for (int l = 1; l < mfsize; l++)
    {
        int d = ((int) mf_sorted[l]) - ((int) mf_sorted[l - 1]);
        if ((d < 0)
            || (exact && (abs(d) > 1))
           )
        {
            std::cout << "invalid: " << d << ":";
            std::cout << l << ":"
                      << mf_sorted[l - 1]  << " > "
                      << mf_sorted[l]
                      << std::endl;
            show_sorted("S-invalid: ");
            return false;
        }
    }
    for (int l = 0; l < mfsize; l++)
    {
        if (not check_for_valid(mf_last[l]))
            return false;
    }
    return true;
}

void mf_push_up(mftype* pmv, mftype* pmv_end)
{
    mftype v0 = *pmv;
    mftype v1;
    for (; pmv < pmv_end; pmv++)
    {
        v1 = *pmv;
        *pmv = v0;
        v0 = v1;
    }
}


void mf_push_down(mftype* mfsp, mftype* pmv)
{
        mftype v0 = *pmv;
        mftype v1;
        for (; pmv > (mfsp - 1); pmv--)
        {
            v1 = *pmv;
            *pmv = v0;
            v0 = v1;
        }
}

int mfilter(const mftype y, bool show=false)
{
    if (show)
    {
        show_sorted("S-mf: ");
    }

    mftype ref_val = get_ref(y);
    show_sorted("mf-post-gref: ");
    // if the new value equals the old value, we need
    // to keep it the same.
    if (y == ref_val)
    {
        show_sorted("mf-special-case: ");
        return *mf_median;
    }
    if (y == *mf_median)
    {
        return y;
    }
    
# if 1
    // we the new value is outside of the current range
    // we need to move the entire sorted list
    if (y <= mf_sorted[0])
    {
        // we need more move mf_sorted up
        //show_sorted("S-mf-push-up: ");
        show_sorted("S-mf-pre-push-up: ");
        mf_push_up(mf_sorted, mf_mend);
        *mf_sorted = y;
        show_sorted("S-mf-post-push-up: ");
        if (not mf_validate())
        {
            assert(0);
        }
        return *mf_median;
    }
    if (y >= *(mf_mend - 1))
    {
        std::cout << "S-mf-ref-gend:" << ref_val << " y:" << y << std::endl;
        std::cout << "S-mf-pd-gend:" << *(mf_mend - 1) << " y:" << y << std::endl;
        show_sorted("S-mf-push_down: ");
        mf_push_down(mf_sorted, mf_mend - 1);
        *(mf_mend - 1) = y;
        show_sorted("S-mf-post-push_down: ");
        if (not mf_validate())
        {
            assert(0);
        }
        return *mf_median;
    }
# endif
    //show_sorted("MF-not-at-ends: ");
    // find replace value ref_val with binary search;
    mftype* mv_pos;
    std::cout << "MF-fref:" << ref_val << " y:" << y << std::endl;
# if USE_STD_SORT
    mv_pos = find_ref(ref_val);
    std::cout << "MF-post-fref:" << (mv_pos - mf_sorted) << " y:" << y << std::endl;
    if (*(mv_pos - 1) == ref_val)
    {
        while (*(--mv_pos) == ref_val);
    }   
    *mv_pos = y;
    show_sorted("MF-before-sort: ");
    std::sort(mf_sorted, mf_mend);
    show_sorted("MF-after-sort: ");
    (void) ref_val;
# else
    mv_pos = find_ref(y, mf_sorted, mf_mend - mf_sorted);
    // we found at least one ref_val, now we need to make sure it
    // is the first one.
    if (*(mv_pos - 1) == ref_val)
    {
        while (*(--mv_pos) == ref_val);
    }
    mf_push_up(mv_pos, mf_mend);
# if 0
    // If it is the first one and it is the median, we flipflop the insert.
    //std::cout << "MF-fpos:" << mv_pos - mf_sorted << std::endl;
    // if we are at the median, we need to push up or down and insert
    // at median, we alternate up and down.
    if (mv_pos == mf_median)
    {
        if (last_median_insert_was_front)
        {
            //show_sorted("at-med-push-down: ");
            mf_push_down(mf_sorted, mf_median);
            *mf_median = y;
            last_median_insert_was_front = false;
        }
        else
        {
            //show_sorted("at-med-push-up: ");
            mf_push_up(mf_median, mf_mend);
            *mf_median = y;
            last_median_insert_was_front = true;
        }
        //std::cout << "mf: y == ref: " << y << std::endl;
        return y;
    }
    //show_sorted("MF-search: ");
    if (mv_pos < mf_median)
    {
        //std::cout << "MF-push-down: y: " << y << std::endl;
        mf_push_down(mf_sorted, mf_median);
    }
    else
    {
        //std::cout << "MF-push-up: y: " << y << std::endl;
        mf_push_up(mf_median, mf_mend);
    }
    *mv_pos = y;
# endif
# endif
    return *mf_median;
}

void test_insert_at_middle()
{
    mftype y0 = *mf_median;
    for (int i = 0; i < mfsize + 1; i++)
    {
        //std::cout << "tiam: " << i << ", y0: " << y0 << std::endl;
        mfilter(y0);
    }
    //show_sorted("mid-mid:");
}

void test_insert_at_end()
{
    //std::cout << "at-end" << std::endl;
    mftype y0 = 100000;
    mftype y1;
    mf_init(y0);
    int i;
    for (i = 0; i < (mfsize / 2); i++)
    {
        y1 = mfilter(y0 + i);
        assert(y1 == y0);
        mf_validate(true);
    }
    y1 = mfilter(y0 + i);
    //show_sorted("end-mid:");
    //std::cout << "end:" << i << ":" << y1 << std::endl;
    assert(y1 == y0);
    mf_validate(true);
    i++;
    y1 = mfilter(y0 + i);
    //show_sorted("end-mid-2:");
    //std::cout << "end:" << i << ":" << y1 << std::endl;
    assert(y1 == (y0 + 1));
    mf_validate(true);
    i++;
    for (; i < 4 * mfsize + 1; i++)
    {
        y1 =  mfilter(y0 + i);
        //std::cout << "loop-end:" << i << ":" << y1 << std::endl;
        if (not mf_validate(true))
        {
            std::cout << "invalid @ " << i << std::endl;
            //show_sorted("inv-end-At-End: ");
            return;
        }
    }
    show_sorted("end-At-End: ");
    test_insert_at_middle();
}

void test_insert_at_front()
{
    mftype y0 = 100000;
    mftype y1;
    mf_init(y0);
    int i;
    //show_sorted("at-front:");
    for (i = 0; i < (mfsize / 2); i++)
    {
        y1 = mfilter(y0 - i);
        //std::cout << "tiam: " << i << ":" << y0 << ", " << y1 << std::endl;
        if (not mf_validate(true))
            return;
        //show_sorted("front-early:");
    }
    y1 = mfilter(y0 - i);
    //show_sorted("front-mid:");
    //std::cout << "front:" << i << ":" << y1 << std::endl;
    if (not mf_validate(true))
        return;
    assert(y1 == y0);
    i++;
    y1 = mfilter(y0 - i);
    //show_sorted("front-mid-2:");
    //std::cout << "front-mid:" << i << ":" << y1 << std::endl;
    if (not mf_validate(true))
        return;
    assert(y1 == (y0 - 1));
    i++;
    for (; i < 4 * mfsize + 1; i++)
    {
        y1 = mfilter(y0 - i);
        if (not mf_validate(true))
            return;
    }
    show_sorted("end-At-front: ");
    test_insert_at_middle();
}

# define TEST_FRONT 0
# define TEST_END 0
# define TEST_F_AND_E_ONLY 0

int main(int argc, char** argv)
{
# if TEST_FRONT
    test_insert_at_front();
# endif
# if TEST_END
    test_insert_at_end();
# endif
# if TEST_F_AND_E_ONLY
    return 1;
# endif
    if (argc < 2)
    {
        std::cout << "usage: ofile, start, slope, ratio, limit, scale" << std::endl;
        return 1;
    }
    int argi = 1;
    std::ofstream csv(argv[argi++]);
    mftype start = 600;
    if (argi < argc)
        start = (mftype) std::stoi(argv[argi++]);
    float m = 0.1;
    //std::cout << "argi:" << argi << " argc: " << argc << std::endl;
    if (argi < argc)
        m = std::stof(argv[argi++]);
    //std::cout << "M:" << m << std::endl;
    //char c;
    //std::cin >> c;
    float r = 0.1;
    if (argi < argc)
         r = std::stof(argv[argi++]);
    int limit = 1024;
    if (argi < argc)
         limit = std::stoi(argv[argi++]);
    float scale = 100;
    if (argi < argc)
         scale = std::stof(argv[argi++]);
    mf_init(scale * start);
    csv << " N,  ln,   Y,    m,   noise,    y + n,    y0,   mfo, ";
    csv << "  fudge, ";
    csv << " smin,  s1,   s2,   s3,    s4,   s5,    s6,   smedian, ";
    csv << "  s8,   s9,   s10,    s11,   s12,    s13,   smax";
    csv << std::endl;
    float fudge = 0.0;
    float last_y = start;
    int last_i = 0;
    for (int i = 0; i < limit; i++)
    {
        if (fudge != 0.0)
        {
            m = -m;
        }
        //float y0 = genline(m, i);
        float y = last_y + m * last_i + fudge;
        last_y = y;
        last_i++;
        float n = some_noise(r);
        float z = y + n;
        //std::cout << n << ", " << z << std::endl;
        mftype y0 =  (int) ((scale * z) + 0.5);
        std::cout << "i:" << i << ": " << y0 << std::endl;
        mftype y1 = mfilter(y0, false);
        //show_sorted("after-main-filter: ");
        csv << i << ", "
            << last_i << ", "
            << y << ", "
            << m << ", "
            << n << ", "
            << z << ", "
            << y0 << ", "
            << y1 << ", "
            << fudge << ", ";
        for (int l = 0; l < mfsize; l++) {
            csv << mf_sorted[l] << ", ";
        }
        csv << std::endl;
        if (fudge != 0.0)
            fudge = 0.0;
        if (not mf_validate())
        {
            std::cout << "NOT SORTED: " << i << std::endl;
            std::cout << "y:" << y << ", z:" << z << ", y0:" << y0 << std::endl;
            std::cout << "y1: " << y1 << ", fudge: " << fudge << std::endl;
            show_sorted("main-not-valid: ");
            return 1;
        }
        if (y > (1.5 * start))
        {
            fudge = -3.0 * (y - (1.5 * start));
            last_i = 1;
            if ((y + fudge) > (1.5 * start))
            {
                std::cout << "failed to adjust: "
                    << y << " > "
                    << (1.5 * start)
                    << std::endl;
                    return 1;
            }
# if DEBUG_FUDGE
            std::cout << "failed to adjust: "
                << y << " > "
                << (1.5 * start)
                << " fudge: " << fudge
                << std::endl;
            //return 1;
# endif
        }
        if (y < (0.5 * start))
        {
            fudge = 3.0 * ((0.5 * start) - y);
            last_i = 1;
# if DEBUG_FUDGE
            std::cout << "failed to adjust: "
                << y << " < "
                << (0.5 * start)
                << " fudge: " << fudge
                << std::endl;
            //return 1
# endif
        }
    }
    show_sorted("end-mf: ");

    return 0;
    //  run m.csv 60 0.01 0.01 100
}

