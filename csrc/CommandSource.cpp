/*
//
// $Id: //Source/Common/libutility/CommandSource.cpp#11 $
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Author: Gordie
//
// Brief Description: 
//
*/

#ifdef _WIN32
#pragma warning (disable : 4786)
#endif //_WIN32

# include "CommandSource.h"

# include <map>
# include <string>
# include <vector>
# include <iostream>
# include <strstream>
# include <stdlib.h>

# include <ctype.h>

__BEGIN_DC_NAME_SPACE

typedef std::map<std::string, CommandHandlerInfo> CommandInfoMap;
typedef CommandInfoMap::const_iterator CommandInfoIterator;
typedef std::vector<const char*> CommandHistory;
typedef std::vector<const char*>::const_iterator CommandHistoryIterator;

class CCommandSource : public PCommandSource
{
public:	
	CCommandSource ();
	virtual ~CCommandSource ();

	virtual void reportHelp (const char* command, POutputAgent* agent) const;
	virtual char* getHistory () const;
	virtual void reportHistory (POutputAgent* agent) const;
	virtual void addCommandToHistory (const char* command) { m_history.push_back(command); }
	virtual bool processCommand (const char* command, POutputAgent* agent, bool addToHistory = true, void* data = 0);
	virtual void registerCommand (const char* commandName, CommandHandlerInfo* handlerInfo);

private:
	CommandInfoMap m_map;
	CommandHistory m_history;
	char* m_pLastCommand;

	// TODO: Enforce maximum size. Vector probably not best thing to use
	// when get to the limit :-)
	const char* getCommandFromHistory (const char* command);

	CCommandSource (CCommandSource& rhs);
	CCommandSource& operator=(CCommandSource& rhs);
};

PCommandSource* PCommandSource::CreateCommandSource () {
	return new CCommandSource();
}

CCommandSource::CCommandSource () : PCommandSource(), m_pLastCommand(0)
{
}

CCommandSource::~CCommandSource ()
{
}

void CCommandSource::reportHelp (const char* command, POutputAgent* agent) const {
	if (agent == 0) {
		return;
	}
	CommandInfoIterator iterator;
	if (command == 0) {
		for (iterator = m_map.begin(); iterator != m_map.end(); iterator++) {
			CommandHandlerInfo cmh = iterator->second;
			if (cmh.getHelpFunction() != 0) {
				(*cmh.getHelpFunction())(cmh.getHandler(), command, agent);
			}
			else if (cmh.getHelpDescription() != 0) {
				agent->outputFormatString("%s", cmh.getHelpDescription());
			}
			else {
				const char* s = iterator->first.c_str();
				agent->outputFormatString("%s", const_cast<char*>(s));
			}
		}
	}
	else {
		iterator = m_map.find(command);
		if (iterator != m_map.end()) {
			CommandHandlerInfo cmh = iterator->second;
			if (cmh.getHelpFunction() != 0) {
				(*cmh.getHelpFunction())(cmh.getHandler(), command, agent);
			}
			else {
				agent->outputFormatString("%s", cmh.getHelpDescription());
			}
		}
		else {
			agent->outputFormatString("Command \"%s\" not found", command);
		}
	}
}

const char* const separators = " (,;)\t\n";

char* CCommandSource::getHistory () const {
	CommandHistoryIterator iterator;
	char* s;
	std::string response = "";
	for (iterator = m_history.begin(); iterator != m_history.end(); iterator++) {
		response += *iterator;
		response += "\n";
	}
	s = new char[response.size() + 1];
	::strcpy(s, response.c_str());
	return s;
}

void CCommandSource::reportHistory (POutputAgent* agent) const {
	if (agent == 0) {
		return;
	}
	CommandHistoryIterator iterator;
	int i = 0;
	for (iterator = m_history.begin(); iterator != m_history.end(); iterator++) {
		agent->outputFormatString("%4d: %s", i++, *iterator);
	}
}

const char* CCommandSource::getCommandFromHistory (const char* command) {
	int i;
	int length = ::strlen(command);
	const char* historyCommand = 0;
	if ((::strcmp(command, "!") ==0) || (::strcmp(command, "!!") ==0)) {
		return m_pLastCommand;
	}
	for (i = 0; i < length; i++) {
		if (isdigit(command[i]) == 0) {
			return 0;
		}
	}
	i = ::atoi(command);
	if (i < static_cast<int>(m_history.size())) {
		historyCommand = m_history[i];
	}
	return historyCommand;
}

bool CCommandSource::processCommand (const char* command, POutputAgent* agent, bool addToHistory, void* data) {
	uint32 argc = 0;
	char* argv[100];
	char* originalArgv[100];
	char* commandCopy;
	const char* s;
	char tempCommand[64];
	const char* historyCommand;
	size_t separatorsLength = ::strlen(separators);
	uint32 i, j;
	bool rc = false;

	size_t commandLength;
	if (command == 0) {
		return false;
	}
	if ((historyCommand = getCommandFromHistory(command)) != 0) {
		if (agent != 0) {
			agent->outputFormatString("(%s)", historyCommand);
		}
		return processCommand(historyCommand, agent, false, data);
	}
	commandLength = ::strlen(command);
	commandCopy = new char[commandLength+1];
	::strcpy(commandCopy, command);

	i = 0;
	for (;;) {
		for (; i < commandLength; i++) {
			for (j = 0; j < separatorsLength; j++) {
				if (commandCopy[i] == separators[j]) {
					break;
				}
			}
			if (j >= separatorsLength) {
				argv[argc] = commandCopy + i;
				originalArgv[argc] = commandCopy + i;
				argc++;
				break;
			}
		}
		for (; i < commandLength; i++) {
			for (j = 0; j < separatorsLength; j++) {
				if (commandCopy[i] == separators[j]) {
					break;
				}
			}
			if (j < separatorsLength) {
				commandCopy[i++] = '\0';
				break;
			}
		}
		if (i >= commandLength) {
			break;
		}
	}

	CommandInfoIterator iterator = m_map.find(argv[0]);
	if (iterator == m_map.end()) {
		// Try to find a partial match
		size_t n = ::strlen(argv[0]);
		for (iterator = m_map.begin(); iterator != m_map.end(); iterator++) {
			s = iterator->first.c_str();
			if (::strncmp(s, argv[0], n) == 0) {
				if (agent != 0) {
					agent->outputFormatString("%s%s", s, command + strlen(argv[0]));
				}
				::strncpy(tempCommand, s, sizeof(tempCommand)-1);
				tempCommand[sizeof(tempCommand)-1] = '\0';
				argv[0] = tempCommand;
				break;
			}
		}
	}
	if (iterator != m_map.end()) {
		CommandHandlerInfo cmh = iterator->second;
		if (cmh.getProcessFunction() != 0) {
			rc = (*cmh.getProcessFunction())(cmh.getHandler(), command, argc, argv, agent, data);
		}
		if ((addToHistory == true) && (cmh.getPutInHistory() == true)) {
			for (i = 0, j = 0; i < argc; i++) {
				j += ::strlen(argv[i]) + 1;
			}
			char* newCommand = new char[j + 1];
			::strcpy(newCommand, argv[0]);
			for (i = 1; i < argc; i++) {
				::strcat(newCommand, " ");
				::strcat(newCommand, argv[i]);
				if (argv[i] != originalArgv[i]) {
					delete [] argv[i];
				}
			}
			if (m_history.empty() || (::strcmp(m_history.back(), newCommand) != 0)) {
				addCommandToHistory(newCommand);
			}
			else {
				delete [] newCommand;
			}
			if (m_pLastCommand != command) {
				if (m_pLastCommand != 0) {
					delete [] m_pLastCommand;
				}
				m_pLastCommand = new char[commandLength+1];
				::strcpy(m_pLastCommand, command);
			}
		}
	}
	delete [] commandCopy;
	return rc;
}

void CCommandSource::registerCommand (const char* commandName, CommandHandlerInfo* handlerInfo) {
	if (handlerInfo == 0) {
		m_map.erase(commandName);
	}
	else {
		m_map[commandName] = *handlerInfo;
	}
}

char* CommandHandlerInfo::DefaultHelpDescription = "";

void CommandHandlerInfo::copyCommandHandlerInfo (void* aHandler, const char* aHelpDescription, 
											 CommandHelpFunction aHelpFunction, CommandProcessFunction aProcessFunction,
											 bool aPutInHistory)
{
	handler = aHandler;
	helpFunction = aHelpFunction;
	processFunction = aProcessFunction;
	putInHistory = aPutInHistory;
	copyCommandHandlerHelpDescription(aHelpDescription);
}

void CommandHandlerInfo::copyCommandHandlerHelpDescription (const char* aHelpDescription)
{
	try {
		if (aHelpDescription != 0) {
			helpDescription = new char[::strlen(aHelpDescription)+1];
			::strcpy(helpDescription, aHelpDescription);
		}
		else {
			helpDescription = CommandHandlerInfo::DefaultHelpDescription;
		}
	} catch (...) {
		helpDescription = CommandHandlerInfo::DefaultHelpDescription;
	}
}

CommandHandlerInfo::CommandHandlerInfo (void* handler, const char* helpDescription, 
										CommandHelpFunction helpFunction, CommandProcessFunction processFunction,
										bool putInHistory) 
{
	copyCommandHandlerInfo(handler, helpDescription, helpFunction, processFunction, putInHistory);
}

CommandHandlerInfo::CommandHandlerInfo ()
{
	copyCommandHandlerInfo(0, 0, 0, 0, false);
}

CommandHandlerInfo::CommandHandlerInfo (const CommandHandlerInfo& rhs)
{
	copyCommandHandlerInfo(rhs.handler, rhs.helpDescription, rhs.helpFunction, rhs.processFunction, rhs.putInHistory);
}

CommandHandlerInfo::~CommandHandlerInfo() {
	if (helpDescription != CommandHandlerInfo::DefaultHelpDescription) {
		delete [] helpDescription;
	}
}

CommandHandlerInfo& CommandHandlerInfo::operator=(const CommandHandlerInfo& rhs) {
	copyCommandHandlerInfo(rhs.handler, rhs.helpDescription, rhs.helpFunction, rhs.processFunction, rhs.putInHistory);
	return *this;
}

__END_DC_NAME_SPACE

