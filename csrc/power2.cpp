#include <iostream>
#include <iomanip>
#include <bitset>
using namespace std;
#include <cstdio>
#include <cstdlib>
#include <cstdint>

int main(int argc, char** argv) 
{
    for (int i = 1; i < argc; i++)  {
        int32_t ai = atoi(argv[i]);
        uint32_t ui = ~ai;
        //uint32_t onebit = (uint32_t) ai & ~ui;
        cout << hex << setw(5) << setfill('0')
              << ai << " : "
              << ui << endl;
        cout << bitset<16>(ai) << endl;
        cout << bitset<16>(ui) << endl;
        cout << bitset<16>(-ai) << endl;
        cout << bitset<16>(-ai & ui) << endl;
        cout << endl;
        //cout << bitset<16>(xi & (ai << 1)) << endl;
    }

    return 0;
}
