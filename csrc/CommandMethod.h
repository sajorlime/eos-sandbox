/*
//
// $Id: //Source/Common/libutility/CommandMethod.h#2 $
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Author: Gordie
//
// Brief Description: 
//
*/

#ifndef __COMMANDMETHOD_H
#define __COMMANDMETHOD_H

# include "dotcastns.h"
# include "dctype.h"

__BEGIN_DC_NAME_SPACE

struct CommandArgument {
	enum Type {
		Int,
		Double,
		Bool,
		String
	};
	Type type;
	union {
		int intValue;
		double doubleValue;
		bool boolValue;
		char* stringValue; // Owned by caller, copy if needed as caller may free upon return
	};
};

#define ARGUMENT_NUMERIC_VALUE(arg) \
(((arg).type == CommandArgument::Int) ? (arg).intValue : \
(((arg).type == CommandArgument::Double) ? (arg).doubleValue : \
(((arg).type == CommandArgument::Bool) ? (arg).boolValue : 0)))

extern const char* ConvertArgumentTypeToString (CommandArgument::Type type);

class PCommandMethodHandler {
public:
	virtual CommandArgument handleCommand (const char* commandName, int id, void* sender, CommandArgument* args, size_t argc) = 0;
};

class PCommandManager {
public:	
	PCommandManager () {}
	virtual ~PCommandManager () {}

	static PCommandManager* CreateCommandManager();

	virtual bool processCommand (const char* commandName, int id, void* sender, CommandArgument* args, size_t argc, CommandArgument& retval) = 0;
	virtual void registerCommandForObject (const char* commandName, PCommandMethodHandler* handler) = 0;

private:
	PCommandManager (PCommandManager& rhs);
	PCommandManager& operator=(PCommandManager& rhs);
};

__END_DC_NAME_SPACE


#endif // __COMMANDMETHOD_H

