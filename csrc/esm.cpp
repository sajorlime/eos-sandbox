
class GridMeter : Meter
{
};

class InverterMonitor : Monitor
{
};
typedef class InverterMonitor IM;

class BatteryMonitor : Monitor
{
};
typedef class BatteryMonitor BM;

class GridPrice
{
    GripPrice(HttpCon con);
    float current_value();
    float future_value(float hours);
};
typedef class GridPrice GP;

class ValueCalculator : Calculator
{
    GP& _gp;
    GM& gm;
    IM& _im;
    BM& _bm;
    ValueCalculator(GM& gp, GM* gm, IM& im,BM& bm);
    std::tuple<iv_state, grid_connected, battery_charge> current_value();
};

typedef class ValueCalculator VC;

class EnergyMonitor : Monitor, ValueCalculator
{
    Devices& _dlist
    BM _bm;
    IM _im;
    GM _gm;

    EnergyMonitor(Devices& dlist, HttpCon con)
    :  VC_dlist(dlist), _bm, ... {}
}

class LimitStateMonitor : EM
{
    
}

class ErrorStateMonitor : LSM
{
}

