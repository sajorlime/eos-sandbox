/* 
// 
// $Id$ 
// 
// Copyright (c) 1999-2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: exception.h
// 
// Creation Date: 11/19/99
// 
// Last Modified: 
// 
// Author: Emil P. Rojas
// 
// Description: 
// 
//  
*/


# ifndef __EXCEPTION_H__
# define __EXCEPTION_H__

__BEGIN_DC_NAME_SPACE

class XException
{
private:
    const char * m_pReason;
protected:
public:
    XException (const char * pReasonFormat, ...);
    virtual ~XException ();
    const char * reason (void) {return m_pReason;}
};

__END_DC_NAME_SPACE

# endif

