#include <iostream>

using namespace std;

// A node in a red-black tree.
struct Node {
  int key;
  Node* left;
  Node* right;
  bool color;
};

// Inserts a new node into the tree.
Node* insert(Node* head, int key) {
  // Create a new node.
  Node* newNode = new Node();
  newNode->key = key;
  newNode->color = RED;

  // Find the insertion point.
  Node* current = head;

  // While the current node is not null.
  while (current != NULL) {
    // If the key is less than the current node's key.
    if (key < current->key) {
      // Move the current node to the left.
      current = current->left;
    } else if (key > current->key) {
      // Move the current node to the right.
      current = current->right;
    } else {
      // Key is already present.
      // Return the current node.
      return current;
    }
  }

  // Make the new node the left child of the current node.
  newNode->left = current;

  // Return the head of the tree.
  return head;
}

// Deletes a node from the tree.
void delete(Node* head, int key) {
  // Find the node to delete.
  Node* current = head;
  Node* found = NULL;

  // While the current node is not null.
  while (current != NULL) {
    // If the key is less than the current node's key.
    if (key < current->key) {
      // Move the current node to the left.
      current = current->left;
    } else if (key > current->key) {
      // Move the current node to the right.
      current = current->right;
    } else {
      // Found the node to delete.
      found = current;
      break;
    }
  }

  // If the node to delete is not found.
  if (found == NULL) {
    // Key is not present.
    return;
  }

  // If the node to delete has only one child.
  if (found->left == NULL || found->right == NULL) {
    // Delete the node.
    delete found;
    head = NULL;
  }

  // If the node to delete has two children.
  if (found->left != NULL && found->right != NULL) {
    // Find the successor of the node to delete.
    Node* successor = found->right;

    // Move the successor's left child to the node to delete's left child.
    found->left = successor->left;

    // Move the successor's right child to the node to delete's right child.
    found->right = successor->right;

    // Delete the successor.
    delete successor;

    // Head is now the successor of the node to delete.
    head = found;
  }
}

// Prints the tree.
void print(Node* head) {
  if (head == NULL) {
    // Empty tree.
    cout << "Empty tree" << endl;
    return;
  }

  cout << "Root: " << head->key << endl;
  print(head->left);
  print(head->right);
}

int main() {
  Node* head = NULL;

  head = insert(head, 10);
  head = insert(head, 20);
  head = insert(head, 30);
  head = insert(head, 40);

  print(head);

  delete(head, 20);

  print(head);

  return 0;
}
