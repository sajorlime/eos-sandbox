
/*
    explore some buffering
*/

# include<cassert>
# include<chrono>
# include<csignal>
# include<cstdio>
# include <iomanip>
# include<iostream>
# include<thread>
# include<string>
# include<sstream>

using namespace std;

string digits(uint64_t x)
{
    auto y = x / 1000;
    if (y == 0) {
        stringstream sout;
        sout << setfill('0');
        sout << setw(3);
        sout << x;
        return sout.str();
    }
    x = x - 1000 * y;
    return digits(y) + ',' + digits(x);
}

string print_digits(uint64_t v)
{
    string s = digits(v);
    if (s[0] == '0') {
        if (s[1] == '0') {
            return digits(v).substr(2);
        }
        return digits(v).substr(1);
    }
    return digits(v);
}

FILE* _ifp;
FILE* _ofp; // out file

void sig_hanlder(int  sig)
{
    fclose(_ifp);
    fclose(_ofp);
    cout << "sig: " << sig << endl;
    exit(1);
}

bool mount_sdcard()
{
    return true;
}

constexpr size_t num_samps_per_msec = 20480;
# define NUMBER_OF_SAMPLES_IN_A_MSEC num_samps_per_msec

# ifndef READ_POWER
    // default to 1 giga byte
#   define READ_POWER 28
# endif
constexpr uint32_t max_read_space = 1 << READ_POWER;

char _buffer[max_read_space];


uint8_t* _raddr = (uint8_t*) _buffer;

uint64_t sdcard_open(const char* file)
{
    _ifp = fopen(file, "r");
    if (_ifp == nullptr) {
        return 0;
    }
    fseek(_ifp, 0, SEEK_END);
    uint64_t fsize = ftell(_ifp);
    fseek(_ifp, 0, SEEK_SET);
    cout << "Fsize:" << print_digits(fsize) << endl;
    return fsize;
}

uint32_t sdcard_read(void* dst, uint32_t bytes)
{
    size_t rbytes = fread(dst, 1, bytes, _ifp);
    //cout << _ifp << ':' << rbytes << '/' << bytes << endl;
    return rbytes;
}

void sdcard_seek0()
{
    fseek(_ifp, 0, SEEK_SET);
}

// global for the Data thread
uint32_t _read_cycle_id;
uint64_t _fsize; // actual size of the file.
uint64_t _roffset; // our current read offset into memory
uint64_t _doffset; // our current data offset into memory
uint64_t _bytes_read;  // bytes of the file we have read.
uint64_t _byte_position;  // byte position in the file.
uint64_t _bytes_consummed;  // bytes of the file we have eaten.
//const char* vectors_file = "AETestVectors/Cat3/cat3_test001_MayGSA.bin";
const char* vectors_file = "x.bin";

constexpr uint32_t access_rows = 640;
constexpr uint32_t access_uint = 8; // 8 bytes or 64-bits.
constexpr uint32_t access_elements = 8;
constexpr uint32_t access_row_size = access_elements * access_uint;
constexpr uint32_t data_elements = 5;
constexpr uint32_t data_row_size = data_elements * access_uint;
constexpr uint32_t address_space = access_rows * access_row_size;
constexpr uint32_t data_space = access_rows * data_row_size;

static_assert(num_samps_per_msec == 4 * data_space / 5);

// The data-space is times 2 for A and B samples.
constexpr uint32_t msec_row_step = 2 * num_samps_per_msec;


constexpr uint32_t one_sec_data = 1000 * msec_row_step;
# ifndef SECONDS_TO_READ
#   define SECONDS_TO_READ 1
# endif
constexpr uint32_t seconds_to_read = SECONDS_TO_READ;
constexpr uint32_t milliseconds_to_sleep = (1000 * SECONDS_TO_READ) / 4;
constexpr uint32_t data_to_read = seconds_to_read * one_sec_data;
// We only want multiples of 1 millisecond of data
constexpr uint32_t max_read_millisecs = max_read_space / seconds_to_read;
constexpr uint32_t available_read_space = max_read_millisecs * seconds_to_read;

uint32_t consume_data(uint64_t bytes)
{
    _doffset += bytes;
    _bytes_consummed += bytes;
    if (_doffset >= available_read_space) {
        cout << "CD roll:"
             << " do: " << print_digits(_doffset)
             << " ro: " << print_digits(_roffset)
             << " BP:" << print_digits(_byte_position) 
             << " BC:" << print_digits(_bytes_consummed) << '\n' << endl;
        _doffset = 0;
    }
    return sizeof(uint16_t) * _doffset;
}

bool _reading = false;

bool binit()
{
    _fsize = sdcard_open(vectors_file);
    // read up to one second to start.
    uint32_t read_size = data_to_read;
    if (read_size > _fsize) {
        cout << "trunc read size:" << _fsize << endl;
        read_size = _fsize;
    }
    _bytes_read = _byte_position = sdcard_read((void*) _raddr, read_size);
    _roffset = _byte_position;
    //printf("BR: 0x%08lx / 0x%08x\n", _byte_position, available_read_space);
    cout << "BR: " << print_digits(_byte_position)
                   << '/' << print_digits(available_read_space)
                   << " ms:" << _fsize / msec_row_step
                   << endl;
    _doffset = 0;
    _bytes_consummed = 0;
    _reading = true;
    // wake up every 1/4 period to try and read data.
    return true;
}

void rthread()
{
    cout << "RT start" << endl;

    int w = 0;
    while (true) {
        w = 0;
        // Because we allocate read millescs this will become equal, but
        // not go over.
        if (_byte_position >= _fsize) {
            cout << "DONE!:: "
                    << print_digits(_byte_position)
                    << '/' << print_digits(_fsize)
                    << " of " << print_digits(_bytes_read) << endl;
            //sdcard_seek0();
            //_byte_position = 0;
            _reading = false;
            break;
        }
        if (_roffset > _doffset) {
            uint8_t* raddr = _raddr + _roffset;
            uint32_t read_size = data_to_read;
            uint32_t limit = available_read_space - _roffset;
            if (read_size > limit)
                read_size = limit;
            uint32_t bytes = sdcard_read(raddr, read_size);
            w = 1;
            _byte_position += bytes;
            _bytes_read += bytes;
            _roffset += bytes;
            cout << "ro over: " << print_digits(_roffset) << " rs: " << _roffset / read_size << endl;
            if (_roffset >= available_read_space) {
                cout << "roll over avail space:" << print_digits(available_read_space)
                     << "BC @ ro:" << print_digits(_bytes_consummed) 
                     << " do:" << print_digits(_doffset) << endl;
                _roffset = 0;
            }
        }
        else { // the data offset has rolled
            // we can read if data-to-read bytes are available
            if (_doffset > 2 * data_to_read)
            {
                if (_roffset < (_doffset - data_to_read)) {
                    uint8_t* raddr = _raddr + _roffset;
                    uint32_t read_size = data_to_read;
                    uint32_t limit = available_read_space - _roffset;
                    if (read_size > limit)
                        read_size = limit;
                    uint32_t bytes = sdcard_read(raddr, read_size);
                    w = 2;
                    _byte_position += bytes;
                    _bytes_read += bytes;
                    _roffset += bytes;
                    cout << "ro under: " << print_digits(_roffset) << " rs: " << _roffset / read_size << endl;
                    if (_roffset >= available_read_space) {
                        cout << "\nroll UNDER avail space:" << print_digits(available_read_space)
                             << "BC @ ro:" << print_digits(_bytes_consummed)
                             << " do:" << print_digits(_doffset) << endl;
                        _roffset = 0;
                    }
                }
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds_to_sleep));
        if (w)
            cout << "RT br(" << w << ')'
                    << ":" << print_digits(_byte_position)
                    //<< '/'
                    //<< print_digits(_fsize)
                    << " @BC " << print_digits(_bytes_consummed)
                    << " ro " << print_digits(_roffset)
                    << " do " << print_digits(_doffset)
                    << " of " << print_digits(_bytes_read) << endl;
    }
    cout << "RT end" << endl;
}

uint32_t steps = 0;

void dma_mm2s(uint64_t* addr, uint32_t step)
{
# if 0
    for (size_t cnt = step / sizeof(uint64_t);  cnt > 0; cnt--)
    {
        assert(*addr == (uint64_t) addr);
    }
# else
    steps++;
    //cout << "fw:" << steps << ':' << step << '\r';
    uint8_t* baddr = (uint8_t*) addr;
    fwrite(baddr, 1, step, _ofp);
# endif
}

uint16_t _ddr_offset = 0;

/* Update the FIFO with 1ms worth of data
*/
void update()
{
    uint16_t *ddr_addr = (uint16_t *) _buffer;

    // Update the ddr_addr with current offset in samples
    ddr_addr += _ddr_offset;

# if 0
    // write values we can varify?
    uint64_t* wlimit = (uint64_t*) (ddr_addr + num_samps_per_msec);
    for (uint64_t* waddr = (uint64_t*) ddr_addr; waddr < wlimit; waddr++) {
        *waddr = (uint64_t) waddr;
    }
# endif

    // samples in 16-bits, so we need 2 times samples
    dma_mm2s((uint64_t*) ddr_addr, msec_row_step);

    // Update the current DdrOffset in uint16_t
    _ddr_offset = consume_data(msec_row_step);
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
}

int main(int argc, char** argv)
{
    _ofp = fopen("y.bin", "w");
    uint32_t epochs = 2 * max_read_millisecs;
    if (argc  > 1) {
        epochs = atoi(argv[1]);
        cout << "epocks set to: " << epochs << endl;
    }
    cout << "Es:" << print_digits(epochs) << endl;
    // prime the pump
    binit();
    std::thread rt_handle(rthread);
    while (_bytes_consummed < _byte_position) {
        update();
    }
    rt_handle.join();
    cout << "steps: " << steps << endl;
    fclose(_ofp);
    fclose(_ifp);

}


