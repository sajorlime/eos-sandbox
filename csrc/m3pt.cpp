
/*
 * File: m3pt.cpp
 *
 * Implementation of a 3 sample median filter.
 *
 */


# include <algorithm>
# include <cassert>
# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <fstream>
# include <iostream>
# include <random>
# include <string>
# include <stdio.h>
# include <time.h>
using namespace std;

typedef uint32_t mftype;
typedef int mfindex;

// Generate some random noise for input, this is a hack for testing
// not a stotasticly meaningful value
// Args:
//  ratio: the max noise level ratio
// Return:
//   random y * random * ration,  where random is [-0.5, 0.5]
mftype some_noise(mftype y, float ratio=0.1)
{
    float f = static_cast<float>(rand()) / RAND_MAX;
    float fr = f - 0.5;
    return (mftype) (y * fr * ratio);
}

# define MF_SIZE 3
const int mfsize = MF_SIZE;
// store the last mfsize values, in rotating buffer
const mfindex lsize = mfsize + 1;
mftype m_last[lsize];
mfindex ml_pos;
// note wie fille both mf_last and mf_sort with a single value to start!
//

mftype m3pt(mftype t0, mftype t1, mftype t2)
{
    cout << " " << t0;
    cout << ", " << t1;
    cout << ", " << t2;
    cout << endl;
    bool b0_le_1 = t0 <= t1;
    bool b1_le_2 = t1 <= t2;
    if (b0_le_1 & b1_le_2)
    {
        return t1;
    }
    if (b0_le_1 & b1_le_2)
    {
        return t2;
    }
    if (b0_le_1)
    {
        return t0;
    }
    bool b0_le_2 = t0 <= t2;
    if (b0_le_2 & b1_le_2)
    {
        return t0;
    }
    if (b1_le_2)
    {
        return t2;
    }
    return t1;
}

mftype median(mftype ynew)
{
    m_last[ml_pos++] = ynew;
    mftype medium;
    ml_pos &= mfsize;
    medium = m3pt(m_last[(ml_pos + 1) & mfsize],
                  m_last[(ml_pos + 2) & mfsize],
                  ynew);
    return medium;
}

void minit(mftype start)
{
    ml_pos = mfsize;
    for (mfindex tpos = 0; tpos < lsize; tpos++)
        m_last[tpos] = start;
# if 0
    for (mfindex tpos = 0; tpos < lsize; tpos++)
        cout << m_last[tpos] << ", ";
    cout << endl;
# endif
}

bool test_median(const char* fname, mftype y0, mftype (*next)(mftype))
{
    minit(y0);
    int i;
    std::ofstream csv(fname); // create the CSV file output.
    csv << "   N,      y0,      xxx,      ml0,      ml1,      ml2,      mfo"
        << std::endl;
    for (i = 0; i < 4 * mfsize; i++)
    {
        mftype m = median(y0);
        //mftype y1 = next(y0);
        csv << setw(4) << i << ", ";
        csv << setw(8) << y0 << ", ";
        for (mfindex li = 0; li < lsize; li++)
        {
            mfindex ji = (ml_pos + li) & mfsize;
            csv << setw(8) << m_last[ji] << ", ";
        }
        csv << setw(8) << m;
        csv << std::endl;
        y0 = next(y0);
    }
    return true;   
}
# define TEST_FRONT 0
# define TEST_BACK 0
# define TEST_UPDOWN 0
# define TEST_AROUND 1
# define TEST_NEG 0

mftype infront2(mftype y)
{
    return y - 2;
}

mftype infront(mftype y)
{
    return y - 1;
}

mftype inback(mftype y)
{
    return y + 1;
}

bool up = false;
mftype inc = 0;
mftype inupdown(mftype y)
{
    up = !up;
    inc += 2;
    if (up)
    {
        return y + inc;
    }
    return y - inc;
}

mftype inaround(mftype y)
{
    mftype n = some_noise(y);
    return y + n;
}

int main()
{
# if TEST_FRONT
    test_median("tfront1.csv", 100000, infront);
    test_median("tfront2.csv", 100000, infront2);
# endif
# if TEST_BACK
    test_median("tback.csv", 100000, inback);
# endif
# if TEST_UPDOWN
    test_median("tud.csv", 100000, inupdown);
# endif
# if TEST_AROUND
    test_median("taround.csv", 100000, inaround);
# endif

    return 0;
    //  run m.csv 60 0.01 0.01 100
}

