#include <iostream>
using namespace std;
int main() 
{
# if 0
    cout << "Hello, World! ";
    string xs;
    cin >> xs;
    cout << xs << endl;
# endif
    float rate;
    cout << "enter pay rate: ";
    cin >> rate;
    cout << "rate is " << rate << endl;
    float hours;
    cout << "enter hours worked: ";
    cin >> hours;
    float pay = rate * hours;
    if (hours > 40.0) {
        pay += 0.5 * (hours - 40.0) * rate;
    }
    cout << "pay is:" << pay << endl;

    return 0;
}
