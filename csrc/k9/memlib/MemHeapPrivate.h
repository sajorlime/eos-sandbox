
/*
** Copyright (C) 1994, 1995,
** ICTV Inc. 14600 Winchester Blvd.
** Los Gatos, CA 95030
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: ictv interactive television memlib
** 
** File: MemHeapPrivate.h - 'Protected' info for Classes which are 'friends'
**     $Id: MemHeapPrivate.h,v 1.10 1995/11/04 02:03:28 wul Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/memlib/MemHeapPrivate.h,v $
**
** Author(s):
**    Aylwin Stewart
**
** Purpose:
**     This file was created primarily because their needs to be a
**     close relationship between the MemHandles, and the MemHeap.
**     It's primary purpose was to encapsulate MemHandle related 
**     function calls.  The rest was put here primarily to facilitate
**     the ease of writing the test code.
** 
** Types/Classes:
**     MemHeap - A memory heap object
**     CellPtr - A bookkeeping object used internally by MemHeaps
** 
** Functions:
**     appendLRUList  - append a Resource Handle to the LRU list
**     deleteLRUList  - remove a Resource Handle from the LRU list
**     incrementLockedResourceCount - (says it all)
**     decrementLockedResourceCount - (says it all)
** 
** 
** 
*/

#ifndef  __MEMHEAPPRIVATE_H__
#define  __MEMHEAPPRIVATE_H__

#include "ictvdebug.h"
#include "ProcessLock.h"

#include "MemHeap.h"
#include "MemHandle.h"
#ifdef ICTV_MEM_DEBUG
    #include "timestamp.h"
#endif

/*****                     M A C R O S                     *****/

# define MEM_ASSERT(params) LOCAL_MSG (params, DBG_MEM_MASKINDEX, DBG_ASSERT)
# define MEM_WARN(params) LOCAL_MSG (params, DBG_MEM_MASKINDEX, DBG_WARN)
# define MEM_MISC(params) LOCAL_MSG (params, DBG_MEM_MASKINDEX, DBG_MISC)
# define MEM_TRACE(params) LOCAL_MSG (params, DBG_MEM_MASKINDEX, DBG_TRACE)

/* 
 * CELL_PTR_BLOCK_SZ: The number of CellPtrs in a MemHeap.
 */
#define CELL_PTR_BLOCK_SZ            (1<<11)

/* 
 * OBJ_SZ_INCREMENT: The size increments used when increasing the object sizes
 * for the FreeLists.
 */
#define OBJ_SZ_INCREMENT             (16)

/* 
 * MAKE_MULTIPLE_OF_EXP_OF_2: Round an integer var. upto the nearest
 * multiple of a factor which must be an exponent of two.  Modifies the
 * initial parameter, 'var' passed.
 */
#define MAKE_MULTIPLE_OF_EXP_OF_2( var, expOf2 ) \
                  (var) = ((var) & ((expOf2) - 1)) \
                          ? ((var) & ~((expOf2) - 1)) + (expOf2) \
			  : (var)
           
                

#ifndef NOINLINE

#define noteAllocation(heap,count) \
	heap->freeBytes -= count;  \
    	heap->busyBytes += count 

#define noteDeallocation(heap,count) \
    heap->freeBytes += count;        \
    heap->busyBytes -= count 

#endif /* NOINLINE */

/*****                      E N U M S                      *****/

/* 
 * CellPtrState: The states a CellPtr can be in.
 */
typedef enum {
    Unused = 0,
    Free,
    Busy,
    Dead
} CellPtrState;


/*****            D A T A   S T R U C T U R E S            *****/


/* 
 * FreeListCounts: a max and available count for a FreeList.
 */
typedef struct {
    u_int16 max;
    u_int16 free;
} FreeListTally;


/* 
 * CellPtr: An object which points to a chunk of memory, referred to as a cell.
 * All allocated cells start w/ a back pointer to a CellPtr, as well.  
 */
typedef struct {
    u_int32 state :2;  /* Equal to one of CellPtrState */
    u_int32 size :30;  /* Cell's size not including back pointer */
    u_int32 pid;       /* The owning process of the memory allocated */
    char  *mem;        /* The address returned to the application */
#ifdef ICTV_MEM_DEBUG
    char *filename;	/* File mem_malloc was called from. */
    u_int32 sizeReq;	/* Size requested. */
    Timestamp_t time;	/* When it was requested. */
    u_int16 lineNum;	/* Line number of call in... */
#endif
} CellPtr;



/* 
 * MemHeap: The object which is used to actually manage memory, (i.e. from
 * whom memory is allocated and deallocated.)  The function pointers allow
 * the MemHeap to be set up to use alloc./dealloc. functions which do, or
 * don't, use a Lock for mutex'ing shared access to memory.  
 *     lru -- least recently used
 */
typedef struct MemHeap {
    u_int32 lockedResCnt;      /* The number of resources locked into memory  */
    CellPtr cells[CELL_PTR_BLOCK_SZ];  /* The CellPtrs for memory cells       */
    CellPtr *startCellPtr;     /* The CellPtr where searching starts from     */
    CellPtr *topCellPtr;       /* The last CellPtr that is in the Free state  */
    MemHandle_p lruFirst;      /* The first item to purge in resource list    */
    MemHandle_p lruLast;       /* Most recently used item in resource list    */
    u_int32 maxObjSz;          /* The largest free list object size           */
    u_int32 maxFreeListSz;     /* The max items in a free list                */
    u_int32 minFreeListSz;     /* The min items in a free list                */
    FreeListTally *freeListCounts;  /* Array of free list tallies */
    u_int32 *freeListLocs;     /* Array of free list start addresses as ints  */
    void ***freeListRoots;     /* Array of free list start addresses          */
    void *endOfFreeLists;      /* Pointer to last memory word in Free Lists   */
    u_int32 freeBytes;         /* The number of free bytes w/o back pointers  */
    u_int32 busyBytes;         /* The number of used bytes with back pointers */
    void *endOfHeap;           /* Pointer to the last allocatable memory word */
    ProcessLock  memLock;      /* The lock used for mutex'ing memory access   */
    CellPtr postCommit ;       /* Used to reset a shared heap to what it was post commit. */
    u_int32 committedBytes;    /* The number of committed bytes back pointer bytes are lost. */
} MemHeap;
typedef MemHeap *MemHeap_p;


/*****      F U N C T I O N   D E C L A R A T I O N S      *****/

void setMemHeapErrno( MemErrCode errorCode );
int32 getMemHeapErrno( void );

int32 appendLRUList( MemHeapId heapId, MemHandle_p handle );
int32 deleteLRUList( MemHandle_p handle );
int32 incrementLockedResCnt( MemHeapId heapId );
int32 decrementLockedResCnt( MemHeapId heapId );



#endif

