#ifndef _OS9000

#ifndef __MEM_MALLOC_H__

#define __MEM_MALLOC_H_
#include <stdlib.h>

#	ifndef mem_malloc
#		define mem_malloc(dummy,sz) malloc(sz)
#	endif

#	ifndef mem_realloc
#		define mem_realloc(dummy,addr,sz) realloc(addr,sz)
#	endif

#	ifndef mem_calloc
#		define mem_calloc(dummy,num,sz) calloc( num, sz)
#	endif

#	ifndef mem_free
#		define mem_free(dummy,addr) free(addr)
#	endif


#endif
#else /* OS9000 defined */
#	include "MemHeap.h"
#endif

