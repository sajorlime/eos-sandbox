
/*
** Copyright (C) 1994, 1995,
** ICTV Inc. 14600 Winchester Blvd.
** Los Gatos, CA 95030
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Permission to use and distribute given to Lapel Software 1997.
** 
** Project: ictv interactive television MMC Memory Manager Library
** 
** File: MemHandle.c - A handle which provides a level of indirection **       assets, to allow for memory management.
**
** Author(s):
**    Aylwin Stewart
**
** Purpose:
**     Memory handles are needed on the MMC to allow for memory
**     management by the MemHeap class. Handles, by adding another
**     level of indirection allow assets resident in memory to be
**     flushed and reloaded while still providing their consumers with
**     a reference to the asset. The MemHeap class has been implemented
**     to use a Least Recently Used algorithm to free memory resident
**     assets which have not been locked into memory if it can not
**     satisfy a memory request.
** 
** Types/Classes:
**
** Functions:
**     create     - create a memory handle for an asset
**     createFromHeap - create a memory handle for an asset from
**                      specific heap
**     destroy    - destroy a memory handle flushing it's asset
**                  from memory if needed
**
**     fetch      - asynchronously load a memory handle asset
**     get        - get the pointer to memory for an asset
**     flush      - remove an asset from memory 
**
**     isResident - a predicate on wether a resource is loaded
**     lock       - lock an asset into memory, and asynchronously
**                  retrieve it if neccessary
**     unlock     - indicate that a resource may be flushed if desired
**
** Assumptions:
**     No more than 65535 locks will be required for a MemHandle at
**     one time.
** 
** Limitations:
**     �if-required�
** 
** Notes:
**     �if-required�
** 
** References:
**     See FrameMaker document MemHandleRequirements.frame.
** 
** 
*/

#include <string.h>

#include "ipc.h"
#include "ipcuser.h"
#include "rpcServer.h"
#include "ProcessLock.h"
#include "mal.h"
#include "MemHeap.h"
#include "MemHeapPrivate.h"

#include "MemHandle.h"


/*****                     M A C R O S                     *****/


/*****                      E N U M S                      *****/


/*****            D A T A   S T R U C T U R E S            *****/


/*****                    G L O B A L S                    *****/                  

static MailHandle MalBox;

static int32 MemHandlErrno;

/*****      F U N C T I O N   D E C L A R A T I O N S      *****/


/*****       F U N C T I O N   D E F I N I T I O N S       *****/


/* 
 * Create: Wrapper around hndl_createFromHeap() using the default heap. 
 */
MemHandle_p
hndl_create( MemHandle_p handle,
             MemHandleType type, MemHandleSrc source, 
	     const char *assetStr, u_int32 size )
{
    return hndl_createFromHeap( handle, DEFAULT_HEAP_ID,
                                type, source, assetStr, size );
}


/* 
 * CreateFromHeap: Create a new MemHandle_p.  This is also used to init
 * a  'subclass' ResHandle's basic structure.  Wether or not memory was
 * passed to hndl_create() is determined by the sign of the resCnt mem-
 * ber.  If it is negative, than the Resource Manager allocated the
 * memory, and will free it when the resource is destroyed.
 */
MemHandle_p
hndl_createFromHeap( MemHandle_p handle, MemHeapId heapId,
                     MemHandleType type, MemHandleSrc source,
                     const char* assetStr, u_int32 size )
{
    
    if ( ! handle ) {
        handle = mem_malloc( heapId, sizeof( MemHandle ) );
	if ( ! handle ) {
	    MemHandlErrno = EMEM_INIT_MEM_FAILURE;
            MEM_WARN(
	        ("hndl_create: Failed to alloc memory for handle.\n"));
	    return NULL;
	}
	
    }
    
    handle->obj.path = mem_malloc( heapId, strlen( assetStr ) + 1 );
    if ( ! handle->obj.path ) {
        MEM_WARN( ("hndl_create: Failed to alloc memory for path.\n"));
        
        MemHandlErrno = EMEM_INIT_MEM_FAILURE;
        return NULL;
    }
    strcpy( handle->obj.path, assetStr );
    
    handle->lock = NULL;
    if ( ! (handle->lock = lock_create( handle->lock, 1 )) ) {
        MEM_WARN( ("hndl_create: Failed to create ProcessLock.\n"));
        
        MemHandlErrno = EMEM_INIT_MEM_FAILURE;
        return NULL;
    }
    
    if ( type <= StartMemHandleTypes || type > HandleTypeCnt ) {
        MEM_WARN( ("hndl_create: bad handle type %d.\n", type));
        
        MemHandlErrno = EMEM_BAD_HANDLE_TYPE;
        return NULL;
    }
    
    handle->type = type;
    handle->source = source;
    handle->size = size;
    handle->heap = heapId;
    handle->asset = NULL;
    handle->prev  = NULL;
    handle->next  = NULL;
    handle->lockCnt = 0;
            
    MemHandlErrno = EMEM_NOERROR;
    return handle;
} 


/* 
 * Destroy: Flush and deactivate a Resource handle, possibly
 * freeing it as well. 
 * 
 * Returns MAL_SUCCESS if successful, or MAL_FAILURE
 */
int
hndl_destroy( MemHandle_p handle )
{
    int32 retVal;
    MemHeapId heapId;
    ProcessLock *theLock;
    
    if ( ! handle || ! handle->type ) {
        MemHandlErrno = EMEM_BAD_HANDLE;
	return MEM_FAILURE;
    }

    heapId = handle->heap;
    theLock = handle->lock;

    if ( handle->lockCnt ) {
        MEM_ASSERT( ("hndl_destroy: Attempt to destroy locke handle "
                    "0x%p.\n", handle));
	return MEM_FAILURE;
    }	

    lock_acquire( theLock );
    
    if ( handle->asset ) {
        deleteLRUList( handle );
        mem_free( handle->heap, handle->asset );
    }
    mem_free( handle->heap, handle->obj.path );
    mem_free( handle->heap, handle );

    lock_release( theLock );
    mem_free( heapId, theLock );
    
    MemHandlErrno = EMEM_NOERROR;
    return MEM_SUCCESS;
}


/* 
 * InternalFetch: Asynchronously retrieve a resource
 */
static void 
internalFetch( MemHandle_p handle, Bool reply )
{
	if (!handle || !handle->type)
	{
		MemHandlErrno = EMEM_BAD_HANDLE;
		return;
	}

	appendLRUList( handle->heap, handle );
	if ( handle->asset )
		return;

	switch ( handle->source )
	{
	case User:
		{
			if (RPC_SEND (IPC_MAL_USER_LOAD, &handle,
				      mal_getMalBox ()))
			{
				MEM_WARN (("hndl_fetch: MalBox send failure\n"));
			}
			break;
		}
	case CMX:
		{
			if (reply)
			{
				if
				(
					RPC_SEND
					(
						IPC_MAL_CMX_LOAD_REPLY, &handle, mal_getMalBox ()
					)
				)
				{
					MEM_WARN (("hndl_fetch: MalBox send failure\n"));
				}
			}
			else if (RPC_SEND (IPC_MAL_CMX_LOAD, &handle, mal_getMalBox ()))
			{
				MEM_WARN (("hndl_fetch: MalBox send failure\n"));
			}
			break;
		}
	default:
		{
			MEM_WARN (("hndl_fetch: bad MemHandleSrc: %d ", handle->source));
			MEM_WARN (("for MemHandle 0x%p\n", handle));
			break;
		}
	}

	return;
}

/* 
 * Fetch: Asynchronously retrieve a resource w/o a reply
 */
void
hndl_fetch( MemHandle_p handle )
{
    internalFetch( handle, NO );
    return;
}

/* 
 * FetchWithReply: Asynchronously retrieve a resource with a reply
 * to the requestor using the ipcuser type 'IPC_MAL_CMX_LOAD_REPLY'.
 */
void
hndl_fetchWithReply( MemHandle_p handle )
{
    internalFetch( handle, YES );
    return;
}

/* 
 * Store: Asynchronously retrieve a resource
 */
void 
hndl_store (MemHandle_p handle)
{
	if (!handle || !handle->type)
	{
		MemHandlErrno = EMEM_BAD_HANDLE;
		return;
	}

	switch (handle->source)
	{
	case User:
		if (RPC_SEND (IPC_MAL_USER_SAVE, &handle, mal_getMalBox ()))
		{
			MEM_WARN (("hndl_store: MalBox send failure\n"));
		}
		break;
	case CMX:
		return;
	default:
		MEM_WARN (("hndl_store: bad MemHandleSrc: %d ", handle->source));
		MEM_WARN (("for MemHandle 0x%p\n", handle));
		break;
	}
	return;
}


/* 
 * Get: derefrence a handle to get its resource
 */
void*
hndl_get (MemHandle_p handle)
{
	int32 returnVal, returnSize, myRetVal;
	MemHandle_p tmp = handle;

	if (!handle || !handle->type)
	{
		MemHandlErrno = EMEM_BAD_HANDLE;
		return NULL;
	}

	if (handle->asset)
		return handle->asset;

	switch (handle->source)
	{
	case User:
		RPC_READ_SYNC
		(
			IPC_MAL_USER_LOAD, &handle, &returnVal, mal_getMalBox ()
		);
		if (returnVal)
		{
			MEM_WARN (("hndl_get: MalBox send failure (%d)\n", returnVal));
		}
		break;
	case CMX:
		RPC_READ_SYNC (IPC_MAL_CMX_LOAD_REPLY, &handle, &returnVal,
				   mal_getMalBox ());
		if (returnVal)
		{
			MEM_WARN (("hndl_get: MalBox send failure (%d)\n", returnVal));
		}
		break;
	default:
		MEM_WARN (("hndl_get: bad MemHandleSrc: %d ", handle->source));
		MEM_WARN (("for MemHandle 0x%p\n", handle));

		MemHandlErrno = EMEM_BAD_HANDLE;
		return NULL;
	}

	if (returnVal != 0)
	{
		MemHandlErrno = returnVal;
		return NULL;
	}

	return handle->asset;
}


/* 
 * Flush: flush a resource from memory
 */
int32
hndl_flush( MemHandle_p handle )
{
    int32 retVal;
    
    if ( ! handle || ! handle->type )
        return EMEM_BAD_HANDLE;
    
    lock_acquire( handle->lock );
    
    if ( handle->lockCnt ) {
        lock_release( handle->lock );
        return EMEM_LOCKED_HANDLE;
    }
    
    if ( handle->asset ) {
        deleteLRUList( handle );
        mem_free( handle->heap, handle->asset );
	handle->asset = NULL;
	
	lock_release( handle->lock );
    }else 
        lock_release( handle->lock );
    
    return MEM_SUCCESS;
}


/* 
 * InMemory: return 'YES' if a resource is actually resident in 
 * memory, otherwise return 'NO'.  This call should be made after
 * acquiring the ResHandles resLock.
 */
Bool
hndl_isResident( MemHandle_p handle )
{
    if ( ! handle || ! handle->type ){
        MemHandlErrno = EMEM_BAD_HANDLE;
        return NO;
    }
    
    return handle->asset ?  YES : NO;
}


/* 
 * Lock: increment a handle's lock count and retrieve it if neccessary.
 */
void
hndl_lock( MemHandle_p handle )
{
    if ( ! handle || ! handle->type ) {
        MemHandlErrno = EMEM_BAD_HANDLE;
	return;
    }
    
    if ( handle->lockCnt == 0 )
        deleteLRUList( handle );

    incrementLockedResCnt( handle->heap );
    
    lock_acquire( handle->lock );
    handle->lockCnt++;
    lock_release( handle->lock );
    
    if ( ! handle->asset )
        hndl_fetch( handle );
    
    return;
}


/* 
 * Unlock: decrement a handle's lock count, possibly causing it to
 * be placed in a MemHeap's LRU list.
 */
void
hndl_unlock( MemHandle_p handle )
{
        
    if ( ! handle || ! handle->type ) {
        MemHandlErrno = EMEM_BAD_HANDLE;
	return;
    }
    
    lock_acquire( handle->lock );
    
    if ( handle->lockCnt ) 
        handle->lockCnt--;
    
    if ( handle->lockCnt <= 0 ) {
	handle->lockCnt = 0;
	appendLRUList( handle->heap, handle );
    }   

    lock_release( handle->lock );

    decrementLockedResCnt( handle->heap );

    MemHandlErrno = EMEM_NOERROR;    
    return;
}


/* 
 * GetMemHandleErrno:  accessor to the value of MemHandleErrno.
 */
int32
getMemHandlErrno( void )
{
    return MemHandlErrno;
}

