
/*
** Copyright (C) 1994, 1995,
** ICTV Inc. 14600 Winchester Blvd.
** Los Gatos, CA 95030
**
** Project: ictv interactive television MMC Memory Manager Library
** 
** File: MemHandle.h - A handle which provides a level of indirection **       assets, to allow for memory management.
**     $Id: MemHandle.h,v 1.4 1995/06/25 22:28:06 wul Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/memlib/MemHandle.h,v $
**
** Author(s):
**    Aylwin Stewart
**
** Purpose:
**     Memory handles are needed on the MMC to allow for memory
**     management by the MemHeap class. Handles, by adding another
**     level of indirection allow assets resident in memory to be
**     flushed and reloaded while still providing their consumers with
**     a reference to the asset. The MemHeap class has been implemented
**     to use a Least Recently Used algorithm to free memory resident
**     assets which have not been locked into memory if it can not
**     satisfy a memory request.
** 
** Types/Classes:
**     MemHandle   - an "Abstract Superclass" used to create domain
**                   specific memory handles which will be managed
**                   by a MemHeap.
** 
** Functions:
**     create     - create a memory handle for an asset
**     createFromHeap - create a memory handle for an asset from
**                      specific heap
**     destroy    - destroy a memory handle, flushing it's asset
**                  from memory if needed
**
**     fetch      - asynchronously load a memory handle asset
**     get        - get the pointer to memory for an asset
**     flush      - remove an asset from memory 
**
**     isResident - a predicate on wether an asset is loaded
**     lock       - lock an asset into memory, (asynchronously
**                  retrieve it if neccessary)
**     unlock     - indicate that an asset may be flushed if desired
**
** 
** Assumptions:
**     No more than 65535 locks will be required for a MemHandle at
**     one time.
**     A user will not allocate memory from the same MemHeap for
**     which it holds a lock on MemHandle from that MemHeap.
** 
** Limitations:
**     �if-required�
** 
** Notes:
**     �if-required�
** 
** References:
**     See FrameMaker document MemHandleRequirements.frame.
** 
*/

#ifndef __MEMHANDLE_H__
#define __MEMHANDLE_H__

#include "ictvcommon.h"
#include "MemHeap.h"
#include "ProcessLock.h"


/*****                     M A C R O S                     *****/

/* 
 * HNDL_ASSET_SZ: get the size of the asset for a MemHandle
 */
#define HNDL_ASSET_SZ( memHandlePtr ) \
                       (memHandlePtr)->size


/*****                      E N U M S                      *****/


/* 
 * MemHandleType: the types of MemHandles.  Each new MemHandle
 * subclass must add a type to this enumeration.
 */
typedef enum {
    StartMemHandleTypes,
    Font,
    Image,
    IPCode,
    AudioBlip,
    HandleTypeCnt = AudioBlip
} MemHandleType;

/* 
 * MemHandleSrc: the source for the MemHandles assets
 */    
typedef enum {
    User,
    CMX
} MemHandleSrc;


/*****            D A T A   S T R U C T U R E S            *****/

/* 
 * MemHandle_p: A pointer to a MemHandle structure.
 */
typedef struct MemHandle *MemHandle_p;


/* 
 * FetchStoreFunc: A pointer to a function which is called
 * to load an asset, or save a modifed asset to secondary storage. 
 * These functions must be compiled into MAL's function table so
 * that it can find and call the correct function based on your
 * MemHandle type and wether or not you are fetching/storing.
 * See mal.c for more detail on the use of LoadStoreFuncs for loading
 * an object into memory, or saving them to secondary storage.
 *
   ( Since MAL executes in a different code space a MemHandle user
     really needn't know about LoadStoreFunc's although mal.c will
     have to be modified for use with any real MemHandle. )
typedef int32 (*FetchStoreFunc)(MemHandle_p);
 */


/* 
 * MemHandle:  A handle used to maintain state and recovery infor-
 * mation for an asset.  It is meant to be 'inherited' from by
 * users and made the first member of any specific MemHandle they
 * develop.  (Sorry, but we are coding in 'C'.)
 */
typedef struct MemHandle {
    MemHandleType type;                 /* The MemHandle 'subclass' */
    MemHandleSrc  source;               /* The assets source */
    union {
        char *path;                     /* Path to file on server */
	char *name;                     /* String identifier      */
    } obj;
    u_int16 lockCnt;                    /* How many lockeres  */
    u_int32 size;                       /* Asset size in bytes   */
    MemHeapId heap;                     /* The heap used for memory */
    void *asset;                        /* Ptr to asset in memory */
    MemHandle_p prev;                   /* Previous handle (LRU)    */
    MemHandle_p next;                   /* Next handle     (LRU)    */
    ProcessLock *lock;                  /* Asset retrieval mutex */
} MemHandle; 


/* 
 * HndlMsg: Requests to MAL to load a MemHandle will use this structure
 * in the IPC message buffer so that the reply message has a reference
 * to the MemHandle for whom the request was made.  MemHandles use an
 * RPC semantic that is completely unaware of this structure however.
 * It is declared here, as this is the header that users will be 
 * including, as opposed to mal.h.
 */
typedef struct _hndlMsg {
    int returnVal;
    MemHandle_p handle;
}hndlMsg;


/*****      F U N C T I O N   D E C L A R A T I O N S      *****/

/* 
 * Create: Create a MemHandle.  Fetch and get asset functions must be
 * passed as part of handle creation.  Save, flush, and free functions
 * are optional.
 *    handle       - a pointer to memory for a MemHandle, or NULL
 *                   If NULL, memory will be allocated. 
 *    type         - the MemHandle 'subclass'
 *    source       - the assets fetch/store source 
 *    assetString  - the full path to the asset on the server,
 *                   or an asset name
 *    size         - the size of the actual asset in bytes
 * 
 *    Returns: a MemHandle_p, or NULL,      
 */
MemHandle_p
hndl_create( MemHandle_p handle,
             MemHandleType type, MemHandleSrc source, 
	     const char *assetStr, u_int32 size );

 
/* 
 * CreateFromHeap: hndl_create() with the heap to use for memory
 * allocation specified by the caller as well.
 *    handle      - a pointer to memory for a MemHandle, or NULL.
 *                  If NULL, memory will be allocated for a basic
 *                  MemHandle only. 
 *    heap        - the MemHeap from which memory will be allocated
 *    type        - the MemHandle 'subclass'
 *    source       - the assets fetch/store source 
 *    assetString - the full path to the asset on the server,
 *                  or an asset name
 *    size      - the size of the actual asset in bytes
 *
 *    Returns: a MemHandle_p or NULL
 */
MemHandle_p
hndl_createFromHeap( MemHandle_p handle, MemHeapId heapId,
                     MemHandleType type, MemHandleSrc source,
                     const char* assetStr, u_int32 size );


/*
 * Destroy:  Destroy a MemHandle and free the memory used by its asset.   
 *    handle    - a pointer to an MemHandle
 * 
 * Returns MAL_SUCCESS if successful, or MAL_FAILURE
 */ 
int
hndl_destroy( MemHandle_p handle );


/* 
 * Fetch: This function makes an asynchronous 'RPC' call to MAL
 * for asset retrieval, and does not reply for notification.
 *    handle    - a pointer to an MemHandle
 */
void
hndl_fetch( MemHandle_p handle );
 
/* 
 * Fetch: This function makes an asynchronous 'RPC' call to MAL
 * for asset retrieval, and replies for notification using the
 * ipcuser type 'IPC_MAL_CMX_LOAD_REPLY'.
 *    handle    - a pointer to an MemHandle
 * 
 */
void
hndl_fetchWithReply( MemHandle_p handle );
 
/* 
 * Store: This function makes an asynchronous 'RPC' call to MAL
 * to save a modified or newly created asset.
 *    handle    - a pointer to an MemHandle
 * 
 */
void
hndl_store( MemHandle_p handle );
 
 
/* 
 * Get: This function makes a synchronous 'RPC' call to MAL
 * for asset retrieval.   Sets MemHandleErrno to the value
 * returned by the fetchAssetFunc.
 *    handle    - a pointer to an MemHandle
 * 
 * Returns: A pointer to the actual asset, or NULL.
 */
void *
hndl_get( MemHandle_p handle );
 
 
/* 
 * Flush: This function frees the memory used by an asset..
 *    handle    - a pointer to an MemHandle
 *
 * Returns: MEM__LOCKED, or a user supplied error code via the
 * flushAssetFunc.
 */
int32
hndl_flush( MemHandle_p handle );
 

/* 
 * IsResident: Determine if the asset is actually in memory. 
 * Primarly useful after the lock() call which may need to retrieve
 * the asset as well.  This call should be made only after acquiring
 * the MemHandle's lock.
 *    handle    - a pointer to an MemHandle
 * 
 * Returns: YES, if the asset is in memory, otherwise returns NO.
 */
Bool
hndl_isResident( MemHandle_p handle );


/* 
 * Lock: Wire an asset into memory, so that it cannot be flushed.
 * This is the only means to gurantee that an asset will stay in 
 * memory.  Use of an asset should always have parenthetical lock(),
 * and unlock() calls around it.  It will retrieve the asset if it
 * is not in memory via an RPC call to MAL.
 *    handle    - a pointer to an MemHandle
 *
 */
void
hndl_lock( MemHandle_p handle );


/* 
 * Unlock: Remove a lock on an asset so that it may be flushed from
 * memory if neccessary.  This call is synchronous.
 */
void
hndl_unlock( MemHandle_p handle );


/* 
 * GetMemHandleErrno:  accessor to the value of MemHandleErrno.
 */
int32
getMemHandlErrno( void );

#endif    /* __MEMHANDLE_H__ */
