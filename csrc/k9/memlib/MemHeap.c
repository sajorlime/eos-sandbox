
/*
** Copyright (C) 1994, 1995,
** ICTV Inc. 14600 Winchester Blvd.
** Los Gatos, CA 95030
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: ictv interactive television Memory Manager Library
** 
** File: MemHeap.c - A memory heap with a 'C' like malloc/free interface
**     $Id: MemHeap.c,v 1.16 1995/11/04 02:03:26 wul Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/memlib/MemHeap.c,v $
**
** Author(s):
**    Aylwin Stewart, Bruce D. Nilo
**
** Purpose:
**     MemHeap is a memory allocator that provides speed in allocating/
**     deallocating small chunks of memory by managing multiple free lists
**     of fixed size memory objects.  The number of lists, and the size of
**     the lists is controlled by the user at MemHeap creation and by the
**     registration objects of various sizes and numbers with the heap, and
**     then by 'commiting' the current heap values for free lists, and/or
**     static allocations.  After the commit, a heap is ready to fulfill
**     general memory requests. It was ported from a previous C++ implemention.
** 
** Types/Classes:
** 
** Functions:
**     get/setDefaultHeap - get/set the MemHeap to be used as the
**                          default heap
**
**     createHeap   - create a new, 'untuned', general purpose heap.
**     destroyHeap  - destroy a heap, and all references from it.
**     registerObjects - ensure the existence of a number of objects of specific
**                       size in the free lists used to speed allocation/deallo-
**                       cation, "tuning" a heap.
**     commitHeap   - commit the free lists and non-transient allocs to be
**                    locked into the top of the heap.
**     enablefreeLists - use, or not, the free lists, if available
**     malloc       - allocate a block of memory
**     realloc      - reallocate a block of memory to a new size
**     free         - deallocate an allocated block of memory.
**
**     getErrno     - get the error code value for a MemHeap call
**     
**     cleanUp      - free up all the memory cells, not free list
**                    chunks, used by a single process.
**     
**     checkMemoryUse - check memory to see if it has been thrashed     
**
**     Private Functions:
**     -----------------------------------------------------------------------
**     setMemHeapErrno       - set the value of the MemHeapErrno varibale
**     initFreeListCounts -  get memory for the freeListLocs member
**     threadFreeList - make a free list out of a block of memory  
**     firstFit       - find intitial Cell big enough for a request
**     coalesceMemory - coalesce all neighboring Free Cells
**     findfreeListIndex - find the index to the free list for an address
**     popFreeList    - pop a free list Cell
**     pushFreeList   - push a free list Cell
**     noteAllocation - update the free/busyBytes for bytes allocated
**     noteDeallocation - update the free/busyBytes for bytes deallocated
**
** Limitations:
**     This code assumes the user has very clear ideas of how they will use
**     memory.  It is meant to be optimized for small objects of sizes up to
**     MAX_MAX_OBJ_SZ.  It can not dynamically grow in size, and has an upper
**     limit of a couple thousand memory references for objects not taken from 
**     the free lists.  (This number of references can only be changed
**     at compile time, see CELL_PTR_BLOCK_SZ in MemHeapPrivate.h.)
**      
** 
** References:
**     See test code in memTest.c for examples.
** 
** History:
**
** 8/8 Modified to provide for a default heap which uses shared memory and
** process locking of critical structures. BDN.
*/

#include <errno.h>
#include <modes.h>
#include <types.h>
#include <process.h>
#include <string.h>
#include <stdlib.h>

#include "MemHeap.h"
#include "MemHeapPrivate.h"
#include "k9Module.h"
#include "isX86.h"


/*****                     M A C R O S                     *****/

/* 
 * Always ignore locks on the NeXT because of problems linking
 * with the cthread shared libaray.
 */
#define __MEMHEAP_C__


#if (defined _OS9000) && (defined __MEMHEAP_C__)

#define lock_acquire( lock_p ) lock_acquire((lock_p) )
#define lock_release( lock_p ) lock_release((lock_p) )

#elif (defined __MEMHEAP_C__)

static int StopWarnings = 0;
#define lock_acquire( lock_p ) StopWarnings++
#define lock_release( lock_p ) StopWarnings--

#endif 

/* Get rid of static key word for debugging, as Rombug
   can't see static objects.
 */
#define static

/* 
 * GET_CELLPTR: Get the CellPtr from a memory Cell.
 */
#define GET_CELLPTR(Cell) \
            (*(CellPtr**)((char*)Cell - PTR_SZ))

/* 
 * SET_CELL_BACKPTR: Set the back pointer to a CellPtr for a memory Cell.
 * Assumes the CellPtr's mem member already points to a memory Cell.
 */
#define SET_CELL_BACKPTR(cellPtr_p) \
              *((CellPtr**)((char*)(cellPtr_p)->mem - PTR_SZ)) = (cellPtr_p)

 
/*
 * SKIP_CELLS_SHIFT_AMT: Used to help calculate number of Dead
 * cells to put after the allocation of chunk from the heap.
 * Based on the largest item in the free list being 256 bytes a 
 * 1K chunk shouldn't be broken up into more than three times.
 * The actual number of cells to skip being the bit position of
 * the highest order bit in the size of the request.
 */
#define SKIP_CELLS_SHIFT_AMT      (8)


#ifdef ICTV_MEM_DEBUG
    #define MEM_PATTERN 0xFA
#endif

/*****            D A T A   S T R U C T U R E S            *****/

/* (See MemHeapPrivate.h) */

/*****                    G L O B A L S                    *****/  

/*  
 * DefaultHeap: The default globally shared heap.
 */
static MemHeap *DefaultHeap = (MemHeap*)NULL;

/* 
 * BoundaryVal: a four byte value used to mark the end of a non free
 * list memory allocation.  This will be used only for debugging
 * to make it easier to determine when people overwrite memory which
 * is not theirs.
 */
static int32 BoundaryVal = 0xFECEFEED;


/*****      F U N C T I O N   D E C L A R A T I O N S      *****/

static void mem_exit( int32 errno );
static int32 initFreeListCounts( MemHeap_p heap );
static void threadFreeList( void **start, u_int32 count, u_int32 size);
static CellPtr *firstFit(  MemHeap_p heap, u_int32 size );
static int coalesceMemory( MemHeap_p heap, CellPtr *cellPtr_p  );
static int findFreeListIndex( MemHeap_p heap, void *oldPtr );
static void *popFreeList( MemHeap_p heap, u_int32 size );
static int pushFreeList( MemHeap_p heap, void *oldPtr );
static void  flushHandle( MemHandle_p );
static int32 internalFree( MemHeap_p heap, void *oldPtr );
static int checkCellPtr( CellPtr *cellPtr_p );

#ifdef NOINLINE
static void noteAllocation( MemHeap_p heap, int32 amount );
static void noteDeallocation( MemHeap_p heap, int32 amount );
#endif




/*****       F U N C T I O N   D E F I N I T I O N S       *****/


void *mem_getDefaultHeap(void)
{
  return DefaultHeap ;
}


void mem_setDefaultHeap(void* addr)
{
  DefaultHeap = (MemHeap *)addr ;
}


void mem_walkHeap(MemHeap *heap, void (*fun)(CellPtr *, CellPtr *, CellPtr *))
{
  int i ;
  CellPtr *current = &(heap->cells[0]) ;
  for(i = 0 ; i < CELL_PTR_BLOCK_SZ ; i++) {
    (*fun)(current,heap->startCellPtr,heap->topCellPtr) ;
  }
}

/*
 * CreateHeap: create a new heap.
 */
MemHeapId 
mem_createHeap
(
        void *memory, u_int32 size, u_int32 maxObjSz, u_int32 maxFreeListSz, u_int32 minFreeListSz
)
{
  MemHeap_p heap = (MemHeap_p) memory;
  CellPtr *cellPtr_p;

  if (!memory)
    {
      MEM_WARN(("mem_createHeap: Invalid address (0x0) to initialize "
                "heap.\n"));
      setMemHeapErrno (EMEM_BAD_ADDRESS);
      return (MemHeapId) 0;
    }

  if(((u_int32)memory % PTR_SZ ) || (size % PTR_SZ)) {
    MEM_WARN(("mem_createHeap: Memory not word aligned.\n"));
                    
    setMemHeapErrno( EMEM_NOTALIGNED );
    return (MemHeapId) 0;
  }
    
  if ( size < MIN_FREE_POOL_SZ ) {
    MEM_WARN(("mem_createHeap: Not enough memory.\n"));
                    
    setMemHeapErrno( EMEM_NOTENUFMEM );
    return (MemHeapId) 0;
  }
  
  /* Make sure we can touch the memory we want to use as a heap */
  mem_permitHeap( (MemHeapId)memory, size );
  
  /* Initialize the cells with state:Unused, size:0, mem:NULL */
  memset((char*)heap->cells, 0, sizeof(heap->cells));
    
  /* Set the first cell to point to remainder of memory */
  cellPtr_p = &(heap->cells[0]);
  cellPtr_p->state = (int32)Free;
  cellPtr_p->size  = size - (sizeof(MemHeap) + PTR_SZ);
  cellPtr_p->mem   = ((char*)heap + sizeof(MemHeap) + PTR_SZ);
  SET_CELL_BACKPTR( cellPtr_p );
    
  heap->startCellPtr = cellPtr_p;
  heap->topCellPtr = cellPtr_p;
  heap->postCommit = *cellPtr_p ;
  heap->committedBytes = 0 ;

  heap->lockedResCnt = 0;
  heap->lruFirst = heap->lruLast = NULL;
    
  heap->maxObjSz = MIN( maxObjSz, MAX_MAX_OBJ_SZ );
  MAKE_MULTIPLE_OF_EXP_OF_2( heap->maxObjSz, OBJ_SZ_INCREMENT );
  heap->maxFreeListSz = MIN( maxFreeListSz, MAX_LIST_SZ );
  heap->minFreeListSz =  minFreeListSz;
  heap->freeListCounts = NULL;
  heap->freeListLocs = NULL;
  heap->freeListRoots =  NULL;
  heap->endOfFreeLists = NULL ;
    
  heap->freeBytes = cellPtr_p->size;
  heap->busyBytes = PTR_SZ;
  heap->endOfHeap = (char*)heap + size - PTR_SZ;
    
  if ( ! lock_create(&(heap->memLock), 1 ) ) {
    MEM_WARN(("mem_createHeap: Failed to create ProcessLock.\n"));
        
    setMemHeapErrno( EMEM_INIT_MEM_FAILURE );
    return (MemHeapId) 0;
  }
    
  if ( heap->maxObjSz ) {
    if ( initFreeListCounts( heap ) != MEM_SUCCESS ) {
      MEM_WARN(("mem_createHeap: Failed initFreeListCounts().\n"));
            
      setMemHeapErrno( EMEM_INIT_MEM_FAILURE );
      return (MemHeapId) 0;
    }
  }

  return (MemHeapId)heap;
}


/*
 * DestroyHeap: destroy a heap, and all refrences from it; this only 
 * succeeds if there are no locked Resource Handles using the MemHeap.
 */
int32
mem_destroyHeap( MemHeapId heapId )
{
    MemHeap_p  heap = (MemHeap_p)heapId;
    
    if ( heap->lockedResCnt ) {
        MEM_WARN(("mem_destroyHeap: Outstanding locked "
               "Resources.\n"));

        setMemHeapErrno( EMEM_IN_USE );
        return MEM_FAILURE;
    }
    
    /* Set all CellPtr references to state:Unused, size:0, mem:NULL */
    memset((char*)heap->cells, 0, sizeof(heap->cells));

    /* If someone handed me memory, how do I "free" it */
    return MEM_SUCCESS;
}


/*
 * RegisterObjects:  Register a number of objects at a certain size to be added
 * to one of the free lists created for efficiency.
 */
int32
mem_registerObjects( MemHeapId heapId, u_int32 count, u_int32 size )
{
    u_int32 index;
    u_int16 *freeListCountN;
    MemHeap_p heap = heapId ? (MemHeap_p)heapId : DefaultHeap;
    
    MAKE_MULTIPLE_OF_EXP_OF_2( size, OBJ_SZ_INCREMENT );
    size = MAX( MIN_OBJ_SZ, size );
    
    if ( ! heap->freeListCounts || size > heap->maxObjSz ) 
        return MEM_FAILURE;
    
    index = ((size-1) / OBJ_SZ_INCREMENT) ;
    freeListCountN = &(heap->freeListCounts[index].max);
    
    if ((count + *freeListCountN) <= heap->maxFreeListSz )
        *freeListCountN += count;
    else
        *freeListCountN = heap->maxFreeListSz;

    return MEM_SUCCESS;    
}


/* Used when committing a heap. This function reclaims all cellPtrs
 * that may have been used prior to the commit. */
static void resetCellPtrs(MemHeap_p heap)
{
  heap->postCommit = heap->cells[0] = *(heap->topCellPtr) ;
  heap->committedBytes = heap->busyBytes - PTR_SZ ;
  heap->busyBytes = PTR_SZ ;
  memset((char*)&(heap->cells[1]), 0, sizeof(heap->cells) - sizeof(CellPtr));
  heap->topCellPtr = heap->startCellPtr = &(heap->cells[0]) ;
  SET_CELL_BACKPTR( heap->topCellPtr );
}

/* Dangerous function. Used to reset a shared heap. Committed memory remains
 * untouched. Post commit memory is made available to the heap again. If tasks
 * still have live pointers to reclaimed memory all bets are off. */

void mem_resetHeap(MemHeapId heapId)
{
  MemHeap_p heap = heapId ? (MemHeap_p)heapId : DefaultHeap;

  int i, numFreelists = heap->maxObjSz/OBJ_SZ_INCREMENT ;
  int size = 0 ;


  heap->cells[0] = heap->postCommit ;
  heap->freeBytes += (heap->busyBytes - PTR_SZ) ;
  heap->busyBytes = PTR_SZ ;
  /* Reset everything but the top to unused. */
  memset((char*)&(heap->cells[1]), 0, sizeof(heap->cells) - sizeof(CellPtr));  
  heap->startCellPtr = heap->topCellPtr = &(heap->cells[0]) ;

  /* This should reclaim all outstanding freelist memory.*/
  for(i = 0 ; i < numFreelists ; i++) {
    size += OBJ_SZ_INCREMENT ;
    if ( heap->freeListCounts[i].max )  {
      heap->freeListRoots[i] = (void **)(heap->freeListLocs[i]) ;
      threadFreeList( heap->freeListRoots[i],
                      heap->freeListCounts[i].max, size) ;
    }
  }
  
  /* Reset the MemHandle_ps for lruFirst and lruLast */
  heap->lruFirst = NULL;
  heap->lruLast = NULL;
}

/*
 * CommitHeap: lock the "tuning" of the heap down, and prepare the heap to be
 * used as a general memory allocator.
 */
int32
mem_commitHeap( MemHeapId heapId )
{
  MemHeap_p heap = heapId ? (MemHeap_p)heapId : DefaultHeap;

  int32 numFreeLists = (heap->maxObjSz / OBJ_SZ_INCREMENT) ;
  int32 freeListRootsSz = numFreeLists * PTR_SZ;
  int32 i, size, totalBytes = freeListRootsSz;
  u_int32 *freeListLocs = heap->freeListLocs;
  FreeListTally *freeListCounts = heap->freeListCounts;
  void ***freeListRoots;

  if(heap->maxObjSz == 0) {
    /* Lock in any mallocs that occurred prior to the commit. */              
    resetCellPtrs(heap) ;
    return MEM_SUCCESS ;
  }
    
  size = OBJ_SZ_INCREMENT ;
    
  for ( i = 0; i < numFreeLists; i++ ) {
    u_int16 *freeListCountMax = &(freeListCounts[i].max);
        
    if ( *freeListCountMax <  heap->minFreeListSz )
        *freeListCountMax = heap->minFreeListSz;
        
    freeListCounts[i].free = *freeListCountMax;
    totalBytes += size * *freeListCountMax;

    size += OBJ_SZ_INCREMENT;
  }
    
  if ( ! (freeListRoots = heap->freeListRoots =
          mem_malloc( heapId, totalBytes )) )
    return MEM_FAILURE;

  /* Lock in any mallocs that occurred prior to the commit. */
  resetCellPtrs(heap) ;

  size = OBJ_SZ_INCREMENT ;
    
  heap->endOfFreeLists = (void*)((char*)freeListRoots + totalBytes);
        
  freeListRoots[0] = (void**)((char*)freeListRoots + freeListRootsSz);
  freeListLocs[0] = (int)freeListRoots[0];
         
  if ( freeListCounts[0].max )
    threadFreeList( freeListRoots[0],freeListCounts[0].max, size );
  else {
    freeListRoots[0] = NULL;
  }

  /* The invariant is that the freeListRoots are NULL if there are
   * no elements for the free list and freeListLoc[i] = freeListLoc[i-1].
   */
  for ( i = 1; i < numFreeLists; i++ ) {
    int32 lastFreeListIndex = i - 1;

    size += OBJ_SZ_INCREMENT;

    while ( lastFreeListIndex && 
            ! freeListCounts[lastFreeListIndex].max )
      lastFreeListIndex--;

    freeListRoots[i] = 
      (void**)((char*)(freeListLocs[lastFreeListIndex])
               + ((lastFreeListIndex + 1) * OBJ_SZ_INCREMENT)
               * freeListCounts[lastFreeListIndex].max) ;

    freeListLocs[i] = (int)freeListRoots[i];
                
    if ( freeListCounts[i].max )
      threadFreeList( freeListRoots[i], freeListCounts[i].max, size );
    else 
      freeListRoots[i] = NULL;
  }
        
  return MEM_SUCCESS;
}


/* 
 * Malloc: Allocate a chunk of memory for use.
 */
#ifndef ICTV_MEM_DEBUG
void*
mem_malloc( MemHeapId heapId, u_int32 size )
#else
void *mem_malloc_debug( MemHeapId heapId, u_int32 size, char *file, int line)
#endif
{
    void *addr = NULL;
    char *patternPtr;
    CellPtr *cellPtr_p;
    u_int32 sizeReq = size;
    MemHeap_p heap = heapId ? (MemHeap_p)heapId : DefaultHeap;
    
    if ( size == 0 )
        return NULL;
    
    if ( ! heap ) {
        MEM_ASSERT(("mem_malloc: Attempt to malloc from NULL "
                "heap.\n"));
                    
        setMemHeapErrno( EMEM_NOHEAP );
        return NULL;
    }
    
    #if defined(ICTVDEBUG) && defined(MEMCHECKING)
    /* Add enough for the BoundaryVal */
    size += sizeof(BoundaryVal);
    #endif
    lock_acquire( &(heap->memLock) );
    
    if ( size <= heap->maxObjSz ) {
        if ((addr = popFreeList( heap, size )) ) {
            lock_release( &(heap->memLock) );
            return addr;
        }
    }

    MAKE_MULTIPLE_OF_EXP_OF_2( size, OBJ_SZ_INCREMENT );
   
    while ( ! (cellPtr_p = firstFit( heap, size )) ) {
# ifndef NO_LRU_FREE
        if ( heap->lruFirst ) 
            flushHandle( heap->lruFirst );
    else 
# endif
        {
            void *foo1, *foo2;
            
            lock_release( &(heap->memLock) );
            setMemHeapErrno( EMEM_NOMEM );
            #ifdef ICTV_MEM_DEBUG
            MEM_ASSERT(("mem_malloc: Out of memory for %d bytes in heap at "
			"0x%p\n\n\n", sizeReq, heap ));

            if ( (MemHeapId)heap != MEDIA_HEAP_ID ) {
	       sleep( 3 );
               _os_sysdbg( foo1, foo2 );
	     }
            #endif
	    
            return heap == DefaultHeap ? NULL : mem_malloc( DEFAULT_HEAP_ID,
							    size );
        }
    }

    lock_release( &(heap->memLock) );
    
    cellPtr_p->pid = getpid();
    
    #if defined(ICTVDEBUG) && defined(MEMCHECKING)
        #ifdef ICTV_MEM_DEBUG
            cellPtr_p->sizeReq = sizeReq;
            cellPtr_p->lineNum = line;
            TimeStamp_Current(&cellPtr_p->time);
            cellPtr_p->filename = file;
            addr = cellPtr_p->mem + sizeReq;
            *(int*)addr = BoundaryVal;
            memset( (void*)((char*)addr + sizeof(BoundaryVal)),
                MEM_PATTERN,
                cellPtr_p->size - (sizeReq + sizeof(BoundaryVal)) );
        #else
            addr = cellPtr_p->mem + size - sizeof(BoundaryVal);
            *(int*)addr = BoundaryVal;
        #endif
    #endif
    
    return cellPtr_p->mem;
}

#ifndef ICTV_MEM_DEBUG
void *mem_calloc( MemHeapId heapId, u_int32 num, u_int32 size )
#else
void *mem_calloc_debug( MemHeapId heapId, u_int32 num, u_int32 size,
    char *file, int line)
#endif
{
  u_int32 totalsize = size * num ;
#ifndef ICTV_MEM_DEBUG
  void *ptr = mem_malloc(heapId, totalsize);
#else
  void *ptr = mem_malloc_debug(heapId, totalsize, file, line) ;
#endif
  if(ptr == NULL)
    return NULL ;

  memset(ptr, 0, totalsize) ;
  
  return ptr ;
}


/* 
 * Realloc: Reallocate a chunk of memory for use.
 * All this work is probably not worth it. BDN.
 */
void *mem_realloc( MemHeapId heapId, void *oldPtr, u_int32 size )
{
    int oldSize;
    CellPtr *cellPtr_p, *nextCellPtr_p;
    MemHeap_p heap = heapId ? (MemHeap_p)heapId : DefaultHeap;

    #if defined(ICTVDEBUG) && defined(MEMCHECKING)
    void *boundary;
    
    if ( heap->maxObjSz && 
        ((int)oldPtr < heap->freeListLocs[0])
         || oldPtr > heap->endOfHeap) {
        MEM_WARN(("mem_realloc: Attempt to realloc from the "
              "wrong heap.\n"));
                    
        setMemHeapErrno( EMEM_WRONG_HEAP );
        return NULL;
    }
    #endif
    
    if ( oldPtr < heap->endOfFreeLists ) { /* It's from a free list */
        int index = findFreeListIndex( heap, oldPtr );
        
        oldSize = (index + 1) * OBJ_SZ_INCREMENT;
        
        if ( size <= oldSize ) 
            return oldPtr;
        else {
            void *newPtr;
        
            newPtr = mem_malloc( heapId, size );
            if ( newPtr ) {
                memcpy( newPtr, oldPtr, oldSize );
                mem_free( heapId, oldPtr );
                return newPtr;
            }else {
                MEM_WARN(("mem_realloc: No Cell big enough for "
                  "realloc.\n"));
                return NULL;
            }
        }
    }
    
    /* Since it's not from the free lists, it must have a CellPtr */
    cellPtr_p = GET_CELLPTR( oldPtr );

    #if defined(ICTVDEBUG) && defined(MEMCHECKING)
    if ( ! cellPtr_p || cellPtr_p->mem != oldPtr ) {
        MEM_WARN(("mem_realloc: Corrupted memory at %p CellPtr->mem: "
              "%p\n", oldPtr, cellPtr_p->mem ));
                    
        setMemHeapErrno( EMEM_BAD_ADDRESS );
        return NULL;
    }
    
    boundary = cellPtr_p->mem + cellPtr_p->size - sizeof(BoundaryVal);
    if ( *(int32*)boundary != BoundaryVal ) {
        MEM_WARN(("mem_realloc: Corrupted memory: memory overun at %p\n",
              oldPtr));
    }
    
    if ( cellPtr_p->size >= size + sizeof(BoundaryVal) ) {
       return cellPtr_p->mem;   
    #else
    if ( cellPtr_p->size >= size ) {
       return cellPtr_p->mem;
    #endif
    
     }else {  /* Allocate new memory and copy */
        void *newPtr;
        
        newPtr = mem_malloc( heapId, size );

        if ( newPtr ) {
            memcpy( newPtr, oldPtr, cellPtr_p->size );
            mem_free( heapId, oldPtr );
            return newPtr;
        }else {
            MEM_WARN(("mem_realloc: No Cell big enough for realloc.\n"));
            return NULL;
        }
      }

    return NULL;
}


/* 
 * Free: Deallocate memory previously allocated.
 */
int32
mem_free( MemHeapId heapId, void *oldPtr )
{
  CellPtr *cellPtr_p;
  MemHeap_p heap = heapId ? (MemHeap_p)heapId : DefaultHeap;
  int32 result = MEM_SUCCESS;
#if defined(ICTVDEBUG) && defined(MEMCHECKING)
  void *boundary;
#endif

    if ( oldPtr == NULL ) {
        MEM_ASSERT(("mem_free: Attempt to free 'NULL'.\n"));
                
        setMemHeapErrno( EMEM_BAD_ADDRESS );
        result = MEM_FAILURE;
        goto result ;
    }
    
#if defined(ICTVDEBUG) && defined(MEMCHECKING)    
    if ((heap->maxObjSz && (int)oldPtr < heap->freeListLocs[0])
            || oldPtr > heap->endOfHeap ) {
        if ( heap != DefaultHeap ) {
	    MEM_WARN(("mem_free: Attempt to free from the wrong heap "
                      "(0x%p)\n", heap));
    	    MEM_WARN(("mem_free: heap changed to DefaultHeap (0x%p).\n",
                  DefaultHeap ));

	    heap = DefaultHeap;
	}
    }

    if ((heap->maxObjSz && (int)oldPtr < heap->freeListLocs[0])
            || oldPtr > heap->endOfHeap ) {
        MEM_WARN(("mem_free: Attempt to free from the wrong heap.\n"));
                
        setMemHeapErrno( EMEM_WRONG_HEAP );
        result = MEM_FAILURE;
        goto result ;
    }
#endif
    
  lock_acquire( &(heap->memLock) );
    
  if ( oldPtr < heap->endOfFreeLists ) { /* It's from the free lists */
    if ( pushFreeList( heap, oldPtr ) != MEM_SUCCESS ) {
      MEM_WARN(("mem_free: Memory not allocated from MemHeap.\n"));
                        
      setMemHeapErrno( EMEM_BAD_ADDRESS );
      result = MEM_FAILURE;
      goto result ;
    }
    else
      goto result ;
  }
    
  cellPtr_p = GET_CELLPTR( oldPtr );

#if defined(ICTVDEBUG) && defined(MEMCHECKING)
    if ( ! cellPtr_p || cellPtr_p->mem != oldPtr ) {
        MEM_WARN(("mem_free: Corrupted memory at %p CellPtr->mem: %p\n",
            oldPtr, cellPtr_p->mem ));
            
        setMemHeapErrno( EMEM_BAD_ADDRESS );
        result = MEM_FAILURE ;
        goto result ;
    }

    #ifdef ICTV_MEM_DEBUG
        boundary = cellPtr_p->mem + cellPtr_p->sizeReq;
        if(cellPtr_p->size < cellPtr_p->sizeReq) {
            MEM_WARN(("mem_free: Cell:%p says size:%d is less than "
                "sizeReq:%d!\n", cellPtr_p, cellPtr_p->size,
                cellPtr_p->sizeReq));
        }
    #else
        boundary = cellPtr_p->mem + cellPtr_p->size - sizeof(BoundaryVal);
    #endif
    
    if ( *(int32*)boundary != BoundaryVal ) {
        MEM_WARN(("mem_free: Corrupted memory, memory overun at 0x%p\n",
            oldPtr ));
    }
    
    #ifdef ICTV_MEM_DEBUG
    {
        char *patternPtr = cellPtr_p->mem + cellPtr_p->sizeReq +
        sizeof(BoundaryVal);
        for (; patternPtr < cellPtr_p->mem + cellPtr_p->size; patternPtr++) {
            if (*patternPtr != (char) MEM_PATTERN) {
                MEM_WARN(("mem_free:Memory in cell@%p: *addr:%p == %x != %x\n",
                    cellPtr_p, patternPtr, *patternPtr, MEM_PATTERN));
                break;
            }
        }
    }
    #endif
   
    if(cellPtr_p->state != Busy) {
        LOCAL_MSG(("mem_free: Attempt to free memory "
            "which isn't busy: %p\n",oldPtr ),
            DBG_MEM_MASKINDEX, DBG_WARN);
        setMemHeapErrno( EMEM_FREE_NOT_BUSY );
        result = MEM_FAILURE;
        goto result;
    }

#endif

    
  cellPtr_p->state = Free;
  
  noteDeallocation( heap, cellPtr_p->size );
    
  if ( coalesceMemory( heap, cellPtr_p ) != MEM_SUCCESS ) 
    result = MEM_FAILURE;

  /* One stop shopping for releasing a lock...*/
 result:
    
  lock_release( &(heap->memLock) );
  return result ;
}


/* 
 * CleanUp:  Free all the memory used by a process that is pointed
 * to by CellPtrs.  (There is currently no way to retrieve free list
 * chunks that a process doesn't free itself.)
 */
void
mem_cleanUp( MemHeapId heapId, int32 procId )
{
    MemHandle_p handle;
    CellPtr *cellPtr_p, *lastCellPtr_p;
    MemHeap_p heap = heapId ? (MemHeap_p)heapId : DefaultHeap;
        
    if ( ! heap )
        return;
    
    cellPtr_p = &heap->cells[0];
    lastCellPtr_p = &heap->cells[CELL_PTR_BLOCK_SZ];
    handle = heap->lruFirst;

    lock_acquire( &(heap->memLock) );
    
    for ( ; cellPtr_p < lastCellPtr_p; cellPtr_p++ ) 
        if ( cellPtr_p->pid == procId )
            internalFree( heap, cellPtr_p->mem );
    
    while ( handle ) {
        cellPtr_p = GET_CELLPTR( handle->asset );
        if ( cellPtr_p->pid == procId )
            internalFree( heap, cellPtr_p->mem );
    
        handle = handle->next;
    }
    
    lock_release( &(heap->memLock) );
    
    return;
}


/* 
 * CheckMemoryUsed: Check for trashed memory.  Check free lists, by
 * comparing the free count, with the number of chunks actually on
 * the list.  Compare the CellPtr based memory by checking back
 * pointers, and by checking for a boundary value.
 */
#if defined(ICTVDEBUG) && defined(MEMCHECKING)
int 
mem_checkMemoryUsed( MemHeapId heapId )
{
    CellPtr *cellPtr_p;
    void ***freeListRoots;
    int i, numFreeLists, retVal = 0;
    MemHeap_p heap = heapId ? (MemHeap_p)heapId : DefaultHeap; 

    
    if ( ! heap ) {
        MEM_WARN(("No heap found for call to checkMemoryUsed()\n" ));
        return MEM_FAILURE;
    } else {
        freeListRoots = heap->freeListRoots;
    }
    
    /* Check Free Lists */
    numFreeLists = (heap->maxObjSz / OBJ_SZ_INCREMENT);
    
    for ( i = 0; i < numFreeLists; i++ ) {
        void **chunk;
        int count = 0;
        u_int16 freeListCountFree;
        
        freeListCountFree = heap->freeListCounts[i].free;
        
        chunk = freeListRoots[i];
        while ( chunk && count < freeListCountFree) {
            int nextList = i + 1 < numFreeLists ? i + 1 : i;
            
            count++;        
            
            if ( (int)chunk < heap->freeListLocs[i] ) 
                MEM_WARN(("Heap at 0x%p has broken free list for size "
                          "%d.", heap, (i+1) * OBJ_SZ_INCREMENT ));

            if ( (nextList + 1) < numFreeLists ) {
                if ( (int)chunk >= heap->freeListLocs[nextList] ) {
                    MEM_WARN(("Heap at 0x%p has broken free list for "
                              "size %d.",
                              heap, (i+1) * OBJ_SZ_INCREMENT ));
               }
           }else {
                if ( (int)chunk >= (int)heap->endOfFreeLists ) {
                    MEM_WARN(("Heap at 0x%p has broken free list for "
                              "size %d.",
                              heap, (i+1) * OBJ_SZ_INCREMENT ));
                }
	    }
	    chunk = *chunk;
        }
    
        if ( count != freeListCountFree || chunk != NULL) {
            MEM_WARN(("Heap at 0x%p has broken free list for size %d.",
                heap, (i+1) * OBJ_SZ_INCREMENT ));
        }
    }

    /* Check CellPtr based Memory */
    cellPtr_p = heap->startCellPtr;
    for ( cellPtr_p; cellPtr_p < heap->topCellPtr; cellPtr_p++ )
        if ( cellPtr_p->state == Busy )
            retVal = checkCellPtr( cellPtr_p );

    return retVal;
}
#endif


/* 
 * GetErrno: Return the current value of MemHeapErrno.  
 */
MemErrCode
mem_getMemHeapErrno(void)
{
    return getMemHeapErrno();
}



/* Private Functions */

/*
 * Exit: Abort on some fatal error.
 */
static void
mem_exit( int32 errno )
{
    setMemHeapErrno( errno );
    exit( errno );
    return;
}


/* 
 * initFreeListCounts: Get memory for freeListCounts and freeListLocs members;
 * to hold the sizes and locations of each free list.
 */
static int 
initFreeListCounts( MemHeap_p heap )
{
    int32 i, errno;
    int32 numFreeLists = (heap->maxObjSz / OBJ_SZ_INCREMENT) ;
           
    if ( ! (heap->freeListCounts = 
                mem_malloc((MemHeapId)heap,
                           numFreeLists *  sizeof( FreeListTally))) )
        return MEM_FAILURE;
    
    if ( ! (heap->freeListLocs =
                mem_malloc((MemHeapId)heap,
                   numFreeLists * sizeof( int ))) )
        return MEM_FAILURE;
    
    for ( i = 0; i < numFreeLists; i++ ) {
        heap->freeListCounts[i].max = 0;
        heap->freeListCounts[i].free = 0;
        heap->freeListLocs[i] = 0;
    }
    
    return MEM_SUCCESS;
}


/* 
 * ThreadFreeList: Turn a chunk of memory into a list of same sized nodes.
 */
static void
threadFreeList( void **start, u_int32 count, u_int32 size )
{
    int32 i;
    void **next = start;
    
    for ( i = 1; i < count; i++ ) {
        *next = (void*) (((char*)next) + size);
        next = (void**) *next;
    }
    
    /* NULL terminate the free list */
    *next = NULL;
    return;
}


/* 
 * FirstFit: Find the first Cell large enough to accomodate a memory request.
 */
static CellPtr *
firstFit( MemHeap_p heap, u_int32 size )
{
    int32 sizePlusPtrSz = size + PTR_SZ;
    CellPtr *cellPtr_p, *nextCellPtr_p;
    CellPtr *lastCellPtr_p = &(heap->cells[CELL_PTR_BLOCK_SZ - 1]);
    
    for ( cellPtr_p = heap->startCellPtr;
            cellPtr_p->state != Unused  && cellPtr_p < lastCellPtr_p;
            cellPtr_p++ ) {
        if ( cellPtr_p->state == Free && cellPtr_p->size >= size )
            break;
    }
    
    if ( cellPtr_p->state == Unused  || cellPtr_p >= lastCellPtr_p) {
        MEM_WARN(("firstFit: Out of memory for %d bytes\n", size ));

        setMemHeapErrno( EMEM_NOMEM );
        return NULL;
    }
        
    cellPtr_p->state = Busy;

    nextCellPtr_p = cellPtr_p + 1;
    if ( nextCellPtr_p == lastCellPtr_p )
        return cellPtr_p;

    switch( nextCellPtr_p->state) {
        case Unused:  {
            int32 tmpSize = cellPtr_p->size - sizePlusPtrSz;
            int i, skipCellCount =
                       bitScanReverse( size >> SKIP_CELLS_SHIFT_AMT );
            
            if ( tmpSize < OBJ_SZ_INCREMENT ) {
                noteAllocation( heap, cellPtr_p->size );
                heap->topCellPtr = nextCellPtr_p;
                break;
            }
            
            for ( i = 0; i < skipCellCount; i++ ) {
                if ( nextCellPtr_p + 1 == lastCellPtr_p )
                    return cellPtr_p;
                    
                nextCellPtr_p->state = Dead;
                nextCellPtr_p->size = 0;
                nextCellPtr_p->pid = 0;
                nextCellPtr_p->mem = NULL; 
                
                nextCellPtr_p++;
            }
            
            nextCellPtr_p->state = Free;
            nextCellPtr_p->size = tmpSize;
            nextCellPtr_p->mem = cellPtr_p->mem + sizePlusPtrSz;
            
            heap->topCellPtr = nextCellPtr_p;
            noteAllocation( heap, sizePlusPtrSz );
            SET_CELL_BACKPTR( nextCellPtr_p );

            cellPtr_p->size = size;
            break;
        }
        case Free:  {
            nextCellPtr_p->size += cellPtr_p->size - size;
            nextCellPtr_p->mem = cellPtr_p->mem + size;
            
            noteAllocation( heap, size );
            SET_CELL_BACKPTR( nextCellPtr_p );

            cellPtr_p->size = size;
            break;
        }
        case Busy:  {
            noteAllocation( heap, cellPtr_p->size );
            break;
        }
        case Dead:  {
            int32 tmpSize = cellPtr_p->size - sizePlusPtrSz;
        
            if ( tmpSize > 0 ) {
                nextCellPtr_p->size = tmpSize;
                nextCellPtr_p->state = Free;
                nextCellPtr_p->mem = cellPtr_p->mem + sizePlusPtrSz;
                noteAllocation( heap, sizePlusPtrSz );
                
                SET_CELL_BACKPTR( nextCellPtr_p );

                cellPtr_p->size = size;
            } else {
                noteAllocation( heap, cellPtr_p->size );
            }
            
            break;
        }
    }

    return cellPtr_p;
}


/* 
 * CoalesceMemory: Coalesce memory around a Cell whose CellPtr has
 * been, or is about to be, changed to a Free state.
 */
static int32
coalesceMemory( MemHeap_p heap, CellPtr *cellPtr_p  )
{
    CellPtr *leftPtr_p , *rightPtr_p;
    CellPtr *firstPtr_p = heap->startCellPtr;
    
        
    /* Coalesce the right side */
    
    for ( rightPtr_p = cellPtr_p + 1;
            rightPtr_p->state == Dead && rightPtr_p <= heap->topCellPtr;
            rightPtr_p++ ) {
        ;
    }
      
      
    if ( rightPtr_p == heap->topCellPtr ) {
        if ( rightPtr_p->state != Unused ) {  /* Assumes state is 'Free' */
            cellPtr_p->size += rightPtr_p->size + PTR_SZ;
            noteDeallocation( heap, PTR_SZ );
 
            rightPtr_p->state = Unused;
            rightPtr_p->size = 0;
            rightPtr_p->pid = 0;
            rightPtr_p->mem = NULL;

            while ( --rightPtr_p > cellPtr_p )
                rightPtr_p->state = Unused;
       }
       
       heap->topCellPtr = cellPtr_p;
        
    }else if ( rightPtr_p->state == Free ) {
        cellPtr_p->size += rightPtr_p->size + PTR_SZ;
        noteDeallocation( heap, PTR_SZ );

        rightPtr_p->state = Dead;
        rightPtr_p->size = 0;
        rightPtr_p->pid = 0;
        rightPtr_p->mem = NULL; 
    }
    
    
    /* Coalesce the left side */
    
    leftPtr_p = cellPtr_p - 1;
    if ( leftPtr_p < firstPtr_p ) {
        
        return MEM_SUCCESS;
    }
        
    while ( leftPtr_p->state == (int)Dead && leftPtr_p >= firstPtr_p ) 
        leftPtr_p--;
    
    if ( leftPtr_p->state == (int)Busy ) {
     
        return MEM_SUCCESS;
    }else if ( leftPtr_p->state == (int)Unused ) {
        MEM_ASSERT(
          ("coalesceMemory: Unused CellPtr to left of topPtrCell.\n"));

        mem_exit( EMEM_IMPL );
        return MEM_FAILURE;
    }
    
    /* The left CellPtr's state must be Free to reach here 
       and all the Cells b/n it and CellPtr must be Dead   */
    leftPtr_p->size += cellPtr_p->size + PTR_SZ;
    noteDeallocation( heap, PTR_SZ );
    
    cellPtr_p->state = Dead;
    cellPtr_p->size = 0;
    cellPtr_p->pid = 0;
    cellPtr_p->mem = NULL;
    
    /* Free up Dead CellPtr's if the top CellPtr was coalesced */
    if ( cellPtr_p == heap->topCellPtr ) {
        heap->topCellPtr = leftPtr_p;

        for ( leftPtr_p = leftPtr_p + 1; leftPtr_p <= cellPtr_p; leftPtr_p++ )
            leftPtr_p->state = Unused;
    }
    
   return MEM_SUCCESS;
}


/* 
 * FindfreeListIndex: Return the index into freeListRoots for the address
 * passed in the parameter oldPtr.  Returns the correct index or MEM_FAILURE.
 */
static int32
findFreeListIndex( MemHeap_p heap, void *oldPtr )
{
    u_int32 *freeListLocs = heap->freeListLocs;
    void ***freeListRoots = heap->freeListRoots;
    FreeListTally *freeListCounts = heap->freeListCounts;
    int32 i, numFreeLists = (heap->maxObjSz / OBJ_SZ_INCREMENT) ;
    
    for ( i = 0; i < numFreeLists; i++ ) {
        if ((int)oldPtr < freeListLocs[i] ) {
            if ( freeListCounts[i - 1].max )
                return i - 1;
            else {
                while ( i < numFreeLists )
                    if ( freeListRoots[i] != NULL )
                        return i;
                    else
                        i++;
            }
        }
    }
    
    return freeListCounts[numFreeLists - 1].max ?
               numFreeLists - 1 :
               MEM_FAILURE;
}
 
 
/* 
 * PopFreeList: Pop a Cell from the appropriate free list, and return it.
 * Return NULL if no Cells are available.
 */
static void*
popFreeList( MemHeap_p heap, u_int32 size )
{
    
    int32 index, numFreeLists;
    void *next, **freeList;
    void ***freeListRoots = heap->freeListRoots;
    
    if ( ! freeListRoots )
        return NULL;

    index = ((size-1) / OBJ_SZ_INCREMENT) ;
    numFreeLists = (heap->maxObjSz / OBJ_SZ_INCREMENT) ;
    
    do {
        freeList = freeListRoots[index];
    }while ( ! freeList && ++index < numFreeLists );
    
    
    if ( ! freeList )
        return NULL;
    
    next = *freeList;
    freeListRoots[index] = (void**)next;
    
    #if defined(ICTVDEBUG) && defined(MEMCHECKING)
    heap->freeListCounts[index].free--;
    *(int*)freeList = BoundaryVal;
    freeList = (void**)((char*)freeList + sizeof(BoundaryVal));
    #endif
    
    noteAllocation( heap, (index + 1) * OBJ_SZ_INCREMENT );

    return (void*)freeList;
}


/* 
 * PushFreeList: Put a Cell from a free list, back on to its list.
 */
static int32
pushFreeList( MemHeap_p heap, void *oldPtr )
{
    void **freeList;
    void ***freeListRoots = heap->freeListRoots;
    int32 chunkSize, index = findFreeListIndex( heap, oldPtr );
    #ifdef ICTV_MEM_DEBUG
    void **aChunk;
    #endif
    
    if ( index == MEM_FAILURE )
        return MEM_FAILURE;
    
    chunkSize = (index + 1) * OBJ_SZ_INCREMENT;
    
    #if defined(ICTVDEBUG) && defined(MEMCHECKING)
    oldPtr = (void*)((char*)oldPtr - sizeof(BoundaryVal));
    if ( *(int32*)oldPtr != BoundaryVal ) {
    MEM_WARN(("mem_free: Corrupted memory, memory overwritten at "
              "0x%p from free lists.\n", oldPtr ));
    }
    #endif
    
    freeList = freeListRoots[index];
    #ifdef ICTV_MEM_DEBUG 
    for ( aChunk = freeList; aChunk != NULL; aChunk = *aChunk ) {
        if ( oldPtr == aChunk ) {
        MEM_WARN(("mem_free: 0x%p is on free list, and being "
                  "freed again.\n", oldPtr ));
        return MEM_FAILURE;
        }
    }
    #endif

    #ifdef ICTV_MEM_DEBUG
    memset( oldPtr, MEM_PATTERN, chunkSize );
    #endif

    *((void**)oldPtr) = freeList;
    freeListRoots[index] = (void**)oldPtr;
    
    #if defined(ICTVDEBUG) && defined(MEMCHECKING)
    heap->freeListCounts[index].free++;
    #endif
    
    noteDeallocation( heap, chunkSize );

    return MEM_SUCCESS;
} 


#ifdef NOINLINE

/*
 * NoteAllocation: update FreeBytes and BustBytes for a heap on allocation.
 * (This could be a macro.)
 */
static void
noteAllocation( MemHeap_p heap, int32 count )
{
    heap->freeBytes -= count;
    heap->busyBytes += count;
    return;
}


/*
 * NoteDeallocation: update FreeBytes and BustBytes for a heap on deallocation.
 * (This could be a macro.)
 */
static void
noteDeallocation( MemHeap_p heap, int32 count )
{
    heap->freeBytes += count;
    heap->busyBytes -= count;
    return;
}

#endif /* NOINLINE */


/* 
 * InternalFree: Deallocate memory previously allocated w/o
 * reaquiring the heap's memLock.
 */
static int32
internalFree( MemHeap_p heap, void *oldPtr )
{
    CellPtr *cellPtr_p;
    int32 result = MEM_SUCCESS;
    #if defined(ICTVDEBUG) && defined(MEMCHECKING)
    void *boundary;
    #endif
    
    if ( oldPtr == NULL ) {
        MEM_ASSERT(("mem_free: Attempt to free 'NULL'.\n"));
                
        setMemHeapErrno( EMEM_BAD_ADDRESS );
        return MEM_FAILURE;
    }
    
    
#if defined(ICTVDEBUG) && defined(MEMCHECKING)
    if ((heap->maxObjSz && (int)oldPtr < heap->freeListLocs[0])
            || oldPtr > heap->endOfHeap ) {
        MEM_WARN(("mem_free: Attempt to free from the wrong heap.\n"));
                
        setMemHeapErrno( EMEM_WRONG_HEAP );
        return MEM_FAILURE;
    }
#endif
    
    if ( oldPtr < heap->endOfFreeLists ) { /* It's from the free lists */
        if ( pushFreeList( heap, oldPtr ) != MEM_SUCCESS ) {
            MEM_WARN(("mem_free: Memory not allocated from MemHeap.\n"));
                    
            setMemHeapErrno( EMEM_BAD_ADDRESS );
            return MEM_FAILURE;
        } else {
            return MEM_SUCCESS;
        }
    }
    
    cellPtr_p = GET_CELLPTR( oldPtr );
    
#if defined(ICTVDEBUG) && defined(MEMCHECKING)
    if ( ! cellPtr_p || cellPtr_p->mem != oldPtr ) {
        MEM_WARN(("mem_free: Corrupted memory at %p CellPtr->mem: "
              "%p\n", oldPtr, cellPtr_p->mem ));
                        
        setMemHeapErrno( EMEM_BAD_ADDRESS );
        return MEM_FAILURE ;
    }
    
    #ifdef ICTV_MEM_DEBUG
        boundary = cellPtr_p->mem + cellPtr_p->sizeReq;
    #else
        boundary = cellPtr_p->mem + cellPtr_p->size -
            sizeof(BoundaryVal);
    #endif

    if ( *(int32*)boundary != BoundaryVal ) {
        MEM_WARN(("mem_free: Corrupted memory: memory overun at "
              "%p\n",oldPtr ));
    }
#endif
    
    cellPtr_p->state = Free;
    
    noteDeallocation( heap, cellPtr_p->size );
    
    if ( coalesceMemory( heap, cellPtr_p ) != MEM_SUCCESS ) {
        MEM_WARN(("internalfree: failed to colesce memory for "
              "%p\n",oldPtr ));
        return MEM_FAILURE;
    }
    
    return MEM_SUCCESS ;
}


/* 
 * FlushHandle: similar to hndl_flush(), but using internalFree(),
 * instead of mem_free(), and no checking.
 */
static void
flushHandle( MemHandle_p handle )
{
    MemHeap_p heap = handle->heap   ? (MemHeap_p)handle->heap
                                    : DefaultHeap;
    
    deleteLRUList( handle );
    internalFree( heap, handle->asset );
    handle->asset = NULL;
    
    return;
}


/* 
 * PermitHeap: give a process permission to access the memory for a
 * specific heap.  
 */
int 
mem_permitHeap( MemHeapId heapId, int heapSize )
{
    void *memory = (void*)heapId;
    error_code retVal;
    
    if ( ! memory ) 
        return MEM_FAILURE;
    
    if ( ! heapSize )
        return MEM_FAILURE;
    
# ifdef USE_SSM
    retVal = _os_permit( memory, heapSize, S_IREAD | S_IWRITE,
                         getpid() );
#if 0
    if ( retVal )
        GLOBAL_MSG ( ("_os_permit failed:%d, you may not have "
             "SSM installed\n", retVal), DBG_MISC );
#endif /* 0 */

# endif /* USE_SSM */
    return MEM_SUCCESS;
}


#if defined(ICTVDEBUG) && defined(MEMCHECKING)
/* 
 * CheckCellPtr: Check a cell pointer and the chunk of memory it
 * points too for memory corruption.
 */

static int
checkCellPtr( CellPtr *cellPtr_p )
{
    int retVal = 0;
    Bool boundaryValueFound = NO;
    int32 *startCellMemory, *cellMemory;
    
    if ( ! cellPtr_p ) {
        MEM_WARN(("NULL cell pointer passed to checkCellPtr()\n" ));
        return MEM_FAILURE;
    }
    
    startCellMemory = (int32*)cellPtr_p->mem;
    cellMemory = (int32*)GET_CELLPTR( cellPtr_p->mem );
    if ( cellMemory != (int32*)cellPtr_p ) {
        void *foo1, *foo2;
        
        MEM_WARN(("CellPtr pointer at 0x%p points to memory which "
            "does not point to it.\n    Memory was requested "
            "by process %d\n\n\n", cellPtr_p, cellPtr_p->pid));
        sleep( 3 );
        _os_sysdbg( foo1, foo2 );
        
        retVal = -1;
    }
    
   #ifdef ICTV_MEM_DEBUG
    cellMemory = (int32*)(cellPtr_p->mem + cellPtr_p->sizeReq);
    boundaryValueFound = (*cellMemory == BoundaryVal);
    {
        u_char *patternPtr;
        for(patternPtr = (u_char*)cellMemory + sizeof(int32);
                patternPtr < (u_char*)(cellPtr_p->mem + cellPtr_p->size);
                patternPtr++) {
            if(*patternPtr != (u_char)MEM_PATTERN) {
                MEM_WARN(("checkCellPtr:Memory in cell@%p owned by:%d:"
                    " *addr:%p == %x != %x\n", cellPtr_p, cellPtr_p->pid,
                    patternPtr, *patternPtr, MEM_PATTERN));
                break;
            }
        }
    }
   #else
    cellMemory = (int32*)(cellPtr_p->mem +
                          cellPtr_p->size - sizeof( int32 ));

    for (; cellMemory > (int32*)cellPtr_p->mem ; cellMemory-- ) {
        if ( *cellMemory == BoundaryVal ) {
            boundaryValueFound = YES;
        }
    }
   #endif

    
    if ( ! boundaryValueFound ) {
        void *foo1, *foo2;
        
        MEM_WARN(("Memory at end of memory requested at 0x%p was "
            "trashed.\n    Memory was requested "
            "by process %d\n\n\n", cellPtr_p->mem, cellPtr_p->pid ));
        sleep( 3 );
        _os_sysdbg( foo1, foo2 );
    
        retVal = -1;
    }
    
    return retVal;
}

#endif
