
/*
** Copyright (C) 1994, 1995,
** ICTV Inc. 14600 Winchester Blvd.
** Los Gatos, CA 95030
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: ictv interactive television memlib
** 
** File: MemHeapPrivate.h - 'Protected' info for Classes which are 'friends'
**     $Id: MemHeapPrivate.c,v 1.4 1995/11/04 02:03:27 wul Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/memlib/MemHeapPrivate.c,v $
**
** Author(s):
**    Aylwin Stewart
**
** Purpose:
**     This file was created primarily because their needs to be a fairly
**     close relationship between the Resource Manager, and the memlib.
**     It's primary purpose was to encapsulate resource handle related 
**     function calls.  The rest was put here primarily to facilitate
**     the ease of writing the test code.
** 
** Types/Classes:
** 
** Functions:
**     appendLRUList  - append a Resource Handle to the LRU list
**     deleteLRUList  - remove a Resource Handle from the LRU list
**     incrementLockedResourceCount - (says it all)
**     decrementLockedResourceCount - (says it all)
** 
** 
** 
*/

#include "MemHeapPrivate.h"


/*****                     M A C R O S                     *****/


/*****                      E N U M S                      *****/


/*****            D A T A   S T R U C T U R E S            *****/


/*****                    G L O B A L S                    *****/                  

/* 
 * MemHeapErrno:  the error code value
 */
static MemErrCode MemHeapErrno = EMEM_NOERROR ;


/*****       F U N C T I O N   D E F I N I T I O N S       *****/

/* 
 * GetErrno: Return the current value of MemHeapErrno.  
 */
MemErrCode
getMemHeapErrno(void)
{
    return MemHeapErrno;
}


/* 
 * SetMemHeapErrno: set the value of MemHeapErrno to an error code.
 */
void
setMemHeapErrno( MemErrCode errorCode )
{
    MemHeapErrno = errorCode;   
    return;
}



/* 
 * AppendLRUList: Put a MemHandle_p at the end of a MemHeap's
 * Least Recently Used list.
 */
int32
appendLRUList( MemHeapId heapId, MemHandle_p handle )
{
    MemHeap_p heap = heapId ? (MemHeap_p)heapId : mem_getDefaultHeap();
    MemHandle_p tmpHndl = heap->lruFirst;
    MemHandle_p lastRes = heap->lruLast;
    
    /* If it's already in the list simply return MEM_SUCCESS */
    while ( tmpHndl ) {
	if ( tmpHndl == handle )
	    return MEM_SUCCESS;

	tmpHndl = tmpHndl->next;
    }

    handle->prev = lastRes;
    handle->next = NULL;
    
    if ( lastRes )
        lastRes->next = handle;
    
    if ( ! heap->lruFirst )
        heap->lruFirst = handle;
	
    heap->lruLast = handle;
    
    return MEM_SUCCESS;
}


/* 
 * DeleteLRUList: Remove a MemHandle_p from a MemHeap's Least Recently
 * Used (LRU) list.  The LRU list is traversed to gurantee that the
 * the handle is really in the list.
 */
int32
deleteLRUList( MemHandle_p handle )
{
    MemHeap_p heap = handle->heap ? (MemHeap_p)handle->heap
                                  : mem_getDefaultHeap();
    MemHandle_p currentHandle = heap->lruFirst;
    
    while ( currentHandle ) {
        if ( handle == currentHandle ) {
	    if ( handle->prev )
	        handle->prev->next = handle->next;
	    else
	        heap->lruFirst = handle->next;
	    
	    if ( handle->next )
	        handle->next->prev = handle->prev;
	    else 
	        heap->lruLast = handle->prev;
		
	    return MEM_SUCCESS;
	}
	
	currentHandle = currentHandle->next;
    }
    
    return MEM_FAILURE;
}


/* 
 * IncrementLockedResCnt: The name says it all.  Returns the
 * locked resource count after the operation.
 */
int32
incrementLockedResCnt( MemHeapId heapId )
{
    MemHeap_p heap = heapId ? (MemHeap_p)heapId : mem_getDefaultHeap();
    
    return ++(heap->lockedResCnt);
}


/* 
 * DecrementLockedResCnt: The name says it all.  Returns the
 * locked resource count after the operation.
 */
int32
decrementLockedResCnt( MemHeapId heapId )
{
    MemHeap_p heap = heapId ? (MemHeap_p)heapId : mem_getDefaultHeap();
    
    return --(heap->lockedResCnt);
}
