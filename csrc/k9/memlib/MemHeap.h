
/*
** Copyright (C) 1994, 1995,
** ICTV Inc. 14600 Winchester Blvd.
** Los Gatos, CA 95030
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: ictv interactive television Memory Manager Library
** 
** File: MemHeap.h - A memory heap with a 'C' like malloc/free interface
**     $Id: MemHeap.h,v 1.15 1995/11/04 02:03:27 wul Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/memlib/MemHeap.h,v $
**
** Author(s):
**    Aylwin Stewart, Bruce D. Nilo
**
** Purpose:
**     MemHeap is a memory allocator that provides speed in allocating/
**     deallocating small chunks of memory by managing multiple free lists
**     of fixed size memory objects.  The number of lists, and the size of
**     the lists is controlled by the user at MemHeap creation and by the
**     registration objects of various sizes and numbers with the heap, and
**     then by 'commiting' the current heap values for free lists, and/or
**     static allocations.  After the commit, a heap is ready to fulfill
**     general memory requests. It was ported from a previous C++ implementation.
** 
** Types/Classes:
**     MemHeapId  - the identifier for a specific heap.
** 
** Functions:
**     get/setDefaultHeap - get/set the MemHeap to be used as the
**                          default heap
**
**     createHeap   - create a new, 'untuned', general purpose heap.
**     destroyHeap  - destroy a heap, and all references from it.
**     registerObjects - ensure the existence of a number of objects of
**                       specific size the free lists used to speed
**                       allocation/deallocation, (i.e. "tuning".)
**     commitHeap   - commit the free lists and non-transient allocations to be
**                    locked into the top of the heap.
**     
**     malloc       - allocate a block of memory
**     calloc
**     realloc      - reallocate a block of memory to a new size
**     free         - deallocate an allocated block of memory
**     
**     getErrno     - get the error code value for a MemHeap call
**     
**     cleanUp      - free up all the memory cells, not free list
**                    chunks, used by a single process.
**     
**     checkMemoryUsed - check memory to see if it has been thrashed     
**
** Limitations:
**     This code assumes the user has very clear ideas of how they will use
**     memory.  It is meant to be optimized for small objects of sizes up to
**     MAX_MAX_OBJ_SZ.  It can not dynamically grow in size, and has an upper
**     limit of a couple thousand memory references for objects not taken from 
**     the free lists.  (This number of references can only be changed
**     at compile time, see CELL_PTR_BLOCK_SZ in MemHeapPrivate.h.)
**      
** 
** References:
**     See test code in memTest.c for examples.
** 
** History:
**
** 8/8 Modified to provide for a default heap which uses shared memory and
** process locking of critical structures. BDN.
*/

#ifdef _OS9000

#ifndef __MEMHEAP_H__
#define __MEMHEAP_H__

#include "ictvcommon.h"

/*****                     M A C R O S                     *****/

/* 
 * PTR_SZ: The size of a pointer variable.
 */
#define PTR_SZ             sizeof(void*)

/* 
 * DEFAULT_HEAP_ID: For users who haven't created their own heap, the
 * default heap can be used.  It is a global, shared heap.
 */
#define DEFAULT_HEAP_ID         ((MemHeapId)0)


/*
 * MEDIA_HEAP_ID: It is a global heap for the media processes.
 */
# if 1
#define MEDIA_HEAP_ID         ((MemHeapId)0x72e000)
# else
#define MEDIA_HEAP_ID 0
# endif

/*
 * MEDIA_HEAP_SZ: The size of the MediaHeap, 840K.
 */
#define MEDIA_HEAP_SZ         (840 * (1<<10))


/* 
 * MIN_OBJ_SZ: The smallest legal value for maxObjSz. (To be consistent
 * this should be MIN_MAX_OBJ_SZ - BDN)
 */
#define MIN_OBJ_SZ              (16)

/* 
 * MAX_MAX_OBJ_SZ: The largest legal value for maxObjSz.
 */
#define MAX_MAX_OBJ_SZ          (256)

/* 
 * MAX_LIST_SZ: The largest legal value for min/maxFreeListSz.
 */
#define MAX_LIST_SZ         ((1<<12) - 1)

/* 
 * MIN_FREE_POOL_SZ: The least amount of memory deemed useful for heapifying
 */
#define MIN_FREE_POOL_SZ        (1<<16)


/* 
 * MEM_SUCCESS & MEM_FAILURE: int return codes to indicate the
 * success/failure of a call
 */
#define MEM_SUCCESS                  0
#define MEM_FAILURE                  -1

/*****                      E N U M S                      *****/

/* 
 * Error Return Codes:
 *
 * EMEM_NOERROR       No error was generated
 *                   
 * EMEM_NOMEM         A warning, generated when allocating memory or
 *                    more masters.
 *
 * EMEM_NOTENUFMEM    A fatal error, generated by new operator.
 * 		      Not enough memory specified to initialize a heap
 *
 * EMEM_NOTALIGNED    A fatal error, generated by new operator.  Cannot 
 *                    initialize a heap with memory that is not a multiple of 4.
 *
 * EMEM_INIT_MEM_FAILURE  An fatal error, generated when initialization of a
 *                    MemHeap or a MemHandle fails.
 *
 * EMEM_IMPL	      A fatal error.  Memory package implementation error.
 *
 * EMEM_NOCELLPTRS    A warning, generated when allocating memory.
 * 		      Ran out of master pointers  
 *
 * EMEM_IN_USE        A warning generated when attempting to destruct a heap
 *                    which is still being reference by a locked ResHandle.
 *
 * EMEM_WRONG_HEAP    A warning generated when attempting to realloc/free
 *                    memory from the wrong heap.
 *
 * EMEM_BAD_ADDRESS   A warning generated when an attempt is made to free an
 *                    address that was not returned by the malloc/realloc
 *                    routines.
 *
 * EMEM_BAD_HANDLE   A NULL MemHandle was passed.
 *
 * EMEM_LOCKED_HANDLE A MemHandle wasn't flushed because it is locked.
 *
 * EMEM_NO_FETCH_FUNC A MemHandle wasn't supplied a FetchAssetFunc on creation.
 *
 * EMEM_NO_GET_FUNC   A MemHandle wasn't supplied a GetAssetFunc on creation.
 *
 * EMEM_LINK_FAILURE  Unable to successfully link to default shared module.
 *
 * EMEM_FREE_NOT_BUSY Attempt to free a cell which isn't busy,
 *	probably previously freed.
 */
typedef enum {
    EMEM_NOERROR,
    EMEM_NOHEAP,
    EMEM_NOMEM,
    EMEM_NOTENUFMEM,
    EMEM_NOTALIGNED,  
    EMEM_INIT_MEM_FAILURE,
    EMEM_IMPL,
    EMEM_NOCELLPTRS,
    EMEM_IN_USE,       
    EMEM_WRONG_HEAP,
    EMEM_BAD_ADDRESS,
    EMEM_BAD_HANDLE,
    EMEM_LOCKED_HANDLE,
    EMEM_NO_FETCH_FUNC,
    EMEM_NO_GET_FUNC,
    EMEM_LINK_FAILURE,
    EMEM_FREE_NOT_BUSY,
    EMEM_BAD_HANDLE_TYPE
} MemErrCode;



/*****            D A T A   S T R U C T U R E S            *****/

typedef u_int32 MemHeapId;


/*****                    G L O B A L S                    *****/                  

 
/*****      F U N C T I O N   D E C L A R A T I O N S      *****/

void *mem_getDefaultHeap(void) ;
void mem_setDefaultHeap(void*) ;
 

/* 
 * CreateHeap: create a new heap for shared or private use.  If the value
 * passed for the parmeter 'memory' is NULL, the memory will be allocated. 
 *    memory     - a pointer to the memory to be managed, must be non 'NULL'
 *    size       - the size of the memory pool passed,, or to be allocated
 *    maxObjSz   - the maximum size for an object in the free list
 *    maxFreeListSz - the maximum number of objects in a free list
 *    minFreeListSz - the minimum number of objects in a free list
 *    
 *    Returns a MemHeapId to be used as a reference to a heap, or '0'.
 */
MemHeapId mem_createHeap( void *memory, u_int32 size, u_int32 maxObjSz,
                          u_int32 maxFreeListSz, u_int32 minFreeListSz) ;
/* 
 * DestroyHeap: destroy a heap, and all refrences from it
 *    heapId    - the Id of the heap to be destroyed
 *    error     - the error code returned by reference
 *
 *    Returns Success, or Failure.  DestroyHeap will
 *    not destroy a MemHeap with locked resource handles.
 */
int32 mem_destroyHeap( MemHeapId heapId );

/* 
 * PermitHeap: give a process permission to access the memory for a
 * specific heap.  If the heap has already been created, 'heapSize'
 * should be passed as a '0', (it will be calculated then.)
 */
int mem_permitHeap( MemHeapId heapId, int heapSize );


/* 
 * RegisterObjects:  Register a number of objects of a certain size to be added
 * to one of the free lists created for efficiency.
 *    count      - the number of objects to register 
 *    size       - the size of the objects to be register
 *
 *    Returns Success, or Failure.
 *
 * Usage: Assume you know you will need to allocate 'N' objects of type 'foo',
 * and 'M' objects of type 'bar'.  'M' and 'N' both need to be smaller than the
 * maxObjectSz passed on heap creation. Than you should make the calls:
 *    registerObjects( N, sizeof(foo) );
 *    registerObjects( M, sizeof(bar) );
 * 
 */
int32 mem_registerObjects( MemHeapId heapId, u_int32 count, u_int32 size );


/*  
 * CommitHeap: lock the "tuning" of the heap down, and prepare the heap to be
 * used as a general memory allocator.
 *    heapId     - the heap to be 'committed'
 * 
 *    Returns Success, or Failure.
 *
 * Usage: Before the heap is used generally as a memory allocator, (i.e. after
 * you have malloc'd all static memory, and made your registerObjects() calls.) 
 */
int32 mem_commitHeap( MemHeapId heapId );

/* Resets a heap reclaiming all uncommitted memory. */
void mem_resetHeap(MemHeapId heapId) ;

/* 
 * Malloc: Allocate a chunk of memory for use.
 *    heapId     - the heap to allocate from, or DEFAULT_HEAP
 *    size       - the size of the chunk of memory to allocate.
 *
 *    Returns NULL on Failure, or a valid pointer.
 */
#define ICTV_MEM_DEBUG 1
#ifndef ICTV_MEM_DEBUG
void *mem_malloc( MemHeapId heapId, u_int32 size );
#else
#define mem_malloc(heap, size) mem_malloc_debug((heap), (size), __FILE__, __LINE__)
void *mem_malloc_debug( MemHeapId heapId, u_int32 size, char *file, int line);
#endif

/* 
 * Calloc: Allocate a chunk of memory for use.
 *    heapId     - the heap to allocate from, or DEFAULT_HEAP
 *    num	 - Number of elements to allocate.
 *    size       - the size of an element.
 *
 *    Returns NULL on Failure, or a valid pointer. Set memory
 *    allocated to zero.
 */
#ifndef ICTV_MEM_DEBUG
void *mem_calloc( MemHeapId heapId, u_int32 num, u_int32 size );
#else
#define mem_calloc(heap, num, size) mem_calloc_debug((heap), (num), (size), __FILE__, __LINE__)
void *mem_calloc_debug( MemHeapId heapId, u_int32 num, u_int32 size,
    char *file, int line);
#endif
 
 
/* 
 * Realloc: Reallocate a chunk of memory to a different, typically larger size.
 *    heapId     - the heap to allocate from
 *    oldPtr     - the memory previously allocated
 *    size       - the new size of the chunk of memory to be pointed to by 
 *                 oldPtr, rounded up to a word.
 * 
 *    Returns NULL on Failure, or a valid pointer.
 */
void *mem_realloc( MemHeapId heapId, void *oldPtr, u_int32 size );


/* 
 * Free: Deallocate memory previously allocated.
 *    heapId     - the heap to free up the memory in
 *    oldPtr     - the memory to be freed
 * 
 *    Returns Success, or Failure.
 */
int32 mem_free( MemHeapId heapId, void *oldPtr );


/* 
 * CleanUp:  Free all the memory used by a process that is pointed
 * to by CellPtrs.  (There is currently no way to retrieve free list
 * chunks that a process doesn't free itself.)
 */
void mem_cleanUp( MemHeapId heapId, int32 procId );


#if defined(ICTVDEBUG) && defined(MEMCHECKING)
/* 
 * CheckMemoryUsed: Check for trashed memory.  Check free lists, by
 * comparing the free count, with the number of chunks actually on
 * the list.  Compare the CellPtr based memory by checking back
 * pointers, and by checking for a boundary value.
 */
int mem_checkMemoryUsed( MemHeapId heapId );

#endif

 
/* 
 * GetErrno: get the value of errno for the last MemHeap function
 * that was called.  (This value is only guranteed to be accurate
 * until the next call 'mem_' prefixed call, and only valid if the
 * functions return value indicated an error occured.)
 */
MemErrCode
mem_getErrno(void);

# define malloc(size) mem_malloc (DEFAULT_HEAP_ID, (size))
# define calloc(items, size) mem_calloc (DEFAULT_HEAP_ID, (items), (size))
# define realloc(omp, size) mem_realloc (DEFAULT_HEAP_ID, (omp), (size))
# define free(mp) mem_free (DEFAULT_HEAP_ID, (mp))

#endif    /* __HEAP_H__ */

#else
#	include "mem_malloc.h"
#endif /* _OS9000 */
