/*
** Copyright (C) 1994, ICTV Inc. 14600 Winchester Blvd., Los Gatos, CA 95030 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: ictv interactive television ictvMsg
** 
** File: ictvMsg.c - Provide basic message header accessor functions
**
**     $Id: ictvMsg.c,v 1.5 1995/11/04 02:04:12 wul Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/utility/ictvMsg.c,v $
**
** Author: Larry Rosenberg
**
** Purpose:
**      Provide functions for updating and retrieving information stored in
**      header of messages passed in the ictv network.
** 
** Functions:
** 
**     ictvMsgFromByteStream - convert from byte stream to message structure
**     ictvMsgGetClientRecvTime - get the time the client received response
**     ictvMsgGetClientSendTime - get the time the client sent request
**     ictvMsgGetMessageCode - extract the message code
**     ictvMsgGetServerRecvTime - get the time the server received response
**     ictvMsgGetServerSendTime - get the time the server sent response
**     ictvMsgGetStatus - extract the status
**     ictvMsgGetTransId - get the transaction id from header
**     ictvMsginitHeader - initialize structure
**     ictvMsgPrintHeader - debugging routine to display header
**     ictvMsgPrintServerTimes - debugging routine to display time server spent
**     ictvMsgSetClientRecvTime - set the time the client received response
**     ictvMsgSetClientSendTime - set the time the client sent request
**     ictvMsgSetConnection - set information related to who is talking to whom
**     ictvMsgSetMessageCode - set the message code in the header
**     ictvMsgSetServerRecvTime - set the time the server received response
**     ictvMsgSetServerSendTime - set the time the server sent response
**     ictvMsgSetStatus - update status to new value
**     ictvMsgSize - return the size of the message header structure in bytes
**     ictvMsgToByteStream - convert from message structure to byte stream
**     ictvMsgXlat - translate the header from host order to network order

** References:
**     See FrameMaker document Backoffice/ServerFormat.frame.
** 
** Last Modified: Tue Mar  7 16:14:55 1995
*/

#ifdef _SUN4
#   include <memory.h>          /* USING memset */
#else
#   include <string.h>          /* USING memset */
#endif

#include <stdlib.h>
#ifdef _OS9000
#   include <types.h>
#   include <INET/in.h>         /* USING htonl, ntohl */
#else
#   include <sys/types.h>
#   include <netinet/in.h>      /* USING htonl, ntohl */
#endif

#include "ictvMsg.h"
#include "timestamp.h"          /* USING TimeStamp_Xlat() */

#define IGNORE(x)    
#define XLAT(x)     x = (toNet ? htonl(x) : ntohl(x))
#define XLATs(x)    x = (toNet ? htons(x) : ntohs(x))

static void xlatServerMachineData (ServerMachineData_t *data, boolean toNet);

/****************************************************************************
 * void ictvMsgPrintHeader(ictvMessageHeader_t *header) 
 * Purpose:     Display output useful for debugging/tracing
 *
 * Parameters:  header - object to output
 *
 * Returns:     nothing
 ***************************************************************************/
#ifdef ICTVDEBUG
void ictvMsgPrintHeader(ictvMessageHeader_t *header)
{
    ServerTimingData_t *timing = &header->timingData;
    TimestampStr_t timeStr1, timeStr2;
    Timestamp_t ts;
    int dbgState;

    header->id[TRANSACTION_ID_STRING_LEN+1+5] = '\0';

    dbgState = dbg_getDebugState(DBG_GLOBAL_MASKINDEX);

    dbg_setDebugState(DBG_GLOBAL_MASKINDEX,dbgState | DBG_TERSE_BIT);

    GLOBAL_MSG(("Msg Transaction ID %s \n",(char *)header->id),
	       DBG_TRACE);

    GLOBAL_MSG(("Msg sent %s; server recv %s\n",
                TimeStamp_Str(&timing->clientSend, timeStr1, 6),
                TimeStamp_Str(&timing->serverRecv, timeStr2, 6)),
               DBG_TRACE);

    GLOBAL_MSG(("Server sent %s; client recv %s\n",
                TimeStamp_Str(&timing->serverSend, timeStr1, 6),
                TimeStamp_Str(&timing->clientRecv, timeStr2, 6)),
               DBG_TRACE);

    /* determine how long the server took to process the message */
    TimeStamp_Elapsed(&ts, &timing->serverRecv, &timing->serverSend);
    GLOBAL_MSG(("The server took %s to process the request.\n",
                TimeStamp_ElapsedStr(&ts, timeStr1, /* showFrac */ TRUE)),
               DBG_TRACE);

    /* determine how long the client waited for a response */
    TimeStamp_Elapsed(&ts, &timing->clientSend, &timing->clientRecv);
    GLOBAL_MSG(("The client waited %s for the response.\n",
                TimeStamp_ElapsedStr(&ts, timeStr1, /* showFrac */ TRUE)),
               DBG_TRACE);

    dbg_setDebugState(DBG_GLOBAL_MASKINDEX,dbgState);

} /* ictvMsgPrintHeader() */

/****************************************************************************
 * void ictvMsgPrintServerTimes(ictvMessageHeader_t *header) 
 * Purpose:     Display output useful for debugging/tracing server execution
 *
 * Parameters:
 *   header - data to parse
 *
 * Returns:     nothing
 ***************************************************************************/
void ictvMsgPrintServerTimes(ictvMessageHeader_t *header)
{
    ServerTimingData_t *timing = &header->timingData;
    TimestampStr_t timeStr;
    Timestamp_t ts;

    GLOBAL_MSG(("server recv msg at %s",
                TimeStamp_Str(&timing->serverRecv, timeStr, 6)),
               DBG_TRACE);

    GLOBAL_MSG(("server sent reply at %s\n",
                TimeStamp_Str(&timing->serverSend, timeStr, 6)),
               DBG_TRACE);

    /* determine how long the server took to process the message */
    TimeStamp_Elapsed(&ts, &timing->serverRecv, &timing->serverSend);
    GLOBAL_MSG(("The server took %s to process the request.\n",
                TimeStamp_ElapsedStr(&ts, timeStr, /* showFrac */ TRUE)),
               DBG_TRACE);
} /* ictvMsgPrintServerTimes() */
#endif

/****************************************************************************
 * void ictvMsgFromByteStream (ictvMessageHeader_t *header, void *result) 
 * Purpose:     Convert the message header from a string to structure
 *
 * Parameters:  
 *  header - translation placed here
 *  result - data to convert
 *
 * Returns:     nothing
 ***************************************************************************/
void ictvMsgFromByteStream (ictvMessageHeader_t *header, void *result) 
{
    memcpy(header, (const void *)result, sizeof(*header));
    ictvMsgXlat(header, /* toNet = */ FALSE);
} /* ictvMsgFromByteStream() */

/****************************************************************************
 * Timestamp_t ictvMsgGetClientRecvTime (ictvMessageHeader_t *header) 
 * Purpose:     Return the time the client received response 
 *
 * Parameters:  header - object to check
 *
 * Returns:     timestamp stored in header
 ***************************************************************************/
#ifndef ictvMsgGetClientRecvTime
Timestamp_t ictvMsgGetClientRecvTime (ictvMessageHeader_t *header)
{
    return header->timingData.clientRecv;
} /* ictvMsgGetClientRecvTime() */
#endif

/****************************************************************************
 * Timestamp_t ictvMsgGetClientSendTime (ictvMessageHeader_t *header) 
 * Purpose:     Return the time the client sent this message
 *
 * Parameters:  header - object to check
 *
 * Returns:     timestamp stored in header
 ***************************************************************************/
#ifndef ictvMsgGetClientSendTime
Timestamp_t ictvMsgGetClientSendTime (ictvMessageHeader_t *header)
{
    return header->timingData.clientSend;
} /* ictvMsgGetClientSendTime() */
#endif

/****************************************************************************
 * ServerMsgCode_t ictvMsgGetMessageCode(ictvMessageHeader_t *header) 
 * Purpose:     Extract message code from header
 *
 * Parameters:  header - object to extract data from
 *
 * Returns:     message code in header
 ***************************************************************************/
#ifndef ictvMsgGetMessageCode
ServerMsgCode_t ictvMsgGetMessageCode(ictvMessageHeader_t *header)
{
    return header->code;
} /* ictvMsgGetMessageCode() */
#endif

/****************************************************************************
 * Timestamp_t ictvMsgGetServerRecvTime (ictvMessageHeader_t *header) 
 * Purpose:     Return the time the server received this message
 *
 * Parameters:  header - object to check
 *
 * Returns:     timestamp stored in header
 ***************************************************************************/
#ifndef ictvMsgGetServerRecvTime
Timestamp_t ictvMsgGetServerRecvTime (ictvMessageHeader_t *header)
{
    return header->timingData.serverRecv;
} /* ictvMsgGetServerRecvTime() */
#endif

/****************************************************************************
 * char *ictvMsgGetTransId (ictvMessageHeader_t *header, char *transId) 
 * Purpose:     Extract transaction id from message header
 *
 * Parameters:  
 *  header - extract data from here
 *  transId - put results here
 *
 * Returns:     extracted value (in transId)
 ***************************************************************************/
#ifndef ictvMsgGetTransId
char *ictvMsgGetTransId (ictvMessageHeader_t *header, char *transId)
{
    return strcpy(transId, header->id);
} /* ictvMsgGetTransId() */
#endif

/****************************************************************************
 * Timestamp_t ictvMsgGetServerSendTime (ictvMessageHeader_t *header) 
 * Purpose:     Return the time the server sent this message
 *
 * Parameters:  header - object to check
 *
 * Returns:     timestamp stored in header
 ***************************************************************************/
#ifndef ictvMsgGetServerSendTime
Timestamp_t ictvMsgGetServerSendTime (ictvMessageHeader_t *header)
{
    return header->timingData.serverSend;
} /* ictvMsgGetServerSendTime() */
#endif

/****************************************************************************
 * ServerStatus_t ictvMsgGetStatus (ictvMessageHeader_t *header) 
 * Purpose:     Extract status from message header
 *
 * Parameters:  header - object to use
 *
 * Returns:     status from header
 ***************************************************************************/
#ifndef ictvMsgGetStatus
ServerStatus_t ictvMsgGetStatus (ictvMessageHeader_t *header)
{
    return header->status;
} /* ictvMsgGetStatus() */
#endif

/****************************************************************************
 * void ictvMsgInitHeader(ictvMessageHeader_t *header, ServerMsgCode_t code,
 *   rmsConnHndl conn) 
 * Purpose:     Initialize message header
 *
 * Parameters:  
 *  header - structure to initialize
 *  code - type of message being sent
 *  conn - connection used (NULL is ok)
 *
 * Returns:     nothing
 ***************************************************************************/
void ictvMsgInitHeader(ictvMessageHeader_t *header, ServerMsgCode_t code,
                       rmsConnHndl conn)
{
    memset(header, 0, sizeof(*header));
    header->code = code;
    if (conn) ictvMsgSetConnection(header, conn);
} /* ictvMsgInitHeader() */

/****************************************************************************
 * void ictvMsgSetClientRecvTime (ictvMessageHeader_t *header)
 * Purpose:     set the time server receives a message
 *
 * Parameters:  header - object to update
 *
 * Returns:     nothing
 ***************************************************************************/
#ifndef ictvMsgSetClientRecvTime
void ictvMsgSetClientRecvTime (ictvMessageHeader_t *header)
{
    TimeStamp_Current(&header->timingData.clientRecv);
} /* ictvMsgSetClientRecvTime() */
#endif

/****************************************************************************
 * void ictvMsgSetClientSendTime (ictvMessageHeader_t *header)
 * Purpose:     set the time client sends a message
 *
 * Parameters:  header - object to update
 *
 * Returns:     nothing
 ***************************************************************************/
#ifndef ictvMsgSetClientSendTime
void ictvMsgSetClientSendTime (ictvMessageHeader_t *header)
{
    TimeStamp_Current(&header->timingData.clientSend);
} /* ictvMsgSetClientSendTime() */
#endif

/****************************************************************************
 * void ictvMsgSetConnection (ictvMessageHeader_t *header, rms_ConnHndl *conn) 
 * Purpose:     Set the connection information for the header
 *
 * Parameters:  
 *   header - object to update
 *   conn - connection to get local & remote addresses
 *
 * Returns:     none 
 ***************************************************************************/
void ictvMsgSetConnection (ictvMessageHeader_t *header, rmsConnHndl conn)
{
    if (conn == NULL) return;
    header->client.addr.ipAddress = conn->localAddress.addr.ip.addr;
    header->client.addr.port = conn->localAddress.addr.ip.port;
    header->server.addr.ipAddress = conn->remoteAddress.addr.ip.addr;
    header->server.addr.port = conn->remoteAddress.addr.ip.port;
} /* ictvMsgSetConnection() */

/****************************************************************************
 * void ictvMsgSetMessageCode(ictvMessageHeader_t *header, ServerMsgCode_t request) 
 * Purpose:     Set the message code in the header
 *
 * Parameters:  
 *  header - object to update
 *  request - new value for message code
 *
 * Returns:     nothing
 ***************************************************************************/
#ifndef ictvMsgSetMessageCode
void ictvMsgSetMessageCode(ictvMessageHeader_t *header, ServerMsgCode_t request) 
{
    header->code = request;
} /* ictvMsgSetMessageCode() */
#endif

/****************************************************************************
 * void ictvMsgSetServerRecvTime (ictvMessageHeader_t *header)
 * Purpose:     set the time server receives a message
 *
 * Parameters:  header - object to update
 *
 * Returns:     nothing
 ***************************************************************************/
#ifndef ictvMsgSetServerRecvTime
void ictvMsgSetServerRecvTime (ictvMessageHeader_t *header)
{
    TimeStamp_Current(&header->timingData.serverRecv);
} /* ictvMsgSetServerRecvTime() */
#endif

/****************************************************************************
 * void ictvMsgSetServerRecvTime (ictvMessageHeader_t *header)
 * Purpose:     set the time server receives a message
 *
 * Parameters:  header - object to update
 *
 * Returns:     nothing
 ***************************************************************************/
#ifndef ictvMsgSetServerSendTime
void ictvMsgSetServerSendTime (ictvMessageHeader_t *header)
{
    TimeStamp_Current(&header->timingData.serverSend);
} /* ictvMsgSetServerSendTime() */
#endif

/****************************************************************************
 * void ictvMsgSetStatus (ictvMessageHeader_t *header, ServerStatus_t stat) 
 * Purpose:     Update status for message header
 *
 * Parameters:  
 *  header - object to update
 *  stat - new status value
 *
 * Returns:     nothing
 ***************************************************************************/
#ifndef ictvMsgSetStatus
void ictvMsgSetStatus (ictvMessageHeader_t *header, ServerStatus_t stat)
{
    header->status = stat;
} /* ictvMsgSetStatus() */
#endif

/****************************************************************************
 * size_t ictvMsgSize (ictvMessageHeader_t *header) 
 * Purpose:     return the size (in bytes) of an ictv message header
 *
 * Parameters:  header - object to take size of
 *
 * Returns:     size of header in bytes 
 ***************************************************************************/
#ifndef ictvMsgSize
size_t ictvMsgSize (ictvMessageHeader_t *header)
{
    return sizeof(*header);
} /* ictvMsgSize() */
#endif

/****************************************************************************
 * void ictvMsgToByteStream (ictvMessageHeader_t *header, void *result) 
 * Purpose:     Convert the message header to a string
 *
 * Parameters:  
 *  header - data to convert
 *  result - translation placed here
 *
 * Returns:     none (see result)
 ***************************************************************************/
void ictvMsgToByteStream (ictvMessageHeader_t *header, void *result) 
{
    memcpy(result, (const void *)header, sizeof(*header));
    ictvMsgXlat( (ictvMessageHeader_t *) result, /* toNet = */ TRUE);
} /* ictvMsgToByteStream() */

/****************************************************************************
 * void ictvMsgXlat(ictvMessageHeader_t *header, Bool toNet) 
 * Purpose:     Provide network <--> host order translation
 *
 * Parameters:  header - object to translate
 *
 * Returns:     nothing
 ***************************************************************************/
void ictvMsgXlat(ictvMessageHeader_t *header, Bool toNet)
{
    XLAT(header->code);
    /* transport layer */
    xlatServerMachineData(&header->client, toNet);
    xlatServerMachineData(&header->server, toNet);
    XLAT(header->serverVersion);
    XLATs(header->serverType);
    /* packet layer */
    XLAT(header->packetType);
    XLAT(header->seqNum);
    /* timestamp layer */
    TimeStamp_Xlat(&header->timingData.clientSend, toNet);
    TimeStamp_Xlat(&header->timingData.clientRecv, toNet);
    TimeStamp_Xlat(&header->timingData.serverSend, toNet);
    TimeStamp_Xlat(&header->timingData.serverRecv, toNet);
    /* transaction layer */
    IGNORE(header->id);
    /* application layer */
    XLAT(header->usage);
    XLAT(header->status);
    XLAT(header->priority);
    XLAT(header->messageLength);
} /* ictvMsgXlat() */

                           /********************
                           ** LOCAL FUNCTIONS **
                           ********************/

/****************************************************************************
 * static void xlatServerMachineData (ServerMachineData_t *data, boolean toNet)
 * Purpose:     Provide host <-> network translation for ServerMachineData
 *
 * Parameters:  
 *  data - what to translate
 *  toNet - direction of translation
 *
 * Returns:     nothing
 ***************************************************************************/
static void xlatServerMachineData (ServerMachineData_t *data, boolean toNet)
{
    XLAT(data->addr.ipAddress);
    XLATs(data->addr.port);
} /* xlatServerMachineData() */


