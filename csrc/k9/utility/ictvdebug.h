/* 
   Author: BDN.
   Date 2/20/94.
**
** Permission to copy given to and distribute give to Lapel Software 1997.

** $Id: ictvdebug.h,v 1.32 1995/11/04 02:04:14 wul Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/utility/ictvdebug.h,v $

** This facility was developed with four goals in mind.  1.)
** Portability.  2.) Desire to support multiple modules each with
** orthogonal levels of debug code. The controlling app can
** dynamically specify different debug levels for different
** modules. Additionally there is global control.  3.) Simplicity of
** use.  4.) Ability to compile all debug references away.

** ICTVDEBUG should be defined or all macros within will expand to
** NULL.  32 private debug flags are available per app. 4 hierarchical
** levels of global debug flags have been defined as well.

   QUICK USAGE EXAMPLE:

** First to use this package one needs to set the appropriate
** debug level for each mask. The default is DBG_ASSERT. That is only
** dire messages are printed out. If this is not the behavior that is
** needed change it with the following call.
**
** dbg_setDebugState(DBG_APP_MASKINDEX,DBG_WARN) ;
**
** For ease of use 4 debug levels have been defined, each more severe
** than the next: DBG_MISC_LEVEL, DBG__TRACE_LEVEL, DBG_WARN_LEVEL, and
** DBG_ASSERT_LEVEL. An application can use  the mask index in other
** ways as long as the first 4 bits of the mask are not touched.

** Debug messages can be printed using the following macros: 
**
** DEBUG_MSG((debug_buffer,"foobar is %x.",foobar),DBG_MEM_MASKINDEX,DBG_WARN,DBG_WARN) ; 
**
** 
**
** LOCAL_MSG(("foobar is %x.",foobar),DBG_MEM_MASKINDEX,DBG_WARN) ;
**
** APP_MSG(("foobar is %x.",foobar),DBG_WARN) ;

** GLOBAL_MSG is like DEBUG_MSG but uses the GLOBAL_MASKINDEX and sets
** the LOCAL_MASK to DBG_ALL. LOCAL_MSG is just an abbreviation for 
** the above idiomatic use of DEBUG_MSG. APP_MSG is like LOCAL_MSG
** where the maskindex is always DBG_APP_MASKINDEX.
**
** Use LOCAL_MSG for libraries. Use APP_MSG for files which get linked
** directly into the executable. Use GLOBAL_MSG in either libraries or
** files linked directly into executables for "more important" types of
** messages. In this way an application can set the global debug level to
** DBG_MISC and the specific local masks and app mask to DBG_WARN or 
** DBG_ASSERT.
**
** In this example the first argument is used by printf, the second
** argument indicates which local mask index to reference. The third
** argument indicate the global mask level. If the logical AND of
** third argument and the current value of the global mask index is 0
** then the message is not printed. Finally the last argument is used
** to indicate the local mask level.  I have found that the idiom of
** setting the global mask level and the local mask level to the same
** mask is in general the most useful and covers most purposes.  
**
** Finally, a predicate: dbg_maskDebugp(DBG_APP_MASKINDEX,DBG_WARN,DBG_WARN)
** is available. The user may want to wrap its use by an ifdef/endif
** ICTVDEBUG.

   USAGE

** There are two external functions and three message macros. If
** local debug levels are required then a private <module|lib
** name>_debug.h should be created that defines up to 32 bit debug
** classes as bit fields in a 32 bit word.  Additionally this file
** needs to be updated to include a DBG_<module>_MASKINDEX value and
** the DBG_NUMBER_MASKS must be incremented. Alternatively one
** <appname>_debug.h file can define all the indices for each library
** or module and define the DBG_NUMBER_MASKS field before the
** ictvdebug.h file is included.

** Once defined the macro DEBUG_MSG can be used to print arbirtrary
** messages to stderr. The current debug level for and given index can
** be set with the dbg_setDebugState() function. The function
** dbg_maskDebugp() returns 1 if the current debug level is active and
** 0 otherwise. Code which uses this function should be wrapped by
** #ifdef ICTVDEBUG ... #endif preprocessor blocks.

** DEBUG_MSG(pargs,maskindex,globalflag,localflag):
**
**  pargs is of the form ("foobar: %s%x.\n",foobar,foobar).
**
** maskindex specifies to which module, library or app this MSG
** applies.  DBG_GLOBAL_MASKINDEX is used to specify the global mask.
** DBG_APP_MASKINDEX is used to specify the application's mask.
**
** globalflag is a 32 bit field indicating a global debug level for
** this MSG.  
**
** localflag is a 32 bit field indicating a local debug level for this MSG.

** void dbg_setDebugState( int maskindex, int state ) ; 
**
** Used to set the current debug level for the module, library or app
** using maskindex.

** int dbg_maskDebugp( int maskindex, int globalflag, int localflag ) ;

** Returns 1 if the globalflag and localflag specified are currently
** set for the module,library or app, using maskindex. 0 is otherwise
** returned.  The state that globalflag and localflag are compared
** against is that set by the dbg_setDebugState call.

** The default global state is DBG_ASSERT. In this state only dire
** debug clauses will execute. The local states are initialized to
** DBG_NONE. The global state has a special bit,
** DBG_OVERRIDE_BIT. When set, only the globalflag is used by
** dbg_maskDebugp(). In this way all local flags can be ignored.
** If the override bit is or'd into the gmask then whenever override
** is specified the message print regardless of the gmask normally
** used.

*/

/* Example Usage: 

*/

#ifndef _ICTVDEBUG_H
#define _ICTVDEBUG_H

/* The reason for a GLOBAL and an APP mask is that libraries may want
** to print out regardless of the setting of the local mask. This is
** probably a subtle enought difference to never be taken advantage of.
*/
#define DBG_GLOBAL_MASKINDEX   0
#define DBG_APP_MASKINDEX      1
#define DBG_AVGSM_MASKINDEX    2
#define DBG_MPEG_MASKINDEX     3
#define DBG_STAGING_MASKINDEX  4
#define DBG_MEM_MASKINDEX      5
#define DBG_MAL_MASKINDEX      6
#define DBG_DMA_MASKINDEX      7
#define DBG_K9EXT_MASKINDEX    8
#define DBG_PPF_MASKINDEX      9
#define DBG_RDBMSG_MASKINDEX  10
#define DBG_BOGW_MASKINDEX    11
#define DBG_PME_MASKINDEX     12
#define DBG_DBF_MASKINDEX     13
#define DBG_CFG_MASKINDEX     14
#define DBG_DVP_MASKINDEX     15
#define DBG_MMAN_MASKINDEX    16
#define DBG_VGL_MASKINDEX     17
#define DBG_MMCDEV_MASKINDEX  18
#define DBG_SEXP_MASKINDEX    19
#define DBG_USECASE_MASKINDEX 20

/* HOW TO ADD NEW MASKINDICES  */
/* 1) Add Name to list to above. Take the next available number */
/* 2) Increment DBG_LAST_MASKINDEX by one. */

#define DBG_LAST_MASKINDEX    21


/* Duplicate definitions */
#define DBG_RES_MASKINDEX      DBG_MAL_MASKINDEX

/* As different libraries appear they need to claim a mask index. 
   This is controllable by cvs. */

#ifndef DBG_NUMBER_MASKS
/* 1 global mask + the number of private masks. */
#define DBG_NUMBER_MASKS DBG_LAST_MASKINDEX
#endif

/* inital max number of masks - this can be increased*/
#define DBG_MAX_MASKS    32


#define DBG_ALL     0xFFFFFFFF
#define DBG_NONE    0x0

/* Global to all users. These flags are to be used to classify debug
   messages on a per app/library basis in increasing order of severity.
   DBG_OVERRIDE_BIT is specified when the value of the local mask should be
   ignored. Bits 4 thru 30 are available for specialized use. */

#define DBG_MISC    0x8
#define DBG_TRACE   0x4
#define DBG_WARN    0x2
#define DBG_ASSERT  0x1

#define DBG_ASSERT_LEVEL    DBG_ASSERT
#define DBG_WARN_LEVEL      DBG_ASSERT | DBG_WARN
#define DBG_TRACE_LEVEL     DBG_WARN_LEVEL | DBG_TRACE
#define DBG_MISC_LEVEL      DBG_TRACE_LEVEL | DBG_MISC

/* This bit if set indicates to ignore the local mask. */

/* No longer used. */
#define DBG_OVERRIDE_BIT    0x00000000
/* Controls whether or not the description \(where\) line 
 * is printed. */
#define DBG_TERSE_BIT       0x40000000

#define LOCAL_MSG(pargs,maskindex,flag) DEBUG_MSG(pargs,maskindex,flag,flag)

#define GLOBAL_MSG(pargs,flag) \
                      DEBUG_MSG(pargs,DBG_GLOBAL_MASKINDEX,flag,DBG_ALL)

#define APP_MSG(pargs,flag) DEBUG_MSG(pargs,DBG_APP_MASKINDEX,flag,flag)

/*
 function macros
 */
#define LOCAL_FUN(fun,maskindex,flag) DEBUG_FUN(fun,maskindex,flag,flag)
#define GLOBAL_FUN(fun,flag) DEBUG_FUN(fun,DBG_GLOBAL_MASKINDEX,flag,DBG_ALL)
#define APP_FUN(fun,flag) DEBUG_FUN(fun,DBG_APP_MASKINDEX,flag,flag)

#ifdef ICTVDEBUG


#define DEBUG_MSG(pargs,maskindex,globalflag,localflag)                 \
            if ( dbg_maskDebugp(maskindex,globalflag,localflag) ) {     \
            Do_DebugMsg(__LINE__,__FILE__,maskindex,PRINTF pargs );\
                                      }
/* interface for general functions on debug level*/
#define DEBUG_FUN(fun,maskindex,globalflag,localflag)               \
            if (dbg_maskDebugp (maskindex,globalflag,localflag))    \
            {                                                       \
                fun;                                                \
            }


/* What the hell keep it around. BDN. */
#ifdef __cplusplus
extern "C" {
#endif

  /* If the specified debug state has been set this returns 1, otherwise 0. */
  int dbg_maskDebugp( int maskindex, int globalflag, int localflag ) ;

  /* Set debug state to one of the above flags.*/
  void dbg_setDebugState( int maskindex, int state ) ;
  int dbg_getDebugState (int index);

  /* set state for all masks*/
  void dbg_MaskInit ( int , int , int );


  int debug_SetUp( char* progname, int usePIDinProgTag );
  void debug_SetProgTag(char *name, char appendPID);
  char* debug_GetProgTag(void);
  void Do_DebugMsg(int, char * , int ,char * );

  void debug_initAllMasks(int state) ;
#ifndef _OS9000
  int debug_Open(int fd);
  int debug_filed(void);
#endif /* #ifdef _OS9000 */

/* helper functions, NOT part of API*/
void Do_LogMsg(int, int, int,char *);
char * PRINTF(char *,...);

#ifdef __cplusplus
}
#endif

#else               /* ifdef ICTVDEBUG */

/*
 * If debugging is not defined DEBUG_MSG is a noop i.e. a null statement
 */
#define DEBUG_MSG(pargs,index,flag1,flag2)
#define DEBUG_FUN(fun,index,flag1,flag2)
#define dbg_MaskInit ( arg1 , arg2 , arg3 ) 
#define dbg_setDebugState(index,i)
#define dbg_maskDebugp(index,flag1,flag2)  (0)
#define debug_SetProgTag(arg1, arg2)
#define debug_GetProgTag()                 (0)
#define dbg_end() 0;
#define debug_initAllMasks(state)
#ifndef _OS9000
    #define debug_Open(arg) 0;
    #define debug_filed() 0;
#endif /* #ifdef _OS9000 */

#endif              /* #ifdef ICTVDEBUG */

#endif              /* #ifndef _ICTVDEBUG_H */

