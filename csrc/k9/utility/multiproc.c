/*
** Copyright (C) 1994
** ICTV Inc. 14600 Winchester Blvd., Los Gatos, CA 95030 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: ictv interactive television OS9000 Extensions
** 
** File: MultiProc.c - run several processes from a single image
**     $Id: multiproc.c,v 1.4 1995/04/05 22:23:44 luke Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/utility/multiproc.c,v $
**
** Author(s):
**    Aylwin Stewart
**
** Purpose:
**     Provides a template for users to create a single executable
**     which forks itself several times to run different processes.
**     The primary goal of this is a reduction in code size for a
**     system because libraries will not be included separately in
**     each executable which is built.
** 
** Types/Classes:
**     MainId - associates a process with a string identifier
** 
** Functions:
**     mpFork     - fork a process whose main is determined by its
**                  first arg, which is a MainId.string
**     runMain    - determines which "main" should actually be executed
**     
**     freeMemory - clean up memory on exit
** 
** Usage:
** 
** Notes:
**     masterMain spawns processes in the order of their
**     entry into the Procs table.
**
** Assumptions:
** 
** 
** References:
**     See code in mmcinit.c for an example.
** 
*/

#include <types.h>
#include <string.h>
#include "ictvdebug.h"
#include "ictvcommon.h"
#include "k9fork.h"
#include "MemHeap.h"

#include "multiproc.h"

# define __MULTIPROC_C__

/*****                     M A C R O S                     *****/

/* 
 * MAX_ARGS: the maximum number of arguments.
 */
#define MAX_ARGS            32


/*****                      E N U M S                      *****/


/*****            D A T A   S T R U C T U R E S            *****/


/*****                    G L O B A L S                    *****/                  

/* 
 * Argv: a global copy of the argv passed plus an identity string
 * at mp_Argv[1].
 */
char *mp_Argv[MAX_ARGS + 1];



#if 0  /* A template for a MainsTable */
/* 
 * Procs: the table of processes for this multiprocess to exec.
 */
MainId Procs[] = {
    { NULL, &masterMain },
    { "1Process string", &foo1Main },
    { "2Process string", &foo2Main },
    { "3Process string", &foo3Main },
    { "4Process string", &foo4Main },
    { "5Process string", &foo4Main }
};
#endif 

/*****       F U N C T I O N   D E F I N I T I O N S       *****/

/*
 * Fork: exec a process with an identifying string
 */
int32 
mp_fork( char *idString, char **args, process_id *childId,
         int paths, u_int16 priority )
{
    int32 i;
    error_code error;
    
    mp_Argv[0] = args[0];
    mp_Argv[1] = idString;
    
    for ( i = 1; args[i]; i ++ )
        mp_Argv[i + 1] = args[i];
    
    error = k9_fork( mp_Argv, childId, paths, priority );
    return error;
}


/* 
 * RunMain: determine which process should actually be run and start
 * its main_p function.
 */
int32
mp_runMain( int32 argc, char *argv[], MainId procTable[], int32 numProcs )
{
    int32 i, j;
    Bool entryFound = NO;

    
    /* This is a short cut since if the argc is less than two,     */
    /* than the process couldn't have been spawned as a MultiProc. */
    if ( argc == 1 ) 
        return  (*procTable[0].main_p)( argc, argv );
    
    /* Search for the main_p to call for this process */
    for( i = 1; i < numProcs; i++ ) {
        if ( ! strcmp( argv[1], procTable[i].string ) ) {
	    for( j = 1; j < argc; j++ )
	        argv[j] = argv[j + 1]; 
	    argc--;
	    entryFound = YES;
	    break;
	}
    }
    
    if ( ! entryFound ) 
        i = 0;
        
    return  (*procTable[i].main_p)( argc, argv );
}


/* 
 * FreeMemory: clean up memory at exit
 */
void
mp_freeMemory(void)
{
    mem_cleanUp( DEFAULT_HEAP_ID, getpid() );
    
    return;
}

