/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, CA 95050-4320
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ICTV Library
**
** File:  atox.h - convert an ASCII string to a hex value
**     $Id: atox.h,v 1.2 1994/11/02 23:21:19 dougf Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/utility/atox.h,v $
**
** Author:  Doug Fuhriman
**
** Purpose:
**    To provide a conversion from ASCII to hexadecimal   
**
** Types/Classes:
**
** Functions:
**    atox
**
** Assumptions:
**
** Limitations:
**
** Notes:
**
** References:
**
*/

#ifndef __ATOX_H__
#define __ATOX_H__


/*
 * include files
 */



/*
 * public function prototypes
 */

#ifdef __cplusplus
    extern "C" {
#endif

int
atox(char* str);

#ifdef __cplusplus
    }
#endif


#endif /* __ATOX_H__ */
