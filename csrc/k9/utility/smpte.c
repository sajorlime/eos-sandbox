
/*
** COPYRIGHT (C) 1994, 1995
** ICTV Inc.
** 14600 Winchester Blvd.
** Los Gatos, CA 95030
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: Utility Library
**
** File: smpte.c - 
**
** Author: jmt, emil, aylwin
**
** Purpose:
**  Defines the smpte interface.
**
** Constants:
**  SMPTENow -- globally available now value;
**  SMPTEZero -- globally available zero value.
** Functions:
**
** Usage:
**
** Notes:
**
** $Id: smpte.c,v 1.15 1995/11/04 02:04:16 wul Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/utility/smpte.c,v $
**
*/

/*
    Implementation for SMPTE utilities

    Change history:
        8-24-94    jmt    First checked in.

    Things to do:
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "smpte.h"

# define __SMPTE_C__

/*
    Utilities for SMPTE
*/
SMPTEtime SMPTENow = {SMPTE_NOW};
SMPTEtime SMPTEZero = {0};
SMPTEtime SMPTEForever = {SMPTE_FOREVER};

SMPTETime 
SMPTEAdd (SMPTETime a, SMPTETime b)
{
    u_int32 frames = a.hmsf.f + b.hmsf.f;
    u_int32 seconds = a.hmsf.s + b.hmsf.s;
    u_int32 minutes = a.hmsf.m + b.hmsf.m;
    u_int32 hours = a.hmsf.h + b.hmsf.h;

    if (frames >= 30)
    {
        seconds++;
        frames -= 30;
    }

    if (seconds >= 60)
    {
        minutes++;
        seconds -= 60;
    }

    if (minutes >= 60)
    {
        hours++;
        minutes -= 60;
    }

# if 0
    /* we don't want clocks to roll for now */
    if (hours >= 24)
        hours -= 24;
# endif

    a.value = frames | (seconds << 8) | (minutes << 16) | (hours << 24);
    return a;
}

SMPTETime
SMPTESubtract (SMPTETime a, SMPTETime b)
{
    int32 frames = a.hmsf.f - b.hmsf.f;
    int32 seconds = a.hmsf.s - b.hmsf.s;
    int32 minutes = a.hmsf.m - b.hmsf.m;
    int32 hours = a.hmsf.h - b.hmsf.h;

    if (frames < 0)
    {
        seconds--;
        frames += 30;
    }
    if (seconds < 0)
    {
        minutes--;
        seconds += 60;
    }
    if (minutes < 0)
    {
        hours--;
        minutes += 60;
    }
    if (hours < 0)
        hours += 24;

    a.value = frames | (seconds << 8) | (minutes << 16) | (hours << 24);
    return a;
}

SMPTETime MSToSMPTE(u_int32 ms)
{
    SMPTEtime retVal;
    retVal.value =
        (ms / (1000*60*24)) << 24 | (ms / (1000*60)) << 16
        | (ms / 1000) << 8 | (ms * 33 / 1000);
    return retVal;
}

u_int32 SMPTEToMS(SMPTETime time)
{
    u_int32 smpte = time.value;
    return (smpte >> 24) * (1000*60*24)
        + (smpte & 0xff0000 >> 16) * (1000*60)
        + (smpte & 0xff00 >> 8) * 1000 + (smpte & 0xff) * 33;
}

char * 
sprintSMPTE (char * buffer, SMPTETime time)
{
    if (time.value == SMPTE_NOW)
        sprintf (buffer, " SMPTE NOW  ");
    else
        sprintf
        (
            buffer, "%02i:%02i:%02i:%02i",
            time.hmsf.h, time.hmsf.m, time.hmsf.s, time.hmsf.f
        );
    return buffer;
}

void
writeSMPTE (SMPTETime time, FILE * stream)
{
    char buffer[16];

    sprintSMPTE (buffer, time);
    fputs (buffer, stream);
    fflush (stream);
}

void 
PrintSMPTE (SMPTETime time)
{
    writeSMPTE (time, stderr);
}
 

SMPTETime 
GregorianToSMPTE (int time, int ticks)
{
    SMPTEtime retVal;
    long rate = (ticks & 0xffff0000) >> 16;
    ticks &= 0x0000ffff;

    retVal.value = (time << 8) + ((30 * ticks) / rate);
    return retVal;
}


SMPTEtime
strToSMPTE( char *smpteString )
{
    int tmp;
    SMPTEtime theTime = {0};
    char *minutes, *seconds;
    char *frames = strrchr (smpteString, ':');

    if (frames)
    {
        *frames = '\0';
        theTime.hmsf.f = atoi (++frames);

        seconds = strrchr (smpteString, ':');
        if (seconds)
        {
            *seconds = '\0';
            theTime.hmsf.s = atoi (++seconds);

            minutes =
                strrchr (smpteString, ':');
            if (minutes)
            {
                *minutes = '\0';
                theTime.hmsf.m = atoi (++minutes);

                theTime.hmsf.h = atoi (smpteString);
            }
            else
                theTime.hmsf.m = atoi (smpteString);
        }
        else
            theTime.hmsf.s = atoi (smpteString);
    }
    else
        theTime.hmsf.f = atoi (smpteString);

    if ((tmp = theTime.hmsf.f / FRAMES_PER_SECOND) > 0)
    {
        theTime.hmsf.s += tmp;
        theTime.hmsf.f -= tmp * FRAMES_PER_SECOND;
    }

    if ((tmp = theTime.hmsf.s / SECONDS_PER_MINUTE) > 0)
    {
        theTime.hmsf.m += tmp;
        theTime.hmsf.s -= tmp * SECONDS_PER_MINUTE;
    }

    if ((tmp = theTime.hmsf.m / MINUTES_PER_HOUR) > 0)
    {
        theTime.hmsf.h += tmp;
        theTime.hmsf.m -= tmp * MINUTES_PER_HOUR;
    }

    return theTime;
}



SMPTEtime 
framesToSMPTE (u_int frames)
{
    SMPTEtime theTime;
    
    theTime.hmsf.h = frames / FRAMES_PER_HOUR;
    frames -= (theTime.hmsf.h * FRAMES_PER_HOUR);
    
    theTime.hmsf.m = frames / FRAMES_PER_MINUTE;
    frames -= (theTime.hmsf.m * FRAMES_PER_MINUTE);

    theTime.hmsf.s = frames / FRAMES_PER_SECOND;
    frames -= (theTime.hmsf.s * FRAMES_PER_SECOND);

    theTime.hmsf.f = frames;

    return theTime;
}


u_int
smpteToFrames (SMPTEtime time)
{
    u_int frames;
    

    frames = time.hmsf.h * FRAMES_PER_HOUR;
    frames += time.hmsf.m * FRAMES_PER_MINUTE;
    frames += time.hmsf.s * FRAMES_PER_SECOND;
    frames += time.hmsf.f;

    return frames;
}

