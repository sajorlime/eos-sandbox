/*
** Copyright (C) 1994, ICTV Inc. 14600 Winchester Blvd. Los Gatos, CA 95030.
** All rights reserved.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television
** 
** Description: iassert: Ictv assert implementation
** 
** $Id: iassert.h,v 1.2 1995/01/06 07:16:09 george Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/utility/iassert.h,v $
** 
** Purpose:
**      Provide a true assert macro that actually dumps core and doesn't
**      store the same string in data space each time it's used.
**
** $Log: iassert.h,v $
** Revision 1.2  1995/01/06 07:16:09  george
** Added header and C++ guards.
**
** Revision 1.1  1995/01/06  06:40:19  george
** Added iassert to support vodServer.
**
**/

#ifndef __IASSERT_H__
#define __IASSERT_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Define NO_IASSERT to turn off assertions.  Note: if you do this your
   code must handle fall-through cases. */

#ifdef NO_IASSERT
#       define iassert(ex)
#       define _iassert(a,b)
#else
#       define iassert(ex) {if (!(ex)) _iassert(__FILE__, __LINE__);}

void _iassert(char         *pModule,
              unsigned int lineNo);
#endif


#ifdef __cplusplus
}
#endif

#endif  /* __IASSERT_H__ */
