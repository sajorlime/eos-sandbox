/*
**
** Project: Utility Library
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** File: .../libsrc/utility/argsget.c
**
** Author: emil
** $Author: wul $
**
** Purpose:
**  Code to get the command line options according to a string.
**
** Class/Typedefs:
**
** Functions:
**  arg_OptGet -- gets an option that matches the string an option.
**
**  arg_OptRemove -- removes all remaining option from the arg list,
**  and copy them (optionally) to a string.
**
**  arg_OptAdjust -- compresses the options after OptGet, OptRemove
**  and Next have depleted the arg list.
**
**  arg_Next -- get the next argument nulls string in the list.
**
**  arg_Last -- get the last argument and nulls string in the list.
**
**  arg_Copy -- copies agument list to string.
**
** Staic Functions:
**  kmatch -- return the length of a beging match or a key string.
**
** Notes:
**
** $Id: argsget.c,v 1.12 1995/11/04 02:04:10 wul Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/utility/argsget.c,v $
**
*/


# include <string.h>
# include <stdio.h>
# include <stdlib.h>
# include <ctype.h>

# define NO_ARGSGET_INLINE
# include "argsget.h"

# define __ARGSGET_C__
/*
** Return the length of the match a the begining of keyp and ssp.
*/
static int
kmatch (char * keyp, char * ssp)
{
    int len;
    int i;
    
    if (!((int) keyp & (int) ssp))
        return 0;
    if (!(*keyp & *ssp))
        return 0;
    i = len = strlen (keyp);
    while (i-- > 0)
        if (*keyp++ != *ssp++)
            return 0;
    return len;
}


/*
**  arg_OptGet -- gets an option that matches the string an option.
*/
char *
arg_OptGet (int argc, char ** argv, char * opt, int copy)
{
    int i;
    int j;
    int len;
    char b;

    for (i = 1; i < argc; i++)
    {
        j = i;
        if ((len = kmatch (opt, argv[i])) != 0)
        {
            if (argv[i][len] != 0)
            {
                if (copy > 0)
                {
                    strncpy (opt, &argv[i++][len], copy);
                    opt[copy-1] = 0;
                    argv[j] = 0;
                }
                else
                    copy--;
            }
            else
            {
                if (argv[i + 1] && (argv[i + 1][0] != '-'))
                {
                    if (copy > 0)
                    {
                        strncpy (opt, argv[++i], copy);
                        argv[j] = 0;
                        opt[copy-1] = 0;
                        argv[i++] = 0;
                    }
                    else
                        copy--;
                }
                else
                {
                    if (copy)
                        *opt = 0;
                    argv[i] = 0;
                }
            }
            return opt;
        }
    }
    if (!copy) /* check for boolean flags in a list of boolean options */
    {
        if ((j = strlen (opt)) <= 2) /* it is a single character option */
        {
            b = opt[j - 1];
            for (i = 1; i < argc; i++)
            {
                if (argv[i] && (argv[i][0] == '-'))
                {
                    if (strchr (argv[i], b))
                        return opt;
                }
            }
        }
    }
    return 0;
}

/*
** arg_OptRemove -- takes removes the any remainging options for the
** arg list. It 
*/
int
arg_OptRemove (int argc, char ** argv, char * optList)
{
    int i;
    int cnt;

    if (optList)
        *optList = 0;
    for (cnt = 0, i = 1; i < argc; i++)
    {
        if (argv[i] && (argv[i][0] == '-'))
        {

            if (optList)
                strcat (optList, argv[i]);
            argv[i] = 0;
            cnt++;
        }
    }
    return cnt;
}


/*
**  arg_OptAdjust -- compresses the options after OptGet, OptRemove
*/
int
arg_OptAdjust (int argc, char ** argv)
{
    int i;
    int j;
    int cnt;

    /* init cnt, and find first hole */
    for (cnt = i = 1; i < argc; i++)
    {
        if (argv[i])
            cnt++;
        else
            break;
    }
    /* for the rest of the list copy up,
    ** if we find more holes skip them */ 
    for (j = i++; i < argc; i++)
    {
        if (argv[i])
        {
            argv[j++] = argv[i];
            cnt++;
        }
    }
    return cnt;
}




/*
**  arg_Next -- get the next none null string in the list.
*/
char *
arg_Next (int argc, char ** argv)
{
    int i;
    char * cp;

    for (i = 1; i < argc; i++)
    {
        if ((cp = argv[i]) != 0)
        {
            argv[i] = 0;
            return cp;
        }
    }
    return (char *) 0;
}




/*
**  arg_Last -- get the last argument and null string in the list.
*/
char *
arg_Last (int argc, char ** argv)
{
    int i;
    char * cp;

    for (i = argc - 1; i > 0; i--)
    {
        if ((cp = argv[i]) != 0)
        {
            argv[i] = 0;
            return cp;
        }
    }
    return (char *) 0;
}

char *
arg_Position (int pos, char ** argv)
{
    char * cp;

    if ((cp = argv[pos]) != 0)
    {
        argv[pos] = 0;
    }
    return cp;
}



char *
arg_Copy (int argc, char ** argv, char * clp, int n)
{
    int i;
    int len;

    if (!clp)
    {
        return (char *) 0;
    }
    *clp = 0;
    n--;
    for (i = 0, len = 0; i < argc; i++)
    {
        if (argv[i])
        {
            len += strlen (argv[i]) + 1;
            if (len <= n)
            {
                /* this is expensive but who cares (AYLWIN?) */
                strcat (clp, argv[i]);
                strcat (clp, " ");
            }
        }
    }
    return clp;
}



char *
arg_getIValue (int argc, char ** argv, const char * optp, u_int * dip)
{
    char tempStr[64];
    char * cp;

    strcpy (tempStr, optp);
    if ( (cp = arg_OptGet (argc, argv, tempStr, sizeof (tempStr))) )
    {
        if (dip)
            *dip = atoi(tempStr);
    }
    return cp;
}

char *
arg_getBValue (int argc, char ** argv, const char * optp, Bool * dbp)
{
    char * cp;

    if ( (cp = arg_OptGet (argc, argv, (char *) optp, 0)) )
    {
        if (dbp)
            *dbp = TRUE;
    }
    return cp;
}



