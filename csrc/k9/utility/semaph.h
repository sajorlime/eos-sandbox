#ifndef	_SEMAPH_H
#define	_SEMAPH_H

#ifdef _OS9000
#include  <types.h>
#include "ProcessLock.h"
typedef u_int key_t;
typedef ProcessLock *sema_id;
typedef char*        sema_key;

#else
#include	<sys/types.h>
#include	<sys/ipc.h>
#include	<sys/sem.h>
typedef int   sema_id;
typedef key_t sema_key;

#ifdef _SUN4
#include	<sys/stat.h>
#else
#include	<sys/mode.h>
#endif

#endif

#include	<errno.h>
#include 	"ictvcommon.h"
/* function protos*/

#ifndef _OS9000
int	sem_create(sema_key, int);
int	sem_open(sema_key);
void	sem_op(sema_id, int);
#else
sema_id	sem_create(sema_key, int);
sema_id	sem_open(sema_key);
#endif

void	sem_rm(sema_id);
void	sem_close(sema_id);
void	sem_wait(sema_id);
void	sem_signal(sema_id);


/* !@)#$) Sun function protos*/
#ifdef _SUN4
extern int semget(sema_key key, int nsems, int semflg);
extern int semop(int semid, struct sembuf *sops, unsigned nsops);
extern int semctl(int semid, int semnum, int cmd, ...);
#endif

#endif /* _SEMAPH_H*/

