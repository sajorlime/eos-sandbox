#ifdef _OS9000
#ifdef MMC
#include "globMMC.h"
#include "MemHeap.h"
#else
#include <types.h>
#include <module.h>
#endif

#else
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#endif


#include "ictvcommon.h"
#include "LogPrivate.h"

#ifdef MMC
static MMCGlobals * Debug_globptr= NULL;
#endif


void *
debug_SetUp_file(int server)
{

  int debug_fd,i;
  char buffer[100];
  LogMsg stuff;
  
  
#ifdef _OS9000
  void * area;
  void * tmp_ptr;
#ifndef MMC
  void * header;
  mh_com * dont_care;
  u_int16 attr,lang,retval;
  char * modname_p = Debug_IPC_modulename;



  if  (server)
    {
      attr=CUR_ATTR;
      lang=ML_ANY;
      if (retval=_os_link(&modname_p,&dont_care,
			  (void **)&area,&lang,&attr))
	{
	  retval=_os_datmod(modname_p,
			    (u_int32)(sizeof(Q_info)+
				      (Debug_QSize*sizeof(LogMsg))),
			    &attr,&lang,
			    0x777,
			    (void **)&area,
			    (mh_com **)&header);
	}
      else
	{
	  printf("Hmm, datamodule still there\n");
	  if (attr!=CUR_ATTR)
	    printf("attribs don't match\n");
	}
    }
  else
    {
      retval=_os_link(&modname_p,&dont_care,(void **)&area,&lang,&attr);
      if (attr!=CUR_ATTR)
	printf("attribs don't match\n");
    }
  if (retval)
    {
      sprintf (buffer,"couldn't open datamodule,error %x",retval);
      perror(buffer);
      printf(buffer);
      fflush(stderr);
      fflush(stdout);
	      
      return (NULL);
    }
#else
  /* OS9k and MMC*/
  if (glob_link())
    {				/* problems: bail!*/
      printf("globlink failed!!!\n");
      exit(-1);
    }
  Debug_globptr=glob_getGlobals();
  tmp_ptr=Debug_globptr->debugptr;

#ifndef SHARE_DEBUGPTR
  area=tmp_ptr;
#endif

  if (server)
    {
#ifdef SHARE_DEBUGPTR
      if(tmp_ptr==NULL)
	{
	  /* needed to fix a pointer sharing problem*/
	  int eflags;

	  eflags = irq_change (0);
        
	  if ((tmp_ptr = glob_getDebugPtr()) == NULL)
	    {
	      tmp_ptr = (globDebugHdr *) mem_malloc
		(DEFAULT_HEAP_ID, sizeof (globDebugHdr));
	      
	      if (tmp_ptr)
                {
		  /* initialize all parts of the header sharing block */
		  tmp_ptr->globDebugHead = NULL;
		  tmp_ptr->globLoggingHead = NULL;
                }
	      glob_setDebugPtr (tmp_ptr);
	    }
	  
	  
	  irq_change (eflags);
	}
      area=tmp_ptr->globLoggingHead;
#endif
      
      
      
      if (area!=NULL)
	{
	  /* someone's been here!!*/
	  printf("is someone still here?\n");
	}
      else
	{
	  area=mem_malloc (DEFAULT_HEAP_ID,sizeof(Q_info) +
			   (Debug_QSize * sizeof(LogMsg)) );
#ifdef SHARE_DEBUGPTR
	  (globDebugHdr*) (Debug_globptr->debugptr)->globLoggingHead=area;
#else
	  Debug_globptr->debugptr=area;
#endif
	}
    }
  
  else				/* client*/
    if (area==NULL)
      {
	/* where's the server !!*/
	printf("server missing\n");
      }
  /* end of MMC part*/	    
#endif
  
#else
  /* unix*/
  
  caddr_t area;
  if (server)
    {
      if ((debug_fd = open (Debug_IPC_filename, O_RDWR|O_CREAT,0666)) <0)
	return (NULL);
    }
  else
    {
      if( (debug_fd=open (Debug_IPC_filename,O_RDWR) ) <0 )
	return (NULL);
    }
  
#ifndef FILE_WRITE_HACK
  /*  	lseek(debug_fd,sizeof(Q_info)+10*sizeof(LogMsg), SEEK_SET);*/
  
  stuff.type=1;
  stuff.len=1;
  strcpy(stuff.body,"hello67890hello67890hello67890hello67890hello67890hello67890hello67890hello67890hello67890hello67890");
  
  
  if (server)
    {
      write(debug_fd,"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", sizeof(Q_info));	/*XXX do we need this?*/
      for (i = 0; i<7000;i+=sizeof(LogMsg)) /*XXX do we need this?*/
	{
	  stuff.seq=i;
	  write(debug_fd,&stuff, sizeof(LogMsg));
	}
    }
#endif /* file_write_hack*/
  /* now mmap it*/
  area=  mmap(0,sizeof(Q_info) + (Debug_QSize* sizeof(LogMsg)),
	      PROT_READ | PROT_WRITE,
	      MAP_SHARED, debug_fd, 0);
  
#endif /*unix*/
  return(area);
}

#ifdef MMC
int debug_config_check(void)
{
  if (Debug_globptr)
    return Debug_globptr->configp;
  else
    return NULL;
}

#endif


int debug_SetUp_semas(sema_id * lock_id,sema_id * signal_id,int server)
{
  *lock_id=sem_open(LOCK_SEM_KEY);
  if ((*lock_id >0) && server)
    {
      fprintf(stderr,"still have lock sema\n");
      fflush(stderr);
      /*opps, the ipc is still there, nuke it*/
      sem_rm(*lock_id);
    }
  if (server)
    *lock_id=sem_create(LOCK_SEM_KEY,1); /* creates the lock sema*/
  
  if (*lock_id <= 0) 
    {
      perror("sema prob");
      return (-1);
    }
  
#ifdef USE_SIGNAL_SEMA
  *signal_id=sem_open(SIGNAL_SEM_KEY);
  if(server && (*signal_id > 0))
    {
      fprintf(stderr,"still have signal sema\n");
      fflush(stderr);
      sem_rm(*signal_id);
    }
  if (server)
    *signal_id=sem_create(SIGNAL_SEM_KEY,0); /* creates the signal sema*/

  if (*signal_id <= 0)
    return (-1);


#endif


  /*  atexit( cleanup );*/ /*XXX NO LINKAGE!!!*/ 
  return (0);

}

void debug_close_semas(sema_id lock_sem_id, sema_id signal_sem_id)
{
  sem_close(lock_sem_id);
#ifdef USE_SIGNAL_SEMA
  sem_close(signal_sem_id);
#endif

}

