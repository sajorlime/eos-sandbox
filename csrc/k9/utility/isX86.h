
/*
** Copyright (C) 1994, ICTV Inc. 14600 Winchester Blvd., Los Gatos, CA 95030 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: Any X86 project
**
** File: isX86.h instruction set X86 definitions.
**
**     $Id: isX86.h,v 1.2 1994/12/03 04:31:06 aylwin Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/utility/isX86.h,v $
**
** Purpose:
**  To provide instruction set access for functions not available in c.
**  These functions are only useful/available on os9k.
**
** Author: emil
** $Author: aylwin $
**
** Functions:
**  int bitScanForward -- bit scan forward.
**  int bitScanReverse -- bit scan reverse.
**  int extract32 - extract 32 bits out of the two word r0 and r1.
**  int rotateLeft - extract 32 bits out of the two word r0 and r1.
**  int rotateRight - extract 32 bits out of the two word r0 and r1.
**  int swapShort - swaps the two bytes in i.
**  int swapLong - swaps the four bytes in i.
**
**
** Notes:
**
*/

#ifndef __ISX86_H__
#define __ISX86_H__

#ifdef _OS9000

#include <types.h>


/*
** bitScanForward -- return the bit index of the first (low order) bit
** in bits that is a one.
**
** Parameter: 32 bit bit-field that you want an index from.
**
** Usage:
**  regbits = inl (myBitsPort);
**  while (regBitIndex)
**  {
**      regBitIndex = bitScanForward (regbits);
**      regBitIndex ^= regDispatchTable[regBitIndex] (myBitParams)
**  }
*/
int bitScanForward (u_int bits);


/*
** bitScanReverse -- return the bit index of the last (high order) bit
** in bits that is a one.
**
** Parameter: 32 bit bit-field that you want an index from.
**
** Usage: see bitScanForward
*/
int bitScanReverse (int bits);


/*
** swapShort -- swaps the two bytes in i.
*/
int swapShort (u_short i);


/*
** swapLong -- swaps the four bytes in i.
*/
int swapLong (u_int i);


/*
** rotate the 32 bit value r c % 32 bits to the right.
*/
int rotateRight (u_int r, int c);

/*
** rotate the 32 bit value r c % 32 bits to the left.
*/
int rotateLeft (u_int r, int c);

/*
** extract32 -- extract 32 bits out of the two word r0 and r1.
**
** r0 is treated as the least significant word of a long long word.
*/
int extract32 (u_int r0, u_int r1, int firstBit);

# define X86_CLI _asm ("    cli ")
# define X86_STI _asm ("    sti ")

void x86_cli (void);
void x86_sti (void);

#endif  /* _OS9000 */

#endif  /* __ISX86_H__ */
