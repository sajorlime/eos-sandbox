
/*
** COPYRIGHT (C) 1994, 1995
** ICTV Inc.
** 14600 Winchester Blvd.
** Los Gatos, CA 95030
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: Utility Library
**
** File: smpte.h - 
**
** Author: jmt, emil, aylwin
**
** Purpose:
**  Defines the smpte interface.
**
** Constants:
**  SMPTENow -- globally available now value;
**  SMPTEZero -- globally available zero value.
**  SECONDS_PER_MINUTE --
**  MINUTES_PER_HOUR --
**  FRAMES_PER_SECOND --
**  FRAMES_PER_MINUTE --
**  FRAMES_PER_HOUR --
**  SMPTE_NOW --
**
** Types/Clases:
**  SMPTEtime --
**
** Operatiors/Functions
**  SMPTE_IS_NOW --
**  SMPTE_EQ --
**  SMPTE_LTE --
**  SMPTE_LT --
**  SMPTE_GTE --
**  SMPTE_GT --
**  SMPTE_SET --
**
** Usage:
**
** Notes:
**
** $Id: smpte.h,v 1.14 1995/05/02 01:20:26 ljr Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/utility/smpte.h,v $
**
*/

/* 
   File:  smpte.h 
*/


#ifndef __SMPTE_H__
#define __SMPTE_H__


#include "ictvcommon.h"

/*
 * SECONDS_PER_MINUTE:  lets us change seconds in a minute easily
 */
#define SECONDS_PER_MINUTE       (60)

/*
 * MINUTES_PER_HOUR:  lets us change minutes in a hour easily
 */
#define MINUTES_PER_HOUR         (60)


/* 
 * FRAMES_PER_SECOND: number of fields per second
 */
#define FRAMES_PER_SECOND            (30)

/* 
 * FRAMES_PER_MINUTE: number of fields per minute non drop frame time
 */
#define FRAMES_PER_MINUTE            (SECONDS_PER_MINUTE * FRAMES_PER_SECOND)

/* 
 * FRAMES_PER_HOUR: number of fields per hour non drop frame time
 */
#define FRAMES_PER_HOUR               (MINUTES_PER_HOUR * FRAMES_PER_MINUTE)


/* 
 * SMPTE_NOW: schedule a ClockEvent to happen ASAP
 */
#define SMPTE_NOW  (-1)
/* SMPTE_FOREVER 23 hours, 59 minutes, 59 seconds, and 29 frames */
/* We may want to increase the hours later */
#define SMPTE_FOREVER  0x173b3b1d



# define SMPTE_IS_NOW(t) ((t).value == SMPTE_NOW)
# define SMPTE_EQ(t1, t2) ((t1).value == (t2).value)
# define SMPTE_LTE(t1, t2) ((t1).value <= (t2).value)
# define SMPTE_LT(t1, t2) ((t1).value < (t2).value)
# define SMPTE_GTE(t1, t2) ((t1).value >= (t2).value)
# define SMPTE_GT(t1, t2) ((t1).value > (t2).value)

# define SMPTE_SET(Time, Hours, Minutes, Seconds, Frames) \
    { \
        (Time).hmsf.h = (Hours); (Time).hmsf.m = (Minutes); \
        (Time).hmsf.s = (Seconds); (Time).hmsf.f = (Frames); \
    }

/*
    Data types defined
*/

typedef union _SMPTEtime
{
    int32 value;
    struct _hmsf
    {
#ifdef LITTLE_ENDIAN
        u_int8 f;
        u_int8 s;
        u_int8 m;
        u_int8 h;
#else /* BIG_ENDIAN */
        u_int8 h;
        u_int8 m;
        u_int8 s;
        u_int8 f;
#endif
    } hmsf;
} SMPTEtime; /* Type for specifying frame numbers in video. */

typedef SMPTEtime SMPTETime;    /* Alias. */

#ifdef __cplusplus
extern "C" {
#endif

extern SMPTEtime SMPTENow;
extern SMPTEtime SMPTEZero;
extern SMPTEtime SMPTEForever;
/*
    Utility function declarations
*/

SMPTEtime SMPTEAdd (SMPTEtime a, SMPTEtime b);
SMPTEtime SMPTESubtract (SMPTEtime a, SMPTEtime b);

SMPTEtime framesToSMPTE( u_int frames );
SMPTEtime MSToSMPTE (u_int32 milliseconds);
u_int32 SMPTEToMS (SMPTEtime smpte);
SMPTEtime GregorianToSMPTE (int time, int ticks);
char * sprintSMPTE (char * buffer, SMPTETime time);

void PrintSMPTE (SMPTEtime time);
SMPTEtime strToSMPTE( char *smpteString );
void writeSMPTE (SMPTETime time, FILE * stream);

#ifdef __cplusplus
}
#endif

#endif

