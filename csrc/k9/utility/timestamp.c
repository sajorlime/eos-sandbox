/*
** Copyright (C) 1994, ICTV Inc. 14600 Winchester Blvd., Los Gatos CA 95030
** Project: ictv interactive television
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** File: timestamps.c - timestamping prototypes
**     $Id: timestamp.c,v 1.16 1995/11/04 02:04:17 wul Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/utility/timestamp.c,v $
**
** Author:
**      Larry Rosenberg
**
** Purpose:
**      This file implements the basic timestamp type as well as routines
**      for generating them, displaying them and comparing them.
**
**      Timestamps have two purposes. They are either the current date
**      and time (accurate to some machine dependent value) or they are
**      the difference between two timestamps and represent elapsed time.
**
** Functions:
**      TimeStamp_Add - Adds two times together
**      TimeStamp_Current - generates the current time as an internal timestamp
**      TimeStamp_CurrentStr - generates the current time as a displayable string
**      TimeStamp_CurrentDateStr - generates the current date only 
**      TimeStamp_CurrentTimeStr - generates the current time only 
**      TimeStamp_DateStr - creates a displayable date string from timestamp
**      TimeStamp_Divide - divides a timestamp into N equal parts. 
**      TimeStamp_Elapsed - calculates elapsed time from two timestamps
**      TimeStamp_ElapsedStr - creates an elapsed time string from timestamp
**      TimeStamp_FormatStr - generic timestamp format string
**      TimeStamp_FromMicroSecs - sets both the time and fractional portion
**      TimeStamp_FromMilliSecs - sets both the time and fractional portion
**      Timestamp_fromString - converts "mm/dd/yy hh:mm" to Timestamp_t
**      Timestamp_formatSet - converts arbitrary string to Timestamp_t
**      Timestamp_GetMonth - calculate the month from a timestamp
**      Timestamp_GetDayOfWeek - calculate the day of the week from a timestamp
**      TimeStamp_MinuteOfWeek - return a number between 0-10079.
**      TimeStamp_SetDate - updates the date portion in the timestamp
**      TimeStamp_SetMicroSecs - updates the the fractional portion of the time
**      TimeStamp_SetMilliSecs - updates the the fractional portion of the time
**      TimeStamp_SetSecs - updates the seconds portion
**      TimeStamp_SetTime - updates the time portion in the timestamp
**      TimeStamp_Str - creates a displayable string from timestamp
**      TimeStamp_Test - compares two timestamp
**      TimeStamp_TestCurrent - compares a timestamp against the current time
**      TimeStamp_TimeStr - creates a displayable time string from timestamp
**      TimeStamp_Xlat - provide host <-> network translation
**
** Added TimeStamp_Divide. BDN
** Modified K9 functions to be accurate to within approximately 1 micro second. BDN.
** Last Edited: Thu Sep 28 11:14:37 1995
*/

#include <stdio.h>                  /* USING printf */
#include <string.h>                 /* USING strlen */
#include <errno.h>                  /* USING errno */
#include <stdlib.h>
#include <ctype.h>                  /* USING tolower */
#ifdef _SUN4
#   include <memory.h>              /* USING memset */
#else
#   include <string.h>              /* USING memset */
#endif

/* FOR ntohl & htonl */
#ifdef _OS9000
#   include <types.h>
#   include <INET/in.h>
#   include "k9misc.h"
#else
#   include <sys/types.h>
#   include <netinet/in.h>
#endif

#include "timestamp.h"

/* EXTERNAL ROUTINES */
#ifdef _OS9000
#   define EINVAL   EOS_ILLARG
#else
extern int gettimeofday (struct timeval *, struct timezone *);
extern time_t timelocal(struct tm *);
#ifdef _SUN4
extern char *strptime(const char *, const char *, struct tm *);
#endif
#endif /* _OS9000 */

/* INTERNAL CONSTANTS */
#define SECS_PER_MIN    60
#define SECS_PER_HOUR   3600
#define SECS_PER_DAY    86400
/* =-= jd I took off 60 from the actual value. 315532800 was the real value */
#define OS9k_EPOCH_DIFF      315532740    

static char *dateStr = "%m/%d/%y";
static char *dateTimeStr = "%m/%d/%y %H:%M:%S";
static char *timeStr = "%H:%M:%S";

/* Because of space leaks in OS9000 version of strftime */
#ifdef _OS9000
static u_int32 localStrftime(char *buffer, u_int32 len, char *formatStr,
                             struct tm *timeVal);
#endif

/****************************************************************************
 * int TimeStamp_Add(Timestamp_t *result, Timestamp_t *first, Timestamp_t *last)
 * Purpose:     Calculate the sum between two times and return it
 *
 * Parameters:
 *  result - answer is stored here
 *  first, last - calculate "last + first"
 *
 * Returns:     0 = ok; -1 = invalid arguments
 ***************************************************************************/
int TimeStamp_Add(Timestamp_t *result, Timestamp_t *first, Timestamp_t *last)
{
    if (!result || !first || !last) { errno = EINVAL; return -1; }
    result->time = last->time + first->time;
    result->frac = last->frac + first->frac;
    /* Handle any results of the fractional part by carrying one second over */
    if (result->frac > 1000000) {
        result->frac -= 1000000;
        result->time++;
    }
    return 0;
} /* TimeStamp_Add() */

/****************************************************************************
 * Timestamp_t *TimeStamp_Current(Timestamp_t *result)
 * Purpose:     Generates the current time and date
 *
 * Parameters:
 *  result - where to store the time
 *  fracDigits - how many digits in the fractional portion of the time
 *
 * Returns:     results
 ***************************************************************************/
Timestamp_t *TimeStamp_Current(Timestamp_t *result)
{
#ifdef _OS9000
    time_t time;
    u_int32 osticks, refinement, eflags;
    u_int16 ticks, ticksPerSec;
    u_int8 roll;

    /* 
    ** The basic methodology for timestamping is to combine a coarse
    ** second/tick resolution timer kept by OS/9 with a fine microsecond
    ** level resolution timer artificially created by values of the
    ** countdown timer which runs the coarse timer.  To associate one
    ** with the other in a racefree manner, the readings of both timers
    ** must be taken when interrupts are disabled.
    **
    ** There is a chance that the fine timer can wrap while this
    ** measurement is being taken.  There are two cases of wrap; the
** first is that wrap occurred after interrupts were disabled, but
    ** before the fine counter was read; this means that the refinement
    ** number needs to be associated with (tick+1). 
    **
    ** The second case of wrap is that it occurs after the refinement
    ** number is read; just before we poll the PIC for it.
    **
    ** To handle both cases with the same code we reread the fine counter
    ** if the PIC detected a wrap and always move to the next tick.  This
    ** guarantees that the fine counter's value is offset from the
    ** specified tick.
    **
    ** NB: we make an implicit assumption that at most one wrap event
    ** occurs...
    */

    eflags = irq_change (0);

    /* first get system tickval and its refinements */
    refinement = k9_getDeltaMicroSecs ();
    _os_getime (&time, &osticks);

    /* see if rollover interrupt is pending */
    roll = k9_picTimerInterruptPending ();

    /* reread microseconds and increment ticks if wrap detected */
    if (roll)
    {
        refinement = k9_getDeltaMicroSecs ();
        osticks++;
    }

    irq_change (eflags);

#ifdef TESTING
    printf ("ticks = %d, low word = %d, high word = %d.\n",
        osticks, (osticks & 0xFFFF), ((osticks & 0xFFFF0000) >> 16));
#endif

    /* We need to normalize the base to Jan 1 1970 since OS9K uses Jan 1 1980 */
    result->time = time + OS9k_EPOCH_DIFF;

    ticksPerSec = (osticks & 0xFFFF0000) >> 16;
    ticks = (osticks & 0xFFFF);

    if (ticksPerSec == 100)
        result->frac = 10000L * ticks + refinement;
    else
        result->frac = ((1000000L * ticks) / ticksPerSec) + refinement;

    if (result->frac >= 1000000)
    {
        result->time++;
        result->frac -= 1000000;
    }

#else
    struct timeval tv;
    struct timezone ignore;
    gettimeofday (&tv, &ignore);
    result->time = tv.tv_sec;
    result->frac = tv.tv_usec;
#endif
    return result;
}

/****************************************************************************
 * char *TimeStamp_CurrentStr(char *buffer, int fracDigits)
 * Purpose:     Display the current time into the given buffer
 *
 * Parameters:
 *  buffer - where to store the results
 *  fracDigits - how many digits in the fractional portion of the time
 *
 * Returns:     buffer (in case you want to immediately use it)
 ***************************************************************************/
char *TimeStamp_CurrentStr(char *buffer, int fracDigits)
{
    Timestamp_t ts;
    TimeStamp_Current(&ts);
    return TimeStamp_Str(&ts, buffer, fracDigits);
} /* TimeStamp_CurrentStr() */

/****************************************************************************
 * char *TimeStamp_CurrentDateStr(char *buffer) 
 * Purpose:     Display the current date (ignore time)
 *
 * Parameters:  buffer - where to put data
 *
 * Returns:     buffer (or NULL if error)
 ***************************************************************************/
char *TimeStamp_CurrentDateStr(char *buffer)
{
    Timestamp_t ts;
    TimeStamp_Current(&ts);
    return TimeStamp_DateStr(&ts, buffer);
} /* TimeStamp_CurrentDateStr() */

/****************************************************************************
 * char *TimeStamp_CurrentTimeStr(char *buffer, int fracDigits)
 * Purpose:     Display the current time (but not date) into the given buffer
 *
 * Parameters:
 *  buffer - where to store the results
 *  fracDigits - how many digits in the fractional portion of the time
 *
 * Returns:     buffer (in case you want to immediately use it)
 ***************************************************************************/
char *TimeStamp_CurrentTimeStr(char *buffer, int fracDigits)
{
    Timestamp_t ts;
    TimeStamp_Current(&ts);
    return TimeStamp_TimeStr(&ts, buffer, fracDigits);
} /* TimeStamp_CurrentTimeStr() */

/****************************************************************************
 * char *TimeStamp_DateStr(Timestamp_t *ts, char *buffer) 
 * Purpose:     Generate string for date portion of timestamp
 *
 * Parameters:  
 *  ts - time to display
 *  buffer - where to store the results
 *
 * Returns:     buffer (in case you want to immediately use it)
 ***************************************************************************/
char *TimeStamp_DateStr(Timestamp_t *ts, char *buffer)
{
    return TimeStamp_FormatStr(ts, buffer, dateStr, 0, TIMESTAMP_STRING_LEN);
} /* TimeStamp_DateStr() */

/****************************************************************************
 * int TimeStamp_Divide(Timestamp_t *result, Timestamp_t *argp, int n) 
 * Purpose:     divides a timestamp into N equal parts. 
 *
 * Parameters:
 *   result - where the calculation is placed
 *   argp - value to divide
 *   n - number of parts
 *
 * Returns:     0 = success ; -1 = error (see errno)
 ***************************************************************************/
int TimeStamp_Divide(Timestamp_t *result, Timestamp_t *argp, int n)
{
    Timestamp_t arg ;
    long remainder_time = 0 ;
    long frac ;

    if (!result || !argp) { errno = EINVAL; return -1; }
    arg = *argp ;
  
    result->time = arg.time/n ;

    remainder_time = arg.time - n*result->time ;
    remainder_time = (remainder_time * 1000000)/n ;

    frac = remainder_time + arg.frac/n ;

    if (frac >= 1000000) {
        result->time++ ;
        frac -= 1000000 ;
    }

    result->frac = frac ;
    return 0;
} /* TimeStamp_Divide() */

/****************************************************************************
 * int TimeStamp_Elapsed(Timestamp_t *result, Timestamp_t *first, Timestamp_t *last)
 * Purpose:     Calculate the difference between two times and return it
 *
 * Parameters:
 *  result - answer is stored here
 *  first, last - calculate "last - first"
 *
 * Returns:     0 = ok; -1 = invalid arguments
 ***************************************************************************/
int TimeStamp_Elapsed(Timestamp_t *result, Timestamp_t *first, Timestamp_t *last)
{
    if (!result || !first || !last) { errno = EINVAL; return -1; }
    result->time = last->time - first->time;
    result->frac = last->frac - first->frac;
    /* Handle any results of the fractional part by carrying one second over */
    if (result->frac < 0) {
        result->frac += 1000000;
        result->time--;
    }
    return 0;
} /* TimeStamp_Elapsed() */

/****************************************************************************
 * char *TimeStamp_ElapsedStr(Timestamp_t *ts, char *buffer, boolean showFrac)
 * Purpose:     Show the represented elapsed time as an english phrase
 *
 * Parameters:  
 *  ts - time to display
 *  buffer - where to store the results
 *  showFrac - show the fractional portion or not
 *
 * Returns:     buffer (in case you want to immediately use it)
 ***************************************************************************/
char *TimeStamp_ElapsedStr(Timestamp_t *ts, char *buffer, boolean showFrac)
{
    if (!ts) { errno = EINVAL; return NULL; }
    else if (ts->time == 0) {
        if (showFrac)
            *buffer = '\0';
        else
            sprintf(buffer, "0 secs");
    } else if (ts->time < SECS_PER_MIN) {
        sprintf(buffer, "%ld secs", ts->time);
    } else if (ts->time < SECS_PER_HOUR) {
        sprintf(buffer, "%ld mins %ld secs", 
                ts->time / SECS_PER_MIN, ts->time % SECS_PER_MIN);
    } else if (ts->time < SECS_PER_DAY) {
        long hrs = ts->time / SECS_PER_HOUR;
        long time = ts->time % SECS_PER_HOUR;
        sprintf(buffer, "%ld hrs %ld mins %ld secs",
                hrs, time / SECS_PER_MIN, time % SECS_PER_MIN);
    } else {
        long days = ts->time / SECS_PER_DAY;
        long time = ts->time % SECS_PER_DAY;
        long hrs = time / SECS_PER_HOUR;
        time = ts->time % SECS_PER_HOUR;
        sprintf(buffer, "%ld days %ld hrs %ld mins %ld secs",
                days, hrs, time / SECS_PER_MIN, time % SECS_PER_MIN);
    }
    if (showFrac) {
        char *cp = buffer + strlen(buffer);
        sprintf(cp, "%s%ld usecs", (cp == buffer) ? "" : " ", ts->frac);
    }
    return buffer;
} /* TimeStamp_ElapsedStr() */

/****************************************************************************
 * char *TimeStamp_FormatStr (TimeStamp_t *ts, char *buffer, char *formatStr,
 *   int fracDigits, int len) 
 * Purpose:     Low-level time/date format function (uses strftime)
 *
 * Parameters:  
 *  ts - timestamp to use
 *  buffer - result placed here
 *  formatStr - how to display result
 *  fracDigits - how many digits to allow for fractional portion
 *  len - how long buffer is
 *
 * Returns:     buffer (or NULL if invalid time)
 ***************************************************************************/
char *TimeStamp_FormatStr (Timestamp_t *ts, char *buffer, char *formatStr,
                           int fracDigits, int len)
{
    if (!ts) { errno = EINVAL; return NULL; }
    else {
        char *cp = buffer;
        struct tm *tm = TimeStamp_LocalTime(ts);

#ifdef _OS9000
        cp += localStrftime(buffer, len, formatStr, tm);
#else
        cp += strftime(buffer, len, formatStr, tm);
#endif
        if (fracDigits != 0) {
            long frac;
            /* reduce the number of digits to be printed without using math lib */
            switch (fracDigits) {
              case 1: frac = ts->frac / 100000L; break;
              case 2: frac = ts->frac / 10000L; break;
              case 3: frac = ts->frac / 1000L; break;
              case 4: frac = ts->frac / 100L; break;
              case 5: frac = ts->frac / 10L; break;
              default: frac = ts->frac; fracDigits = 6; break;
            }
            sprintf(cp, ".%0*ld", fracDigits, frac); 
        }
        return buffer;
    }
} /* TimeStamp_FormatStr() */

/****************************************************************************
 * int TimeStamp_FromMilliSecs(Timestamp_t *ts, long secs, long milliSecs) 
 * Purpose:     Update both the whole and fractional portion and allow for
 *              a large number of milliSecs to "wrap" into secs
 *
 * Parameters:
 *   ts - structure to update
 *   secs - seconds to set
 *   milliSecs - milliSeconds to set
 *
 * Returns:     error (if any)
 ***************************************************************************/
int TimeStamp_FromMilliSecs(Timestamp_t *ts, long secs, long milliSecs)
{
    secs += milliSecs / 1000;
    milliSecs = milliSecs % 1000;
    ts->time = secs;
    ts->frac = milliSecs * 1000;
    return 0;
} /* TimeStamp_FromMilliSecs() */

/****************************************************************************
 * int TimeStamp_FromMicroSecs(Timestamp_t *ts, long secs, long microSecs) 
 * Purpose:     
 * Purpose:     Update both the whole and fractional portion and allow for
 *              a large number of milliSecs to "wrap" into secs
 *
 * Parameters:
 *   ts - structure to update
 *   secs - seconds to set
 *   microSecs - microSeconds to set
 *
 * Returns:     error (if any)
 ***************************************************************************/
int TimeStamp_FromMicroSecs(Timestamp_t *ts, long secs, long microSecs)
{
    secs += microSecs / 1000000;
    microSecs = microSecs % 1000000;
    ts->time = secs;
    ts->frac = microSecs;
    return 0;
} /* TimeStamp_FromMicroSecs() */

#ifndef _OS9000
/****************************************************************************
 * Timestamp_t TimeStamp_fromString(char *s)
 * Purpose:     converts "mm/dd/yy hh:mm" to Timestamp_t
 *
 * Parameters:  s - string in format "mm/dd/yy hh:mm"
 *
 * Returns:     Timestamp_t
 *
 * Note: This won't compile on OS9K.  To make this function work on OS9K,
 *       'strptime' or equivalent would need to be written.
 ***************************************************************************/
Timestamp_t Timestamp_fromString(char *s)
{
    Timestamp_t  ts;
    Timestamp_formatSet("%m/%d/%y %H:%M", s, &ts);

    return (ts);
} /* Timestamp_fromString() */
#endif /* _OS9000 */

#ifndef _OS9000
/****************************************************************************
 * Timestamp_t TimeStamp_formatSet(char *format, char *s, Timestamp_t *pTS)
 * Purpose:     converts arbitrary string to Timestamp_t using specified
 *              format
 *
 * Parameters:  format - describes format of input string; see strptime(3)
 *              s - string to use
 *              pTS - pointer to Timestamp_t returned
 *
 * Returns:     0 = success; -1 = error (bad format string)
 *
 * Note: This won't compile on OS9K.  To make this function work on OS9K,
 *       'strptime' or equivalent would need to be written.
 ***************************************************************************/
int Timestamp_formatSet(char *format, const char *s, Timestamp_t *pTS)
{
    struct tm   tm;
    char        buff[128];
    int         i, len;

    memset(&tm, 0, sizeof (tm));
    memset(buff, 0, sizeof (buff));

    /* skip leading white space */
    while (*s == ' ' || *s == '\t') s++;
    
    /* Lower case the input: strptime is picky about am/pm */
    len = strlen(s);
    if (len > sizeof(buff)-1) len = sizeof(buff)-1;
    for (i = 0; i < len ; i++) buff[i] = tolower(s[i]);
    buff[len] = 0;

    if (!strptime(buff, format, &tm))
      return (-1);

    if (tm.tm_year < 39)
      tm.tm_year += 100;  /* continue to work after 1/1/2000 */

    TimeStamp_MakeTime(pTS, &tm);

    return (0);
} /* Timestamp_formatSet() */
#endif /* _OS9000 */

/****************************************************************************
 * void TimeStamp_GetDayOfMonth (Timestamp_t *ts) 
 * Purpose:     Get the day of the month from the time stamp
 *
 * Parameters:  
 *  ts - time specifying the month
 *
 * Returns:     the day of the month 
 ***************************************************************************/
int TimeStamp_GetDayOfMonth(Timestamp_t *ts)
{
    struct tm *tm = TimeStamp_LocalTime(ts);
    return tm->tm_mday;
}

/****************************************************************************
 * void TimeStamp_GetMonth (Timestamp_t *ts) 
 * Purpose:     Get the month of the year from the time stamp
 *
 * Parameters:  
 *  ts - time specifying the month
 *
 * Returns:     the month of the year
 ***************************************************************************/
int TimeStamp_GetMonth(Timestamp_t *ts)
{
    struct tm *tm = TimeStamp_LocalTime(ts);
    return tm->tm_mon;
}

/****************************************************************************
 * struct tm *TimeStamp_LocalTime(Timestamp_t *ts) 
 * Purpose:     Deal with operating system having different starting points
 *              for time values
 *
 * Parameters:
 *   ts - time stampt to convert
 *
 * Returns:     broken out time values (struct tm)
 ***************************************************************************/
struct tm *TimeStamp_LocalTime(Timestamp_t *ts)
{
    time_t timeVal = ts->time;

#ifdef _OS9000
    /* Set the base of calculating time to Jan 1 1980 */
    timeVal  -= OS9k_EPOCH_DIFF;
#endif
    
    return localtime(&timeVal);
} /* TimeStamp_LocalTime() */

/****************************************************************************
 * Timestamp_t *TimeStamp_MakeTime(Timestamp_t *ts, struct tm *tm) 
 * Purpose:     Create a time stamp from the standard unix tm struct
 *
 * Parameters:
 *   ts - where to put the data
 *   tm - data to create time-stamp from
 *
 * Returns:     timestamp 
 ***************************************************************************/
Timestamp_t *TimeStamp_MakeTime(Timestamp_t *ts, struct tm *tm)
{
#if defined(_AIX)
    tm->tm_isdst = -1; /* For TZ - TimeZone problem */
#endif

#if defined(_AIX) || defined(_OS9000)
    ts->time = mktime(tm);
#if defined(_OS9000)
    /* Normalize base to Jan 1 1970 */
    ts->time += OS9k_EPOCH_DIFF;
#endif
#else
    ts->time = timelocal(tm);
#endif

    /* always set the fractional portion to zero */
    ts->frac = 0;
    return ts;
} /* TimeStamp_MakeTime() */

/****************************************************************************
 * u_int16 TimeStamp_MinuteOfWeek(Timestamp_t *ts) 
 * Purpose:     Return the number of minutes from the beginning of week
 *              (Sunday is 0). Useful for determining preferences that have
 *              one minute granualarity.
 *
 * Parameters:
 *   ts - object to convert
 *
 * Returns:     
 ***************************************************************************/
u_int16 TimeStamp_MinuteOfWeek(Timestamp_t *ts)
{
    struct tm *tm = TimeStamp_LocalTime(ts); 
    return (1440 * tm->tm_wday) + (60 * tm->tm_hour) + tm->tm_min;
} /* TimeStamp_MinuteOfWeek() */

/****************************************************************************
 * int TimeStamp_SetDate(Timestamp_t *ts, int month, int day, int year)
 * Purpose:     Set the specified values in the timestamp to the passed in values
 *
 * Parameters:
 *  ts - time buffer to update
 *  month, day, year - new values to set
 *
 * Returns:     0 = all ok; less than zero = invalid value given
 ***************************************************************************/
int TimeStamp_SetDate(Timestamp_t *ts, int month, int day, int year)
{
    int stat = 0;
    struct tm *tm = TimeStamp_LocalTime(ts);

    if (month < 1 || month > 12) stat += -4;
    if (day < 1 || day > 31) stat += -2;
    if (year < 1970 || year > 2100) stat += -1;
    if (stat == 0) {
        tm->tm_mon = month - 1;
        tm->tm_mday = day;
        tm->tm_year = year - 1900;
        TimeStamp_MakeTime(ts, tm);
    }
    return stat;
} /* TimeStamp_SetDate() */

/****************************************************************************
 * int TimeStamp_SetMicroSecs(Timestamp_t *ts, long microSecs)
 * Purpose:     Set the fractional portion of the timestamp to the passed in values
 *
 * Parameters:
 *  ts - time buffer to update
 *  microSecs - value to set
 *
 * Returns:     error (if any)
 ***************************************************************************/
int TimeStamp_SetMicroSecs(Timestamp_t *ts, long microSecs)
{
    if (microSecs >= 1000000) {
        errno = EINVAL;
        return -1;
    } else {
        ts->frac = microSecs;
        return 0;
    }
} /* TimeStamp_SetMicroSecs() */

/****************************************************************************
 * int TimeStamp_SetMilliSecs(Timestamp_t *ts, long milliSecs)
 * Purpose:     Set the fractional portion of the timestamp to the passed in values
 *
 * Parameters:
 *  ts - time buffer to update
 *  milliSecs - value to set
 *
 * Returns:     error (if any)
 ***************************************************************************/
int TimeStamp_SetMilliSecs(Timestamp_t *ts, long milliSecs)
{
    if (milliSecs >= 1000) {
        errno = EINVAL;
        return -1;
    } else {
        ts->frac = milliSecs * 1000;
        return 0;
    }
} /* TimeStamp_SetMilliSecs() */

/****************************************************************************
 * void TimeStamp_SetSecs(Timestamp_t *ts, long secs)
 * Purpose:     Set the sec portion of the timestamp to the passed in values
 *
 * Parameters:
 *  ts - time buffer to update
 *  secs - value to set
 *
 * Returns:     nothing
 ***************************************************************************/
void TimeStamp_SetSecs(Timestamp_t *ts, long secs)
{
    ts->time = secs ;
} /* TimeStamp_SetSecs() */

/****************************************************************************
 * int TimeStamp_SetTime(Timestamp_t *ts, int hour, int min, int sec)
 * Purpose:     Set the specified values in the timestamp to the passed in values
 *
 * Parameters:
 *  ts - time buffer to update
 *  hour, min, sec - new values to set
 *
 * Returns:     0 = all ok; less than zero = invalid value given
 ***************************************************************************/
int TimeStamp_SetTime(Timestamp_t *ts, int hour, int min, int sec)
{
    int stat = 0;
    struct tm *tm = TimeStamp_LocalTime(ts);

    if (hour < 0 || hour > 24) stat += -4;
    if (min < 0 || min > 59) stat += -2;
    if (sec < 0 || sec > 59) stat += -1;
    if (stat == 0) {
        tm->tm_hour = hour == 24 ? 0 : hour;
        tm->tm_min = min;
        tm->tm_sec = sec;
        TimeStamp_MakeTime(ts, tm);
    }
    return stat;
} /* TimeStamp_SetTime() */

/****************************************************************************
 * char *TimeStamp_Str(Timestamp_t *ts, char *buffer, int fracDigits)
 * Purpose:     Display the time into the given buffer
 *
 * Parameters:
 *  ts - time to display
 *  buffer - where to store the results
 *  fracDigits - how many digits in the fractional portion of the time
 *
 * Returns:     buffer (in case you want to immediately use it)
 ***************************************************************************/
char *TimeStamp_Str(Timestamp_t *ts, char *buffer, int fracDigits)
{
    return TimeStamp_FormatStr(ts, buffer, dateTimeStr, fracDigits,
                        TIMESTAMP_STRING_LEN);
} /* TimeStamp_Str() */

/****************************************************************************
 * int TimeStamp_Test(Timestamp_t *ts1, Timestamp_t *ts2)
 * Purpose:     Test whether ts1 is less than, equal to or greater than ts2
 *
 * Parameters:  ts1, ts2 - times to test
 *
 * Returns:     -1 = less than, 0 = equal to, +1 = greater than 
 ***************************************************************************/
int TimeStamp_Test(Timestamp_t *ts1, Timestamp_t *ts2) 
{
    if (ts1->time < ts2->time) return -1;
    if (ts1->time > ts2->time) return +1;
    if (ts1->frac < ts2->frac) return -1;
    if (ts1->frac > ts2->frac) return +1;
    return 0;
} /* TimeStamp_Test() */

/****************************************************************************
 * int TimeStamp_TestCurrent(Timestamp_t *ts)
 * Purpose:     Test whether ts1 is <, =, or > than the current time
 *
 * Parameters:  ts1, ts2 - times to test
 *
 * Returns:     -1 = less than, 0 = equal to, +1 = greater than 
 ***************************************************************************/
int TimeStamp_TestCurrent(Timestamp_t *ts) 
{
    Timestamp_t current;
    TimeStamp_Current(&current);
    return TimeStamp_Test(ts, &current);
} /* TimeStamp_TestCurrent() */

 /****************************************************************************
 * char *TimeStamp_TimeStr(Timestamp_t *ts, char *buffer, int fracDigits) 
 * Purpose:     Generate string for time portion of timestamp
 *
 * Parameters:  
 *  ts - time to display
 *  buffer - where to store the results
 *  fracDigits - how many digits in the fractional portion of the time
 *
 * Returns:     buffer (in case you want to immediately use it)
 ***************************************************************************/
char *TimeStamp_TimeStr(Timestamp_t *ts, char *buffer, int fracDigits)
{
    return TimeStamp_FormatStr(ts, buffer, timeStr, fracDigits,
                               TIMESTAMP_STRING_LEN);
} /* TimeStamp_TimeStr() */

/****************************************************************************
 * void TimeStamp_Xlat (Timestamp_t *ts, boolean toNet) 
 * Purpose:     Provide host <-> network translation routine for type
 *
 * Parameters:  
 *  ts - object to translate
 *  toNet - direction of translation
 *
 * Returns:     nothing
 ***************************************************************************/
void TimeStamp_Xlat (Timestamp_t *ts, boolean toNet) 
{
    ts->time = (toNet ? htonl(ts->time) : ntohl(ts->time));
    ts->frac = (toNet ? htonl(ts->frac) : ntohl(ts->frac));
} /* TimeStamp_Xlat() */

#ifdef _OS9000
/****************************************************************************
 * static u_int32 localStrftime(char *buffer, u_int32 len, char *formatStr,
 *   struct tm *timeVal) 
 * Purpose:     
 *
 * Parameters:
 *   buffer - output buffer
 *   len - length of output buffer
 *   formatStr - how to display time
 *   timeVal - values to display 
 *
 * Returns:     number of characters added
 ***************************************************************************/
static u_int32 localStrftime(char *buffer, u_int32 len, char *formatStr,
                             struct tm *timeVal)
{
    if (strcmp(dateStr, formatStr) == 0) {
        sprintf(buffer, "%02d/%02d/%02d",
                timeVal->tm_mon+1, timeVal->tm_mday, timeVal->tm_year);
        return 8;
    } else if (strcmp(dateTimeStr, formatStr) == 0) {
        sprintf(buffer, "%02d/%02d/%02d %02d:%02d:%02d",
                timeVal->tm_mon+1, timeVal->tm_mday, timeVal->tm_year,
                timeVal->tm_hour, timeVal->tm_min, timeVal->tm_sec);
        return 17;
    } else if (strcmp(timeStr, formatStr) == 0) {
        sprintf(buffer, "%02d:%02d:%02d",
                timeVal->tm_hour, timeVal->tm_min, timeVal->tm_sec);
        return 8;
    } else {
        *buffer = '\0';
        return 0;
    }
} /* localStrftime() */
#endif

