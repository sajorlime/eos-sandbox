
/*
** Copyright (C) 1994, ICTV Inc. 14600 Winchester Blvd., Los Gatos, CA 95030 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: Any X86 project
**
** File: isX86.c instruction set X86.
**
**     $Id: isX86.c,v 1.4 1995/04/05 22:23:43 luke Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/utility/isX86.c,v $
**
** Purpose:
**  To provide instruction set access for functions not available in c.
**  These functions are only useful/available on os9k.
**
** Author: emil
** $Author: luke $
**
** Types/Clases:
**
** Functions:
**  int bitScanForward -- bit scan forward.
**  int bitScanReverse -- bit scan reverse.
**  int extract32 - extract 32 bits out of the two word r0 and r1.
**  int rotateLeft - extract 32 bits out of the two word r0 and r1.
**  int rotateRight - extract 32 bits out of the two word r0 and r1.
**  int swapShort - swaps the two bytes in i.
**  int swapLong - swaps the four bytes in i.
**
** Notes:
**
**
*/

#ifdef _OS9000

# define NO_ISX86_INLINE
# include "ictvcommon.h"

/*
int
bitScanForward (int bits)
{
*/
_asm
("
bitScanForward:
    bsf.l %eax,%eax
    ret
");
/*
}
*/


/*
int
bitScanReverse (int bits)
{
*/
_asm
("
bitScanReverse:
    bsr.l %eax,%eax
    ret
");
/*
}
*/

/*
int
swapShort (u_short i)
{
*/
_asm
("
swapShort:
    xchg.b %al,%ah
	and #0xffff,%eax
    ret
");
/*
}
*/

/*
int
swapLong (int i)
{
*/
_asm
("
swapLong:
    xchg.b %al,%ah
    ror.l #16,%eax
    xchg.b %al,%ah
    ret
");
/*
}
*/

/*
int
rotateRight (int r, int c)
{
*/
_asm
("
rotateRight:
CR equ 8
    push.l %ebp
    mov.l %esp,%ebp
    push.l %ecx
    mov.l CR(%ebp),%ecx
    ror.l %cl,%eax
    pop.l %ecx
    pop.l %ebp
    ret
");
/*
}
*/


/*
int
rotateLeft (int r, int c)
{
*/
_asm
("
rotateLeft:
CL equ 8
    push.l %ebp
    mov.l %esp,%ebp
    push.l %ecx
    mov.l CL(%ebp),%ecx
    rol.l %cl,%eax
    pop.l %ecx
    pop.l %ebp
    ret
");
/*
}
*/


/*
int
extract32 (int r0, int r1, int firstBit)
{
*/
_asm
("
extract32:
R1 equ 8
FirstBit equ 12
    push.l %ebp
    mov.l %esp,%ebp
    push.l %ecx
    push.l %edx
    mov.l FirstBit(%ebp),%ecx
    mov.l R1(%ebp),%edx
    shrd.l %cl,%edx,%eax
    pop.l %edx
    pop.l %ecx
    pop.l %ebp
    ret
");
/*
}
*/


void
x86_Cli (void)
{
    X86_CLI;
}

void
x86_sti (void)
{
    X86_STI;
}


# define ALLASM
# ifdef ALLASM
int
isX86Dummy (void)
{
}
# endif

#endif  /* #ifdef OS9000 */
