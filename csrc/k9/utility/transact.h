
/*
** Copyright (C) 1994, ICTV Inc. 14600 Winchester Blvd., Los Gatos CA 95030
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television
**
** File: transactionid.h - transaction id generation and decoding
**     $Name:  $
**     $Id: transact.h,v 1.3 1994/12/23 22:15:41 ljr Exp $
**
** Author:
**      Larry Rosenberg
**
** Purpose:
**     This file implements generating transaction ids. It also provides a
**     function for decoding them strictly for debugging purposes.
**
**
** Functions:
**     TransactionId_Init - store a unique identifier for each process that
**         generates a transaction id
**     TransactionId_Generate - create a unique transaction id
**     TransactionId_Decode - returns the relevant information contained in
**         a previously generated transaction id
**     
** Last Edited: Fri Dec 23 14:11:53 1994
** Modified:
*/

#ifndef __transactionidh
#define __transactionidh

#include "timestamp.h"

#define TRANSACTION_ID_STRING_LEN 18
typedef char TransactionId_t [TRANSACTION_ID_STRING_LEN+1+5];

#ifdef __cplusplus
extern "C" {
#endif

void TransactionId_Init(u_int16 id);

int TransactionId_Generate(TransactionId_t result);

int TransactionId_Decode(TransactionId_t transId, u_int32 *prefix,
                         Timestamp_t *ts, u_int32 *counter);

#ifdef __cplusplus
}
#endif

#endif     /* __transactionidh */

