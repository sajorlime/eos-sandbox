/*
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ICTV utilities
**
** File: .../libsrc/utility/usage.h
**
** Author: emil
**
** Purpose:
**  Defines external interface to usage.h
**
** Class/Typedefs:
**
** Functions Definitions:
**  usage -- check to see if there is -? option, if so print the
**   usage, and optionally exit.
**
**  usePrint -- does the printing for usage functions, can be called
**  publicly.
**
** Notes:
**
** $Id: usage.h,v 1.3 1994/08/01 22:47:43 emil Exp $
**
*/

# ifndef __USAGE_H__
# define __USAGE_H__

/*
**  usePrint - print an array of strings, preceeds print with Usage:
**  void usePrint (char * name, char ** usages) --
**  Prints name and usages as a usage definitions (See example below).
**  Arguments:
**      name -- the command name for the usage display.  If name
**      include "/" path components only the last part of the string
**      that follows the last path componet is used.
**
**      usages -- A Null terminated list of usage strings.
**
**  int usage (int argc, char ** argv, char ** usages, int doexit) --
**  Looks for the string "-?" or "?" in the argument list. If it
**  finds it prints the usage message using argv[0] as the name
**  parameter to usePrint.
**  Arguments:
**      argc -- the argument count.
**      argv -- the arguemnt list.
**      usages -- the usage strings.
**      doexit -- if non-zero exit is called with doexit as its parameter.
**
**  Example:
**
**  char * sUsages[] =
**  {
**      "[-o]",                 // the default paramters
**      "-o -- an option ",     // extended useage info
**      "-? or ? gives this message",
**      0                       // terminates with Null
**  };
**
**  char sOpt[] = "-o";
**
**  int
**  main (int argc, char ** argv)
**  {
**      usage (argc, argv, sUsages, 0);
**      if (arg_OptGet (argc, argv, sOpt, 0))
**      {
**          printf ("Got opt: %s\n", sOpt);
**      }
**      else
**          usage (argc, argv, sUsages, 1);
**
**      return 0;
**  }
**  progname -? prints:
**      Usage: progname [-o]
**          -o -- an option
**          -? or ? gives this message
*/

void usePrint (char * name, char ** usages);
int usage (int argc, char ** argv, char ** usages, int doexit);

# endif

