/*
** Copyright (C) 1994, ICTV Inc. 14600 Winchester Blvd., Los Gatos, CA 95030 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: ictv interactive television ictvMsg
** 
** File: ictvMsg.h - Provide basic message header accessor functions
**
**     $Id: ictvMsg.h,v 1.4 1995/11/04 02:04:13 wul Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/utility/ictvMsg.h,v $
** 
** Author: Larry Rosenberg
** 
** Purpose:
**      Provide functions for updating and retrieving information stored in
**      header of messages passed in the ictv network.
** 
** Functions:
**     ictvMsgFromByteStream - convert from byte stream to message structure
**     ictvMsgGetClientRecvTime - get the time the client received response
**     ictvMsgGetClientSendTime - get the time the client sent request
**     ictvMsgGetServerRecvTime - get the time the server received response
**     ictvMsgGetServerSendTime - get the time the server sent response
**     ictvMsgGetTransId - get the transaction id from header
**     ictvMsgPrintHeader - debugging routine to display header
**     ictvMsgPrintServerTimes - debugging routine to display time server spent
**     ictvMsgSetClientRecvTime - set the time the client received response
**     ictvMsgSetClientSendTime - set the time the client sent request
**     ictvMsgSetConnection - set information related to who is talking to whom
**     ictvMsgSetMessageCode - set the message code in the header
**     ictvMsgSetServerRecvTime - set the time the server received response
**     ictvMsgSetServerSendTime - set the time the server sent response
**     ictvMsgSize - return the size of the message header structure in bytes
**     ictvMsgToByteStream - convert from message structure to byte stream
**     ictvMsgXlat - translate the header from host order to network order
** 
** References:
**     See FrameMaker document Backoffice/ServerFormat.frame.
** 
** Last Modified: Thu Oct 12 19:19:16 1995
*/

#ifndef __ICTVMSG_H__
#define __ICTVMSG_H__

#include "rms.h"            /* USING rmsConnHndl */
#include "serverFormat.h"   /* USING ictvMessageHeader_t */
#include "timestamp.h"      /* USING TimeStamp_Current() */

                       /***************************
                       ** Macro access functions **
                       ***************************/

#define NO_ICTVMSG_INLINE
#ifndef NO_INLINE
#   ifndef NO_ICTVMSG_INLINE

#define ictvMsgGetClientRecvTime(head) ((head)->timingData.clientRecv)
#define ictvMsgGetClientSendTime(head) ((head)->timingData.clientSend)
#define ictvMsgGetMessageCode(head) ((head))->code)
#define ictvMsgGetServerRecvTime(head) ((head)->timingData.serverRecv)
#define ictvMsgGetServerSendTime(head) ((head)->timingData.serverSend )
#define ictvMsgGetStatus(head) ((head)->status)
#define ictvMsgGetTransId(head, transId) strcpy(transId, (head)->id)

#define ictvMsgSetClientRecvTime(head) \
    (TimeStamp_Current(&(head)->timingData.clientRecv)

#define ictvMsgSetClientSendTime(head) \
    (TimeStamp_Current(&(head)->timingData.clientSend)

#define ictvMsgSetMessageCode(head, request) ((head))->code = request)

#define ictvMsgSetServerRecvTime(head) \
    (TimeStamp_Current(&(head)->timingData.serverRecv)

#define ictvMsgSetServerSendTime(head) \
     (TimeStamp_Current(&(head)->timingData.serverSend)

#define ictvMsgSetStatus(head, stat) ((head)->status = stat)

#define ictvMsgSize(head) (sizeof(*head))

#   endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

                       /***************************
                       ** initialization routine **
                       ***************************/

void ictvMsgInitHeader(ictvMessageHeader_t *header,
    ServerMsgCode_t code, rmsConnHndl conn);

                      /******************************
                      ** debugging/tracing routine **
                      ******************************/

#ifdef ICTVDEBUG
void ictvMsgPrintHeader(ictvMessageHeader_t *header);
void ictvMsgPrintServerTimes(ictvMessageHeader_t *header);
#else
#define ictvMsgPrintHeader(header)
#define ictvMsgPrintServerTimes(header)
#endif

                          /*********************
                          ** access functions **
                          *********************/

#ifndef ictvMsgGetClientRecvTime
Timestamp_t ictvMsgGetClientRecvTime (ictvMessageHeader_t *header);
#endif

#ifndef ictvMsgGetClientSendTime
Timestamp_t ictvMsgGetClientSendTime (ictvMessageHeader_t *header);
#endif

#ifndef ictvMsgGetMessageCode
ServerMsgCode_t ictvMsgGetMessageCode(ictvMessageHeader_t *header);
#endif

#ifndef ictvMsgGetServerRecvTime
Timestamp_t ictvMsgGetServerRecvTime (ictvMessageHeader_t *header);
#endif

#ifndef ictvMsgGetServerSendTime
Timestamp_t ictvMsgGetServerSendTime (ictvMessageHeader_t *header);
#endif

#ifndef ictvMsgGetStatus
ServerStatus_t ictvMsgGetStatus(ictvMessageHeader_t *header);
#endif

#ifndef ictvMsgGetTransId
char *ictvMsgGetTransId (ictvMessageHeader_t *header, char *transId);
#endif

#ifndef ictvMsgSetClientRecvTime
void ictvMsgSetClientRecvTime (ictvMessageHeader_t *header);
#endif

#ifndef ictvMsgSetClientSendTime
void ictvMsgSetClientSendTime (ictvMessageHeader_t *header);
#endif

void ictvMsgSetConnection (ictvMessageHeader_t *header, rmsConnHndl conn);

#ifndef ictvMsgSetMessageCode
void ictvMsgSetMessageCode(ictvMessageHeader_t *header, ServerMsgCode_t code);
#endif

#ifndef ictvMsgSetServerRecvTime
void ictvMsgSetServerRecvTime (ictvMessageHeader_t *header);
#endif

#ifndef ictvMsgSetServerSendTime
void ictvMsgSetServerSendTime (ictvMessageHeader_t *header);
#endif

#ifndef ictvMsgSetStatus
void ictvMsgSetStatus (ictvMessageHeader_t *header, ServerStatus_t stat);
#endif

/* return the size (in bytes) of an ictv message header */
#ifndef ictvMsgSize
size_t ictvMsgSize (ictvMessageHeader_t *header);
#endif

           /****************************************************
           ** HOST <-> NETWORK byte order translation routine **
           ****************************************************/

/* Provide network <--> host order translation */
/* Called by ictvMsgFromByteStream and ictvMsgToByteStream */
void ictvMsgXlat(ictvMessageHeader_t *header, Bool toNet);

/* routines to convert header to and from byte stream */
void ictvMsgFromByteStream (ictvMessageHeader_t *header, void *result);
void ictvMsgToByteStream (ictvMessageHeader_t *header, void *result);

#ifdef __cplusplus
}
#endif

#endif

