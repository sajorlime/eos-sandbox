
/*
** COPYRIGHT (C) 1994, ICTV Inc. 280 Martin Ave. Santa Clara, Calif 95050. 280
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: Utility Library
**
** File: argsget.h - 
**
** Author: emil
**
** Purpose:
**  Defines the argsget function family. Code to get the command line
**  options according to a string.
**
** Types/Clases:
**
** Functions Prototypes:
**  arg_OptGet -- returns a single option and removes it from the list if
**  it takes a parameter.
**
**  arg_OptRemove -- removes all options from the argument list, it
**  returns a string (addr passed to it) with all of the options and
**  the count of the options copies. The source of the options is nulled.
**
**  arg_OptAdjust -- after arg_OptGet and optremove have removed options from
**  the list it adjusts the list and returns a new argument count.
**  It can also be used after argnext.
**
**  arg_Next -- returns the next non-null argument, it make the
**  argument it gets null.
**
**  arg_Position -- get a argument by position. Null the argument.
**
**  arg_Last -- get the last argument it the list. Null the argument.
**  
**  arg_Copy -- copies all of the arguments into a string.
**  
**
** Usage:
**  See Below.
**
** Notes:
**  The routines in this module form a family that allow the user to
**  get options and arguments on a as needed basis.
**
** $Id: argsget.h,v 1.10 1995/11/04 02:04:11 wul Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/utility/argsget.h,v $
**
*/

# ifndef __ARGSGET_H__
# define __ARGSGET_H__

# include "ictvcommon.h"

#ifdef __cplusplus
extern "C" {
#endif

/*  char * arg_OptGet (int argc, char ** argv, char * opt, int copy)
**  -- gets an option that matchs the string in opt.  If copy is greater
**  than zero it replaces
**  the opt string with the option value (upto copy - 1 characters)
**  and returns a pointer to that string. If it does not find a match
**  it returns null. If copy is false
**  it returns and it finds the option it return a pointer to the option
**  but does not replace the string.
**  arg_OptGet Null out non-boolean options. It can deal with either
**  single option flags with or w/o parametes, or it can deal with
**  groups of boolean options as it "-abc" where 'a', 'b', and 'c'
**  are all seperate boolean options.
**  Usage:
**      char sFileName[256] = "-f" 
**      char sReverse[] = "-r" 
**
**      char * cp;
**
**      cp = arg_OptGet (argc, argv, sFileName, sizeof (sFileName));
**      if (fp)
**          strcpy (sFileName, sDefaultName);
**      cp =  arg_OptGet (argc, argv, sReverse, 0);
**      reverse = !(!cp);
*/
char * arg_OptGet (int argc, char ** argv, char * opt, int copy);


/* int arg_OptRemove (int argc, char ** argv, char * optList)
**  arg_OptRemove cleans up after OptGet is called for all the desired
**  boolean options.  It copies all of the remaning option fields
**  into the optlist parameters if it is not Null and returns a count
**  of the options it removed.  It removes the options by nulling the
**  argv loction that pointed to it.  It copies both '-' and the
**  option value into optlist.
**  Usage:
**      char optList[64];
**
**      -- First call arg_OptGet for all non-boolean options, and all
**      -- boolean options that we want now.
**      -- This leaves the argv with Null where non-boolean options
**      -- where removed, and with some boolean options.
**
**      i = arg_OptRemove (argc, argv, optList)
**      if (i)
**      {
**          -- print usage, or whatever else might be useful.
**          -- note this can also be a way to get the options.
**      }
*/
int arg_OptRemove (int argc, char ** argv, char * optList);

/* int arg_OptAdjust (int argc, char ** argv)
** 
** Usage:
**  argc = arg_OptAdjust (argc, argv);
**  if (argc < 2)
**  {
**      fprintf ("Usage: ...
**      exit (BADARGS);
**  }
*/
int arg_OptAdjust (int argc, char ** argv);

/* char * arg_Next (int argc, char ** argv)
**  gets the next non-Null argument in the argv list.  It Nulls the
**  argument after. 
** 
** Usage:
**  while ((argp = arg_Next (argc, argv)) != 0)
**      useArg (argp);
*/
char * arg_Next (int argc, char ** argv);

/* char * arg_Last (int pos, char ** argv)
**  Like arg_Next, but from the end of the list. 
**  It Nulls the argv pointer.
** 
** Usage:
**  while ((argp = arg_Last (argc, argv)) != 0)
**      useArg (argp);
*/
char * arg_Last (int argc, char ** argv);

/* char * arg_Position (int pos, char ** argv)
** Returns the pointer at position, regardless of the value.  It
** Nulls the pointer.
** 
** Usage:
**  while ((argp = arg_Position (pos, argv)) != 0)
**      useArg (argp);
*/
char * arg_Position (int pos, char ** argv);


/* char * arg_Copy (int argc, char ** argv, char * command, int n)
** Copies the arguments to a string, saving or reforming the command line.
** 
** Usage:
**  char commandline[80];
**
**  arg_Copy (argc, argv, commandline, sizeof (commandline));
**
**  or
**
**  char * clp;
**  clp = arg_Copy (argc, argv, 0, 0);
**
**  In this case arg_Copy mallocs the string space required.
*/
char * arg_Copy (int argc, char ** argv, char * command, int n);

/* char *
** arg_getIValue (int argc, char ** argv, const char * optp, u_int * dip)
** Gets an integer value from the arguments without affecting the
** option pointer (optp).  It only assigns a new value to the
** integer if the option exists.
**
** Usage:
**  int x = 5;
**
**  arg_getIValue (argc, argv, "-Myoption", &x);
**  -- x change iff -Myotion is in the argument list.
*/
char *
arg_getIValue (int argc, char ** argv, const char * optp, u_int * dip);


/* char *
** arg_getBValue (int argc, char ** argv, const char * optp, Bool * dip)
** Gets an boolean value from the arguments without affecting the
** option pointer (optp).  It only assigns a new value to the
** integer if the option exists.
**
** Usage:
**  Bool b = FALSE;
**
**  arg_getBValue (argc, argv, "-MBoolOptint", &b);
**  -- b change iff -MyBoolOption is in the argument list.
*/
char *
arg_getBValue (int argc, char ** argv, const char * optp, Bool * dbp);

#ifdef __cplusplus
}
#endif

# endif

