/*
 ** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, CA 95050 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
 ** 
 ** Project: ictv interactive television FOO
 ** 
 ** File: shiftopt.c - A command option parser like UNIX getopt()
 **     $Id: shiftopt.c,v 1.7 1994/12/14 05:04:20 aylwin Exp $
 **     $Source: /import/cvsfiles/ICTV/./libsrc/utility/shiftopt.c,v $
 **
 ** Author:
 **    Aylwin Stewart
 **
 ** Purpose:
 **     Shiftopt is used to parse command options and their arguments from the
 **  argument vector associated with a commands invocation.  It leaves regular
 **  arguments in the 
 ** 
 ** Types/Classes:
 ** 
 ** Functions:
 **     shiftoptPrintError -- enable/disable shifopt errror printing
 **     shiftopt -- parse arguments in argument vector
 **     shiftarg -- shift remaining arguments down one
 ** 
 ** Assumptions:
 ** 
 ** Limitations:
 **     Optarg points to the correct value only up to the next invocation. 
 ** 
 ** Notes:
 ** 
 ** References:
 **     See getopt UNIX man page 
 ** 
 */

#include <stdio.h>
#include <string.h>
#include "ictvcommon.h"
#include "shiftopt.h"

#define __SHIFTOPT_C__

/*****		M A C R O S		*****/
#ifdef __NeXT__
#define strchr		index
#endif

/*
 * ERR: Print an error message to stderr preceded by invoked
 * command and followed by offending option character.
 */
#define ERR(str,ch) if(Opterr) fprintf( stderr, "%s %s %c\n",argv[0], str, ch )

/* 
 * UNKNOWN: The char returned when an 
 * unknown option letter is found.
 */
#define UNKNOWN '?'


/*****		G L O B A L S		*****/
/*
 * Opterr: Enable/disable error printing, enabled by default.
 */
static Bool Opterr = YES;

/*
 * Optarg: pointer to current options argument.
 */  
char *Optarg;

/*
 * Indx: current argument in argv, used in shiftopt()
 */
static int Indx = 1;	/* indx of next argument */

/*
 * Sp: next char to look at in argv[indx], used in shiftopt() 
 */
static int Sp = 1;		

/*****      D E C L A R A T I O N S	*****/
static void shiftarg( int *argcp, char **argv, int indx );

/*****        F U N C T I O I N S 	*****/

/*
 * ShiftoptPrintError(): enable/disable error printing
 */

void
shiftoptPrintError( Bool flag )
{
    Opterr = flag;
    return;
}


/*
 * resetShiftOpt(): set Indx & Sp to '1'
 */
void
resetShiftopt( void )
{
    Indx = 1;
    Sp   = 1;
    return;
}


/*
 * Shiftopt(): argument vector options parser.
 */

int
shiftopt (int *argcp, char **argv, char *opts)
{
    register int c;
    register char *cp;

    while (1)
	{
	    if (Sp == 1)
		{
		    if (Indx >= *argcp)
			return (EOF);

		    if (!argv[Indx][0])
			continue;
		    if (argv[Indx][0] != '-' || argv[Indx][1] == '\0')
			{
			    Indx++; /* skip non-option arguments */
			    continue;
			}
		}
	    /* capture next option letter */
	    c = argv[Indx][Sp];

	    /* if the next option character is not in the `opts'
	       list, it is an error. */
	    if (c == ':' || (cp = strchr (opts, c)) == NULL)
            {
                if ( Opterr )
                    GLOBAL_MSG(
                        ("option requires an argument --%c\n", c),
                        DBG_ASSERT
                    );

                if (argv[Indx][++Sp] == '\0')
                    {
                        /* shift out the erroneous option */
                        shiftarg (argcp, argv, Indx);

                        Sp = 1;
                    }
                return (UNKNOWN);
            }
	    /* see if this option needs an argument */
	    if (*++cp == ':')
	    {
		/* does argument follow without white space ? */
		if (argv[Indx][Sp + 1] != '\0')
		{
		    Optarg = &argv[Indx][Sp + 1]; /* save arg  */
		    shiftarg (argcp, argv, Indx); /* shift out */
		}
		else if ((Indx + 1) < *argcp)
		{	/* ELSE see if any more args in list */
		    shiftarg (argcp, argv, Indx); /* dump flag */
		    Optarg = argv[Indx]; /* save arg  */
		    shiftarg (argcp, argv, Indx); /* shift out */
		}
		else
		{
		if ( Opterr )
		    GLOBAL_MSG(("option requires an argument --%c\n", c),
				DBG_ASSERT );

		Sp = 1;
		shiftarg (argcp, argv, Indx); /* shift out bad option */
		return (UNKNOWN);
		}
            Sp = 1;
        }
        else
        {
            if (strlen (argv[Indx]) > 2)    /* more than one option */
                strcpy (&(argv[Indx][1]), &(argv[Indx][2]));    /* remove first option  */
            else if (argv[Indx][++Sp] == '\0')
            {
                shiftarg (argcp, argv, Indx);
                Sp = 1;
            }
            Optarg = (char *) NULL;    /* no argument */
        }
        return c;
    }
}


/*
 * Shiftarg(): Shift remaining arguments down by one.
 */

static void
shiftarg( int *argcp, char **argv, int indx )
{
  if( indx >= *argcp )
    return;

  (*argcp)--;

  for(; indx < *argcp; indx++ )
    argv[ indx ] = argv[ indx + 1 ];
}

