
/*
** Copyright (C) 1994, 1995,
** ICTV Inc.
** 14600 Winchester Blvd.
** Los Gatos, CA 95030
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ICTV debug print facility.
**
** File: ictvdebug.c
**
** Author: Burce Nilo, et. al.
**
** Purpose:
**  Provides a uniform debug output methodogy.
**
** Types/Clases:
**
** Functions:
**
** Notes:
**
** $Id: ictvdebug.c,v 1.27 1995/11/04 02:04:13 wul Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/utility/ictvdebug.c,v $
**
*/

/* I am defining this for this module when it is to be included
   in a library. This will not work correctly if linked directly
   into an executable. BDN */

#ifndef ICTVDEBUG
#define ICTVDEBUG
#endif

#ifdef _OS9000
  #include <types.h>
  void * __inline_va_start__(void) ;
#endif

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "ictvdebug.h"
#include "ictvcommon.h"
#include "timestamp.h"


#ifdef MALLOC_D_MASKS
#include "ipc.h"
#include "MMCConfig.h"
#include "MemHeap.h"
#endif


/* various protos and declarations dependent on os*/

#ifdef _SUN4
int getpid(void);
static int our_pid;
#endif
#ifdef _AIX
 pid_t getpid(void);
static pid_t our_pid;
#endif
#ifdef _OS9000
static int our_pid;
#endif

#define ON 1
#define OFF 0


/* The default is to ignore local masking and execute all global levels. 
   The local masks are set to DBG_NONE. */
/* Default is to set everything to warn until some later date.
** Note that the the number of intialized masks is DBG_MAX_MASKS, (now 32) 
** which is the maximum possible # of masks.
*/

#ifdef MALLOC_D_MASKS
static mmcc_DebugMask * mmccDebugMask = NULL;
static u_char * DebugMasks;
char * my_procName = NULL;
/*proto for mallocing masks*/
static void  dbg_malloc_masks(char *);
#else
static int DebugMasks[DBG_MAX_MASKS] =
{
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL,
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL,
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL,
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL,
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL,
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL,
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL,
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL
};
#endif


/* DECLARATIONS for this file*/
/*buffers*/
static char Debug_buffer[2000];

static char prog_tag[255]="";
char name_buffer[255]="noname";

/*file handles*/
FILE * debug_fd = stderr;
static int  debug_ipc_set=-1;


/*SET STATE of one indicated mask
 */

void dbg_setDebugState (int index, int state)
{
#ifdef MALLOC_D_MASKS
  if(mmccDebugMask == NULL)
    dbg_malloc_masks(NULL);
#endif    

    DebugMasks[index] = state;
}

int dbg_getDebugState (int index)
{
#ifdef MALLOC_D_MASKS
  if(mmccDebugMask == NULL)
    dbg_malloc_masks(NULL);
#endif    

    return (DebugMasks[index]); 
}


/*
 * set state of all masks
 *
 */
void debug_initAllMasks(int state)
{
  int i ;
#ifdef MALLOC_D_MASKS
  if(mmccDebugMask == NULL)
    dbg_malloc_masks(NULL);
#endif    

  for (i = 0 ; i < DBG_MAX_MASKS ; i++)
    DebugMasks[i] = state ;
}

/* 
 * set up debug:
 *
 * 1) get ipc set
 * 2) get semas
 * 3) set all masks
 */
int log_SetUp(char* name)
{
    debug_SetProgTag(name, 'y');
    debug_initAllMasks(DBG_WARN_LEVEL) ;

    return (0);
}
#ifdef MALLOC_D_MASKS
static void  dbg_malloc_masks(char * my_procName)
{
/* if we don't know our mailbox name, find it, so that we can get our
 *  debug_masks
 */
  if (my_procName != NULL) /* check for null configp here??*/
    {
      mmccDebugMask=mmcc_getDebugMask(my_procName);/* use name as string*/
      if (mmccDebugMask!=NULL)
    DebugMasks=mmccDebugMask->mask;
    }
  if (DebugMasks==NULL)
    DebugMasks=mem_malloc(DEFAULT_HEAP_ID,DBG_MAX_MASKS *sizeof(u_int) );


}
#endif
static int
debug_SetUp_NoIPC(char * name)
{

      debug_ipc_set=-1;
      return(1);
}



char*
debug_GetProgTag()
{
   if (prog_tag[0] == '\0') return 0;

   return prog_tag;
}

void debug_SetProgTag(char *name, char appendPID)
{
    if(name) {
    strncpy(prog_tag, name, 254);
    prog_tag[254] = '\0';
    }
    if(appendPID) {
        int32 nameLen = strlen(name);
    our_pid = getpid();
    
    if(nameLen > 240) { /* Allow long PID strings... */
        nameLen = 240;
    }
    /* Add the pid to the prog_tag */
    sprintf(&prog_tag[nameLen], "_%d ", our_pid);
    }

#ifdef NOT_DESIREABLE
    debug_initAllMasks(DBG_WARN_LEVEL) ;
#endif

    return;
}

# ifndef DBG_LINE_LEN
#   define DBG_LINE_LEN 79
# endif
static int DBGLineLen = DBG_LINE_LEN;

int
dbg_setLineLength (int len)
{
    int i;

    i = DBGLineLen;
    if (len > (sizeof (Debug_buffer) - 8))
        len = sizeof (Debug_buffer) - 8;
    DBGLineLen = len;
    return i;
}

/*
 * test to see if we actually need to do the debug msg
 */
int
dbg_maskDebugp (int index, int gmask, int lmask)
{
    if
    (
         (gmask & DebugMasks[DBG_GLOBAL_MASKINDEX])
         && (lmask & DebugMasks[index])
    )
        return 1;
    return 0;
}

/* actually processing the debug message*/
void
Do_DebugMsg(int lineno, char * filename, int maskindex, char * msg_buffer )
{
  char time_str[25];
  int32 msg_buf_len = 0;
  int32 header_len = 0;


  /* TimeStamp_CurrentStr(time_str,0);*/

    TimeStamp_CurrentTimeStr(time_str,2);    

    if(((DebugMasks[DBG_GLOBAL_MASKINDEX] & DBG_TERSE_BIT) == 0) && 
       ((DebugMasks[maskindex] & DBG_TERSE_BIT) == 0)) {
      /* Output the debug header. */
      if(prog_tag[0] != '\0') {
        fprintf(debug_fd,"%s F:%s:%d T:%s%n",
        prog_tag, filename, lineno, time_str, &header_len);
      } else {
        fprintf(debug_fd,"F:%s:%d T:%s%n",
        filename, lineno, time_str, &header_len);
      }
    }

    
    /* Make sure the message ends with a newline. */
    if(msg_buffer[msg_buf_len = strlen(msg_buffer) -1] != '\n') {
      msg_buffer[++msg_buf_len] = '\n';
      msg_buffer[++msg_buf_len] = '\0';
    }
    
    /* Header and message won't fit on one line, put a newline between them */
    if((msg_buf_len + 2 + header_len) > DBGLineLen)
        fprintf(debug_fd, "\n");
    
    /* Output the message and flush the buffer. */
    fprintf(debug_fd," %s",msg_buffer);
    fflush(debug_fd);
}



/* this ugly code is nescessary to hijack the printf args to sprintf args*/
/* it works*/
char *
PRINTF(char * format,...) 
{
    va_list args;   /* required for var args */

    va_start (args, format);    /* setup */

    vsprintf (Debug_buffer, format, args); /* all of that just for this? */
    /* cut down to 256 or DBGLineLen chars which ever is greater */
    if (strlen (Debug_buffer) > 256)
    {
        if (DBGLineLen > 256)
        {
            if (strlen (Debug_buffer) > DBGLineLen)
                strcpy (&(Debug_buffer[DBGLineLen - 7]), " TRUNC");
        }
        else
            strcpy(&(Debug_buffer[256 - 8]), "  TRUNC");
    }
    va_end (args);

    return Debug_buffer;    /* return the pointer */
}

#ifndef _OS9000

int debug_Open(int fd)
{
   our_pid= getpid();
   debug_fd= fdopen(fd, "w");

   debug_initAllMasks(DBG_WARN_LEVEL) ;

   return (0);

}


int debug_filed()
{
   return fileno(debug_fd);
}

#endif /* ifdef _OS9000 */
