/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, CA 95050 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: Utility Library
** 
** File: shiftopt.h - header file for the shiftopt functions
**     
**     $Id: shiftopt.h,v 1.6 1994/12/14 05:04:21 aylwin Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/utility/shiftopt.h,v $
**
** Author:
**     Aylwin Stewart
** 
** Purpose:
**     Shiftopt() provides a functionality very similar to the standard
**     UNIX getopt() function.  There is no optind global variable, but since 
**     shiftopt() shifts its arguments, the next argument to be processed is 
**     always argv[1].
** 
** Types/Classes:
** 
** Functions:
**     shiftoptPrintError - set wether shiftopt prints error messages
**     shiftopt - process argv arguments similar to UNIX getopt()
** 
** Assumptions:
** 
** Limitations:
**     Optarg points to the correct value only up to the next invocation. 
** 
** Notes:
**     Updates argc as well, so it will change after a shiftopt call
** 
** References:
**     See UNIX man page getopt
**     See test code in shiftoptTest.c for complete examples.
** 
*/

#ifndef __SHIFTOPT_H__
#define __SHIFTOPT_H__

#include "ictvcommon.h"

/*****************************************************************************
 * Optarg: copy of most recently processed option argument
 *****************************************************************************/
extern char *Optarg;

/*****************************************************************************
 * ShiftoptPrintError(): set a flag which determines wether or not
 * shiftopt prints an error message when it encounters an illegal
 * option, or an option which is missing an argument.
 *
 * Usage:
 *    shiftoptPrintError( YES ); -- print an error message when an
 * unexpected argument is found, or an option is missing its argument.
 * shiftoptPrintError( NO );  -- don't print any error messages
 *****************************************************************************/
extern void shiftoptPrintError( Bool flag );

/*****************************************************************************
 * ResetShiftOpt(): allow shiftopt to be used for parsing more than one
 * I argument vector per main, by resetting statically  maintained variables.
 * Should only be called after fulling parsing and using an argument vector.
 *****************************************************************************/
extern void resetShiftopt( void );



/*****************************************************************************
 * Shiftopt(): parse the option flags, and their arguments, from left to right
 * in a command invocation.  Leave remaining arguments in argv, with argc set
 * to the number of arguments remaining.
 *
 * Usage:
 *	  main(int argc, char *argv[])
 *	  {
 *	       int c;
 *	       .
 *	       .
 *	       .
 *	       while ((c = shiftopt(&argc, argv, "abf:o:")) != EOF)
 *		    switch (c) {
 *		    case `a':
 *			 if (bflg)
 *			      errflg++;
 *			 else
 *			      aflg++;
 *			 break;
 *		    case `b':
 *			 if (aflg)
 *			      errflg++;
 *			 else
 *			      bproc();
 *			 break;
 *		    case `f':
 *			 ifile = Optarg;
 *			 break;
 *		    case `o':
 *			 ofile = Optarg;
 *			 break;
 *		    case `?':
 *		    default:
 *			 errflg++;
 *			 break;
 *		    }
 *	       if (errflg) {
 *		    fprintf(stderr, "Usage: ...");
 *		    exit(2);
 *	       }
 *	       .
 *	       .
 *	       .
 *	  }
 *****************************************************************************/
extern int shiftopt( int *argcp, char *argv[], char *opts );

#endif /*__SHIFTOPT_H__*/
