/*
** Copyright (C) 1994, ICTV Inc. 14600 Winchester Blvd. Los Gatos, CA 95030.
** All rights reserved.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: ictv interactive television
** 
** iassert implements a better assert routine.
**
** $Source: /import/cvsfiles/ICTV/./libsrc/utility/iassert.c,v $
**
** Purpose:
**
** $Log: iassert.c,v $
** Revision 1.2  1995/01/07 01:17:13  george
** Fixed for OS9000.
**
** Revision 1.1  1995/01/06  06:40:18  george
** Added iassert to support vodServer.
**
*/


#include <stdio.h>
#include <signal.h>

#ifndef _OS9000
#       include <unistd.h>
#else
#       include <stdlib.h>
#endif

#include "iassert.h"

static char sccs_id[]="$Id: iassert.c,v 1.2 1995/01/07 01:17:13 george Exp $";


void _iassert(char         *pModule,
              unsigned int lineNo)
{
    fprintf(stderr, "\nAssertion failed at %s:%d\n",
            pModule, lineNo);


#ifdef _OS9000
    /* The brute-force method: generate a SEGV.  Use this if the OS
       doesn't support kill() */
    *(int *) 0 = 0;
#else
    /* Generate a core dump */
    kill(getpid(), SIGABRT);


    /* Give it time to work */
    sleep(5);


    /* One more time.  Why haven't we dumped core yet? */
    kill(getpid(), SIGABRT);
#endif


    /* Make sure we don't return */
    fprintf(stderr, "\n_iassert: EXIT!\n");
    exit(1);

    if (sccs_id) {}  /* Kill the warning */

}  /* _iassert() */
