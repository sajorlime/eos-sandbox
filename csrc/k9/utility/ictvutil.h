/*
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ICTV utilities
**
** File: .../libsrc/utility/ictvutil.h
**
** Author: emil
**
** Purpose:
**  Defines external interfaces for all utilites in the direcectory.
**
** Class/Typedefs:
** Functions Definitions:
**  See included files.
** $Id: ictvutil.h,v 1.6 1994/11/24 02:20:14 aylwin Exp $
**
*/

#ifndef __ICTVUTIL_H__
#define __ICTVUTIL_H__

#include "ictvcommon.h"
#include "argsget.h"
#include "shiftopt.h"
#include "usage.h"

#ifdef _OS9000
    #include "multiproc.h"
#endif

#endif

