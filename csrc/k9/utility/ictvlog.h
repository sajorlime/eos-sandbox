#ifndef _ICTVLOG_H
#define _ICTVLOG_H
/* top level header file for the entire audit/alarm console/ops
 * subsystem.
 */

enum Log_msg_type
{
Log_dont_care,Log_test_base, Log_test_1
};

enum msgs
{
DEBUG_MSG,LOG_MSG,OPS_MSG,ALARM_MSG,AUDIT_MSG
};

enum log_levels
{
Log_off,Log_info,Log_warn,Log_assert,Log_fatal
};




/*
 * Prototypes for audit and alarm
 */
#ifdef __cplusplus
extern "C" {
#endif
int  log_SetUp( char * );
void log_end(void);
int dbg_setLineLength (int );

/* set and get functions*/
void set_LogMask(int);
int  get_LogMask(void);
void  alarm_off(void);
void  alarm_on (void);
void  audit_off(void);
void  audit_on (void);
void  set_OpsMask(int);
int  get_OpsMask(void);

/* helper functions, NOT part of API*/
void Do_LogMsg(int, int, int,char *);
char * PRINTF(char *,...);
#ifdef __cplusplus
}
#endif
/*
 * log macros: part of API
 */

#define LOG(pargs,msgtype,level) \
            Do_LogMsg(LOG_MSG,level,msgtype,PRINTF pargs )

#define OPS(pargs,msgtype,level) \
            Do_LogMsg(OPS_MSG,level,msgtype,PRINTF pargs )


#define AUDIT(pargs,msgtype,level) \
            Do_LogMsg(AUDIT_MSG,level,msgtype,PRINTF pargs )

#define ALARM(pargs,msgtype) \
            Do_LogMsg(ALARM_MSG,Log_dont_care,msgtype,PRINTF pargs )


#endif
