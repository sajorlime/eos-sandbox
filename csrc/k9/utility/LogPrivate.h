/* Our own header, to be included *after* all standard system headers */

#ifndef	_LogPrivate_h
#define	_LogPrivate_h

#ifdef _OS9000
#include	<types.h>
#else
#include	<sys/types.h>		/* required for some prototypes*/
#include	<stdlib.h>		/* for convenience */
#endif


#include	<stdio.h>		/* for convenience */
#include	<string.h>		/* for convenience */
#ifndef _OS9000
#include	<unistd.h>		/* for convenience */
#endif

#include "semaph.h"
#include "ictvcommon.h"
#include "ictvtypes.h"

typedef struct {
  int type;
  int seq;
  int len;
  char body[100];
}LogMsg;
    

typedef struct {
  /* base of our memory area*/
  char guard[12];
  int head; /* head of our available */
  int tail; /* tail */
  int full; /* is q full (saves us 1 buffer)*/
  int max;  /* max number of things*/
  int errors; /* opsies*/
  int pid;/* pid of daemon, for signal type wakeup*/
} Q_info;


#define	MAXLINE	256			/* max line length */
#define Debug_QSize  20			/* size of q, in messages*/


#ifndef _OS9000

#define LOCK_SEM_KEY   25525
#define SIGNAL_SEM_KEY 25520
#define Debug_IPC_filename "/tmp/log_proc"



#else

#define LOCK_SEM_KEY   "debug_lock_sema"
#define SIGNAL_SEM_KEY "debug_signal_sema"
#define Debug_IPC_modulename "debug_ipc"
#define CUR_ATTR mkattrevs(MA_REENT,1)


#endif

int		 daemon_init(void);			/* {Prog daemoninit} */

/* prototypes for our own functions */

int DeQ(Q_info *,LogMsg *,LogMsg*,sema_id);
int DeQ1(Q_info *,LogMsg *,LogMsg * );
int En_Q( Q_info *,LogMsg *,LogMsg *,sema_id );
void initQ(Q_info *);
void * debug_SetUp_file(int);
int debug_SetUp_semas(sema_id *, sema_id * ,int);
void debug_close_semas(sema_id , sema_id );
#endif	/* _LogPrivate_h */
