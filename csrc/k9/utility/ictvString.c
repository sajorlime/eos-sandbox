/*
** Copyright (C) 1995
** ICTV Inc. 14600 Winchester Blvd., Los Gatos, CA 95030 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: ictv interactive television utilities
** 
** File: ictvString.c - desired string functions not supported by K9.
**     $Id: ictvString.c,v 1.2 1995/02/09 23:03:37 emil Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/utility/ictvString.c,v $
**
** Author(s):
**    Aylwin Stewart, emil
**
** Purpose:
**     A single place to add desired generic string functions not
**     supported by K9.
** 
** 
*/

#include "ictvtypes.h"

#include "ictvString.h"

/*****                     M A C R O S                     *****/


/*****                      E N U M S                      *****/


/*****            D A T A   S T R U C T U R E S            *****/


/*****                    G L O B A L S                    *****/                  


/*****       F U N C T I O N   D E F I N I T I O N S       *****/

int tolower (char);

/* 
 * Stricmp: a case insensitive string compare
 */
int 
stricmp (char * s1p, char * s2p)
{
	char c1;
	char c2;
	int i;

	while ((i = (c1 = *s1p++) - (c2 = *s2p++)) == 0)
	{
		if (!c1)
			return i;
		if (tolower (c1) != tolower (c2))
			return i;
	}
	return i;
}


/*
** 
*/
int
safeStrCmp (char * s1p, char * s2p)
{
	char c;
	int i;

	if (!s1p)
	{
		if (!s2p)
			return 0;
		else
			return -1;
	}
	if (!s2p)
		return 1;
	while ((i = (c = *s1p++) - *s2p++) == 0)
	{
		if (!c)
			return i;
	}
	
	return i;
}



/*
** 
*/
int
safeStrICmp (char * s1p, char * s2p)
{
	char c1;
	char c2;
	int i;

	if (!s1p)
	{
		if (!s2p)
			return 0;
		else
			return -1;
	}
	if (!s2p)
		return 1;
	while ((i = (c1 = *s1p++) - (c2 = *s2p++)) == 0)
	{
		if (!c1)
			return i;
		if (tolower (c1) != tolower (c2))
			return i;
	}
	return i;
}


