/*
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: code to get the command line option -? and print the usage.
**
** File: .../libsrc/utility/usage.c
**
** Author: emil
** $Author: emil $
**
** Purpose:
**  This file is part of the utility library
**
** Class/Typedefs:
**
** Functions:
**  usage -- check to see if there is -? option, if so print the
**   usage, and optionally exit.
**
**  usePrint -- does the printing for usage functions, can be called
**  publicly.
**
** Notes:
**
** $Id: usage.c,v 1.7 1994/08/19 05:04:56 emil Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/utility/usage.c,v $
**
*/

# include <stdio.h>
# include <string.h>
# include <stdlib.h>

# include "ictvutil.h"

# define __USAGE_C__

/*
**  usePrint - print an array of strings, preceeds print with Usage:
*/
void
usePrint (char * name, char ** usages)
{
    char * cp;
    
    if ((cp = strrchr (name, '/')) != 0)
        cp++;
    else
        cp = name;
    fprintf(stderr, "\nUsage: %s %s", cp, *usages++);
    while ((cp = *usages++) != 0)
    {
        fprintf(stderr, "\n    ");
        fprintf(stderr, cp);
    }
    fprintf(stderr, "\n");
}

static char sUsage0[] = "-?";
static char sUsage1[] = "?";
int
usage (int argc, char ** argv, char ** usages, int doexit)
{
	if (!argv[1])
		return 0;
    if
	(
		arg_OptGet (argc, argv, sUsage0, 0)
		|| arg_OptGet (argc, argv, sUsage1, 0)
	)
    {
        usePrint (argv[0], usages);
        if (doexit)
            exit (doexit);
        return 1;
    }
    return 0;
}



