
/*
** Copyright (C) 1994, 1995,
** ICTV Inc.
** 14600 Winchester Blvd.
** Los Gatos, CA 95030
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ICTV debug print facility -- MMC version.
**
** File: ictvdebug_mmc.c
**
** Author: Burce Nilo, et. al.
**
** Purpose:
**  Provides a uniform debug output methodogy.
**
** Types/Clases:
**
** Functions:
**
** Notes:
**
** $Id: ictvdebug_mmc.c,v 1.2 1995/11/04 02:04:15 wul Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/utility/ictvdebug_mmc.c,v $
**
*/

/* I am defining this for this module when it is to be included
   in a library. This will not work correctly if linked directly
   into an executable. BDN */

#ifndef ICTVDEBUG
#define ICTVDEBUG
#endif

#include <types.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#ifndef USE_SIGNAL_SEMA
#include <signal.h>
#endif

#include "ictvdebug.h"
#include "ictvcommon.h"
#include "globMMC.h"
#include "timestamp.h"
#include "MMCConfig.h"
#include "rms.h"

#ifdef MALLOC_D_MASKS
#include "ipc.h"
#include "MemHeap.h"
#endif


#define ON 1
#define OFF 0

#define MSGLEN_MAX      (2000)
#define DEBUGHDR_LEN    (255)
#define PROGTAG_LEN     (128)

#define MAX_NETWORK_RETRIES   (10)


static rmsConnHndl NetworkConn = NULL;

/* various protos and declarations */
void * __inline_va_start__(void) ;
static int our_pid;
static int InDebug = 0;



/* The default is to ignore local masking and execute all global levels. 
   The local masks are set to DBG_NONE. */
/* Default is to set everything to warn until some later date.
** Note that the the number of intialized masks is DBG_MAX_MASKS, (now 32) 
** which is the maximum possible # of masks.
*/

#ifdef MALLOC_D_MASKS
static mmcc_DebugMask * mmccDebugMask = NULL;
static u_char * DebugMasks;
char * my_procName = NULL;
/*proto for mallocing masks*/
static void  dbg_malloc_masks(char *);
#else
static int DebugMasks[DBG_MAX_MASKS] =
{
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL,
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL,
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL,
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL,
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL,
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL,
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL,
    DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL, DBG_WARN_LEVEL
};
#endif


/* DECLARATIONS for this file*/
/*buffers*/

static char Debug_buffer[MSGLEN_MAX]="";
static char prog_tag[PROGTAG_LEN]="";
static char debugHdr[DEBUGHDR_LEN]="";


/*file handles*/
FILE * debug_fd = stderr;



/*SET STATE of one indicated mask
 */

void dbg_setDebugState (int index, int state)
{
#ifdef MALLOC_D_MASKS
  if(mmccDebugMask == NULL)
    dbg_malloc_masks(NULL);
#endif    

    DebugMasks[index] = state;
}

int dbg_getDebugState (int index)
{
#ifdef MALLOC_D_MASKS
  if(mmccDebugMask == NULL)
    dbg_malloc_masks(NULL);
#endif    

    return (DebugMasks[index]); 
}


/*
 * set state of all masks
 *
 */
void debug_initAllMasks(int state)
{
  int i ;
#ifdef MALLOC_D_MASKS
  if(mmccDebugMask == NULL)
    dbg_malloc_masks(NULL);
#endif    

  for (i = 0 ; i < DBG_MAX_MASKS ; i++)
    DebugMasks[i] = state ;
}

Bool glob_getSendLocalMsg(void);
Bool glob_getLocalMsgsOk(void);


/* 
 * set up debug:
 *
 * 1) get ipc set
 * 2) get semas
 * 3) set all masks
 */
int
debug_SetUp (char *progname, int usePIDinProgTag)
{
    int retVal;
    char *destName;
    rmsError rmsErr;
    rmsConnAddr *address_p;
    mmcc_Endpoint *mmcEndpoint_p;

    /* We don't want RMS to try to do debug output until we're done setting up */
    InDebug = 1;
    
    debug_SetProgTag (progname, usePIDinProgTag);
    debug_initAllMasks (DBG_WARN_LEVEL);


    if ( ! glob_getSendLocalMsg() ) {
        address_p = mmcc_getMyAddr ();
        NetworkConn = rms_CreateUDP (address_p);
    
        mmcEndpoint_p = mmcc_getEndpoint( "DEBUG" );
        if ( ! mmcEndpoint_p ) {
            #ifdef ICTVDEBUG
        if ( glob_getLocalMsgsOk() )
        fprintf( stderr, "Unable to get Endpoint for "
             "debug server.\n"  );
            #else
            /* So we don't get a warning about a 'vacuous' if. */
            destName[0] = destName[0];
            #endif
            
            retVal = 1;
        goto exit;
        }
        
        rmsErr = rms_Open( NetworkConn, &(mmcEndpoint_p->addr) );
        if ( rmsErr != rms_OK ) {
            #ifdef ICTVDEBUG
        if ( glob_getLocalMsgsOk() )
        fprintf( stderr, "Unable to open connection to debug "
             "server sending debug messages to stderr.\n" );
            #else
            /* So we don't get a warning about a 'vacuous' if. */
            destName[0] = destName[0];
            #endif
            
            retVal = 1;
        goto exit;
        }
    }
    
    retVal = 0;

 exit:
    /* We should be able to handle output now */
    InDebug = 0;
    
    GLOBAL_MSG(("Sending DBG/LOG messages to %s\n\n", 
        glob_getSendLocalMsg() ? "stderr" : "network" ), DBG_ASSERT);
    return retVal;
}


void
debug_tearDown(void)
{
    if ( ! glob_getSendLocalMsg() && NetworkConn != NULL )
        rms_Shutdown( NetworkConn );
    
    return;
}

#ifdef MALLOC_D_MASKS
static void  dbg_malloc_masks(char * my_procName)
{
/* if we don't know our mailbox name, find it, so that we can get our
 *  debug_masks
 */
  if (my_procName != NULL) /* check for null configp here??*/
    {
      mmccDebugMask=mmcc_getDebugMask(my_procName);/* use name as string*/
      if (mmccDebugMask!=NULL)
          DebugMasks=mmccDebugMask->mask;
    }
  if (DebugMasks==NULL)
    DebugMasks=mem_malloc(DEFAULT_HEAP_ID,DBG_MAX_MASKS *sizeof(u_int) );


}
#endif


char*
debug_GetProgTag()
{
   if (prog_tag[0] == '\0') return 0;

   return prog_tag;
}

void debug_SetProgTag(char *name, char appendPID)
{
    if(name) {
        strncpy(prog_tag, name, PROGTAG_LEN - 1);
        prog_tag[PROGTAG_LEN - 1] = '\0';
    }
    
    if(appendPID) {
        int32 nameLen = strlen(name);
        our_pid = getpid();
    
        if(nameLen > 240) { /* Allow long PID strings... */
            nameLen = 240;
        }
        /* Add the pid to the prog_tag */
        sprintf(&prog_tag[nameLen], "_%d ", our_pid);
    }

    return;
}

# ifndef DBG_LINE_LEN
#   define DBG_LINE_LEN 79
# endif
static int DBGLineLen = DBG_LINE_LEN;

int
dbg_setLineLength (int len)
{
    int i;

    i = DBGLineLen;
    
    /* What is the '8' represent ?  God, I hate magic numbers. */
    if (len > (MSGLEN_MAX - 8))
        len = MSGLEN_MAX - 8;
    DBGLineLen = len;
    return i;
}

/*
 * test to see if we actually need to do the debug msg
 */
int
dbg_maskDebugp (int index, int gmask, int lmask)
{
    if(InDebug) {
      return 0;
    }

    if
    (
         (gmask & DebugMasks[DBG_GLOBAL_MASKINDEX])
         && (lmask & DebugMasks[index])
    )
        return 1;
    return 0;
}

static void
sendLocalMsg( char *header, int hdrLength,
              char *message, int msgLength )
{
    if (hdrLength > 0)
      fprintf(debug_fd,"%s",header);
    
    /* Header and message won't fit on one line,
    put a newline between them.
    */
    if((hdrLength + 2 + msgLength) > DBGLineLen)
    fprintf(debug_fd, "\n");
    
    /* Output the message and flush the buffer. */
    if (msgLength > 0)
      fprintf(debug_fd," %s", message);

    fflush(debug_fd);
    
    return;
}

static void
sendNetworkMsg( char *header, int hdrLength,
                char *message, int msgLength ) 
{
    void *rmsMsgData; 
    int msgSize = hdrLength + msgLength;
    rmsConnMsgHndl rmsMsg;

    rmsMsg =  rms_CreateMessage( NetworkConn, msgSize );
    if ( ! rmsMsg && glob_getLocalMsgsOk() ) {  
    /* Can't get a message so send locally */
        sendLocalMsg( header, hdrLength, message, msgLength );
    return;
    }else {
        rmsMsgData = rms_GetMessageData( rmsMsg );
    memset(rmsMsgData,0,msgSize);

    if (hdrLength > 0)
      strcpy( (char*)rmsMsgData, header );
    strcat( (char*)rmsMsgData, message );
    }

    if ( rms_SendMessage( rmsMsg, msgSize ) != rms_OK ) {
    int errno = rms_GetErrnoValue( NetworkConn );

        if ( glob_getLocalMsgsOk() ) {
        fprintf( stderr, "Error sending Message on NetworkConn:: %d:",
             "\t%s\n", errno, strerror( errno ) );
        sendLocalMsg( header, hdrLength, message, msgLength );
    }else {
        int retries = MAX_NETWORK_RETRIES;

        while ( retries-- > 0 ) {
         if ( rms_SendMessage( rmsMsg, msgSize ) == rms_OK )
             retries = 0;
        }
    }
    rms_FreeMessage( rmsMsg );
    }
    
    return;
}


/* actually processing the debug message*/
void
writeDebugMsg (int lineno, char *filename, int maskindex, char *msg_buffer)
{
    char time_str[25];
    int32 msg_buf_len = strlen (msg_buffer) + 1;
    int32 debugHdrLen = 0;

    TimeStamp_CurrentTimeStr (time_str, 2);
    glob_incrementDebugCnt ();

    if (((DebugMasks[DBG_GLOBAL_MASKINDEX] & DBG_TERSE_BIT) == 0) &&
        ((DebugMasks[maskindex] & DBG_TERSE_BIT) == 0))
    {
        /* Output the debug header. */
        if (prog_tag[0] != '\0')
        {
            sprintf (debugHdr, "%s F:%s:%d T:%s N:%d:: %n",
                 prog_tag, filename, lineno, time_str,
                 glob_debugCount (), &debugHdrLen);
        }
        else
        {
            sprintf (debugHdr, "F:%s:%d T:%s: N:%d:: %n",
                 filename, lineno, time_str, glob_debugCount (), &debugHdrLen);
        }
    }

    if (!NetworkConn)
    {
        sendLocalMsg (debugHdr, debugHdrLen, msg_buffer, msg_buf_len);
    }
    else if (glob_getLocalMsgsOk () && glob_getSendLocalMsg ())
    {
        sendLocalMsg (debugHdr, debugHdrLen, msg_buffer, msg_buf_len);
    }
    else
    {
        sendNetworkMsg (debugHdr, debugHdrLen, msg_buffer, msg_buf_len);
    }
}

/* Only print out two messages for repeated messages. The first one,
   and the second being a total of the times the message was repeated,
   plust the message itself.
*/
#define REPEAT_MSG_MAX    (100)

void
Do_DebugMsg (int lineno, char *filename, int maskindex, char *msg_buffer)
{
  static char PrevMsgBuffer[MSGLEN_MAX] = "";
  static char PrevFileName[DEBUGHDR_LEN] = "";
  static int PrevLineNumber = -1;
  static int PrevMaskindex  = 0;
  static int RepeatMsgCnt = 0;
    char repeatMsg[64];

    if (InDebug)
        return;
    InDebug = 1;
    if
    (
        PrevLineNumber == lineno
        && !strcmp (PrevFileName, filename)
        && !strcmp (PrevMsgBuffer, msg_buffer)
        && RepeatMsgCnt < REPEAT_MSG_MAX
    )
    {
        RepeatMsgCnt++;
        InDebug = 0;
        return;
    }

    if (RepeatMsgCnt > 0)
    {
        /* Print out the repeated message */
        sprintf
        (
            repeatMsg, "The following message was repeated %3d times:",
            RepeatMsgCnt
        );
        writeDebugMsg (__LINE__, __FILE__, maskindex, repeatMsg);
        writeDebugMsg (PrevLineNumber, PrevFileName, PrevMaskindex, PrevMsgBuffer);
    }

    /* Print out the current message */
    writeDebugMsg (lineno, filename, maskindex, msg_buffer);
    PrevLineNumber = lineno;
    strcpy (PrevFileName, filename);
    PrevMaskindex = maskindex;
    strcpy (PrevMsgBuffer, msg_buffer);
    RepeatMsgCnt = 0;
    InDebug = 0;
}




/* this ugly code is nescessary to hijack the printf args to sprintf args*/
/* it works*/
char *
PRINTF(char * format,...) 
{
    int buffLen;
    va_list args;   /* required for var args */

    va_start (args, format);    /* setup */

    vsprintf (Debug_buffer, format, args); /* all of that just for this? */
    /* cut down to 256 or DBGLineLen chars which ever is greater */
    if (strlen (Debug_buffer) > 256)
    {
        if (DBGLineLen > 256)
        {
            if (strlen (Debug_buffer) > DBGLineLen)
                strcpy (&(Debug_buffer[DBGLineLen - 7]), " TRUNC");
        }
        else
            strcpy(&(Debug_buffer[256 - 8]), "  TRUNC");
    }
    va_end (args);

    /* Make sure the message ends with a newline. */
    if ( Debug_buffer[buffLen = strlen(Debug_buffer) - 1] != '\n' ) {
        Debug_buffer[++buffLen] = '\n';
        Debug_buffer[++buffLen] = '\0';
    }else {
        Debug_buffer[++buffLen]= '\0';
    }
    
    return Debug_buffer;    /* return the pointer */
}

#ifndef _OS9000

int debug_Open(int fd)
{
   our_pid= getpid();
   debug_fd= fdopen(fd, "w");

   debug_initAllMasks(DBG_WARN_LEVEL) ;

   return (0);

}


int debug_filed()
{
   return fileno(debug_fd);
}

#endif /* ifdef _OS9000 */

