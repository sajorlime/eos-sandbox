
/*
** Copyright (C) 1994, ICTV Inc. 14600 Winchester Blvd., Los Gatos CA 95030
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television
**
** File: transact.c - transaction id generation and decoding
**     $Name:  $
**     $Id: transact.c,v 1.2 1994/11/28 21:55:33 ljr Exp $
**
** Author:
**      Larry Rosenberg
**
** Purpose:
**     This file implements generating transaction ids. It also provides a
**     function for decoding them strictly for debugging purposes.
**
**
** Functions:
**     TransactionId_Init - store a unique identifier for each process that
**         generates a transaction id
**     TransactionId_Generate - create a unique transaction id
**     TransactionId_Decode - returns the relevant information contained in
**         a previously generated transaction id
**     
** Last Edited: Mon Nov 28 13:53:30 1994
** Modified:
*/

#include <stdio.h>                      /* USING sprintf */
#include <ctype.h>                      /* USING isdigit */
#include <errno.h>                      /* USING errno */
#include <string.h>                     /* USING strlen */
#include "transact.h"
#include "timestamp.h"


#ifdef _OS9000
#   define EINVAL   EOS_ILLARG
#endif

static u_int16 transactionPrefix = 0;
static u_int16 counter = 1;

/****************************************************************************
 * static u_int32 GetHexValue (char **buffer, int length)
 * Purpose:     Internal routine to avoid using scanf in here
 *
 * Parameters:
 *  buffer - string to read (and advance pointer as we do it)
 *  length - maximum number of characters to read 
 *
 * Returns:     value scanned
 ***************************************************************************/
static u_int32 GetHexValue (char **buffer, int length)
{
    u_int32 val = 0;
    while (length--) {
        char ch = *(*buffer)++;
        val *= 16;
        if (isdigit(ch)) val += ch - '0';
        else val += ch - 'A' + 10;
    }
    return val;
} /* GetHexValue() */

/****************************************************************************
 * void TransactionId_Init(u_int16 id)
 * Purpose:     Store the passed in value as the unique prefix for the to be
 *              generated transaction ids
 *
 * Parameters:  id - value to save
 *
 * Returns:     nothing
 ***************************************************************************/
void TransactionId_Init(u_int16 id) 
{
    transactionPrefix = id;
} /* TransactionId_Init() */

/****************************************************************************
 * int TransactionId_Generate(TransactionId_t result)
 * Purpose:     Generate a unique transaction id. Use the prefix, the current
 *              time (down to the thousandths of a second), and a counter
 *
 * Parameters:  result - where to store the generated string
 *
 * Returns:     0 = ok; -1 = invalid arg
 ***************************************************************************/
int TransactionId_Generate(TransactionId_t result)
{
    Timestamp_t ts;
    if (!result) { errno = EINVAL; return -1; }
    TimeStamp_Current(&ts);
    sprintf(result, "%04X%08lX%03lX%03X",
            transactionPrefix, ts.time, ts.frac/1000, counter);
    /* increment and wrap counter */    
    if (++counter >= 1296) counter = 0;
    return 0;
} /* TransactionId_Generate() */

int TransactionId_Decode (TransactionId_t transId, u_int32 *prefix,
                          Timestamp_t *ts, u_int32 *counter) 
{
    if (strlen(transId) != TRANSACTION_ID_STRING_LEN) {
        errno = EINVAL; return -1; }
    *prefix = GetHexValue(&transId, 4);
    ts->time = GetHexValue(&transId, 8);
    ts->frac = GetHexValue(&transId, 3);
    ts->frac *= 1000;
    *counter = GetHexValue(&transId, 3);
    return 0;
} /* TransactionId_Decode() */
