/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, CA 95050-4320
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ICTV Library
**
** File:  atox.c - convert an ASCII string to a hex value
**     $Id: atox.c,v 1.1 1994/09/23 17:14:15 dougf Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/utility/atox.c,v $
**
** Author:  Doug Fuhriman
**
** Purpose:
**    To provide a conversion from ASCII to hexadecimal   
**
** Types/Classes:
**
** Functions:
**    atox
**
** Assumptions:
**
** Limitations:
**
** Notes:
**
** References:
**
*/


/*
 * include files
 */

#include <stdio.h>
#include <stdlib.h>

#include "atox.h"


/*
 * public function definitions
 */

int
atox(char* str)
{
    return ((int) strtol(str, (char **)NULL, 16));
}
