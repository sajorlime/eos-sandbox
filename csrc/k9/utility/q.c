
#include "ictvcommon.h"

#include "semaph.h"
#include "LogPrivate.h"


#ifdef _SUN4
int getpid(void);
#endif
#ifdef _AIX
 pid_t getpid(void);
#endif


static void kill_queue( Q_info * MyQ)
{
  MyQ->head  =0;
  MyQ->tail  =0;
  MyQ->errors++;
}



/* invariants:
 * head -> head of a msg
 * tail -> beginning of next avail message
 * if tail==head && full=0 no messages
 * if tail==head && full!=0 q full
 */

void initQ(Q_info * myQ)
{
  /* myQ*/
  strcpy(myQ->guard,"GuardGuard"); 
  myQ->errors = 0;
  myQ->head=0;
  myQ->tail=0;
  myQ->full=0; /* zilch it out*/
  myQ->max=20;/* XXX hard coded val*/
  myQ->pid=getpid();/* to make signals work*/

}
/* puts 1 msg on the queue. Takes care of locking, and gets rid of 
 * a message if it needs room*/
int En_Q( Q_info * MyQ,LogMsg * Qp,LogMsg * msg,sema_id id)
{
  LogMsg junk;

  sem_wait(id);

  if (MyQ->full) /* full!*/
    /*( ( (MyQ->tail + msg->len) / MyQ->max)>= MyQ->head)*/
    /*XXX use mod &figure it out*/
    /*out of room!*/
    {
      MyQ->errors+=1;/* opps, we've run out of room */
      /*remove 1 msg*/
      DeQ1(MyQ,Qp,&junk);
    }
  if ((MyQ->head > MyQ->max)||(MyQ->tail > MyQ->max))
    {
      kill_queue(MyQ);
      /*problems!!!*/
      /* can we put in a stock msg here?*/
      sem_signal(id);
      return (0);
    }
  if ( (MyQ->tail+ 1) < MyQ->max)/* it fits*/
    {
      memcpy (Qp+MyQ->tail,
	      msg,
	      sizeof (LogMsg));
      MyQ->tail+= 1; /*acutally, +msg->len, but got bit by bug*/
    }
  else /* msg wraps*/
    {/* copy from msg to MyQ, sizeof (LogMsg)*/
      memcpy (Qp , msg , sizeof (LogMsg));
      MyQ->tail=0;
    }
  if (MyQ->tail==MyQ->head)/*opps full now*/
    MyQ->full=1;

  sem_signal(id);
  if ((MyQ->head > MyQ->max)||(MyQ->tail > MyQ->max))
    {
      kill_queue(MyQ);
      /* we need to put in a stock msg here*/
      return (-1);
    }
  return(0);
}

/* DeQ1 needs to have the lock.  Gets 1 msg off the queue*/ 
int DeQ1(Q_info * MyQ,LogMsg* Qp,LogMsg * msg )
{

  if (!(MyQ->full) && (MyQ->head == MyQ->tail))  /* OPPS! nothing here*/
    {
      return  (0);
    }
  if ((MyQ->head > MyQ->max)||(MyQ->tail > MyQ->max))
    {
      /*problems!!!*/
      kill_queue(MyQ);
      /* flush the entire q*/
       sprintf(msg->body,"reset queue on bad values\n");
       return (1);
    }
  /* what about errors?*/
      /*get msg at head*/

      if ( MyQ->head + 1 < MyQ->max) /* no wrap*/
	{
	  memcpy ( msg,
		  Qp+(MyQ->head),
		  sizeof(LogMsg) );
	  

	  if (msg->len > 1)
	    msg->len=1;/* what now*/

	  (Qp+(MyQ->head))->len=0;/* mark msg as read*/
	  MyQ->head ++;
	}
      else
	/* split across end*/
	/* i.e. head + 1= max, so next is at base*/
	{
	  memcpy( msg ,
		 Qp, 
		 sizeof (LogMsg) );

	  if (msg->len > 1)
	    msg->len=1;/*what now?*/

	  MyQ->head = 0 ;
	  (Qp+(MyQ->head))->len=0;/* mark msg as read*/

	}
  MyQ->full=0;
  return (1);/* return # of msgs*/
}

/* removes all messages available from queue
 * Needs to get lock, will sleep on it.
 * 
 * returns up to n messages in my_msg, which is an array
 * of messages
 *
 */
int DeQ(Q_info *MyQ,LogMsg * Qp,LogMsg * my_msg,sema_id id)
{


  int num_msgs;
#ifdef _OS9000
  int value;
#endif

  sem_wait(id);
#if 0
  value=0;
  _os_ev_set(id,&value,0);
#endif
  num_msgs=0;
  if (MyQ->errors)
    {
      sprintf(my_msg->body,"%d messages lost on input\n",MyQ->errors);
      my_msg++;
      num_msgs++;
      /* output errors and log it ??*/
      MyQ->errors=0;
    }
  
  if ((MyQ->head > MyQ->max)||(MyQ->tail > MyQ->max))
    {
      kill_queue(MyQ);
      sprintf(my_msg->body,"killed Queue on output\n");
      my_msg++;
      num_msgs++;
      sem_signal(id);
      return (num_msgs);
    }
  sprintf(my_msg->body,"tail =%d,head =%d, full=%d\n",
	  MyQ->tail,MyQ->head, MyQ->full);
  my_msg++;
  num_msgs++;


  while (DeQ1(MyQ,Qp,my_msg) > 0)
    {
      num_msgs++;
      my_msg ++;
      /* output it, or on a list*/
      
    }
  sem_signal(id);
  return(num_msgs);
}
