/*
** Copyright (C) 1994
** ICTV Inc. 14600 Winchester Blvd., Los Gatos, CA 95030 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: ictv interactive television OS9000 Extensions
** 
** File: MultiProc.h - run several processes from a single image
**     $Id: multiproc.h,v 1.2 1995/04/05 22:23:46 luke Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/utility/multiproc.h,v $
**
** Author(s):
**    Aylwin Stewart
**
** Purpose:
**     Provides a template for users to create a single executable
**     which forks itself several times to run different processes.
**     The primary goal of this is a reduction in code size for a
**     system because libraries will not be included separately in
**     each executable which is built.
** 
** Types/Classes:
**     MainId - associates a process with a string identifier
** 
** Functions:
**     mpFork     - fork a process whose main is determined by its
**                  first arg, which is a MainId.string
**     runMain    - determines which "main" should actually be executed
**     masterMain - a default function which spawns all the processes
**                  in a MainId table
**     freeMemory - clean up memory on exit
** 
** Usage:
** 
** Notes:
**     masterMain spawns processes in the order of their
**     entry into the Procs table.
**
** Assumptions:
** 
** 
** References:
**     See code in mmcinit.c for an example.
** 
*/

# ifndef __MULTIPROC_H__

#include <types.h>

#include "ictvdebug.h"
#include "ictvcommon.h"



/*****                      E N U M S                      *****/


/*****            D A T A   S T R U C T U R E S            *****/

/* 
 * MainId: associates a string with a "foo_main()" function
 * for a process invocation.  The identifying strings would ideally
 * all be unique within the first three characters.   A priority of
 * '0' means run with original process' priority.
 */
typedef struct
{
  char *string;
  int32 (*main_p) (int32 argc, char **argv);
} MainId;


/*****                     M A C R O S                     *****/

/* 
 * MP_MAIN:  The main function to be used for your multiprocess
 * executable.
 */
#define MP_MAIN( MainsTable ) \
int32 \
main( int32 argc, char *argv[] ) \
{ \
    if ( atexit( mp_freeMemory ) ) { \
        GLOBAL_MSG(("%s: atexit() failure exiting\n", argv[0]), \
	           DBG_WARN ); \
        return -1; \
    } \
    mp_runMain( argc, argv, (MainsTable), sizeof((MainsTable))/sizeof(MainId) ); \
}


/*****                    G L O B A L S                    *****/                  

#if 0  /* A template for a MainsTable */
/* 
 * Procs: the MainsTable of processes for this multiprocess to exec.
 */
MainId Procs[] = {
    { NULL, &masterMain },
    { "1Process string", &foo1Main },
    { "2Process string", &foo2Main },
    { "3Process string", &foo3Main },
    { "4Process string", &foo4Main },
    { "5Process string", &foo4Main }
};
#endif 

/*****      F U N C T I O N   D E C L A R A T I O N S      *****/

/*
 * Fork: exec a process with an identifying string.
 *
 * Returns the return value of a k9_fork() call.
 */
int32 mp_fork( char *idString, char **args, process_id *childId,
               int paths, u_int16 priority );

/* 
 * RunMain: determine which process should actually be run and start
 * its main_p function.
 */
int32 mp_runMain( int32 argc, char *argv[], 
                  MainId procTable[], int32 numProcs );


/* 
 * FreeMemory: clean up memory at exit
 */
void mp_freeMemory(void);

# endif

