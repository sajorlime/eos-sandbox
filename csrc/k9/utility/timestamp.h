/*
** Copyright (C) 1994, ICTV Inc. 14600 Winchester Blvd., Los Gatos CA 95030
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television
**
** File: timestamps.h - timestamping prototypes
**     $Name:  $
**     $Id: timestamp.h,v 1.13 1995/11/04 02:04:17 wul Exp $
**
** Author:
**      Larry Rosenberg
**
** Purpose:
**     This file defines the basic timestamp type as well as routines
**     for generating them, displaying them and comparing them.
**
**     Timestamps have two purposes. They are either the current date
**     and time (accurate to some machine dependent value) or they are
**     the difference between two timestamps and represent elapsed time.
**
** Functions:
**      TimeStamp_Add - Adds two times together
**      TimeStamp_Current - generates the current time as an internal timestamp
**      TimeStamp_CurrentStr - generates the current time as a displayable string
**      TimeStamp_CurrentDateStr - generates the current date only 
**      TimeStamp_CurrentTimeStr - generates the current time only 
**      TimeStamp_Divide - divides a timestamp into N equal parts. 
**      TimeStamp_Elapsed - calculates elapsed time from two timestamps
**      TimeStamp_ElapsedStr - creates an elapsed time string from timestamp
**      TimeStamp_FromMicroSecs - sets both the time and fractional portion
**      TimeStamp_FromMilliSecs - sets both the time and fractional portion
**      TimeStamp_FormatStr - generic timestamp format string
**      Timestamp_fromString - converts "mm/dd/yy hh:mm" to Timestamp_t
**      Timestamp_formatSet - converts arbitrary string to Timestamp_t
**      Timestamp_GetMonth - calculate the month from a timestamp
**      Timestamp_GetDayOfWeek - calculate the day of the week from a timestamp
**      TimeStamp_LocalTime - deal with OS differences for starting points
**      TimeStamp_MakeTime - deal with OS differences for starting points
**      TimeStamp_MinuteOfWeek - return a number between 0-10079.
**      TimeStamp_SetDate - updates the date portion in the timestamp
**      TimeStamp_SetMicroSecs - updates the fractional portion of the time
**      TimeStamp_SetMilliSecs - updates the fractional portion of the time
**      TimeStamp_SetSecs - updates the seconds portion
**      TimeStamp_SetTime - updates the time portion in the timestamp
**      TimeStamp_DateStr - creates a displayable date string from timestamp
**      TimeStamp_TimeStr - creates a displayable time string from timestamp
**      TimeStamp_Str - creates a displayable string from timestamp
**      TimeStamp_Test - compares two timestamps
**      TimeStamp_TestCurrent - compares a timestamp against the current time
**      TimeStamp_Xlat - provide host <-> network translation
**
** Last Edited: Thu Sep 21 18:37:40 1995
*/

#ifndef __timestamph
#define __timestamph

#include "ictvcommon.h"

#ifdef _OS9000
#   include <time.h>
#   include <types.h>
#else
#ifdef _AIX
#   include <time.h>
#endif
#   include <sys/time.h>
#endif


/* INTERNAL FORMAT OF A TIMESTAMP - just like time() produces with a
 * fractional portion as well. */

typedef struct Timestamp {
  time_t time;     /* number of seconds since Jan 1, 1970 */
  long frac;       /* fractional portion of second - we store micro-seconds though
                    * the smallest value that can be generated varies by
                    * architecture */
} Timestamp_t;

/* DEFINES FOR DISPLAY STRINGS LENGTH */
#define TIMESTAMP_STRING_LEN 30
#define TIMESTAMP_ELAPSED_LEN 60

typedef char TimestampStr_t[TIMESTAMP_STRING_LEN+1];
typedef char TimestampElapsedStr_t[TIMESTAMP_ELAPSED_LEN+1];

#ifdef __cplusplus
    extern "C" {
#endif

/* Generates the current time and date */
Timestamp_t *TimeStamp_Current(Timestamp_t *result);

/* Set the specified values in the Timestamp to the passed in values */
int TimeStamp_SetDate(Timestamp_t *ts, int month, int day, int year);
int TimeStamp_SetTime(Timestamp_t *ts, int hour, int min, int sec);

void TimeStamp_SetSecs(Timestamp_t *ts, long secs) ;

/* The next two function set only the fractional portion. They will
 * not update the second portions, instead they will return an error */
/* This expects a number less than one million */
int TimeStamp_SetMicroSecs(Timestamp_t *ts, long microSecs);
/* This expects a number less than one thousand */
int TimeStamp_SetMilliSecs(Timestamp_t *ts, long milliSecs);

/* Set both the whole and fractional portion for an elapsed time at once */
int TimeStamp_FromMilliSecs(Timestamp_t *ts, long secs, long milliSecs);
int TimeStamp_FromMicroSecs(Timestamp_t *ts, long secs, long microSecs);

/* Return the number of minutes from the beginning of week (Sunday is 0).
 * Useful for determining preferences that have one minute granualarity */
u_int16 TimeStamp_MinuteOfWeek(Timestamp_t *ts);

/* Deal with operating system having different starting points
 * for time values */
struct tm *TimeStamp_LocalTime(Timestamp_t *ts);
Timestamp_t *TimeStamp_MakeTime(Timestamp_t *ts, struct tm *tm);

/* Get the Month of the year */
int TimeStamp_GetMonth(Timestamp_t *ts);

/* Get the day of the month */
int TimeStamp_GetDayOfMonth(Timestamp_t *ts);

/* Display Function Prototypes */

/* IF fracDigits == 0 ; don't show any fractional part of second */
/* Otherwise, up to 6 digits (microseconds) can be displayed */
char *TimeStamp_CurrentStr(char *buffer, int fracDigits);
char *TimeStamp_Str(Timestamp_t *ts, char *buffer, int fracDigits);

/* Convert strings to Timestamps.  Timestamp_fromString takes a string
 * of the form "mm/dd/yy hh:mm".  Timestamp_formatSet lets you specify
 * the format of your string using the strptime(3) format.  Note: These
 * won't compile on OS9K because 'strptime' or equivalent isn't
 * available.
 */
#ifndef _OS9000

Timestamp_t Timestamp_fromString(char *s);

/* Returns 0 on success, -1 on error (bad format string) */
int Timestamp_formatSet(char        *format,
                        const char  *input,
                        Timestamp_t *pTS);

#endif /* _OS9000 */


/* Show only the time or date for the current timestamp */
char *TimeStamp_CurrentTimeStr(char *buffer, int fracDigits);
char *TimeStamp_CurrentDateStr(char *buffer);

/* Show only the time or date for the timestamp */
char *TimeStamp_TimeStr(Timestamp_t *ts, char *buffer, int fracDigits);
char *TimeStamp_DateStr(Timestamp_t *ts, char *buffer);

/* Allow full strftime control of how timestamp get displayed */
char *TimeStamp_FormatStr (Timestamp_t *ts, char *buffer,
                           char *formatStr, int fracDigits, int len);

/* if (showFrac != 0) will display elapsed number of micro-seconds */
char *TimeStamp_ElapsedStr(Timestamp_t *ts, char *buffer, boolean showFrac);

/* Comparsion Functions */

/* return -1, 0, +1 if less than, equal to, or greater than current time */
int TimeStamp_TestCurrent(Timestamp_t *ts);
int TimeStamp_Test(Timestamp_t *ts1, Timestamp_t *ts2);

/* Math Functions Prototypes */
/* return -1 if invalid pointer are given, return 0 if result has a value */
int TimeStamp_Add(Timestamp_t *result, Timestamp_t *first, Timestamp_t *last) ;
int TimeStamp_Elapsed(Timestamp_t *result, Timestamp_t *first, Timestamp_t *last);
int TimeStamp_Divide(Timestamp_t *result, Timestamp_t *arg, int n) ;

/* Provide host <-> network translation routine for type */
void TimeStamp_Xlat (Timestamp_t *ts, boolean toNet);

#ifdef __cplusplus
    }
#endif

#endif /* __timestamph */

