/*
** Copyright (C) 1995
** ICTV Inc. 14600 Winchester Blvd., Los Gatos, CA 95030 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: ictv interactive television utilities
** 
** File: ictvString.h - desired string functions not supported by K9.
**     $Id: ictvString.h,v 1.2 1995/02/09 23:03:40 emil Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/utility/ictvString.h,v $
**
** Author(s):
**    Aylwin Stewart
**
** Purpose:
**     A single place to add desired generic string functions not
**     supported by K9.
** 
** 
*/

#ifndef __ICTVSTRINGS_H__
#define __ICTVSTRINGS_H__

/*****                     M A C R O S                     *****/

/* 
 * UP_CASE_MASK: an 'and' mask to make all alpha characters upper case.
 */
#define UP_CASE_MASK       (0xDF)



/*****                      E N U M S                      *****/


/*****            D A T A   S T R U C T U R E S            *****/


/*****                    G L O B A L S                    *****/                  


/*****      F U N C T I O N   D E C L A R A T I O N S      *****/

/* 
 * Stricmp: a case insensitive string compare
 */
int stricmp( char *str1, char *str2 );

/*
** safeStrCmp -- compares s1 to s2.  If safly handles the
** case where either s1 or s2 is a null pointer.  It treats
** a null pointer as less than all string except the null pointer.
** I.e. safeStrCmp (0, 0) will return zero.
*/
int safeStrCmp (char * s1p, char * s2p);

/* Like safeStrCmp, except that the comparison is case insensitive */
int safeStrICmp (char * s1p, char * s2p);

/*****       F U N C T I O N   D E F I N I T I O N S       *****/


#endif    /* __ICTVSTRINGS_H__ */
