
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, CA 95050 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: General K9 pblock facility
** 
** File: PBlock.h
**     $Id: PBlock.h,v 1.7 1994/10/21 22:36:07 mikes Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/PBlock.h,v $
**
** Author: Bruce D. Nilo
**
** Purpose: To provide an abstraction for a resource pblock.
**     
** 
** Types/Classes:
**  PBlock -- this is our abstraction and use of K9 events.
**     
** Notes:
**  PBlock is intended to be a base class for other classes in our
**  system that allow us to block on resources. It provides a
**  set of functions (or macros) to suspend and wake up processes.
**
** Assumptions:
**  Our usage of K9 events assumes as set of cooperative processes
**  that have a strict priority relationship.  Therefore when we wake
**  tasks we wake all task waiting for an event value.  The assumption
**  here is that the highest priority task will get the resource and
**  then each remaining task will get the resource in turn.  We believe
**  this to be a better scheme than each task being woken one at a time.
**  Note that this discussion looks more at the intended use of PBlock
**  that its implementation, but the mechanisms chosen here work best
**  with these assumptions.
**
** History:
** 8/6/94: 
** Modified pblock_sleep to loop around _os_ev_wait waiting for event
** to equal the actualEvent. Without this logic a stray signal would
** cause pblock_sleep to exit. This assumes that the event passed into
** pblock_sleep is NOT zero. Current usage follows this contraint. BDN
** 
** 
*/


# ifndef __PBLOCK__H__
# define __PBLOCK__H__

# include "ictvcommon.h"
# include <events.h>
# include <errno.h>
# include <signal.h>
# include <memory.h>

# define PBLOCK_EVENT_NAME "_MMC_PBlock"

typedef struct _PBlock
{
  event_id ev;
} PBlock;

# define pblock_native(pblockp) ((pblockp)->ev)

# ifndef NO_INLINE
#   ifndef NO_PBLOCK_INLINE
#       define pblock_wake(plp, event) (_os_ev_pulse ((plp)->ev, &(event), 1))
#   endif
# endif
/* pblock_init -- intializes a PBlock.  It require a pointer to 
** a PBlock and no other paramters.
** It uses the underlying K9 event mechansim with the name
** defined as PBLOCK_EVENT_NAME above.
** returns:
**  the pointer passed if event was found or created, otherwise zero.
*/ 
PBlock * pblock_init (PBlock * ptr);

/* pblock_init -- intializes a PBlock.  It require a pointer to 
** a PBlock and no other paramters.
** It uses the underlying K9 event mechansim with the name
** specified by "name".
** returns:
**  the pointer passed if event was found or created, otherwise zero.
*/ 
PBlock * pblock_initByName (PBlock * ptr, char * name);

void pblock_delete (PBlock * ptr);

/* pblock_sleep -- causes the process to go to sleep on the 
** process pblock with the event value "event."  This function
** is meant to be paired with pblock_wake.  All process that sleep
** on this process pblock with a particular event value will be woken
** when a call to pblock_wake with the same event value is called by
** some other process.  Another possible use of pblock_sleep would
** be to monitor pblock_releases.
** Parameters:
**  pbp -- a pointer to a PBlock.
**  event -- this should be a unique 32 bit value agreed upon by mulitple
**  tasks. Generally the best choice of this value is the address of a
**  shared resource.
** Return:
**  pblock_sleep return the event value when it was woken.
**  Note that if the task recieved a signal this value will 
**  NOT be the expected event value, but otherwise it will be.
**  and otherwise it reutrns the 
*/

int32 pblock_sleep (PBlock * lp, int32 event);



/* pblock_wake -- cause all process bpblock wait for the event value "event" to
** be woken.  pblock_wake is meant to be paired will calls to pblock_sleep.
*/
# ifndef pblock_wake
    void pblock_wake (PBlock * lp, int32 event);
# endif


# endif
