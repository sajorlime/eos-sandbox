
/*
** COPYRIGHT (C) 1994, ICTV Inc. 280 Martin Ave. Santa Clara, Calif 95050. 280
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television
**
** File: rpcServer.c
**
** Purpose:
**  Provides a method for a server task to register a funtion to
**  handle any subclass of IPCUser type messages.
**
** Types/Clases:
**  Has intimate knowledge of IPCUser enums.
**
** Data:
**  RPCUserHandlers[][] -- a table of funtion pointers that are installed by
**  the user to service RPC messages.
**
** Functions:
**  rpc_noHandler --
**  rpc_init --
**  rpc_UserRegister --
**  rpc_Server --
**
** Usage:
** Note:
**
** $Id: rpcServer.c,v 1.22 1995/06/25 22:28:31 wul Exp $
**
*/

# define __RPCSERVER_C__

# include "rpcServer.h"
# include "k9misc.h"


/* SyncRoot, GetThis, and SerErrno are private to IPC */
IpcB * ipc_SyncRoot (IpcB * ipcp, MailHandle , int type);
MailBox * ipc_GetThis (void);
int ipc_SetErrno (int err);

void
rpc_userFunction (void)
{
}

RPCUserFunction RPCUFp = rpc_userFunction;

void
rpc_setUserFunction (RPCUserFunction userFp)
{
	RPCUFp = userFp;
}

RPCDisposition
rpc_noHandler (void * datap, int len)
{
	/* The default behavoir of the server is to return
	** any IPC buffer that has not had a handler registered
	** for it. */
	return RPC_Exit;
}


# define MAXHANDLERS IPC_SUBTYPE_MAX

RPCUserHandler RPCUserHandlers[LastIPC_CLASS][MAXHANDLERS];

void
rpc_init (RPCUserHandler defaultHandler)
{
    int i;
    int j;

    if (!defaultHandler)
        defaultHandler = rpc_noHandler;
    for (i = 0; i < LastIPC_CLASS; i++)
        for (j = 0; j < MAXHANDLERS; j++)
            RPCUserHandlers[i][j] = defaultHandler;
    if ((IPC_SYS_END - IPC_SYS_BASE) > MAXHANDLERS)
        K9_WARN (("To manny SYS User subtypes\n"));
    if ((IPC_CMX_END - IPC_CMX_BASE) > MAXHANDLERS)
        K9_WARN (("To manny CMX User subtypes\n"));
    if ((IPC_AVGSM_END - IPC_AVGSM_BASE) > MAXHANDLERS)
        K9_WARN (("To manny AVGSM User subtypes\n"));
    if ((IPC_VDCP_END - IPC_VDCP_BASE) > MAXHANDLERS)
        K9_WARN (("To manny VDCP User subtypes\n"));
    if ((IPC_ADCP_END - IPC_ADCP_BASE) > MAXHANDLERS)
        K9_WARN (("To manny ADCP User subtypes\n"));
    if ((IPC_SET_END - IPC_SET_BASE) > MAXHANDLERS)
        K9_WARN (("To manny SET User subtypes\n"));
    if ((IPC_CDSL_END - IPC_CDSL_BASE) > MAXHANDLERS)
        K9_WARN (("To manny CDSL User subtypes\n"));
    if ((IPC_RTDB_END - IPC_RTDB_BASE) > MAXHANDLERS)
        K9_WARN (("To manny RTDB User subtypes\n"));
    if ((IPC_AVSTR_END - IPC_AVSTR_BASE) > MAXHANDLERS)
        K9_WARN (("To manny AVSTR User subtypes\n"));
    if ((IPC_GFT_END - IPC_GFT_BASE) > MAXHANDLERS)
        K9_WARN (("To manny GFT User subtypes\n"));
    if ((IPC_MAL_END - IPC_MAL_BASE) > MAXHANDLERS)
        K9_WARN (("To manny MAL User subtypes\n"));
    if ((IPC_APP_END - IPC_APP_BASE) > MAXHANDLERS)
        K9_WARN (("To manny APP User subtypes\n"));
    if ((IPC_ISM_END - IPC_ISM_BASE) > MAXHANDLERS)
        K9_WARN (("To manny IPC ISM User subtypes\n"));
}




RPCUserHandler
rpc_UserRegister (IPCUser uType, RPCUserHandler rpcfp)
{
    u_int utype = (u_int) uType;
    u_int uclass;
	RPCUserHandler oldRPCFp;

    uclass = uType >> IPC_RANGE_SHIFT;
    utype &= IPC_UTYPE_MASK;
    if ((uclass >= LastIPC_CLASS) || (utype >= MAXHANDLERS))
    {
        GLOBAL_MSG
        (
            (
                "rpc_UserRegister: unexpected message class:%x or type:%x\n",
                uclass, utype
            ),
            DBG_WARN
        );
        return (RPCUserHandler) 0;
    }
    if (!rpcfp)
        rpcfp = rpc_noHandler;
    oldRPCFp = RPCUserHandlers[uclass][utype];
    RPCUserHandlers[uclass][utype] = rpcfp;
    return oldRPCFp;
}


# define xIPC_CHECK_COUNT
# ifdef IPC_CHECK_COUNT
void ipc_checkIpcCount (void);
# endif

IpcB *
rpc_Server (void)
{
	IpcB *ipcp;
	u_int uclass;
	u_int utype;
	RPCDisposition disposition;
	u_int flags;


	while (1)
	{
# ifdef IPC_CHECK_COUNT
	ipc_checkIpcCount ();
# endif
		/* turn the interrupts off for the user function */
		flags = irq_change (0);
		RPCUFp ();
		ipcp = ipc_Rcv ();
		irq_change (flags); /* we always turn the interrupts back on here! */
		if (ipcp)
		{
			utype = ipc_GetUserType (ipcp);
			uclass = utype >> IPC_RANGE_SHIFT;
			if (uclass < LastIPC_CLASS)
			{
				utype &= IPC_UTYPE_MASK;
				if (utype < MAXHANDLERS)
				{
					disposition = RPCUserHandlers[uclass][utype]
					(
						 ipc_GetDataP (ipcp), ipc_GetMsgLen (ipcp)
					);
				}
				else
				{
					GLOBAL_MSG
					(
						 (
							 "rpc_Server: unexpected message utype:%x or type:%x\n",
							 uclass, utype
						 ),
						 DBG_WARN
					);
					disposition = RPC_Continue;
				}
			}
			else
			{
				GLOBAL_MSG
				(
					(
						"rpc_Server: unexpected message class:%x or type:%x\n",
						uclass, utype
					),
					DBG_WARN
				);
				disposition = RPC_Continue;
			}
		}
		else
		{
			disposition = RPCUserHandlers[0][0] (0, 0);
		}
# ifdef IPC_CHECK_COUNT
	ipc_checkIpcCount ();
# endif
		switch (disposition)
		{
		case RPC_FreeAndExit:
			ipc_FreeBuffer (ipcp);
			return (IpcB *) 0;
		case RPC_Exit:
			return ipcp;
		case RPC_Continue:
			break;
		case RPC_FreeAndContinue:
		default:
			ipc_FreeBuffer (ipcp);
			break;
		}
	}
}

/* the current implemetations of rpc_send[Sync] do not allow the 
** delivery of arbitray sized messages.  It would be easy to extend
** it do this via the use of a memory allocated buffer.  This data
** could come from a small heap maintained by the rpc module.
*/

int
rpc_send (int utype, void * datap, int len, RPCServer server)
{
    IpcB * ipcp;
    u_int * uip;
    u_int * dp = (u_int *) datap;

    ipcp = ipc_NewBuffer ();
    uip = ipc_GetDataP (ipcp);
    ipc_SetUserType (ipcp, utype);
    if (len > ipc_GetMaxSize (ipcp))
    {
        len = ipc_GetMaxSize (ipcp);
        K9_WARN (("rpc_send: Message length truncated to %d\n", len));
    }
    ipc_SetMsgLen (ipcp, len);
    /* Note: in general the copy below requires that the processor support
    ** a word copy from any address.  Note that len may be less
    ** than sizeof (u_int), in which case we copy some extra bytes
    ** but that does not hurt any thing. */
	if (!dp)
		len = 0;
	while (len > 0)
    {
        *uip++ = *dp++;
        len -= sizeof (u_int);
    }
    ipcp->type = RPC_TYPE_REQUEST;
    ipcp->sender = ipc_GetThis ();
    return ipc_Forward (ipcp, server);
}


int
rpc_sendSync (int utype, void * datap, int len, RPCServer server)
{
    IpcB * ipcp;
    u_int * uip;
    u_int * dp = (u_int *) datap;

    ipcp = ipc_NewBuffer ();
    uip = ipc_GetDataP (ipcp);
    ipc_SetUserType (ipcp, utype);
    if (len > ipc_GetMaxSize (ipcp))
    {
        len = ipc_GetMaxSize (ipcp);
        K9_WARN (("rpc_send: Message length truncated to %d\n", len));
    }
    ipc_SetMsgLen (ipcp, len);
    /* Note: in general the copy below requires that the processor support
    ** a word copy from any address */
    while (len > 0)
    {
        *uip++ = *dp++;
        len -= sizeof (u_int);
    }
    ipcp = ipc_SyncRoot (ipcp, server, RPC_TYPE_SYNC_REQUEST);
    if (ipcp)
    {
        ipc_FreeBuffer (ipcp);
        return 0;
    }
    return 1;
}

/* Just like rpc_sendSync except it receives the ipcB and puts it's
** data in the datap, and the size of the data in len.
** If outlen is null, don't put in a size.
*/

int
rpc_readSync
(
	int utype, void * inp, int inlen,
	void * outp, int *outlen, RPCServer server
)
{
    IpcB * ipcp;
    u_int * uip;
    u_int * dp = (u_int *) inp;
    u_int len;

    ipcp = ipc_NewBuffer ();
    uip = ipc_GetDataP (ipcp);
    ipc_SetUserType (ipcp, utype);
    if (inlen > ipc_GetMaxSize (ipcp))
    {
        inlen = ipc_GetMaxSize (ipcp);
        K9_WARN (("rpc_send: Message length truncated to %d\n", inlen));
    }
    ipc_SetMsgLen (ipcp, inlen);
    /* Note: in general the copy below requires that the processor support
    ** a word copy from any address */
    while (inlen > 0)
    {
        *uip++ = *dp++;
        inlen -= sizeof (u_int);
    }
    ipcp = ipc_SyncRoot (ipcp, server, RPC_TYPE_SYNC_REQUEST);
    if (ipcp)
    {
        if (outlen)
          *outlen = ipc_GetMsgLen(ipcp);

        dp = (u_int *)ipc_GetDataP(ipcp);
        uip = (u_int *)outp;
        len = ipc_GetMsgLen(ipcp);

        while(len > 0)
        {
          *uip++ = *dp++;
          len -= sizeof(u_int);
        } 
        ipc_FreeBuffer (ipcp);
        return 0;
    }
    K9_ASSERT (("sync read failed for %s\n", ipc_UserTypeString (utype)));
    return 1;
}


int
rpc_reply (u_int utype, void * datap, int len)
{
    IpcB * ipcp;

    if (!datap)
    {
        K9_ASSERT (("rpc_reply: Not an IPC Buffer %x\n", datap));
        return IPCERR_ILLEGAL_BUFFER;
    }
    ipcp = RPC_TO_IPC (datap);
    if (ipcp != ipcp->self)
    {
        K9_ASSERT
        ((
				"rpc_reply: Not an IPC Buffer %x, self:%x  wrong\n ",
				ipcp, ipcp->self
		));
        return IPCERR_ILLEGAL_BUFFER;
    }
    ipc_SetUserType (ipcp, utype);
    ipc_SetMsgLen (ipcp, len);
    return ipc_Reply (ipcp);
}

int
rpc_forward (u_int utype, void * datap, int len, MailHandle mh)
{
    IpcB * ipcp;

    if (!datap)
    {
        K9_ASSERT (("rpc_forward: Not an IPC Buffer %x\n", datap));
        return IPCERR_ILLEGAL_BUFFER;
    }
    ipcp = RPC_TO_IPC (datap);
    if (ipcp != ipcp->self)
    {
        K9_ASSERT
        ((
			"rpc_forward: Not an IPC Buffer %x, self:%x wrong\n",
			ipcp, ipcp->self
		));
        return IPCERR_ILLEGAL_BUFFER;
    }
    ipc_SetMsgLen (ipcp, len);
    ipc_SetUserType (ipcp, utype);
    return ipc_Forward (ipcp, mh);
}

int
rpc_redirect (void * datap, RPCServer server)
{
    IpcB * ipcp;

    if (!datap)
	{
        K9_ASSERT (("rpc_redirect: Not an IPC Buffer %x\n", datap));
        return IPCERR_ILLEGAL_BUFFER;
    }
    ipcp = RPC_TO_IPC (datap);
	return ipc_Send (ipcp, server);
}

