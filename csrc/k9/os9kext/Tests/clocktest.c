#include <types.h>
#include <stdio.h>
#include <stdlib.h>
#include "ictvcommon.h"
#include "ictvutil.h"
#include "k9misc.h"

static void printUsage(void)
{
  printf("ct: probe the clock\n") ;
}

main (int argc, char **argv)
{
  int micros, nanos, ticks ;

  if(argc > 1) 
    printUsage() ;

  ticks = getClockTicks() ;
  nanos = getDeltaNanoSecs() ;
  micros = getDeltaMicroSecs() ;

  printf("Clock: %d (%d nanos) (%d micros)\n",ticks,nanos,micros) ;
  exit(0) ;
}    
