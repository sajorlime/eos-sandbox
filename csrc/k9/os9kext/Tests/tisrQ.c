/*
**
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** Project: 
**
** File: .../libsrc/utility/Tests/tisrQ.c
**
** Author: emil
**
** Purpose:
**  This file is part of the utility library
**
** Class/Typedefs:
**
** Functions:
**
** Notes:
**
** $Id: tisrQ.c,v 1.3 1994/08/17 01:00:05 emil Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/Tests/tisrQ.c,v $
**
*/

# include <stdio.h>
# include <string.h>
# include <signal.h>

# include "isrQueue.h"

# include "globMMC.h"

# define __TISRQ_C__

ISRQ * sigQp = 0;

void
sigUsr1Handler (void)
{
	QElem qe;

	if (!sigQp)
	{
		printf ("Somethings is very wrong\n");
		exit (4);
	}
	qe = isrq_Get (sigQp);
	printf ("SIGUSR1 Handler qe:{%x, %x}\n", qe.tag, qe.val);
}

int
fillQue (ISRQ * qp, int start)
{
	int i;
	int j;
	int k;
	QElem qe;

	i = isrq_MaxElements (qp);
	for (k = 1, j = start; i > 0; i--, j++, k++)
	{
		qe.tag = k;
		qe.val = j;
		isrq_Put (qp, qe);
	}
	return isrq_MaxElements (qp) - isrq_Count (qp);
}

int
verifyQue (ISRQ * qp, int start)
{
	int i;
	int j;
	QElem qe;

	for (i = 1, j = start; isrq_Count (qp) > 0; i++, j++)
	{
		qe = isrq_Get (qp);
		if ((j != qe.val) || (i != qe.tag))
		{
			printf ("Retrieval Error: i(%d) != qe.tag(%d)\n", i, qe.tag);
			printf ("               : j(%d) != qe.val(%d)\n", j, qe.val);
			exit (2);
		}
	}
	return isrq_Count (qp);
}


void
t0Que (ISRQ * qp, int start, char * name)
{
	int i;

	if (!qp)
	{
		printf ("%s failed\n", name);
		exit (1);
	}
	printf ("%s max elements: %d\n", name, isrq_MaxElements (qp));
	printf ("%s count: %d\n", name, isrq_Count (qp));
	if ((i = fillQue (qp, start)) != 0)
	{
		printf ("fill Que ret 0 != %d \n", i);
		exit (2);
	}
	if ((i = verifyQue (qp, start)) != 0)
	{
		printf ("verify Que ret 0 != %d \n", i);
		exit (2);
	}
}

int
main (int argc, char ** argv)
{
	ISRQ * q1p;
	ISRQ * q2p;
	ISRQ * q3p;
	ISRQ * q4p;
	QElem qe;

	while (glob_link () < 0)
	{
		printf ("Sleeping while waiting for MMCInit to run (dummy)\n");
		sleep (1);
	}
	q1p = isrq_CreateDefault ();
	t0Que (q1p, 1, "Q1 ");

	q2p = isrq_Create (0, 0);
	t0Que (q2p, 1000, "Q2 ");
	q3p = isrq_Create (0, 111);
	t0Que (q3p, 0x80000000, "Q3 ");
	q4p = isrq_Create (q3p, 23);
	t0Que (q3p, 23, "Q33 ");

	signal (SIGUSR1, sigUsr1Handler);
	fillQue (q3p, 100);
	sigQp = q3p;
	qe.tag = 0x377;
	qe.val = 0x773;
	isrq_Put (q3p, qe);
	verifyQue (q3p, 100);

    return 0;
}


