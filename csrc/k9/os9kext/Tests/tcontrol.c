
/*
**
** Copyright (C) 1994,  ICTV Inc.
**                      280 Martin Ave.,
**                      Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** File: tcontrol.c - stand alone to test the ipc code.
**
** Author: emil
** $Author: emil $
**
** Purpose:
**  This preiodicly controls a message to some other task.
**
** Types/Clases:
**
** Program:
**  void main (int argc, char ** argv) -- 
**
** Notes:
**
** $Id: tcontrol.c,v 1.4 1994/08/17 01:00:01 emil Exp $
**
*/

# include <assert.h>
# include <signal.h>
# include <stdio.h>
# include <stdlib.h>
# include <modes.h>
# include <string.h>
# include <ctype.h>
# include <time.h>
# include <process.h>

# include "sighandle.h"
# include "siguser.h"
# include "ictvutil.h"
# define NO_IPC_INLINE
# include "ipc.h"
# include "ipcscreen.h"
# include "ipcuser.h"

# include "globMMC.h"

# define __TCONTROL_C__

# define JULIAN_TK  3           /* function code for _sysdate() */

static int sigrcvd = 0;
# define ALARM_INTERVAL (1 * 100)

void
controlSigHandler (int sig)
{
    if (sigrcvd == SIGQUIT)
        exit (SIGQUIT);
    sigrcvd = sig;
}

void
doReply (IpcB * ipcp, char * append, int type)
{
    strcat (ipc_GetDataP (ipcp), " S-> ");
    strcat (ipc_GetDataP (ipcp), append);
    ipc_SetUserType (ipcp, type);
    ipc_Reply (ipcp);
}



u_int32
getTicks (void)
{
    int date;
    int time;
    short day;
    int32 ticks = -1;

    _sysdate(JULIAN_TK, &time, &date, &day, &ticks);
    return ticks & 0xffff;
}


process_id procID;
u_int16 pri, age, group, user;
int32 sched;
int
cTestPriority (void)
{
    int r;

    r = _os_id (&procID, &pri, &age, &sched, &group, &user);
    if ((u_int32) pri > (u_int32) PRIORITY (Pri_Highest))
    {
        fprintf (stderr, "Priority Error: %x/%x\n", pri, PRIORITY(Pri_Highest));
        exit (1);
    }
    return 0;
}

char commandline[128];



/*
** control -- waits in an infinite loop for messages, it waits
** for ipc abort, flush messages, and for data  messages.
** It breaks the rules and uses some ipc internals for the purpose
** of testing and debugging.
** Parmeters:
**  sServers -- the ASCIIZ name of
**  msDelay -- the milliseconds to delay after receiving a data message
**      before sending.
**  priority -- if non-zero used priorty signals and set the task priority
**      to it.
*/
void
control (char * sMe, char * sFor, char * sTar, int msDelay, Priority priority)
{
    int i, err;
    IpcB * ipcp;
    MailHandle me;
    MailHandle forward;
    MailHandle target;
    char * cp;
    int sigNormal =  0;
    int sigPriority =  0;
    signal_code sig;
    int msgs;
    int mnum = 0;
    time_t time;
    u_int32 ticks;

    sigHandlerSet (controlSigHandler);
    me = ipc_MailBoxCreate (sMe, priority, sigNormal, sigPriority);
    ipc_SetRcvTimeout (10000);
    pscr_init (commandline);
    if (!me)
    {
        fprintf (stderr, "Failed to Create me:%s", sMe);
        exit (2);
    }
    if ((target = ipc_GetMailHandle (sTar)) == 0)
    {
        fprintf (stderr, "Failed to File %s", sFor);
        exit (3);
    }
    if ((forward = ipc_GetMailHandle (sFor)) == 0)
    {
        fprintf (stderr, "Failed to task %s", sFor);
        exit (3);
    }
    pscr_show ("target:%s mh:%x", sTar, target);
    if (!me)
    {
        fprintf (stderr, "Failed to task %s", sTar);
        exit (4);
    }
    ipcp = ipc_NewBuffer ();
    cp = ipc_GetDataP (ipcp);
    sprintf (cp, "To:%s:%d", sTar, msDelay / 4);
    ipc_SetUserType (ipcp, IPC_SYS_DATA);
    ipc_Send (ipcp, forward);

    for (msgs = 0;;)
    {
        cTestPriority ();
        ipcp = ipc_RcvMSDelay (-1);
        if (ipcp)
        {
            switch (ipc_GetUserType (ipcp))
            {
            case IPC_SYS_ABORT:
                pscr_show
                (
                    "%s rcvd ABORT from %s",
                    sMe, ipc_GetSender (ipcp)->name
                );
                ipc_Flush (0);
                doReply (ipcp, sMe, IPC_SYS_ABORTED); 
                break;
            case IPC_SYS_FLUSH:
                pscr_show
                (
                    "%s rcvd FLUSH from %s",
                    sMe, ipc_GetSender (ipcp)->name
                );
                ipc_FreeBuffer (ipcp);
                ipc_Flush (ipc_GetSender (ipcp));
                doReply (ipcp, sMe, IPC_SYS_FLUSHED); 
                break;
            case IPC_SYS_DATA:
                cp = ipc_GetDataP (ipcp);
                if (isdigit (*cp))
                    msDelay = atoi (cp);
                _os_getime (&time, &ticks);
                pscr_show ("t:%d", time & 0xff);
                sprintf (cp, "To:%s:%d", sTar, msDelay / 4);
                ipc_SetUserType (ipcp, IPC_SYS_DATA);
                ipc_Send (ipcp, forward);
                break;
            case IPC_SYS_ACK:
                _os_getime (&time, &ticks);
                cp = ipc_GetDataP (ipcp);
                pscr_show ("t:%d-- ACK:%s:%s", time & 0xfff, ipcp->sender->name, cp);
                ticks = msDelay / 5;
                if (ticks > 10)
                    ticks = 10;
                _os_sleep (&ticks, &sig);
                sprintf (cp, "To:%s:%d:#%d", sTar, msDelay / 4, msgs++);
                ipc_SetUserType (ipcp, IPC_SYS_DATA);
                ipc_Send (ipcp, forward);
                break;
            default:
                pscr_show
                (
                    "%s unexpected from %s",
                    sMe, ipc_GetSender (ipcp)->name
                );
                ipc_FreeBuffer (ipcp);
                break;
            }
        }
        else
        {
            pscr_show ("%s woke w/o msgs:%d", sMe, msgs);
            printf ("%s woke w/o msgs:%d\n\007", sMe, msgs);
            exit (5);
        }
        if (sigrcvd)
        {
            switch (sigrcvd)
            {
            case SIGQUIT:
                pscr_show ("%s rcvd SIGQUIT", sMe);
                if (ipcp)
                    ipc_FreeBuffer (ipcp);
                pscr_show ("exiting");
                exit (SIGQUIT);
            case SIG_ICTV_ALARM:
                ipcp = ipc_NewBuffer ();
                ipc_SetUserType (ipcp, IPC_SYS_DATA);
                ipc_Send (ipcp, me);
                break;
            }
            sigrcvd = 0;
        }
    }
}



/* parameters the ipcinit is looking for 
*/
char sSleepTime[16] = "-t";
char sPriority[16] = "-p";

char * sUsages[] =
{
    "cname fname tname [-t msdelay] [-p priotity]",
    "Test program to test sending and forwarding messages to other tasks",
    "cname -- is the name of this tasks (the control tasks) mail box",
    "fname -- is the name of the intermediate forwarding tasks mail box",
    "rname -- is the name of the target tasks mail box",
    "-t -- ms delay Time passed forward to sleep at the forward",
    "      and target tasks",
    "-p -- priority of this task",
    "   t and p are decimal values",
    0
};

int
main (int argc, char ** argv)
{
    int clientFlag = 1;
    int i = 0;
    int msTime = 500;
    Priority priority = Pri_AVGSM;
    char * sMep;
    char * sForp;
    char * sTarp;


    sleep (3);
    usage (argc, argv, sUsages, 1);
	while (glob_link () < 0)
	{
		printf ("Sleeping while waiting for MMCInit to run (dummy)\n");
		sleep (1);
	}
    if (argc <= 1)
    {
        usePrint (argv[0], sUsages);
        return 1;
    }
    arg_Copy (argc, argv, commandline, sizeof (commandline));
    if (arg_OptGet (argc, argv, sSleepTime, sizeof (sSleepTime)))
        msTime = atoi (sSleepTime);
    if (arg_OptGet (argc, argv, sPriority, sizeof (sPriority)))
        priority = (Priority) atoi (sPriority);
    if (priority > Pri_Highest)
        priority = Pri_Highest;
    sMep = arg_Next (argc, argv);
    sForp = arg_Next (argc, argv);
    sTarp = arg_Next (argc, argv);
    if (!sMep || !sForp || !sTarp)
    {
        usePrint (argv[0], sUsages);
        return 1;
    }

    /*alm_cycle (SIG_ICTV_ALARM, ALARM_INTERVAL); */
    control (sMep, sForp, sTarp, msTime, priority);
    return 0;
}


