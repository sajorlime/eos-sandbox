#include <types.h>
#include <stdio.h>
#include <stdlib.h>
#include "ictvcommon.h"
#include "ictvutil.h"
#include "k9misc.h"

main (int argc, char **argv)
{
  int major, minor;
  u_int32 version;

  version = getRomVersion();
  major = version >> 16;
  minor = version & 0xffff;
  printf("Rom version %d.%d\n", major, minor) ;
  exit(0) ;
}    
