
/*
**
** Copyright (C) 1994,  ICTV Inc.
**                      280 Martin Ave.,
**                      Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** Author: emil
** $Author: emil $
**
** File: tforward.c - stand alone to test the ipc code.
**
** Purpose:
**  This preiodicly forwards a message to some other task.
**
** Types/Clases:
**
** Program:
**  void main (int argc, char ** argv) -- 
**
** Notes:
**
** $Id: tforward.c,v 1.4 1994/08/17 01:00:04 emil Exp $
**
*/

# include <assert.h>
# include <modes.h>
# include <string.h>

# include <signal.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <time.h>
# include <process.h>

# include "sighandle.h"
# include "ictvutil.h"
# include "ipc.h"
# include "ipcscreen.h"
# include "ipcuser.h"

# include "globMMC.h"

# define __TFORWARD_C__

# define JULIAN_TK  3           /* function code for _sysdate() */

static int sigrcvd = 0;
static int iforward = 0;

void
forwardSigHandler (int sig)
{
    if (sigrcvd == SIGQUIT)
        exit (2);
    sigrcvd = sig;
}

void
doReply (IpcB * ipcp, char * append, int type)
{
    strcat (ipc_GetDataP (ipcp), " S-> ");
    strcat (ipc_GetDataP (ipcp), append);
    ipc_SetUserType (ipcp, type);
    ipc_Reply (ipcp);
}


static int
realwork (void)
{
    if (sigrcvd)
        return iforward;
    else
        return -iforward;
}

static int
fwork (int work)
{
    int i;
    int s;

    for (i = work; i > 0; i--)
        s += realwork ();
    return s;
}



void

doForward (IpcB * ipcp, char * append)
{
    MailHandle mh;
    int i;
    u_int32 ticks;
    time_t time;
    signal_code sig;
    char * cp;
    char * top;
    char * ticksp;
    char To[16];
    static int ltime = 0;

    cp = ipc_GetDataP (ipcp);
    i = strncmp (cp, "To:", 3);
    if (!i)
    {
        top = strchr (cp, ':');
        *top++ = 0;
        ticksp = strchr (top, ':');
        *ticksp++ = 0;
        strcpy (To, top);
        ticks = atoi (ticksp);
        /* Do some real work and ... */
        fwork (ticks);
        /* sleep a bit also. */
        _os_sleep (&ticks, &sig);
        _os_getime (&time, &ticks);
        sprintf (ipc_GetDataP (ipcp), "%s:dt:%d", append, time - ltime);
        ltime = time;
        mh = ipc_GetMailHandle (To);
        if (!mh)
        {
            pscr_show ("FWD: No <%s>", To);
            ipc_FreeBuffer (ipcp);
        }
        else
        {
            ipc_Forward (ipcp, mh);
        }
    }
    else
    {
        strcat (ipc_GetDataP (ipcp), "<no To>");
        ipc_SetUserType (ipcp, IPC_SYS_ACK);
        ipc_Reply (ipcp);
    }
}



process_id procID;
u_int16 pri, age, group, user;
int32 sched;
int
sTestPriority (void)
{
    int r;

    r = _os_id (&procID, &pri, &age, &sched, &group, &user);
    if ((u_int32) pri > (u_int32) PRIORITY (Pri_Highest))
    {
        fprintf (stderr, "Priority Error: %x/%x\n", pri, PRIORITY(Pri_Highest));
        exit (1);
    }
    return 0;
}

char commandline[128];


char sF[] = "<F>"; /* klduged in */

/*
** forward -- waits in an infinite loop for messages, it waits
** for ipc abort, flush messages, and for data  messages.
** It breaks the rules and uses some ipc internals for the purpose
** of testing and debugging.
** Parmeters:
**  sServers -- the ASCIIZ name of
**  msDelay -- the milliseconds to delay after receiving a data message
**      before forwarding.
**  priority -- if non-zero used priorty signals and set the task priority
**      to it.
*/
void
forward (char * sMe, int msDelay, Priority priority)
{
    int i;
    IpcB * ipcp;
    MailHandle me;
    char * cDatap;
    int sigNormal =  0;
    int sigPriority =  0;
    signal_code sig;
    int msgs;
    int forwards = 999;
    int mnum = 0;

    sigHandlerSet (forwardSigHandler);
    me = ipc_MailBoxCreate (sMe, priority, sigNormal, sigPriority);
    ipc_SetRcvTimeout (10000);
    pscr_init (commandline);
    pscr_show ("me:%s  mh:%x", me->name, me);
    if (!me)
    {
        fprintf (stderr, "Failed to Create me:%s", sMe);
        exit (2);
    }
    if (!me)
    {
        fprintf (stderr, "Could not create %s", sMe);
        exit (2);
    }
    for (msgs = 0;;)
    {
        sTestPriority ();
        ipcp = ipc_Rcv ();
        if (!ipcp)
        {
            pscr_show ("%s woke w/o msgs:%d", sMe, msgs);
            continue;
        }
        if (sigrcvd)
        {
            pscr_show ("%s rcvd SIG %x", sMe, sigrcvd);
            switch (sigrcvd)
            {
            case SIGQUIT:
                if (ipcp)
                    ipc_FreeBuffer (ipcp);
                pscr_show ("exiting");
                exit (SIGQUIT);
                break;
            default:
                break;
            }
            sigrcvd = 0;
        }
        msgs--;
        switch (ipc_GetUserType (ipcp))
        {
        case IPC_SYS_ABORT:
            pscr_show
            (
                "%s rcvd ABORT from %s",
                sMe, ipc_GetSender (ipcp)->name
            );
            ipc_Flush (0);
            doReply (ipcp, sMe, IPC_SYS_ABORTED); 
            break;
        case IPC_SYS_FLUSH:
            pscr_show
            (
                "%s rcvd FLUSH from %s",
                sMe, ipc_GetSender (ipcp)->name
            );
            ipc_FreeBuffer (ipcp);
            ipc_Flush (ipc_GetSender (ipcp));
            doReply (ipcp, sMe, IPC_SYS_FLUSHED); 
            break;
        case IPC_SYS_DATA:
            pscr_show ("rcv:%s:%s", ipcp->sender->name, ipc_GetDataP (ipcp));
            doForward (ipcp, sF);
            break;
        default:
            pscr_show
            (
                "%s rcvd FLUSH from %s",
                sMe, ipc_GetSender (ipcp)->name
            );
            ipc_FreeBuffer (ipcp);
            break;
        }
    }
}



/* parameters the ipcinit is looking for 
*/
char sSleepTime[16] = "-t";
char sPriority[16] = "-p";

char * sUsages[] =
{
    "fname rname [-t msdelay] [-p priotity]",
    "Test program to test forwarding messages to other tasks",
    "sname -- is the name of this tasks mail box -- if no sender"
        " default to argv[0]",
    "rname -- is the name of the target tasks mail box",
    "-t -- msdelay wait on recieving a message before trying againg",
    "-p -- priority of this task",
    "   t and p are decimal value",
    0
};


int
main (int argc, char ** argv)
{
    int clientFlag = 1;
    int i = 0;
    int msTime = 5000;
    Priority priority = Pri_DVP;
    char * sMep;
    char * sItp;


    sleep (1);
    usage (argc, argv, sUsages, 1);
	while (glob_link () < 0)
	{
		printf ("Sleeping while waiting for MMCInit to run (dummy)\n");
		sleep (1);
	}
    if (argc <= 1)
    {
        usePrint (argv[0], sUsages);
        return 1;
    }
    arg_Copy (argc, argv, commandline, sizeof (commandline));
    if (arg_OptGet (argc, argv, sSleepTime, sizeof (sSleepTime)))
        msTime = atoi (sSleepTime);
    if (arg_OptGet (argc, argv, sPriority, sizeof (sPriority)))
        priority = (Priority) atoi (sPriority);
    if (priority > Pri_Highest)
        priority = Pri_Highest;
    sMep = arg_Next (argc, argv);
    sF[1] = sMep[0];

    forward (sMep, msTime, priority);
    return 0;
}


