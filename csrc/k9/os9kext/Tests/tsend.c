
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** File: tsend.c - stand alone to test the ipc code.
**
** Author: emil
** $Author: emil $
**
** Purpose:
**  This preiodicly sends a message to some other task.
**
** Types/Clases:
**
** Program:
**  void main (int argc, char ** argv) -- 
**
** Notes:
**
** $Id: tsend.c,v 1.4 1994/08/17 01:00:08 emil Exp $
**
*/

# include <assert.h>
# include <signal.h>
# include <stdio.h>
# include <stdlib.h>
# include <modes.h>
# include <string.h>
# include <process.h>

# include "sighandle.h"
# include "ictvutil.h"
# define NO_IPC_INLINE
# include "ipc.h"
# include "ipcscreen.h"
# include "ipcuser.h"

# include "globMMC.h"

# define __TSEND_C__

# define JULIAN_TK  3           /* function code for _sysdate() */

static int sigrcvd = 0;

void
sendSigHandler (int sig)
{
    if (sigrcvd == SIGQUIT)
        exit (2);
    sigrcvd = sig;
}

void
doReply (IpcB * ipcp, char * append, int type)
{
    strcat (ipc_GetDataP (ipcp), " S-> ");
    strcat (ipc_GetDataP (ipcp), append);
    ipc_SetUserType (ipcp, type);
    ipc_Reply (ipcp);
}



u_int32
getTicks (void)
{
    int date;
    int time;
    short day;
    int32 ticks = -1;

    _sysdate(JULIAN_TK, &time, &date, &day, &ticks);
    return ticks & 0xffff;
}


process_id procID;
u_int16 pri, age, group, user;
int32 sched;
int
sTestPriority (void)
{
    int r;

    r = _os_id (&procID, &pri, &age, &sched, &group, &user);
    if ((u_int32) pri > (u_int32) PRIORITY (Pri_Highest))
    {
        fprintf (stderr, "Priority Error: %x/%x\n", pri, PRIORITY(Pri_Highest));
        exit (1);
    }
    return 0;
}

char commandline[128];



/*
** send -- waits in an infinite loop for messages, it waits
** for ipc abort, flush messages, and for data  messages.
** It breaks the rules and uses some ipc internals for the purpose
** of testing and debugging.
** Parmeters:
**  sServers -- the ASCIIZ name of
**  msDelay -- the milliseconds to delay after receiving a data message
**      before sending.
**  priority -- if non-zero used priorty signals and set the task priority
**      to it.
*/
void
send (char * sMe, char * sSend, int msDelay, Priority priority)
{
    int i, err;
    IpcB * ipcp;
    MailHandle me;
    MailHandle target;
    char * cDatap;
    int sigNormal =  0;
    int sigPriority =  0;
    signal_code sig;
    int msgs;
    int sends = 999;
    int mnum = 0;

    sigHandlerSet (sendSigHandler);
    me = ipc_MailBoxCreate (sMe, priority, sigNormal, sigPriority);
    ipc_SetRcvTimeout (10000);
    pscr_init (commandline);
    pscr_show ("me:%s  mh:%x", me->name, me);
    if (!me)
    {
        fprintf (stderr, "Failed to Create me:%s", sMe);
        exit (2);
    }
    while ((target = ipc_GetMailHandle (sSend)) == 0)
    {
        sleep (1);
        if ((unsigned) i++ > 10)
        {
            pscr_show ("%s waiting for %s to be created", sMe, sSend);
            i = 0;
        }
    }
    pscr_show ("target:%s mh:%x", sSend, target);
    if (!me)
    {
        fprintf (stderr, "Could not create %s", sMe);
        exit (2);
    }
    for (msgs = 0;;)
    {
        sTestPriority ();
        ipcp = ipc_RcvMSDelay (msDelay);
        if (!ipcp)
        {
            pscr_show ("%s woke w/o msgs:%d", sMe, msgs);
            if (msgs > 1)
            {
                int delay;

                delay = msgs;
                pscr_show ("%s adhead by %s by %d", sMe, sSend, msgs);
                ipcp = ipc_RcvMSDelay (-1);
                if (ipcp)
                {
                    pscr_show
                    (
                        "%s rcvd (%x) after ahead of:%s by %d ",
                        sMe, ipcp, sSend, msgs
                    );
                    msgs--;
                }
                while (delay-- > 0)
                    sleep (1); /* sleep for a sencond */
                continue;
            }
        }

        if (sigrcvd)
        {
            if (ipcp)
                ipc_FreeBuffer (ipcp);
            pscr_show ("%s rcvd SIG %x", sSend, sigrcvd);
            if (sigrcvd == SIGQUIT)
            {
                pscr_show ("exiting");
                exit (SIGQUIT);
            }
            sigrcvd = 0;
        }
        if (ipcp)
        {
            msgs--;
            switch (ipc_GetUserType (ipcp))
            {
            case IPC_SYS_ABORT:
                pscr_show
                (
                    "%s rcvd ABORT from %s",
                    sSend, ipc_GetSender (ipcp)->name
                );
                ipc_Flush (0);
                doReply (ipcp, sSend, IPC_SYS_ABORTED); 
                break;
            case IPC_SYS_FLUSH:
                pscr_show
                (
                    "%s rcvd FLUSH from %s",
                    sSend, ipc_GetSender (ipcp)->name
                );
                ipc_FreeBuffer (ipcp);
                ipc_Flush (ipc_GetSender (ipcp));
                doReply (ipcp, sSend, IPC_SYS_FLUSHED); 
                break;
            case IPC_SYS_ACK:
                if ((sends++ % 3) == 0)
                {
                    pscr_show
                    (
                        "ACK:%s:%s", ipcp->sender->name, ipc_GetDataP (ipcp)
                    );
                }
                ipc_FreeBuffer (ipcp);
                break;
            default:
                pscr_show
                (
                    "%s rcvd FLUSH from %s",
                    sSend, ipc_GetSender (ipcp)->name
                );
                ipc_FreeBuffer (ipcp);
                break;
            }
        }
        ipcp = ipc_NewBuffer ();
        sprintf
        (
            ipc_GetDataP (ipcp), "%s:%d--", sMe, mnum++
        );
        ipc_SetUserType (ipcp, IPC_SYS_DATA);
        ipc_Send (ipcp, target);
    }
}



/* parameters the ipcinit is looking for 
*/
char sSendtoMailBox[16] = "-r";
char sSleepTime[16] = "-t";
char sPriority[16] = "-p";

char * sUsages[] =
{
    "sname rname [-t msdelay] [-p priotity]",
    "or -r rname [-t msdelay] [-p priotity]",
    "Test program to test sending messages to other tasks",
    "sname -- is the name of this tasks mail box -- if no sender"
        " default to argv[0]",
    "rname -- is the name of the target tasks mail box",
    "-t -- msdelay wait on recieving a message before trying againg",
    "-p -- priority of this task",
    "   t and p are decimal value",
    0
};

int
main (int argc, char ** argv)
{
    int clientFlag = 1;
    int i = 0;
    int msTime = 5000;
    Priority priority = Pri_AVGSM;
    char * sMep;
    char * sItp;


    sleep (3);
    usage (argc, argv, sUsages, 1);
	while (glob_link () < 0)
	{
		printf ("Sleeping while waiting for MMCInit to run (dummy)\n");
		sleep (1);
	}
    if (argc <= 1)
    {
        usePrint (argv[0], sUsages);
        return 1;
    }
    arg_Copy (argc, argv, commandline, sizeof (commandline));
    sItp = arg_OptGet (argc, argv, sSendtoMailBox, sizeof (sSendtoMailBox));
    if (arg_OptGet (argc, argv, sSleepTime, sizeof (sSleepTime)))
        msTime = atoi (sSleepTime);
    if (arg_OptGet (argc, argv, sPriority, sizeof (sPriority)))
        priority = (Priority) atoi (sPriority);
    if (priority > Pri_Highest)
        priority = Pri_Highest;
    sMep = arg_Next (argc, argv);
    if (!sItp)
        if ((sItp = arg_Next (argc, argv)) == 0)
        {
            usePrint (argv[0], sUsages);
            return 1;
        }

    send (sMep, sItp, msTime, priority);
    return 0;
}


