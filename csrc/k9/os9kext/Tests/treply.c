
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** File: treply.c - stand alone to test the ipc code.
**
** Author: emil
** $Author: emil $
**
** Purpose:
** This code waits for messages and then replies.
**
** Types/Clases:
**
** Program:
**  void main (int argc, char ** argv) -- 
**
** Notes:
**
** $Id: treply.c,v 1.4 1994/08/17 01:00:07 emil Exp $
**
*/

# include <assert.h>
# include <signal.h>
# include <types.h>
# include <stdio.h>
# include <stdlib.h>
# include <modes.h>
# include <string.h>
# include <process.h>

# include "ictvutil.h"
# include "sighandle.h"
# include "ipcuser.h"
# define NO_IPC_INLINE
# include "ipc.h"
# include "ipcscreen.h"

# include "globMMC.h"

# define __TREPLY_C__

# define JULIAN_TK  3           /* function code for _sysdate() */

char commandline[128];

static int sigrcvd = 0;
static int ireply = 0;

void
replySigHandler (int sig)
{
    if (sigrcvd == SIGQUIT)
        exit (2);
    sigrcvd = sig;
}

void
doReply (IpcB * ipcp, char * append, int type)
{
    pscr_show
    (
        "%s->%s(%d)<-%s",
        ipcp->sender->name, append, ireply, ipc_GetDataP (ipcp)
    );
    strcat (ipc_GetDataP (ipcp), "<->");
    strcat (ipc_GetDataP (ipcp), append);
    ipc_SetUserType (ipcp, type);
    ipc_Reply (ipcp);
}


u_int32
getTicks (void)
{
    int date;
    int time;
    short day;
    int32 ticks = -1;

    _sysdate(JULIAN_TK, &time, &date, &day, &ticks);
    return ticks & 0xffff;
}



static int
realwork (void)
{
    if (sigrcvd)
        return ireply;
    else
        return -ireply;
}

static int
rwork (int work)
{
    int i;
    int s;

    for (i = work; i > 0; i--)
        s += realwork ();
    return s;
}



static int
rTestPriority (void)
{
    process_id procID;
    u_int16 pri, age, group, user;
    int32 sched;
    int r;

    r = _os_id (&procID, &pri, &age, &sched, &group, &user);
    if ((u_int32) pri > (u_int32) PRIORITY (Pri_Highest))
    {
        fprintf (stderr, "Priority Error: %x/%x\n", pri, PRIORITY (Pri_Highest));
        exit (1);
    }
    return 0;
}


/*
** reply -- waits in an infinite loop for messages, it waits
** for ipc abort, flush messages, and for data  messages.
** It breaks the rules and uses some ipc internals for the purpose
** of testing and debugging.
** Parmeters:
**  sServers -- the ASCIIZ name of
**  msDelay -- the milliseconds to delay after receiving a data message
**      before replying.
**  priority -- if non-zero used priorty signals and set the task priority
**      to it.
*/
void
reply (char * sMe, int msDelay, int priority)
{
    int i, err;
    IpcB * ipcp;
    MailHandle me;
    char * cDatap;
    int sigNormal =  0;
    int sigPriority =  0;
    signal_code sig;
    u_int32 ticks;

    sigHandlerSet (replySigHandler);
    me = ipc_MailBoxCreate (sMe, Pri_VGL, sigNormal, priority);
    ipc_SetRcvTimeout (10000);
    pscr_init (commandline);
    pscr_show ("%s:delay:%d/%d", sMe, msDelay, ticks);
    if (!me)
    {
        fprintf (stderr, "Failed to Create me:%s", sMe);
        exit (2);
    }
    if (priority)
    {
        _os_setpr (me->procID, priority);
    }
    for (;;)
    {
        rTestPriority ();
        ipcp = ipc_Rcv ();
        ireply++;

        if (sigrcvd)
        {
            if (ipcp)
                ipc_FreeBuffer (ipcp);
            pscr_show
            (
                "%d:%s rcvd SIG %x",
                ireply, sMe, sigrcvd, 0, 0
            );
            if (sigrcvd == SIGQUIT)
            {
                pscr_show ("%s:exiting", sMe);
                exit (SIGQUIT);
            }
            sigrcvd = 0;
        }
        if (!ipcp)
        {
            pscr_show ("%d:%s rcvd NULL IPC", ireply, sMe);
            continue;
        }
        switch (ipc_GetUserType (ipcp))
        {
        case IPC_SYS_ABORT:
            i = ipc_Flush (0);
            pscr_show
            (
                "%s->ABORT Flshd:%d", ipc_GetSender (ipcp)->name, i
            );
            doReply (ipcp, sMe, IPC_SYS_ABORTED); 
            break;
        case IPC_SYS_FLUSH:
            i = ipc_Flush (ipc_GetSender (ipcp));
            pscr_show
            (
                "%s->FLUSH Flshd%d", ipc_GetSender (ipcp)->name, i
            );
            doReply (ipcp, sMe, IPC_SYS_FLUSHED); 
            break;
        case IPC_SYS_DATA:
            if (msDelay > 8)
            {
                /* Do some real work and ... */
                rwork (msDelay);
                /* Sleep a bit also */
                ticks = msDelay / 8;
                _os_sleep (&ticks, &sig);
                if (sig)
                    pscr_show ("%s woke early", sMe);
            }
            doReply (ipcp, sMe, IPC_SYS_ACK); 
            break;
        default:
            pscr_show
            (
                "%s unexpected msg ut:%d", sMe,
                ipc_UserTypeString (ipc_GetUserType (ipcp))
            );
            ipc_FreeBuffer (ipcp);
            break;
        }
    }
}

/* parameters the ipcinit is looking for 
**  Usage: ipcinit [-b buffersize] [-n numberofbuffers] [-m numberofmailboxes]
**                      [-N IPCMMName]
*/
char * sUsages[] = 
{
    "rname [-t msdelay] [-p priority]",
    "Program to test the ipc reply mechanism",
    "rname -- Reply Mail Box Name"
    "-t -- msdelay before reply (decimal number)",
    "-p -- priority of task (decimal number)",
    0
};
char sMeMailBox[16] = "-c";
char sSleepTime[16] = "-t";
char sPriority[16] = "-p";



int
main (int argc, char ** argv)
{
    int clientFlag = 1;
    int i = 0;
    int msTime = 100;
    int priority = 0;
    char * sMep;


    sleep (1);
    usage (argc, argv, sUsages, 1);
	while (glob_link () < 0)
	{
		printf ("Sleeping while waiting for MMCInit to run (dummy)\n");
		sleep (1);
	}
    if (argc <= 1)
    {
        usePrint (argv[0], sUsages);
        return 1;
    }
    arg_Copy (argc, argv, commandline, sizeof (commandline));
    if (arg_OptGet (argc, argv, sSleepTime, sizeof (sSleepTime)))
        msTime = atoi (sSleepTime);
    if (arg_OptGet (argc, argv, sPriority, sizeof (sPriority)))
        priority = atoi (sPriority);
    sMep = arg_Next (argc, argv);
    if (!sMep)
        sMep = argv[0];

    reply (sMep, msTime, priority);
    return 0;
}


