
/*
**
** Copyright (C) 1994,  ICTV Inc.
**                      280 Martin Ave.,
**                      Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** File: tsend.c - stand alone to test the ipc code.
**
** Purpose:
**  This preiodicly sends a synchronous message to some other task.
**
** Types/Clases:
**
** Program:
**  void main (int argc, char ** argv) -- 
**
** Notes:
**
** $Id: tsync.c,v 1.4 1994/08/17 01:00:10 emil Exp $
**
*/

# include <assert.h>
# include <signal.h>
# include <stdio.h>
# include <stdlib.h>
# include <modes.h>
# include <string.h>

# include "sighandle.h"
# include "ictvutil.h"
# define NO_IPC_INLINE
# include "ipc.h"
# include "ipcscreen.h"
# include "ipcuser.h"

# include "globMMC.h"

# define __TSYNC_C__

# define JULIAN_TK  3           /* function code for _sysdate() */

static int sigrcvd = 0;

void
sendSigHandler (int sig)
{
    if (sigrcvd == SIGQUIT)
        exit (2);
    sigrcvd = sig;
}

void
doReply (IpcB * ipcp, char * append, int type)
{
    strcat (ipc_GetDataP (ipcp), " S-> ");
    strcat (ipc_GetDataP (ipcp), append);
    ipc_SetUserType (ipcp, type);
    ipc_Reply (ipcp);
}



u_int32
getTicks (void)
{
    int date;
    int time;
    short day;
    int32 ticks = -1;

    _sysdate(JULIAN_TK, &time, &date, &day, &ticks);
    return ticks & 0xffff;
}

char commandline[128];


/*
** sync -- waits in an infinite loop for messages, it waits
** for ipc abort, flush messages, and for data  messages.
** It breaks the rules and uses some ipc internals for the purpose
** of testing and debugging.
** Parmeters:
**  sServers -- the ASCIIZ name of
**  msDelay -- the milliseconds to delay after receiving a data message
**      before sending.
**  priority -- if non-zero used priorty signals and set the task priority
**      to it.
*/
void
sync (char * sMe, char * sTarget, int msDelay, Priority priority)
{
    int i;
    IpcB * ipcp;
    IpcB * fipcp;
    IpcB * ripcp;
    MailHandle me;
    MailHandle target;
    char * cDatap;
    int sigNormal =  0;
    int sigPriority =  0;
    signal_code sig;
    int msgs;
    int mnum = 0;

    sigHandlerSet (sendSigHandler);
    me = ipc_MailBoxCreate (sMe, priority, sigNormal, sigPriority);
    ipc_SetRcvTimeout (10000);
    pscr_init (commandline);
    pscr_show ("%s mb:%x\n", sMe, me);
    while ((target = ipc_GetMailHandle (sTarget)) == 0)
    {
        sleep (1);
    }
    pscr_show ("me:%s has a delay of:%d\n", sMe, msDelay);
    if (!me)
    {
        fprintf (stderr, "Could not create %s\n", sTarget);
        exit (2);
    }
    ipc_SetRcvTimeout (msDelay);
    for (msgs = 0;;)
    {
        ipcp = ipc_RcvMSDelay (msDelay);
        if (!ipcp)
        {
            pscr_show ("%s woke w/o msg %d\n", sMe, msgs);
            if (msgs > 4)
            {
                pscr_show ("Leaving for msgs");
                exit (0);
            }
        }

        if (sigrcvd)
        {
            pscr_show ("%s rcvd SIGNAL %x\n", sTarget, sigrcvd);
            if (sigrcvd == SIGQUIT)
            {
                pscr_show ("exiting\n");
                exit (SIGQUIT);
            }
            sigrcvd = 0;
        }
        if (ipcp)
        {
            msgs--;
            switch (ipc_GetUserType (ipcp))
            {
            case IPC_SYS_ABORT:
                pscr_show
                (
                    "%s rcvd ABORT from %s\n",
                    sTarget, ipc_GetSender (ipcp)->name
                );
                ipc_Flush (0);
                doReply (ipcp, sTarget, IPC_SYS_ABORTED); 
                break;
            case IPC_SYS_FLUSH:
                pscr_show
                (
                    "%s rcvd FLUSH from %s\n",
                    sTarget, ipc_GetSender (ipcp)->name
                );
                ipc_FreeBuffer (ipcp);
                ipc_Flush (ipc_GetSender (ipcp));
                doReply (ipcp, sTarget, IPC_SYS_FLUSHED); 
                break;
            case IPC_SYS_ACK:
                pscr_show
                (
                    "rxACK of %s\n", ipc_GetDataP (ipcp)
                );
                ipc_FreeBuffer (ipcp);
                break;
            default:
                pscr_show
                (
                    "%s rcvd FLUSH from %s\n",
                    sTarget, ipc_GetSender (ipcp)->name
                );
                ipc_FreeBuffer (ipcp);
                break;
            }
        }
        /* First do a normal send, then ... */
        fipcp = ipc_NewBuffer ();
        strcpy (ipc_GetDataP (fipcp), "BEFORE");
        ipc_SetUserType (fipcp, IPC_SYS_DATA);
        ipc_Send (fipcp, target);

        /* Do sync send! We should get ... */
        ipcp = ipc_NewBuffer ();
        sprintf (ipc_GetDataP (ipcp), "%s->%d", sMe, mnum++);
        ipc_SetUserType (ipcp, IPC_SYS_DATA);
        ripcp = ipc_SendSync (ipcp, target);
        pscr_show ("<-> %s\n", ipc_GetDataP (ipcp));
        if (ipcp != ripcp)
        {
            pscr_show ("Bad IPC order %x != %x", ipcp, ripcp);
            /*printf ("Bad IPC order: %x != %x\n", ipcp, ripcp); */
        }
        else
            ipc_FreeBuffer (ripcp);
    }
}



/* parameters the ipcinit is looking for 
*/
char sTargetMailBox[16] = "-r";
char sSleepTime[16] = "-t";
char sPriority[16] = "-p";
char sOpts[32];

char * sUsages[] =
{
    "sname rname [-t msdelay] [-p priotity]",
    "or -r rname [-t msdelay] [-p priotity]",
    "Test program to test sending messages to other tasks",
    "sname -- is the name of this tasks mail box -- if no sender"
        " default to argv[0]",
    "rname -- is the name of the target tasks mail box",
    "-t -- msdelay wait on recieving a message before trying againg",
    "-p -- priority of this task",
    "   t and p are decimal value",
    0
};



int
main (int argc, char ** argv)
{
    int clientFlag = 1;
    int i = 0;
    int msTime = 5000;
    Priority  pri = Pri_AVGSM;

    char * sMep;
    char * sItp;


    sleep (3);
    usage (argc, argv, sUsages, 1);
	while (glob_link () < 0)
	{
		printf ("Sleeping while waiting for MMCInit to run (dummy)\n");
		sleep (1);
	}
    if (argc <= 1)
    {
        usePrint (argv[0], sUsages);
        return 1;
    }
    arg_Copy (argc, argv, commandline, sizeof (commandline));
    sItp = arg_OptGet (argc, argv, sTargetMailBox, sizeof (sTargetMailBox));
    if (arg_OptGet (argc, argv, sSleepTime, sizeof (sSleepTime)))
        msTime = atoi (sSleepTime);
    if (arg_OptGet (argc, argv, sPriority, sizeof (sPriority)))
        pri = atoi (sPriority);
    i = arg_OptRemove (argc, argv, sOpts);
    if (i)
    {
        usePrint (argv[0], sUsages);
        exit (0);
    }
    sMep = arg_Next (argc, argv);
    if (!sItp)
        if ((sItp = arg_Next (argc, argv)) == 0)
        {
            usePrint (argv[0], sUsages);
            return 1;
        }

    sync (sMep, sItp, msTime, pri);
    puts ("exiting testipc main()");
    return 0;
}


