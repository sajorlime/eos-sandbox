/*
**
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** File: ICTV/libsrc/os9kext/Tests/tTime.c
**
** Author: emil
**
** Purpose:
**  This file is part of the utility library
**
** Class/Typedefs:
**
** Functions:
**
** Notes:
**
** $Id: tTime.c,v 1.1 1994/10/21 22:37:39 mikes Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/Tests/tTime.c,v $
**
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <types.h>


#include "ictvcommon.h"
#include "sighandle.h"
#include "siguser.h"

#define __TTIME_C__

int Sig;

typedef struct _Int64
{
	u_int32 low32;
	u_int32 high32;
} Int64;

Int64 my64 = {0, 0};

void
inc64 (Int64 * i64p)
{
	if (++i64p->low32 == 0)
		i64p->high32++;
}

void
sigHandler (int sig)
{
	Sig = sig;
}

error_code _os_alarm_cycle(alarm_id *alrm_id, signal_code signal, u_int32 time);

int display(Int64 cur, Int64 max)
{
  int percent;
  int i;
  printf ("%08d%08d  ", my64.high32, my64.low32);
  while (max.high32)
  {
    max.low32 = (max.low32 /2) + (max.high32 % 2);
    max.high32 /= 2;
    cur.low32 = (cur.low32 /2) + (cur.high32 % 2);
    cur.high32 /= 2;
  }
  percent =  (100*cur.low32)/max.low32;
  printf("(%03d %%)", percent);
  for (i=0; i < (percent/2);i++)
    printf("X");
  printf("\n");
}

int alm_cycle (int sigcode, int time_interval)
{
  alarm_id alrm_id = 1; /* I don't know if this value matters. */
  error_code error;

  if (error = _os_alarm_cycle(&alrm_id, sigcode, time_interval))
  {
     GLOBAL_MSG(("tTime alm_cycle: Alarm not set!\n"), DBG_ASSERT);
  }
}

int
main (int argc, char ** argv)
{
	int time = 0x80000100; /* this should be one second */
	int i;
        int calibrate_time = 4;
        int first_time = 1;
        Int64 fixed64 = { 0, 0 };

	if (argc > 1)
	{
		i = atoi (argv[1]);
		if (i > 0)
			time = 0x80000000 + i * 0x100;
	}
	printf ("%x\n", time);
        sigHandlerSet (sigHandler);
	alm_cycle (SIG_ICTV_ALARM, time);
	while (1)
	{
		char c = 0;

		switch (Sig)
		{
		case SIG_ICTV_ALARM:
                        if (!calibrate_time)
                          display(my64, fixed64);
                        if (calibrate_time && 
                             ( (fixed64.high32 < my64.high32) ||
                               (fixed64.high32 == my64.high32 &&
                                fixed64.low32 < my64.low32)
                             )
                           )
                        {
                          fixed64.high32 = my64.high32;
                          fixed64.low32 = my64.low32; 
                          calibrate_time--;
                        }
                        my64.high32 = 0;
                        my64.low32 = 0;
			Sig = 0;
			break;
		case SIGQUIT:
		case SIGINT:
			exit (0);
			break;
                default:
                        break;
		}
		inc64 (&my64);
	}
}


