
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** File: tSuspend.c - stand alone to test the ipc code.
**
** Author: emil
** $Author: emil $
**
** Purpose:
**  This preiodicly suspends a message to some other task.
**
** Types/Clases:
**
** Program:
**  void main (int argc, char ** argv) -- 
**
** Notes:
**
** $Id: tSuspend.c,v 1.1 1994/09/21 23:32:40 emil Exp $
**
*/

# include <assert.h>
# include <signal.h>
# include <stdio.h>
# include <stdlib.h>
# include <modes.h>
# include <string.h>
# include <process.h>

# include "sighandle.h"
# include "ictvutil.h"
# define NO_IPC_INLINE
# include "rpcServer.h"
# include "ipcuser.h"
# include "k9misc.h"

# include "globMMC.h"

# define __TSUSPEND_C__

static int SigRcvd = 0;

char commandline[128];

typedef struct _LDATA
{
	int data;
} LDATA;

LDATA ldata;

/*
** suspend -- waits in an infinite loop for messages, it waits
** for ipc abort, flush messages, and for data  messages.
** It breaks the rules and uses some ipc internals for the purpose
** of testing and debugging.
** Parmeters:
**  sServers -- the ASCIIZ name of
**  msDelay -- the milliseconds to delay after receiving a data message
**      before suspending.
**  priority -- if non-zero used priorty signals and set the task priority
**      to it.
*/
void
suspend (char * sMe, char * sReplyTask, int msDelay, Priority priority)
{
    int i, err;
    IpcB * ipcp;
    MailHandle me;
    MailHandle target;
    char * cDatap;
    signal_code sig;
    int msgs;
    int mnum = 0;
	process_id replyId;

    me = ipc_MailBoxCreate (sMe, priority, 0, 0);
    printf ("me:%s  mh:%x\n", me->name, me);
    if (!me)
    {
        printf ("Failed to Create me:%s\n", sMe);
        return;
    }
    while ((target = ipc_GetMailHandle (sReplyTask)) == 0)
    {
        sleep (1);
        if ((unsigned) i++ > 10)
        {
            printf ("%s waiting for %s to be created\n", sMe, sReplyTask);
            i = 0;
        }
    }
	sleep (1);
	replyId = mh_getProcId (target);
    printf ("target:%s mh:%x\n", sReplyTask, target); fflush (stdout);
	sleep (1);
	RPC_SEND (IPC_SYS_DATA, &ldata, target);
	ipcp = ipc_RcvMSDelay (msDelay);
	if (!ipcp)
	{
		printf ("%s woke w/o msgs:%d\n", sMe, msgs); fflush (stdout);
		goto suspend_end;
		exit (0);
	}
	printf ("%s woke, now suspend %s\n", sReplyTask); fflush (stdout);
	sleep (1);

	_os_suspend (replyId);
	RPC_SEND (IPC_SYS_DATA, &ldata, target);
	ipcp = ipc_RcvMSDelay (msDelay);
	if (ipcp)
	{
		printf ("%s woke with unexpected message\n", sMe); fflush (stdout);
		goto suspend_end;
		exit (0);
	}
	printf ("%s woke, now wake %s\n", sReplyTask); fflush (stdout);
	_os_aproc (replyId);

	ipcp = ipc_RcvMSDelay (1000);
	if (!ipcp)
	{
		printf ("%s no message -- bad\n", sMe); fflush (stdout);
		exit (0);
	}
	printf ("%s expected messag waiting -- good\n", sMe); fflush (stdout);
suspend_end:
	_os_send (replyId, SIGQUIT);
}

void
itHandler (int sig)
{
	SigRcvd = sig;
}


/* parameters the ipcinit is looking for 
*/
char sReplyTasktoMailBox[16] = "-r";
char sSleepTime[16] = "-t";
char sPriority[16] = "-p";

char * sUsages[] =
{
    "sname rname [-t msdelay] [-p priotity]",
    "or -r rname [-t msdelay] [-p priotity]",
    "Test program to test suspending messages to other tasks",
    "sname -- is the name of this tasks mail box -- if no suspender"
        " default to argv[0]",
    "rname -- is the name of the target tasks mail box",
    "-t -- msdelay wait on recieving a message before trying againg",
    "-p -- priority of this task",
    "   t and p are decimal value",
    0
};


char *argblk[] = {0, 0, 0};
char Arg2[] = "DONT_USE_THIS_AS_REAL_PARAMETER";


int
main (int argc, char ** argv)
{
    int clientFlag = 1;
    int i = 0;
    int msTime = 5000;
    Priority priority = 1;
    char * sMep = "Suspend";
    char * sItp = "It";
	process_id pid;


    usage (argc, argv, sUsages, 1);
	if (glob_link () < 0)
	{
		printf ("Sleeping while waiting for MMCInit to run (dummy)\n");
		return 1;
	}
	if (argv[1] && (strcmp (argv[1], Arg2) == 0))
	{
		MailHandle me;
		IpcB * ipcp;

		sigHandlerSet (itHandler);
		me = ipc_MailBoxCreate (sItp, priority, 0, 0);
		printf ("%s: mh :%x\n", me); fflush (stdout);
		ipcp = ipc_RcvMSDelay (-1);
		printf ("%s: ipcp :%x\n", ipcp); fflush (stdout);
		ipc_Reply (ipcp);
		ipcp = ipc_RcvMSDelay (-1);
		printf ("%s: ipcp :%x\n", ipcp); fflush (stdout);
		ipc_Reply (ipcp);
		sleep (0);
		printf ("%s:rcvd sig:%d\n", sItp, SigRcvd);
		exit (0);
	}
	argblk[0] = argv[0];
	argblk[1] = Arg2;
	argblk[2] = NULL;
	/* set the forked process to max priority until it
	** create its mail blox. */
	if (k9_fork (argblk, &pid, 5, 0))
	{
		printf ("%s: Failed to fork self.\n", argv[0]);
		exit (0);
	}
    suspend (sMep, sItp, 1000, priority);
    return 0;
}


