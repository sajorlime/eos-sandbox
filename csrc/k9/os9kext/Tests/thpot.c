
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** File: thpot.c - Forks to 3 processes that test ipc speed.
**
** Author: mikes (borrowed from emil's tsend)
**
** Purpose:
**   Tests the time it takes for ipc packet to make a full loop.
**
** Types/Clases:
**
** Program:
**  void main (int argc, char ** argv) -- 
**
** Notes:
**
** $Id: thpot.c,v 1.1 1994/10/29 00:35:37 mikes Exp $
**
*/

# include <assert.h>
# include <signal.h>
# include <stdio.h>
# include <stdlib.h>
# include <modes.h>
# include <string.h>
# include <process.h>

# include "sighandle.h"
# include "ictvutil.h"
# define NO_IPC_INLINE
# include "ipc.h"
# include "ipcscreen.h"
# include "ipcuser.h"
# include "k9fork.h"
# include "k9misc.h"

# include "globMMC.h"

# define __TSEND_C__

# define JULIAN_TK  3           /* function code for _sysdate() */

int forwardMain(int argc, char **argv);
int replyMain(int argc, char **argv);
int parentMain(int argc, char **argv);
int mainRun(int argc, char **argv);
int proc_setup(void);

static int sigrcvd = 0;

typedef struct mainArgs
{
  char *arg1;
  int (* mainfunp) (int argc, char **argv);
} mainArgs;

typedef struct timedata
{
  int time;
  int ticks;
} timedata;

#define EXEC_FORWARD "I_AM_FORWARD!"
#define EXEC_REPLY "I_AM_REPLY!"
mainArgs forkmains[] =
{
  { EXEC_FORWARD, forwardMain },
  { EXEC_REPLY, replyMain },
  { 0, parentMain }
};



int
getTime (timedata *curtime)
{
    int date;
    short day;

    _sysdate(JULIAN_TK, &(curtime->time), &date, &day, &(curtime->ticks));
}

void
SigHandler (int sig)
{
    if (sigrcvd == SIGQUIT)
        exit (2);
    sigrcvd = sig;
}

/*
** forward -- waits in an infinite loop for messages, it waits
** for ipc abort, flush messages, and for data  messages.
** It breaks the rules and uses some ipc internals for the purpose
** of testing and debugging.
** Parmeters:
**  sMe    -- My mailbox name
**  sForward -- Target's mailbox name
**  msDelay -- the milliseconds to delay after receiving a data message
**      before forwarding.
**  priority -- if non-zero used priorty signals and set the task priority
**      to it.
*/
void
forward (char * sMe, char * sForward, int msDelay, Priority priority)
{
    IpcB * ipcp;
    MailHandle me;
    MailHandle mh;
    int sigNormal = 0;
    int sigPriority = 0;

    sigHandlerSet (SigHandler);
    me = ipc_MailBoxCreate (sMe, priority, sigNormal, sigPriority);
    ipc_SetRcvTimeout (10000);
    if (!me)
    {
        fprintf (stderr, "Failed to Create me:%s\n", sMe);
        exit (2);
    }
    while (!(mh = ipc_GetMailHandle (sForward)))
    {
        fprintf (stderr, "Cannot find %s. Ouch ouch, hot hot.", sForward);
        sleep (1);
    }
    for (;;)
    {
        ipcp = ipc_Rcv ();
        if (!ipcp)
            continue;
# if 0
        strcat(ipc_GetDataP (ipcp), "ForwardPot ->");
# endif
        ipc_Forward (ipcp, mh);
    }
}

/*
** reply -- waits in an infinite loop for messages, it waits
** for ipc abort, flush messages, and for data  messages.
** It breaks the rules and uses some ipc internals for the purpose
** of testing and debugging.
** Parmeters:
**  sMe    -- My mailbox name
**  msDelay -- the milliseconds to delay after receiving a data message
**      before replying.
**  priority -- if non-zero used priorty signals and set the task priority
**      to it.
*/
void
reply (char * sMe, int msDelay, int priority)
{
    int i, err;
    IpcB * ipcp;
    MailHandle me;
    int sigNormal = 0;
    int sigPriority = 0;

    sigHandlerSet (SigHandler);
    me = ipc_MailBoxCreate (sMe, priority, sigNormal, sigPriority);
    ipc_SetRcvTimeout (10000);
    if (!me)
    {
        fprintf (stderr, "Failed to Create me:%s\n", sMe);
        exit (2);
    }
    if (priority)
    {
        _os_setpr (me->procID, priority);
    }
    for (;;)
    {
        ipcp = ipc_Rcv ();
# if 0
        strcat(ipc_GetDataP (ipcp), "ReplyPot ->");
# endif
        ipc_Reply (ipcp);
    }
}

/*
** send -- waits in an infinite loop for messages, it waits
** for ipc abort, flush messages, and for data  messages.
** It breaks the rules and uses some ipc internals for the purpose
** of testing and debugging.
** Parmeters:
**  sMe    -- My mailbox name
**  sSend  -- Target's mailbox name
**  msDelay -- the milliseconds to delay after receiving a data message
**      before sending.
**  priority -- if non-zero used priorty signals and set the task priority
**      to it.
*/
void
send (char * sMe, char * sSend, int msDelay, Priority priority, int loop)
{
    timedata starttime, endtime;
    IpcB * ipcp;
    MailHandle me;
    MailHandle target;
    int sigNormal = 0;
    int sigPriority = 0;
    signal_code sig;
    int seconds;
    float fraction;
    int num;

    sigHandlerSet (SigHandler);
    me = ipc_MailBoxCreate (sMe, priority, sigNormal, sigPriority);
    ipc_SetRcvTimeout (10000);
    if (!me)
    {
        fprintf (stderr, "Failed to Create me:%s\n", sMe);
        exit (2);
    }
    while ((target = ipc_GetMailHandle (sSend)) == 0)
    {
        printf("Can't find %s. Ouch, ouch, hot hot.\n", sSend);
        sleep (1);
    }
    if (!me)
    {
        fprintf (stderr, "Could not create %s", sMe);
        exit (2);
    }
  
    /******************
    * Infinite testing
    ******************/
    for (;;)
    {
      getTime(&starttime);
      /******************************
      * Here we test multiple trips
      ******************************/
      for(num = 0; num < loop; num++)
      {
        ipcp = ipc_NewBuffer ();
# if 0
        strcpy(ipc_GetDataP (ipcp), "(start) SendPot ->");
# endif
        ipc_SetUserType (ipcp, IPC_SYS_DATA);
        ipc_Send (ipcp, target);

        while (!(ipcp = ipc_RcvMSDelay (msDelay)))
        {
          if (sigrcvd)
          {
              pscr_show ("exiting");
              exit (SIGQUIT);
          }
        }
        /* We have finished a full cycle! */
        ipc_FreeBuffer(ipcp);
      }
      /**********************
      * Now we check the time
      **********************/
      getTime(&endtime);
      if (ipc_GetSender(ipcp) == me)
      {
        printf("BAD! Replypot is juggling to itself!\n");
        exit(2);
      }
      seconds = endtime.time - starttime.time;
      if (!endtime.ticks)
      {
        printf("ERROR: Not counting ticks!\n");
        exit(2);
      }
      fraction = ( ((float) (endtime.ticks & 0xffff))/
                   ((float) (endtime.ticks >> 16)))
                  - 
                 ( ((float)(starttime.ticks & 0xffff))/
                   ((float)(starttime.ticks >> 16)));
      if (seconds)
      {
        printf("Round trip took %f seconds for %d loops. Hot!\n",
                seconds+fraction, loop);
      }
      else /* Just show fraction */
      {
        printf("Round trip took %f seconds for %d loops. Really Hot!\n",
               fraction, loop);
      }
    }    
}

char commandline[128];

/* parameters the ipcinit is looking for 
*/
char sSendtoMailBox[16] = "-r";
char sSleepTime[16] = "-t";
char sPriority[16] = "-p";

char * sUsages[] =
{
    "<# of loops before report>",
    0
};

int 
proc_setup()
{
    while (glob_link () < 0)
    {
      printf ("Where is that darned MMCInit and its shared memory?  ");
      printf ("I think I will sleep till it gets here.\n");
      sleep (1);
    }
    return 1;
}

int
fork (char * myName, char * arg1)
{
    MailHandle mh;
    int i;
    process_id pid;
    static char * Argblk[3] = {0};
    
    Argblk[0] = myName;
    Argblk[1] = arg1;
    Argblk[2] = 0;

    if (k9_fork (Argblk, &pid, 5, 0))
    {
        return 0;
    }
    k9_defer ();
    return 1;
}

int
main (int argc, char ** argv)
{
  mainRun(argc, argv);
}

int
mainRun(int argc, char **argv)
{
  char *arg1 = "Stupid phoney value";
  int i;

  if (argc > 1)
  {
    arg1 = argv[1];
  }
  
  for(i=0; i < sizeof(forkmains)/sizeof(mainArgs)-1; i++)
  {
    if (!strcmp(arg1, forkmains[i].arg1))
      break;
  }
  
  forkmains[i].mainfunp(argc, argv);
}

int msTime = 5000;
Priority priority = Pri_AVGSM;

int
parentMain (int argc, char ** argv)
{
    int clientFlag = 1;
    int i = 0;
    char * sMep;
    char * sItp;
    int loop = 10000;

    usage (argc, argv, sUsages, 1);
    if (argc > 1)
    {
      sscanf(argv[1], "%d", &loop);
    }
    
    /*******************************************************
    **
    **  Here we fork into the 3 programs we need:
    **    a sender (SendPot)
    **    a forwarder (ForwardPot)
    **    a replier (ReplyPot)
    **
    **  All timing functionality goes into the Sender.
    **  Therefore, I have made the parent the Sender.
    **
    *******************************************************/

    fork("thpot", EXEC_REPLY);
    fork("thpot", EXEC_FORWARD);
    proc_setup();
    send("SendPot", "ForwardPot", msTime, priority, loop);
}

int forwardMain (int argc, char **argv)
{
  proc_setup();
  forward("ForwardPot", "ReplyPot", msTime, priority);
}

int replyMain (int argc, char **argv)
{
  proc_setup();
  reply("ReplyPot", msTime, priority);
}
