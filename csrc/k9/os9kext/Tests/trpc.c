
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** File: tdefer.c - Forks to 2 processes that test defer speed.
**
** Author: mikes (borrowed from emil's tsend)
**
** Purpose:
**   Tests the time it takes for k9_defer.
**
** Types/Clases:
**
** Program:
**  void main (int argc, char ** argv) -- 
**
** Notes:
**
**
*/

# include <assert.h>
# include <signal.h>
# include <stdio.h>
# include <stdlib.h>
# include <modes.h>
# include <string.h>
# include <process.h>

# include "sighandle.h"
# include "ictvutil.h"
# define NO_IPC_INLINE
# include "ipc.h"
# include "ipcscreen.h"
# include "ipcuser.h"
# include "k9fork.h"
# include "k9misc.h"
# include "rpcServer.h"

# include "globMMC.h"

# define __TSEND_C__

# define JULIAN_TK  3           /* function code for _sysdate() */

int mainRun(int argc, char **argv);
int clientMain(int argc, char **argv);
int serverMain(int argc, char **argv);
int proc_setup(void);

static int sigrcvd = 0;

typedef struct mainArgs
{
  char *arg1;
  int (* mainfunp) (int argc, char **argv);
} mainArgs;

typedef struct timedata
{
  int time;
  int ticks;
} timedata;

#define EXEC_SERVER "I_AM_SERVER!"
#define EXEC_CLIENT "I_AM_CLIENT!"
mainArgs forkmains[] =
{
  { EXEC_CLIENT, clientMain },
  { EXEC_SERVER, serverMain },
  { 0, clientMain }
};



int
getTime (timedata *curtime)
{
    int date;
    short day;

    _sysdate(JULIAN_TK, &(curtime->time), &date, &day, &(curtime->ticks));
}

void
SigHandler (int sig)
{
    if (sigrcvd == SIGQUIT)
        exit (2);
    sigrcvd = sig;
}

#define QUESTION "What time is it?"
#define ANSWER "Four-thirty."

process_id parent_id, child_id;

typedef struct
{
  char string[40];
} message;
 
int simpleHandler(void *datap, int len)
{
  message *mes = (message *) datap;
  printf("Server: %s\n", datap);
  strcpy(datap, ANSWER);  
  printf("Server: %s\n", datap);
  RPC_RETURN(mes);
  return 0;
}

/*
** Defers a bunch of times.
** Parmeters:
**  msDelay -- the milliseconds to delay after receiving a data message
**      before sending.
**  priority -- if non-zero used priorty signals and set the task priority
**      to it.
*/
void
server (void)
{
    timedata starttime, endtime;
    MailHandle me;
    int sigNormal = 0;
    int sigPriority = 0;
    signal_code sig;
    int seconds;
    float fraction;
    int num;

    sigHandlerSet (SigHandler);
    /* sets priority */
  
    /******************
    * Server 
    ******************/
    printf("Setting up rpc server.\n");
    rpc_init(0);  /* default handler */

    rpc_UserRegister( 0, simpleHandler);

    rpc_Server();
}

char commandline[128];


/*
** Defers a bunch of times, then sees how long it took for it to do it.
** Parmeters:
**  msDelay -- the milliseconds to delay after receiving a data message
**      before sending.
**  priority -- if non-zero used priorty signals and set the task priority
**      to it.
*/
void
client (int loop)
{
    timedata starttime, endtime;
    MailHandle me;
    MailHandle target;
    int sigNormal = 0;
    int sigPriority = 0;
    signal_code sig;
    int seconds;
    float fraction;
    int num;
    int x;
    message question = { QUESTION };
    message answer;

    sigHandlerSet (SigHandler);
    /* sets priority */
  
    /******************
    * client
    ******************/
    ipc_SetRcvTimeout(10000);
    target = ipc_GetMailHandle("testserver");
    while (!target)
    {
       printf("Can't find testserver.\n");
       fflush(stdout);
       sleep(1);
       target = ipc_GetMailHandle("testserver");
    }
    printf("Starting client loop...\n");
    for(x=0;x<3;x++)
    {
      printf("Question: %s\n", question.string);
      RPC_READ_SYNC(0, &question, &answer, target);
      printf("Answer: %s\n", answer.string);
    }
}

char commandline[128];
 
/* parameters the ipcinit is looking for 
*/
char sSendtoMailBox[16] = "-r";
char sSleepTime[16] = "-t";
char sPriority[16] = "-p";

char * sUsages[] =
{
    "<# of loops before report>",
    0
};

int 
proc_setup()
{
    while (glob_link () < 0)
    {
      printf ("Where is that darned MMCInit and its shared memory?  ");
      printf ("I think I will sleep till it gets here.\n");
      sleep (1);
    }
    return 1;
}

process_id
fork (char * myName, char * arg1)
{
    MailHandle mh;
    int i;
    process_id pid;
    static char * Argblk[3] = {0};
    
    Argblk[0] = myName;
    Argblk[1] = arg1;
    Argblk[2] = 0;

    if (k9_fork (Argblk, &pid, 5, 0))
    {
        return 0;
    }
    k9_defer ();
    return pid;
}

int
main (int argc, char ** argv)
{
  mainRun(argc, argv);
}

int
mainRun(int argc, char **argv)
{
  char *arg1 = "Stupid phoney value";
  int i;

  if (argc > 1)
  {
    arg1 = argv[1];
  }
  
  for(i=0; i < sizeof(forkmains)/sizeof(mainArgs)-1; i++)
  {
    if (!strcmp(arg1, forkmains[i].arg1))
      break;
  }
  
  forkmains[i].mainfunp(argc, argv);
}

int msTime = 5000;
Priority priority = Pri_AVGSM;

int
clientMain (int argc, char ** argv)
{
    int clientFlag = 1;
    int i = 0;
    char * sMep;
    char * sItp;
    int loop = 10000;

    usage (argc, argv, sUsages, 1);
    if (argc > 1)
    {
      sscanf(argv[1], "%d", &loop);
    }
	
    proc_setup();
    ipc_MailBoxCreate ("testclient", priority, 0,0);
    
    /*******************************************************
    **
    **  Here we fork into the 2 programs we need:
    **    a client watchman (loud)
    **    a server watchman (quiet)
    **
    **  All timing functionality goes into the client watchman.
    **  Therefore, I have made the parent the client one.
    **
    *******************************************************/

    parent_id = getpid();
    child_id = fork("trpc", EXEC_SERVER);
    client(loop);
    _os_send (child_id, SIGKILL);
}

int serverMain (int argc, char **argv)
{
  proc_setup();
  ipc_MailBoxCreate ("testserver", priority, 0,0);
  server();
}
