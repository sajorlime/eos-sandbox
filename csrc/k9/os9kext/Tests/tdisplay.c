
/*
**
** Copyright (C) 1994,  ICTV Inc.
**                      280 Martin Ave.,
**                      Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** File: display.c - stand alone to test the ipc code.
**
** Author: emil
** $Author: emil $
**
** Purpose:
**  To receive test debug messages and put them on the sceen.
**
** Types/Clases:
**  Win -- A window to place data in.
**
** Data:
**  windows -- an array of Win.
**
** Functions:
**  displaySigHandler -- signal handler for this program.
**
**  pscr_winPrint -- places data in windows for later painting by winPaint.
**
**  pscr_winPaint -- is called periodicly to paint the screen.
**
**  pscr_winInit -- Initialize the window data structure.
**
**  display -- accepts display messages.
**
**  main -- the program entry point.  Does intialization and then calls display.
**
** Notes:
**
** $Id: tdisplay.c,v 1.4 1994/08/17 01:00:02 emil Exp $
**
*/

# include <assert.h>
# include <signal.h>
# include <stdio.h>
# include <stdlib.h>
# include <modes.h>
# include <string.h>
# include <process.h>

# include "sighandle.h"
# include "siguser.h"
# include "ictvutil.h"
# define NO_IPC_INLINE
# include "ipc.h"
# include "ipcscreen.h"
# include "ipcuser.h"

# include "globMMC.h"

# define __TDISPLAY_C__

# define JULIAN_TK  3           /* function code for _sysdate() */

/* Alarm intevel 4 seconds */
# define ALARM_INTERVAL (1 * 100)

static int sigrcvd = 0;
static int dispcnt = 0;

void
displaySigHandler (int sig)
{
    if (sigrcvd == SIGQUIT)
        exit (2);
    if (sigrcvd == SIG_ICTV_ALARM)
        exit (2);
    sigrcvd = sig;
    dispcnt++;
}


# define MAXROWS 25
# define WINROWS 6
# define MAXWIN (2 * ((MAXROWS - 1) / WINROWS))
# define WINCOLS 35
# define WINWIDTH (WINCOLS + 5)
# define WBLEN WINWIDTH


static int NextWin = 0; /* this is the next window number */


typedef
struct _Win
{
    int dirty;
    int col;
    int row;
    int minRow;
    int maxRow;
    int lens[WINROWS];
    char bufs[WINROWS][WBLEN];
} Win;

Win windows[MAXWIN] = {0};



process_id procID;
u_int16 pri, age, group, user;
int32 sched;
int
sTestPriority (void)
{
    int r;

    pri = -1;
    r = _os_id (&procID, &pri, &age, &sched, &group, &user);
    if ((u_int32) pri > (u_int32) PRIORITY (Pri_Highest))
    {
        fprintf (stderr, "TD:Priority Error: %x/%x\n", pri, PRIORITY(Pri_Highest));
        exit (1);
    }
    return pri;
}



/* Store strings in the window structure.
*/
void
pscr_winPrint (int win, char * string)
{
    int len;
    int tl;
    int end;
    int row;
    int i;
    Win * winp;

    if ((unsigned) win > MAXWIN)
    {
        fprintf (stderr, "Bad Window: 0x%x\n", win);
        return;
    }
    winp = &windows[win]; /* get the window pointer */
    winp->dirty = 1;
    len = strlen (string); /* the new length */
    while (len > 0)
    {
        row = winp->row - winp->minRow;
        end = winp->lens[row]; /* the end is the last string length */
        tl = len;
        if (tl >= WINCOLS) /* check against max */
            tl = WINCOLS - 1;
        strncpy (winp->bufs[row], string, tl + 1);
        winp->lens[row] = tl; /* save the new length */
        winp->bufs[row][tl] = 0; /* Force a null */
        for (i = tl; i < end; i++) /* fill to the end */
            winp->bufs[row][i] = ' ';
        row = ++row + winp->minRow;
        if (row >= winp->maxRow) /* next row */
            row = winp->minRow + 1; /* wrap to min row */
        winp->row = row;
        len -= tl;
        string += tl;
    }
}



static int pscr_wakes = 0;

/*
** Called periodicly to unpdate the screen.
*/
void
pscr_winPaint (void)
{
    int len;
    int tl;
    int end;
    int row;
    int i;
    int win;
    Win * winp;

    dispcnt = 0;
    for (win = 0; win < MAXWIN; win++)
    {
        winp = &windows[win]; /* get the window pointer */
        if (winp->dirty)
        {
            winp->dirty = 0;
            for (row = 0; row < WINROWS; row++)
            {
                printf
                (
                    "[%d;%dH%s", row + winp->minRow, winp->col, winp->bufs[row]
                );
            }
        }
    }
    printf ("[25;0H%d/%d/%x wakes   ", pscr_wakes++, dispcnt, sTestPriority ());
    fflush (stdout);
}

void
pscr_winDirty (void)
{
    int win;

    for (win = 0; win < MAXWIN; win++)
        windows[win].dirty = 1;
}


/*
** pscr_winInit -- Initializes the window data structures.
*/

void
pscr_winInit (void)
{
    int win;
    int i;
    int j;
    int startRow;
    Win * winp;
    int row = 1;

    for (win = 0, startRow = 1; win < MAXWIN; win++)
    {
        winp = &windows[win];
        winp->row = startRow + 1;
        winp->minRow = startRow;
        winp->maxRow = startRow + WINROWS - 1;
        if (win & 1)
        {
            winp->col = WINWIDTH + 1;
            startRow += WINROWS;
        }
        else
        {
            winp->col = 1;
        }
        for (j = 0; j < WINROWS; j++)
        {
            winp->lens[j] = WINCOLS;
            for (i = 0; i < WBLEN; i++)
            {
                winp->bufs[j][i] = 0;
            }
        }
    }
    printf ("[H[2J");
    fflush (stdout);
}





/*
** display -- waits in an infinite loop for messages, it waits
** for ipc abort, flush messages, and for data  messages.
** It breaks the rules and uses some ipc internals for the purpose
** of testing and debugging.
** Parmeters:
**  sMe -- the ASCIIZ name of
**  priority -- if non-zero used priorty signals and set the task priority
**      to it.
*/
void
display (char * sMe, Priority priority)
{
    int i, err;
    IpcB * ipcp;
    MailHandle me;
    char * cp;
    int win;
    int msgs;
    int sigNormal =  0;
    int sigPriority =  0;
    signal_code sig;

    sigHandlerSet (displaySigHandler);
    me = ipc_MailBoxCreate (sMe, priority, sigNormal, sigPriority);
    if (!me)
    {
        fprintf (stderr, "Failed to Create me:%s", sMe);
        exit (2);
    }
    
    
    for (msgs ; ; msgs++)
    {
        sTestPriority ();
        if (sigrcvd)
        {
            switch (sigrcvd)
            {
            case SIGQUIT:
                if (ipcp)
                    ipc_FreeBuffer (ipcp);
                printf ("DISPLAY:exiting");
                exit (SIGQUIT);
                break;
            case SIGUSR1:
            case SIGUSR2:
                printf ("[2J");
                pscr_winDirty ();
                pscr_winPaint ();
                break;
            case SIG_ICTV_ALARM:
                if (dispcnt > 200)
                {
                    printf ("DISPLAY:No Activity exiting");
                    exit (3);
                }
                pscr_winPaint ();
                break;
            default:
                if (ipcp)
                    ipc_FreeBuffer (ipcp);
                printf ("DISPLAY:exiting for unkown signal %d", sigrcvd);
                exit (sigrcvd);
                break;
            }
            sigrcvd = 0;
        }
        ipcp = ipc_RcvMSDelay (1000);
        if (!ipcp)
        {
            pscr_winPaint ();
            continue;
        }
        else
        {
            switch (ipc_GetUserType (ipcp))
            {
            case IPC_SYS_ABORT:
                printf
                (
                    "DISPLAY:%s rcvd ABORT from %s",
                    sMe, ipc_GetSender (ipcp)->name
                );
                ipc_Flush (0);
                ipc_FreeBuffer (ipcp);
                break;
            case IPC_SYS_FLUSH:
                printf
                (
                    "DISPLAY:%s rcvd FLUSH from %s",
                    sMe, ipc_GetSender(ipcp)->name
                );
                ipc_Flush (ipc_GetSender (ipcp));
                ipc_FreeBuffer (ipcp);
                break;
            case IPC_SYS_DATA:
                cp = ipc_GetDataP (ipcp);
                *cp = NextWin;
                sprintf
                (
                    windows[NextWin].bufs[0], "[7m%s[0m -- %s",
                    ipcp->sender, ((char *) ipc_GetDataP (ipcp)) + 1
                );
                windows[NextWin].dirty = 1;
                if (NextWin++ >= MAXWIN)
                    *cp = NextWin = 0;
                ipc_Reply (ipcp);
                break;
            case IPC_SYS_DISPLAY:
                cp = ipc_GetDataP (ipcp);
                win = *cp++;
                pscr_winPrint (win, cp);
                dispcnt++;
                if (dispcnt > 10)
                {
                    pscr_winPaint ();
                    dispcnt = 0;
                }
                ipc_Reply (ipcp);
                break;
            default:
                printf
                (
                    "%s rcvd unknow msg from %s",
                    sMe, ipc_GetSender (ipcp)->name
                );
                ipc_FreeBuffer (ipcp);
                break;
            }
        }
    }
}



/* parameters the ipcinit is looking for 
*/

char * sUsages[] =
{
    "tdisplay",
    0
};

int
main (int argc, char ** argv)
{
    usage (argc, argv, sUsages, 1);
	while (glob_link () < 0)
	{
		printf ("Sleeping while waiting for MMCInit to run (dummy)\n");
		sleep (1);
	}
    /*alm_cycle (SIG_ICTV_ALARM, ALARM_INTERVAL);*/
    pscr_winInit ();
    display ("Display", Pri_Highest);
    return 0;
}


