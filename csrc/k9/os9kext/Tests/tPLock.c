/*
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
**
** File: ICTV/libsrc/os9kext/Tests/tisrQ.c
**
** Author: emil
**
** Purpose:
**  This file is part of the utility library
**
** Class/Typedefs:
**
** Functions:
**
** Notes:
**
** $Id: tPLock.c,v 1.2 1994/08/18 04:49:03 emil Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/Tests/tPLock.c,v $
**
*/

# include <stdio.h>
# include <string.h>
# include <signal.h>

# include "lbFreeList.h"
# include "k9fork.h"
# include "sighandle.h"
# include "ipc.h"
# include "ipcpriority.h"
# include "globMMC.h"

# define __TPLOCK_C__

int Sig;

void
sigHandler (int sig)
{
	Sig = sig;
}

char *argblk[] = {0, 0, 0};
char Arg2[] = "DONT_USE_THIS_AS_REAL_PARAMETER";

int
main (int argc, char ** argv)
{
	LBList *lblp;
	LBNode *lbnp;
	process_id pid;
	int secs = 5;
	char * sName = "first";

	if (argc < 2)
	{
		printf ("usage: %s arg\n", argv[0]);
		return 0;
	}
	while (glob_link () < 0)
	{
		printf ("Sleeping while waiting for MMCInit to run (dummy)\n");
		sleep (1);
	}
	sigHandlerSet (sigHandler);
	lblp = glob_getTransferPool (1);
	if (strcmp (argv[1], Arg2))
	{
		printf ("%s:%d\n", argv[1], getpid ());
		printf ("Got list lblp:%x -- yo\n", lblp);
		fflush (stdout);
		sleep (1);

		ipc_MailBoxCreate (sName, Pri_VGL, 0, 0);
		lblp = lbList_newList (lblp, 2, 1);
		lbnp = lbList_alloc (lblp);
		printf ("Alloced lbnp:%x -- hi\n", lbnp);
		argblk[0] = argv[0];
		argblk[1] = Arg2;
		argblk[2] = NULL;

		/* set the forked process to max priority until it
		** create its mail blox. */
		if (k9_fork (argblk, &pid, 5, 0xffff))
		{
			printf ("%s: Failed to fork self.\n", argv[0]);
			exit (0);
		}
	}
	else
	{
		sName = "second";
		ipc_MailBoxCreate (sName, Pri_VGL + 1, 0, 0);
		lbnp = lbList_alloc (lblp);
		printf ("%s:%d\n", argv[1], getpid ());
		printf ("Alloced lbnp:%x -- hi\n", lbnp);
		fflush (stdout);
		secs = 2;
	}
	printf ("%s:about to sleep, Sig:%d\n", sName, Sig);
	fflush (stdout);
	sleep (secs);
	printf ("%s:Wake from sleep, Sig:%d\n", sName, Sig);
	fflush (stdout);
	lbList_free (lblp, lbnp);
	printf ("%s:Freed lbnp:%x -- bye\n", sName, lbnp);
	fflush (stdout);

	return 0;
}


