
/*
** Copyright (C) 1994,  ICTV Inc.
**                      280 Martin Ave.,
**                      Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television using OS9K
**
** File: ~dev/ICTV/libsrc/k9fork.c - k9 fork.
**
** Author: brucee & emil
**
** Purpose:
**
** Types/Clases:
**
** External Data:
**	_environ -- the K9 environment!
**
** Functions:
**	k9_fork -- returns the K9 process priority.
**
** Notes:
**
** $Id: k9fork.c,v 1.4 1994/12/07 00:47:44 bruce Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/k9fork.c,v $
**
*/

#include <types.h>
#include <stdio.h>
#include <modes.h>
#include <io.h>

# define __K9FORK_C__
/*
** externals
*/
extern char **_environ;

/*
** forker() - forks a process given an argument block returns the pid of the child process at
**            the address pointed to by childId. The child will inherit the standard 3 IO paths
*/

error_code
k9_orphanFork(char **args, process_id *childId, int paths, u_int16 priority, int orphanp)
{
	error_code error = 0;
	u_int32 edata = 0;
	u_int16 typelang = 0;
	char orphan = orphanp;

	typelang = mktypelang (MT_PROGRAM, ML_OBJECT);

	error = _os_exec (_os_fork, priority, paths, args[0], args, _environ,
			  edata, childId, typelang, orphan);

	return error;
}

error_code k9_fork(char **args, process_id *childId, int paths, u_int16 priority)
{
  return k9_orphanFork(args,childId,paths,priority,0) ;
}

