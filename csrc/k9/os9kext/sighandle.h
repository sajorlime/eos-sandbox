
/*
** COPYRIGHT (C) 1994, ICTV Inc. 280 Martin Ave. Santa Clara, Calif 95050. 280
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television
**
** File: sighandle.h - local sighanler interface.
**
** Purpose:
**  To provide an interface that allows a system task to use ipc and other
**  ictv interfaces and still received signal.
**
** Types/Clases:
**  SignalHandler -- type defining a signal handler.
**
** Functions:
**  see sighandler.h of defintitions below.
**
** Notes:
**  In order to allow the ipc system to get alarm signals (this may be extended
**  to other ictv interfaces) and still allow tasks to receive other signals
**  if need be this interface must be used to handle signal.
**
** Usage:
**
** $Log: sighandle.h,v $
** Revision 1.2  1994/08/15 14:59:48  emil
** A bunch of changes to use the memlib facilty.  Plus I removed tabs from all.
**
 * Revision 1.1.1.1  1994/07/08  23:50:36  emil
 * OS9KExt creation, should include ipc, sighandle, Lock, and other 9K
 * extensions.
 *
 * Revision 1.1.1.1  1994/03/25  19:19:59  emil
 * Creating this.
 *
**
*/

# ifndef __SIGHANDLE_H__
# define __SIGHANDLE_H__

# include <signal.h>

typedef void (* SignalHandler) (int);

/* Public sighandle interface */

/* sigHandlerSet set the current OS signal intercept function.
** it returns the current (i.e. last set by this call) signal handler.
** If the task wishes to be compatable with other ICTV libraries, it
** use this call to set its signal handler. 
** Passing Null into this procedure disables signal handling.
*/
SignalHandler sigHandlerSet (SignalHandler handler);

# endif

