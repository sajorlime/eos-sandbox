
/*
** Copyright (C) 1994,  ICTV Inc.
**                      280 Martin Ave.,
**                      Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv -- multimedia controller.
**
** File: k9Module.h -- 
**
** Author: emil
**
** Purpose:
**  This module, provides a simplified interface to 9K module
**  creation and linking.
**
** Types/Clases:
**
** Notes:
**
** $Id: k9Module.h,v 1.5 1994/08/18 22:23:45 emil Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/k9Module.h,v $
**
*/

# ifndef __K9MODULE_H__
# define __K9MODULE_H__

# include "ictvcommon.h"

void * k9_moduleLink (char * modp);

void * k9_moduleCreate (char * moduleNamep, int32 spaceNeeded);

# endif

