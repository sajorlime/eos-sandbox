/*
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: Utility Library
**
** File: .../libsrc/utility/isrQueue.c
**
** Author: emil
** $Author: wul $
**
** Purpose:
**  This module provides a class that supports communication between 
**  one interrut and one task in such a way the interrupts need not be shut
**  off in the task. In general these queues can be used for communication
**  between one producer and one consumer operating in a preemptive mode
**  on a CISC architecture.  These queues can (and probably will) be
**  used with either multiple consumer and multiple producers if the
**  set of producer or the set of consumers are able to guarantee
**  mutual exclusion among the set.  This is for instance the case
**  when there are multiple interrupt handlers talking to a single
**  queue.
**
** Class/Typedefs:
**  see isrQueue.h
**
** Data:
**  ZeroElem -- returned value from get when queue is empty.
**
** Functions:
**  isrq_Create -- creates an ISR Queue.
**  isrq_Count -- return count of items in que, this is also inline in header.
**  isrq_CreateDefault -- creates a default maxElements queue from
**  the free store.
**  isrq_Put -- puts a QElem in the queue.
**  isrq_Get -- gets the next QElem from the queue.
**
** Static Functions:
**
** Notes:
**
** $Id: isrQueue.c,v 1.15 1995/06/25 22:28:29 wul Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/isrQueue.c,v $
**
*/

# define __ISRQUEUE_C__

# include "sighandle.h"
# include "siguser.h"
# include "MemHeap.h"


# define NO_ISRQUEUE_INLINE
# include "isrQueue.h"

# ifdef _OS9000
        /* This gets rid of a warning */
#       define SIGWAKEIT SIG_ISRQ_OVERFLOW
# else
        /* This gets rid of an error */
#       define SIGWAKEIT SIGUSR1
# endif

/* ZeroElem is used for returning a value when the queue is empty. */
static QElem ZeroElem = {0, 0} ;


/* Create either allocates data from the free store, or it can be passed.
** in particular a previously allocated queue, can be passed to be
** initialized.
*/
ISRQ *
isrq_Create (ISRQ * qp, int numElements, u_int32 pid)
{
    /* A zero in the size make the queue a default size */
    if (numElements <= 0)
        numElements = ISRQUEUE_DEFAULT_ELEMENTS;
    if (!qp)  /* Allocate from free store if Null */
    {
        qp =
            (ISRQ *) mem_malloc
                    (
                        MEDIA_HEAP_ID,
                        numElements * sizeof (QElem) + sizeof (ISRQ)
                    );
        if (!qp)
            return 0;
        qp->datap = (QElem *) (qp + 1);
    }
    else
    {
        if (!qp->datap) /* Allocate from free store if Null */
        {
            qp->datap =
                (QElem *) mem_malloc (MEDIA_HEAP_ID, numElements * sizeof (QElem));
            if (!qp->datap)
                return 0;
        }
    }
    /* Now we have the data so setup the queue.  Set the get and put
    ** pointers equal and set the count to zero. */
    qp->getp = qp->datap;
    qp->putp = qp->datap;
    qp->endp = qp->datap + numElements; /* points past last member */
    qp->pid = pid ? pid : getpid ();    /* OS Process ID */
    qp->count = 0;
    QELEM_SET_VP (qp->extra, NULL);
    QELEM_SET_VP2 (qp->extra, NULL);
    qp->maxElements = numElements;

    return qp;
}

void 
isrq_Reset (ISRQ * qp, u_int32 pid)
{
    if (qp == NULL)
        return;
    qp->getp = qp->datap;
    qp->putp = qp->datap;
    qp->pid = pid;
    qp->count = 0;
    QELEM_SET_VP (qp->extra, NULL);
    QELEM_SET_VP2 (qp->extra, NULL);
}

/*
** CreateDefault acts like Create (0, 0, 0)
*/
ISRQ *
isrq_CreateDefault (void)
{
    return isrq_Create (0, 0, 0);
}

int
isrq_Count (ISRQ * qp)
{
    return qp->count;
}

int
isrq_MaxElements (ISRQ * qp)
{
    return qp->maxElements;
}

int
isrq_Available (ISRQ * qp)
{
    return qp->maxElements - qp->count;
}



/*
** Note that count is used as a semephore that allows the getter into
** the current block. It must not be modified until the block has
** been updated.  When it is modified it is done indivisibly */
int
isrq_Put (ISRQ * qp, QElem qe)
{
    QElem * qep;

    /* When there has been an overflow, we store away the overflow
    ** element in a special slot and signal the task.
    ** Note that any additional overflows  will overwrite the same
    ** same overflow slot.*/    
    if (!qp)
        return -1;
    if (qp->count >= qp->maxElements)
    {
        qp->extra = qe;
# ifdef _OS9000
        if (qp->pid)
            _os_send (qp->pid, SIGWAKEIT);
# else
        if (qp->pid)
            kill (qp->pid, SIGWAKEIT);
# endif
        return 1;
    }
    qep = qp->putp;
    *qep++ = qe;
    if (qep >= qp->endp)
        qep = qp->datap;
    qp->putp = qep;
    qp->count++; /* increment count as an indivisible action */
    return 0;
}

int
isrq_PutAndWake (ISRQ * qp, QElem qe)
{
    int value;

    value = isrq_Put (qp, qe);
    if (!value)
        _os_send (qp->pid, SIGWAKE);
    return value;
}

/* */
QElem
isrq_Get (ISRQ * qp)
{
    QElem qe;
    QElem * qep;

    /* We need to exit if count is zero, return empty value */
    if (!qp || !qp->count)
        return ZeroElem;

    qep = qp->getp;
    qe = *qep++;
    if (qep == qp->endp)
    {
        if (qep > qp->endp)
        {
            GLOBAL_MSG (("BAD qep pointer\n"), DBG_ASSERT);
        }
        qep = qp->datap;
    }
    qp->getp = qep;
    qp->count--;    /* decrement count as an indivisible action */
    return qe;
}

QElem
isrq_Peek (ISRQ * qp)
{
    QElem qe;
    QElem *qep;

    /* We need to exit if count is zero, return empty value */
    if (!qp || !qp->count)
        return ZeroElem;
    else
        return *(qp->getp);
}

QElem
isrq_GetOverflow (ISRQ * qp)
{
    QElem qe;
    QElem *qep;

    if (!qp)
        return ZeroElem;
    qe = qp->extra;
    qp->extra = ZeroElem;
    return qe;
}


int
isrq_setPID (ISRQ * qp, int pid)
{
    return qp->pid = pid;
}


