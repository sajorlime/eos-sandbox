
/*
** Copyright (C) 1994,  ICTV Inc.
**                      280 Martin Ave.,
**                      Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television multimedia controller.
**
** File: k9Module.c
**
** Author: emil
**
** Purpose:
**  This module, provides a simplified interface to 9K module
**  creation and linking.
**
** Types/Clases:
**
** Functions:
**  k9_moduleCreate -- 
**  k9_moduleLink -- 
**
** Notes:
**
** $Id: k9Module.c,v 1.9 1994/12/13 22:17:10 bruce Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/k9Module.c,v $
**
*/

# include <module.h>
# include <errno.h>

# include "k9Module.h"

# define __K9MODULE_C__


void *
k9_moduleLink (char * modp)
{
	u_int16 attr_rev;
	u_int16 type_lang;
	void *mp;
	mh_data *mhdp;
	error_code ec;

	attr_rev = 0;
	type_lang = 0;	/* any type */

	ec = _os_link (&modp, &mhdp, &mp, &type_lang, &attr_rev);
	if (ec)
	{
		LOCAL_MSG (("Failed to link to %s\n", modp), DBG_K9EXT_MASKINDEX, DBG_ASSERT);
		return (void *) 0;
	}
	return mp;
}


void *
k9_moduleCreate (char * moduleNamep, int32 spaceNeeded)
{
	u_int16 attr_rev;
	u_int16 type_lang;
	mh_data *mhdp;
	void *vp;
	error_code ec;

	/* Calculate the space needed, we allocate a linear table with *
	   maximum number of interrutps we can possibly handle.  We *
	   will mark both the ones we do handle and the ones we do not *
	   handle in this table. */

	/* Magic 9K stuff */
	attr_rev = MA_REENT << 8;	/* reentrant module */
	type_lang = MT_DATA << 8;	/* data module */

	/* create the data module */
	ec = _os_datmod
		(
			moduleNamep, spaceNeeded, &attr_rev, &type_lang,
			MP_OWNER_READ + MP_OWNER_WRITE + MP_WORLD_READ + MP_WORLD_WRITE,
			&vp, &mhdp
		);
	switch (ec)
	{
	case EOS_NORAM:
		LOCAL_MSG
		(
			(
				"Not enough RAM to create module. Requested space: 0x%08x\n",
				spaceNeeded
			),
			DBG_K9EXT_MASKINDEX, DBG_ASSERT
		);
		return 0;
	case EOS_KWNMOD:
		LOCAL_MSG
		(
			("Data module not created (it probably already exists)\n"),
			DBG_K9EXT_MASKINDEX, DBG_WARN
		);
		return (void *) 0;
	default:
		break;
	}
	return vp;
}

