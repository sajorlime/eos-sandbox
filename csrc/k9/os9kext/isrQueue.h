/*
**
** Project: Utility Library
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** File: .../libsrc/utility/isrQueue.h
**
** Author: emil
**
** Purpose:
**  This module provides a class that supports communication between one 
**	interrupt service routine (ISR) and one task in such a
**	way that interrupts need not be shut
**	off the in task. It should suffice as a communication mechanism
**	between any two producer/consumer pairs operating at different
**	(i.e. preemptive) priorities.  The code does assume a CISC
**	single indivisible instruction increment of memory.
**	These queues can (and probably will) be
**	used with either multiple consumer and multiple producers if the
**	set of producer or the set of consumers are able to guarantee
**	mutual exclusion among the set.  This is for instance the case
**	when there are multiple interrupt handlers talking to a single
**	queue.
**
**
** Constants:
**	ISRQUEUE_EMPTY_FLAG -- 0 -- if queue data does not contain this value it can
**	be used to indicate that the queue is empty, it is currently zero.
**
**	ISRQUEUE_DEFAULT_ELEMENTS default max number of elements in a queue.
**
** Class/Typedefs:
**	QElem -- this is the queue element structure.
**	ISRQ -- this is the queue structure.
**
** Inline Functions:
**	int isrq_Empty (ISRQ * qp) --
**	int isrq_MaxElements (ISRQ *qp) --
**
** Notes:
**
**	These routines assume a common address space.  To make them work
**	in seperate address with mapped shared memory, make the
**	pointers indexes.
**
** $Id: isrQueue.h,v 1.12 1994/12/01 12:17:48 emil Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/isrQueue.h,v $
**
** History:
**
** Modified 8/6 and 8/7 by BDN.
**
** Added pid to create function arglist. Defined a number of new macros and functions
** and modified the behavior of isrq_get. In particular get never looks at the overflow
** element, an extra call isrq_GetOverflow was defined. In this way the isrq does not
** care in any way about the value of the tag field. If an overflow is generated then
** the process that receives the signal is responsible for reading the overflow value if
** it wants to.
*/

# ifndef __ISRQUEUE_H__
# define __ISRQUEUE_H__

# include "ictvcommon.h"

/* No meaning is enforced by the queue mechanisms for any of
** these fields. There are two 32 bit fields available for use.
** The structures and macros below are intended to give consistant
** access ** to the 64 bits of the queue elements.  The base
** structure is the Token32 which allows one to access a 32 bit
** field almost any combitation of bytes, shorts, or long values.
** For conpleteness I have created access methods that we don't
** currently use.  Please don't let these confuse you.  The
** final goal is to use the QELM accessor without reference
** to either Token32s or Token64 outside of this header.
*/

typedef union _Token32
{
	void * vp;
	u_int32 ui;
	u_char uc4[4];
	u_int16 us2[2];
	struct
	{
		u_char tag;
		u_int8 uc;
		u_int16 us;
	} sv;
} Token32;

/* For each field there are two four accessors,
** for both getting and setting the the field
** there is one each for a pointer and a the structure. */
/* View 1: These accessors provide a view of the field as a
** 32 bit value. */
/* get the field as a void pointer. */
# define TOKEN32_FIELD_GET_VP(tf) (tf.vp)
# define TOKEN32_FIELDP_GET_VP(tfp) (tfp->vp)

/* get the field as an unsigned integer  */
# define TOKEN32_FIELD_GET_UI(tf) (tf.ui)
# define TOKEN32_FIELDP_GET_UI(tfp) (tfp->ui)

/* set the field as a void pointer. */
# define TOKEN32_FIELD_SET_VP(tf, Vp) (tf.vp = Vp)
# define TOKEN32_FIELDP_SET_VP(tfp, Vp) (tfp->vp = Vp)

/* set the field as an unsigned integer  */
# define TOKEN32_FIELD_SET_UI(tf, Ui) (tf.ui = Ui)
# define TOKEN32_FIELDP_SET_UI(tfp, Ui) (tfp->ui = Ui)

/* View 2: this view of the Token32 is as three indepenant fields.
** Two of these are are 8-bit and one 16-bit.  The next
** three accesors gives this independent access to these
** fields. */
/* Get the "tag" field of the 32 field. */
# define TOKEN32_FIELD_GET_TAG(tf) (tf.sv.tag)
# define TOKEN32_FIELDP_GET_TAG(tfp) (tfp->sv.tag)

/* Set the "tag" field of the 32 field. */
# define TOKEN32_FIELD_SET_TAG(tf, Tag) (tf.sv.tag = Tag)
# define TOKEN32_FIELDP_SET_TAG(tfp, Tag) (tfp->sv.tag = Tag)

/* Get the "uc" unsigned char field of the 32 field. */
# define TOKEN32_FIELD_GET_UC(tf) (tf.sv.uc)
# define TOKEN32_FIELDP_GET_UC(tfp) (tfp->sv.uc)

/* Set the "uc" unsigned char field of the 32 field. */
# define TOKEN32_FIELD_SET_UC(tf, Uc) (tf.sv.uc = Uc)
# define TOKEN32_FIELDP_SET_UC(tfp, Uc) (tfp->sv.uc = Uc)

/* Get the "us" unsigned short field of the 32 field. */
# define TOKEN32_FIELD_GET_US(tf) (tf.sv.us)
# define TOKEN32_FIELDP_GET_US(tfp) (tfp->sv.us)

/* Set the "us" unsigned short field of the 32 field. */
# define TOKEN32_FIELD_SET_US(tf, Us) (tf.sv.us = Us)
# define TOKEN32_FIELDP_SET_US(tfp, Us) (tfp->sv.us = Us)

/* View 3: this view is of two unsigned (16-bit) shorts */
/* get each of the values */ 
# define TOKEN32_FIELD_GET_US0(tf) (tf.us2[0])
# define TOKEN32_FIELD_GET_US1(tf) (tf.us2[1])
# define TOKEN32_FIELDP_GET_US0(tfp) (tfp->us2[0])
# define TOKEN32_FIELDP_GET_US1(tfp) (tfp->us2[1])
/* set each of the values */ 
# define TOKEN32_FIELD_SET_US0(tf, Us) (tf.us2[0] = Us)
# define TOKEN32_FIELD_SET_US1(tf, Us) (tf.us2[1] = Us)
# define TOKEN32_FIELDP_SET_US0(tfp, Us) (tfp->us2[0] = Us)
# define TOKEN32_FIELDP_SET_US1(tfp, Us) (tfp->us2[1] = Us)

/* View 4: this view is of four unsigned (8-bit) chars */
/* get each of the values */ 
# define TOKEN32_FIELD_GET_UC0(tf) (tf.uc2[1])
# define TOKEN32_FIELD_GET_UC1(tf) (tf.uc2[1])
# define TOKEN32_FIELD_GET_UC2(tf) (tf.uc2[1])
# define TOKEN32_FIELD_GET_UC3(tf) (tf.uc2[1])
# define TOKEN32_FIELDP_GET_UC0(tfp) (tfp->uc2[1])
# define TOKEN32_FIELDP_GET_UC1(tfp) (tfp->uc2[1])
# define TOKEN32_FIELDP_GET_UC2(tfp) (tfp->uc2[1])
# define TOKEN32_FIELDP_GET_UC3(tfp) (tfp->uc2[1])
/* get each of the values */ 
# define TOKEN32_FIELD_SET_UC0(tf, Uc) (tf.uc2[1] = Uc)
# define TOKEN32_FIELD_SET_UC1(tf, Uc) (tf.uc2[1] = Uc)
# define TOKEN32_FIELD_SET_UC2(tf, Uc) (tf.uc2[1] = Uc)
# define TOKEN32_FIELD_SET_UC3(tf, Uc) (tf.uc2[1] = Uc)
# define TOKEN32_FIELDP_SET_UC0(tfp, Uc) (tfp->uc2[1] = Uc)
# define TOKEN32_FIELDP_SET_UC1(tfp, Uc) (tfp->uc2[1] = Uc)
# define TOKEN32_FIELDP_SET_UC2(tfp, Uc) (tfp->uc2[1] = Uc)
# define TOKEN32_FIELDP_SET_UC3(tfp, Uc) (tfp->uc2[1] = Uc)

/* The Token64 structure simply combines two Token32s */
typedef struct _Token64
{
	Token32 field1;
	Token32 field2;
} Token64;
typedef Token64 Tag56Bit;
typedef Token64 QElem;
typedef Token64 ISRQElem;
 

/* The Token64 takes the view that there should be only one tag
** field (in the first field), and the default 32bit values should
** be in the second field.  Therefore when two 32-bit fields are
** need the second field replaces the tag word. */

# define TOKEN64_GET_VP(token64) TOKEN32_FIELD_GET_VP (token64.field2)
# define TOKENP64_GET_VP(token64p) TOKEN32_FIELDP_GET_VP(token64p->field2)
# define TOKEN64_SET_VP(token64, vp) TOKEN32_FIELD_SET_VP (token64.field2, vp)

# define TOKENP64_SET_VP(token64p, vp) TOKEN32_FIELDP_SET_VP(token64p->field1, vp)
# define TOKEN64_GET_VP2(token64) TOKEN32_FIELD_GET_VP (token64.field1)
# define TOKENP64_GET_VP2(token64p) TOKEN32_FIELDP_GET_VP (token64p->field1)
# define TOKEN64_SET_VP2(token64, vp) TOKEN32_FIELD_SET_VP (token64.field1, vp)
# define TOKENP64_SET_VP2(token64p, vp) TOKEN32_FIELDP_SET_VP (token64p->field1, vp)

# define TOKEN64_GET_UI(token64) TOKEN32_FIELD_GET_UI (token64.field1)
# define TOKENP64_GET_UI(token64p) TOKEN32_FIELDP_GET_UI (token64p->field1)
# define TOKEN64_GET_UI2(token64) TOKEN32_FIELD_GET_UI (token64.field2)
# define TOKENP64_GET_UI2(token64p) TOKEN32_FIELDP_GET_UI (token64p->field2)
# define TOKEN64_SET_UI(token64, ui) TOKEN32_FIELD_SET_UI (token64.field1, ui)
# define TOKENP64_SET_UI(token64p, ui) TOKEN32_FIELDP_SET_UI (token64p->field1, ui)
# define TOKEN64_SET_UI2(token64, ui) TOKEN32_FIELD_SET_UI (token64.field2, ui)
# define TOKENP64_SET_UI2(token64p, ui) TOKEN32_FIELDP_SET_UI (token64p->field2, ui)

# define TOKEN64_GET_TAG(token64) TOKEN32_FIELD_GET_TAG (token64.field1)
# define TOKENP64_GET_TAG(token64p) TOKEN32_FIELDP_GET_TAG (token64p->field1)
# define TOKEN64_SET_TAG(token64, tag) TOKEN32_FIELD_SET_TAG (token64.field1, tag)
# define TOKENP64_SET_TAG(token64p, tag) TOKEN32_FIELDP_SET_TAG (token64p->field1, tag)

# define TOKEN64_GET_UC(token64) TOKEN32_FIELD_GET_UC (token64.field1)
# define TOKENP64_GET_UC(token64p) TOKEN32_FIELDP_GET_UC (token64p->field1)
# define TOKEN64_SET_UC(token64, uc) TOKEN32_FIELD_SET_UC (token64.field1, uc)
# define TOKENP64_SET_UC(token64p, uc) TOKEN32_FIELDP_SET_UC (token64p->field1, uc)

# define TOKEN64_GET_US(token64) TOKEN32_FIELD_GET_US (token64.field1)
# define TOKENP64_GET_US(token64p) TOKEN32_FIELDP_GET_US (token64p->field1)
# define TOKEN64_SET_US(token64, us) TOKEN32_FIELD_SET_US (token64.field1, us)
# define TOKENP64_SET_US(token64p, us) TOKEN32_FIELDP_SET_US (token64p->field1, us)

# define TOKEN64_GET_US0(token64) TOKEN32_FIELD_GET_US0 (token64.field2)
# define TOKENP64_GET_US0(token64p) TOKEN32_FIELDP_GET_US0 (token64p->field2)
# define TOKEN64_SET_US0(token64, Us) TOKEN32_FIELD_SET_US0 (token64.field2, Us)
# define TOKENP64_SET_US0(token64p, Us) TOKEN32_FIELDP_SET_US0 (token64p->field2, Us)

# define TOKEN64_GET_US1(token64) TOKEN32_FIELD_GET_US1 (token64.field2)
# define TOKENP64_GET_US1(token64p) TOKEN32_FIELDP_GET_US1 (token64p->field2)
# define TOKEN64_SET_US1(token64, Us) TOKEN32_FIELD_SET_US1 (token64.field2, Us)
# define TOKENP64_SET_US1(token64p, Us) TOKEN32_FIELDP_SET_US1 (token64p->field2, Us)

# define TOKEN64_GET_UC0(token64) TOKEN32_FIELD_GET_UC0 (token64.field2)
# define TOKENP64_GET_UC0(token64p) TOKEN32_FIELDP_GET_UC0 (token64p->field2)
# define TOKEN64_SET_UC0(token64, Uc) TOKEN32_FIELD_SET_UC0 (token64.field2, Uc)
# define TOKENP64_SET_UC0(token64p, Uc) TOKEN32_FIELDP_SET_UC0 (token64p->field2, Uc)

# define TOKEN64_GET_UC1(token64) TOKEN32_FIELD_GET_UC1 (token64.field2)
# define TOKENP64_GET_UC1(token64p) TOKEN32_FIELDP_GET_UC1 (token64p->field2)
# define TOKEN64_SET_UC1(token64, Uc) TOKEN32_FIELD_SET_UC1 (token64.field2, Uc)
# define TOKENP64_SET_UC1(token64p, Uc) TOKEN32_FIELDP_SET_UC1 (token64p->field2, Uc)

# define TOKEN64_GET_UC2(token64) TOKEN32_FIELD_GET_UC2 (token64.field2)
# define TOKENP64_GET_US2(token64p) TOKEN32_FIELDP_GET_UC2 (token64p->field2)
# define TOKEN64_SET_UC2(token64, Uc) TOKEN32_FIELD_SET_UC2 (token64.field2, Uc)
# define TOKENP64_SET_UC2(token64p, Uc) TOKEN32_FIELDP_SET_UC2 (token64p->field2, Uc)

# define TOKEN64_GET_UC3(token64) TOKEN32_FIELD_GET_UC3 (token64.field2)
# define TOKENP64_GET_US3(token64p) TOKEN32_FIELDP_GET_UC3 (token64p->field2)
# define TOKEN64_SET_UC3(token64, Uc) TOKEN32_FIELD_SET_UC3 (token64.field2, Uc)
# define TOKENP64_SET_UC3(token64p, Uc) TOKEN32_FIELDP_SET_UC3 (token64p->field2, Uc)


/* An other name for the default 32-bit pointer value is the "VISIBLE"
** VP.  The second is call the "OPAQUE" field.  This reflects a particular
** intended usage. */
# define TOKEN64_GET_VISIBLE_VP TOKEN64_GET_VP
# define TOKENP64_GET_VISIBLE_VP TOKENP64_GET_VP
# define TOKEN64_SET_VISIBLE_VP TOKEN64_SET_VP
# define TOKENP64_SET_VISIBLE_VP TOKENP64_SET_VP

# define TOKEN64_GET_OPAQUE_VP TOKEN64_GET_VP2
# define TOKENP64_GET_OPAQUE_VP TOKENP64_GET_VP2
# define TOKEN64_SET_OPAQUE_VP TOKEN64_SET_VP2
# define TOKENP64_SET_OPAQUE_VP TOKENP64_SET_VP2
    


/* The QLELM version of Token64 is the version that should be used
** when accessing QElem strutures.  The intension of these is to
** provide the following usage models:
**	A)	tag -- 8 bit
		uc  -- 8 bit
		us  -- 16 bit
		ui/vp/us[2]/uc[4] -- 32 bit.
	where the tag field is meant to identify the interrupt source,
	the uc or us field is meant to provide secondary information
	(usually sub-type information, sometimes it conveys data),
	the ui/vp/us[2]/uc[4] field is meant to convey data form the ISR.
	The usage can be used for output or input but is primerily menat
	for output from the ISR.

	B)	ui/vp -- 32bit
		ui/vp -- 32bit.
	This is an input format for a sepcial purpose consummer.
*/
/* To support view A 32-bit values default to the second field.
** To access a second 32-bit field use the V2, UI or OPAQUE
** names. */
/* Views A and B pointer */
# define QELEM_GET_VP TOKEN64_GET_VP
# define QELEM_PGET_VP TOKENP64_GET_VP

# define QELEM_SET_VP TOKEN64_SET_VP
# define QELEMP_SET_VP TOKENP64_SET_VP

/* View B second pointer */
# define QELEM_GET_VP2 TOKEN64_GET_VP2
# define QELEM_PGET_VP2 TOKENP64_GET_VP2

# define QELEM_SET_VP2 TOKEN64_SET_VP2
# define QELEMP_SET_VP2 TOKENP64_SET_VP2

/* Views A and B 32-bit unsigned  */
# define QELEM_GET_UI TOKEN64_GET_UI2
# define QELEM_PGET_UI TOKENP64_GET_UI2

# define QELEM_SET_UI TOKEN64_SET_UI2
# define QELEM_PSET_UI TOKENP64_SET_UI2

/* View B second 32-bit unsigned */
# define QELEM_GET_UI2 TOKEN64_GET_UI
# define QELEM_PGET_UI2 TOKENP64_GET_UI

# define QELEM_SET_UI2 TOKEN64_SET_UI
# define QELEM_PSET_UI2 TOKENP64_SET_UI

/* View A 8-bit Tag */
# define QELEM_GET_TAG TOKEN64_GET_TAG
# define QELEM_PGET_TAG TOKENP64_GET_TAG

# define QELEM_SET_TAG TOKEN64_SET_TAG
# define QELEMP_SET_TAG TOKENP64_SET_TAG

/* View A 8-bit unsigned sub-type of data */
# define QELEM_GET_UC TOKEN64_GET_UC
# define QELEM_PGET_UC TOKENP64_GET_UC

# define QELEM_SET_UC TOKEN64_SET_UC
# define QELEMP_SET_UC TOKENP64_SET_UC

/* View A 16-bit unsigned sub-type of data */
# define QELEM_GET_US TOKEN64_GET_US
# define QELEMP_GET_US TOKENP64_GET_US

# define QELEM_SET_US TOKEN64_SET_US
# define QELEMP_SET_US TOKENP64_SET_US

/* View A 32-bit pointer, view B first 32-bit pointer */
# define QELEM_GET_VISIBLE_VP TOKEN64_GET_VISIBLE_VP
# define QELEM_PGET_VISIBLE_VP TOKENP64_GET_VISIBLE_VP

# define QELEM_SET_VISIBLE_VP TOKEN64_SET_VISIBLE_VP
# define QELEMP_SET_VISIBLE_VP TOKENP64_SET_VISIBLE_VP

/* View B second 32-bit pointer */
# define QELEM_GET_OPAQUE_VP TOKEN64_GET_OPAQUE_VP
# define QELEM_PGET_OPAQUE_VP TOKENP64_GET_OPAQUE_VP

# define QELEM_SET_OPAQUE_VP TOKEN64_SET_OPAQUE_VP
# define QELEMP_GET_OPAQUE_VP TOKENP64_GET_OPAQUE_VP


# define QELEM64_GET_US0 TOKEN64_GET_US0
# define QELEMP64_GET_US0 TOKENP64_GET_US0
# define QELEM64_SET_US0 TOKEN64_SET_US0
# define QELEMP64_SET_US0 TOKENP64_SET_US0

# define QELEM64_GET_US1 TOKEN64_GET_US1
# define QELEMP64_GET_US1 TOKENP64_GET_US1
# define QELEM64_SET_US1 TOKEN64_SET_US1
# define QELEMP64_SET_US1 TOKENP64_SET_US1

# define QELEM64_GET_UC0 TOKEN64_GET_UC0
# define QELEMP64_GET_US0 TOKENP64_GET_US0
# define QELEM64_SET_UC0 TOKEN64_SET_UC0
# define QELEMP64_SET_UC0 TOKENP64_SET_UC0

# define QELEM64_GET_UC1 TOKEN64_GET_UC1
# define QELEMP64_GET_US1 TOKENP64_GET_US1
# define QELEM64_SET_UC1 TOKEN64_SET_UC1
# define QELEMP64_SET_UC1 TOKENP64_SET_UC1

# define QELEM64_GET_UC2 TOKEN64_GET_UC2
# define QELEMP64_GET_US2 TOKENP64_GET_US2
# define QELEM64_SET_UC2 TOKEN64_SET_UC2
# define QELEMP64_SET_UC2 TOKENP64_SET_UC2

# define QELEM64_GET_UC3 TOKEN64_GET_UC3
# define QELEMP64_GET_US3 TOKENP64_GET_US3
# define QELEM64_SET_UC3 TOKEN64_SET_UC3
# define QELEMP64_SET_UC3 TOKENP64_SET_UC3



/* If the returned tag value is ISRQUEUE_EMPTY_TAG the queue is empty */
#	define ISRQUEUE_EMPTY_TAG 0
/* This is the default number of elements, if numElements request is zero */
/* The command FIFO in the CL450 can hold 126 low priority commands to use
 * upto 116 of these slots for NEW_PACKET type commands. */
# define ISRQUEUE_DEFAULT_ELEMENTS 128


typedef struct _ISRQ
{
/* private: treat all data items here as private */
	int pid; /* pid of task to be signaled if there is an overflow error */
	int maxElements; /* maximum number of elements in queue */
	int count; /* count of elements in queue */
	QElem extra; /* location of first overflow word */
	QElem * putp; /* pointer used for placing data in queue */
	QElem * getp; /* pointer used for getting data out of queue */
	QElem * endp; /* pointer used to mark end of queue */
	QElem * datap; /* pointer to data array for queue */
} ISRQueue;
typedef ISRQueue ISRQ;

#define ISRQ_RESET(isrq) ((isrq)->datap = NULL)

#define ISRQ_DATA_SIZE(isrq) ((isrq)->maxElements * sizeof(QElem))

/*
** isrq1_Create -- this is used to or initialize an ISR Queue.
** Parameters:
**	qp -- is a pointer to an ISRQ, or Null.  If the pointer is non-null the
**	space is initialized as a queue header. If a queue header is
**	passed then the assumption is that the
**	space pointed to by data is already correct, unless it is Null in
**	which case the data is allocated from the free store.  This
**	mechanism can be used reinitalizing the queue, or reducing its
**	effecitive size.
**
**	numElements -- is the numElements of the new queue, i.e. the
**	number of elements that
**	can be contained by a full queue.  If numElements is <= zero a queue of
**	ISRQUEUE_DEFAULT_ELEMENTS is created.
**
**	pid - is the process to signal if the queue overflows. If zero it uses
**	the pid that created the queue.
**
** Returns:
**	Pointer to an ISRQ of the indicated numElements. It returns Null
**	if it failed to allocate the data.
**
** Usage:
**	ISRQ myque = {0};
**	ISRQ * qp = {0};
**
**	qp = isrq_Create (&myque, 37,0) -- allocate data but keep queue local 
**	if (qp != &myque)
**		error ...
**	
**	or
**
**	qp = isrq_Create (0, 47,0) -- allocate everything, give 47 queue elements. 
**	if (!qp)
**		error ...
**	
**	or
**
**	qp = isrq_Create (0, 0,0) -- allocate everything, use default numElements 
**	This last call is equivalent to isrq_CreateDefault ().
*/
ISRQ * isrq_Create (ISRQ * qp, int maxElements, u_int32 pid);

/*
** Equivalent of isrq_Create (0, 0, 0);
*/
ISRQ * isrq_CreateDefault (void);

/* Reset an isrq's instance variables to empty. */
void isrq_Reset(ISRQ *qp, u_int32 pid) ;

/* Functions defined below can be in-line, or are available by a call, by
** defining either NO_INLINE, or NO_ISRQUEUE_INLINE. NO_INLINE disables
** all in-lining, while NO_ISRQUEUE_INLINE only disables in-lining of
** ISR Queue functions.
*/
# ifndef NO_INLINE
#	ifndef NO_ISRQUEUE_INLINE
#		define isrq_Count(qp)((qp)->count)
#		define isrq_MaxElements(qp) ((qp)->maxElements)
#		define isrq_Available(qp) ((qp)->maxElements - (qp)->count)
#	endif
# endif


/* isrq_Count -- returns the current queue count.
**
** Usage:
**	if (isrq_Count (qp))
**		qe = isrq_Get (qp);
*/
# ifndef isrq_Count
int isrq_Count (ISRQ * qp);
# endif

/* isrq_MaxElements -- returns the maximum elements of the queue.
**
** Usage:
**	cnt = isrq_MaxElements (qp) - isrq_Count (qp);
**	while (cnt--)
**		isrq_Put (qp, element++);
**
**	or
**
**	while (isrq_MaxElements (qp) - isrq_Count (qp))
**		isrq_Put (qp, element++);
*/
# ifndef isrq_MaxElements
int isrq_MaxElements (ISRQ * qp);
# endif

# ifndef isrq_Available
int isrq_Available (ISRQ * qp);
# endif

/* isrq_CreateDefault -- Create a queue from the heap of the default
** numElements */
# ifndef isrq_CreateDefault
	ISRQ * isrq_CreateDefault (void);
# endif

/* isrq_Put -- these put a QElem into the queue in FIFO
** order. 
** If the queue if full the first overflow element is 
** placed in the extra field of the queue. Put will then 
** signal the creating process with the value SIG_ISRQ_OVERFLOW
** (defined in siguser.h).  This is meant to be an error indicator
** rather than queue full indicator (though it could be used that
** way). Note that this will kill the task if it not installed
** an interrupt handler. Note that QElems are passed by value.
**
** Parameters:
**	qp -- a queue pointer returned by isrq_Create.
**	qe -- a QElem structure.
**
** Usage:
**	QElem mye;
**
**	mye.tag = MYFLAG;
**	mye.val = MYVAL;
**	if (isrq_Put (qp, mye))
**		error ...
*/
int isrq_Put (ISRQ * qp, QElem u);

/* Like isrq_Put but also sends a SIGWAKE to pid. */
int isrq_PutAndWake (ISRQ * qp, QElem qe) ;

/* isrq_Get -- these get the next element from the queue.  If
** there is an extra element in the queue the extra element gets
** returned first. 
** The extra element is only placed in the queue when the queue
** has overflown. This is generally an error condition.
** Note that the extra element is the last item
** put in the queue but it is the first returned.  The notion
** here is that the signal handler either calls the isrq_Get
** directly or will set state in the process that allows the
** proper recovery of order if that order is needed.  More
** likely is that the process should just exit after cleaning up and
** printing a proper error indication, as this mechanism is meant
** to be an error indicator rather than a notification mechanism.
** Note that the QElem is returned by value.
**
** Parameters:
**	qp -- is queue pointer returned by isrq_Create.
**
** Usage:
**	qe = isrq_Get (qp);
**	if (qe.tag == 0) // assumes tag never set to zero.
**		error ...
*/
QElem isrq_Get (ISRQ * qp);
QElem isrq_NoOverflowGet (ISRQ * qp);
QElem isrq_Peek (ISRQ * qp) ;
QElem isrq_GetOverflow (ISRQ * qp);

# endif

