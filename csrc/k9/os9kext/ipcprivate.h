
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** File: ipcprivate.h - private defintions for the IPC subsystem.
**
** Purpose:
**  To provide the private interface to the IPC interface.
**
** Author: emil
** $Author: emil $
**
** Constants:
**  IPCMAILBOXNUM -- default number of mail boxes
**  IPCBUFFERLEN -- the default ipc data len.
**  IPCNUMBER -- the default number of ipc buffers.
**  IPCMMNAME -- an ascii string that names the ipc memory module.
**  IPCMMEVENTNAME -- an ascii string that names the ipc event.
**
** Types/Clases:
**  IpcMem -- the header of the IPC memory structure.
**  MailBox -- the actual mail box structure where message buffers are queued.
**  IpcB -- the actual message buffer structure.
**
** Inline Functions:
**  Note IPC inline functions can be undefine by the user, causing the real
**  function to be called, by defining NO_IPC_INLINE or NO_INLINE.
**  ipc_GetPriority -- see defintion in ipc.h
**  ipc_SetPriority -- see defintion in ipc.h
**  ipc_GetSender -- see defintion in ipc.h
**  ipc_GetMaxSize -- see defintion in ipc.h
**  ipc_GetMsgLen -- see defintion in ipc.h
**  ipc_SetMsgLen -- see defintion in ipc.h
**  ipc_GetType -- see defintion in ipc.h
**  ipc_GetUserType -- see defintion in ipc.h
**  ipc_SetUserType -- see defintion in ipc.h
**
** Notes:
**
** $Id: ipcprivate.h,v 1.9 1994/12/01 12:17:44 emil Exp $
**
*/

/* We use this to allow the use of the ProcessLock */

/* Note that this is defined iff it is included in ___IPC_H__ */
# ifdef __IPC_H__
# ifndef __IPCPRIVATE_H__
# define __IPCPRIVATE_H__

# include "ProcessLock.h"
# include "ipcuser.h"

# define IPCMAILBOXNUM 32
# define IPCBUFFERLEN IPCMINBUFFERLEN
# define IPCNUMBER 256

# define IPCEVENTNAME "IPCEVENT"

# define IPCLOCKNAME "_IPC_PBlock"



# define SANITY1 (('S'<<24) | ('A'<<16) | ('N'<<8) | 'E')
# define SANITY2 (('E'<<24) | ('N'<<16) | ('A'<<8) | 'S')

typedef struct _IpcB
{
    struct _IpcB * self;
    struct _IpcB * next;
    IpcType type;
    IpcPriority priority;
    MailHandle sender;  /* originator of the ipc */
    MailHandle target;  /* recipient of the ipc */
    size_t dataSize;    /* total size of data[] */
    size_t dataLen; /* actual size of data[] */
    int mnum; /* copy of ipcmp->mnum on alloc, set to -mnum on receive */
    long sanity1;
    IPCUser userType;
    char data[IPCBUFFERLEN + 4]; /* actual size = .dataSize */
    long sanity2; /* to value that allows error checking */
} IpcB;

# ifndef _OS9000
typedef u_int32 process_id;
typedef u_int32 event_id;
# endif

typedef struct _MailBox
{
    char name[MBNAMESIZ + 1]; /* name of process */
    process_id procID; /* os-9000 process id */
    u_int16 procPri; /* os-9000 process priority */
    int16 useNormalSig; /* optional user wakeup signal for normal messges*/
    int16 usePrioritySig; /* optional user wakeup signal for priority msgs */
    IpcType twaiting; /* indicate that the receiver is waiting for a message */
    /* if swaiting == this mail box it is treated as a flag that with
    ** twaiting == TYPE_ANY means that any message will do, if twaiting
    ** is Null it is either running or in waiting for some other OS call.
    ** If swaiting is some other mail box it is wait for a message from
    ** that task */
    struct _MailBox * swaiting;
    int msg;
    IpcB * head;
    IpcB * tail;
    void * rootp;
} MailBox;

# define XIPC_LOG 4 * 1024

typedef struct _IpcMem /* shared memory control header */
{
    struct _IpcMem * self;
    int maxMailboxes;   /* max nbr of mailboxes */
    int maxIPCs;    /* max nbr of ipcs */
    MailHandle mailboxes;   /* first mailbox in shared memory */
    int inuseMailboxes; /* mailboxes in use */
    IpcB * ipcFreeList; /* available ipc buffers */
    ProcessLock IPCLock; /* Uses ProcessLock to lock ipc buffers */
    int waitingProcs; /* used to not if some task need to be woken */
    int mnum; /* message number, this is a debugging thing */
    event_id ipcEvent;  /* block event */
    MailBox boxes[IPCMAILBOXNUM];
    IpcB ipcs[IPCNUMBER];
# ifdef IPC_LOG
	char debugLog[IPC_LOG];
	int di;
# endif
} IpcMem;



/*
** Note caution should be used by any one modifying these functions to
** ensure that they also change the real functions in ipc.c.
*/
#   ifndef NO_INLINE
#       ifndef NO_IPC_INLINE
#           define ipc_GetPriority(ipcp) ((ipcp)->priority)
#           define ipc_SetPriority(ipcp,pri) ((ipcp)->priority = (pri))

#           define ipc_GetSender(ipcp) ((ipcp)->sender)
#           define ipc_GetMsgLen(ipcp) ((ipcp)->dataLen)
#           define ipc_SetMsgLen(ipcp,len) ((ipcp)->dataLen = (len))
    
#           define ipc_GetType(ipcp) ((ipcp)->type)
#           define ipc_GetUserType(ipcp) ((ipcp)->userType)
#           define ipc_SetUserType(ipcp,utype) ((ipcp)->userType = (utype))
#           define ipc_GetDataP(ipcp) ((void *) ((ipcp)->data))
#           define  ipc_GetMaxSize(ipcp) ((ipcp)->dataSize)
#       endif
#   endif

int ipc_create (IpcMem * ipcmp);

# endif
# endif

