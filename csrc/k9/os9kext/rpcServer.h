
/*
** COPYRIGHT (C) 1994, ICTV Inc. 280 Martin Ave. Santa Clara, Calif 95050. 280
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television
**
** File: ipcServer.h
**
** Purpose:
**  Defines interfaces for the ipcServer module.
**
** Types/Clases:
**  Has intimate knowledge of IPCUser enums.
**  RPCServer -- an other name for a MailBox.
**  RPCUserHandler -- funtion type for handling RPCs.
**
** Constants:
**  RPC_NULL_MSG -- used to register function call when task wakes without
**  a message.
** Data:
**
** Functions/Macros:
**  RPC_SEND
**  RPC_SEND_SYNC
**  IPC_DATA_OFFSET
**  RPC_TO_IPC
**  RPC_REPLY
**
** Usage:
**  Note to add a new RPC value to this file, first check to see if
**  there is an appropriate IPCTypes for the value you want to add.
**  Do the obvious: if there is one use it, and if not, add one at the
**  end of the enum list. If you add one also add a new RPC_???_BASE at the
**  end of the IPCUser enum list (copy the form from the existing examples.
**  If you need an explicit add it following the RPC_???_BASE name in
**  the IPCUser enum list.
** Note:
**  The IPC types and the ipc range are constucted to provide a number where
**  the high half of the word provides the type, and the low half is a subtype.
**
**  
**
** $Id: rpcServer.h,v 1.18 1995/11/04 02:03:44 wul Exp $
**
*/

#ifndef __RPCSERVER_H__
#define  __RPCSERVER_H__

#include "ipc.h"



/* RPC User Handler return functions values,
** The value here are chosen to match the ill
** defined early implementation values, i.e.
** 0 ment don't free the buffer and continue,
** and positive (1) ment free it.
** 
**	FreeAndContine -- default for most UserHandlers
**	Continue       -- default when replying to buffer.
**	Exit           -- Value when wanting to have the server 
**	               -- return the buffer to be processed from
**				   -- the caller.
**	FreeAndExit    -- Free the buffer and return with a null message.
**	
** Value that do not fall in this range are treated as FreeAndContinue.
*/


typedef enum _RPCDisposition
{
	RPC_FreeAndExit = -2,
	RPC_Exit = -1,
	RPC_Continue = 0,
	RPC_FreeAndContinue = 1,
    Last_RPCDisposition
} RPCDisposition;

typedef RPCDisposition (* RPCUserHandler) (void * datap, int len);
typedef void (* RPCUserFunction) (void);

void rpc_setUserFunction (RPCUserFunction userFp);

/* For each and every IPC class that is to be supported by the
** ipcServer/ipcClient interface a size item should be defined.
*/


/* RPC_NULL_MSG is used to install a function that should be called when
** the task wakes with no message. */
#define RPC_NULL_MSG IPC_SYS_BASE

/* rpc_init -- this should be called before any other user functions are
**  registered.  It registers the default handler that should be called when
**  the task receives a message that is not registered. If default handler
**  is null then the rpc_noHandler function is used as the handler.
** Parameter:
**  defaultHandler -- function pointer or null.
**
** Note: the macro RPC_RESTORE_IPCP can be used by the called function
** to restore the data pointer to an IpcB pointer.
*/
void rpc_init (RPCUserHandler defaultHandler);

/* rpc_UserRegister --  allows the user to install function handler for
** the IPCUser type.  The user task installs all handlers that
** it is interested in.  This call should be done before you call
** server.
**
** Parameters:
**  utype -- this comes from the list of ipc user type in the file ipcuser.h,
**      and the value PRC_NULL_MSG allows the task to install a handler for
**      the case where the IPC system returns without a message (this can happen
**      for instance when the task recieves a signal.
**  rpcfp -- is the function pointer to function that takes void * and length
**      values.  If rpcfp is null the rpc_noHandler functions is installed
**      in the appropreiate slot.
**
** Returns:
**  returns 0 iff installation succeded.    
**
** Usage:
**  void * AAAHandler (void * vp, int len) { ... }
**  ...
**  void * ZZZHandler (void * vp, int len) { ... }
**  main (...)
**  {
**      ... non ipc/rpc initialization ...
**      ipc_MailBoxCreate (...)
**      ... access to other named mailboxes ...
**      ... other initialization ...
**      rpc_init (mydefaultHandler) or rpc_init (0)
**      rpc_UserRegister (AAA, AAAHandler)
**      ...
**      if (rpc_UserRegister (ZZZ, ZZZHandler))
**          error_message (...)
**      rpc_Server ()
**  }
*/
RPCUserHandler rpc_UserRegister (IPCUser utype, RPCUserHandler rpcfp);


/* rpc_Server -- is the call that does not return because all messages
** or other wake-ups are handled internally.
*/
IpcB * rpc_Server (void);

typedef MailHandle RPCServer;

/* The rpc system provides several mechanisms to for sending requests to
** server.  First one can use the unwrapped IPC mechanism and save the
** the copy synmantics of RPC.  In addition there are two function entry
** points for sending RPC, and to macros that wrap these functions for
** semantic convience.
*/

/* rpc_send -- the basic rpc asynchronous send mechanism.  It many instances it will
**  be easyier to use RPC_SEND (see below).
** Paramters:
**  utype -- should come form the IPCUser enum in ipcuser.h
**  datap -- this can be a pointer to any data.  Generally it should
**  be a pointer to some data structure.
**  len -- this is the length of the data in bytes.  Using the
**  associated macro releives one from calulate in this if datap has
**  an appropriate type.
**  server -- the rpc server that is to process this call.
** Returns:
**  0 iff send operated properly.
*/
int rpc_send (int utype, void * datap, int len, RPCServer server);
/* rpc_sendSync -- the basic rpc synchronous send mechanism.  In many instances it will
**  be easyier to use RPC_SEND_SYNC (see below).
** Paramters:
**  utype -- should come form the IPCUser enum in ipcuser.h
**  datap -- this can be a pointer to any data.  Generally it should
**  be a pointer to some data structure.
**  len -- this is the length of the data in bytes.  Using the
**  associated macro releives one from calulate in this if datap has
**  an appropriate type.
**  server -- the rpc server that is to process this call.
** Returns:
**  0 iff send operated properly.
*/
int rpc_sendSync (int utype, void * datap, int len, RPCServer server);

/* rpc_readSync -- the basic rpc synchronous read mechanism. 
** Paramters:
**  utype -- should come form the IPCUser enum in ipcuser.h
**  inp -- this can be a pointer to any data.  Generally it should
**  be a pointer to some data structure.
**  inlen -- this is the length of the data in bytes.  Using the
**  associated macro releives one from calulate in this if datap has
**  an appropriate type.
**  outp -- this holds the outgoing data
**  outlen -- this is the size of the outgoing data
**            If this is null, don't put in a size.
**  server -- the rpc server that is to process this call.
** Returns:
**  0 iff send operated properly.
*/
int rpc_readSync (int utype, void * inp, int inlen,
                  void *outp, int *outlen, RPCServer server);

/* RPC_SEND -- this a macro that wraps rpc_send when sending a structure.
** Parameters:
**  utype -- should come form the IPCUser enum in ipcuser.h
**  structp -- a pointer to a structure or simple type you want to send.
**  server -- the rpc server that is to process this call.
*/  
#define RPC_SEND(utype, structp, server) \
    rpc_send \
    ( \
        (int) (utype), (void *) (structp), sizeof (*(structp)), server \
    )

/* RPC_SEND_SYNC -- this a macro that wraps rpc_sendSync when sending a structure.
** Parameters:
**  utype -- should come form the IPCUser enum in ipcuser.h
**  structp -- a pointer to a structure or simple type you want to send.
**  server -- the rpc server that is to process this call.
*/  
#define RPC_SEND_SYNC(utype, structp, server) \
    rpc_sendSync \
    ( \
        (int) (utype), (void *) (structp), sizeof (*(structp)), server \
    )

/* RPC_READ_SYNC -- this a macro that wraps rpc_sendSync when sending a structure.
** Parameters:
**  utype -- should come form the IPCUser enum in ipcuser.h
**  inp -- a pointer to a structure or simple type you want to send.
**  outp -- a pointer to a structure that contains the read data.
**  sizep -- a pointer to the size of the outp structure.
**  server -- the rpc server that is to process this call.
*/  
# define RPC_READ_SYNC(utype, inp, outp, server) \
    rpc_readSync \
    ( \
        (int) (utype), (void *) (inp), sizeof (*(inp)), \
        (void *) (outp), 0, server \
    )

/* Send Usage:
**  server = ipc_GetMailHandle (ASCIIServerName);
**  struct {int a; int b; char c[13]} mystruct;
**  // genearlly this structure whould be published by the server.
**  RPC_SEND (IPC_XXX_AAA, &mystruct, server);
**  // returns immediatly if we have priority over server
**  or
**  RPC_SEND_SYNC (IPC_XXX_AAA, &mystruct, server);
**  // return only after reply from server.
*/


/* IPC_DATA_OFFSET -- should be treated as private to rpc/ipc. */
#define IPC_DATA_OFFSET ((int) (((IpcB *) 0)->data - (char *) ((IpcB *) 0)))

/* RPC_TO_IPC -- This macro allows an RPC function to access the ipc
** informaton about the receive message via the IPC interface calls.
*/
#define RPC_TO_IPC(datap) ((IpcB *) (((char *) (datap)) - IPC_DATA_OFFSET))

/*
** rpc_reply -- this function along with the associated macro allows a
** server function to reply to a client without directly inenquiring as
** to the identity of the client.
*/
int rpc_reply (u_int utype, void * datap, int len);

#define RPC_REPLY(utype, datap) rpc_reply ((utype), (datap), sizeof (*(datap)))

#define RPC_RETURN(datap) ipc_Reply (RPC_TO_IPC((datap)))

/*
** rpc_forward -- this allows the recieving fucntion to forward the underlying
** IPC buffer.  The macro RPC_FORWARD works like the other macros defined
** above.
*/
int rpc_forward (u_int utype, void * datap, int len, MailHandle mh);

/*
** rpc_redirect -- takes a void pointer received as an RPC message
** redirects to another server unchanged.  The send of the message
** is the redirector, not the oriognal sender.
*/
int rpc_redirect (void * datap, RPCServer server);

#define RPC_FORWARD(utype, datap, mh) \
    rpc_forward ((utype), (datap), sizeof (*(datap)), mh)

#define rpc_getSender(dp) (ipc_GetSender (RPC_TO_IPC (dp)))

#define RPC_FREE_BUFFER(datap) ipc_FreeBuffer (RPC_TO_IPC (datap))

#endif

