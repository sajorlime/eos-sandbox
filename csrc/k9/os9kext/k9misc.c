
/*
** Copyright (C) 1994,  ICTV Inc.
**                      280 Martin Ave.,
**                      Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television using OS9K
**
** File: ~dev/ICTV/libsrc/k9misc.c - other miscellaneous k9 wapers.
**
** Author: bruce & emil
**
** Purpose:
**
** Types/Clases:
**
** Data
**
** Functions:
**	k9_getpriority -- returns the K9 process priority.
**
** Notes:
**
** $Id: k9misc.c,v 1.8 1995/11/04 02:03:41 wul Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/k9misc.c,v $
**
*/

#include <types.h>
#include <stdio.h>
#include <modes.h>
#include <io.h>
#include <stddef.h>
#include <rom.h>
#include <sysglob.h>
#include <module.h>

#include <ifman.h>
#include <INET/in.h>
#include <bootp_save.h>
#include <bootp.h>

#include "ictvcommon.h"
#include "k9misc.h"

# define __K9MISC_C__

u_int32 
k9_getpriority (void)
{
	process_id pid;
	prior_level pr;
	u_int16 age, group, user;
	int32 sched;

	_os_id (&pid, &pr, &age, &sched, &group, &user);
	return (u_int32) pr;
}

process_id getppid (void)
{
	process_id pid;
	process_id ppid;
	pr_desc proc;
	u_int32 size = sizeof(proc);
	error_code error = 0;

	if (_os_gprdsc(getpid (), &proc, &size))
		return 0;
	return  proc.p_pid;
}


void
k9_defer (void)
{
    signal_code sig = 0;
    u_int32 ticks = 1;

	_os_sleep (&ticks, &sig);
}

static struct {
	struct in_addr myIPaddr;
	struct in_addr cfgAddr;
	u_int32 cfgPort;
	struct bootp *bootpPkt;
} bootpInfo = {0, 0, 0, 0};

#define AIX_COOKIE0 0x63
#define AIX_COOKIE1 0x82
#define AIX_COOKIE2 0x53
#define AIX_COOKIE3 0x63

int32 parseBPvend(void *vdata)
{
	u_char *ptr = (u_char *)vdata;

	if (!(ptr[0]==AIX_COOKIE0 && ptr[1]==AIX_COOKIE1 &&
		ptr[2]==AIX_COOKIE2 && ptr[3]==AIX_COOKIE3))
	  return -1;

	ptr += 4;
	
	while ((*ptr != TAG_END) && ((u_int32)ptr - (u_int32)vdata < 64)){
		u_int32 length;

		length = ptr[1];
		switch (*ptr) {
		  case TAG_TIME_OFFSET:
			ptr += 2;
			bootpInfo.cfgPort = ntohl(*((u_int32 *)ptr));
			break;

		  case TAG_TIME_SERVER:
			ptr += 2;
			bootpInfo.cfgAddr.s_addr = ntohl(*((u_int32 *)ptr));
			break;

		  default:
			ptr += 2;			/* point at the length byte */
			break;
		}

		ptr += length;
	}

	if ((bootpInfo.cfgAddr.s_addr == 0) || (bootpInfo.cfgPort == 0))
	  return -1;

	return 0;
}

int32 getBootpInfo(void)
{
	Rominfo sysrom;
	struct bootp_save *bpSave;
	struct bootp *bpPkt;
	u_int32 mypid = getpid();

	if ((u_int32) bootpInfo.bootpPkt == 0) {
	
		/* get the pointer to the Rominfo structure */
		_os_getsys(offsetof(sysglobs, d_sysrom), 4, (glob_buff *) &sysrom);

		/* allow read access to the Rominfo structure */
		_os_permit((void *)sysrom, sizeof(rominfo), MP_WORLD_READ, mypid);

		/* find the pointer to the bootp save structure */
		bpSave = (struct bootp_save *)(sysrom->rom_unused[0]);

		if (bpSave == NULL)
		  return -1;

		/* allow read access to the bootp save structure */
		_os_permit((void *)bpSave, sizeof(struct bootp_save), MP_WORLD_READ, mypid);

		/* find the pointer to the bootp response packet */
		bpPkt = bpSave->bprp;

		/* allow read access to the bootp response packet */
		_os_permit((void *)bpPkt, sizeof(struct bootp), MP_WORLD_READ, mypid);

		bootpInfo.bootpPkt = bpPkt;

		/* parse the bootp packet */

		/* get out own ip address */
		bootpInfo.myIPaddr.s_addr = ntohl(bpPkt->bp_yiaddr.s_addr);

		/* parse the vendor specific data */
		if (parseBPvend((void *)bpPkt->bp_vend)) {
			bootpInfo.bootpPkt = NULL;
			return -1;
		}
	}

	return 0;

}

struct bootp *getBootp(void)
{
    getBootpInfo();
    return bootpInfo.bootpPkt;
}

int32 getRomVersion(void)
{
	Rominfo sysrom;
    static u_int32 version = -1;
	u_int32 mypid = getpid();

    if (version == -1)
    {
        /* get the pointer to the Rominfo structure */
        _os_getsys(offsetof(sysglobs, d_sysrom), 4, (glob_buff *) &sysrom);

        /* allow read access to the Rominfo structure */
        _os_permit((void *)sysrom, sizeof(rominfo), MP_WORLD_READ, mypid);

        /* find the pointer to the bootp save structure */
        version = ((u_int32)sysrom->rom_unused[1]);
    }

    return version;
}



extern error_code _os_gs_fd(int,int,ifm_dev_list*) ;

u_long getLocalIPAddrDrv(char *deviceName)
{
  int i ;
  ifm_dev_list ifd;
  struct sockaddr_in *sin;
  error_code error ;
  char defaultDevice[]="/e30";

  if (deviceName == NULL)
	deviceName = defaultDevice;

  if ((i = open(deviceName, 1)) == -1)
    {
      fprintf(stderr,"can't open '%s'\n", deviceName);
      return 0 ;
    }
	
  if ((error = _os_gs_fd(i, sizeof(ifm_dev_list), &ifd)) != SUCCESS)
    {
      fprintf(stderr,"getstat SS_FD to ifman failed!\n");
      close(i) ;
      return 0 ;
    }

  close(i) ;
  sin = IA_SIN(&ifd);
  return sin->sin_addr.s_addr ;
}


u_long getLocalIPAddr(char *deviceName)
{
	if (getBootpInfo() == 0)
	  return bootpInfo.myIPaddr.s_addr;
	else 
	  return ntohl(getLocalIPAddrDrv(deviceName));
}

int32 getCfgEndPoint(struct in_addr *cfgAddr, u_int32 *cfgPort)
{
	if (getBootpInfo() == 0) {
		*cfgAddr = bootpInfo.cfgAddr;
		*cfgPort = bootpInfo.cfgPort;
		return 0;
	}

	return -1;
}


#ifdef TIMESTAMP
void
nop()
{
}
#endif

static unsigned int readTimer(u_int32 reg)
{
	int i;

	return ((unsigned int) inc(PIT1+reg));
}

static void writeTimer(u_int32 reg, u_char val)
{
	int i;

	outc(PIT1+reg, val);
}

int k9_getClockTicks(void)
{
  int time;
	
  writeTimer(PIT_CONTROL, CR_SC0); /* setup for read during countdown */
  time = readTimer(PIT_TIMER0);	/* get lsb of count */

  time |= (readTimer(PIT_TIMER0) << 8) ;
  return time ;
}

int
k9_picTimerInterruptPending (void)
{
	outc (PIC_PC_ADDR, IRR_SELECT);
	return (inc (PIC_PC_ADDR) & PIC_TIMER_INTR);
}
