
/*
** Copyright (C) 1994,  ICTV Inc.
**                      280 Martin Ave.,
**                      Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv -- multimedia controller.
**
** File: k9Fork.h -- 
**
** Author: scott, adapted by BDN
**
** Purpose:
**  This module, provides a simplified interface to 9K fork/exec.
**
** Types/Clases:
**
** Notes:
**
** $Id: k9fork.h,v 1.4 1994/12/07 00:47:45 bruce Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/k9fork.h,v $
**
*/

# ifndef __K9FORK_H__
# define __K9FORK_H__

#include <types.h>

/* Does a unix style fork/exec. args is null terminated argument list
** of strings. childid is returned. paths is the number of paths to
** keep open in the child process. A priority of 0 gives the child the
** same priority as the parent. */

extern error_code k9_fork(char **args, process_id *childId, 
			  int paths, u_int16 priority) ;

extern error_code k9_orphanFork(char **args, process_id *childId, 
				int paths, u_int16 priority, int orphanp) ;

# define k9fork k9_fork

#endif
