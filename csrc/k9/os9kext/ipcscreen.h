
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** File: ipcscreen.h - define the screen interface.
**
** Purpose:
**
** Author: emil
**
** Types/Clases:
**
** Fucntions:
**
** Notes:
**
** $Id: ipcscreen.h,v 1.5 1995/06/25 22:28:24 wul Exp $
**
*/

# ifndef __IPCSCREEN_H__
# define __IPCSCREEN_H__


void pscr_init (char * title);


void pscr_show
(
    char * format, ...
);



void ipc_printIPCB (IpcB * ipcp, int verbose);
void ipc_printIPCData (IpcB * ipcp, int verbose);
void ipc_printIPCMem (IpcMem * ipcmp, int verbose);
void ipc_printMailbox (MailBox * mbp, int verbose);


/* these are printing accessors */
char * ipcfriend_sender (IpcB * ipcp);
int    ipcfriend_senderId (IpcB * ipcp);
char * ipcfriend_target (IpcB * ipcp);
char * ipcfriend_type (IpcB * ipcp);
char * ipcfriend_userType (IpcB * ipcp);
char * ipcfriend_priority (IpcB * ipcp);

char * rpcfriend_sender (void * dp);
int    rpcfriend_senderId (void * dp);
char * rpcfriend_target (void * dp);
char * rpcfriend_type (void * dp);
char * rpcfriend_userType (void * dp);
char * rpcfriend_priority (void * dp);

int ipcfriend_getProcId (void);
char * ipcfriend_getMailName (void);
int ipcfriend_getProcPri (void);

# endif

