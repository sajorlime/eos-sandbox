
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** File: ipctypeprint.c - print the various ipc enum type
**
** Purpose:
**  To aid in debugging.
**
** Origonal Author: emil
** $Author: wul $
**
** Types/Clases:
**
** Data:
**
** Functions:
**  ipc_ErrorString -- prints a string for an IpcErrorType.
**  ipc_PriorityString -- prints a string for an IpcPriorityType.
**  ipc_TypeString -- prints a string for an IpcType.
**  ipc_UserTypeString -- prints a string for an IpcUserType.
**
** Notes:
**
** $Id: ipctypeprint.c,v 1.13 1995/11/04 02:03:40 wul Exp $
**
*/

# define __IPCTYPEPRINT_C__
# include <assert.h>
# include <signal.h>
# include <types.h>
# include <stdio.h>
# include <stdlib.h>
# include <modes.h>
# include <string.h>


# include "ipc.h"
# include "ipcuser.h"


# define MAXINDEX(ARRAY) (sizeof (ARRAY) / sizeof (ARRAY[0]))

static char sOUT_OF_RANGE[40] = "OUT_OF_RANGE";
static char *
ipcOutOfRange (char * tp, unsigned int v)
{
    char sV[15];

    sprintf (sV, " %s%x", tp, v);
    strcpy (&sOUT_OF_RANGE[12], sV);
    return sOUT_OF_RANGE;
}



/* IpcError defintions */
static char sIPCERR_NONE[] = "IPCERR_NONE";
static char sIPCERR_UNABLE_TO_LINK_IPCMM[] = "IPCERR_UNABLE_TO_LINK_IPCMM";
static char sIPCERR_UNKNOWN_MAILBOX[] = "IPCERR_UNKNOWN_MAILBOX";
static char sIPCERR_UNKNOWN_REPLY_TYPE[] = "IPCERR_UNKNOWN_REPLY_TYPE";
static char sIPCERR_ILLEGAL_BUFFER[] = "IPCERR_ILLEGAL_BUFFER";
static char sIPCERR_ILLEGAL_MAILBOX[]  = "IPCERR_ILLEGAL_MAILBOX";
static char sIPCERR_UNALLOCATED_BUFFER[] = "IPCERR_UNALLOCATED_BUFFER";
static char sIPCERR_DUPNAME[] = "IPCERR_DUPNAME";
static char sIPCERR_NO_HANDLER[] = "IPCERR_NO_HANDLER";

static char * IpcErrorSV [] =
{
    sIPCERR_NONE,
    sIPCERR_UNABLE_TO_LINK_IPCMM,
    sIPCERR_UNKNOWN_MAILBOX,
    sIPCERR_UNKNOWN_REPLY_TYPE,
    sIPCERR_ILLEGAL_BUFFER,
    sIPCERR_ILLEGAL_MAILBOX,
    sIPCERR_UNALLOCATED_BUFFER,
    sIPCERR_DUPNAME,
    sIPCERR_NO_HANDLER
};


char *
ipc_ErrorString (IpcError err)
{
    if (((unsigned) err) >= MAXINDEX (IpcErrorSV))
        return ipcOutOfRange ("e:", err);
    return IpcErrorSV[err];
}

static char sIPC_PRI_NORMAL[] = "IPC_PRI_NORMAL";
static char sIPC_PRI_HIGH[] = "IPC_PRI_HIGH";

static char * IpcPrioritySV[] =
{
    sIPC_PRI_NORMAL,
    sIPC_PRI_HIGH
};



char *
ipc_PriorityString (IpcPriority pri)
{
    if (((unsigned) pri) >= MAXINDEX (IpcPrioritySV))
        return ipcOutOfRange ("p:", pri);
    return IpcPrioritySV[pri];
}

static char sIPC_TYPE_ANY[] = "IPC_TYPE_ANY";
static char sIPC_TYPE_REQUEST[] = "IPC_TYPE_REQUEST";
static char sIPC_TYPE_REPLY[] = "IPC_TYPE_REPLY";
static char sIPC_TYPE_SYNC_REQUEST[] = "IPC_TYPE_SYNC_REQUEST";
static char sIPC_TYPE_SYNC_REPLY[] = "IPC_TYPE_SYNC_REPLY";
static char sIPC_TYPE_SYNC_DISPOSE[] = "IPC_TYPE_SYNC_DISPOSE";
static char sRPC_TYPE_REQUEST[] = "RPC_TYPE_REQUEST";
static char sRPC_TYPE_REPLY[] = "RPC_TYPE_REPLY";
static char sRPC_TYPE_SYNC_REQUEST[] = "RPC_TYPE_SYNC_REQUEST";
static char sRPC_TYPE_SYNC_REPLY[] = "RPC_TYPE_SYNC_REPLY";
static char sIPC_TYPE_RENDEZVOUS[] = "IPC_TYPE_RENDEZVOUS";
static char sIPC_TYPE_RENDEZVOUS_REPLY[] = "IPC_TYPE_RENDEZVOUS_REPLY";
static char sIPC_TYPE_UNUSED[20] = "IPC_TYPE_UNUSED";


static char * IpcTypeSV[] = 
{
    sIPC_TYPE_ANY,
    sIPC_TYPE_REQUEST,
    sIPC_TYPE_REPLY,
    sIPC_TYPE_SYNC_REQUEST,
    sIPC_TYPE_SYNC_REPLY,
    sIPC_TYPE_SYNC_DISPOSE,
    sRPC_TYPE_REQUEST,
    sRPC_TYPE_REPLY,
    sRPC_TYPE_SYNC_REQUEST,
    sRPC_TYPE_SYNC_REPLY,
    sIPC_TYPE_RENDEZVOUS,
    sIPC_TYPE_RENDEZVOUS_REPLY,
    sIPC_TYPE_UNUSED
};

char *
ipc_TypeString (IpcType type)
{

    if (((unsigned) type) >= MAXINDEX (IpcTypeSV))
    {
        if ((type & ~0x0f) == IPC_TYPE_UNUSED)
        {
            type &= 0xf;
            strcpy (&sIPC_TYPE_UNUSED[7], IpcTypeSV[type]);
            return sIPC_TYPE_UNUSED;
        }
        return ipcOutOfRange ("t:", type);
    }
    return IpcTypeSV[type];
}

static char sIPC_SYS_BASE[] = "SYS_BASE";
static char sIPC_SYS_ABORT[] = "SYS_ABORT";
static char sIPC_SYS_ABORTED[] = "SYS_ABORTED";
static char sIPC_SYS_FLUSH[] = "SYS_FLUSH";
static char sIPC_SYS_FLUSHED[] = "SYS_FLUSHED";
static char sIPC_SYS_DATA[] = "SYS_DATA";
static char sIPC_SYS_DISPLAY[] = "SYS_DISPLAY";
static char sIPC_SYS_ACK[] = "SYS_ACK";
static char sIPC_SYS_SET_DEBUG_LEVEL[] = "SYS_SET_DEBUG_LEVEL";
static char sIPC_SYS_SET_VERBOSE_LEVEL[] = "SYS_SET_VERBOSE_LEVEL";

static char * IpcSysUTypeSV[] = 
{
    sIPC_SYS_BASE,
    sIPC_SYS_ABORT,
    sIPC_SYS_ABORTED,
    sIPC_SYS_FLUSH,
    sIPC_SYS_FLUSHED,
    sIPC_SYS_DATA,
    sIPC_SYS_DISPLAY,
    sIPC_SYS_ACK,
    sIPC_SYS_SET_DEBUG_LEVEL,
    sIPC_SYS_SET_VERBOSE_LEVEL
};

static char *
ipc_UserSysString (unsigned int systype)
{
    if (systype >= MAXINDEX (IpcSysUTypeSV))
        return ipcOutOfRange ("-sys:", systype);
    return IpcSysUTypeSV[systype];
}

static char *
ipc_UserCMXString (unsigned int cmxtype)
{
    return ipcOutOfRange ("-cmx:", cmxtype);
}


static char sIPC_AVGSM_BASE[] = "AVGSM_BASE";
static char sIPC_AVGSM_ACTION[] = "AVGSM_ACTION";
static char sIPC_AVGSM_CANCEL_ACTION[] = "AVGSM_CANCEL_ACTION";
static char sIPC_AVGSM_ALARM[] = "AVGSM_ALARM";
static char sIPC_AVGSM_CANCEL_ALARM[] = "AVGSM_CANCEL_ALARM";

static char * IpcAVGSMUTypeSV[] = 
{
    sIPC_AVGSM_BASE,
    sIPC_AVGSM_ACTION,
    sIPC_AVGSM_CANCEL_ACTION,
    sIPC_AVGSM_ALARM,
    sIPC_AVGSM_CANCEL_ALARM
};


static char *
ipc_UserAVGSMString (unsigned int avgsmtype)
{
    if (avgsmtype >= MAXINDEX (IpcAVGSMUTypeSV))
        return ipcOutOfRange ("-avgsm:", avgsmtype);
    return IpcAVGSMUTypeSV[avgsmtype];
}


static char *
ipc_UserVDCPString (unsigned int vdcptype)
{
    return ipcOutOfRange ("mt:", vdcptype);
}

static char sIPC_DVP_BASE[] = "DVP_BASE";
static char sIPC_DVP_VE_REQUEST[] = "DVP_VE_REQUEST";
static char sIPC_DVP_VE_COMPLETE[] = "DVP_VE_COMPLETE";
static char sIPC_DVP_VE_ABORT[] = "DVP_VE_ABORT";
static char sIPC_DVP_MODE_REQUEST[] = "DVP_MODE_REQUEST";
static char sIPC_DVP_MODE_COMPLETE[] = "DVP_MODE_COMPLETE";
static char sIPC_DVP_NOTIFY[] = "DVP_NOTIFY";
static char sIPC_DVP_DATA_ERROR[] = "DVP_DATA_ERROR";
static char sIPC_DVP_ERROR[] = "DVP_ERROR";
static char sIPC_DVP_CHASER[] = "DVP_CHASER";
static char sIPC_DVP_CHASER_COMPLETE[] = "DVP_CHASER_COMPLETE";
static char sIPC_DVP_GFX_REQUEST[] = "DVP_GFX_REQUEST";
static char sIPC_DVP_GFX_COMPLETE[] = "DVP_GFX_COMPLETE";
static char sIPC_DVP_TRANSITION[] = "DVP_TRANSITION";
static char sIPC_DVP_TRANSITION_COMPLETE[] = "DVP_TRANSITION_COMPLETE";
static char sIPC_DVP_FILL_REQUEST[] = "DVP_FILL_REQUEST";
static char sIPC_DVP_FILL_COMPLETE[] = "DVP_FILL_COMPLETE";

static char * IpcDVPUTypeSV[] = 
{
    sIPC_DVP_BASE,
    sIPC_DVP_VE_REQUEST,
    sIPC_DVP_VE_COMPLETE,
    sIPC_DVP_VE_ABORT,
    sIPC_DVP_MODE_REQUEST,
    sIPC_DVP_MODE_COMPLETE,
    sIPC_DVP_NOTIFY,
    sIPC_DVP_DATA_ERROR,
    sIPC_DVP_ERROR,
    sIPC_DVP_CHASER,
    sIPC_DVP_CHASER_COMPLETE,
    sIPC_DVP_GFX_REQUEST,
    sIPC_DVP_GFX_COMPLETE,
    sIPC_DVP_GFX_COMPLETE,
    sIPC_DVP_TRANSITION,
    sIPC_DVP_TRANSITION_COMPLETE,
    sIPC_DVP_FILL_REQUEST,
    sIPC_DVP_FILL_COMPLETE
};

static char *
ipc_UserDVPString (unsigned int dvptype)
{
    if (dvptype >= MAXINDEX (IpcDVPUTypeSV))
        return ipcOutOfRange ("dt:", dvptype);
    return IpcDVPUTypeSV[dvptype];
}


static char sIPC_VGL_BASE[] = "VGL_BASE";
static char sIPC_VGL_RENDER_REQUEST[] = "VGL_RENDER_REQUEST";
static char sIPC_VGL_SHOW_REQUEST[] = "VGL_SHOW_REQUEST";
static char sIPC_VGL_SHOW_COMPLETE[] = "VGL_SHOW_COMPLETE";
static char sIPC_VGL_DATA_ERROR[] = "VGL_DATA_ERROR";
static char sIPC_VGL_ADMIN_REQUEST[] = "VGL_ADMIN_REQUEST";
static char sIPC_VGL_INIT[] = "VGL_INIT";
static char sIPC_VGL_INIT_FAILURE[] = "VGL_INIT_FAILURE";

static char * IpcVGLUTypeSV[] = 
{
    sIPC_VGL_BASE,
    sIPC_VGL_RENDER_REQUEST,
    sIPC_VGL_SHOW_REQUEST,
    sIPC_VGL_SHOW_COMPLETE,
    sIPC_VGL_DATA_ERROR,
    sIPC_VGL_ADMIN_REQUEST,
    sIPC_VGL_INIT,
    sIPC_VGL_INIT_FAILURE
};

static char *
ipc_UserVGLString (unsigned int vgptype)
{
    if (vgptype >= MAXINDEX (IpcVGLUTypeSV))
        return ipcOutOfRange ("vt:", vgptype);
    return IpcVGLUTypeSV[vgptype];
}



static char sIPC_ADCP_DATA[] = "ADCP_DATA";
static char sIPC_ADCP_RESET[] = "ADCP_RESET";
static char sIPC_ADCP_RESTART[] = "ADCP_RESTART";
static char sIPC_ADCP_MUTE[] = "ADCP_MUTE";
static char sIPC_ADCP_PLAY[] = "ADCP_PLAY";
static char sIPC_ADCP_PAUSE[] = "ADCP_PAUSE";
static char sIPC_ADCP_STOP[] = "ADCP_STOP";
static char sIPC_ADCP_SYNC[] = "ADCP_SYNC";
static char sIPC_ADCP_TIME[] = "ADCP_TIME";
static char sIPC_ADCP_DELAY[] = "ADCP_DELAY";
static char sIPC_ADCP_STATUS[]  = "ADCP_STATUS";
static char sIPC_ADCP_SELECT[] = "ADCP_SELECT";
static char sIPC_ADCP_NOTIFY[] = "ADCP_NOTIFY";
static char sIPC_ADCP_ERROR[] = "ADCP_ERROR";
static char sIPC_ADCP_FLUSH[] = "ADCP_FLUSH"; 

static char * IpcADCPUTypeSV[] = 
{
    sIPC_ADCP_DATA,
    sIPC_ADCP_DATA,
    sIPC_ADCP_RESET,
    sIPC_ADCP_RESTART,
    sIPC_ADCP_MUTE,
    sIPC_ADCP_PLAY,
    sIPC_ADCP_PAUSE,
    sIPC_ADCP_STOP,
    sIPC_ADCP_SYNC,
    sIPC_ADCP_TIME,
    sIPC_ADCP_DELAY,
    sIPC_ADCP_STATUS,
    sIPC_ADCP_SELECT,
    sIPC_ADCP_NOTIFY,
    sIPC_ADCP_ERROR, 
    sIPC_ADCP_FLUSH 
};

static char *
ipc_UserADCPString (unsigned int adcptype)
{
    if (adcptype >= MAXINDEX (IpcADCPUTypeSV))
        return ipcOutOfRange ("adcp:", adcptype);
    return IpcADCPUTypeSV[adcptype];
}


static char sIPC_DAP_UNDEFINED[] = "IPC_DAP_UNDEFINED";

static char * IpcDAPUTypeSV[] = 
{
    sIPC_DAP_UNDEFINED
};

static char *
ipc_UserDAPString (unsigned int daptype)
{
    if (daptype >= MAXINDEX (IpcDAPUTypeSV))
        return ipcOutOfRange ("dt:", daptype);
    return IpcDAPUTypeSV[daptype];
}

static char sIPC_SET_UNDEFINED[] = "IPC_SET_UNDEFINED";

static char sIPC_SET_BASE[] = "SET_BASE";
static char sIPC_SET_SESSION_TIMEOUT[] = "SET_SESSION_TIMEOUT";
static char sIPC_SET_SERVICE_LAUNCH[] = "SET_SERVICE_LAUNCH";
static char sIPC_SET_SERVICE_EXIT[] = "SET_SERVICE_EXIT";
static char sIPC_SET_SERVICE_READY[] = "SET_SERVICE_READY";
static char sIPC_SET_SERVICE_EVENTMAP[] = "SET_SERVICE_EVENTMAP";
static char sIPC_SET_SETTOP_OVERLAY[]  = "SET_SETTOP_OVERLAY";
static char sIPC_SET_SETTOP_REMOVE[] = "SET_SETTOP_REMOVE";
static char sIPC_SET_SETTOP_TUNE[] = "SET_SETTOP_TUNE";
static char sIPC_SET_SETTOP_RATE[] = "SET_SETTOP_RATE";
static char sIPC_SET_SETTOP_KEYS[] = "SET_SETTOP_KEYS";
static char sIPC_SET_SETTOP_STATUS[] = "SET_SETTOP_STATUS";

static char * IpcSETUTypeSV[] = 
{
    sIPC_SET_BASE,
    sIPC_SET_SESSION_TIMEOUT,
    sIPC_SET_SERVICE_LAUNCH,
    sIPC_SET_SERVICE_EXIT,
    sIPC_SET_SERVICE_READY,
    sIPC_SET_SERVICE_EVENTMAP,
    sIPC_SET_SETTOP_OVERLAY,
    sIPC_SET_SETTOP_REMOVE,
    sIPC_SET_SETTOP_TUNE,
    sIPC_SET_SETTOP_RATE,
    sIPC_SET_SETTOP_KEYS,
    sIPC_SET_SETTOP_STATUS
};

static char *
ipc_UserSETString (unsigned int settype)
{
    if (settype >= MAXINDEX (IpcSETUTypeSV))
        return ipcOutOfRange ("sett:", settype);
    return IpcSETUTypeSV[settype];
}


static char sIPC_CDSL_UNDEFINED[] = "IPC_CDSL_UNDEFINED";

static char * IpcCDSLUTypeSV[] = 
{
    sIPC_CDSL_UNDEFINED
};

static char *
ipc_UserCDSLString (unsigned int cdsltype)
{
    if (cdsltype >= MAXINDEX (IpcCDSLUTypeSV))
        return ipcOutOfRange ("cdslt:", cdsltype);
    return IpcCDSLUTypeSV[cdsltype];
}


static char sIPC_DB_UNDEFINED[] = "IPC_DB_UNDEFINED";
static char sIPC_DB_REQUEST[] = "IPC_DB_REQUEST";
static char sIPC_RDB_REPLY[] = "IPC_RDB_REPLY";

static char * IpcDBUTypeSV[] = 
{
    sIPC_DB_UNDEFINED,
    sIPC_DB_REQUEST,
    sIPC_RDB_REPLY
};

static char *
ipc_UserDBString (unsigned int dbtype)
{
    if (dbtype >= MAXINDEX (IpcDBUTypeSV))
        return ipcOutOfRange ("dbt:", dbtype);
    return IpcDBUTypeSV[dbtype];
}


static char sIPC_AVSTR_BASE[] = "AVSTR_BASE";
static char sIPC_AVSTR_DATA[] = "AVSTR_DATA";
static char sIPC_AVSTR_RESET[] = "AVSTR_RESET";
static char sIPC_AVSTR_ABORT[] = "AVSTR_ABORT";
static char sIPC_AVSTR_RESTART[] = "AVSTR_RESTART";
static char sIPC_AVSTR_MUTE[] = "AVSTR_MUTE";
static char sIPC_AVSTR_PLAY[] = "AVSTR_PLAY";
static char sIPC_AVSTR_STOP[] = "AVSTR_STOP";
static char sIPC_AVSTR_PAUSE[] = "AVSTR_PAUSE";
static char sIPC_AVSTR_RESUME[] = "AVSTR_RESUME";
static char sIPC_AVSTR_TIME[] = "AVSTR_TIME";
static char sIPC_AVSTR_DELAY[] = "AVSTR_DELAY";
static char sIPC_AVSTR_PARSER_INIT[] = "AVSTR_PARSER_INIT";
static char sIPC_AVSTR_RECLAIMER_INIT[] = "AVSTR_RECLAIMER_INIT";
static char sIPC_AVSTR_SETSTREAM[] = "AVSTR_SETSTREAM";
static char sIPC_AVSTR_OPEN[] = "AVSTR_OPEN";
static char sIPC_AVSTR_CLOSE[] = "AVSTR_CLOSE";
static char sIPC_AVSTR_OPENED[] = "AVSTR_OPENED";

static char * IpcAVSTRUTypeSV[] = 
{
    sIPC_AVSTR_BASE,
    sIPC_AVSTR_DATA,
    sIPC_AVSTR_RESET,
    sIPC_AVSTR_ABORT,
    sIPC_AVSTR_RESTART,
    sIPC_AVSTR_MUTE,
    sIPC_AVSTR_PLAY,
    sIPC_AVSTR_STOP,
    sIPC_AVSTR_PAUSE,
    sIPC_AVSTR_RESUME,
    sIPC_AVSTR_TIME,
    sIPC_AVSTR_DELAY,
    sIPC_AVSTR_PARSER_INIT,
    sIPC_AVSTR_RECLAIMER_INIT,
    sIPC_AVSTR_SETSTREAM,
    sIPC_AVSTR_OPEN,
    sIPC_AVSTR_CLOSE,
    sIPC_AVSTR_OPENED,
};

static char *
ipc_UserAVSTRString (unsigned int avstrtype)
{
    if (avstrtype >= MAXINDEX (IpcAVSTRUTypeSV))
        return ipcOutOfRange ("avstrt:", avstrtype);
    return IpcAVSTRUTypeSV[avstrtype];
}


static char sIPC_GFT_BASE[] = "GFT_BASE";
static char sIPC_GFT_RENDER_GRAPHICS[] = "GFT_RENDER_GRAPHICS";
static char sIPC_GFT_RENDER_ALPHA[] = "GFT_RENDER_ALPHA";
static char sIPC_GFT_RENDER_COMPRESSED_ALPHA[] = "GFT_RENDER_COMPRESSED_ALPHA";
static char sIPC_GFT_FILL_RECTANGLE[] = "GFT_FILL_RECTANGLE";
static char sIPC_GFT_INSET_MASK[] = "GFT_INSET_MASK";
static char sIPC_GFT_RENDER_ALPHA_OVERLAY[] = "GFT_RENDER_ALPHA_OVERLAY";
static char sIPC_GFT_RENDER_GRAPHICS_OVERLAY[] = "GFT_RENDER_GRAPHICS_OVERLAY";
static char sIPC_GFT_ABORT[] = "GFT_ABORT";
static char sIPC_GFT_THROTTLE[] = "GFT_THROTTLE";

static char * IpcGFTUTypeSV[] = 
{
    sIPC_GFT_BASE,
    sIPC_GFT_RENDER_GRAPHICS,
    sIPC_GFT_RENDER_ALPHA,
    sIPC_GFT_RENDER_COMPRESSED_ALPHA,
    sIPC_GFT_FILL_RECTANGLE,
    sIPC_GFT_INSET_MASK,
    sIPC_GFT_RENDER_ALPHA_OVERLAY,
    sIPC_GFT_RENDER_GRAPHICS_OVERLAY,
    sIPC_GFT_ABORT,
    sIPC_GFT_THROTTLE
};

static char *
ipc_UserGFTString (unsigned int gfttype)
{
    if (gfttype >= MAXINDEX (IpcGFTUTypeSV))
        return ipcOutOfRange ("gftt:", gfttype);
    return IpcGFTUTypeSV[gfttype];
}

static char sIPC_MAL_BASE[] = "MAL_BASE";
static char sIPC_MAL_USER_LOAD[] = "MAL_USER_LOAD";
static char sIPC_MAL_USER_SAVE[] = "MAL_USER_SAVE";
static char sIPC_MAL_CMX_LOAD[] = "MAL_CMX_LOAD";
static char sIPC_MAL_CMX_SAVE[] = "MAL_CMX_SAVE";
static char sIPC_MAL_CMX_LOAD_REPLY[] = "MAL_CMX_LOAD_REPLY";

static char * IpcMALUTypeSV[] = 
{
    sIPC_MAL_BASE,
    sIPC_MAL_USER_LOAD,
    sIPC_MAL_USER_SAVE,
    sIPC_MAL_CMX_LOAD,
    sIPC_MAL_CMX_SAVE,
    sIPC_MAL_CMX_LOAD_REPLY
};

static char *
ipc_UserMALString (unsigned int maltype)
{
    if (maltype >= MAXINDEX (IpcMALUTypeSV))
        return ipcOutOfRange ("malt:", maltype);
    return IpcMALUTypeSV[maltype];
}

static char sIPC_APP_SESSION_INCEPTION[] = "APP_SESSION_INCEPTION";
static char sIPC_APP_SESSION_DESTRUCTION[] = "APP_SESSION_DESTRUCTION";
static char sIPC_APP_SERVICE_SUSPEND[] = "APP_SERVICE_SUSPEND";
static char sIPC_APP_SERVICE_RESUME[] = "APP_SERVICE_RESUME";
static char sIPC_APP_RUNTIME_TERMINATE[] = "APP_RUNTIME_TERMINATE";
static char sIPC_APP_USER_EVENT[] = "APP_USER_EVENT";
static char sIPC_APP_NOTIFY[] = "APP_NOTIFY";
static char sIPC_APP_PURCHASE_AUTHORIZED[] = "APP_PURCHASE_AUTHORIZED";
static char sIPC_APP_PURCHASE_CONFIRMED[] = "APP_PURCHASE_CONFIRMED";
static char sIPC_APP_PURCHASE_CAANCELLED[] = "APP_PURCHASE_CAANCELLED";

static char * IpcAPPUTypeSV[] = 
{
    sIPC_APP_SESSION_INCEPTION,
    sIPC_APP_SESSION_DESTRUCTION,
    sIPC_APP_SERVICE_SUSPEND,
    sIPC_APP_SERVICE_RESUME,
    sIPC_APP_RUNTIME_TERMINATE,
    sIPC_APP_USER_EVENT,
    sIPC_APP_NOTIFY,
    sIPC_APP_PURCHASE_AUTHORIZED,
    sIPC_APP_PURCHASE_CONFIRMED,
    sIPC_APP_PURCHASE_CAANCELLED
};


static char *
ipc_UserAPPString (unsigned int apptype)
{
    if (apptype >= MAXINDEX (IpcAPPUTypeSV))
        return ipcOutOfRange ("appt:", apptype);
    return IpcAPPUTypeSV[apptype];
}


static char sIPC_ISM_HANDSHAKE[] = "ISM_HANDSHAKE";
static char sIPC_ISM_INITIALIZE[] = "ISM_INITIALIZE";
static char sIPC_ISM_ENABLE_VIDEO[] = "ISM_ENABLE_VIDEO";
static char sIPC_ISM_DISABLE_VIDEO[] = "ISM_DISABLE_VIDEO";
static char sIPC_ISM_STATUS[] = "ISM_STATUS";
static char sIPC_ISM_CONFIGURE[] = "ISM_CONFIGURE";

static char * IpcISMUTypeSV[] = 
{
    sIPC_ISM_HANDSHAKE,
    sIPC_ISM_INITIALIZE,
    sIPC_ISM_ENABLE_VIDEO,
    sIPC_ISM_DISABLE_VIDEO,
    sIPC_ISM_STATUS,
    sIPC_ISM_CONFIGURE
};

static char *
ipc_UserISMString (unsigned int ismtype)
{
    if (ismtype >= MAXINDEX (IpcISMUTypeSV))
        return ipcOutOfRange ("ismt:", ismtype);
    return IpcISMUTypeSV[ismtype];
}

static char *
ipc_UserOutOfRange (unsigned int utype)
{
    return ipcOutOfRange ("User:", utype);
}

typedef char * (* Tcpfp) (unsigned int a);

Tcpfp IpcTPFPV[] =
{
    ipc_UserSysString,
    ipc_UserCMXString,
    ipc_UserAVGSMString,
    ipc_UserVDCPString,
    ipc_UserDVPString,
    ipc_UserVGLString,
    ipc_UserADCPString,
    ipc_UserDAPString,
    ipc_UserSETString,
    ipc_UserCDSLString,
    ipc_UserDBString,
    ipc_UserAVSTRString,
    ipc_UserGFTString,
    ipc_UserMALString,
    ipc_UserAPPString,
    ipc_UserISMString,
    ipc_UserOutOfRange
};



char *
ipc_UserTypeString (unsigned int utype)
{
    unsigned int i;

    i = (utype >> IPC_RANGE_SHIFT);
    if (i >= MAXINDEX (IpcTPFPV))
        return ipcOutOfRange ("User:", utype);
    return IpcTPFPV[i] (utype & ((1 << IPC_RANGE_SHIFT) - 1));
}



