
/*
** COPYRIGHT (C) 1994, ICTV Inc. 280 Martin Ave. Santa Clara, Calif 95050. 280
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television
**
** File: sighandle.h - local sighandler interface registration interface.
**
** Purpose:
**  To provide an interface that allows a system task to use ipc and other
**  ictv interfaces and still received signal.
**
** Types/Clases:
**  SignalHandler -- type defining a signal handler.
**
** Functions:
**  SignalHandler sigHandlerSet (SinganlHandler) --
**
** Notes:
**  In order to allow the ipc system to get alarm signals (this may be extended
**  to other ictv interfaces) and still allow tasks to receive other signals
**  if need be this interface must be used to handle signal.
**  The user should see the IPC documentation for use of signals with
**  IPC.
**
** Usage:
**
** $Log: sighandle.c,v $
** Revision 1.4  1994/08/15 18:46:31  emil
** Intergrated changes in bruces and my code.
**
 * Revision 1.3  1994/08/15  14:59:47  emil
 * A bunch of changes to use the memlib facilty.  Plus I removed tabs from all.
 *
 * Revision 1.2  1994/08/02  21:33:08  emil
 * Corrected mem_alloc to mem_malloc in ProcessLock.
 * Added define __FILENAME_C__ for all C files.
 * Removed sigsend.
 *
 * Revision 1.1.1.1  1994/07/08  23:50:36  emil
 * OS9KExt creation, should include ipc, sighandle, Lock, and other 9K
 * extensions.
 *
 * Revision 1.1.1.1  1994/03/25  19:19:59  emil
 * Creating this.
 *
**
*/

# define __SIGHANDLE_C__
# include "ictvcommon.h"
# include "sighandle.h"

static SignalHandler currentHandler;

/* Public sighandle interface */

/* sigHandlerSet sets the current OS signal intercept function.
** it returns the current (i.e. last set by this call) signal handler.
** If the task wishes to be compatible with other ICTV libraries, it
** must use this call to set its signal handler. 
** Passing Null into this procedure disable signal handling.
*/
SignalHandler
sigHandlerSet (SignalHandler handler)
{
    SignalHandler tshp;
    
    tshp = currentHandler;
    currentHandler = handler;
    intercept (handler);
    return tshp;
}

