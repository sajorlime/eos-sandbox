
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Project: ictv interactive television using OS9K
**
** File: ~dev/ICTV/libsrc/ipc.c - inter-process communications
**
** Author: emil
** $Author: wul $
**
** Purpose:
**  This module provides the basic cabilbily to send messages between OS9K
**  tasks.  It is intended and only capble of intertask communication. This
**  code is designed such that it limits each OS9K task to one mail box.
**
** Types/Clases:
**   LocalIpcRoot -- structure to hold the local root info for the library.
**
** Data
**  IpcLocalRoot -- the root structure for ipc for the calling task.
**
** Functions:
**  void ipc_MailBoxCreate -- construct access to IPCs.
**
**  void ipc_MailBoxDestroy  -- destruct access to IPCs.
**
**  MailHandle ipc_GetMailHandle  -- get access to a mail box.
**  int ipc_Flush  -- flush messages in my mail queue.
**
**  int ipc_FreeBuffer  -- release an IPC buffer.
**
**  IpcB * ipc_NewBuffer  -- acquire an IPC buffer.
**
**  IpcB *ipc_Rcv  -- default call to receive a message.
**
**  IpcB * ipc_Receive  -- call to get a qualifed message.
**
**  int ipc_Reply  -- reply to send of the origonal IPC.
**
**  void ipc_SendSync  -- syncronous message send.
**
**  int ipc_Send  -- send a message to a mailbox.
**
**  IpcPriority ipc_GetPriority  -- get the priority of a message.
**
**  void ipc_SetPriority  -- set the priority of a message.
**
**  MailHandle ipc_GetSender  -- get the sender of a message
**
**  size_t ipc_GetMaxSize  -- get the size of a message
**
**  size_t ipc_GetMsgLen  -- get the size of a message
**
**  void ipc_SetMsgLen  -- get the size of a message
**
**  IpcType ipc_GetType  -- get the IPC type of a message
**
**  int ipc_GetUserType  -- get the User type of a message.
**
**  void ipc_SetUserType  -- set the user type of a message.
**
**  void * ipc_GetBp  -- get a pointer to the user writable
**  part of the IPC structure.
**
**  void ipc_SetRcvTimeout  -- set the timeout used by the
**  ipc_Rcv, and ipc_SendSync calls.
**
** Notes:
**   Limitations: a substantial assumption is made that this is a fully
**   integrated system and therefore all process's are required and a failure
**   of any process will bring down the system which includes this library
**   and its shared memory segment.  This effectively resets all the ipc
**   pointers. Therefore, this function attempts limited cleanup.
**   Additional work such as flushing the receive list,
**   clearing list event waits, and notifying other waiting processes
**   might be useful if this assumption were not true and a single process
**   fails.
**
** Overview:
**  This code is intended to provide the basis for interprocess communication
**  on ICTV OS9000 based systems.  Abstractly the ipc interface implements
**  two classes, one for ipc messeages which have a friend in the MailBox class.
**
**  The library hides both mail box and the mechanism for accessing it.  Each
**  part of the library hides a local root structure that allows it to identify
**  the sender of a message. The local root is intialized by the MailBoxCreate
**  call, which therefore must be called before another other calls to this library.
**
** $Id: ipc.c,v 1.33 1995/11/04 02:03:37 wul Exp $
**
*/

# include <alarm.h>
# include <assert.h>
# include <errno.h>
# include <memory.h>
# include <modes.h>
# include <module.h>
# include <process.h>
# include <signal.h>
# include <stddef.h>
# include <stdlib.h>
# include <stdio.h>
# include <sysglob.h>
# include <types.h>
# include <string.h>

# include "ipc.h"
# include "siguser.h"
# include "ProcessLock.h"
# include "k9misc.h"


# define __IPC_C__

# define IPC_FATAL_CHECK (-1)

/* Internal functions */
int ipc_BaseSend (IpcB * ipcp, MailHandle target);
IpcB * ipc_BaseRcv (long msdelay);

static
struct _LocalIpcRoot /* shared memory control header */
{
    MailHandle this;    /* mailbox of this process */
    IpcMem * mp;    /* shared memory ipc header */
    mh_data * mod_head;
    long int msDelay;
    int errno; /* if any */
} IpcLocalRoot = {0};

typedef struct _LocalIpcRoot LIpcR;


void
ipc_setup (IpcMem * ipcmp)
{
    IpcLocalRoot.mp = ipcmp;
}

void
osBlock (void)
{
    _os_setpr (IpcLocalRoot.this->procID, PRI_BLOCK);
}

void
osUnblock (void)
{
    _os_setpr (IpcLocalRoot.this->procID, IpcLocalRoot.this->procPri);
}

# define FATAL_PARAMS(num,txt) num, txt, __FILE__, __LINE__


u_int16
ipcGetMaxAge (void)
{
    glob_buff t;

    _os_getsys (offsetof (sysglobs, d_maxage), sizeof (u_int16), &t);
    return t.wrd;
}

typedef struct _K9ProcessStuff
{

    u_int16 pri;
    u_int16 age;
    u_int16 group;
    u_int16 user;
    int32 sched;
} K9ProcessStuff;

static K9ProcessStuff K9Stuff;

process_id
ipcGetStuff (K9ProcessStuff * stuff)
{
    process_id pid;
    glob_buff t;
    error_code ec;

    ec = _os_id
        (
            &pid, &stuff->pri, &stuff->age, &stuff->sched, &stuff->group,
            &stuff->user
        );
    return pid;
}

ipcPrintK9Stuff (process_id procId, u_int16 maxage)
{
    K9_MSG
    (
        (
            "pid:%d, pri:%x, age:%x, maxage:%x -- sched:%x, group:%x, user:%x\n",
            procId, K9Stuff.pri, K9Stuff.age, maxage, K9Stuff.sched,
            K9Stuff.group, K9Stuff.user
        ) 
    );
}

/*
** This function exists for debugging purposes only
*/
int
ipcReportPri (int print)
{
    process_id procID;
    u_int16 pri, age, group, user;
    int32 sched;
    u_int16 maxage;
    glob_buff t;

    procID = ipcGetStuff (&K9Stuff);
    if (print)
        ipcPrintK9Stuff (procID, ipcGetMaxAge ());
    return K9Stuff.pri;
}


/* for debug only, because k9 is broken */
void
ipcFatalLeave (struct _LocalIpcRoot * lipcrp, char * cp)
{
    u_int32 ticks;
    u_int32 sig;

    ticks = 200;
    _os_sleep (&ticks, &sig);
    _os_sysdbg (lipcrp, cp);
}



static int IpcInFatal = 0;
/*
** This function exists for debugging purposes only
*/
void
ipcFatalError (int num, char * file, char * txt, int line)
{
    u_int32 eticks = 1, esig = 0;
    MailHandle this = IpcLocalRoot.this;

    if (IpcInFatal)
    {
        K9_ASSERT
        ((
            "Rentrant FATAL ERROR in %s/%s at line %d in file  %s,  errno = %d\n",
            file, this ? this->name : "local this null", line, txt, num
        ));
        ipcFatalLeave (&IpcLocalRoot, "Reentrant ipcFatalError");
        while (IpcInFatal)
        {
            ipcReportPri (0);
            eticks = 1000;
            _os_sleep (&eticks, &esig);
        }
    }
    IpcInFatal = 1;
    K9_ASSERT
    ((
        "FATAL ERROR in %s/%s at line %d in file  %s,  errno = %d\n",
        file, this ? this->name : "local this null", line, txt, num
    ));
    ipcReportPri (1);
    _os_sleep (&eticks, &esig);
    ipcFatalLeave (&IpcLocalRoot, txt);
}




/*
** This function is public for testing purposes only, applications should
** never call it! It is also available for rpcServer.
*/
IpcMem *
ipc_GetRoot (void)
{
    return IpcLocalRoot.mp;
}

/*
** This function exists for rpcServer.
*/
MailHandle
ipc_GetThis (void)
{
    return IpcLocalRoot.this;
}


int
ipc_GetErrno (void)
{
    int err = IpcLocalRoot.errno;

    IpcLocalRoot.errno = IPCERR_NONE;
    return err;
}

int
ipc_SetErrno (int err)
{
    IpcLocalRoot.errno = err;
}

int
ipcfriend_getProcId (void)
{
    return IpcLocalRoot.this->procID;
}


char *
ipcfriend_getMailName (void)
{
    return IpcLocalRoot.this->name;
}


int
ipcfriend_getProcPri (void)
{
    return IpcLocalRoot.this->procPri;
}


/* Macros to be inserted in the code to provide validation */
#define VALIDATE_IPC(ipcp)                      \
    assert( (char*)ipc>=IpcLocalRoot.mp->memFreeStart && \
           (char*)ipc<IpcLocalRoot.mp->memFreeEnd );    \
    assert (ipcp->sanity1 == SANITY1);        \
    assert (ipcp->sanity2 == SANITY2);

#define VALIDATE_MAILBOX(mbox)                 \
    assert (((mbox) >= IpcLocalRoot.mp->mailboxes &&       \
            (mbox) <  IpcLocalRoot.mp->mailboxes + IpcLocalRoot.mp->maxMailboxes));



/*
**  ipc_exit - exit cleanup called via atexit()
**  returns: none
*/
static void 
ipc_exit (void)
{
    ipc_MailBoxDestroy (IpcLocalRoot.this); /* process no longer exists */
}



/* Magic sauce to get the process ID */
static process_id
ipc_GetProcID (void)
{
    process_id procID;
    u_int16 pri, age, group, user;
    int32 sched;

    _os_id (&procID, &pri, &age, &sched, &group, &user);
    return procID;
}

extern int glob_link (void);
/*
**  ipc_MailBoxCreate - initialize the ipc library
**     procName - string naming the process/mailbox. maximum 8 characters
**     returns: none
*/
MailHandle 
ipc_MailBoxCreate (char * procName, Priority pri, int useNormalSig, int usePrioritySig)
{
    MailBox * mbp;
    int i;
    int l;
    process_id procID;
    char dummy;
    IpcMem * ipcmp;
    u_int16 procPri;
    u_int16 procProc;

    /* The external system must provide a call to glob link.  This call
    ** must set the value of IpcLocalRoot! */
    for (i = 0; (i < 10) && glob_link (); i++)
        sleep (1);
    for (; (i < 10) && !IpcLocalRoot.mp; i++)
        sleep (1);
    if (!IpcLocalRoot.mp)
    {
        K9_ASSERT (("IpcLocal.mp not set (call glob_init)\n"));
        return (MailHandle) 0;
    }
    atexit (ipc_exit ); /* exit cleanup function */
    IpcLocalRoot.this = 0;
    IpcLocalRoot.msDelay = -1; 
    procID = ipc_GetProcID ();
    procPri = PRIORITY (pri);
    if (!procName) /* Utilities exit at this point */
    {
        _os_setpr (procID, procPri);
        return 0;
    }
    _os_setpr (procID, PRI_BLOCK);
    ipcmp = IpcLocalRoot.mp;
    if (ipcmp != ipcmp->self)
    {
        K9_ASSERT
        ((
            "Error mail box struct incorrect or uninitialized"
            " ipcmp:%x != ipcmp->self:%x\n", procName
        ));
        return (MailHandle) 0;
    }

    if (mbp = ipc_GetMailHandle (procName))
    {
        /* A handle already exists by this name, this should only happen
        ** if the process has died
        ** but we somehow failed to clean up and reused this handle.
        ** Because OS9K reuses process ID's we have either the choice
        ** of trying to reuse this box or making this fatal.  To make
        ** it non-fatal define REUSEIPC */
# define xREUSEIPC
# ifdef REUSEIPC
        mbp->procID = procID;
        mbp->procPri = procPri;
        mbp->useNormalSig = useNormalSig;
        mbp->usePrioritySig = usePrioritySig;
        mbp->swaiting = 0;
        mbp->twaiting = IPC_TYPE_ANY;
        mbp->rootp = &IpcLocalRoot;
        ipc_Flush (0);
        K9_WARN (("Warning mail box %s not clear -- reusing", procName));
        _os_setpr (procID, procPri);
        return mbp;
# else
        IpcLocalRoot.errno = IPCERR_DUPNAME;
        
        K9_ASSERT (("Warning! Duplicate mailbox name error!\n"));
        return 0;
# endif
    }
    /* use brute force here, we should not be doing it often! */
    for (i = 0, mbp = ipcmp->mailboxes; i < ipcmp->maxMailboxes; i++, mbp++)
    {
        if (!(mbp->rootp))
        {
            IpcLocalRoot.this = mbp;    /* save to load with new procName */
            ipcmp->inuseMailboxes++;    /* using one more mailbox */
            break;
        }
    }
    if (!IpcLocalRoot.this)
        ipcFatalError (FATAL_PARAMS (IPC_FATAL_CHECK, "mailbox procName full"));

    /* found empty mailbox slot, load for this process */
    mbp->procID = procID;
    mbp->procPri = procPri;

    mbp->useNormalSig = useNormalSig;
    mbp->usePrioritySig = usePrioritySig;
    mbp->head = mbp->tail = 0;
    mbp->swaiting = 0;
    mbp->twaiting = IPC_TYPE_ANY;
    mbp->msg = 0;
    mbp->rootp = &IpcLocalRoot;
    strncpy (mbp->name, procName, sizeof (mbp->name));
    mbp->name[sizeof (mbp->name) - 1] = 0; /* force null terminator */

    _os_setpr (procID, procPri);
    return mbp;
}



/*
** ipc_MailBoxDestroy - cleanup everything that was setup
**   returns: none
*/
void 
ipc_MailBoxDestroy (MailHandle mh)
{
    error_code err;

    if ((mh == IpcLocalRoot.this) && mh) /* normal self destroy */
    {
        ipc_Flush (0);
        IpcLocalRoot.this = 0;
        mh->rootp = 0;
        IpcLocalRoot.mp->inuseMailboxes--;  /* using one less mailbox */
    }
    if (!mh) /* we come here for untilites */
        return;

    if (!mh->rootp)
        return;
    /* Destroying an other tasks handle */
    /* So kill the task too! */
    if (!mh->procID)
        return;
    err = _os_send (mh->procID, SIGQUIT);
    if (!err)
    {
        signal_code sig;
        u_int32 ticks = 100;

        _os_sleep (&ticks, &sig); /* wait a while and make sure it's dead */
    }
    if (mh->rootp)
    {
        mh->rootp = 0;
        if (mh->procID)
            _os_send (mh->procID, SIGKILL); /* really kill him */
    }
}



/*
** ipc_GetMailHandle - given a process name, find its associated mailbox
**    procName - mailbox/process name as registered with ipc_MailBoxCreate()
**    returns: internal mailbox structure or NULL if not found
** This routine does this in a brute force manor with the assumption that
** this will not be done often.  The user should call this once for each 
** task it intends to call and save the value.
** If procName is null it returns IpcLocalRoot.this.
*/
MailHandle
ipc_GetMailHandle (char * procName)
{
    MailBox * mbp;
    int i;

    if (!procName)
        return IpcLocalRoot.this;
    for
    (
        mbp = IpcLocalRoot.mp->mailboxes, i = 0;
        i < IpcLocalRoot.mp->maxMailboxes;
        i++, mbp++
    )
    {
        if (mbp->rootp)
            if (strcmp (procName, mbp->name) == 0)  /* check the name */
                return mbp; /* found matching name */
    }
    return NULL; /* name not found */
}

# define xIPC_CHECK_COUNT
# ifdef IPC_CHECK_COUNT
void
ipc_checkIpcCount (void)
{
    IpcB * tipcp;
    int i;

    if (!(tipcp = IpcLocalRoot.mp->ipcFreeList))
        K9_MSG (("!just ran out of buffers\n"));
    for (i = 1; i < 4; i++)
        if (!(tipcp = tipcp->next))
            break;
    if (i < 4)
        K9_MSG (("!about to run out of buffers\n"));
}
# endif


/*
** ipc_FreeBuffer - release a buffer
**    ipc - ipc address as returned from ipc_GetBuffer()
**    returns: none
*/
int 
ipc_FreeBuffer (IpcB * ipcp)
{
    IpcMem * ipcmp;
    u_int32 pflags;

    if (!ipcp)
    {
        K9_MSG (("Null IPC freed\n"));
        return IpcLocalRoot.errno = IPCERR_NULL_BUFFER;
    }
# ifdef IPC_DEBUG
    if (ipcp != ipcp->self)
    {
        K9_MSG
        ((
            "IPC Buffer is not valid.  ipcp(%x) != ipcp->self(%x)\n",
            ipcp, ipcp->self
        ));
        return IpcLocalRoot.errno = IPCERR_ILLEGAL_BUFFER;
    }
    if (IPC_IS_UNUSED (ipcp))
        K9_ASSERT (("IPC Buffer is unused\n"));
    if (ipcp->next)
    {
        K9_ASSERT (("IPC Buffer is already free, or on list -- must fix!\n"));
        K9_MSG
        ((
            "IpcB:%x->{ty:%s uty:%s, snd:%s->tgt:%s, nxt:%s}\n",
            ipcp, ipc_TypeString  (ipcp->type),
            ipc_UserTypeString (ipcp->userType),
            ipcp->sender ? ipcp->sender->name : "null sender",
            ipcp->target ? ipcp->target->name : " null target",
            ipcp->next ? "next is set" : " next is null"
        ));
        if (ipcp->userType == IPC_TYPE_UNUSED)
        {
            ipcFatalLeave (&IpcLocalRoot, "unused buffer being freed");
        }
        return IpcLocalRoot.errno = IPCERR_UNALLOCATED_BUFFER;
    }
# endif
    ipcmp = IpcLocalRoot.mp;
    pflags = irq_change (0);
# ifdef IPC_CHECK_COUNT
    ipc_checkIpcCount ();
# endif
    ipcp->next = ipcmp->ipcFreeList;
    ipcmp->ipcFreeList = ipcp;
    /* or in value to the type fields so that we id them is unused but
    ** remember what they were */
    ipcp->type |= IPC_TYPE_UNUSED;

    irq_change (pflags);
    return IPCERR_NONE;
}

/*
** ipc_NewBuffer - allocate a buffer
**    Notes: fatal exit if no memory available,
**           sets ipc.priority = IPC_PRI_NORMAL
**    returns: address of ipc buffer
*/
IpcB *
ipc_NewBuffer (void)
{
    IpcB * ipcp;
    IpcMem * ipcmp;
    u_int32 pflags;

    ipcmp = IpcLocalRoot.mp;
    pflags = irq_change (0);
    /* 99.99% of the time we will not enter the the wait or wake blocks */
    ipcp = ipcmp->ipcFreeList;
    if (!ipcp)
    {
        ipcFatalError (FATAL_PARAMS (-1, "NewBuffer: no ipcs"));/* out of memory */
    }
    ipcmp->ipcFreeList = ipcp->next;
# ifdef IPC_CHECK_COUNT
    ipc_checkIpcCount ();
# endif
    ipcp->next = 0;
    ipcp->mnum = ipcmp->mnum++;
    irq_change (pflags);
    ipcp->priority = IPC_PRI_NORMAL;

    return ipcp;
}

# define MSTOTICKS(ms) ((ms) >> 2)
/* the min wait ticks are defined as 2 because this is the minimum OS9K allows
** to be remotely meaningful. */
# define MINWAITTICK 2
# define TICK256FLAG (1 << 31)

static u_int32
ipc_MStoTicks (long ms)
{
    long ticks;

    if (ms <= 0)
        return 0; /* this is the sleep forever value */
    ticks = MSTOTICKS (ms);
    if (ticks < MINWAITTICK)
        return MINWAITTICK;
    return ticks | TICK256FLAG;
}

/*
** ipc_RcvMsDelay -- provide an interface that allows the task to wait
** a maximum time in miliseconds.
*/
IpcB *
ipc_RcvMSDelay (long msDelay)
{
  return ipc_BaseRcv (msDelay);
}

/*
** ipc_BaseRcv -- provide an interface that allows the task to wait
** a maximum time in miliseconds.  It called by ipc_Rcv for all calls,
** and by ipc_Receive when the receive is not conditioned. 
*/
IpcB*
ipc_BaseRcv (long msDelay)
{
    IpcB * ipcp;
    MailBox * thisp;
    signal_code sig;
    u_int32 ticks;
    u_int32 eflags;

    thisp = IpcLocalRoot.this;
    if (!thisp)
    {
        ipcFatalError (FATAL_PARAMS (-1, "Bad IpcLocalRoot"));
    }
    ticks = ipc_MStoTicks (msDelay);
    eflags = irq_change (0);
# ifdef IPC_CHECK_COUNT
    ipc_checkIpcCount ();
# endif
    ipcp = thisp->head;
    if (!ipcp)
    {
        if (msDelay == 0)
        {
            eflags = irq_change (eflags);
            return ipcp; /* this is null */
        }
        /* Note if ticks != 0 it is now in 1/256 of a second */
        /* The waiting must be set while the task is asleep waiting
        ** for a message, and cleared otherwise */
        thisp->swaiting = thisp;
        thisp->twaiting = IPC_TYPE_ANY;


        /* Note priority must be reset by waker */
        _os_sleep (&ticks, &sig);

        ipcp = thisp->head;
        if (!ipcp) /* We woke up for some other reason, a sig or timeout? */
        {
            thisp->swaiting = 0;
            eflags = irq_change (eflags);
            return ipcp; /* we have a timeout, so return Null */
        }
    }
    if ((thisp->head = ipcp->next) == 0)
    {
        thisp->tail = 0; /* the list is empty */
    }
    ipcp->next = 0;
    thisp->swaiting = 0; /* clear waiting before releasing the mail box */
    thisp->twaiting = IPC_TYPE_ANY;
# ifdef IPC_CHECK_COUNT
    ipc_checkIpcCount ();
# endif
    eflags = irq_change (eflags);
    return ipcp;
}

/*
** ipc_Rcv - retrieve next ipc for the process.
** This is the default call for most tasks.  They typically will not set
** the msDelay time, which defaults to forever.  Causing them to wait
** until the received some sort of message.
*/
IpcB *
ipc_Rcv (void)
{
    return ipc_BaseRcv (IpcLocalRoot.msDelay);
}



/*
** IPCSEARCH -- macro the defines static functions to delete
** message on a condition.
*/
# define IPCSEARCH(CONDITION)                   \
    IpcB * ipcp;                                \
    IpcB * lastp;                               \
    IpcB * nextp;                               \
                                                \
    ipcp = mbp->head;                           \
    if (!ipcp)                                  \
        return ipcp;                            \
    if CONDITION                                \
    {                                           \
        if ((mbp->head = ipcp->next) == 0)      \
            mbp->tail = 0;                      \
        ipcp->next = 0;                         \
        return ipcp;                            \
    }                                           \
    lastp = ipcp;                               \
    while ((ipcp = ipcp->next) != 0)            \
    {                                           \
        if CONDITION                            \
        {                                       \
            nextp = ipcp->next;                 \
            lastp->next = nextp;                \
            if (!nextp)                         \
            {                                   \
                mbp->tail = lastp;              \
            }                                   \
            ipcp->next = 0;                     \
            return ipcp;                        \
        }                                       \
        lastp = ipcp;                           \
    }                                           \
/* End of IPCSEARCH */


/*
** ipc_SearchType, ipc_SearchSender, ipc_SearhTS are all private functions
** to search and return a matching condition for ipc_Receive.
*/
static IpcB *
ipc_SearchType (MailBox * mbp, IpcType type)
{
    IPCSEARCH ((ipcp->type == type))
    return ipcp;
}


static IpcB *
ipc_SearchSender (MailBox * mbp, MailHandle sender)
{
    IPCSEARCH ((ipcp->sender == sender))
    return ipcp;
}



static IpcB *
ipc_SearchTS (MailBox * mbp, MailHandle sender, IpcType type)
{
    IPCSEARCH (((ipcp->type == type) && (ipcp->sender == sender)))
    return ipcp;
}






/*
** ipc_Receive - retrieve next ipc for the process
**    sender - retrieve ipcs sent by this originator.  If value is NULL or
**             pointer is NULL, retrieve ipcs from any originator.
**             If pointer not NULL returns mailbox of originator.
**    type   - retrieve ipcs of this type. Same rules as 'sender'.
**    msDelay  - rule if no ipcs match retrieval criteria.
**             0 = return immediately if no ipcs match criteria.
**             <0  = wait forever until an ipc matches the criteria.`
**             >0 = wait specified number of 256ths of a second for
**                  matching ipc before returning.
**    returns: address of ipc buffer or NULL if no ipcs
**
** Note: This routine was a bitch.  I had to to rely on the ipc_BaseSend
** routine note waking a task unless it is waiting for the exact message
** to make it work.  It still needs to search down the list of messages
** to have the just incerted message after it wakes up.  Doing otherwise, 
** like having forward incert at the beging of the list made it all the more
** complex.  If this code is barrowed for another project one should
** consider discarding the capability of
** looking for a particular sender and/or type in favor a simpler sychonous
** method.
*/
IpcB *
ipc_Receive (MailHandle sender, IpcType type, long msDelay)
{
    IpcB * ipcp;
    int err;
    signal_code sig;
    u_int32 ticks;
    MailBox * mbp;
    u_int32 eflags;

    mbp = IpcLocalRoot.this;

    /* Only allow retrieve on known types */
    switch (type)
    {
    case IPC_TYPE_ANY:
    case IPC_TYPE_REQUEST:
    case IPC_TYPE_REPLY:
    case IPC_TYPE_SYNC_REQUEST:
    case IPC_TYPE_SYNC_REPLY:
    case RPC_TYPE_REQUEST:
    case RPC_TYPE_REPLY:
    case RPC_TYPE_SYNC_REQUEST:
    case RPC_TYPE_SYNC_REPLY:
    case IPC_TYPE_RENDEZVOUS:
    case IPC_TYPE_RENDEZVOUS_REPLY:
        break;
    default:
        IpcLocalRoot.errno = IPCERR_UNKNOWN_REPLY_TYPE;
        type = IPC_TYPE_ANY;
        break;
    }
    /* If this routine was called without conditions call the simpler routine */
    if (!(sender || type))
        return ipc_BaseRcv (msDelay);

    eflags = irq_change (0);
    /* Setting twaiting and swaiting should prevent a wake-up for other than
    ** for this condition. Therefore, if there is no message now.  We will
    ** go to sleep once and return the first message on the queue if we
    ** woken.  The enqueue code is responsible for incerting at the front
    ** of the queue if other messages have arrived. */
    mbp->twaiting = type;
    if (sender)
        mbp->swaiting = sender;
    else
        mbp->swaiting = mbp;

    sig = -1; /* -1 is treated as a flag below to indicate that we were woken or timed out */
    if (msDelay == 0)
        sig = 0; /* cause the sig value to indicate that we should look only once */
    for (; ; )
    {
        ipcp = mbp->head;
        if (ipcp)
        {
            if (ipcp->priority != IPC_PRI_NORMAL)
            {
                /* If he has received a high priority message deliver it
                ** regardless of what he is waiting for */
                if ((mbp->head = ipcp->next) == 0)
                    mbp->tail = 0;
                ipcp->next = 0;
                break;
            }
            else
            {
                /* if it was't priority we need to find the right type */
                ipcp = 0;
                if (type != IPC_TYPE_ANY)
                {
                    if (sender) /* both are true */
                    {
                        ipcp = ipc_SearchTS (mbp, sender, type);
                    }
                    else /* only type is true */
                    {
                        ipcp = ipc_SearchType (mbp, type);
                    }
                }
                else /* then sender must be the only one true */
                {
                    ipcp = ipc_SearchSender (mbp, sender);
                }
            }
            /* If we have an IPC at this point we were woken */
            if (ipcp)
            {
                ipcp->next = 0;
                break;
            }
        }
        if (sig == 0) /* We were woken by timing out on the last sleep */
        {
            break;
        }
        ticks = ipc_MStoTicks (msDelay);
        _os_sleep (&ticks, &sig);
    }
    mbp->swaiting = 0;
    mbp->twaiting = IPC_TYPE_ANY;
    irq_change (eflags);
    return ipcp;
}



/*
** ipc_Flush - flush all pending ipc's that match criteria
**    sender - if NULL flush ipcs regardless of originator,
**             if not NULL only flush ipcs from specified originator
**    returns: the number of messages flushed.
*/
int 
ipc_Flush (MailHandle sender)
{
    IpcB * ipcp;
    int i;
    MailBox * mbp;
    u_int32 eflags;

    mbp = IpcLocalRoot.this;
    if (!mbp->head) /* this is here so that tools don't need to lock! */
        return 0;

    i = 0;
    if (sender)
    {
        eflags = irq_change (0);
        while ((ipcp = ipc_SearchSender (mbp, sender)) != 0)
        {
            irq_change (eflags);
            ipc_FreeBuffer (ipcp); /* release to free pool */
            i++;
        }
        irq_change (eflags);
    }
    else
    {
        while ((ipcp = ipc_BaseRcv (0)) != 0)
        {
            ipc_FreeBuffer (ipcp); /* release to free pool */
            i++;
        }
    }
    return i;
}



/*
** ipc_Reply - sends the ipc obtained from ipc_Receive() back to the originator
**    ipc  - ipc buffer from ipc_GetBuffer()
**    returns: what ever ipc_Send sends
*/
int 
ipc_Reply (IpcB * ipcp)
{
    MailHandle mh;

    switch (ipcp->type)
    {
    case IPC_TYPE_REQUEST:
        ipcp->type = IPC_TYPE_REPLY;
        break;
    case IPC_TYPE_SYNC_REQUEST:
        ipcp->type = IPC_TYPE_SYNC_REPLY;
        break;
    case RPC_TYPE_REQUEST:
        ipcp->type = RPC_TYPE_REPLY;
        break;
    case RPC_TYPE_SYNC_REQUEST:
        ipcp->type = RPC_TYPE_SYNC_REPLY;
        break;
    case IPC_TYPE_RENDEZVOUS:
        ipcp->type = IPC_TYPE_RENDEZVOUS_REPLY;
        break;
    case IPC_TYPE_SYNC_DISPOSE:
        ipc_FreeBuffer (ipcp);
        return IPCERR_NONE;
    default:
        ipc_FreeBuffer (ipcp);
        return IpcLocalRoot.errno = IPCERR_UNKNOWN_REPLY_TYPE;
    }
    mh = ipcp->sender;
    ipcp->mnum = -ipcp->mnum; /* BaseSend redoes this */
    ipcp->sender = IpcLocalRoot.this; /* our mailbox */
    return ipc_BaseSend (ipcp, mh);   /* send back to sender */
}


/*
** ipc_Forward - sends the ipc to the target process asynchronously
**    ipc    - ipc buffer from ipc_GetBuffer()
**    target - mailbox to destination
**    returns: error status
*/
int
ipc_Forward (IpcB * ipcp, MailHandle target)
{
  return ipc_BaseSend (ipcp, target);
}

/*
** ipc_BaseSend - sends the ipc to the target process asynchronously
**    NOTE: PRIVATE FUNCTION. DO NOT CALL EXTERNALLY.
**    ipc    - ipc buffer from ipc_GetBuffer()
**    target - mailbox to destination
**    returns: error status
**
** Note:
**  See note at Receive regarding some of the strangeness here.  This
**  function cannot wake a task that is not waiting for the specific
**  condition because there is no way to tell the differenc between 
**  a SIGWAKE and a timeout.
*/
int 
ipc_BaseSend (IpcB * ipcp, MailHandle target)
{
    IpcB * tipcp;
    MailBox * mbp;
    MailHandle tmh;
    IpcType tty;
    u_int32 eflags;

    if (!ipcp || (ipcp != ipcp->self))
    {
        K9_ASSERT (("IPC Buffer null or bad, must fix!\n"));
        return IpcLocalRoot.errno = IPCERR_ILLEGAL_BUFFER;
    }
    if (IPC_IS_UNUSED (ipcp) || ipcp->next)
    {
        K9_ASSERT (("IPC Buffer is free or on list, must fix!\n"));
        K9_MSG
        ((
            "IpcB:%x->{ty:%s uty:%s, snd:%s->tgt:%s, nxt:%s}\n",
            ipcp, ipc_TypeString  (ipcp->type),
            ipc_UserTypeString (ipcp->userType),
            ipcp->sender ? ipcp->sender->name : " null sender ",
            ipcp->target ? ipcp->target->name : " null target ",
            ipcp->next ? "next is set" : " next is null"
        ));
        return IpcLocalRoot.errno = IPCERR_UNALLOCATED_BUFFER;
    }
# ifdef IPC_CHECK_COUNT
    ipc_checkIpcCount ();
# endif
    ipcp->mnum = -ipcp->mnum;
    mbp = IpcLocalRoot.this; /* our mailbox */
    ipcp->target = target; /* receipients mailbox */
    if (!target || !target->rootp)
    {
# ifdef IPC_DEBUG
        K9_MSG
        ((
            "Warning target for IpcB:%x->{%s, %s->%s, %s/%x} dead\n",
            ipcp, ipc_UserTypeString (ipcp->userType),
            mbp ? mbp->name : "No local.this", target ? target->name : "No target",
            ipcp->data, *((u_int *) ipcp->data)
        ));
# endif
        ipc_FreeBuffer (ipcp);
        return IpcLocalRoot.errno = IPCERR_ILLEGAL_MAILBOX;
    }
    eflags = irq_change (0);
    target->msg++; /* this is debug stuff */

    tipcp = target->tail;
    if (!tipcp) /* no messages are waiting */
    {
        target->head = target->tail = ipcp;
    }
    else
    {
        if (ipcp->priority == IPC_PRI_NORMAL)
        {   /* normal always gets appended */
            tipcp->next = ipcp;
            target->tail = ipcp;
        }
        else
        { /* this is a high priority ipc */
            /* Note that I do not try to insert at the end of a priority list.
            ** this is based on the meaning of a priority message */
            ipcp->next = target->head;
            target->head = ipcp;
        }
    }
    /* Now we decide if we wake it or not, if we wake disable rewaking
    ** it by setting swaiting to Null */

    tmh = target->swaiting;
    if (tmh != target) /* this test works if swaiting is Null, or -1 */
        if (tmh != mbp)
            goto BaseSend_RcvrNotWaitingForThisMessage;
    tty = target->twaiting;
    if (tty != IPC_TYPE_ANY)
        if (tty != ipcp->type)
            goto BaseSend_RcvrNotWaitingForThisMessage;

    /* If we get here we are going to wake it set swaiting to -1 so no one
    ** else will wake it, also set tmh as a flag */
    tmh = target->swaiting = (MailBox *) -1; /* mark it as woken */
    
    /* If target has greater priority, set our prioirty to his until we release
    ** this list, so that he doesn't go queue himself on the semaphore.
    ** Don't block other higher priority tasks from running though */

    if (ipcp->priority == IPC_PRI_NORMAL)
    {
        if (target->useNormalSig)
            _os_send (target->procID, SIG_IPC_PRI_MSG_RCVD);
        else
            _os_send (target->procID, SIGWAKE);
    }
    else
    {
        if (target->usePrioritySig)
            _os_send (target->procID, SIG_IPC_PRI_MSG_RCVD);
        else
            _os_send (target->procID, SIGWAKE);
    }

    irq_change (eflags);

    return IPCERR_NONE;

BaseSend_RcvrNotWaitingForThisMessage:

    /* Now we need only check if it reqires a signal */
    /* Note: pervious version had a check that we were
    ** not doing a complex wait, to prevent the process
    ** from getting woken when it wanted to wait for
    ** special message.  This failed for a process that
    ** wanted to be woken for a simple message, but
    ** had some other sleep call, or another signaler.
    ** Therefore, I have removed this test and always
    ** signal tasks that want a signal.  This means that
    ** problems may occur if someone tries to use signal
    ** and complex conditions at the some time. epr */
    if (ipcp->priority == IPC_PRI_NORMAL)
    { 
        if (target->useNormalSig)
        {
            /* mark it as woken */
            tmh = target->swaiting = (MailBox *) -1;
            _os_send (target->procID, SIG_IPC_MSG_RCVD);
        }
    }
    else
    { /* this is a high priority ipc */
        if (target->usePrioritySig)
        {
            /* mark it as woken */
            tmh = target->swaiting = (MailBox *) -1;
            _os_send (target->procID, SIG_IPC_PRI_MSG_RCVD);
        }
    }
    irq_change (eflags);

    return IPCERR_NONE;
}



/*
** ipc_Send - sends the ipc to the target process asynchronously
**    ipc    - ipc buffer from ipc_GetBuffer()
**    target - mailbox to destination
**    returns: error status
*/
int 
ipc_Send (IpcB * ipcp, MailHandle target)
{
    ipcp->sender = IpcLocalRoot.this; /* our mailbox */
    ipcp->type = IPC_TYPE_REQUEST;
    return ipc_BaseSend (ipcp, target);
}



/*
** ipc_SyncRoot
** ipc_SendSync
** rpc_SendSync
**  - sends the ipc to the target process and waits for the reply
**    ipc    - ipc buffer from ipc_GetBuffer()
**    target - mailbox of destination
**    returns: none
**
** Note:  SYNC_DISPOSE exists so that if a  timeout occurs, neither the
**  sender or receiver need concern itself with this message.  It
**  will be discarded.  Note that tasks wanting an interrupt for a
**  message will not receive one if they make a synchronous call.
**  The only reason for loosing a synchronous message should be a timeout
**  some other signal.
**  SyncRoot is a private function to IPC to compose rpc and ipc.
*/
IpcB * 
ipc_SyncRoot (IpcB * ipcp, MailHandle target, int type)
{
    int i;
    MailBox * mbp;
    IpcB * ripcp;

    ipcp->type = type;
    ipcp->sender = mbp = IpcLocalRoot.this; /* our mailbox */
# ifdef IPC_DEBUG
    if (!mbp)
    {
        K9_ASSERT (("n"));
        ipcFatalError
        (
            FATAL_PARAMS (IPC_FATAL_CHECK, "Bad Local Root, have you called glob?")
        );
    }
# endif
    if (ipc_BaseSend (ipcp, target)) /* send the ipc */
        return (IpcB *) 0;

    /* If the called process is at a lower priority we must raise its
    ** priortity until this message is processed, this avoids
    ** priortity inversion. */
    if (mbp->procPri > target->procPri)
        _os_setpr (target->procID, mbp->procPri);
    ripcp = ipc_Receive (0 /* target */, IPC_REQUEST_TO_REPLY (type), IpcLocalRoot.msDelay);
    if (mbp->procPri > target->procPri)
        _os_setpr (target->procID, target->procPri);
    if (!ripcp)
    {
        if (ipcp->type == type)
            ipcp->type = IPC_TYPE_SYNC_DISPOSE; 
    }
    return ripcp;
}

IpcB *
ipc_SendSync (IpcB * ipcp, MailHandle target)
{
    return ipc_SyncRoot (ipcp, target, IPC_TYPE_SYNC_REQUEST);
}


/*
**  ipc_GetPriority - returns the priority of the ipc
**     ipc  - ipc buffer from ipc_GetBuffer()
**     returns: priority of the ipc
** Note that this function may be defined as an inline marco in
** ipcprivate.h. We undefine it here so that it is available for linking.
*/

# ifdef ipc_GetPriority
#   undef ipc_GetPriority
# endif

IpcPriority
ipc_GetPriority (IpcB * ipcp)
{
    return ipcp->priority;
}

/*
**  ipc_SetPriority - set the priority of the ipc
**     ipc  - ipc buffer from ipc_GetBuffer()
**     pri  - priority to set the ipc to
**     returns: none
** Note that this function may be defined as an inline marco in
** ipcprivate.h. We undefine it here so that it is available for linking.
*/
# ifdef ipc_SetPriority
#   undef ipc_SetPriority
# endif

void 
ipc_SetPriority (IpcB * ipcp, IpcPriority pri)
{
    ipcp->priority = pri;   /* set new priority */
}

/*
**  ipc_GetSender - returns the sender of the ipc
**     ipc  - ipc buffer from ipc_GetBuffer()
**     returns: mailbox of the sender of the ipc
*/
# ifdef ipc_GetSender
#   undef ipc_GetSender
# endif

MailHandle
ipc_GetSender (IpcB * ipcp)
{
    return ipcp->sender;
}

/*
**  ipc_GetSize - returns the size of the data in the ipc
**     ipc  - ipc buffer from ipc_GetBuffer()
**     returns: size of the ipc data
** Note that this function may be defined as an inline marco in
** ipcprivate.h. We undefine it here so that it is available for linking.
*/
# ifdef ipc_GetMaxSize
#   undef ipc_GetMaxSize
# endif

size_t 
ipc_GetMaxSize (IpcB * ipcp)
{
    return ipcp->dataSize;
}


/*
**  ipc_GetMsgLen - returns the length of the data in the ipc
**     ipc  - ipc buffer from ipc_GetBuffer()
**     returns: size of the ipc data
** Note that this function may be defined as an inline marco in
** ipcprivate.h. We undefine it here so that it is available for linking.
*/
# ifdef ipc_GetMsgLen
#   undef ipc_GetMsgLen
# endif

size_t 
ipc_GetMsgLen (IpcB * ipcp)
{
    return ipcp->dataLen;
}


/*
**  ipc_SetMsgLen - sets the length of the data in the ipc
**     ipc  - ipc buffer from ipc_GetBuffer()
**     returns: size of the ipc data
** Note that this function may be defined as an inline marco in
** ipcprivate.h. We undefine it here so that it is available for linking.
*/
# ifdef ipc_SetMsgLen
#   undef ipc_SetMsgLen
# endif

size_t 
ipc_SetMsgLen (IpcB * ipcp, size_t size)
{
    return ipcp->dataLen = size;
}


/*
** ipc_GetType - returns the type of the ipc
**    ipc  - ipc buffer from ipc_GetBuffer()
**    returns: type of the ipc
** It is not clear that this is at all useful to user, but why not.
*/
# ifdef ipc_GetType
#   undef ipc_GetType
# endif

IpcType 
ipc_GetType (IpcB * ipcp)
{
    return ipcp->type;
}


/*
** ipc_GetUserType - get the user type of the ipc
**    ipc  - ipc buffer from ipc_GetBuffer()
**    returns: user type of the ipc
*/
# ifdef ipc_GetUserType
#   undef ipc_GetUserType
# endif

int 
ipc_GetUserType (IpcB * ipcp)
{
    return ipcp->userType;
}

/*
** ipc_SetUserType - set the user type of the ipc
**    ipc    - ipc buffer from ipc_GetBuffer()
**    utype  - user type to set ipc to
**    returns: none
*/
# ifdef ipc_SetUserType
#   undef ipc_SetUserType
# endif

void 
ipc_SetUserType (IpcB * ipcp, IPCUser utype)
{
    ipcp->userType = utype; /* set new user type */
}


/*
** ipc_SetRcvTimeout -- set the receive timeout
*/
int
ipc_SetRcvTimeout (int mstimeout)
{
    int ltimeout = IpcLocalRoot.msDelay;

    IpcLocalRoot.msDelay = mstimeout;

    return ltimeout;
}

/*
** ipc_GetDataP -- retrieve a pointer to the data buffer.
*/
# ifdef ipc_GetDataP
#   undef ipc_GetDataP
# endif

void *
ipc_GetDataP (IpcB * ipcp)
{
    return (void *) ipcp->data;
}


int
ipc_sendRendezvous (MailHandle mh)
{
    IpcB * ipcp;

    ipcp = ipc_NewBuffer ();
    ipcp->type = IPC_TYPE_RENDEZVOUS;
    ipcp->userType = IPC_SYS_RENDEZVOUS;
    ipcp = ipc_SyncRoot (ipcp, mh, IPC_TYPE_RENDEZVOUS);
    if (ipcp)
    {
        ipc_FreeBuffer (ipcp);
        return SUCCESS;
    }
    return !SUCCESS;
}

/*
** ipc_nameRendezvous -- waits for upto oneSecondRetries seconds
** for the mailNamep to be a vailid mail name.
*/
MailHandle
ipc_nameRendezvous (char * mailNamep, int oneSecondRetries)
{
    MailHandle mh;
    int i;
    
    oneSecondRetries &= 0x7fffffff;
    for (i = oneSecondRetries; i > 0; i--)
    {
        mh = ipc_GetMailHandle (mailNamep);
        if (mh)
            return mh;
        sleep (1);
    }
    return (MailHandle) 0;
}

MailHandle
ipc_waitForRendezvous (MailHandle fromMH, long msDelay)
{
    IpcB * ipcp;
    MailHandle mh;

    ipcp = ipc_Receive (fromMH, IPC_TYPE_RENDEZVOUS_REPLY, msDelay);
    if (ipcp)
    {
        mh = ipcp->sender;
        ipc_Reply (ipcp);
        return mh;
    }
    return (MailHandle) 0;
}

