
/*
** COPYRIGHT (C) 1994, ICTV Inc. 280 Martin Ave. Santa Clara, Calif 95050. 280
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television
**
** File: ipcpriority.h - priority values for tasks using IPC.
**
** Purpose:
**  To provide global defintion of IPC priority values.
**
** Types/Clases:
**  IpcUser -- enum defining IPC values.
**
** Functions:
**  None
**
** Usage:
**  The values defined in this file are to be used as parameters
**	to ipc_MailBoxCreate.
** Note:
**	The enum here should really be under mmc.
**
** $Id: ipcpriority.h,v 1.10 1994/10/26 16:36:12 emil Exp $
**
*/

# ifndef __IPCPRIORITY_H__
# define __IPCPRIORITY_H__


typedef enum _Priority
{
    Pri_Sliced = 0,
    Pri_Lowest = 1,
    Pri_Syslog,
    Pri_VGL, /* Render Task */
    Pri_GFT, /* graphics feeder task */
    Pri_MAL,
    Pri_BODB, /* back office data base task */
    Pri_RDB, /* run time data base */
    Pri_APPDB, /* application specific data base */
    Pri_APPProxy,
	/* Non real-time servers are above here */
	Pri_RealTime, /* a plase holder */
	/* Real-time servers are below here */
    Pri_ADCP, /* Audio Feeder Task, normal operation */
    Pri_alertSysLog, /* If syslog can't get it work done */
    Pri_AVSPacketParser,
    Pri_alertADCP, /* Audio Feeder Task when data is low */
    Pri_AVSReclaimer,
    Pri_DVP,
    Pri_AVSControl,
    Pri_AVStrm = Pri_AVSControl,
    Pri_AVGSM,
	/* Application layer event handlers are below here */
    Pri_APP, /* this is the interplay priority */
    Pri_AppClock,
    Pri_ChannelApp,
    Pri_SET,
    Pri_AppSET = Pri_SET,
	/* Special priorities */
    Pri_Highest, /* insert addtional priorities above here */
    Pri_Block,
    Pri_Killer /* Priority of task that can always terminate all MB tasks */
} Priority;

/* IPCPRIORITIES give us the number of priorities we get to work with,
** it as allocated at the high end of the priority range to allow 
** the normal (bizarre) OS9K priority scheme to work */
# define IPCPRIORITIES (536)
/* MAXPRIORITY is based in the info in the _os_setpr() call documentation
** on page 8-380 of the Using Ultra C Manual. */
# define MAXPRIORITY ((1 << 16) - 1)

/* We need to arrange to set the OS9K system global d_maxage to
** PRI_MINIMUM in ipcInit. */
# define PRI_MINIMUM (MAXPRIORITY - IPCPRIORITIES)

# define PRIORITY(N) ((PRI_MINIMUM + (N)) & MAXPRIORITY)
/* Priority block is set to highest and not MAX so that Killer (or other
** non-ipc tasks can run as appropriate */
# define PRI_BLOCK PRIORITY(Pri_Block)

/* PRI_SLICE -- this is defined here because I don't know what would be a
** better place.  It is used by ipcinit to set the system time slice.
** Using this will change the behavior of regular OS9K tasks! */
# define PRI_SLICE 20

# endif

