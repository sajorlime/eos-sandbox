
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television using OS9K
**
** File: lbFreeList.h -- definitions Locking Blocking Free Lists.
**
** Author: emil
**
** Purpose:
**
** Types/Clases:
    LBNode -- A node of a locking blocking free list.
    LBList -- The root of a locking blocking free list.
**
** Purpose:
    This moudle implements a base class that manipulates and provides
    templates for locking blocking free list of structures.

** Notes:
**  If we had C++ this would be an ideal tempate class.
**
** History:
** 8/7/94
** Fixed a couple of bugs and misfeatures in the newlist and free logic. BDN
**
** $Id: lbFreeList.h,v 1.11 1995/01/14 14:47:16 emil Exp $
**
*/

# ifndef __LBFREELIST_H__
# define __LBFREELIST_H__

# include "ictvcommon.h"
# include "ProcessLock.h"

typedef struct _ListNode
{
    struct _ListNode * nextNodep;
} ListNode;

/* To use a locking blocking list the first element of your structure
** must be an LBNode */
typedef ListNode LBNode;

/* LB_WAITING_EV is used as the event value to pass to lock_sleep and
** lock_wake the free list is empty */
# define LB_WAITING_EV 1


/* The LBList structure is a private structure for the LBList,
** It contains the free list and other book keeping information.
*/
typedef struct _LBList
{
    void * rootp; /* this is the root of the origonal alloc */
    int allocNodes; /* the number of LBNodes that were allocated */
    int nodeSize; /* Size of the allocated nodes */
    int numNodes; /* number of the allocated nodes */
    int nfree; /* number on the free list. */
    LBNode * freep; /* this is the dynamic free list */
    int waiting; /* number of tasks waiting for list members */
    ProcessLock pl; /* this allows single threaded access to the List */
} LBList;


/* lbList_newList -- makes a new list of num LBNodes each with nodeSize
** data.  nodeSize should include the size of the nextNodeP.  This function
** should not be accessed directly, rather one should use the macro
** LBLIST_NEW_LIST.
*/
LBList * lbList_newList (LBList * lblp, int num, int nodeSize);

/* lbList_freshList -- makes the list fresh again based on it intialized value.
*/
LBList * lbList_freshList (LBList * lblp);

/*
** lbList_deleteList -- frees any allocated memory in the list
** Use LBLIST_DELETE_LIST.
*/
void lbList_deleteList (LBList * lblp);

/*
** lbList_alloc -- gets a node from the free list.
** Use LBLIST_ALLOC.
*/
LBNode * lbList_alloc (LBList * lblp);

# ifndef LBFL_USE_LOCK
LBNode * lbList_allocNoBlock (LBList * lblp);
# endif

/*
** lbList_free -- returns a list of nodes to the free list.
** Use LBLIST_FREE.
*/
void lbList_free (LBList * lblp, LBNode * lbnp);

/*
** lbList_nextAndFree -- the returned member can be a list. 
** The first member is removed, and the rest if the list is 
** returned.
** Use LBLIST_NEXT_AND_FREE.
*/
LBNode * lbList_nextAndFree (LBList * lblp, LBNode * lbnp);

/*
** lbList_next -- takes a node list and gets the next member,
** but does not modify the list.
** Use LBLIST_NEXT
**
** lbList_setNext -- sets the next node pointer.
** Use LBLIST_SET_NEXT
*/
# ifndef NO_INLINE
#   ifndef NO_LBFREELIST_INLINE
#        define lbList_next(lbnp) ((lbnp)->nextNodep)
#        define lbList_setNext(lbnp, nextNp) ((lbnp)->nextNodep = (nextNp))
#   else
        LBNode * lbList_next (LBNode * lbnp);
        LBNode * lbList_setNext (LBNode * lbnp, LBNode * nextNp);
#   endif
# endif


# define LBLIST_NEW_LIST(listp, num, Type) \
    (lbList_newList (((LBList *) listp), sizeof (Type), num))
/*
** usage:
**  typedef struct _MyType {LBNode lbn; ...} MyType;
**  LBList * myList = 0;
**  myList = LBLIST_NEW_LIST (myList, myNumOfBlocks, MyType);
*/
# define LBLIST_DELETE_LIST(listp) (lbList_deleteList (((LBList *) listp)))
/*
** LBLIST_LIST_DELETE_LIST (myList);
*/

# define LBLIST_ALLOC(listp, Type) ((Type *) lbList_alloc (((LBList *) listp)))
/*
** MyType * mtp;
** mtp = LBLIST_ALLOC (myList);
*/

# define LBLIST_FREE(listp, nodep) \
    (lbList_free ((LBList *) listp, ((LBNode *) nodep)))
/*
*/

# define LBLIST_NEXT_AND_FREE(listp, nodep, Type) \
    (Type *) lbList_nextAndFree(((LBList *) listp), ((LBNode *) nodep))
/*
** mtp = LBLIST_NEXT_AND_FREE (myList, mtp, MyType);
*/

# define LBLIST_GET_NEXT(nodep, Type) (Type *) lbList_next (((LBNode *) nodep))
/*
** mtp = LBLIST_GET_NEXT (mtp, MyType);
*/

# define LBLIST_SET_NEXT(nodep, nextNp, Type) \
    (Type *) lbList_setNext ((LBNode *) (nodep), (LBNode *) (nextNp))
/*
** mtp = LBLIST_SET_NEXT (mtp, mtNp, MyType);
*/

/* Non-shared list operators */

typedef struct _LocalList
{
	ListNode * headp;
	ListNode * tailp;
} LocalList;


/* ll_getList -- returns a node pointer that contains the entire
** list is returned, the LocalList is is empty after this call.
*/
ListNode * ll_getList (LocalList * llp);

# define LL_GET_LIST(llp, Type) ((Type *) ll_getList (llp))

/* ll_getNext -- returns a node pointer that is a singleton.  It
** is removed from the local list.
*/
ListNode * ll_getNext (LocalList * llp);

# define LL_GET_NEXT(llp, Type) (Type *) ll_getNext (llp)
/*
** ll_lifoPut -- puts a node list (the node can be one or more values
** it puts the value such that if getList or getNext are called
** after this call it would be the returned first.
*/
ListNode * ll_lifoPut (LocalList * llp, ListNode * lnListp);

# define LL_LIFO_PUT(llp, lnp, Type) \
	(Type *) ll_lifoPut ((llp), (ListNode *) (lnp))

/*
** ll_lifoPut -- puts a node list (the node can be one or more values
** it puts the value such that if getList or getNext are called
** after this call it would be the returned last.
*/
ListNode * ll_fifoPut (LocalList * llp, ListNode * lnListp);

# define LL_FIFO_PUT(llp, lnp, Type) \
	(Type *) ll_fifoPut ((LocalList *) (llp), (ListNode *) (lnp))

/* lbList_findEnd -- is a utility function for returning the end
** of a list of ListNodes.
*/
ListNode * lbList_findEnd (ListNode * lnp);

# define LBLIST_FIND_END(nodep, Type) \
    (Type *) lbList_findNext ((ListNode *) (nodep))

/* LL_INIT_LIST -- sets the head and tail pointers to null */
# define LL_INIT_LIST(llp) ((llp)->headp = (llp)->tailp = (ListNode *) 0)

# endif

