
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television using OS9K, IPC Tools
**
** File: ~dev/ICTV/libsrc/Tools/ipcenq.c
**
** Author: emil
** $Author: wul $
**
** Purpose:
**  This module provides the capabability of enquring about all of ipc mail
**  boxes active in the system.
**
** Types/Clases:
**
** Data
**
** Functions:
**  main -- this does it.
**
** Notes:
**
** Overview:
**
** $Id: ipcenq.c,v 1.30 1995/11/04 02:03:47 wul Exp $
**
*/

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <process.h>
# include <reg386.h>

# include "sighandle.h"
# include "ictvutil.h"
# include "ipc.h"
# include "ipcscreen.h"
# ifdef VPM
# include "globVPM.h"
# else
# include "globMMC.h"
# endif
# include "MemHeap.h"
# include <sysglob.h>
# include <types.h>

# define __IPCENQ_C__

IpcMem * ipc_GetRoot (void);

# define MAXBOXES (2 * IPCMAILBOXNUM)

struct _list
{
    int inlist;
    char names[MAXBOXES][16];
}
list = {0};

void
addtolist (char * namep)
{
    if (!namep)
        return;
    strcpy (list.names[list.inlist], namep);
    list.inlist++;
}

char * isinlist (char * namep)
{
    int i;
    char * cp;

    if (!namep)
        return 0;
    for (i = 0; i < list.inlist; i++)
    {
        cp = strstr (namep, list.names[i]);
        if (cp)
            return namep;
    }
    return 0;
}


void
killMB (MailBox * mbp, int killit)
{
    if (!mbp)
        return;
    if (!mbp->rootp)
        return;
    if (!mbp->procID)
        return;
    if (mbp->procID != getpid ())
    {
        if (killit == SIGQUIT)
        {
            printf ("Let's Destroy %s\n", mbp->name);
            /* This frees all memory handles from
            ** the default heap (0) */
            ipc_MailBoxDestroy (mbp);
            mem_cleanUp(0, mbp->procID);
        }
        else
        {
            _os_setpr (mbp->procID, mbp->procPri);
            _os_send (mbp->procID, killit);
        }
    }
    else
    {
        printf ("PID same as me %d\n", mbp->procID);
        printf ("Cleaning the mailbox\n");
        mbp->rootp = 0;
    }
}

void
showMBox (MailBox * mbp, int killit, int Verbose)
{
    if (!mbp)
        return;
    if (!(*mbp->name))
        return;
    if (!mbp->rootp)
        return;
    ipc_printMailbox (mbp, Verbose);
    if (Verbose && isinlist (mbp->name))
    {
        IpcB * ipcp;

        ipcp = mbp->head;
        if (ipcp)
        {
            do
            {
                ipc_printIPCB (ipcp, Verbose);
            }
            while (ipcp = ipcp->next);
        }
    }
    if (killit)
    {
        if (!mbp->procID)
            return;
        if (mbp->procID != getpid ())
            _os_send (mbp->procID, killit);
    }
}

void
ie_delay (void)
{
    signal_code sig;
    u_int32 ticks = 10;

    _os_sleep (&ticks, &sig);
}

# if 0
void
printMaxage (void)
{
    glob_buff t;
    error_code i;

    i = _os_getsys (offsetof (sysglobs, d_maxage), sizeof (u_int16), &t);
    if (!i)
        printf ("maxage:%d/%x\n", t.wrd, t.wrd);
    else
        printf ("Error in getSys:%d\n", i);
    i = _os_getsys (offsetof (sysglobs, d_tslice), sizeof (u_int16), &t);
    if (!i)
        printf ("tslice:%d/%x\n", t.wrd, t.wrd);
    else
        printf ("Error in getSys:%d\n", i);
}

int
rTestPriority (void)
{
    process_id procID;
    u_int16 pri, age, group, user;
    int32 sched;
    int r;

    r = _os_id (&procID, &pri, &age, &sched, &group, &user);
    if ((u_int32) pri > (u_int32) PRIORITY (Pri_Highest))
    {
        fprintf (stderr, "Priority Error: %x/%x\n", pri, PRIORITY (Pri_Highest));
        exit (1);
    }
    return 0;
}
# endif


void
permit (void)
{
	u_char *mem_ptr = (u_char) 0;
	u_int32 size = 0x800000;
	u_int16 perm = 0xffff;
	process_id pid = getpid ();

	_os_permit (mem_ptr, size, perm, pid);
}


void
ipcenq_sigHandler (int sig)
{
	exit (0);
}

MailHandle me;
int Killum = 0;
Priority Pri = Pri_Highest;
int Pause = 0;
int cont = 0;
int Verbose = 0;
int Mother = 0;
int Alternate = 0;
int Clean = 0;
int Report = 0;

pr_desc bigbuffer;

void
printProcs (void)
{
	MailBox *mbp;
	IpcMem *ipcmp;
	u_int32 count = sizeof (pr_desc);
	int i;
	pr_desc *pd = &bigbuffer;
	char *modname;

	u_int16 attr_rev;
	u_int16 type_lang;
	void *mp;
	error_code ec;
	regs *stack;

	attr_rev = 0;
	type_lang = 0;	/* any type */

	ipcmp = ipc_GetRoot ();
	if (ipcmp->inuseMailboxes - (Mother ? 0 : 2) <= 0)
	{
		printf ("No matching Mailboxes.\n");
		return;
	}
    if (!Verbose)
        printf
        (
            " Id PId Grp.Usr  Prior  DataSiz Sig S StackP    IP       "
            "MBName Module\n"
        );
	for
	(
		 mbp = ipcmp->mailboxes + (Mother ? 0 : 2),
            i = ipcmp->maxMailboxes - (Mother ? 0 : 2);
		 i > 0;
		 i--, mbp++
	)
	{
		/* if we have a list use it, otherwise all */
		if
		(
			(list.inlist && isinlist (mbp->name))
			|| (!list.inlist && mbp && (mbp != me))
		)
		{
			if (!mbp)
				continue;
			if (!(*mbp->name))
				continue;
			if (!mbp->rootp)
				continue;
			ec = _os_gprdsc (mbp->procID, (u_char *) pd, &count);
			/* Dead process either has no process descriptor or * is not sitting in a queue. */
			if
			(
				ec
				|| pd->p_queueid == '*'
				|| (*(MailBox **) mbp->rootp != mbp)
			)
			{
				if (Clean)
				{
					ipcmp->inuseMailboxes--;
					mbp->rootp = 0;
					printf ("Cleaning ... ");
				}
				printf ("--- DEAD PROCESS : %s ---\n", mbp->name);
				continue;
			}
			ec = _os_linkm (pd->p_pmodul, &mp, &type_lang, &attr_rev);
			if (ec)
			{
				fprintf (stderr, "Link error %d\n", ec);
				exit (1);
			}
			stack = (regs *) ((int) (&pd->p_stackend)
                    - ((int) &pd->p_stackend & 3) - sizeof (regs));
			modname = (char *) (pd->p_pmodul->m_name + (u_int32) pd->p_pmodul);
            if (Verbose)
                printf
                (
                    " Id PId Grp.Usr  Prior  DataSiz Sig S StackP    IP       "
                    "MBName Module\n"
                );
			printf
			(
				"%3d %3d %3d.%d\t %d\t%5dB %3d  %c %08p  %08p %s [ %s ]",
				pd->p_id, pd->p_pid, pd->p_owner.grp_usr.grp,
				pd->p_owner.grp_usr.usr, pd->p_prior,
				pd->p_datasz,
				pd->p_siglst, pd->p_queueid, stack->esp,
				stack->eip,
				mbp->name, modname
			);
            if (Verbose)
            {
                printf
                (
                    "\neax:     ebx:     ecx:     edx:      "
                        "esi:     edi:     ebp:     esp:\n"
                    "%08p %08p %08p %08p  %08p %08p %08p %08p\n"
                    "cs:%04x  ds:%04x  es:%04x  fs:%04x   gs:%04x ss:%04x\n",
                    stack->eax, stack->ebx, stack->ecx, stack->edx,
                    stack->esi, stack->edi, stack->ebp, stack->esp,
                    stack->cs, stack->ds, stack->es, stack->fs,
                    stack->gs, stack->ss
                );
                printf
                (
                    "sigmask:%08x, sigcnt:%d, sigsiz:%d, siglst:%d\n",
                    pd->p_sigmask, pd->p_sigcnt, pd->p_sigsiz, pd->p_siglst
                );
            }
			printf ("\n");
			_os_unlink (pd->p_pmodul);	/* Must link in order to access */
		}
	}
}


/******************************************************
*
*   MailBox Printout    
*
******************************************************/

void
printMailBoxes (void)
{
    MailBox * mbp;
    IpcMem * ipcmp;
    IpcB * ipcp;
    int ipcs;
    int i;

    ipcmp = ipc_GetRoot ();
contloop:
    ipc_printIPCMem (ipcmp, Verbose);
    
    for (ipcs = 0, ipcp = ipcmp->ipcFreeList; ipcp; ipcp = ipcp->next)
        ipcs++;
    printf ("%d ipcs on FreeList\n", ipcs);
    
    for
    (
        mbp = ipcmp->mailboxes + 2, i = ipcmp->maxMailboxes - 2;
        i > 0;
        i--, mbp++
    )
    {
        /* if we have a list use it, otherwise all */
        if (list.inlist)
        {
            if (isinlist (mbp->name))
                showMBox (mbp, Killum, Verbose);
        }
        else
        {
            if (mbp && (mbp != me))
                showMBox (mbp, Killum, Verbose);
        }
    }
    if (Killum == SIGQUIT) /* if sig quit then, check out the mailboxs again */
    {
        sleep (3);  /* give a little for the signal abvoue to have worked */
        for (mbp = ipcmp->mailboxes + 2, i = ipcmp->maxMailboxes - 2; i > 0; i--, mbp++)
        {
            if (list.inlist)
            {
                if (isinlist (mbp->name))
                    killMB (mbp, Killum);
            }
            else
            {
                if (mbp && (mbp != me))
                    killMB (mbp, Killum);
            }
        }
    }
    if (Mother)
    { /* if Mother is true we show Mother, and we may kill her */
        showMBox (ipcmp->mailboxes, 0, Verbose);
        showMBox (ipcmp->mailboxes + 1, 0, Verbose);
        if (Killum)
        {
            killMB (ipcmp->mailboxes, Killum);
            killMB (ipcmp->mailboxes + 1, Killum);
        }
    }
    if (cont)
    {
        sleep (3);
        printf ("\n");
        goto contloop;
    }
    return;
}


/*
** Check if ipc Buffer is checked out by mailbox
*/

int
ipcB_mailBoxCheck (IpcB * ipcp)
{
    IpcMem *ipcmp;
    IpcB *ipcusedp;
    MailBox *mbp;
    int mbcount;

    ipcmp = ipc_GetRoot();
    for /* each mailbox ... */
    (
        mbcount = 0,  mbp = ipcmp->boxes;
        mbcount < ipcmp->maxMailboxes;
        mbp++, mbcount++
    )
    {
        for /* each buffer held by that mailbox ... */
        (
            ipcusedp = mbp->head;
            ipcusedp;
            ipcusedp = ipcusedp->next
        )
        {
            if (ipcusedp == ipcp) /* Found a match! */
              return 1;
        }
    }
    return 0; /* No match found */
}



/*
** Check if ipc Buffer is found in free list
*/
int
ipcB_freeListCheck (IpcB * ipcp)
{
    IpcMem *ipcmp;
    IpcB *ipcfreep;

    ipcmp = ipc_GetRoot();

     for
	 (
          ipcfreep = ipcmp->ipcFreeList;
          ipcfreep;
          ipcfreep = ipcfreep->next
     )
     {
          if (ipcp == ipcfreep) /* If on free list */
            return 1; /* Found it! */
     }
     return 0; /* Never found on free list */
}



/*
** Reinitialize a buffer (fixes trashed buffers)
*/
int
ipcB_init (IpcB *ipcp)
{
    ipcp = ipcp;
    ipcp->next = ++ipcp;
    ipcp->self = ipcp;
    ipcp->dataSize = IPCBUFFERLEN;
    ipcp->sanity1 = SANITY1;
    ipcp->sanity2 = SANITY2;
    ipcp->type = IPC_TYPE_UNUSED;
    ipcp->priority = 0;
    ipcp->sender = 0;
    ipcp->target = 0;
    ipcp->dataLen = 0;
    ipcp->mnum = 0;
    ipcp->userType = IPC_TYPE_UNUSED;
    ipcp->data[0] = 0;
    return 1;
}

int FoundOphans = 0;

void
printReport (void)
{
	IpcMem *ipcmp;
	IpcB *ipcp, *ipcfreep;
	int ipcs;
	int i;

	ipcmp = ipc_GetRoot ();

	for (ipcp = ipcmp->ipcFreeList, ipcs = 0; ipcp; ipcp = ipcp->next)
		ipcs++;
	printf ("%d Ipcs on the Free List.\n", ipcs);
	for
	(
		 ipcs = 0, i = 0, ipcp = ipcmp->ipcs;
		 ipcs < ipcmp->maxIPCs;
		 ipcp++
	)
	{
		if (ipcp->type != IPC_TYPE_UNUSED)	/* its been used */
		{
			i++;
			printf ("%d -> ", ipcs);
			ipc_printIPCB (ipcp, Verbose);
		}

		/** Check if buffer was trashed (someone overwrote it)
		** If so, reinitialize it. */
		if (ipcp->self != ipcp)
		{
			FoundOphans++;
			printf ("FOUND TRASHED IPC BUFFER....\n");
		}

		/** Check if buffer is either on free list
		** or in use by a mailbox.  Buffer must be one or the other. */
		if (!ipcB_mailBoxCheck (ipcp) && !ipcB_freeListCheck (ipcp))
		{
			FoundOphans++;
			printf ("FOUND LOST IPC BUFFER... .\n"); fflush (stdout);
# if 0
			ipc_FreeBuffer (ipcp);
# endif
		}
		ipcs++;
	}
	printf ("%d ipcs used\n", i);
}

void
recoverIPCs (void)
{
	IpcMem * ipcmp;
	IpcB * ipcp;
	IpcB * ipcfreep;
	int ipcs;
	int i;

	printf
	(
		"There are %d orphan or damaged IPC buffers\n"
		"\n"
		"\tDo you want to recover all buffers?\n"
		"\tIf there are any outstanding messages this will break!\n"
		"\n"
		"Ctl-C to exit, <Return> to do it\n"
	);
	getchar ();
	
	ipcmp = ipc_GetRoot ();

	ipcmp->ipcFreeList = (IpcB *) 0;

	for
	(
		 ipcs = 0, i = 0, ipcp = ipcmp->ipcs;
		 ipcs < ipcmp->maxIPCs;
		 ipcp++
	)
	{
		ipcB_init (ipcp);
	}
	ipcp--;
	ipcp->next = (IpcB *) 0;
}



char sKill[] = "-k";
char sClean[] = "-C";
char sWake[8] = "-w";
char sNoBlock[] = "-n";
char sPause[] = "-p";
char sContinue[4] = "-c";
char sAlternate[] = "-a";
char sVerbose[] = "-v";
char sMother[] = "-M";
char sReport[] = "-r";

static char * sUsages[] =
{
    "[-k | -w[n]] [-n] [-p] [-c [secs]] [-v] [-M] [-r] [partial mailbox names]",
    "Enquires as to the status of all IPC tasks.  This is a debug tool.",
    "-k -- kills all the processes (SIGKILL)",
    "-w[n] -- sends a SIGWAKE to the process or if a decimal value",
	"         is specified it send that value to the process.",
    "-C -- Cleans out all the Mailboxes owned by dead processes",
    "-w -- wakes all the processes (SIGWAKE)",
    "-n -- enquire without blocking, this may make the results unreliable",
    "-p -- pause until a carriage return is pressed, after enquiry",
    "-a -- use alternate format",
    "-c -- continuous",
    "-v -- print Verbosely",
    "      if name and  Verbose is specified outstanding messages are printed",
    "-M -- show Mother & Mal -k -M kills mother with other processes",
    " partial mailbox names -- restricts the list of printed mailboxes to",
    "       those that contain the string(s) passed",
    "",
    "e.g. ipcEnq xyz -- prints all mailboxes containing an xyz",
    "-r -- report buffers",
    0
};


int
main (int argc, char ** argv)
{
    int i;
    char *namep;

    usage (argc, argv, sUsages, 1);
    if (arg_OptGet (argc, argv, sKill, 0))
        Killum = SIGQUIT;
    if (arg_OptGet (argc, argv, sClean, 0))
        Clean = 1;
    if (arg_OptGet (argc, argv, sWake, sizeof (sWake)))
	{
		if ((Killum = atoi (sWake)) == 0)
			Killum = SIGWAKE;
	}
    if (arg_OptGet (argc, argv, sNoBlock, 0))
        Pri = Pri_Lowest;
    if (arg_OptGet (argc, argv, sPause, 0))
        Pause = 1;
    if (arg_OptGet (argc, argv, sAlternate, 0))
        Alternate = 1;
    if (arg_OptGet (argc, argv, sContinue, sizeof (sContinue)))
        if ((cont = atoi (sContinue)) == 0)
            cont = 3;
    if (arg_OptGet (argc, argv, sVerbose, 0))
        Verbose = 1;
    if (arg_OptGet (argc, argv, sMother, 0))
        Mother = 1;
    if (arg_OptGet (argc, argv, sReport, 0))
        Report = 1;

    while (namep = arg_Next (argc, argv))
        if (namep[0] != '-')
            addtolist (namep);

    if (glob_link ())
    {
        printf ("Failed to link to shared memory\n");
        return 0;
    }

	sigHandlerSet (ipcenq_sigHandler);
	permit ();

    me = ipc_MailBoxCreate (0, Pri, 0, 0);
    if (Report)
	{
        printReport ();
		if (FoundOphans)
			recoverIPCs ();
        printMailBoxes ();
	}
    else if (Alternate || Clean)
        printProcs ();
    else
        printMailBoxes ();
    return 0;
}

