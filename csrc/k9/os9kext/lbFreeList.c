
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television using OS9K
**
** File: lbFreeList.c -- locking blocking free list.
**
** Author: emil
**
** Purpose:
**  Implements a the locking blocking free list.  The locking part is that
**  it guards against concurant access by more than one task. The blocking
**  part is that it makes the calling task go to sleep if there are no
**  list members available.
**
** Types/Classes:
**	LBNode -- structure that defines a transfer request for AVStr data.
**
** Notes:
**
** $Id: lbFreeList.c,v 1.20 1995/11/04 02:03:44 wul Exp $
*/

# define __LBFREELIST_C__
# define NO_LBFREELIST_INLINE

# include "lbFreeList.h"
# include "MemHeap.h"
# include "ipc.h"
# include "ipcscreen.h"
# include "k9misc.h"

void
lbList_free (LBList * lblp, LBNode * lbnp)
{
	LBNode * lbNextp;
	ProcessLock *pl;
	u_int32 eflags;

    if (!lblp)
        return;
	pl = &lblp->pl ;
# ifdef LBFL_USE_LOCK
	lock_acquire (pl);
# else
	eflags = irq_change (0);
# endif
	while (lbnp)
	{
		lbNextp = lbnp->nextNodep;
		lbnp->nextNodep = lblp->freep;
		lblp->freep = lbnp;
		lblp->nfree++;
		lblp->allocNodes--;
		lbnp = lbNextp;
	}
# ifdef LBFL_USE_LOCK
	lock_release (pl);
# else
	irq_change (eflags);
# endif
	if (lblp->waiting) /* N.B. lblp gets smashed by this call! */
		pblock_wake (lock_pblockp (pl), *((int32 *)&lblp));
}

# define CHECK_LBLS
# ifdef CHECK_LBLS

int
checkLBNodeIsFree (LBList * lblp, LBNode * lbnp)
{
	LBNode * tlbnp;
    u_int eflags;

	eflags = irq_change (0);
    tlbnp = lblp->freep;
    do
    {
        if (tlbnp == lbnp)
        {
			K9_ASSERT (("LBNode * %x found on free list\n", lbnp));
            irq_change (eflags);
            return 1;
        }
    }
	while (tlbnp = tlbnp->nextNodep);
	irq_change (eflags);
    return 0;
}

void
checkLBNode (LBNode * lbnp)
{
	int i = 0;

	while ((lbnp = lbnp->nextNodep) != (LBNode *) 0)
    {
		if (++i > 3)
		{
			K9_ASSERT (("Long LBL\n"));
			return;
		}
    }
}

int
checkLBList (LBList * lblp)
{
    int onFree = 0;
	LBNode * lbnp;

    lbnp = lblp->freep;
    while (lbnp)
    {
        onFree++;
        lbnp = lbnp->nextNodep;
    }
    if (onFree != lblp->nfree)
    {
        K9_ASSERT
        ((
            "lblp:%x does not have proper count on free list\n"
            "onFree:%d != nfree:%d\n",
            lblp, onFree, lblp->nfree
        ));
    }
    return onFree;
}
# endif

LBNode *
lbList_nextAndFree (LBList * lblp, LBNode * lbnp)
{
	LBNode * lbNextp;
	ProcessLock *pl;
	u_int32 eflags;

    if (!lblp)
        return (LBNode *) 0;
	if (!lbnp)
		return lbnp;
	lbNextp = lbnp->nextNodep;
	pl = &lblp->pl ;

# ifdef LBFL_USE_LOCK
	lock_acquire (pl);
# else
	eflags = irq_change (0);
# endif
# ifdef xCHECK_LBLS
	checkLBNode (lbnp);
# endif
	lbnp->nextNodep = lblp->freep;
	lblp->freep = lbnp;
	lblp->nfree++;
	lblp->allocNodes--;
# ifdef LBFL_USE_LOCK
	lock_release (pl);
# else
	irq_change (eflags);
# endif
	if (lblp->waiting)
		pblock_wake (lock_pblockp (pl), *((int32 *)&lblp));

	return lbNextp;
}


LBNode *
lbList_alloc (LBList * lblp)
{
	LBNode * lbnp;
	u_int32 eflags;
	ProcessLock *pl;

    if (!lblp)
        return (LBNode *) 0;
	pl = &lblp->pl ;
# ifdef LBFL_USE_LOCK
	lock_acquire (pl);
# else
	eflags = irq_change (0);
# endif
	while ((lbnp = lblp->freep) == 0)
	{
		K9_ASSERT (("%s: blocking in lbL alloc\n", ipcfriend_getMailName ()));
		lblp->waiting++;
# ifdef LBFL_USE_LOCK
		/* We must block other processes from entering between the
		** release of the lock and the sleep, we do this with osBlock
		** Note that other tasks of higher priority might be waiting,
		** so if we do not block the other task would wake and 
		** take the semaphore
		** and try to wake our process before it went to sleep.
		** This would fail.
		*/
		osBlock ();
		lock_release (pl);
# endif
		pblock_sleep (lock_pblockp (pl), (int32) lblp);
		/* We must unblock now, or we hose the system */
		lblp->waiting--; /* first dec waiting to avoid extra wakes */
# ifdef LBFL_USE_LOCK
		osUnblock ();
		lock_acquire (pl);
# endif
	}
	lblp->freep = lbnp->nextNodep;
	lbnp->nextNodep = 0;
	lblp->nfree--;
	lblp->allocNodes++;
# ifdef LBFL_USE_LOCK
	lock_release (pl);
# else
	irq_change (eflags);
# endif

	return lbnp;
}



# ifndef LBFL_USE_LOCK
LBNode *
lbList_allocNoBlock (LBList * lblp)
{
	LBNode * lbnp;
	u_int32 eflags ;
	ProcessLock *pl;

    if (!lblp)
        return (LBNode *) 0;
	pl = &lblp->pl ;
	eflags = irq_change (0);
	if ((lbnp = lblp->freep) == 0)
	{
		irq_change (eflags);
		return lbnp;
	}
	lblp->freep = lbnp->nextNodep;
	lbnp->nextNodep = 0;
	lblp->nfree--;
	lblp->allocNodes++;
	irq_change (eflags);

	return lbnp;
}
# endif





LBList *
lbList_newList (LBList * lblp, int size, int num)
{
	int i;
	LBNode *lbnp;

	/* round size up to a word size */
	size = ((size + (sizeof (int) - 1)) / sizeof (int)) * sizeof (int);
	if (lblp)
	{
		lbnp = (LBNode *) mem_calloc (DEFAULT_HEAP_ID, num, size);
		lblp->rootp = lbnp;
	}
	else
	{
		lblp = (LBList *) mem_calloc (DEFAULT_HEAP_ID, 1, size * num + sizeof (LBList));
		if (lblp)
		{
			lblp->rootp = lblp;
			lbnp = (LBNode *) (((u_char *) lblp) + sizeof (LBList));
		}
	}
	if ((lblp == NULL) || (lblp->rootp == NULL))
	{
		LOCAL_MSG (("calloc failed in lbList_newList\n"), DBG_K9EXT_MASKINDEX, DBG_ASSERT);
		return 0;
	}
	lblp->freep = 0;
	lblp->nfree = 0;

	if (lock_create (&lblp->pl, 1) == NULL)
	{
		LOCAL_MSG (("lock_create failed in lbList_newList\n"), DBG_K9EXT_MASKINDEX, DBG_ASSERT);
		mem_free (DEFAULT_HEAP_ID, lblp->rootp);
	}

	for (i = 0; i < num; i++)
	{
		lbnp->nextNodep = 0;
		lbList_free (lblp, lbnp);
		lbnp = (LBNode *) ((u_char *) lbnp + size);
	}

	lblp->nodeSize = size;
	lblp->numNodes = num;
	lblp->allocNodes = 0;

	return lblp;
}

LBList *
lbList_freshList (LBList * lblp)
{
	int i;
	int num;
	int size;
	LBNode * lbnp;

	if (!lblp)
		return (LBList *) 0;
	lock_force (&lblp->pl);
	lblp->waiting = 0;
	num = lblp->numNodes;
	if (num == lblp->nfree)
		return lblp;
	if ((size = lblp->nodeSize) <= 0)
		return (LBList *) 0;
	
	lbnp = (LBNode *) lblp->rootp;

	lblp->freep = 0;
	lblp->nfree = 0;

	for (i = 0; i < num; i++)
	{
		lbnp->nextNodep = 0;
		lbList_free (lblp, lbnp);
		lbnp = (LBNode *) ((u_char *) lbnp + lblp->nodeSize);
	}
	lblp->allocNodes = 0;
	lblp->numNodes = num;

	return lblp;
}



void
lbList_deleteList (LBList * lblp)
{
	void * rootp;

	if (!lblp)
		return;
	lblp->nodeSize = 0;
	lblp->numNodes = 0;
	lblp->allocNodes = 0;
	lblp->freep = 0;
	lblp->nfree = 0;
	lblp->waiting = 0;
	lock_destroy (&lblp->pl, NULL);
	if ((rootp = lblp->rootp) != (void *) 0)
		mem_free (DEFAULT_HEAP_ID, rootp);
	lblp->rootp = NULL;
}


LBNode *
lbList_next (LBNode * lbnp)
{
	return lbnp->nextNodep;
}

LBNode *
lbList_setNext (LBNode * lbnp, LBNode * nextNp)
{
	return lbnp->nextNodep = nextNp;
}

LBNode *
lbList_findEnd (LBNode * lbnp)
{
	LBNode * lastlbnp;

	lastlbnp = lbnp;
	while ((lbnp = lastlbnp->nextNodep) != (LBNode *) 0)
		lastlbnp = lbnp;
	return lastlbnp;
}

LBNode *
ll_fifoPut (LocalList * llp, LBNode * lbnp)
{
	LBNode * rootlbnp;

    rootlbnp = llp->tailp;
    /* this is a kludge to avoid a race condition in avgsmSched */
    if (rootlbnp == lbnp)
        return lbnp;
    if (rootlbnp)
		rootlbnp->nextNodep = lbnp;
	else
		llp->headp = lbnp;
	if (lbnp->nextNodep)
{

		LOCAL_MSG (("next not null\n"), DBG_K9EXT_MASKINDEX, DBG_ASSERT);
		return 0;
		llp->tailp = lbList_findEnd (lbnp);
}
	else
{
		llp->tailp = lbnp;
}
	return lbnp;
	
}


LBNode *
ll_lifoPut (LocalList * llp, LBNode * lbnp)
{
	LBNode * endlbnp;

    if (!llp->headp)
		return ll_fifoPut (llp, lbnp);

	endlbnp = lbnp->nextNodep;
	if (lbnp->nextNodep)
		endlbnp = lbList_findEnd (lbnp);
	else
		endlbnp = lbnp;
	endlbnp->nextNodep = llp->headp;
	llp->headp = lbnp;
	return lbnp;
	
}

LBNode *
ll_getList (LocalList * llp)
{
	LBNode * lbnp;

	lbnp = llp->headp;
	llp->headp = llp->tailp = (LBNode *) 0;
	
	return lbnp;
}


LBNode *
ll_getNext (LocalList * llp)
{
	LBNode * lbnp;

	lbnp = llp->headp;
	if (lbnp)
	{
		if ((llp->headp = lbnp->nextNodep) == (LBNode *) 0)
			llp->tailp = (LBNode *) 0;
		lbnp->nextNodep = 0;
	}
	return lbnp;
}



