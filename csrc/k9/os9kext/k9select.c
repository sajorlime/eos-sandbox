/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, CA 95050 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: ictv interactive television FOO
** 
** File: select.c
**     $Id: k9select.c,v 1.3 1995/11/04 02:03:42 wul Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/k9select.c,v $
**
** Author: Bruce D. Nilo
**
** Purpose:
**     OS9K compatibility function
** 
** Types/Classes:
**     fd_set
** 
** Functions:
**     select
** 
** Assumptions/Limitations:
**     1. Maximum descriptor number supported is 31.
**     2. installSelectHandler needs to be called prior to data arriving on an
**     fd.
**     3. Once the fd is ready, and prior to data arriving on it, an initial
**        _os_ss_sendsig should be made on that fd with a signal value of
**        SIG_SELECT_BASE+fd.
**     4. Only read events are supported. The other fd_sets are for compatibility.
** 
** Notes:
**     
** 
** References:
** 
*/

#include "siguser.h"
#include "ictvcommon.h"

# define __K9SELECT_C__
#include "k9select.h"

static SignalHandler PreviousHandler = 0 ;
static int SignalsPending = 0 ;
static fd_set SignalsFired = 0 ;
static int IPCSignalFired = 0 ;

extern void exit(int) ;

/* Signals should be masked when this fires. */
static void handler(int signum)
{
  int fd ;
  if((signum >= SIG_SELECT_BASE) && 
     (signum < (SIG_SELECT_BASE + FD_MAX_NUMBER + 1))) {
    fd = signum - SIG_SELECT_BASE ;
    SignalsFired |= (1<<fd) ;
    SignalsPending++ ;
  }
  /* Check to see if there is a signal from an IPC mailbox */
  else if ((signum == SIG_IPC_MSG_RCVD)
	   ||
	   (signum == SIG_IPC_PRI_MSG_RCVD))
    {
      IPCSignalFired++;
    }
  else if(PreviousHandler)
    (*PreviousHandler)(signum) ;
  else
    exit(1) ;
}


static int selectInternal(fd_set *readfds)
{   
  int cnt = 0 ;
  /* Check to see if the signal was from IPC */
  if (IPCSignalFired){
    cnt = SIG_IPC_MSG_RCVD ;
    IPCSignalFired--;
  }

  *readfds = SignalsFired ;
  cnt += SignalsPending ;
  SignalsPending = SignalsFired = 0 ;
  _os_sigmask(0) ;
  return cnt ;   
}

/* This is somewhat ugly but them's the breaks. Particularly because
   I think that os9k sockets don't quite work consistently with ss_sendsig.
*/

void installSelectHandler(void)
{
  static int HandlerInstalled = 0 ;
  if(HandlerInstalled == 0) {
    HandlerInstalled = 1 ;
    PreviousHandler = sigHandlerSet((SignalHandler)handler) ;
  }
}

int select(int fdmax, fd_set *readfds, fd_set *ignore1, fd_set *ignore2, 
	   Timestamp_t *timeout) 
{
  int cnt, i ;
  u_int32 sig = SIGWAKE;
  u_int32 ticks = 0 ;

  if(timeout) {
      ticks = TICKS_PER_SECOND * timeout->time + (timeout->frac/USECS_PER_TICK) ;
  }

  /* Block all signals. */
  _os_sigmask(1) ;

  if(fdmax > FD_MAX_NUMBER) {
    _os_sigmask(0);  
    return -1 ;
  }

  if(SignalsFired || IPCSignalFired) {
    return selectInternal(readfds) ;
  }

  /* Ensure that the descriptors specified will signal when there is data! */
  for(i = 0 ; i < fdmax+1 ; i++) {
    if(*readfds & 1<<i)
      _os_ss_sendsig(i, SIG_SELECT_BASE + i) ;
  }

  if (timeout) {
    if ((timeout->time == 0)
	&&
	(timeout->frac == 0))
      return(0);
  }

  /* This call chould clear the sigmask! */
  _os_sleep(&ticks,&sig) ;

  if(timeout) {
    if(ticks) {
      timeout->time = ticks/TICKS_PER_SECOND ;
      timeout->frac = (ticks - (timeout->time)*TICKS_PER_SECOND)*USECS_PER_TICK ;
    }
    else {
      timeout->time = timeout->frac = 0L ;
      return 0 ; /* A timer went off! */
    }
  }

  /* Block all signals. */
  _os_sigmask(1) ;

  if(SignalsFired || IPCSignalFired)
    return selectInternal(readfds) ;
  else {
    _os_sigmask(0) ;
    return 0 ;
  }
}

  
  

