/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, CA 95050 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: General K9 locking utility.
** 
** File: ProcessLock.c
**     $Id: ProcessLock.c,v 1.17 1994/12/01 12:17:38 emil Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/ProcessLock.c,v $
**
** Author: Bruce D. Nilo
**
** Purpose: To provide an abstraction for a resource lock.
** 
** Types/Classes: ProcessLock
**     
** Functions:
**
** Usage:
** 
** Assumptions: Assumes that incrementing and decrementing an integer is
**      an atomic operation.
**     
** 
** Limitations: (See assumptions.)
** 
** References: Emil uses a similar technique for IPC.
**     
*/

# define __PROCESSLOCK_C__
# include "ProcessLock.h"
# include "MemHeap.h"


# if 1
#	define USE_IRQ_BLOCK
# endif

#ifdef LOCKDEBUG
static u_int32 TaskPID = 0 ;
#endif

ProcessLock *lock_init(ProcessLock * lockp)
{
  return lock_initByName(lockp, 0);
}

/* Used when you custom specify a name */
ProcessLock *lock_initByName(ProcessLock *lockp, char * lockname)
{
  if (!pblock_initByName (&lockp->pblock, lockname)) /* Get the global event. */
    {
      LOCAL_MSG
    (
     ("lock_access: Failed to initialize PBlock.\n"),
     DBG_K9EXT_MASKINDEX, DBG_WARN) ;
      return 0;
    }
#ifdef LOCKDEBUG
  TaskPID = getpid() ;
  lockp->owningtask = 0 ; /* No one has yet acquired the lock. */
#endif
  return lockp;
}

/* Note we are ignoring resource Count */

ProcessLock *
lock_create (ProcessLock * lockp, int resourceCount)
{
	return lock_createByName(lockp, resourceCount, 0);
}


ProcessLock *
lock_createByName (ProcessLock * lockp, int resourceCount, char * lockname)
{
	int32 need = sizeof (ProcessLock);
	u_int16 attr_rev = MA_REENT << 8;	/* reentrant module */
	u_int16 type_lang = MT_DATA << 8;	/* data module */
	mh_data *mod_head;

	/* First create the lock in shared memory if lockp is NULL. *
	** Otherwise use the lockp passed to us */
	if (!lockp)
	{
		lockp = (ProcessLock *) mem_malloc (DEFAULT_HEAP_ID, sizeof (ProcessLock));
		if (!lockp)
		{
			LOCAL_MSG (("lock_create: Failed to create data module.\n"),
				   DBG_K9EXT_MASKINDEX, DBG_WARN);
			return lockp;	/* lockp is null */
		}
	}
	/* Slame -1 into semaphore ignoring resource count */
	lockp->accessSemaphore = -1;
	if (lock_initByName (lockp, lockname) == NULL)
	{
		LOCAL_MSG
			(
			 ("lock_create: Failed to initialize PBlock.\n"),
			 DBG_K9EXT_MASKINDEX, DBG_WARN);
		return 0;
	}
	return lockp;
}


ProcessLock *
lock_createMM (char* name, int resourceCount)
{
    int32 need = sizeof (ProcessLock);
    u_int16 attr_rev = MA_REENT << 8;   /* reentrant module */
    u_int16 type_lang = MT_DATA << 8;   /* data module */
    mh_data *mod_head;
    ProcessLock * lockp = 0; 
    error_code error ;

    error = _os_datmod
        (
            name, need, &attr_rev, &type_lang,
            MP_OWNER_READ + MP_OWNER_WRITE + MP_WORLD_READ + MP_WORLD_WRITE,
            (void **) (&lockp), &mod_head
        );
    if (error || !lockp)
    {
        LOCAL_MSG (("lock_create: Failed to create data module.\n (%x)", error),
               DBG_K9EXT_MASKINDEX, DBG_WARN) ;
        return (ProcessLock *) 0;
    }
    return lock_createByName (lockp, resourceCount, name);
}


ProcessLock *
lock_accessMM (char * name)
{
    mh_com * mod_head;
    ProcessLock * theLock;
    u_int16 attr_rev = 0;
    u_int16 type_lang = 0;
    error_code error ;

    error = _os_link (&name, &mod_head, (void **) (&theLock), &type_lang, &attr_rev);
    if (error || (theLock == NULL))
      return (ProcessLock *) 0;
    return theLock ;
}

void
lock_destroy (ProcessLock * lock, char * name)
{
    u_int16 type_lang = MT_DATA << 8;   /* data module */
    pblock_delete (&lock->pblock);
    if (name)
        _os_unload (name, type_lang);
    return;
}


void 
lock_acquire (ProcessLock * lock)
{
	u_int32 eflags;
	int32 event;
	/* Note that if a process is woken for pblock_sleep it will
	** increment the accessSemaphore again but will not escapae
	** form this loop.  When the lock is release the semaphore
	** value is slamed back to -1 rather than decremented.
	*/
	eflags = irq_change (0);
	while (++lock->accessSemaphore > 0)
	{
		event = pblock_sleep (&lock->pblock, (int32) lock);
		if (event != ((int32) lock))
			GLOBAL_MSG (("Woke without lock event\n"), DBG_MISC);
	}
	eflags = irq_change (eflags);
#ifdef LOCKDEBUG
	lock->owningtask = (TaskPID) ? TaskPID : (TaskPID = getpid ());
#endif
}


void
lock_release (ProcessLock * lock)
{
	int32 ev = (u_int) lock;
	u_int32 eflags;

	eflags = irq_change (0);
	if (--lock->accessSemaphore >= 0)
	{
		/* Note that we slam accessSemaphore back to -1 here *
		   this guarantees that the first process that wake *
		   will get the lock!  Note that if a process is woken *
		   by a signal it will increment the accessSemephore *
		   falsely indicating that more processes are waiting for *
		   the semaphore. This is why we need to slame the value *
		   back to -1!  We need to analyze two things later: * 1)
		   can we make this work with are resource count > 1? *
		   2) are there holes if the process is time sliced? */
		lock->accessSemaphore = -1;
		/* Note that other processes may be run, do to the
		   reshedule * implied here. */
		pblock_wake (&lock->pblock, *(int32 *) &lock);
	}
	irq_change (eflags);
}

void
lock_force (ProcessLock * lockp)
{
	u_int32 eflags;

	eflags = irq_change (0);
	lockp->accessSemaphore = -1;
	pblock_wake (&lockp->pblock, *(int32 *) &lockp);
	irq_change (eflags);
}


