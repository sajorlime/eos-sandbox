
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** File: ipcscreen.c - send message to tDisplay.
**
** $Id: ipcscreen.c,v 1.5 1994/08/17 00:58:33 emil Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/ipcscreen.c,v $
**
** Purpose:
** 
**
** Author: emil
** $Author: emil $
**
** Types/Clases:
**
** Functions:
**
** Notes:
**
**
*/

# define __IPCSCREEN_C__
# include <assert.h>
# include <signal.h>
# include <types.h>
# include <stdio.h>
# include <stdlib.h>
# include <modes.h>
# include <string.h>


# include "ipc.h"
# include "ipcuser.h"

typedef struct _Screen
{
    MailHandle mh;
    int win;
} Screen;

static Screen screen = {0, 0};

void
pscr_init (char * title)
{
    IpcB * ipcp;
    char * cp;

    screen.mh = ipc_GetMailHandle ("Display");
    if (screen.mh) 
    {
        ipcp = ipc_NewBuffer ();
        strcpy (((char *) ipc_GetDataP (ipcp)) + 1, title);
        ipc_SetUserType (ipcp, IPC_SYS_DATA);
        ipcp = ipc_SendSync (ipcp, screen.mh);
        if (ipcp)
        {
            cp = ipc_GetDataP (ipcp);
            screen.win = *cp;
        }
        else
        {
            if (screen.mh)
            {
                GLOBAL_MSG
                (
                    (
						"[25;40HSCREEN: No IPC buffer return screen.win:%d",
						screen.win
					),
					DBG_ASSERT
                );
                screen.mh = 0;
            }
            return;
        }
        ipc_FreeBuffer (ipcp);
    }
    if (title)
        if (!screen.mh)
            APP_MSG
            (
                ("pscr_lib: failed to get mail handle for Display\n"),
				DBG_MISC
            );
}

void
pscr_show (char * format, void * a, void * b, void * c, void * d, void * e)
{
    IpcB * ipcp;
    char * cp;


    if (screen.mh)
    {
        ipcp = ipc_NewBuffer ();
        cp = ipc_GetDataP (ipcp);
        *cp++ = screen.win;
        sprintf (cp, format, a, b, c, d, e);
        ipc_SetUserType (ipcp, IPC_SYS_DISPLAY);
        ipcp = ipc_SendSync (ipcp, screen.mh);
        if (ipcp)
        {
            if (ipc_GetType (ipcp) != IPC_TYPE_SYNC_REPLY)
            {
                printf
                (
                    "SCREEN: IPC type %d screen.win:%d\n",
                    ipc_GetType (ipcp), screen.win
                );
            }
            ipc_FreeBuffer (ipcp);
        }
        else
        {
            printf
            (
                "SCREEN: No IPC buffer return screen.win:%d\n",
                screen.win
            );
            screen.mh = 0;
            exit (1);
        }

    }
	else
	{
        printf (format, a, b, c, d, e);
		printf ("\n");
	}
}





