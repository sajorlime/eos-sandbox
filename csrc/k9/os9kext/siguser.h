
/*
** COPYRIGHT (C) 1994, ICTV Inc. 280 Martin Ave. Santa Clara, Calif 95050. 280
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television
**
** File: siguser.h - user signal values
**
** Purpose:
**  To provide global defintion of signal value to be used in interactive television.
**
** Types/Clases:
**  SigHandle -- enum defining signal numbers.
**
** Functions:
**  None
**
** Usage:
**  Note to add a new signal value to this file, first check to see if
**  there is an appropriate SigTypes for the signal you want to add.
**  Do the obvious: if there is one use it, and if not, add one at the
**  end of the enum list. If you add one also add a new SIG_???_BASE at the
**  end of the SigHandle enum list (copy the form from the existing examples.
**  If you need an explicit add it following the SIG_???_BASE name in
**  the SigHandle enum list.
**
** $Id: siguser.h,v 1.7 1994/12/29 03:22:56 bruce Exp $
**
*/

# ifndef __SIGUSER_H__
# define __SIGUSER_H__

# include "sighandle.h"

enum _SigTypes
{
    SIG_IPC_TYPE = 1,
    SIG_MCA_TYPE, 
    SIG_CMX_TYPE,
    SIG_VDCP_TYPE,
    SIG_DVP_TYPE,
    SIG_DAP_TYPE,
    SIG_ADCP_TYPE,
    SIG_DMA_TYPE,
    SIG_SELECT_TYPE,
    LastSigBase /* Note: place new defs above here */
};

# ifdef _OS9000
/* it would be nice if sig base could be out at 16 bits, but kill
** under 9K a signal is a short.  9K defines the value from 1 to 255
** to be reserved for 9K, and the values from 256 to ((1 << 16) - 1)
** to be user codes. The signal_code type is a u_int32.  Anyway
** to be sure that we are safe I have reduced the range to 256 for
** a single type.  This should also discourage excessive use of
** signal values.
*/
# define SIG_BASE (1 << 8)
# define SIG_TYPE_RANGE (1 << 8)

typedef enum _SigHandle
{
    MINSIGUSER = SIG_BASE,
    /* General Purpose alarms go here */
    SIG_ICTV_ALARM,
    SIG_ISRQ_OVERFLOW,
    SIG_IPC_PRI_MSG_RCVD = SIG_BASE + SIG_IPC_TYPE * SIG_TYPE_RANGE,
    SIG_IPC_MSG_RCVD,
    SIG_IPC_TARGET_DEAD,
    SIG_IPC_TARGET_BAD,
    SIG_IPC_MESSAGE_FLUSHED,
    SIG_MCA_BASE = SIG_BASE + SIG_MCA_TYPE * SIG_TYPE_RANGE,
    SIG_MCA_READY,
    SIG_MCA_INQUIRE,
    SIG_MCA_COMPLETE,
    SIG_CMX_BASE = SIG_BASE + SIG_CMX_TYPE * SIG_TYPE_RANGE,
    /* note that cmx uses adds buffer to sig_cmx_base */
    SIG_VDCP_BASE = SIG_BASE + SIG_VDCP_TYPE * SIG_TYPE_RANGE,
	SIG_VDCP_INTERRUPT_ERROR,
    SIG_DVP_BASE = SIG_BASE + SIG_DVP_TYPE * SIG_TYPE_RANGE,
    SIG_DAP_BASE = SIG_BASE + SIG_DAP_TYPE * SIG_TYPE_RANGE,
    SIG_ADCP_BASE = SIG_BASE + SIG_ADCP_TYPE * SIG_TYPE_RANGE,
    SIG_AV110BAFL,
    SIG_DMA_BASE = SIG_BASE + SIG_DMA_TYPE * SIG_TYPE_RANGE,
    SIG_DMA_UNDERFLOW,
    SIG_SELECT_BASE = SIG_BASE + SIG_SELECT_TYPE * SIG_TYPE_RANGE,
    LastSigHandle /* Note: place new defs above here */
} SigHandle;

# else

/* If this is being used under UNIX */
typedef enum _SigHandle
{
    MINSIGUSER = SIGUSR1,
    /* General Purpose alarms go here */
    SIG_ICTV_ALARM = SIGUSR1,
    SIG_ISRQ_OVERFLOW = SIGUSR1,
    SIG_IPC_PRI_MSG_RCVD = SIGUSR1,
    SIG_IPC_MSG_RCVD = SIGUSR1,
    SIG_IPC_TARGET_DEAD = SIGUSR1,
    SIG_IPC_TARGET_BAD = SIGUSR1,
    SIG_IPC_MESSAGE_FLUSHED = SIGUSR1,
    SIG_MCA_BASE = SIGUSR1,
    SIG_MCA_READY = SIGUSR1,
    SIG_MCA_INQUIRE = SIGUSR1,
    SIG_MCA_COMPLETE = SIGUSR1,
    SIG_CMX_BASE = SIGUSR1,
    /* note that cmx adds buffer index to sig_cmx_base */
    SIG_VDCP_BASE = SIGUSR1,
    SIG_DVP_BASE = SIGUSR1,
    SIG_DAP_BASE = SIGUSR1,
    SIG_ADCP_BASE = SIGUSR1,
    SIG_DMA_BASE = SIGUSR1,
    SIG_SELECT_BASE = SIGUSR1,
    LastSigHandle /* Note: place new defs above here */
} SigHandle;
# endif

# endif

