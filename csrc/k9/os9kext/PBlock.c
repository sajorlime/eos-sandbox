/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, CA 95050 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: General K9 OS Process Blocking utility.
** 
** File: PBLock.c
** $Id: PBlock.c,v 1.8 1994/10/21 22:36:05 mikes Exp $
**
** Author: Emil Rojas
**
** Purpose: To provide an abstraction for process blocking.
** 
** Types/Classes: PBLock
**     
** Functions:
**
** block_initByName -- Allow you to specify the name of the lock
** you wish to link to (allowing the event to be distinct from
** the default one).
**
** block_init -- Link to a process block if it already exists or
** create it and return access to a resource to allow
** a process to block until an other process tells it to wake up.
** 
** pblock_destroy -- returns the resource to the void of the bit bucket.
** 
** 
** pblock_sleep -- puts the calling process to sleep until some other
** process wakes the it by calling pblock_wake with the same event value.
** 
** pblock_wake -- causes all process that have gone to sleep via pblock_sleep
** with the same "k9 resource" and event value as passed here to be woken.
**
** 
** 
** Notes: 
**  
** Assumptions:
**  An important assumption in this code is that the cost of using
**  a single global resource for block will not be very high.  It
**  this should turn out not be to true we will need a method of
**  associating events with process groups.
**
** References: Emil uses a similar technique for IPC.
**     
*/

# define __PBLOCK_C__
# define NO_PBLOCK_INLINE
# include "PBlock.h"


# define PB_EVENT_TYPE (MP_OWNER_READ + MP_OWNER_WRITE + \
                        MP_WORLD_READ + MP_WORLD_WRITE)

PBlock *
pblock_init (PBlock * pbp)
{
  return pblock_initByName (pbp, PBLOCK_EVENT_NAME);
}

PBlock *
pblock_initByName (PBlock * pbp, char * blockname)
{
	error_code error;

        /* if name not given, use default */
        if (!blockname)
          blockname = PBLOCK_EVENT_NAME; 

	/* Get the global event. */
	error = _os_ev_link (blockname, &pbp->ev);
	if (error)
	{
		error = _os_ev_creat
				(
					0, 0, PB_EVENT_TYPE, &pbp->ev, blockname, 0, MEM_ANY
				);
		if (error)
		{
			GLOBAL_MSG
			(
				("lock_create: Failed to create event. (%d)\n", error), DBG_WARN
			);
			return (PBlock *) 0;
		}
	}
	return pbp;
}

void
pblock_delete (PBlock * pbp)
{
	error_code error;

	if ((error = _os_ev_unlink (pbp->ev)) != 0)
	{
		GLOBAL_MSG
		(
			("pblock_delete: Failed to unlink event.\n"), DBG_WARN
		);
		return;
	}
}


int32
pblock_sleep (PBlock * plp, int32 event)
{
    int32 actualEvent = 0;
    signal_code sig = 0 ;

	 /* Note that if this process recieves a signal it will
	 ** return with a zero value, otherwise it returns with
	 ** the event value. */
     _os_ev_wait (plp->ev, &actualEvent, &sig, event, event);

    return actualEvent;
}


void
pblock_wake (PBlock * plp, int32 event)
{
	_os_ev_pulse (plp->ev, &event, 1);
}

