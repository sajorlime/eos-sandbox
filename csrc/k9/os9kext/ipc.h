
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** File: ipc.h - inter-process communications
**
** Purpose:
**  To provide the public and private interface to the IPC interface.
**
** Author: emil
** $Author: wul $
**
** Constants:
**  IPCMINBUFFERLEN -- the minimum size of an IPC buffer (nominally 128).
** Types/Clases:
**  MailHandle -- the thing the IPC messages are sent to. This is the
**  public name of a mail box.
**  MailBox -- a mail box is used to store messages until they are read.
**  IpcB -- this is the type that contains IPC messages.
**  IpcError -- enum for ipc errors.
**  IpcPriority -- enum for ipc priorities.
**
** Functions:
**  None
**
** Notes:
**   Limitations: a substantial assumption is made that this is a fully
**   integrated system and therefore all process's are required and a failure
**   of any process will bring down the system which includes this library
**   and its shared memory segment.  This effectively resets all the ipc
**   pointers. Therefore, this function attempts limited cleanup.
**   Additional work such as flushing the receive list,
**   clearing list event waits, and notifying other waiting processes
**   might be useful if this assumption were not true and a single process
**   fails.
**
**  The origonal version of this code write by Larry Stein.
**
** Usage:
**  The IPC library uses the IPCMem structure, stored inside the
**  default heap module (currently MMC_HEAP) which generally
**  should be loaded at intialization time by the program mmcinit. 
**
**  To set up a process to use the IPC system, you must [1] call 
**  glob_link (in globMMC.c) to make sure you are connected to the
**  shared memory (it should return a positive value). [2] call 
**  ipc_MailBoxCreate in order to create a new mail box in shared
**  memory.
**
** The user can control use of inline functions by defining the compile
** flag NO_INLINE or NO_IPC_INLINE.  Defining either of these will disable
** the use of the inline macros in the ipcprivate file, thus allowing the
** user to link with the library and to set break points at these functions.
**
** $Id: ipc.h,v 1.14 1995/11/04 02:03:39 wul Exp $
**
*/

# ifndef __IPC_H__
# define __IPC_H__

# include "ictvcommon.h"
# include <stddef.h>

# include "ipcpriority.h"


# define IPCMINBUFFERLEN 128



/* IpcError defintions */
typedef enum _IpcError
{
    IPCERR_NONE,
    IPCERR_UNABLE_TO_LINK_IPCMM,
    IPCERR_UNKNOWN_MAILBOX,
    IPCERR_UNKNOWN_REPLY_TYPE,
    IPCERR_ILLEGAL_BUFFER,
    IPCERR_NULL_BUFFER,
    IPCERR_ILLEGAL_MAILBOX,
    IPCERR_UNALLOCATED_BUFFER,
    IPCERR_DUPNAME,
    RPCERR_NO_HANDLER,
    LastIpcError
} IpcError;

/* IpcPriority defintions */
typedef enum _IpcPriority
{
    IPC_PRI_NORMAL,
    IPC_PRI_HIGH    /* (out of band) expedited ipc */
} IpcPriority;

/* IpcType defintions -- reply values are assumed to follow request values by
** one */
typedef enum _IpcType
{
    IPC_TYPE_ANY = 0,   /* matches all for ipc_Receive() */
    IPC_TYPE_REQUEST,   /* outgoing ipc message */
    IPC_TYPE_REPLY, /* response to IPC_TYPE_REQUEST */
    IPC_TYPE_SYNC_REQUEST,  /* outgoing synchronous ipc message */
    IPC_TYPE_SYNC_REPLY,    /* response to synchronous ipc message */
    IPC_TYPE_SYNC_DISPOSE,  /* Set after timeout waiting for sync message */
    RPC_TYPE_REQUEST,   /* outgoing rpc message */
    RPC_TYPE_REPLY, /* response to RPC_TYPE_REQUEST */
    RPC_TYPE_SYNC_REQUEST,  /* outgoing synchronous rpc message */
    RPC_TYPE_SYNC_REPLY,    /* response to synchronous rpc message */
    IPC_TYPE_RENDEZVOUS,    /* allows waiter to wait for rendezvous message */
    IPC_TYPE_RENDEZVOUS_REPLY, 
    IPC_TYPE_UNUSED = 0x737700
} IpcType;

# define IPC_IS_UNUSED(ipcp) ((ipcp->type & 0xffffff00) == IPC_TYPE_UNUSED)

# define IPC_REQUEST_TO_REPLY(type) ((IpcType) (type + 1))

#define IPC_RCV_NOBLOCK (0)
#define IPC_RCV_FOREVER (-1)

# define MBNAMESIZ 11


/* Define the MailHandle based on the private mailbox */
typedef struct _MailBox * MailHandle;

/* Define the Ipc based on the private IpcB struct */
typedef struct _IpcB Ipc;

/* ipcprivate.h contains constants and data structure that are considered
** private to the ipc subsystem, but need to be made across several IPC
** modules, or should not be accessible to a user. */

# include "ipcprivate.h"

/* Report and clear the last error */
int ipc_GetErrno (void);

/* ipc_MailBoxCreate -- must be called by the task before
** any other calls are made to the IPC system. Before any
** task tries to create a mail box the ipcinit program must
** successfully run.
** The caller passes in its ASCII name (procName), and
** the boolean values indicating that it wants to receive signals for
** normal and priority messages, respectivly. It receives back its
** own mail handle, unless some error occurs in which case it receives
** a NULL.  ipc_GetErrno can be called to determine the error.
** If the user wants to use the signal capabilities of the IPC system
** S/he should used the ICTV sighandler call to catch signals and 
** it should expect the values SIG_IPC_MSG_RCVD, and SIG_IPC_PRI_MSG_RCVD
** defined in sighandle.h (located in ICTV/libsrc/sigahandler/sighandle.h).
** Parameters:
**  procName -- ASCII name of mail box.
**  useNormSig -- flag to tell the ipc system as signal the task when
**      it receives a message.  Not recommeded for ordinary tasks.
**  usePrioritySig -- like useNormSig.
** Returns:
**  the mail handle of the caller.
*/
MailHandle ipc_MailBoxCreate
(
    char * procName, Priority pri, int useNormSig, int usePrioritySig
);

/* ipc_MailBoxDestroy -- unlinks from the ipcMM.
** Parameter:
**  mail handle to be destroyed.
*/
void ipc_MailBoxDestroy (MailHandle mh);

/* ipc_GetMailHandle -- is used by task to find other tasks by name.
** Parmeter:
**  procName -- ASCII name of mail block we want the handle for.
** Returns:
**  MailHandle of task
*/
MailHandle ipc_GetMailHandle (char * procName);

/* ipc_Flush -- allows the user to flush all pending message in its queue,
** all messages are returned to the message free list.
** Parameter:
**  sender -- qualifies the messages to flush, if sender is Null all
**      messages are flush, otherwise only those sent by sender are
**      flushed.
** Returns:
**  the number of flushed messages.
*/
int ipc_Flush (MailHandle sender);

/* icpNewBuffer -- is used to by a task to allocate a message buffer
** when it wants to send a message to some task. Only IpcBs 
** aquired this way can be used for sending a message.
** Returns: 
**  An ipc buffer pointer allocated out of shared memory.
*/
IpcB * ipc_NewBuffer (void);

/* ipc_FreeBuffer -- is used to free a message buffer after it has
** been received.  All buffers that have been received must be
** dispossed of in one of the following ways: it should be freed
** used in a reply, send, or forward.  Message can be saved on
** local list or in variable names until the appropriate actions
** have been taken, but care must be taken to not allow the buffer
** to leak.
** Parameter:
**  ipcp -- a pointer to an ipc buffer
*/
int ipc_FreeBuffer (IpcB * ipcp);

/* Functions for sending messages:
** There are four functions for delivering an IPC message to an
** other task. ipc_Send, ipc_Rpc, ipc_Reply, and ipc_Forward.  Each
** is intended for seperate purposes.  ipc_Send and ipc_SendSync
** initiates communication. ipc_Forward and ipc_Reply take a message
** deliver it to some other task. ipc_Reply returns it to the
** "sender" as identified in the buffer, while ipc_Forward sends
** it to an other task without modifying the sender.  The effect
** of a ipc_Forward is that the receiver may reply to the message
** without the intermediary needing to intervene.
*/
/* ipc_Send -- initiates an IPC message to the target mail handle.
** The send is done asynchronously.  This implies that delivery
** will occur when the receiving task is able to wake up.  In
** particular the recieving task will not run before the sending
** task if the sending task is of higher priority.  The receiving
** task will only wake if it is waiting for a message.  In addition
** unless the message has been defined to be a priority message (see
** below) the recieve may not wake if it is waiting for some other
** type of message.
** Parameters:
**  ipcp -- the message buffer being sent to target.
**  target -- the mail handle of the target task
** Returns:
**  0 iff ipcp was inserted into the target mail box, othersize it
**  returns an error code.
*/
int ipc_Send (IpcB * ipcp, MailHandle target);

/* ipc_SendSync -- is like ipc_Send except that it gaurantees that the
** reciever receives and replies to the message before the
** call returns (unless a timeout occurs). Use of ipc_SendSync assumes
** that some sort of client/server relationship has been established,
** and therefore that the caller is willing to be suspended to
** allow the server to do some work at its behest.  An SendSync request
** must be can timeout if ipc_SetRcvTimeout has been called.
** a timeout occurs a Null pointer is returned.
** Parameters:
**  ipcp -- the message buffer being sent to target.
**  target -- the mail handle of the target task
** Returns:
**  the return message. It should be the same ipc buffer.
*/ 

# ifndef ipc_SendSync
    IpcB * ipc_SendSync (IpcB * ipcp, MailHandle server);
# endif

/* ipc_Forward -- is use to delegate work to an other task.
** The only difference between ipc_Forward and ipc_Send is
** that ipc_Forward does not set the sender field of the ipc.
** Parameters:
**  ipcp -- the message buffer being sent to target.
**  target -- the mail handle of the target task
** Returns:
**  0 iff ipcp was inserted into the target mail box, othersize it
**  returns an error code.
*/
int ipc_Forward (IpcB * ipcp, MailHandle target);

/* ipc_Reply -- allow the receiver to respond to the sender of a message
** without "knowing" who the sender was.  It
** must be used by RPC servers to return synchronous messsages.
** Parameters:
**  ipcp -- the message buffer being replied to the sender.
*/
int ipc_Reply (IpcB * ipcp);


/* ipc_SetRcvTimeout -- sets the default timeout period, in miliseconds, for
** an ipc_Rcv, and ipc_SendSync.  If this functions is not called it will
** default to an infinite wait.
** Parameters:
**  mstimeout -- the timeout period (roughly) in miliseconds. Times below
**      four miliseconds are not valid. The is valid to only on part in
**      256.
*/
int ipc_SetRcvTimeout (int mstimeout);

/* ipc_Rcv -- is the default receive function for simple (normal) message
** retrieval.  It calls ipc_Receive with the appropriate parameters
** to effect a receive of the first message in the queue without
** regard for its sender or type.  It uses the parameter passed
** in ipc_SetRcvTimeout as the delay parameter to ipc_RcvDelay.
** note this function may be an inline macro.
** Parameters:
**  pointer to a ipc buffer that was sent to the calling task.  If
**  mstimeout expirers, or the task is otherwise woken a Null will
**  be returned.
** Returns:
*/
IpcB * ipc_Rcv (void);

/* ipc_RcvMSDelay -- is like ipc_Rcv except that it lets the user
** specificy the msdelay time directly.
** Parameters:
**  msdealy -- the timeout period (roughly) in miliseconds. Times below
** Returns:
**  pointer to a ipc buffer that was sent to the calling task.  If
**  msdelay expirers, or the task is otherwise woken a Null will
**  be returned.
*/
IpcB * ipc_RcvMSDelay (long msdelay);

/* ipc_Receive -- is the call though which a task gets qualified
** messages from other tasks.  The caller can qualify the messages
** it receives in two ways. One way is by the sender, and the second is by
** the type. The user can also specify a timeout/delay parameter.
** if msdelay is less than 0 it will wait forever for the reception
** of a message, if it is zero it will not wait at all, and if msdelay
** is greater than zero it will wait approximatly that many miliseconds
** before returning with a Null pointer if no qualified message is
** received.
** Note: This is a dangerous call if called with an infinite timeout.
** The task may not wake even though messages are waiting.
** Parameters:
**  sender -- qualifies the received message.
**  type -- qualifies the received message.
**  msdelay -- sets a msdelay for the message receive.
** Returns:
**  pointer to a ipc buffer that was sent to the calling task.  If
**  msdelay expirers, or the task is otherwise woken a Null will
**  be returned.  The buffer returned will either be Null or be
**  qualifed by the sender and the type.  This is true unless there
**  is a priority message in the queue. A priority message will always be
**  returned.
*/ 
IpcB * ipc_Receive (MailHandle fromSender, IpcType ofType, long msDelay);

/*
** ipc_Priority -- allows the message receiver to determing the priority
** of the message it received. 
** note this function may be an inline macro.
** Parameters:
**  ipcp -- buffer to retieve priority from.
** Returns:
**  return priority at which buffer was sent.
*/
# ifndef ipc_GetPriority
IpcPriority ipc_GetPriority (IpcB * ipcp);
# endif

/* ipc_SetPriority -- allows the sender to set the priority of a message
** note this function may be an inline macro.
** Parameters:
**  ipcp -- buffer to set priority in.
*/
# ifndef ipc_SetPriority
void ipc_SetPriority (IpcB * ipcp, IpcPriority pri);
# endif

/* ipc_GetSender -- allows a message receiver to determine whihc task sent
** it the message in question.
** note this function may be an inline macro.
** Parameters:
**  ipcp -- buffer to retieve priority from.
** Returns:
**  return sender of buffer that was received.
*/
# ifndef ipc_GetSender
MailHandle ipc_GetSender (IpcB * ipcp);
# endif

/* ipc_GetMaxSize -- allows the receiver to determine the maximum number
** of byte that can be shoved into a single message.  The minumum size
** for this is IPCMINDATALEN (currently 128 bytes).
** Parameters:
**  ipcp -- buffer to retieve size from.
** Returns:
**  return max size of buffer.
*/
# ifndef ipc_GetMaxSize
size_t ipc_GetMaxSize (IpcB * ipcp);
# endif

/* ipc_GetMsgLen -- allows the receiver to determine the number of bytes in
** the message.  This called is only needed when the user type in the
** message indicated some variable size.
** note this function may be an inline macro.
** Parameters:
**  ipcp -- buffer to retieve message size from.
** Returns:
**  return message size of buffer.
*/
# ifndef ipc_GetMsgLen
size_t ipc_GetMsgLen (IpcB * ipcp);
# endif


/* ipc_SetMsgLen -- is used by the sender when it is sending information that
** may be of variable size.  The sender is responsible for ensuring that
** the data sent does not exceed IPCDATALEN.  If messages with larger
** memory requirements are needed the sender should send pointers to
** shared memory (note that some protect must then be implemented to
** ensure that the send and recevier do not access the data simultanously),
** or the sender should break the message across several message buffer
** relying on the FIFO nature of the IPC system, user type fields and
** other data in the message to ensure proper delivery.
** note this function may be an inline macro.
** Parameters:
**  ipcp -- buffer to set message size in.
*/
# ifndef ipc_SetMsgLen
    void ipc_SetMsgLen (IpcB * ipcp, size_t size);
# endif

/* ipc_GetType -- allows the recevier to determine the type of message receveied.
** Parameters:
**  ipcp -- buffer to get message type from.
** Returns:
**  return message type of buffer.
*/
# ifndef ipc_GetType
IpcType ipc_GetType (IpcB * ipcp);
# endif

/* ipc_GetUserType -- allows to user to determine the value of the user
** type field.  Note this function may be an inline macro.
** Parameters:
**  ipcp -- buffer to get message user type from.
** Returns:
**  return message user type of buffer.
*/
# ifndef ipc_GetUserType
    int ipc_GetUserType (IpcB * ipcp);
# endif

/* ipc_SetUserType -- allows the sender to set the user type field.
** note this function may be an inline macro.
** Parameters:
**  ipcp -- buffer to set message user type from.
*/
# ifndef ipc_SetUserType
    void ipc_SetUserType (IpcB * ipcp, IPCUser utype);
# endif


/* ipc_GetDataP -- allows the sender to set the user type field.
** note this function may be an inline macro.
** Parameters:
**  ipcp -- buffer to set message user type from.
** Returns:
**  pointer to data area of ipc.
*/
# ifndef ipc_GetDataP
    void * ipc_GetDataP (IpcB * ipcp);
# endif

/* mh_getProcId -- returns OS process identifier. */
# ifndef mh_getProcId
#	define mh_getProcId(mh) (mh->procID)
# endif

/* mh_getPriority -- returns OS priority. */
# ifndef mh_getProcId
#	define mh_getProcId(mh) (mh->procPri)
# endif

/* functions to validate mail handle */
# ifndef mh_isAlive
#	define  mh_isAlive(mh) (mh && mh->rootp)
# endif

/* return the ipc internal process identifier */
int ipc_getProcId (void);
/* return the ipc internal process priority */
int ipc_getProcPri (void);

/* Utility functions for printing ipc internal types */
char * ipc_UserTypeString (unsigned int type);
char * ipc_TypeString (IpcType type);
char * ipc_PriorityString (IpcPriority pri);
char * ipc_ErrorString (IpcError err);

/* debug function */
int ipc_testPriority (void);

/* osBlock and osUnblock are general untility functions
** that allows at task to gain exclusive access from other
** tasks for a short period of time.  They should be used
** to gain exclusive access to simple data structures that
** are "seldom accessed."  E.g. the MMC Control register.
*/
void osBlock (void);
void osUnblock (void);


/*
** ipc_sendRendezvous -- sends a rendezvous ipc type, no data is sent.
** The message is sent synchornously.  It can fail if the time of the
** sender does not allow the receive time to reply, or the sender is
** woken by some signal.
*/
int ipc_sendRendezvous (MailHandle mh);

/*
** ipc_waitForRendezvous -- allows the caller to wait a rendezvous
** message from a particular process.
** Parameters:
**  fromMH -- specifies the particular sender that the caller is waiting
**  for.  A zero value allows any task to send the rendezvous message.
**  msDelay -- specifies the number of milliseconds to wait. -1 waits
**  forever.  Zero does not wait at all.
** Returns:
**  MailHandle of message sender, O if timed out or woke for some other 
**  reason.
*/
MailHandle ipc_waitForRendezvous (MailHandle fromMH, long msDelay);

/*
** ipc_nameRendezvous -- waits for upto oneSecondRetires seconds
** for the mailNamep to be a vailid mail name.
** Parameters:
**  The sign bit on oneSecondRetires is ignored allowing a -1 to
**  passed for effectively infinite.
*/
MailHandle ipc_nameRendezvous (char * mailNamep, int oneSecondRetries);

# endif

