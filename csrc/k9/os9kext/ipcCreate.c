
/*
**
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television mmc
**
** File: ipcinit.c - stand alone program initialize ipc stuff.
**
** Author: emil
** $Author: mikes $
**
** Purpose:
**  This file produces a stand alone executable that intializes the IPC Memory
**  Module (IPCMM)
**
** Types/Clases:
**
** Program:
**  void main (int argc, char ** argv) -- 
**  Usage: ipcinit [-B buffersize] [-N numberofbuffer] [-M numberofmailboxes]
**                      [-F IPCMMName]
**  buffersize, numberofbuffers, and numberofmailboxes default to the numbers
**  IPCMMName defaults to "IPCMM".
**  ipc.h.
**
** Notes:
**
** $Id: ipcCreate.c,v 1.7 1994/10/21 22:36:15 mikes Exp $
**
*/

# define __IPCCREATE_C__
# include <assert.h>
# include <errno.h>
# include <signal.h>
# include <stddef.h>
# include <stdlib.h>
# include <stdio.h>

/* OS9K Inlcudes */
# include <memory.h>
# include <modes.h>
# include <module.h>
# include <types.h>
# include <process.h>
# include <sysglob.h>

# include "ipc.h"
# include "ictvutil.h"


# define  SIZEOF_IPC (sizeof (IpcB))



/*
** void memInit - initialize the shared memory area and events
** 
** memInit takes four parametes that by default are generally expected
** to be zero.  For each parameter if it is zero it check to see if
** a value has been set in the evironment for it.  If is zero and
** there is no environment value for it sets it to the default defined
** in ipc.h.
*/
static int 
memInit (IpcMem * ipcmp)
{
    int i;
    IpcB * ipcp;
    IpcB * lipcp;
    int t;

    if (!ipcmp)
        return 0;

    /* the shared memory is organized with: a header of misc info,
    ** followed by the mailboxes, and finally the ipc buffers
    ** This would like struct ipcstuff (below) as a structure definition.
    **  struct ipcstuff
    **  {
    **      IpcMem ipcMem;
    **      MailBox mailboxes[maxMailboxes];
    **      IpcB ipcs[maxIPCs]; -- these need to be linked
    **  };
    */
    ipcmp->self = ipcmp;
    ipcmp->maxMailboxes = sizeof (ipcmp->boxes) / sizeof (ipcmp->boxes[0]);
    ipcmp->maxIPCs = sizeof (ipcmp->ipcs) / sizeof (ipcmp->ipcs[0]);
    lock_createByName(&(ipcmp->IPCLock), 1, IPCLOCKNAME);
    /* waitingProcs and mnum are debugging hooks */
    ipcmp->waitingProcs = 0;
    ipcmp->mnum = 1;

    ipcmp->mailboxes = ipcmp->boxes;

    ipcmp->ipcFreeList = ipcp = ipcmp->ipcs;

    for (i = ipcmp->maxIPCs; i > 0; i--) /* allocate all ipc buffers */
    {
        lipcp = ipcp;
        lipcp->next = ++ipcp;
        lipcp->self = lipcp;
        lipcp->dataSize = IPCBUFFERLEN;
        lipcp->sanity1 = SANITY1;
        lipcp->sanity2 = SANITY2;
        lipcp->type = IPC_TYPE_UNUSED;
        lipcp->priority = 0;
        lipcp->sender = 0;
        lipcp->target = 0;
        lipcp->dataLen = 0;
        lipcp->mnum = 0;
        lipcp->userType = IPC_TYPE_UNUSED;
        lipcp->data[0] = 0;
    }
    lipcp->next = 0;

# ifdef IPC_LOG
	ipcmp->di = 0;
# endif

    /* a single event is allocated for use by the ipc library to
    ** indicate: 1) a new ipc placed on a empty mailbox 2) a linked
    ** list being waited on is now free This is accomplished using
    ** the calling process's mailbox index as the min and max value
    ** in the event wait call.  For the indications above, the event
    ** values used are: 1) index of process's mailbox, i.e. zero to
    ** maxMailboxes-1 2) process's index plus maxMailboxes When the
    ** event occurs a large value is used to force the value of the
    ** event off into a unused range to indicate that the current
    ** event has been serviced and prevent a spurious but valid event
    ** value from occurring. */

    if ((errno = _os_ev_link (IPCEVENTNAME, &ipcmp->ipcEvent)) == 0)
    {
        LOCAL_MSG
        (
            ("Event ID: %x->%x\n", &ipcmp->ipcEvent, ipcmp->ipcEvent),
            DBG_K9EXT_MASKINDEX, DBG_MISC
        );
        t = 0;
        while ((errno = _os_ev_unlink (ipcmp->ipcEvent)) == 0)
        {
            errno = _os_ev_delete (IPCEVENTNAME);
            if (!errno)
                break;
            t++;
        }
    }
    /* should be changed to a PrcessLock? */
    errno = _os_ev_creat
            (
                0, /* wait auto-increment */
                0, /* signal inc */
                S_IREAD + S_IWRITE + S_IOREAD + S_IOWRITE,
                &ipcmp->ipcEvent, IPCEVENTNAME,
                -1, MEM_ANY /* initial value, memory color */
            );
    if (errno)
    {
        GLOBAL_MSG
        (
            ("_os_ev_create failed:%d\n", errno), DBG_ASSERT_LEVEL
        );
    }
}




/*
** void ipc_create (void)
*/

int
ipc_create (IpcMem * ipcmp)
{
# ifdef SET_TIME_SLICE
    u_int16 tslice = PRI_SLICE;
# endif
    u_int16 maxage = PRI_MINIMUM;
    glob_buff t;
    int i;

    t.wrd = maxage;
    i = _os_setsys (offsetof (sysglobs, d_maxage), sizeof maxage, t);
    if (i)
    {
        GLOBAL_MSG
        (
            ("Failed to set d_maxage -- ec:%d\n", i), DBG_ASSERT
        );
        return 1;
    }
# ifdef SET_TIME_SLICE
    t.wrd = tslice;
    i = _os_setsys (offsetof (sysglobs, d_tslice), sizeof tslice, t);
    if (i)
    {
        GLOBAL_MSG
        (
            ("Failed to set d_tslice -- ec:%d\n", i), DBG_ASSERT
        );
        return 1;
    }
# endif
    i = _os_setpr (getpid(), PRI_BLOCK);
    if (i)
    {
        GLOBAL_MSG
        (
            ("Failed to set BLOCK -- ec:%d\n", i), DBG_ASSERT
        );
        return 1;
    }
    return memInit (ipcmp);
}

