
/*
** Copyright (C) 1994,	ICTV Inc.
**						280 Martin Ave.,
**						Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv -- multimedia controller.
**
** File: k9misc.h -- 
**
** Author: scott, adapted by BDN
**
** Purpose:
**  This module, provides a simplified interface to 9K fork/exec.
**
** Types/Clases:
**
** Notes:
**
** $Id: k9misc.h,v 1.9 1995/11/04 02:03:42 wul Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/k9misc.h,v $
**
*/

# ifndef __K9MISC_H__
# define __K9MISC_H__

#include <types.h>
#include <INET/in.h>
#include <bootp.h>
# include "k9Module.h"
# include "k9fork.h"

u_int32 k9_getpriority(void) ;
process_id getppid (void) ;

# define getpriority k9_getpriority
void k9_defer (void);

/* 
 * NOTE: this function now returns the ip address in HOST order.
 * Older versions that called the device driver returned the
 * address in network order. If NULL is passed as the deviceName,
 * the code will default to the device "/e30". srw
 */
u_long getLocalIPAddr(char *deviceName) ;

/*
 * Returns boot packet from rom.
 */
struct bootp *getBootp(void);

extern int32 getRomVersion(void);

int32 getCfgEndPoint(struct in_addr *cfgAddr, u_int32 *cfgPort) ;

#define CLOCKSPERSEC	1193000
#define NSECSPERCLOCK   838
#define COUNTDOWNCYCS   119
#define PIT1		0x40
#define PIC_PC_ADDR		0x20
#define PIC_TIMER_INTR	1
#define IRR_SELECT		2
#define TICKSPERSEC	100
#define CLOCKSPERTICK   (CLOCKSPERSEC/TICKSPERSEC)
#define	CR_SC0		0
#define PIT_TIMER0	0
#define PIT_CONTROL	3

int k9_getClockTicks(void) ;
int k9_picTimerInterruptPending(void);

#define k9_getDeltaNanoSecs() (NSECSPERCLOCK * (CLOCKSPERTICK - k9_getClockTicks()))
#define k9_getDeltaMicroSecs() (k9_getDeltaNanoSecs()/1000)


# ifndef K9_MSG_LEVEL
#   define K9_MSG_LEVEL DBG_ASSERT
# endif
# define K9_ASSERT(params) LOCAL_MSG (params, DBG_K9EXT_MASKINDEX, DBG_ASSERT)
# define K9_WARN(params) LOCAL_MSG (params, DBG_K9EXT_MASKINDEX, DBG_WARN)
# define K9_MISC(params) LOCAL_MSG (params, DBG_K9EXT_MASKINDEX, DBG_MISC)
# define K9_TRACE(params) LOCAL_MSG (params, DBG_K9EXT_MASKINDEX, DBG_TRACE)
# define K9_MSG(params) LOCAL_MSG (params, DBG_K9EXT_MASKINDEX, K9_MSG_LEVEL)

#endif

