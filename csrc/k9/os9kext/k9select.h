/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, CA 95050 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: ictv interactive television FOO
** 
** File: select.h
**     $Id: k9select.h,v 1.2 1995/11/04 02:03:43 wul Exp $
**     $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/k9select.h,v $
**
** Author: Bruce D. Nilo
**
** Purpose:
**     OS9K compatibility function
** 
** Types/Classes:
**     fd_set
** 
** Functions:
**     select
** 
** Assumptions/Limitations:
**     1. Maximum descriptor number supported is 31.
**     2. installSelectHandler needs to be called prior to data arriving on an
**     fd.
**     3. Once the fd is ready, and prior to data arriving on it, an initial
**        _os_ss_sendsig should be made on that fd with a signal value of
**        SIG_SELECT_BASE+fd.
**     4. Only read events are supported. The other fd_sets are for compatibility.
** 
** Notes:
**     
** 
** References:
** 
*/

#ifndef __K9SELECT_H__
#define __K9SELECT_H__

#include "timestamp.h"
#include "EventMgr.h"

#define FD_MAX_NUMBER 31
#define TICKS_PER_SECOND 100 /* OS9K Default */
#define USECS_PER_TICK 10000

/* Returns -1 if fdmax > 31. Otherwise the number of fds with pending reads.
 * If 0 and ticks was non null then the tick interval specified expired. 
 * If ticks was non zero and event occurred then the time remaining in the
 * tick count is returned in ticks. 
*/

extern int select(int fdmax, 
		  fd_set *readfds, 
		  fd_set *ignored1, 
		  fd_set *ignored2, 
		  Timestamp_t *timeout);

/* Must be called prior to data arriving on any fd, and the first select call. */
void installSelectHandler(void) ;


/* Same semantics as under UNIX. */
#define FD_SET(fd,fdset) ((*fdset) |= (1<<(fd)))
#define FD_CLR(fd,fdset) ((*fdset) &= ~((1<<(fd))) )
#define FD_ISSET(fd,fdset) ((*fdset) & ((1<<(fd))) )
#define FD_ZERO(fdset) ((*fdset) = 0)


#endif

