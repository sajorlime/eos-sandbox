
/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, Calif. 95050-4320.
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** Project: ictv interactive television using OS9K, IPC Tools
**
** File: ~dev/ICTV/libsrc/ipcPrint.c
**
** Author: emil
**
** Purpose:
**  Tools to print the values of IPC things.
**
** Types/Clases:
**
** Data
**
** Functions:
**
** Notes:
**  This is to be used for debugging only.
**
** Overview:
**
** $Id: ipcPrint.c,v 1.12 1995/06/25 22:28:23 wul Exp $
** $Source: /import/cvsfiles/ICTV/./libsrc/os9kext/ipcPrint.c,v $
**
*/

# include <stdio.h>
# include <signal.h>
# include <ctype.h>
# include <string.h>

# include "ictvutil.h"
# include "ictvcommon.h"
# include "ipc.h"
# include "ipcscreen.h"
# include "rpcServer.h"


# define __IPCPRINT_C__

IpcMem * ipc_GetRoot (void);

static char sane1buf[8];
static char sane2buf[8];

static char *
sanetostring (int sane, char * bp)
{
    *((int *) bp) = sane;
    bp += 4;
    *bp = 0;
    return bp;
}




static char *
sfIpcSender (IpcB * ipcp)
{
	IpcMem * ipcmp = ipc_GetRoot ();

    if
    (
        ((ipcp) >= ipcmp->ipcs)
        && ((ipcp) < &ipcmp->ipcs[IPCNUMBER])
    )
    {
        if (ipcp->sender)
            return ipcp->sender->name;
        return "None";
    }
    return "Sender Bad IpcB";
}

static int
sfIpcSenderId (IpcB * ipcp)
{
	IpcMem * ipcmp = ipc_GetRoot ();

    if
    (
        ((ipcp) >= ipcmp->ipcs)
        && ((ipcp) < &ipcmp->ipcs[IPCNUMBER])
    )
    {
        if (ipcp->sender)
            return ipcp->sender->procID;
        return -1;
    }
    return -2;
}

static char *
sfIpcTarget (IpcB * ipcp)
{
	IpcMem * ipcmp = ipc_GetRoot ();

    if
    (
        ((ipcp) >= ipcmp->ipcs)
        && ((ipcp) < &ipcmp->ipcs[IPCNUMBER])
    )
    {
        if (ipcp->target)
            return ipcp->target->name;
        return "None";
    }
    "Target Bad IpcB";
}


void
ipc_printIPCMem (IpcMem * ipcmp, int verbose)
{
    printf
    (
		verbose ?
        "  IPC Memory Pointer :%08x points to ->\n"
		" {\n"
		"    self:%x\n"
        "    maxMailbloxes:%d, maxIPCs:%d, mailboxes->%x, inuseMailboxes:%d\n"
        "    ipcFreeList->%x, Semaphore:%d, msg num:%d, ipcEvent->%x\n"
        "  }\n"
		:
        "  ipcmp:%08x -> {self:%x\n"
        "    mMB:%d, mIPCs:%d, mbs->%x, inuseMBs:%d\n"
        "    ipcFL->%x, aS:%d, mn:%d, ipcEvent->%x\n"
        "  }\n",
        ipcmp, ipcmp->self,
        ipcmp->maxMailboxes, ipcmp->maxIPCs,
        ipcmp->mailboxes, ipcmp->inuseMailboxes,
        ipcmp->ipcFreeList,
        ipcmp->IPCLock.accessSemaphore, ipcmp->mnum, ipcmp->ipcEvent
    );
}

void
ipc_printIPCData (IpcB * ipcp, int verbose)
{
	int len;
	int i;
	
	len = ipcp->dataLen;
	if (len == 0)
	{
		if (!verbose && isprint (ipcp->data[0]))
		{
			printf ("%s\n", ipcp->data);
			return;
		}
		else
		{
			if (verbose)
				len = sizeof (ipcp->data);
			else
				len = 16;
		}
	}
	for (i = 0; i < len; i++)
	{
		printf ("%02x ", ipcp->data[i] & 0xff);
		if ((len & 0xf) == 0xf)
			printf ("\n");
		else if ((len & 0x3) == 0x3)
			printf (" ");
	}
	printf ("\n");
	fflush (stdout);
}
        

void
ipc_printIPCB (IpcB * ipcp, int verbose)
{
	if (!ipcp)
	{
		printf ("null ipc\n");
		return;
	}
	if (((u_int) ipcp) > 0x800000)
	{
		printf ("ipc is big 0x%x\n", ipcp);
		return;
	}
	printf
	(
		verbose ?
		"IPC Buffer Pointer:%08x ->\n"
		"{\n"
		"   self->%x, next->%x, ipc type:%s  ipc priority:%s\n"
		"   sender is->%s, target is->%s\n"
		"   data Size:%d, data Len:%d, sane1:%s, sane2:%s\n"
		"   msg num:%d, user Type:%s, data:"
		"ipc data:"
		"}\n"
		:
		"ipcp:%08x ->{\n"
		"   slf->%x, nxt->%x, t:%s  p:%s\n"
		"   sndr->%s, trgt->%s\n"
		"   dS:%d, dL:%d, s1:%s, s2:%s\n"
		"   mnum:%d, uT:%s, data:\n"
		"}\n",
		ipcp,
		ipcp->self, ipcp->next,
		ipc_TypeString (ipcp->type), ipc_PriorityString (ipcp->priority),
		sfIpcSender (ipcp),
		sfIpcTarget (ipcp),
		ipcp->dataSize, ipcp->dataLen,
		sanetostring (ipcp->sanity1, sane1buf),
		sanetostring (ipcp->sanity2, sane2buf),
		ipcp->mnum, ipc_UserTypeString (ipcp->userType)
	);
	ipc_printIPCData (ipcp, verbose);
}




void
ipc_printMailbox (MailBox * mbp, int verbose)
{
    char sRoot[16];
    char sWaiting[16];
    char sSig[32];

    if (!(*mbp->name))
        return;
    if (mbp->rootp)
        sprintf (sRoot, "%x", mbp->rootp);
    else
    {
        sprintf (sRoot, "%s", "DEAD");
    }
    if (mbp->swaiting)
	{
		if (((int) mbp->swaiting) == -1)
			strncpy (sWaiting, "Woken", sizeof (sWaiting));
		else if (verbose)
			strncpy (sWaiting, mbp->swaiting->name, sizeof (sWaiting));
		else
			strncpy (sWaiting, mbp->swaiting->name, 3);
	}
    else
		if (verbose)
			strcpy (sWaiting, "Not waiting");
		else
			strncpy (sWaiting, "NWT", 3);
	if (verbose)
		strcpy (sSig, "No signal on");
	else
		strcpy (sSig, "NOSIG");
    if (mbp->useNormalSig || mbp->usePrioritySig)
    {
        if (mbp->useNormalSig && mbp->usePrioritySig)
        {
			if (verbose)
				strncpy (sSig, "Signal on NORMAL & PRIORITY", sizeof (sSig));
			else
				strcpy (sSig, "NR&PR");
        }
        else
        {
            if (mbp->usePrioritySig)
				if (verbose)
					strcpy (sSig, "Signal on PRIORITY");
				else
					strcpy (sSig, "PRIOR");
            else
				if (verbose)
					strcpy (sSig, "Signal on NORMAL");
				else
					strcpy (sSig, "NORMA");
        }
    }
    if (verbose)
    {
	    printf
	    (
		"MailBox:%08x ->\n"
			"{\n"
		"	Name:%11s, PID:%d, proc priority:%d,\n"
			"	%s Messages\n"
		"	wait for process:%s, "
				"wait for type:%s, msg num:%d,\n"
			"	head->%x, "
				"tail->%x, "
				"root back ptr/flag:%s\n"
		"}\n",
		mbp,
			mbp->name,
			mbp->procID,
			mbp->procPri,
		sSig,
			sWaiting,
			ipc_TypeString (mbp->twaiting),
			mbp->msg,
		mbp->head,
			mbp->tail,
			sRoot
	    );
    }
    else
    {
	    printf
	    (
		"%12s:%d@%x,%s,ws:%3s,wt:%s,m:%d,h:%x,t:%x,r:%s\n",
			mbp->name,
			mbp->procID,
			mbp->procPri,
		sSig,
			sWaiting,
			ipc_TypeString (mbp->twaiting),
			mbp->msg,
		mbp->head,
			mbp->tail,
			sRoot
	    );
    }
}



int
ipc_testPriority (void)
{
    process_id procID;
    u_int16 pri, age, group, user;
    int32 sched;
    int r;

    r = _os_id (&procID, &pri, &age, &sched, &group, &user);
    if ((u_int32) pri > (u_int32) ipcfriend_getProcPri ())
    {
        GLOBAL_MSG
		(
			(
				"Priority Error: %x/%x\n", pri,
				ipcfriend_getProcPri ()
			),
			DBG_ASSERT
		);
		return 1;
    }
    return 0;
}

char *
ipcfriend_sender (IpcB * ipcp)
{
	return sfIpcSender (ipcp);
}

int
ipcfriend_senderId (IpcB * ipcp)
{
	return sfIpcSenderId (ipcp);
}

char *
ipcfriend_target (IpcB * ipcp)
{
	return sfIpcTarget (ipcp);
}

char *
ipcfriend_type (IpcB * ipcp)
{
	return ipc_TypeString (ipcp->type);
}

char *
ipcfriend_userType (IpcB * ipcp)
{
	return ipc_UserTypeString (ipcp->userType);
}


char *
ipcfriend_priority (IpcB * ipcp)
{
	return ipc_PriorityString (ipcp->priority);
}


char *
rpcfriend_sender (void * dp)
{
	return ipcfriend_sender (RPC_TO_IPC (dp));
}

int
rpcfriend_senderId (void * dp)
{
	return ipcfriend_senderId (RPC_TO_IPC (dp));
}

char *
rpcfriend_target (void * dp)
{
	return ipcfriend_target (RPC_TO_IPC (dp));
}

char *
rpcfriend_type (void * dp)
{
	return ipcfriend_type (RPC_TO_IPC (dp));
}

char *
rpcfriend_userType (void * dp)
{
	return ipcfriend_userType (RPC_TO_IPC (dp));
}


char *
rpcfriend_priority (void * dp)
{
	return ipcfriend_priority (RPC_TO_IPC (dp));
}

