/*
** Copyright (C) 1994, ICTV Inc. 280 Martin Ave., Santa Clara, CA 95050 
**
** Permission to copy given to and distribute give to Lapel Software 1997.
** 
** Project: General K9 lock facility
** 
** File: ProcessLock.h
**
** $Id: ProcessLock.h,v 1.11 1994/12/01 12:17:39 emil Exp $
** $Sourc$
**
** Author: Bruce D. Nilo
**
** Purpose: To provide an abstraction for a resource lock.
**     
** Types/Classes: ProcessLock
**     
** Functions:
**
**    Create a lock and link the ProcessLock structure to the
**    event created. If no name given or lock_create is called
**    instead of lock_createByName, the default lock is used.
**    lock_create and lock_createByName can take a preallocated
**    lock structure and intialize, or it can get the memory from the 
**    memory manager.
** 
** ProcessLock* lock_createByName (ProcessLock * plp, int resourceCount, char * name)  ;
** ProcessLock* lock_create (ProcessLock * plp, int resourceCount)  ;
**
**    Create a lock which can be accessed by name. If ptr is NULL
**    shared memory is not created for the lock. On K9 name is used
**    to access the lock (if ptr is non null). Returns a pointer
**    to a ProcessLock or NULL on failure.
**    lock_createMM puts name in the global name space and lets other
**    link to it by name using lock_accessMM.
** 
** ProcessLock * lock_createMM (char * name, int resourceCount);
** 
**    Provides a facility to access a lock by name. It must have been created
**    by lock_createMM.
**    It will call to _os_link. If it finds the data item it returns
**    a pointer to it, otherwise it returns null.
** 
** ProcessLock* lock_accessMM(char* name) ;
** 
**    Given a lock cleans up any global structure associated with it. If
**    lock_access was used then the name passed into that call should be
**    passed here. Otherwise name should be null.
** 
** void lock_destroy(ProcessLock* lock, char* name) ;
** 
**    Attempts to acquire the lock. Blocks on an _os_event call if another
**    task has the lock. A critical section is bracketed by the lock_acquire and lock_release
**    calls.
** 
** void lock_acquire(ProcessLock *lock) ;
** 
**    Releases a lock that has been previously acquired by the lock_acquire
**    call. A critical section is bracketed by the lock_acquire and lock_release
**    calls.
** 
** void lock_release(ProcessLock *lock) ;
**
**
** Usage:
** 
** Create a lock with the lock_access or lock_create call. Use it
** via the lock_acquire and lock_release calls. When finished with the
** lock call lock_destroy.
**     
** 
** Assumptions: Assumes that incrementing and decrementing an integer is
**      an atomic operation.
**     
** 
** Limitations: (See assumptions.)
**     
** Notes: The Lock uses a simple technique. To acquire the lock it
** increments an integer flag. If it is zero it has the lock, if it is
** greater than zero another process has acquired the lock and it does an
** _os_event_wait. It is waiting for the event's value to be equal to the
** address of the lock. When the owning task releases the lock it
** decrements the integer flag. If it is >= 0 it sets the integer flag to
** -1 and pulses the event with the address of the lock waking ALL tasks
** waiting for this lock. Based on their scheduling priorities one of
** these task will succeed to get the lock and the others will block
** again or get the lock if the critical section of the succeeding task
** executes to completion during its time slice. This object only used
** one global event_id object. Locks share this event by waiting for
** different events.
** 
** References: Emil uses a similar technique for IPC.
**     
** 
*/


# ifndef __PROCESSLOCK_H__
# define __PROCESSLOCK_H__


# include "ictvcommon.h"
# define NO_PBLOCK_INLINE
# include "PBlock.h"
# include <events.h>
# include <errno.h>
# include <signal.h>
# include <memory.h>

#define LOCKDEBUG

#define LOCK_UNAVAIL_VALUE 0 

typedef struct _ProcessLock
{
    PBlock pblock;
    int accessSemaphore;
#ifdef LOCKDEBUG
    u_int32 owningtask ;
#endif
} ProcessLock;

/* lock_create, lock_createByName -- Create a lock which can be accessed by name.
**  This links your lock structure to an event_id based on the name you pass in
**  (if no name given or lock_create is used, it uses the default lock).
**  If ptr is null **   shared memory is not created for the lock. On K9 name is used
**  to access the lock (if ptr is non null). Returns a pointer
**  to a ProcessLock or NULL on failure.
** Parameters:
**  lockp -- pointer to a process lock, if not lock it is is intialized. If it is
**  null that the parameter name is used to create a named resource.
** or
**  name -- an ASCII used if lockp is null.
**  resourceCount -- the initial condition of the lock.
*/
ProcessLock * lock_createByName (ProcessLock * lockp, int resourceCount, char * name);
ProcessLock * lock_create (ProcessLock * lockp, int resourceCount);

/*
** lock_createMM -- Create a memory module for the lock to reside in.
**                  Then call lock_createByName. Returns pointer to the lock.
*/

ProcessLock * lock_createMM (char * name, int resourceCount);


/* Initializes a lock. May soon be augmented to provide cleanup
functionality as well. BDN*/
ProcessLock *lock_initByName(ProcessLock *lockp, char * name);
ProcessLock *lock_init(ProcessLock *lockp) ;

/* Provides a facility to access a lock by name. Hides the details
   of the _os_datamod call. It will call to _os_link or _os_datamod
   as necessary. If a ProcessLock structure has been allocated in
   another chunk of shared memory it can be initialized by the lock_create
   call, or the lock_createByName call, and this call would not be
   required. This call is only needed if you use lock_createMM to
   create your lock, and then, only by the processes that did not
   create the lock. Returns a pointer to the lock
   on success, NULL otherwise. */

ProcessLock* lock_accessMM(char* name) ;

/* Given a lock cleans up any global structure associated with it. If
   lock_access was used then the name passed into that call should be
   passed here. Otherwise name should be null. */

void lock_destroy(ProcessLock* lock, char* name) ;

/* Attempts to acquire the lock. Blocks on an _os_event call if another
   task has the lock. A critical section is bracketed by the lock_acquire and lock_release
   calls.  The name of the lock is not needed after creating it -- it knows which
   lock you are using by the data now stored in the lock structure. */

void lock_acquire(ProcessLock *lock) ;

/* Releases a lock that has been previously acquired by the lock_acquire
   call. A critical section is bracketed by the lock_acquire and lock_release
   calls. */

void lock_release(ProcessLock *lock) ;

/* Forces a lock to the unlocked state */

void lock_force (ProcessLock * lockp);

/*
** lock_pblockp -- gives access to the under PBLock member, implemented
** inline only.
** PBlock * lock_pblockp (Lock * lockp)
*/
# define lock_pblockp(lockp) (&(lockp)->pblock)

# endif

