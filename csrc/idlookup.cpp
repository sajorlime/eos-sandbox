
/* Measurement Engine
 * Copyright (C) 2020 NavOne, Inc. or its affiliates.  All Rights Reserved.
 *
 * name_service.h -- provide a simple name service for measurement engine
 * threads.
 */

#include <cstring>
#include <iostream>
#include <map>

using namespace std;


struct MsgPair
{
    const char* _str;
    int _id;
};

// The input pairs that we will recognize.
// TODO(epr): optimize with some sort of tree structure generated
//            by some python code.
const MsgPair aspairs[] = 
{
    // assume sorted alphabetically
    {"adjust-time", 0},
    {"capture", 1},
    {"dw", 3},
    {"hw-update", 4},
    {"jtag-contol", 5},
    {"log-level", 6},
    {"playback", 7},
    {"power-mode", 8},
    {"pp", 9},
    {"report-time", 10},
    {"rcrv-reset", 11},
    {"security-mode", 12},
    {"status-request", 13},
    {"svs", 14},
    {0, 0}
};

void identify_msg(const MsgPair* mp, const char* cp)
{
    cout << "pair:: "; 
    char fc = *cp++;
    for (; mp->_str; mp++) {
        char match = *(mp->_str);
        if (fc == match) { // possible match
            if (*cp == mp->_str[1]) { // we have a match
                cout << mp->_str << ':' << mp->_id << endl;
                return;
            }
        }
    }
    std::cout << "NONE:" << fc << ':' << *cp << std::endl;
}


constexpr int mymapcmp(char const* const* lhs, char const* const* rhs)
{
    return strcmp(*lhs, *rhs);
}

typedef std::map<const char*,int, mymapcmp> AsMap;
//typedef std::map<const char*,int> AsMap;
AsMap asmap = {
    // assume unsorted alphabetically
    {"capture", 1},
    {"dw", 3},
    {"hw-update", 4},
    {"jtag-contol", 5},
    {"security-mode", 12},
    {"adjust-time", 0},
    {"playback", 7},
    {"pp", 9},
    {"report-time", 10},
    {"log-level", 6},
    {"svs", 14},
    {"rcrv-reset", 11},
    {"power-mode", 8},
    {"status-request", 13},
};

void identify_msg(AsMap& map, const char* cp)
{
    //char* cp = (char *) data;
    auto got = map.find(cp);
    cout << "map:: ";
    if (got == map.end())
        std::cout << cp << " not found" << endl;
    else
        std::cout << got->first << " is " << got->second << endl;
}


int main(int argc, char ** argv)
{
    int i = 0;
    for (i = 1; i < argc; i++) {
        identify_msg((const MsgPair*) &aspairs, argv[i]);
        identify_msg(asmap, argv[i]);
    }
    return 0;
}

