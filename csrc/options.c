#include <stdio.h>
#include <unistd.h>

struct reply
{
    int a;
    int b;
    int c[];
};
typedef struct reply Reply;

Reply _r = {1, 2, {0, 1, 2}};

int main(int argc, char *argv[])
{
    int opt;

    // put ':' in the starting of the
    // string so that program can
    //distinguish between '?' and ':'
    while((opt = getopt(argc, argv, ":if:lrx")) != -1)
    {
        printf("OPT:%c ", opt);
        switch(opt)
        {
            case 'i':
            case 'l':
            case 'r':
            case 'x':
                printf("opt: %c\n", opt);
                break;
            case 'f':
                printf("fname: %s\n", optarg);
                break;
            case ':':
                printf("opt needs a value\n");
                break;
            case '?':
                printf("unk opt: %c\n", optopt);
                break;
        }
    }

    // optind is for the extra arguments
    // which are not parsed
    for(; optind < argc; optind++){
        printf("extra args: %s\n", argv[optind]);
    }
    printf("%d-%d\n", _r.a, _r.b);
    printf("%d-%d\n", _r.c[0], _r.c[2]);

    return 0;
}

