/* 
// 
// $Id$ 
// 
// Copyright (c) 1999-2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: socket.h
// 
// Creation Date: 11/11/99
// 
// Last Modified: 
    11/19/99 emil Converted to names space, names changed, etc.
// 
// Author: Prakash Manghnani
// 
// Description: 
//	Simple wrapper around a socket handle.
//  Has convenience methods for opening, and closing, from a socket.
// 
//  
*/ 


#ifndef __SOCKET_H__
#define __SOCKET_H__


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

# include <winsock2.h>

# include "exception.h"

__BEGIN_DC_NAME_SPACE

class XSocket : public XException
{
public:
    XSocket (const char * pReason) 
        : XException (pReason) {};
    virtual ~XSocket () {};
};

typedef SOCKET DSocket;


class CSocket
{
public:
	enum socketType
    {
		SOCK_TCP,
		SOCK_UDP
	};
private:
	DSocket m_sockHandle;

    static bool m_bWSAInitialized;
    static void initializeWSASockets (void) throw (XSocket);
    static void uninitializeWSASockets (void) throw (XSocket);

protected:
    DSocket setSocket (int sockHandle) {return m_sockHandle = (DSocket) sockHandle;}

	bool resolveIpAddr(struct sockaddr_in &port_info, const char *hostName);


public:
	CSocket();
	CSocket(DSocket s);
	virtual ~CSocket();

	bool openSocket (enum socketType sType); // open socket (unconnected)
	int close();	// Close socket object

	bool setNonBlocking();		// Routine to set socket to non-blocking mode.

	DSocket getSocketHandle(void) {return m_sockHandle;}

};

__END_DC_NAME_SPACE

#endif

