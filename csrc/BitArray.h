/*
// 
// $Id: //Source/Common/libutility/BitArray.h#6 $ 
// 
// Copyright (c) 2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: BitArray.h
// 
// Creation Date: 11/13/01
// 
// Last Modified: 
// 
// Author: Emil Rojas
// 
// Description: 
    The bit arrays class provides the foundation for a family of classes that deal
    reasonably efficiently with arbritrary bit fields.  Bit fields can be fix size,
    e.g. TwoBitArray, or variable BitBuffer.
//  
*/


# ifndef __BITARRAY_H__
# define __BITARRAY_H__

# ifdef _DEBUG
#   include <fstream.h> // needed only for debug function
#   include <stdlib.h> // need for strtol.
# endif

# include "lapeltype.h"



/*
    CBitArray -- the primary bit-array-class.
    Class responsibilities:
        It provides operations to construct the bit array of the desired size,
        to gain access to the bit-size and raw array data,
        to insert data at an arbritary position in the bit array.

        It allows for several policies on the bit buffer. 1) it can allocate
        and destroy the buffer.  2) It can be passed the buffer and leave it
        when the bit-array is destroyed, finally 3) it can be passed the buffer
        and delete when the bit-array is desstroyed.
       

    Subclass responsibilities:
        To decide on the allocation and deletion policy.
        Expected sub-classes are bit-buffer, OneBitArray, TwoBitArray, ThreeBitArray, 
        etc.
        
    Client responsibilities:

*/

class CBitArray 
{
protected:
    uint32 * m_pBA;
    int m_bytes;
    bool m_delete;

public:
    explicit CBitArray (int byteSize)
        :
          m_pBA (new uint32 [(byteSize + sizeof (uint32) - 1) / sizeof (uint32)]),
          m_bytes ((byteSize + (sizeof (uint32) - 1)) & ~(sizeof (uint32) - 1)),
          m_delete = true
    {
    }

    explicit CBitArray (uint32 * pBitArray, uint size = 0, bool deleteArray = false)
        :
          m_pBA (pBitArray),
          m_bytes (size),
          m_delete (deleteArray)
    {
    }

    virtual ~CBitArray ()
    {
        if (m_delete)
            delete [] m_pBA;
    }

    /*
        array -- returns the buffer.
    */
    uint8 * array (void) {return (uint8 *) m_pBA;}

    /*
        byteSize -- returns the byte size of the buffer.
        If the buffer was passed in this, will return zero.
    */
    int byteSize (void) {return m_bytes;}

    /*
        insertBits -- insert the value (v) or width (bits) into the buffer
        at position (pos).  width <= 32.
        The values is place in the buffer where asked, no attempt is made
        to validate the position.
    */
    uint32 insertBits (uint32 v, uint32 pos, uint32 bits);
    

    /*
        extractBits -- returns the bits at position in the bit-array right justified
        to bits.  Bits <= 32.
    */
    uint32 extractBits (uint32 position, uint32 bits);

    boolean test (uint32 position)
    {
        return (boolean) extractBits (position, 1);
    }
};


# ifdef _DEBUG
/* 
    dumpArray -- a untility function for debugging a bit buffer.
*/
void dumpArray (ofstream & fout, CBitArray * pBB);

# endif


# if defined _WIN32 && !defined ntohl

inline uint32 lntohl (uint32 l)
{
    return 
        (l >> 24) // byte 3 down to byte 0
        | ((l >> 8) & 0xFF00) // byte 2 down to byte 1 (mask after shift for smaller const)
        | ((l  & 0xFF00) << 8) // byte 1 up to byte 2 (mask before shift ")
        | (l << 24); // byte 0 up to byte 3
}

# else

inline uint32 lntohl (uint32 l)
{
    return ntohl (l);
}

# endif

template <class TNBits, t_width>
class NBits : public CBitArray
{
public:
    TNBits (int size)
        : CBitArray (size)
    {
    }

    uint32 operator[] (uint32 index)
    {
        return extractBits (index, t_width);
    }

};




# endif

