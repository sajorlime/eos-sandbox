
/*
!Still in process of being defined
A message should be a base class with the following properties.
1. An instantiation of a message has a particular type, but its storage
    comes from an underlying pool that is the size of the message type.
    TBD (or maybe we can get away with unique pointers).
2. when it is released/deleted its memory is returned the pool.
3. A message is delivered via queue shared by two or more threads.
4. A messages knows if it sync or async. (TBD)
5. A message is a unique_ptr or maybe a weak_ptr while being delivered. i.e.,
   a messaged object is owned in one of two places, a message_queue, or a thread
   manipulating message contents. (weak_ptr implies shared_ptr, both have
   issues if we manage our own pool.)
6. A message's memory goes back to the pool when it is destroyed.
7. The message contents should be savable back the <template type> 
Still in process of being defined!
 */

# include <condition_variable>
# include <chrono>
# include <map>
# include <mutex>
# include <queue>
# include <thread>
# include <string>
# include <string_view>
# include <iostream>

# include "message_id.h"
# include "message.h"
# include "mqueue.h"
# include "mthread.h"

MeThread* _xthread;

constexpr std::string_view n1("abc");
constexpr std::string_view n2{"efg"};
constexpr std::string_view n3{"hij"};
constexpr std::string_view n4{"klm"};
constexpr std::string_view n5{"nop"};
constexpr std::string_view n6{"qrs"};
constexpr std::string_view n7{""};

struct Service
{
    const std::string_view& name;
    enum {
        thread,
        interrupt,
        end_types
    } que_type;
};

constexpr Service namedServices[] =
{
    {n1, Service::thread},
    {n2, Service::thread},
    {n3, Service::interrupt},
    {n4, Service::thread},
    {n5, Service::interrupt},
    {n7, Service::thread},
    {n7, Service::end_types}
};

constexpr int thcnt()
{
    int cnt = 0;
    for (const Service* sp = namedServices; sp->que_type != Service::end_types; sp++) {
        if (sp->que_type == Service::thread)
            cnt++;
    }
    return cnt;
}

constexpr int intcnt()
{
    int cnt = 0;
    for (const Service* sp = namedServices; sp->que_type != Service::end_types; sp++) {
        if (sp->que_type == Service::interrupt)
            cnt++;
    }
    return cnt;
}
 

# if 0
// create and thread class, call it is init and create the thread
// in the OS.
// TODO(epr); calling init with the thread pointer is not very useful but
// the method can access globals in it's context. Is the other context
// data that might be useful to a thread?
template<class Th> void createThread()
{
    auto start_thread = [](MeThread& th) {
            th.init(&th);
            createTask(th.name());
        };
    Th th = Th();
    me_printf("CT %p\n", &th);
    start_thread(th);
}
# endif


int main(int argc, char** argv)
{
    if (MSG::alloc_msg_blocks() != 0) {
        std::cout << "Msg alloc error: " <<std::endl;
        return -1;
    }

    //std::cout << "THCNT: " << thcnt() << std::endl;
    //std::cout << "INTCNT: " << intcnt() << std::endl;
    RxMan rxm;
    std::thread trxm(&RxMan::run, &rxm); // run rxm
    TrackEng track;
    std::thread ttrack(&TrackEng::run, &track); // run track
    AcqEng acq;
    std::thread tacq(&AcqEng::run, &acq); // run acq
    Host host;
    std::thread thost(&Host::run, &host); // run host
    TimerTask timertask;
    timertask.init(&timertask);
    std::thread ttimer(&TimerTask::run, &timertask); // run host

    trxm.join();
    ttrack.join();
    tacq.join();
    thost.join();
    ttimer.join();
    std::cout << "ACKS:" << std::endl;
    for (auto& msg : MeThread::_acks) {
        std::cout << "  :" << msg << ':'
                  << msg->mid()
                  << ':' << msg->reply_to()->name()
                  << ':' << msg->is_sync()
                  << std::endl;
    }
    MSG::dump_msgs();
}


