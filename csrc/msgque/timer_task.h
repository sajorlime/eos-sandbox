/******************************************************************************
*
* Copyright (C) 2020 oneNav, Inc. All rights reserved.
*
*	<<<INSERT oneNav header here -- format TBD >>>
*
******************************************************************************/
/*
    timer task definition
    The Thread class is used to create threads give them a framework
    and consistent handling.
    Inheriting classes implement process_misc(Msg) to receive and process
    messages.
 */
# ifndef _TIMER_TASK_H_
# define _TIMER_TASK_H_

# include <string>

//# include "oswrapper.h"

# include "message.h"
# include "mthread.h"
// TODO(epr): get interrupt timing info

using MQue = MSG::MsgQueue;


constexpr int msecs_per_tick = 10;
constexpr int msecs_per_epoch = 20;
// this value should only exist until we have the epoch interrupt
constexpr int ticks_per_epoch = 20 / msecs_per_tick;

constexpr int ticks_per_second = 1000 / msecs_per_tick;
constexpr int epochs_per_second = 1000 / msecs_per_epoch;

// Time thread implements efficient delivery of time notifications
// any system level messages not handled by AE, TE, or RXM.

/* register for ticks
 * @param tick_rcvq the queue that wants ticks
 * @param ms_rate the rate in milliseconds that ticks should be sent
 * @return true if registration succeeded
 *  If ms_rate is less than the system tick rate it will return false.
 */
bool reg_for_ms_ticks(MSG::MsgQueue* tick_rcvq, int ms_rate);

bool reg_for_epoch_ticks(MSG::MsgQueue* tick_rcvq, int epoch_rate);


inline
bool reg_for_secs(MSG::MsgQueue* tick_rcvq,
                  int sec_rate)
{
    return reg_for_epoch_ticks(tick_rcvq,
                               1000 * sec_rate / msecs_per_epoch);
}

bool reg_for_oneshot(MSG::MsgQueue* tick_rcvq, int ms_ticks);

int getTicks(MSG::MsgPtr& time);

std::string strTicks(int ms);
static_assert(epochs_per_second == 50);


# endif // _TIMER_TASK_H_

