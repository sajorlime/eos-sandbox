
/******************************************************************************
*
* Copyright (C) 2020 oneNav, Inc. All rights reserved.
*
* <<<INSERT oneNav header here -- format TBD >>>
*
******************************************************************************/

/*****************************************************************************
* @file timer_tack.cc
*
*  Implements the tracking engine operations.
*
******************************************************************************/

# include <cassert>
# include <chrono>
# include <deque>
# include <iostream>
# include <memory>
# include <mutex>

#include "othread.h"
#include "timer_task.h"

namespace { // anonymous

using MQue = MSG::MsgQueue;

MQue* _timerQ = nullptr;

// A 32 bit value for ticks gives:
//      4294967296 milliseconds before roll over.
//      1193 days
//      3.2663 years

int _ms_ticks = 0;

// NOTE(epr): the timer task runs at the  highest priority of any thread.

int _epoch_ticks = 0;

volatile bool _run_et = true;

void epoch_thread()
{
	while (_run_et)
	{
        if ((_epoch_ticks & (((1 << 10) - 1))) == 0) 
        {
            othread{} << "ETick:" << _epoch_ticks << std::endl << std::flush;
        }
        
        ++_epoch_ticks;
        //_ms_ticks = msecs_per_epoch * _epoch_ticks;
        //MSG::Epoch* epoch = new MSG::Epoch(_ms_ticks);
        //_timerQ->send(epoch);
		std::this_thread::sleep_for(std::chrono::milliseconds(msecs_per_epoch));
	}
}


# if 0
bool lookup_queue(const std::string& name, NS::NameInfo& ni)
{
    if (NS::lookup(ni, name)) {
        melog("look up for %s gives: %p\n", name.c_str(), ni.queue);
        return true;
    }
    melog("Look up failed for %s\n", name.c_str());
    return false;
}
# endif


// Ticks class schedules a fixed number of ticks based on given tick queue.
class Ticks
{
    //OS::meMutex _mutex;
    std::mutex _mutex;
    struct Node
    {
        int dticks;
        MQue* que;
        int repeat;
        Node()
          : dticks(-1), que(nullptr), repeat(0)
        {
        }
    };
    static constexpr int NODES = 10;
    Node* _nodes;

    std::deque<Node*> freeNodes;
    std::deque<Node*> schedule;

    // place this node in th schedule
    // @param np a pointer to the from getNode().
    // @return true if not errors.
    bool queNode(Node* np)
    {
        //assert(schedule.size() + freeNodes.size() == (unsigned) NODES - 1);
        auto dt = np->dticks;
        if (!schedule.empty()) {
            // walk list, until dticks <= 0;
            for (auto si = schedule.begin(); si != schedule.end(); si++) {
                dt -= (*si)->dticks;
                if (dt <= 0) { // insert here!
                    np->dticks = (*si)->dticks + dt;
                    (*si)->dticks = -dt;
                    schedule.insert(si, np);
                    return true;
                }
            }
        }
        else {
            // special case empty queue
            dt = np->dticks;
        }
        // at end!
        np->dticks = dt;
        schedule.push_back(np);
        return true;
    }


public:
    explicit Ticks()
    {
        _nodes = new Node[10];
        Node* np = _nodes;
        for (int i = 0; i < NODES; i++, np++) {
           freeNodes.push_back(np);
        }
    }

    // @return the free space remaining
    size_t room()
    {
        std::unique_lock<std::mutex> lck(_mutex);
        //OS::takeMutex lck(_mutex);
        return freeNodes.size();
    }

    // @return the number of nodes schedules
    size_t scheduled()
    {
        std::unique_lock<std::mutex> lck(_mutex);
        //OS::takeMutex lck(_mutex);
        return schedule.size();
    }

    // take
    MQue* take()
    {
        std::unique_lock<std::mutex> lck(_mutex);
        //OS::takeMutex lck(_mutex);
        if (schedule.empty()) {
            return static_cast<MQue*>(nullptr);
        }
        Node*& np = schedule.front();
        assert(np != nullptr);
        int dt = np->dticks;
        if (dt > 1) {
            np->dticks = dt - 1;
            return static_cast<MQue*>(nullptr);
        }
        MQue* rq = np->que;
        schedule.pop_front();
        if (np->repeat) {
            np->dticks = np->repeat;
            schedule.push_back(np);
        }
        else {
            freeNodes.push_back(np);
        }
        assert(schedule.size() + freeNodes.size() == (unsigned) NODES);
        // TODO(epr); extend to queue and event, or arbitrary object?
        return rq;
    }

    bool schedTick(int dticks, MQue* que, bool repeat=false)
    {
        std::unique_lock<std::mutex> lck(_mutex);
        //OS::takeMutex lck(_mutex);
        Node* np = freeNodes.front();
        freeNodes.pop_front();
        np->dticks = dticks;
        np->que = que;
        if (repeat)
            np->repeat = dticks;
        return queNode(np);
    }

    // TODO(epr): remove me.
    void dump_sched()
    {
        // should only be called to debug.
        std::unique_lock<std::mutex> lck(_mutex);
        //OS::takeMutex lck(_mutex);
        othread{} << "SFree:" << freeNodes.size() << std::endl;
        othread{} << "SSched:" << schedule.size() << std::endl;
        for (auto si = schedule.begin(); si != schedule.end(); si++) {
            assert(*si != nullptr);
            othread{} << "D:"
                      << (*si)->dticks
                      << ';' << (*si)->que << std::endl;
        }
        othread{} << "ddds\n";
        othread{} << "\n";
    }
};

Ticks  _EpochTicks;

} // namespace anonymous


TimerTask::TimerTask() 
  : MeThread("TimerTask")
{
}

void TimerTask::run()
{
    show_name();
    while(_running) {
    auto msg = get_myq()->receive();
        switch (msg->id()) {
        case MSG::MsgId::ping:
            othread{} << "AcqEngine rcvd ping:" << msg->mid() << ':' << msg.take() << std::endl;
            process_ping(msg, th_name());
            break;
        case MSG::MsgId::quit:
            othread{} << "AcqEngine rcvd Quit\n" << std::flush;
            _running = false;
            break;
        default:
            process_unknown(msg, th_name());
            break;
        }
        //delete msg;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(800));
    othread{} << "Leaving:" << th_name() << std::endl;
}


bool TimerTask::init(void* env)
{
    _timerQ = get_myq();
	std::thread th(&epoch_thread);
    return true;
}

# define LOCAL_TICK 1
# if LOCAL_TICK
MQue* _tickq = nullptr;
int _tick_rate = -1;
int _ticks = -1;
# endif

void TimerTask::process_unknown(MSG::MsgPtr& msg, const std::string& rcvr)
{
    othread{} << "TT:" << msg->mid() << std::endl;
    switch (msg->id()) {
    case MSG::MsgId::ping:
        process_ping(msg, th_name());
        break;
    case MSG::MsgId::quit:
        othread{} << "RxMan rcvd Quit\n" << std::flush;
        _running = false;
        break;
    case MSG::MsgId::ms_tick:
        break;
    case MSG::MsgId::epoch_tick:
        MQue* que;
# if  LOCAL_TICK
        _ticks--;
        if (_ticks <= 0) {
            que = _tickq;
            MSG::Time* time_m = new MSG::Time(_ms_ticks);
            que->send(time_m);
            _ticks = _tick_rate;
        }
# else
        while ((que = _EpochTicks.take()) != nullptr) {
            MSG::Time* time_m = new MSG::Time(_ms_ticks);
            que->send(time_m);
        }
# endif
        break;
    default:
        othread{} << "TT msg :" << msg->mid() << std::endl;
        for (;;) ;
        assert(0);
        break;
    }
        
    //auto time = MSG::take<MSG::Time>(msg);
    //me_printf("timer ticks: %u ... ", time->_ms_ticks);
    return;
}

TimerTask::~TimerTask()
{
    _run_et = false;
}

bool reg_for_ms_ticks(MQue* tick_rcvq, int ms_rate)
{
    return false;
}

bool reg_for_epoch_ticks(MQue* tick_rcvq, int epoch_rate)
{
# if LOCAL_TICK
    _tickq = tick_rcvq;
    _tick_rate = epoch_rate;
    _ticks = epoch_rate;
    othread{} << "LOCAL TICK" << std::endl;
    return true;
# else
    return _EpochTicks.schedTick(epoch_rate, tick_rcvq, true);
# endif
}

bool reg_for_oneshot(MQue* tick_rcvq, int ms_ticks)
{
    return false;
}

int getTicks(MSG::MsgPtr& msg)
{
    MSG::Time* time = static_cast<MSG::Time*>(msg.take());
    return time->_ms_ticks;
}

std::string strTicks(int ms)
{
    int secs = ms / 1000;
    ms = ms - (1000 * secs);
    int mins = secs / 60;
    secs -= 60 * mins;
    int hours = mins / 60;
    mins = mins - 60 * hours;

    char buf[128];
# if LOCAL_TICK
    sprintf(buf, "timer local: %02u:%02u:%02u:%03u ... ", hours, mins, secs, ms);
# else
    sprintf(buf, "timer ticker: %02u:%02u:%02u:%03u ... ", hours, mins, secs, ms);
# endif
    return std::string(buf);
}


