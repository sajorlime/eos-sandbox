
/*
    Define the message structures.
 */

# ifndef _OTHREAD_H_
# define _OTHREAD_H_

# include <iostream>
# include <sstream>

class othread: public std::ostringstream
{
public:
    othread() = default;

    ~othread()
    {
        std::lock_guard<std::mutex> guard(_mutexPrint);
        std::cout << this->str();
    }

private:
    static std::mutex _mutexPrint;
};


# endif // _OTHREAD_H_

