
/*
    Define the message structures.
 */

# ifndef _MQUEUE_H_
# define _MQUEUE_H_

# include <cassert>
# include <condition_variable>
# include <mutex>
# include <queue>
# include <string>

# include "message.h"
# include "othread.h"

namespace MSG {

// message queue handling

class MsgQueue : public std::queue<Msg*>
{
    std::mutex _mutex;
    std::condition_variable _cv;
    std::string _name;

public:
    MsgQueue(std::string name)
     : _name(name)
    {}

    void send(Msg* msg)
    {
        std::unique_lock<std::mutex> lck(_mutex);
        othread{} << " SND:" << _name << ':'
                            << msg << ':' << msg->mid() << std::endl
                            << std::flush;
        push(msg);
        _cv.notify_one();
    }

    MsgPtr receive()
    {
        std::unique_lock<std::mutex> lck(_mutex);
        _cv.wait(lck, [this]()->bool {return !empty();});
        MsgPtr msg = MsgPtr(front());
        othread{} << "RCV:" << _name << ':'
                            << msg.take() << ':' << msg->mid() << std::endl;
        assert(msg.take() != nullptr);
        if (msg.take()->mid() != 0) {
            othread{}<< "ASSERT" << std::flush;
            while (true);
        }

        //MsgPtr msg = MsgPtr(front());
        pop();
# if 0
        if (msg->is_sync()) {
            othread{} << "SYNC::" << msg->mid() << ':' << msg.take() < std::endl;
            MSG::MsgQueue* retq = msg->reply_to();
            MSG::Msg* ack = new MSG::Ack(this);
            retq->send(ack);
        }
# endif
        return msg;
    }

    const std::string name() const
    {
        return _name;
    }
};

} // MSG

# endif // _MQUEUE_H_

