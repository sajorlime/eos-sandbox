
/*
    Threading definition
 */
# ifndef _MTHREAD_H_
# define _MTHREAD_H_

# include <cassert>
# include <map>
# include <thread>
# include <string>
# include <iostream>
# include <vector>

# include "message_id.h"
# include "message.h"
# include "mqueue.h"

class MeThread; // forward ref

typedef std::map<std::string, MeThread*> ThreadMap;

class MeThread
{
    std::string _me;
    MSG::MsgQueue _myq;
    //TODO(epr): we need prioity
protected:
    static std::map<std::string, MeThread*> _thread_map;
public:
    static std::vector<MSG::Msg*> _acks;
    virtual void run() = 0;

    virtual bool init(void* env);

    MSG::MsgQueue* get_myq()
    {
        return &_myq;
    }

    MeThread(std::string th_name)
      : _me(th_name),
        _myq(th_name),
        _running(true)
    {
        _thread_map[th_name] = this;
    }

    const std::string& th_name()
    {
        return _me;
    }

    void process_ping(MSG::MsgPtr& msg, const std::string& rcvr);

    //void process_ack(MSG::Msg* msg,
    void process_ack(MSG::MsgPtr& msg, const std::string& rcvr);

    virtual void process_unknown(MSG::MsgPtr& msg, const std::string& rcvr);

    void show_name()
    {
        //std::string s = "Thread:" + th_name() + '\n';
        //std::cout << s << std::flush;
    }

    static ThreadMap& get_thread_map()
    {
        return _thread_map;
    }

    bool _running;

    virtual ~MeThread()
    {
    }
};

class Host : public MeThread
{
public:
    explicit Host();

    void send_pings();

    void run() override;
};

class RxMan : public MeThread
{
public:
    explicit RxMan() ;

    void run() override;
};

class TrackEng : public MeThread
{
public:
    TrackEng();

    void run() override;
};

class AcqEng : public MeThread
{
public:
    AcqEng() ;

    void run() override;
};

class TimerTask : public MeThread
{
public:
    TimerTask() ;

    void run() override;
    bool init(void* env) override;

    void process_unknown(MSG::MsgPtr& mp, const std::string& rcvr) override;
    virtual ~TimerTask();
};

# endif // _MTHREAD_H_

