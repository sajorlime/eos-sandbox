
/*
    Threading implementation
 */

# include <chrono>
# include <cstdlib>
# include <random>
# include <map>
# include <thread>
# include <string>
# include <iostream>
# include <vector>

# include "message_id.h"
# include "message.h"
# include "mqueue.h"
# include "mthread.h"
# include "othread.h"
# include "timer_task.h"

ThreadMap MeThread::_thread_map;
std::vector<MSG::Msg*> MeThread::_acks;

bool MeThread::init(void* env)
{
    int rnum = rand() % epochs_per_second;
    return reg_for_epoch_ticks(get_myq(), epochs_per_second + rnum);
}

void MeThread::process_ping(MSG::MsgPtr& msg, const std::string& rcvr)
{
    MSG::MsgQueue* retq = msg->reply_to();
# if 0
    std::string s = " " + rcvr
                    + " rcvd PING from: "
                    + retq->name();
    othread{} << msg.take() << s << msg->mid() << std::endl << std::flush;
# endif
    MSG::Msg* ack = new MSG::Ack(retq);
# if 0
    othread{} << ack << std::endl;
    s = " " + retq->name()
                    + " send ACK from: "
                    + rcvr
                    + '\n';
    othread{} << ack << s << std::flush;
# endif
    retq->send(ack);
}

    //void process_ack(MSG::Msg* msg,
void MeThread::process_ack(MSG::MsgPtr& msg, const std::string& rcvr)
{
# if 0
    MSG::MsgQueue* retq = msg->reply_to();
    std::string s = rcvr
                    + " rcvd ack from: "
                    + retq->name()
                    + '\n';
    othread{} << "ACK:" << msg.take() << s << std::flush;
# endif
    // HACK for testing
    _acks.push_back(msg.take());
}

void MeThread::process_unknown(MSG::MsgPtr& msg, const std::string& rcvr)
{
    if (msg->id() == MSG::MsgId::time) {
        MSG::Time* time = static_cast<MSG::Time*>(msg.take());
        othread{} << 't' << time->_ms_ticks << std::endl;
        return;
    }
    othread{} << std::flush;
    std::string s = rcvr
                    + ": UNK rcvd:"
                    + (char) msg->id()
                    + ':'
                    + msg->reply_to()->name()
                    + '\n';
    othread{} << s << std::flush;
    while(true);
}

Host::Host() 
  : MeThread("Host")
{
}

void Host::send_pings()
{
    MSG::MsgQueue* myq = get_myq();
    for (auto th : MeThread::_thread_map) {
        MSG::MsgQueue* sq = th.second->get_myq();
        if (sq == myq)
            continue;

        std::string s = "send ping to:" 
                        + sq->name() 
                        +  '\n';
        othread{} << s << std::flush;
        MSG::Msg* ping = new MSG::Ping(get_myq());
        sq->send(ping);
        std::this_thread::sleep_for(std::chrono::milliseconds(7));
        auto msg = myq->receive();

        assert(msg->id() == MSG::MsgId::ack);
        othread{} << "RCVd Ack\n" << std::flush;
        //delete msg;
        //break;
    }
}

void Host::run()
{
    show_name();
    MSG::MsgQueue* myq = get_myq();
    for (int i = 0; i < 25; i++) {
        send_pings();
    }
# if 0
    for (auto th : MeThread::_thread_map) {
        MSG::MsgQueue* sq = th.second->get_myq();
        if (sq == myq)
            continue;

        std::string s = "send ping to:" 
                        + sq->name() 
                        +  '\n';
        othread{} << s << std::flush;
        MSG::Msg* ping = new MSG::Ping(get_myq());
        sq->send(ping);
        //std::this_thread::sleep_for(std::chrono::milliseconds(700));
    }
# endif
    // give up the CPU so threads can reply
    std::this_thread::sleep_for(std::chrono::milliseconds(700));
    othread{} << "Host getting replies\n" << std::flush;
    while (!myq->empty()) {
        auto msg = myq->receive();
        othread{} << "HO:" << msg->mid() << std::endl;
        switch (msg->id()) {
        case MSG::MsgId::ack:
            process_ack(msg, th_name());
            break;
        default:
            process_unknown(msg, th_name());
            break;
        }
        //delete msg;
    }
    othread{} << "Host sending Quit\n" << std::flush;
    for (auto th : MeThread::_thread_map) {
        MSG::MsgQueue* sq = th.second->get_myq();
        if (sq == myq)
            continue;
        MSG::Msg* quit = new MSG::Quit(get_myq());
        sq->send(quit);
    }
}

RxMan::RxMan() 
  : MeThread("RxMan")
{
}

void RxMan::run()
{
    show_name();
    while (_running) {
        //MSG::Msg* msg = get_myq()->receive();
        auto msg = get_myq()->receive();
        othread{} << "RX:" << msg->mid() << std::endl;
        switch (msg->id()) {
        case MSG::MsgId::ping:
            process_ping(msg, th_name());
            break;
        case MSG::MsgId::quit:
            othread{} << "RxMan rcvd Quit\n" << std::flush;
            _running = false;
            break;
        default:
            process_unknown(msg, th_name());
            break;
        }
        //delete msg;
    }
    othread{} << "sleep:" << th_name() << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    othread{} << "Leaving:" << th_name() << std::endl;
}

TrackEng::TrackEng() 
  : MeThread("TrackEng")
{
    reg_for_epoch_ticks(get_myq(), 2 * epochs_per_second - 7);
}

void TrackEng::run()
{
    show_name();
    while(_running) {
        auto msg = get_myq()->receive();
        othread{} << "TT:" << msg->mid() << std::endl;
        switch (msg->id()) {
        case MSG::MsgId::ping:
            process_ping(msg, th_name());
            break;
        case MSG::MsgId::quit:
            othread{} << "TrackEng rcvd Quit\n" << std::flush;
            _running = false;
            break;
        default:
            process_unknown(msg, th_name());
            break;
        }
        //delete msg;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(300));
    othread{} << "Leaving:" << th_name() << std::endl;
}

AcqEng ::AcqEng() 
  : MeThread("AcqEng")
{
    reg_for_epoch_ticks(get_myq(), epochs_per_second / 2 + 5);
}

void AcqEng ::run()
{
    show_name();
    while(_running) {
        auto msg = get_myq()->receive();
        othread{} << "AE:" << msg->mid() << std::endl;
        switch (msg->id()) {
        case MSG::MsgId::ping:
            othread{} << "AcqEngine rcvd ping:" << msg->mid() << ':' << msg.take() << std::endl;
            process_ping(msg, th_name());
            break;
        case MSG::MsgId::quit:
            othread{} << "AcqEngine rcvd Quit\n" << std::flush;
            _running = false;
            break;
        default:
            process_unknown(msg, th_name());
            break;
        }
        //delete msg;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(800));
    othread{} << "Leaving:" << th_name() << std::endl;
}

std::mutex othread::_mutexPrint{};

