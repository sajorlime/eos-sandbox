
/*
    Implement the message structures.
 */

# include <cassert>
# include <cstring>
# include <iostream>
# include <map>
# include <memory>
# include <mutex>
# include <string>
# include <vector>

# include "message.h"

namespace MSG {

const size_t msg_sizes[] =
{
    sizeof(Ping),
    sizeof(Ack),
    sizeof(Quit),
    sizeof(AdjustTime),
    sizeof(SetMode),
    sizeof(TimeReport),
    sizeof(Time),
    0
};

const int msg_cnt = 10;

struct MemInfo {
    void* mp;
    size_t count;
    size_t size;
    size_t index;
    size_t allocated; // never gets reset, just counts up

    MemInfo(void* mp, size_t alloc, size_t size)
      : mp(mp), count(alloc), size(size), index(0), allocated(0)
    {}
    MemInfo() {}
};


std::map<size_t, MemInfo> msg_map;
std::mutex alloc_mutex;

void dump_msgs()
{
    std::cout << "MI:" <<  std::endl;
    for (auto s : msg_map) {
        std::cout << "Size:" << s.first << "=?"
                  << s.second.size << ':'
                  << s.second.mp << '@'
                  << s.second.count << ':'
                  << s.second.allocated << ':'
                  << std::endl;
    }
}

int msg_allocate(size_t size)
{
    std::map<size_t, size_t> sizes_map;
    for (const size_t* msp = msg_sizes; *msp != 0; msp++) {
        // add all occurrences of a given size.
        sizes_map[*msp] = sizes_map[*msp] + msg_cnt;
    }
# if 0
    for (auto s : sizes_map) {
        std::cout << "Size:" << s.first << ';' << s.second << std::endl;
    }
# endif
    for (auto& sinfo : sizes_map) {
        size_t tsize = sinfo.first * sinfo.second;
        void* mp = new uint8_t[tsize];
        memset(mp, (int) MsgId::empty, tsize);
        std::pair<size_t, MemInfo> pair = std::make_pair(
                                            sinfo.first,
                                            MemInfo(mp,
                                                     sinfo.second,
                                                     size
                                           ));
        msg_map.emplace(std::move(pair));
    }
    return 0;
}

int alloc_msg_blocks()
{
    //std::cout << "Msg sizes:" << sizeof(msg_sizes)/sizeof(size_t) << std::endl;
    //std::cout << "Msg size:" << sizeof(Msg) << std::endl;
    //std::cout << "AdjustTime size:" << sizeof(AdjustTime) << std::endl;
    for (const size_t* msp = msg_sizes; *msp != 0; msp++) {
        //std::cout << "RSIZE:" << *msp << '@' << msp << std::endl;
        if (msg_allocate(*msp) != 0)
            return 1; // error codes?
    }
    //dump_msgs();
    return 0;
}

void* Msg::operator new(size_t size)
{
    std::unique_lock<std::mutex> lck(alloc_mutex, std::defer_lock);
    auto mi = msg_map[size];
    //std::cout << "Req block req size:" << size << " cnt:" << mi.count << std::endl;
    for (size_t i = 0; i < mi.count; i++) {
        void* vp = (uint8_t*) mi.mp + size * mi.index;
        Msg* msg = static_cast<Msg*>(vp);
        if (mi.index > mi.count) // wrap index
        {
            std::cout << "WRAP:" << mi.count << '\n';
            mi.index = 0;
        }
        //std::cout << "Req block Msg size:" << size << ':' << msg->mid() <<  ':' << msg << 'I' << mi.index << std::endl;
        if (msg->_id == MsgId::empty) {
            //std::cout << "RET block Msg size:" << size << ':' << msg << std::endl;
            mi.count--;
            mi.allocated++;
            msg->_size = size;
            mi.index++;
            return msg;
        }
        mi.index++;
    }
    std::cout << "FAIL Msg size:" << size << std::endl;
    for (;;) ;
    assert(0);
    return ::new uint8_t(size);
}

void Msg::operator delete(void* ms)
{
    std::unique_lock<std::mutex> lck(alloc_mutex, std::defer_lock);
    Msg* msg = static_cast<Msg*>(ms);
    //std::cout << msg->mid() << ":MS:" << msg->_size << ':' << msg << std::endl;
    msg->_id = MsgId::empty;
    if (msg->_size == 0) {
        std::cout << "FAIL Msg size:" << msg->_size << std::endl;
        for (;;) ;
        assert(msg->_size != 0);
    }
    auto mi = msg_map[msg->_size];
    mi.allocated--;
    mi.count++;
}

} // namespace MSG

