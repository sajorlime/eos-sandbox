
/*
    Define the message structures.
 */

# ifndef _MESSAGE_H_
# define _MESSAGE_H_

# include "message_id.h"
# include <mutex>

namespace MSG {

class MsgQueue; // forward ref

/* @brief base class for messages delivered through queues.
 * The base contains three elements the message identifier and the senders
 * queue for ease an consistency of replying, and the synchronous flag.
 * Messages delivered with the sync flag are replied to with an ack message.
 */
class Msg {
private:
    MsgId _id;
    MsgQueue* _sender;
    bool _sync;
    size_t _size; // to return to block

public:
    Msg(MsgId id, MsgQueue* sender, bool sync=false)
      : _id(id),
        _sender(sender),
        _sync(sync)
    {
    }

    auto mid() {return static_cast<int>(_id);}
    auto id() {return _id;}

    auto is_sync() {return _sync;}

    MsgQueue* reply_to()
    {
        return _sender;
    }

    void* operator new(size_t size);

    void operator delete(void* ms);
};

class MsgPtr 
{ 
    Msg* _mp;
public: 
    explicit MsgPtr(Msg* p=nullptr)
             : _mp(p)
    {
    }

    ~MsgPtr()
    {
        delete _mp;
    }   

    Msg& operator *()
    {
        return *_mp;
    } 

    Msg* operator->()
    {
        return _mp;
    }

    Msg* take()
    {
        return _mp;
    }
}; 

typedef class Msg Msg_t;


// management messages
struct Ping : public Msg
{
    Ping(MsgQueue* sender)
      : Msg(MsgId::ping, sender, true)
    {
    }
};

struct Ack : public Msg
{
    Ack(MsgQueue* sender)
      : Msg(MsgId::ack, sender)
    {
        //std::cout << "CACK:" << this << std::endl;
    }
};

struct Quit : public Msg
{
    Quit(MsgQueue* sender)
      : Msg(MsgId::quit, sender)
    {
    }
};

struct Epoch : public Msg
{
    Epoch(int ticks)
      : Msg(MsgId::epoch_tick, static_cast<MSG::MsgQueue*>(nullptr)),
        _ticks(ticks)
    {}
    int _ticks;
};

struct Time : public Msg
{
    Time(int ms)
      : Msg(MsgId::time, static_cast<MSG::MsgQueue*>(nullptr)),
        _ms_ticks(ms)
    {}
    int _ms_ticks;
};


// input messages. These should get their own header file.
struct AdjustTime : public Msg
{
    AdjustTime(MsgQueue* sender, int ms)
      : Msg(MsgId::adjust_time, sender),
        _d_ms(ms)
    {}
    int _d_ms; // delta ms
};

struct SetMode : public Msg
{
    SetMode(MsgQueue* sender, int mode, int when)
      : Msg(MsgId::set_dwell, sender),
        _mode(mode), _when(when)
    {}

    int _mode;
    int _when;
};

// output message, will get their own header file.
//
struct STimeReport 
{
    int _ms;
    int _year;
};

struct TimeReport : public Msg
{
    TimeReport(MsgQueue* sender, STimeReport& st)
      : Msg(MsgId::report_time, sender),
        _st(st)
    {}
    STimeReport _st;
};

void dump_msgs();
int alloc_msg_blocks();

} // MSG


# endif // _MESSAGE_H_

