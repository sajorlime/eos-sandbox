
#include <stdio.h>
#include <cstdarg>
#include <cstdlib>
#include <cstring>
#include <iostream>

typedef unsigned long ulong;

char buffer[256] = "[31;1m";
const char * norm = "[0m";

char * myprint(const char * fmt, ...)
{
    ulong nred = strlen(buffer);

    va_list args;
    va_start(args, fmt);


    int n;

    printf("%lu F{%s}\n", nred, fmt);
    n = vsnprintf(&buffer[nred], sizeof buffer - nred, fmt, args);
    printf("%d|%lu|%s|T\n", n, strlen(buffer), buffer);
    strcpy(&buffer[n + nred], norm);
    printf("\n|%lu|%s|T\n", strlen(buffer), buffer);

    return buffer;
}

int main(int argc, char ** argv)
{
    printf("%d\n", argc);
    if (argc < 3)
    {
        return -1;
    }
    printf("MYP: %s\n", myprint("%s %s %s",  argv[0], argv[1], argv[2]));
    //printf("%s\n", norm);
    std::cout << "someting else" << std::endl;

    int j = 3;
    std::cout << "i:" << j << std::endl;
    for (int i = 0; i < 3;  i++, j++) {
        if (i == 0)
            break;
    }
    std::cout << "J:" << j << std::endl;
    return 0;
}

