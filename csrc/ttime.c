//#define _POSIX_C_SOURCE 200809L

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

uint64_t time_in_ms (void)
{
    struct timespec spec;

    clock_gettime(CLOCK_REALTIME, &spec);

    uint64_t ms = 1000 * ((uint64_t) spec.tv_sec);
    ms += spec.tv_nsec / 1000 * 1000;
    return ms;
}

typedef int (* FType)(int a, char b);

int myf(int x, char y)
{
    printf("%d, %c\n", x, y);
    return 0;
}

FType nullf = (FType) 0;

int main(int argc, char** argv)
{
    uint64_t dtlimit = 10 * 1000;
    if (argc > 1) {
        int mul = atoi(argv[1]);
        printf("mul:%s, %d\n", argv[1], mul);
        dtlimit = mul * 1000LL;
    }
    uint64_t ms = time_in_ms();
    uint64_t ms_end = ms + dtlimit;
    printf("%lu %s %lu\n", ms, (ms < ms_end) ? " < " : " !< ", ms_end);

    while (ms < ms_end) {
        printf("%lu d:%lu\n", ms, ms_end - ms);
        ms = time_in_ms();
    }
    nullf = myf;
    int x = nullf(1, 'a');
    printf("%d\n", x);
}

