/*
 * &copyright Copyright (c)1997 BDN Software Inc. (Bruce D. Nilo). 
 * All rights reserved.
 *
 * $Source: /export2/Repositories/odbc2drivers/odbcquery/shiftopt.h,v $
 *
 *
 */



#ifdef __cplusplus
extern "C" {
#endif

	extern int shiftopt( int *argcp, char **argv, char *opts );
	extern int Opterr;		/* 1 => print error message, 0 disables */
	extern char Optarg[];		/* the argument to the current option   */
#ifdef __cplusplus
}
#endif

