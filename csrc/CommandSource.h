/*
//
// $Id: //Source/Common/libutility/CommandSource.h#9 $
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Author: Gordie
//
// Brief Description: 
//
*/

#ifndef __COMMANDSOURCE_H
#define __COMMANDSOURCE_H

# include "dotcastns.h"
# include "dctype.h"

__BEGIN_DC_NAME_SPACE

//
// Forward decls
//
class POutputAgent;
class CommandHandlerInfo;

//
// Main class for this component. Clients create a CommandSource using
// the static CreateCommandSource method, and register handlers for
// specific commands. A CommandSource does not ever do any direct
// output, for any method that could result in output to the user
// (the report methods, and match information when processing a
// command from history or with a partial name match), an OutputAgent
// object is passed in to the method, and the CommandSource calls
// the OutputAgent with a string to output user the application's
// UI and/or logging system. All CommandSource implementations should
// check for a null OutputSource (and avoid blowing up trying to send
// methods to a nonexistent Object at address 0).
//
// When input representing a command is received by the application,
// the input string is sent to the CommandSource via the method
// processCommand. The CommandSource will parse the input into
// a command name and an array of string (char*) arguments, similar
// to argv processing in a C/C++ main routine. After parsing the input,
// the CommandSource will check its set of command handlers for the
// command name, and invoke the method indicated in the CommandHandler
// matching the command.

// If no exact match is found, the CommandSource will attempt to make
// a partial match for the closest name, enabling the user to abbreviate
// commands (i.e. 'in' for 'insert'). In the case of ambiguity on partial
// matches it is not specified which command will be invoked. If a partial
// match is found, before calling the handler the CommandSource will call
// the OutputAgent with the full text of the command (including the arguments)
// after it has substituted the full name of the command for the partial name.
//
// A CommandSource Object maintains a history list of all previous commands.
// If the input string passed to processCommand starts with an integer number,
// the CommandSource will reprocess the command at that index in the history
// list. Before processing the command, the CommandSource will call the
// OutputAgent with the full text of the command (including arguments) to
// be reprocessed.
//
class PCommandSource
{
public:
	// Static constructor used to create a CommandSource Object.
	static PCommandSource* CreateCommandSource();

	// Register a handler for a command by name. When a command with the specified commandName
	// is processed by the CommandSource, it is handed off to the handler specified in the
	// handlerInfo.
	virtual void registerCommand (const char* commandName, CommandHandlerInfo* handlerInfo = 0) = 0;

	// Parse and process input command, finding command name match for first token
	// in command string. OutputAgent (if non 0) is used to indicate match on history
	// index or partial command name, and is also passed into the command handler
	//  so it can use a consistent output stream with the caller of this method.
	virtual bool processCommand (const char* command, POutputAgent* agent, bool addToHistory = true, void* data = 0) = 0;

	// Returns a char* representing all commands that have been
	// processed by this CommandSource Object. Each command in 
	// the history string is separated by a newline ("\n"). For
	// commands which were expanded (see description of processCommand),
	// the history entry contains the expanded name of the command.
	// It is the caller's responsibility to free the returned data.
	virtual char* getHistory () const = 0;

	// Reports history for each command that has been processed
	// by this CommandSource by repeatedly calling the OutputAgent
	// with each command, starting with the first command processed,
	// and ending with the most recent command. The strings passed
	// to the OutputAgent are terminated with '\0', but do not end
	// with a newline. The format of each string is "%4d: %s", where
	// %4d refers to the number of the command in the history (starting
	// with 0, and ending with n-1), and %s is the actual command.
	// For commands which were expanded (see description of processCommand),
	// the history entry contains the expanded name of the command.
	//
	// If OutputAgent is null, this command returns without doing anything.
	virtual void reportHistory (POutputAgent* agent) const = 0;

	// Reports help for the indicated command (or all commands if the
	// command parameter passed in is 0). The OutputAgent is called for
	// to output the help text, which is either a descriptive string
	// provided when the command is registered, or simply the name of
	// the command (if there is no descriptive help).
	//
	// If OutputAgent is null, this command returns without doing anything.
	virtual void reportHelp (const char* command, POutputAgent* agent) const = 0;

	// Adds a command to the history. The char* for the command being added
	// to the history becomes owned by the CommandSource, the caller should not
	// free it, or do anything else that could result in it's destruction (such
	// as pass in a char array from the stack, and then return).
	virtual void addCommandToHistory (const char* command) = 0;

	// Constructor and Destructor decls which can be safely ignored.
	PCommandSource () {}
	virtual ~PCommandSource () {}

private:
	PCommandSource (PCommandSource& rhs);
	PCommandSource& operator=(PCommandSource& rhs);
};

//
// Protocol clients of CommandSource should implement to handle output on behalf of
// a CommandSource Object. As an example, when calling the CommandSource method
// reportHistory(), an OutputAgent is passed in to actually "display" the history
// output, since the CommandSource does not know what to use to do so (console,
// a GUI widget, logging, etc.) 
//
class POutputAgent
{
public:
	// Display the output as appropriate, using "printf" style output.
	// A typical implementation of this would use vsprintf to construct
	// a string and display it to the user via whatever UI the application
	// is presenting.
	virtual void outputFormatString (const char* format, ...) = 0;
	// Display the output string as appropriate, paying attention to newline setting
	virtual void outputString (const char* buffer, bool newline) = 0;
};

// Type definitions for command process and help functions. These are the prototypes
// for static functions that are called by the CommandSource Object. Note that the first
// parameter (handler) is semantically the "this" Object as if these static functions
// were actually member methods.
typedef bool (*CommandProcessFunction)(void* handler, const char* command, uint32 argc, char** argv, POutputAgent* agent, void* data);
typedef char* (*CommandHelpFunction)(void* handler, const char* command, POutputAgent* agent);

//
// Structure representing a particular Command. Contains
// a reference to the Object to handle processing of the command,
// a short description to be used in help (as a char*), 
// callback functions to handle invocation of the Command,
// a descriptive help string, and flag settings for the Command.
//
// Functions registered in the CommandHandlerInfo are static
// class methods, not instance methods. When these methods
// are called, the first parameter is the handler Object, which
// can be treated as a "this" parameter (a common use is to have
// the static function turn around and call a specific member
// function after typecasting the handler to the specific class).
// Perhaps someday someone will come through here and change this
// to use a pointer to a member function however certain buggy
// compiler's prevented me from doing so in the first place.
//
// A CommandHandlerInfo structure is passed in for each Command
// that is registered with a CommandSource, and the CommandSource
// Object makes a copy of the CommandHandlerInfo and the helpDescription.
// Therefore the CommandHandlerInfo can be freed after registration of
// the command, or can be mutated, or created on the stack. The Sky's
// the limit!
//
// Note that the handler Object must not be freed after registration
// of a Command, as the CommandHandlerInfo kept by the CommandHandler
// maintains a direct reference to it.
//
class CommandHandlerInfo
{
public:
	// Constructor. The handler parameter is referenced directly in the
	// created CommandHandlerInfo and must not be freed by the caller. All
	// other parameters are copied and can be freed after the call to the
	// constructor.
	CommandHandlerInfo ();
	CommandHandlerInfo (void* handler, const char* helpDescription, 
		CommandHelpFunction helpFunction, CommandProcessFunction processFunction,
		bool putInHistory);
	virtual ~CommandHandlerInfo ();

	CommandHandlerInfo (const CommandHandlerInfo& rhs);
	CommandHandlerInfo& operator=(const CommandHandlerInfo& rhs);

	// 
	// Setters
	//
	void setHelpDescription (const char* helpDescription) { copyCommandHandlerHelpDescription(helpDescription); }
	void setProcessFunction (CommandProcessFunction aProcessFunction) { processFunction = aProcessFunction; }
	void setPutInHistory (bool aPutInHistory) { putInHistory = aPutInHistory; }

	//
	// Getters
	//
	void* getHandler () { return handler; }
	const char* getHelpDescription () { return helpDescription; }
	CommandHelpFunction getHelpFunction () { return helpFunction; }
	CommandProcessFunction getProcessFunction () { return processFunction; }
	bool getPutInHistory () { return putInHistory; }

private:
	void* handler; // Object that handles command
	char* helpDescription; // Short description for help on all commands
	CommandHelpFunction helpFunction; // Static functions...
	CommandProcessFunction processFunction; // ...because C++ sucks
	bool putInHistory;

	void copyCommandHandlerHelpDescription (const char* helpDescription); 
	void copyCommandHandlerInfo (void* handler, const char* helpDescription, 
		CommandHelpFunction helpFunction, CommandProcessFunction processFunction,
		bool putInHistory);

	static char* DefaultHelpDescription;
};

__END_DC_NAME_SPACE


#endif // __COMMANDSOURCE_H

