/*
 * Fast CRC calculation functions
 *
 * This implementation is based upon that presented in the article
 *	"Computation of Cyclic Redundancy Checks Via Table Look-up",
 *	by Dilip V. Sarwate, CACM Vol.31, No.8, August 1988, pp. 1008-1013
 *
 */

#ifndef __CRC32_H__
#define __CRC32_H__

# include <cstddef>
# include <stdint.h>

#pragma once

namespace REC
{
constexpr uint32_t crc_sum = 0xFFFFFFFF;
uint32_t crc32(uint8_t* buf, size_t n);
uint32_t partialCrc32(uint8_t* buf, size_t n, uint32_t partial);

} // namespace REC


#endif // __CRC32_H__
