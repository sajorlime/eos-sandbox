
/*
    fixedpoint.h -- header for to define some fixed point arithmetic.

    Types:
        Fixed -- the type of a fixed point number.  It is a 64
        bit integer.  It is coded so that the size can easily be 
        changed.

    Implemnetation notes:

        This implementation assumes that LONGEST_INT_BITS (64) bit
        arithmatic is resonably efficient on the target platform.  The
        fixed point implementation assumes that the language supports
        arithmatic of 64 bits or better, but one could have a very
        limited fixed point implementation with 32 bits.

        I believe the implentation is fairly efficient, but it could be
        improved with some inline assembly.

    Basis operations like adding, substracting, comparing are all done as native
    operatiions.  E.g.
        Fixed a;
        Fixed b;
        Fixed c;
        Fixed d;

        if (a > b)
            c = a - b;
        else
            c = b - a;
        d = a + b;

    works.

    To multiply and dived, take absolute value, convert to a string require
    functionals interfaces.  Creating a fixed point number is done with a
    macro, as is retieving the parts of a fixed point number.

        Fixed fp = newFixed (5, 0) -- sets fp to 5.0
        Fixed x = newFixed (10, 0) -- sets x to 10.0
        Fixed y = newFixed (0, FP_HALF) -- y to 0.5
        Fixed z = newFixed (0, FP_QUARTER) -- z to 0.25
        
        a = mulFixed (x, y) -- would set a 5.
        b = mulFixed (fp, y) -- would set b  5/0.5 or one.
        c = divFixed (newFixed (0, FP_SIXTEENTH), z) -- would
                set c to 1/16/1/4 or 1/64 or one.

*/


# ifndef __FIXEDPOINT_H__
# define __FIXEDPOINT_H__


# ifdef __cplusplus
extern "C" {
# endif

/*
    Fixed is the structure that holds audio time.
    Fixed secs and frac from a 16 bit resolution, if this should turn out to not
    be enough, then increase it to 32 bits using to 64 bit integers
*/

typedef uint64_t Fixed;
typedef int32_t lint;
typedef uint32_t ulint;
typedef int64_t llint;
typedef uint64_t ullint;
typedef unsigned char uint8;
typedef unsigned short int uint16;

# ifndef LONGEST_INT_BITS
#   define LONGEST_INT_BITS 64
# endif

/*
    FP_INT_SHT -- the integer resolution.
    We set this and make the fractional part the rest of the integer.
*/
# define FP_INT_RES 20
# define FP_INT_SHT FP_INT_RES 
/*
    FP_RES -- the resolution of the fractional part of
    the fixed point code.
    It must be at least 32 bits.
*/
# ifndef FP_RES 
#   define FP_RES (LONGEST_INT_BITS - FP_INT_SHT)
# endif

/* I defined 1 this way so that it is portable */
# define LL1 ((Fixed) 1)

# define FP_FRAC_MASK ((LL1 << (FP_RES)) - 1)
# define FP_SIGN (LL1 << (FP_RES + FP_INT_SHT - 1))
# define FP_ONE (LL1 << (FP_RES))
# define FP_HALF (LL1 << (FP_RES - 1))
# define FP_QUARTER (FP_HALF >> 1)
# define FP_EIGTH (FP_QUARTER >> 1)
# define FP_SIXTEENTH (FP_EIGTH >> 1)

/*
    intFixed -- return the integer part of a fixed.

    prototype:
        int intFixed(Fixed);
*/
# define intFixed(Pfixed) ((int) ((Pfixed) >> FP_RES))

/*
    fracFixed -- returns the fractional part of a fixed.

    prototype:
        ullint fracFixed(Fixed);
*/
# define fracFixed(Pfixed) \
            ((ullint) ((Pfixed) & FP_FRAC_MASK))

/*
    newFixed -- returns the fractional part of a fixed.

    prototype:
        Fixed newFixed(int i, ullint frac);
*/
# define newFixed(Pi, Pf) \
            ((((Fixed) Pi) << FP_RES) + ((Fixed) Pf << FP_INT_SHT))
            

/*
    signedCmpFixed -- performs a signed compare.  The fixed values
    can be cast to signed values to accomplish the same thing.

    prototype:
        int signedCmpFixed(Fixed, Fixed);
*/
# define signedCmpFixed(Pf0, Pf1) \
            (((int64_t) Pf0) < ((int64_t) Pf1))


/* macro defined to avoid warning. */
# define NEG_FIXED(Pf) (-((llint) Pf))

/*
    absFixed -- returns the absolute value of the fixed value.

    prototype:
        Fixed absFixed(Fixed);
*/
# define absFixed(Pfixed) (((Pfixed) & FP_SIGN) ? NEG_FIXED (Pfixed) : (Pfixed))

/*
    mulFixed -- returns m0 * m1 as a fixed point.
*/
Fixed mulFixed(Fixed m0, Fixed m1);

/*
    divFixed -- returns m0 / m1 as a fixed point.
*/
Fixed divFixed(Fixed num, Fixed dem);


/*
    roundFixed -- round the fixed to the nearest integer.
*/
int roundFixed(Fixed fixed);

/*
    fixedPointToString -- represent a fixed point number as a
    string suitable for printing.  The number is printed as
    either a base 10 or hexidecimal string with decimal point.
    String is termianted with 'D' or 'H', respectively

    pBuf must point to a 32 character space.

    Returns pBuf.
    
*/
char* fixedToString(Fixed fixed, char* pBuf, int base);

/*
    firstOne -- returns the first one in a 64 bit integer.

    The high order bit gets 64, and zero gets zero.
*/
int firstOne(ullint ull);

/*
    lastOne -- returns the last one in a 64 bit integer.

    The low order bit gets 1, and zero gets 0.
*/
int lastOne(ullint ull);


# if defined(DEBUG) || (FIXED_TO_DOUBLE)

double toDouble(Fixed fixed);

# endif

# ifdef DEBUG
void printFixed(char* pF, ...);
# endif

# ifdef __cplusplus
}
# endif

# endif

