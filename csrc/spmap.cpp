// downcast map members
#include <iostream>
#include <string>
#include <unordered_map>
#include <map>
#include <memory>
#include <tuple>


struct X
{
    int _x;
    X(int x) : _x(x) {}
};

X x10(10);
X x20(20);
X x30(30);
X x40(40);

struct X2 : public X
{
    X _x2;
    X2(X const& l1, X const& l2) : X(l1), _x2(l2) {}
};

X2 tl2(x10, x20);

struct X3 : public X2
{
    X _x3;
    X3(X2 const& tlx, X const& x3) : X2(tlx), _x3(x3) {}
    X3(X3 const& thx) : X2(thx), _x3(thx._x3) {}
};

X3 x321(X2(110, 120),X(130));

X3 x33(x321);

X3 x312(X2(130, 120),X(110));

std::shared_ptr<X> mkx1_from_x3(X3 const& x3)
{
   std::shared_ptr<X3> xp3 = std::make_shared<X3>(x3);
   return std::static_pointer_cast<X>(xp3);
}

std::map<std::string, std::shared_ptr<X>> _slmap
=
{
    {"a", std::make_shared<X>(X(140))},
    {"b", mkx1_from_x3(X3(X2(X(130), X(120)), 110))},
    {"c", std::make_shared<X>(X3(x312))},
};

std::map<std::string, X*> _lmap
=
{
    {"a", new X(100)},
    {"b", new X3(X2(X(130), X(120)), 110)},
    {"c", new X3(x312)},
};


void print_x(std::string id, X& x)
{
        std::cout << id << ':' << x._x << std::endl;
}
void print_x3(std::string id, X3& x3)
{
    std::cout << id << ":{" << x3._x << ", " << x3._x2._x <<  ", " << x3._x3._x << '}' << std::endl;
}

struct XYZ {
    int x;
    int y;
    int z;
    XYZ(int a, int b, int c)
        : x(a), y(b), z(c)
    {
    }
    XYZ() {}
};


std::map<uint32_t, XYZ> _xyz_map;

class XyzPtr 
{ 
    XYZ* _mp;
public: 
    explicit XyzPtr(XYZ* p=nullptr)
             : _mp(p)
    {
    }

    ~XyzPtr()
    {
        delete _mp;
    }   

    XYZ& operator *()
    {
        return *_mp;
    } 

    XYZ* operator->()
    {
        return _mp;
    }

    XYZ* take()
    {
        return _mp;
    }
}; 


int main ()
{
    XYZ x = {1, 2, 3};
    XYZ y = {2, 1, 3};
    XYZ z = {3, 1, 2};
    _xyz_map[1] = x;
    _xyz_map[2] = y;
    _xyz_map[3] = z;
    XYZ a = {4, 1, 4};

    XyzPtr xp1(new XYZ(7, 11, 13));
    XyzPtr xp2(new XYZ(17, 19, 23));
    XYZ b = *xp1;
    std::cout << "B:" << b.x << ':' << b.z << std::endl;
    std::cout << "Xp1" << xp1->x << ':' << xp1->z << std::endl;
    std::cout << "Xp2" << xp2->x << ':' << xp2->z << std::endl;
    
    std::pair<uint32_t, XYZ> p = std::make_pair<uint32_t, XYZ>(4, std::move(a));
    _xyz_map.emplace(p);
    for (auto a : _xyz_map) {
        std::cout << a.first << ':' << a.second.x << std::endl;
    }

    X* x11_3 = new X3(X2(11, 13), 17);
    print_x3("reg-ptr-down-cast", *(static_cast<X3*>(x11_3)));
    std::shared_ptr<X> sx11_3p = std::make_shared<X>(X3(X2(11, 13), 17));
    X3 sx11_3 = *(std::static_pointer_cast<X3>(sx11_3p));
    print_x3("shrd-ptr-down-cast", sx11_3);
    std::shared_ptr<X3> sx3p = std::make_shared<X3>(X3(X2(11, 13), 17));
    print_x3("shrd-ptr-w/o-cast", *sx3p);

    return 0;
}
