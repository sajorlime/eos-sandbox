class B
{
    public:
        virtual void f(int) = 0;
        virtual void f(double) = 0;
};

//in X.h:
class X : public B
{
    public:
        virtual void f(int) override;
        virtual void f(double) = 0;
};

//in Y.h:
class Y : public B
{
    public:
        virtual void f(int) = 0;
        virtual void f(double);
};



int main(int argc, char** argv)
{
    return 0;
}

