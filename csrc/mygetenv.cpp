#include <iostream>
#include <cstdlib>

const std::string  get_log_level() {
 {
     const char* llp = std::getenv("LOG_LEVLE");                                                                                                                if (!llp)
        return "";
     return llp;
}
 
int main(int argc, char**argv)
{
    if (argc < 2) {
        std::cout << "Usage: mygetenv ENV_VAR" << std::endl;
        return 1;
    }
    std::string ev{};
    const char* menv_p = std::getenv(argv[1]);
    if (!menv_p)
        ev = "";
    else
        ev = menv_p;
    std::cout << argv[1] << " : |" << ev << '|' << std::endl;
    return 0;
}

