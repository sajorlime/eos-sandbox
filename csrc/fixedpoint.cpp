
/*
    fixedpoint.c -- implementation for some fixed point arithmetic.
                    Some parts of implmentation are in fixedpoint.h

    Types:
        Fixed -- the type of a fixed point number.  It is a 64
        bit integer.  It is coded so that the size can easily be 
        changed.

    Implemnetation notes:

        This implementation assumes that LONGEST_INT_BITS (64) bit
        arithmatic is resonably efficient on the target platform.  The
        fixed point implementation assumes that the language supports
        arithmatic of 64 bits or better, but one could have a very
        limited fixed point implementation with 32 bits.

        I believe the implentation is fairly efficient, but it could be
        improved with some inline assembly.

    Basis operations like adding, substracting, comparing are all done as native
    operatiions.  E.g.
        Fixed a;
        Fixed b;
        Fixed c;
        Fixed d;

        if (a > b)
            c = a - b;
        else
            c = b - a;
        d = a + b;

    works.

    To multiply and dived, take absolute value, convert to a string require
    functionals interfaces.  Creating a fixed point number is done with a
    macro, as is retieving the parts of a fixed point number.

        Fixed fp = newFixed(5, 0) -- sets fp to 5.0
        Fixed x = newFixed(10, 0) -- sets x to 10.0
        Fixed y = newFixed(0, FP_HALF) -- y to 0.5
        Fixed z = newFixed(0, FP_QUARTER) -- z to 0.25
        
        Fixed a = mulFixed(x, y) -- would set a 5.
        Fixed b = mulFixed(fp, y) -- would set b  5/0.5 or one.
        Fixed c = mulFixed(newFixed(0, FP_SIXTEENTH), z) -- would
                set c 1/16/1/4 or 1/64 or one.

    License to copy, use this code, modify, and or whatever is hereby granted.

*/

# define DEBUG
/* remove TEST to use */
# define TEST


# include <string.h>

# ifdef DEBUG
# include <stdio.h>
# endif

# include "fixedpoint.h"


/*
    firstOne -- returns the first one in a LONGEST_INT_BITS bit integer.

    The high order bit gets LONGEST_INT_BITS, and zero gets zero.

    Note: some architectures support and instruction
    that does the same thing.  It would be better
    to use those instructions when available.
*/
int firstOne(ullint fixed)
{
    uint8 t8;
    uint16 t16;
    ulint t32 = (ulint) (fixed >> 32);
    int fone = LONGEST_INT_BITS;

    if (fixed == 0)
        return 0;
    if (t32 == 0)
    {
        t32 = (ulint) fixed;
        fone -= 32;
    }
    t16 = (uint16) (t32 >> 16);
    if (t16 == 0)
    {
        t16 = (uint16) t32;
        fone -= 16;
    }
    t8 = (uint8) (t16 >> 8);
    if (t8 == 0)
    {
        t8 = (uint8) t16;
        fone -= 8;
    }
    if ((t8 & 0xf0) == 0)
    {
        fone -= 4;
        t8 <<= 4;
    }
    if ((t8 & 0xc0) == 0)
    {
        fone -= 2;
        t8 <<= 2;
    }
    if ((t8 & 0x80) == 0)
        fone -= 1;
    return fone;
}


/*
    lastOne -- returns the last one in a LONGEST_INT_BITS bit integer.

    The low order bit gets 1, and zero gets 0.
*/
int lastOne(ullint fixed)
{
    uint8 t8;
    uint16 t16;
    ulint t32 = (ulint) fixed;
    int lone = 0;

    if (fixed == 0)
        return 0;

    if (t32 == 0)
    {
        t32 = (ulint) (fixed >> 32);
        lone += 32;
    }
    t16 = (uint16) t32;
    if (t16 == 0)
    {
        t16 = (uint16) (t32 >> 16);
        lone += 16;
    }
    t8 = (uint8) t16;
    if (t8 == 0)
    {
        t8 = (uint8) (t16 >> 8);
        lone += 8;
    }
    if ((t8 & 0x0f) == 0)
    {
        lone += 4;
        t8 >>= 4;
    }
    if ((t8 & 0x03) == 0)
    {
        lone += 2;
        t8 >>= 2;
    }
    if ((t8 & 0x01) == 0)
        lone += 1;
    return lone;
}

/*
    mulFixed -- fixed point multiply implementation.

    We want to maintain as much precision as possible, and still be efficiant.

    Take absolulte value, and remember return sign.

    Figure out how much room we have.  If there is enough room do
    the multiply and scale to our fixed point.  If there is not
    enough room, iteratively degrade the bigger of the two values.

    Much like floating point we first normalize than scale.
*/
Fixed mulFixed(Fixed fixd0, Fixed fixd1)
{
    int dpp;
    int s0;
    int s1;
    int sr;
    Fixed r;
    int fone0;
    int fone1;
    int lone0;
    int lone1;
    int round = 0;

    if ((fixd0 == 0) || fixd1 == 0)
    {
        return 0;
    }

    /* take and remember the signs */
    s0 = !!(fixd0 & FP_SIGN);
    s1 = !!(fixd1 & FP_SIGN);
    sr = s0 ^ s1;

    /* take the absolute values */
    if (s0)
        fixd0 = NEG_FIXED(fixd0);
    if (s1)
        fixd1 = NEG_FIXED(fixd1);

    /* figure the head room */
    lone0 = lastOne(fixd0);
    lone1 = lastOne(fixd1);

    /* shift down for room */
    fixd0 >>= lone0;
    fixd1 >>= lone1;

    dpp = 2 * FP_RES - lone0 - lone1;

    if (dpp >= 32) /* de we have enough room? */
    {
        fone0 = firstOne(fixd0);
        fone1 = firstOne(fixd1);

        /* make sure we don't have enough room */
        while ((fone0 + fone1) > LONGEST_INT_BITS)
        {
            int shift = dpp - (LONGEST_INT_BITS / 2);
            int diff = fone0 - fone1;
            if (diff > 0)
            {
                if (diff > shift)
                    diff = shift;
                /* we just remember the last one */
                round = ((int) fixd0) & (1 << diff);
                round >>= diff;
                fixd0 >>= diff;
                fone0 -= diff;
            }
            else
            {
                if (diff == 0)
                    diff = 1;
                if (diff > shift)
                    diff = shift;
                /* we just remember the last one */
                round = ((int) fixd0) & (1 << diff);
                round >>= diff;
                fixd1 >>= diff;
                fone1 -= diff;
            }
            dpp -= diff;
        }
    }

    /* all that work to get here */
    r = fixd0 * fixd1 + round;

    /* now scale the fixed point */
    dpp = FP_RES - dpp;
    if (dpp > 0)
        r <<= dpp;
    else
        r >>= -dpp;

    if (sr)
        return NEG_FIXED(r);
    return r;
}

/*
    divFixed -- fixed point divide implementation

    We want to maintain as much precision as possible, and still be efficiant.
    Take absolulte value, and remember return sign.

    Figure out how much room we have.  If there is enough room do
    the multiply and scale to our fixed point.  If there is not
    enough room, iteratively degrade the bigger of the two values.

    Much like floating point we first normalize than scale.
*/
Fixed divFixed(Fixed num, Fixed div)
{
    Fixed r;
    int nfirstOne;
    int dfirstOne;
    int dsw = FP_RES;
    int sn;
    int sd;
    int sr;
    int rshft;
    int lshft;

    if (num == 0)
    {
        return 0;
    }

    sn = !!(num & FP_SIGN);
    sd = !!(div & FP_SIGN);
    sr = sd ^ sn;

    if (sn)
        num = NEG_FIXED(num);
    if (sd)
        div = NEG_FIXED(div);

    nfirstOne = firstOne(num);
    dfirstOne = firstOne(div);
    dsw = nfirstOne - dfirstOne;

    div = div << (LONGEST_INT_BITS - dfirstOne);
    num = num << (LONGEST_INT_BITS - nfirstOne);
    if (num == div)
    {
        r = LL1 << (FP_RES + dsw);

        if (sr)
            return NEG_FIXED(r);
        return r;
    }
# define FIXED_DIV 1
# if FIXED_DIV
    rshft = lshft = 32;
# else
    lshft = lastOne(div) - 1;
    if (lshft < 32)
        lshft = 32;
    rshft = 63 - lshft;
# endif
    if (num > div)
    {
        lshft--;
        dsw += 1;

    }
    r = div >> lshft;
    r = num / r;

    r <<= rshft;

    dsw = FP_INT_SHT - dsw;
    if (dsw >= 0)
        r >>= dsw;
    else
        r <<= dsw;

    if (sr)
        return NEG_FIXED(r);
    return r;
}




int roundFixed(Fixed fixed)
{
    int v;
    int s = !!(fixed & FP_SIGN);

    fixed = absFixed(fixed);
    v = intFixed(fixed);

    if (fixed & FP_HALF)
        v++;
    if (s)
        return -v;
    return v;
}


char* fixedToString(Fixed fixed, char* pBuf, int base)
{
    int s;
    char fbuf[32];
    char* pfBuf;
    int fpos;
    ulint i;
    ullint f;
    Fixed ten = newFixed(10, 0);
    int flimit = sizeof(fbuf) / 2;

    s = !!(fixed & FP_SIGN);
    fixed = absFixed(fixed);
    i = intFixed(fixed);
    f = fracFixed(fixed);

    switch (base)
    {
    case 16:
        sprintf(pBuf, "%c%x.%01luxH",
                s ? '-' : '+',
                (unsigned) i,
                (uint64_t) f);
        break;
    default:
    case 10:
        fpos = sizeof(fbuf) / 4 + 1;
        pfBuf = &fbuf[fpos];

        *pfBuf++ = '.';
        if (f == 0)
            *pfBuf++ = '0'; /* don't move, if f != 0 replace */
        while (f > 0)
        {
            char c;

            f = mulFixed(f, ten);
            c = ((char) intFixed(f));
            f -= newFixed((unsigned) c, 0);
            *pfBuf++ = c + '0';
            if (--flimit <= 0)
                break;
        }
        *pfBuf = 0;

        pfBuf = &fbuf[fpos - 1];

        if (i == 0)
            *pfBuf-- = '0';
        while (i > 0)  
        {
            ulint oi = i % 10;
            char c = ((char) oi) + '0';

            i /= 10;
            *pfBuf-- = c;
        }

        if (s)
            *pfBuf = '-';
        else
            pfBuf++;
        strcpy(pBuf, pfBuf);
        break;
    }
    

    return pBuf;
}

# if defined (DEBUG) || (FIXED_TO_DOUBLE)

# include <math.h>
# include <stdarg.h>

double toDouble(Fixed fixed)
{
    double i;
    double f;
    llint x = (llint) fixed;
    llint one = (llint) LL1;
    double s = 1.0;

    if ((!!(fixed & FP_SIGN)))
    {
        s = -1.0;
    }
    fixed = absFixed(fixed);
    x = (llint) fixed;
    i = intFixed(fixed);
    f = (double) ((x & ((one << (FP_RES + 0)) - 1)) / ((double) (one << FP_RES)));

    return s * (i + f);
}

# endif


# if defined(DEBUG)

# include <stdlib.h>



void printFixed(const char* pF, ...)
{
    va_list vl;
    //char hexbuf[32];
    char fbuf[32];
    char* pPercent;
    char* pNext;
    char* pAlloc;
    int len;
    ullint f;

    len = strlen(pF);
    
    pAlloc = (char*) malloc(len + 1);
    pF = strcpy(pAlloc, pF);

    va_start(vl, pF);


    while ((pF != 0) && ((pPercent = strchr(pF, '%')) != 0))
    {
        switch (*(pPercent + 1))
        {
        case 'F':
            f = va_arg(vl, ullint);

            *pPercent = 0;
            fixedToString(f, fbuf, 10);
            fprintf(stdout, "%s%s", pF, fbuf);
            pF = pPercent + 2;
            break;
        case 'H':
            f = va_arg(vl, ullint);

            *pPercent = 0;
            fixedToString(f, fbuf, 16);
            fprintf(stdout, "%s%s", pF, fbuf);
            pF = pPercent + 2;
            break;
        default:
            pNext = strchr(pPercent + 1, '%');
            if (pNext)
            {
                *pNext = 0;
                vfprintf(stdout, pF, vl);
                *pNext = '%';
                pF = pNext;
            }
            else
            {
                pF = pNext;
                break;
            }
        }
    }
    if (pF)
        vfprintf(stdout, pF, vl);

    free(pAlloc);

    va_end(vl);
}

Fixed testDiv(Fixed num, Fixed div)
{
    double fnum = toDouble(num);
    double fdiv = toDouble(div);
    double fr;
    Fixed r;
    double diff;

    r = divFixed(num, div);
    fr = fnum / fdiv;

    if (r == 0)
        printf("****");
    printf("DIV: ");
    printFixed("%F / %F = %F ", num, div, r);
    diff = fabs(fr - toDouble(r));
    printf("%f, |r - rf| = %0.10f", fr, diff);
    if (diff < 0.00001)
        printf("!!!!!");
    else
    {
        printf("  r/fr = %f", fabs(toDouble(r) / fr));
    }
    printf("\n");

    return r;
}


Fixed testMul(Fixed fx0, Fixed fx1)
{
    double ffx0 = toDouble(fx0);
    double ffx1 = toDouble(fx1);
    double fr;
    Fixed r;
    double diff;

    r = mulFixed(fx0, fx1);
    fr = ffx0 * ffx1;

    if (r == 0)
        printf("****");
    printf("MUL: ");
    printFixed("%F * %F = %F ", fx0, fx1, r);
    diff = fabs(fr - toDouble(r));
    printf("%f, |r - fr| = %0.10f", fr, diff);
    if (diff < 0.00001)
        printf("!!!!!");
    printf("\n");

    return r;
}



void testFixed(void)
{
    Fixed at;
    Fixed odt;
    Fixed idt;
    Fixed one;
    Fixed two;
    Fixed onetenth;
    Fixed onehalf;
    Fixed twotenth;
    Fixed ten;
    Fixed one32;
    int i;
    double f;

    printFixed("10000 = %F\n", newFixed(-10000, 0));

    one = newFixed(1, 0);
    two = newFixed(2, 0);
    at = newFixed(1, 0);
    ten = newFixed(10, 0);

    onehalf = FP_HALF;
    at = divFixed(at, two);

    at = newFixed(1000, 0);
    onetenth = testDiv(at, ten);
    printFixed("Half %F != %H\n", onehalf, at);
    onetenth = one;
    onetenth = testDiv(onetenth, ten);
    at = newFixed(1, 0);
    at = testMul(at, one);
    at = testMul(at, two);
    at = testMul(at, ten);
    at = testMul(at, onetenth);
    onehalf = one;
    onehalf = testDiv(onehalf, two);
    at = one;
    at = testMul(at, onehalf);
    at = testDiv(at, ten);
    at = testDiv(at, ten);
    printFixed("%F 10?\n\n", ten);
    at = newFixed(10000, 0);
    at = testDiv(at, ten);
    at = testDiv(at, ten);
    at = testMul(at, ten);
    at = testMul(at, ten);
    at = newFixed(1, 0);
    two = newFixed(2, 0);
    at = at - two;
    at = at - one;
    one = newFixed(1, 0);
    two = newFixed(2, 0);
    printf("div 1/2\n");
    one = testDiv(one, two); /* = 1/2 */
    f = 1.0 / 3.0;
    printf("\ndiv 1/3: %3.9f\n", f);
    one = newFixed(1, 0);
    at = newFixed(3, 0);
    one = testDiv(one, at); /* = 1/3 */
    at = newFixed(48000, 0);
    odt = newFixed(1, 0);
    f = 1.0 / 48000.0;
    printf("\ndiv 1/48k: %3.9f\n", f);
    odt = testDiv(odt, at); /* = 1/48000 */
    printf("\ndiv 1/1/48k: %3.9f\n", 1.0/ f);
    at = newFixed(1, 0);
    at = testDiv(at, odt); /* = 1/ 1/48000 */
    at = newFixed(22500, 0);
    idt = newFixed(1, 0);
    idt = testDiv(idt, at); /* = 1/48000 */
    f = 1.0 / 22500.0;

    printf("TEST A TIME\n");
    one = newFixed(1, 0);
    ten = newFixed(10, 0);
    f = 1.0/10.0;
    printf("div 1/10: %3.9f\n", f);

    at = newFixed(0, 0);
    at = at - onetenth;
    at = at - onetenth;
    twotenth = newFixed(2, 0);
    twotenth = testDiv(twotenth, ten);
    at = twotenth;
    at = at - twotenth;
    at = newFixed(32, 0);
    at = newFixed(32, 0);
    one32 = one;
    one32 = testDiv(one32, at);




    printf("for 10000 to 0.0001 in 1/10 steps, and back by division\n");
    at = newFixed(10000, 0);
    for (i = 0; i < 10; i++)
    {
        at = testDiv(at, ten);
    }
    for (i = 0; i < 10; i++)
    {
        at = testDiv(at, onetenth);
    }


    at = newFixed(1 << 16, 0);
    printf("for 1 << 16 to 1 / 1 << 160. in 1/2 steps, and back by multiplication\n");
    for (i = 0; i < 10; i++)
    {
        at = testMul(at, onehalf);
    }
    for (i = 0; i < 10; i++)
    {
        at = testMul(at, two);
    }

    at = newFixed(10000, 0);
    printf("for 10000 to 0.0001 in 1/10 steps, and back by multiplication\n");
    for (i = 0; i < 10; i++)
    {
        at = testMul(at, onetenth);
    }
    for (i = 0; i < 10; i++)
    {
        at = testMul(at, ten);
    }

    printf("for -10000 to -0.0001 in 1/10 steps, and back by division\n");
    at = newFixed(-10000, 0);
    for (i = 0; i < 10; i++)
    {
        at = testDiv(at, ten);
    }
    for (i = 0; i < 10; i++)
    {
        at = testDiv(at, onetenth);
    }

    printf("for -10000 to -0.0001 in 1/10 steps, and back by multiplication\n");
    for (i = 0; i < 10; i++)
    {
        at = testMul(at, onetenth);
    }
    for (i = 0; i < 10; i++)
    {
        at = testMul(at, ten);
    }

}

# ifdef TEST
    int main(void)
    {
        testFixed();
        return 0;
    }
# endif

# endif

