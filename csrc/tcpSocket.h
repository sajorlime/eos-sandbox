/* 
// 
// $Id$ 
// 
// Copyright (c) 1999-2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: tcpSocket.h
// 
// Creation Date: 11/11/99
// 
// Last Modified: 
// 
// Author: Prakash Manghnani
// 
// Description: 
//	Class representing a tcp based socket. Has convenience methods connecting to server,
//  reading & writing data from/to a server.
// 
//  
*/ 
 
#ifndef __TCPSOCKET_H__
#define __TCPSOCKET_H__


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <winsock2.h>

# include "socket.h"

__BEGIN_DC_NAME_SPACE

class CTcpSocket : public CSocket 
{
public:

	CTcpSocket ();
	CTcpSocket (DSocket s);
	virtual ~CTcpSocket();

	bool open(const char *hostName, int portNo);	// open socket & connect to client

	int readAvailable();			// Routine returns no of bytes available for read.
	int read(char *buf, int len);	// Routine to read from scoket. Returns nbytes read
	int write(const char *buf, int len);	// Routine to write to socket. Returns nBytes written.

protected:
	bool connect(const char *hostName, int portNo);
};

__END_DC_NAME_SPACE

#endif

