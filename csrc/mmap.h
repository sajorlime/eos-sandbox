/*
// 
// $Id:$ 
// 
// Copyright (c) 1999-2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: mmapit.h
// 
// Creation Date: 4/27/00
// 
// Last Modified: 
// 
// Author: Emil P. Rojas
// 
// Description: 
//  Publishes a very simple interface for memory mapping a file.  
//
//    The CMMap class provides basic mapping, and serveral extern "C" interfaces to make the
//    same basic mapppings available in C.
//  
*/ 


# ifndef __MMAP_H__
# define __MMAP_H__

# include "lapeltype.h"
# include "exception.h"


enum EMemoryAccess
{
    EMA_ReadOnly,
    EMA_ReadWrite,
    Last_EMemoryAccess
};

enum EMemoryView
{
    EMV_Private,
    EMV_Shared,
    Last_EMemoryView
};


class XMMap : public XException
{
public:
    XMMap (const char * pReason)
        : XException (pReason)
    {
    }
    virtual ~XMMap () {}
};

/*
    CMMap -- provides some basic capabilites for memory mapping files,
    but by no means tries to provide all capabilities that might be provided.
    One can map a file and get a pointer to the file and the size of the file.
    But the class does not allow one to manipulate or set the offset of size of
    the mapping.  Only existing files can be mapped.
    If occasion should arise to need additional features please add them on
    an as needed basis.
*/

class CMMap
{
    void * m_pV;
    size_t m_size;
    uint m_handle;
    EMemoryAccess m_access;
    EMemoryView m_view;
    // should I have the file name that was passed in?

public:
    CMMap (const char * pFileName,
           EMemoryAccess access = EMA_ReadOnly,
           EMemoryView view = EMV_Private,
           bool create = false
          ) throw (XMMap);

    CMMap (const char * pFileName,
           size_t fileSize,
           EMemoryView view =EMV_Shared 
          ) throw (XMMap);

    size_t size (void) const {return m_size;};
	//operator void * (void) const {return m_pV;};
	void * data (void) const {return m_pV;};
    virtual ~CMMap ();
};


# endif

