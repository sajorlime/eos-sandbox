/*senderprog.c - a client, datagram*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
# include "argsget.h"

/* the port users will be connecting to */

#define MYPORT 4950


static char g_port[128] = "-port";
static char g_host[128] = "-host";

int main(int argc, char *argv[])
{

    int sockfd;

    /* connector's address information */
    struct sockaddr_in their_addr;
    struct hostent *he;
    int numbytes;
    int rcnt;

    Arguments args(argc, argv);

# if 0
    if (argc != 2)
    {
        fprintf(stderr, "Client-Usage: %s [hostname]\n", argv[0]);
        exit(1);
    }
# endif

    int myport = MYPORT;
    if (args.intValue(g_port, &rcnt))
    {
        myport = rcnt;
        printf("port set to %d\n", myport);
    }

    const char * host = "localhost";
    if (args.get(g_host, sizeof(g_host)))
    {
        printf("g_host: %s\n", g_host);
        host = g_host;
    }
    printf("host: %s\n", host);


    /* get the host info */
    if ((he = gethostbyname(host)) == NULL)
    {
        perror("Client-gethostbyname() error lol!");
        exit(1);
    }
    else
    {
        printf("Client-gethostname() is OK...\n");
    }

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("Client-socket() error lol!");
        exit(1);
    }
    printf("Client-socket() sockfd is OK...\n");

    /* host byte order */
    their_addr.sin_family = AF_INET;

    /* short, network byte order */
    printf("Using port: %d\n", myport);

    their_addr.sin_port = htons(myport);
    their_addr.sin_addr = *((struct in_addr *)he->h_addr);

    /* zero the rest of the struct */
    memset(&(their_addr.sin_zero), '\0', 8);

    int rc = connect(sockfd, 
                    (struct sockaddr *)&their_addr,
                    sizeof(struct sockaddr));

    while (1)
    {
        char in[128];
        numbytes = recv(sockfd, in, sizeof(in), 0);
        if (numbytes <= 0)
        {
            fprintf(stderr, "bad read from sockfd: %d\n", numbytes);
            break;
        }
        in[numbytes] = 0;

        printf("got from server(%d): %s", numbytes, in);
        write(sockfd, in, numbytes);
    }

    if (close(sockfd) != 0)
        printf("Client-sockfd closing is failed!\n");
    else
        printf("Client-sockfd successfully closed!\n");
    return 0;
}

