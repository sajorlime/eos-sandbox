/*
    tcp_test_server

    The program really just acts as a server that reads lines from a file and sends
    them to the client with a delay between reads. However this is the simplest
    to allow testing of arbritary PUI input.

    Usage:
        tcp_test_server [-port P] [-secs S] [-tf file-to-read]
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
# include "argsget.h"

/* the port users will be connecting to */

#define MYPORT 3773
#define MAXBUFLEN 500


static char g_test_file[128] = "-tf";
static char g_port[128] = "-port";
static char g_secs[128] = "-secs";

int main(int argc, char *argv[])
{
    int listen_socket;
    struct sockaddr_in my_addr; /* my address information */
    struct sockaddr_in their_addr; /* connector's address information */
    int addr_len;
    int numbytes;
    char buf[MAXBUFLEN];
    int rcnt;

    Arguments args(argc, argv);

    int myport = MYPORT;
    if (args.intValue(g_port, &rcnt))
    {
        myport = rcnt;
        printf("port set to %d\n", myport);
    }

    const char * input_name = "test-data.txt";
    if (args.get(g_test_file, sizeof(g_test_file)))
    {
        printf("g_test_file: %s\n", g_test_file);
        input_name = g_test_file;
    }
    printf("tf: %s\n", input_name);

    FILE * ifile = fopen(input_name, "r");
    if (ifile == 0)
    {
        fprintf(stderr, "pts failed to open %s\n", input_name);
        return -3;
    }

    int secs = 1;
    if (args.intValue(g_secs, &rcnt))
    {
        secs = rcnt;
        printf("secs set to %d\n", secs);
    }

    listen_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (listen_socket == -1)
    {
        perror("listen_socket error");
        exit(1);
    }
     
    /* TODO(erojas): clean up cribed code. */
    /* host byte order */
    my_addr.sin_family = AF_INET;

    /* short, network byte order */
    my_addr.sin_port = htons(myport);

    /* automatically fill with my IP */
    my_addr.sin_addr.s_addr = INADDR_ANY;

    /* zero the rest of the struct */
    memset(&(my_addr.sin_zero), '\0', 8);

    if (bind(listen_socket,
             (struct sockaddr *)&my_addr,
             sizeof(struct sockaddr)) == -1)

    {
        perror("Server-bind() error lol!");
        exit(1);
    }

    addr_len = sizeof(struct sockaddr);

    listen(listen_socket, 1);
    printf("listned : %d\n", listen_socket);
    int wsock = accept(listen_socket, 0, 0);
    printf("connection accepted: %d\n", wsock);

    while (1)
    {
        char out[128];

        if (feof(ifile))
        {
            break;
        }
        fgets(out, sizeof(out), ifile);

        char * sharp = strchr(out, '#');
        if (sharp)
        {
            *sharp = '\n';
            *(sharp + 1) = 0;
        }

        if (out[0] < ' ')
        {
            printf ("skip: %s", out);
            continue;
        }

        numbytes = strlen(out);

        printf("(%d):out: %s", numbytes, out);
        int wcnt;
        wcnt = write(wsock, out, numbytes);

        if (wcnt < 0)
        {
            break;
        }
        char rbuf[64];
        size_t rcnt = read(wsock, rbuf, wcnt);
        printf(rbuf);
        if (rcnt != wcnt)
        {
            printf("%u != %u\n", rcnt, wcnt);
        }
        sleep(secs);

        if (buf[0] == 'x')
        {
            break;
        }
    }

    close(wsock);
    close(listen_socket);
    return 0;
}


