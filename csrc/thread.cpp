// thread example
//
#include <iostream>       // std::cout
#include <thread>         // std::thread
#include <chrono>         // std::chrono

 
void foo() 
{
  // do stuff...
  auto id = std::this_thread::get_id();
  std::cout << "Foo you wait 5 secs" << " tid: " << std::hex << id << std::endl;
  std::this_thread::sleep_for(std::chrono::seconds(5));
  std::cout << "Foo you" << std::endl;
}

void bar(int x)
{
  // do stuff...
  auto id = std::this_thread::get_id();
  std::cout << "bar wait 2 secs" << " tid: " << std::hex << id << std::endl;
  std::this_thread::sleep_for(std::chrono::seconds(2));
  std::cout << "bar you" << std::endl;
}

int main() 
{
  int a = 0;
  std::thread first (foo);     // spawn new thread that calls foo()
  std::thread second (bar,&a);  // spawn new thread that calls bar(0)

  std::cout << "main, foo and bar now execute concurrently...\n";

  // synchronize threads:
  first.join();                // pauses until first finishes
  second.join();               // pauses until second finishes

  std::this_thread::sleep_for(std::chrono::seconds(2));
  std::cout << "foo and bar completed.\n";

  return 0;
}

