#include <iostream>
#include <iomanip>
#include <unordered_map>
using namespace std;

string bit(uint32_t v, int mbits=5)
{
    string o = "";
    auto bit = 1 << mbits;
    while (bit > 0) {
        if (bit & v)
            o += '1';
        else
            o += '0';
        bit >>= 1;
    }
    return o;
}

uint32_t higher16(uint32_t v)
{
    auto x = v / 16;
    return (x + 1) * 16;
}
int main() 
{
    cout << bit(1) << endl;
    cout << bit(5) << endl;
    cout << bit(8) << endl;
    cout << higher16(0) << endl;
    cout << higher16(1) << endl;
    cout << higher16(2) << endl;
    cout << higher16(15) << endl;
    cout << higher16(23) << endl;
    cout << higher16(50) << endl;

    return 0;
}
