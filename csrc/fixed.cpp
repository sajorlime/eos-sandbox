
/*
 * copyright Copyright  (c)1997 Lapel Software (Emil P. Rojas).
 * All rights reserved.
 *
 * File: fixed.cpp
 *
 * $Id: fixed.cpp,v 1.1 1997/12/16 21:54:39 emil Exp $
 * $Source: /export2/Repositories/bdnswlibs/misc/fixed.cpp,v $
 *
 * $Log: fixed.cpp,v $
// Revision 1.1  1997/12/16  21:54:39  emil
// New
//
 *
 */


# include "fixed.h"

# include <stdio.h>


struct s64
{
    uint32 lo;
    uint32 hi;
};

uint64 fullUMul (uint64 multiplicand, uint64 multiplier, uint64 & highProd)
{
    // note: we could have the copies here by using a refernece
    s64 mc;
    mc.lo = (uint32) multiplicand;
    mc.hi = (uint32) (multiplicand >> fixed::fixedPos);
    s64 mr;
    mr.lo = (uint32) multiplier;
    mr.hi = (uint32) (multiplier >> fixed::fixedPos);

    uint64 prod0 = (uint64) mc.lo * mr.lo;
    uint64 prod1 = (uint64) mc.hi * mr.lo;
    uint64 prod2 = (uint64) mc.lo * mr.hi;
    highProd = (uint64) mc.hi * mr.hi;
    
    //                      y   z
    //                      a   b
    //                      _____
    //                     xx  bz
    //                 xx  by
    //                 xx  az
    //             xx  ay
    //      ______________________________
    //         high prod  |  low prod
    //      ______________________________
    // col  8   7   6   5 |  4   3   2   1
    //
    // put high product in highProd, and low product in prod0
    //

    // high product is highProd and low parts of p2 & p1,
    // it also needs the carry for column 2.
    highProd += prod2 >> fixed::fixedPos;
    highProd += prod1 >> fixed::fixedPos;

    // to calculate the carry we need to break it down.
    uint64 c = (prod0 >> fixed::fixedPos);
    c += (prod1 >> fixed::fixedPos);
    c += (prod2 >> fixed::fixedPos);
    highProd += c >> fixed::fixedPos;

    prod0 += prod1 << fixed::fixedPos;
    prod0 += prod2 << fixed::fixedPos;

    return prod0;
}



uint64 extract64 (uint64 & hi, uint64 & lo, uint32 bit)
{
    if (bit >= 64)
    {
        bit -= 64;
        return hi >> bit;
    }
    return (hi << (64 - bit)) | (lo >> bit);
}




fixed operator* (fixed & fxd1, fixed & fxd2)
{
    fixed r;

    r.m_sign = fixed::fixedPositive;
    if (fxd1.m_sign != fxd2.m_sign)
        r.m_sign = fixed::fixedNegative;
    uint64 highProd;
    uint64 lowProd = fullUMul (fxd1.m_fxd, fxd2.m_fxd, highProd);
    lowProd = extract64 (highProd, lowProd, fixed::fixedPos);
    if (int64 carry = (lowProd >> (fixed::fixedPos - 1)) & 1)
    {
        lowProd += carry;
    }
    r.m_fxd = lowProd;
    return r;
}


int firstOne (uint64 x)
{
    if (!x)
        return 64;
    int r = 0;
    uint32 s = (uint32) (x >> 32);
    if (s == 0)
    {
        r += 32;
        s = (uint32) x;
    }
    uint16 t = (uint16) (s >> 16);
    if (t == 0)
    {
        r += 16;
        t = (uint16) s;
    }
    uint8 u = (uint8) (t >> 8);
    if (u == 0)
    {
        r += 8;
        u = (uint8) t;
    }
    uint8 w = (u >> 4);
    if (w == 0)
    {
        r += 4;
        w = u;
    }
    static char bitsLookup [] = {4, 3, 2, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0};
    return r + bitsLookup[w];
}


fixed operator/ (fixed & numerator, fixed & divisor)
{
    uint64 rem;
    uint64 quo;
    uint64 div;
    fixed quotient;
    int nBits;

    //
    // n / d = q + r/d;
    // r/d <= 0
    //
    quotient.m_sign = fixed::fixedPositive;
    if (numerator.m_sign != divisor.m_sign)
        quotient.m_sign = fixed::fixedNegative;
    // Get the integer part.
    div = divisor.m_fxd;
    quo = numerator.m_fxd / div; // quo is the integer part
    rem = numerator.m_fxd - (quo * div); // rem gives the fractional part
    // Get the fraction part.
    nBits = firstOne (rem);
    if (nBits < fixed::fixedPos)
    {
        div = divisor.m_fxd >> (fixed::fixedPos - nBits);
        rem <<= nBits;
    }
    else
    {
        rem <<= fixed::fixedPos;
    }
    rem = rem / div;
    // move quo into the integer position, 
    quotient.m_fxd = (quo << fixed::fixedPos) + rem;

    return quotient;
}

// Sign result matrix, used below and in other places
//       f2
//    -|  0  -1
//   ----------
//f1  0|  0   1
//   -1| -1   0


fixed operator+ (fixed & fxd1, fixed & fxd2)
{
    fixed r;

    // default sign to be the same is fxd1
    r.m_sign = fxd1.m_sign;

    int16 s = fxd1.m_sign - fxd2.m_sign;
    if (s != 0) // they have differing signs
    {
        // use subtraction
        if (s < 0) // fxd1 is negative
        {
            int64 i = fxd2.m_fxd - fxd1.m_fxd;
            r.m_sign = fixed::fixedPositive;
            if (i < 0)
            {
                r.m_sign = fixed::fixedNegative;
                i = -i;
            }
            r.m_fxd = (uint64) i;
            return r;
        }
        // fxd2 is negative
        // assume positive result
        // r.m_sign = fixed::fixedPositive; sign already positive
        int64 i = fxd1.m_fxd - fxd2.m_fxd;
        if (i < 0)
        {
            r.m_sign = fixed::fixedNegative;
            i = -i;
        }
        r.m_fxd = (uint64) i;
        return r;
    }
    r.m_fxd = fxd1.m_fxd + fxd2.m_fxd;
    return r;
}




# ifdef TEST_FIXED
const char vHex[] = "0123456789ABCDEF";

ostream & operator<< (ostream& outstrm, fixed & fxd)
{
    uint32 whole;
    uint32 frac;
    char sFrac[9];

    if (fxd.m_sign)
    {
       outstrm << '-';
    }
    whole = (uint32) (fxd.m_fxd >> 32);
    frac = (uint32) fxd.m_fxd;
    sprintf (sFrac, "%08x", frac);
    outstrm << hex << whole << '.' << sFrac;
    outstrm << flush;
    return outstrm;
}

char sDouble[64];

char * toDouble (fixed & fxd)
{
    uint32 whole;
    uint32 frac;
    double d;

    whole = (uint32) (fxd.m_fxd >> 32);
    d = (double) whole;
    frac = (uint32) fxd.m_fxd;
    d += ((double) frac) / ((double) (1 << (fixed::fixedPos - 2)) * 4.0);
    if (fxd.m_sign)
        d = -d;
    sprintf (sDouble, "%4.10f", d);
    return sDouble;
}


fixed vX[] = {fixed (0, 1), fixed (0, 2)};
fixed vA[]
=
{
    fixed (0, 2),
    fixed (0, 3 << 16),
    fixed (0, 2 << 16),
    fixed (0, 3 << 16),
    fixed (0, 3 << 24),
    fixed (0, 2 << 24),
    fixed (0, 5 << 24),
    fixed (0x1234, 5 << 24),
    fixed ((uint32) 0x8001, (uint32) 0x80008000),
    fixed (2, 0x377),
    fixed (3 << 16, 0x377),
    fixed (2 << 16, 0x377),
    fixed (3 << 16, 0x377),
    fixed (3 << 24, 0x377),
    fixed (2 << 24, 0x377),
    fixed (5 << 24, 0x377),
    fixed (5 << 24, 0x1234),
    fixed (0, 0)
};

# define NUM_TEST_CASES (sizeof (vA) / sizeof (vA[0]))

fixed vB[NUM_TEST_CASES]
=
{
    fixed (0, 1),
    fixed (0, 2),
    fixed (0, 2 << 16),
    fixed (0, 3 << 16),
    fixed (0, 3 << 16),
    fixed (0, 2 << 24),
    fixed (0, 3 << 24),
    fixed (0, 5 << 24),
    fixed ((uint32) 0xC001, (uint32) 0xC000C000),
    fixed (1, 0x377),
    fixed (2, 0x377),
    fixed (2 << 16, 0x3770ABCD),
    fixed (3 << 16, 0x3770ABCD),
    fixed (3 << 16, 0x3770ABCD),
    fixed (2 << 24, 0x3770ABCD),
    fixed (3 << 24, 0x3770ABCD),
    fixed (5 << 24, 0x3770ABCD),
    fixed (0, 0)
};


void testPlus (void)
{
    int i;
    fixed a = 1;
    fixed b = -1;
    fixed c (1, 0x55555555);
    fixed d (2, 0xAAAAAAAA);

    cout << "\ntest Plus" << endl;
    for (i = 0; i < NUM_TEST_CASES; i++)
    {
        fixed x;
        x = a + b;
        cout << " " << a << " + " << b << " = " << x << '\n';
        x = b + a;
        cout << " " << b << " + " << a << " = " << x << '\n';
        x = c + d;
        cout << " " << c << " + " << d << " = " << x << '\n';
        c += a;
        d += b;
        a = vA[i];
        b = vB[i];
    }
}


void testMinus (void)
{
    int i;
    fixed a = 1;
    fixed b = -1;
    fixed c (0x55555555, 0x55555555);
    fixed d (0xAAAAAAAA, 0xAAAAAAAA);

    cout << "\ntest Minus" << endl;
    for (i = 0; i < NUM_TEST_CASES; i++)
    {
        fixed x;
        x = a - b;
        cout << " " << a << " - " << b << " = " << x << '\n';
        x = b - a;
        cout << " " << b << " - " << a << " = " << x << '\n';
        x = c - d;
        cout << " " << c << " - " << d << " = " << x << '\n';
        c -= a;
        d -= b;
        a = vA[i];
        b = vB[i];
    }
}

void testPlusMinus (void)
{
    int i;
    fixed a = 0;
    fixed b = 0;

    cout << "\ntest PlusMinus" << endl;
    for (i = 0; i < NUM_TEST_CASES; i++)
    {
        a += vA[i];
        b += vB[i];
    }
    cout << "a = " << a << '\n';
    cout << "b = " << b << '\n';
    for (i = 0; i < NUM_TEST_CASES; i++)
    {
        a -= vA[i];
        b -= vB[i];
    }
    cout << "a = " << a << '\n';
    if (a != fixed (0))
        cout << "a should have been zero\n";
    cout << "b = " << b << '\n';
    if (b != fixed (0))
        cout << "b should have been zero\n";
}


void testTimes (void)
{
    int i;
    fixed a = 1;
    fixed b = -1;
    fixed c (0x00000001, 0x80010008);
    fixed d (0x00000003, 0x20010002);
    fixed x = 1;
    fixed y;

    cout << "\ntest Times" << endl;
    x = x * x;
    y = -x;

    for (i = 0; i < NUM_TEST_CASES; i++)
    {
        x = a * b;
        cout << " " << a << " * " << b << " = " << x << '\n';
        x = b * a;
        cout << " " << b << " * " << a << " = " << x << '\n';
        x = -a * a;
        cout << " ";
        cout << a;
        cout << " * -a ";
        cout << " = ";
        cout << x;
        cout << ", -";
        cout << y;
        cout << '\n';
        //cout << " " << a << " * -a " << " = " << x << ", -" << -x << '\n';
        x = c * d;
        cout << " " << c << " * " << d << " = " << x << '\n';
        c += a;
        d += b;
        a = vA[i];
        b = vB[i];
    }
    a = 1;
    for (i = 0; i < 17; i++)
    {
        y = a * a;
        cout << " " << a << " * " << a << " = " << y << '\n';
        a *= (fixed) 2;
    }
    a = 1;
    b = 3;
    for (i = 0; i < 31; i++)
    {
        y = a * b;
        cout << " " << a << " * " << b << " = " << y << '\n';
        a  *= fixed (2);
    }
}


fixed vD []
=
{
    fixed (0, 1),
    fixed (0, 2),
    fixed (0, 2 << 16),
    fixed (0, 3 << 16),
    fixed (0, 3 << 16),
    fixed (0, 2 << 24),
    fixed (0, 3 << 24),
    fixed (0, 5 << 24),
    fixed ((uint32) 0xC001, (uint32) 0xC000C000),
    fixed (1, 0x377),
    fixed (2, 0x377),
    fixed (2 << 16, 0x3770ABCD),
    fixed (3 << 16, 0x3770ABCD),
    fixed (3 << 16, 0x3770ABCD),
    fixed (2 << 24, 0x3770ABCD),
    fixed (3 << 24, 0x3770ABCD),
    fixed (5 << 24, 0x3770ABCD),
    (0, 1)
};



void testDivTimes (void)
{
    fixed a (1, 0x234);
    fixed b (0x1234, 0x9abcdef0);
    fixed d (0x00000020, 0x0);
    fixed q;
    fixed x;
    fixed y;
    fixed r;
    fixed diff;
    fixed diffSum = 0;
    int i;

    cout << "\ntest Div Times" << endl;
    x = a * 0x2e;
    x = a * 0x2e;
    q = b / x;
    y = q * x;
    diff = b - y;
    cout << b << " / " << x << " = "
            << q << " = "
            << y << " "
            << "dif: " << diff << '\n';
    cout << toDouble (b) << " / ";
    cout << toDouble (x) << " = ";
    cout << toDouble (q) << " = ";
    cout << toDouble (y) << " ";
    cout << "dif: " << toDouble (diff) << '\n';

    x = a * 0x80;
    q = b / x;
    y = q * x;
    diff = b - y;
    cout << b << " / " << x << " = "
            << q << " = "
            << y << " "
            << "dif: " << diff << '\n';

    cout << "Into loop\n";
    for (i = 1; i < (1 << 15); i++)
    {
        x = a * i;
        q = b / x;
        y = q * x;
        diff = b - y;
        diffSum += diff;
        cout << b << " / " << x << " = "
                << q
# if 0
                << " = "
                << y << " "
# endif
                << " dif: " << diff
                << " dS: " << diffSum
                << '\n';
        diff.abs ();
        if (diff < fixed (0, 0x1000))
        {
            cout << i << ": ";
# if 0
            cout << "The\'re not close\n";
            cout << "b = " << b << '\n';
# endif
            cout << "y = " << y << '\n';
            return;
        }
        if (i > 0x485)
            i <<= 1;
    }
}




int main (int argc, char ** argv)
{
    testPlus ();
    testMinus ();
    testPlusMinus ();
    testTimes ();
    testDivTimes ();

    return 0;
}


# endif

