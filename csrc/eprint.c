﻿//
// test eprint macro
//


#include <stdarg.h>
#include <stdio.h>


int eprint(const char * format, ...)
{
	va_list args;
	va_start (args, format);

	char buf[129];
	int nBuf = vsprintf (buf, format, args);
	va_end(args);
    printf(buf);
    return nBuf;
}

# if 0
#   define EPRINT eprint
# else
#   define EPRINT(...) ((void) 0)
# endif

int main(int argc, char** argv)
{
    if (argc < 4) {
        printf("bad arg count: %d\n", argc);
        return 1;
    }
    EPRINT("%s %s %s %s\n", argv[0], argv[1], argv[2], argv[3]);
    printf("%s %s %s %s\n", argv[0], argv[1], argv[2], argv[3]);
    return 0;
}

