/* 
// 
// $Id: //Source/Common/tcp/tracer.h#5 $ 
// 
// Copyright (c) 1999-2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: unaligned.h
// 
// Creation Date: 02/13/00
// 
// Last Modified: 
// 
// Author: Emil Rojas
// 
// Description: 

   Unaligned provides in-line functions for reading multibyte network
   byte ordered objects from memory.

//  
*/ 

# ifndef __UNALINGED_H__
# define __UNALINGED_H__

# include "dctype.h"

__BEGIN_DC_NAME_SPACE

/*
    Unaligned Network to Host
    -----------------------
    Note that this code works for either big or little endian machines.
*/
inline uint16 uantohsWInc (const uint8 * & rpData)
{
    uint16 r;
    r = *rpData++;
    r = r << 8 | *rpData++;
    return r;
};

inline uint16 uantohs (const uint8 * pData)
{
    return uantohsWInc (pData);
};


inline uint32 uantohlWInc (const uint8 * & rpData)
{
    uint32 r;
    r = uantohsWInc (rpData);
    r = r << 16 | uantohsWInc (rpData);
    return r;
};

inline uint32 uantohl (const uint8 * pData)
{
    return uantohlWInc (pData);
};


inline uint64 uantohllWInc (const uint8 * & rpData)
{
    uint64 r;
    r = uantohlWInc (rpData);
    r = r << 16 | uantohlWInc (rpData);
    return r;
};

inline uint64 uantohll (const uint8 * pData)
{
    return uantohllWInc (pData);
};

/*
 Host to Network Unaligned
 -----------------------
*/

inline uint8 * htonsua (uint8 * pData, uint16 v)
{
    *pData++ = v >> 8;
    *pData++ = v;
    return pData;
};


inline uint8 * htonlua (uint8 * pData, uint32 v)
{
    pData = htonsua (pData, (uint16) (v >> 16));
    pData = htonsua (pData, (uint16) v);
    return pData;
};


inline uint8 * htonllua (uint8 * pData, uint64 v)
{
    pData = htonlua (pData, (uint32) (v >> 16));
    pData = htonlua (pData, (uint32) v);
    return pData;
};



__END_DC_NAME_SPACE

# endif
