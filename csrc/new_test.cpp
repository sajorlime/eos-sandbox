//


# include <string>
# include <cstdio>
# include <iostream>

using namespace std;

struct sabc {
    uint8_t a;
    uint8_t b;
    uint8_t c;
    uint8_t end;
};

int main(int argc, char** argv)
{
    char* cp = new char[25];

    //sabc* abcp = (sabc*) cp;
    //sabc* abcp = static_cast<sabc*>(cp);
# if 1
    sabc* abcp = reinterpret_cast<sabc*>(cp);
    abcp->a = (uint8_t) 'a';
    abcp->b = (uint8_t) 'b';
    abcp->c = (uint8_t) 'c';
    abcp->end = 0;
# else
    sabc& abcp = reinterpret_cast<sabc&>(cp);
    cout << &abcp << endl;
    abcp.a = (uint8_t) 'a';
    abcp.b = (uint8_t) 'b';
    abcp.c = (uint8_t) 'c';
    abcp.end = 0;
# endif
    cout  << cp << endl;
    return 0;
}

