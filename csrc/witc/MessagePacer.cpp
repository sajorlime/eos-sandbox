/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
* .........................................................................
*
* File: MessagePacer.cpp
*
* .........................................................................
*
    Contains implementation of a class that paces CMessageReceiver based
    inter-thread communication.
*
* .........................................................................
*
*   Date        Description
*   ------       -----------
    06/21/99    Release
*
*
*
* NOTES:

*
*
* Classes/Types:
    CMessagePacer -- 
*
*/



# include "MessagePacer.h"


bool CMessagePacer::send (CMessage * pMsg)
{
    if (m_freeCount <= 1)
    {
        waitForEmpty ();
    }
    return m_pMR->send (pMsg);
}

CMessagePacer::CMessagePacer (CMessageReceiver * pMR, CThread * pThread)
    :
        m_pThread (pThread),
        m_pMR (pMR)
{
}


CMessage * CMessagePacer::receive (void)
{
    return m_pMR->receive ();
}

void CMessagePacer::failed (const XException & reason)
{
    m_pMR->failed (reason);
}

CMessage * CMessagePacer::allocMessage (void)
{
    return m_pMR->allocMessage ();
}

int CMessagePacer::messagesAvailable (void)
{
    return m_pMR->messagesAvailable ();
}

int CMessagePacer::attach (void)
{
    return m_pMR->attach ();
}

int CMessagePacer::detach (void)
{
    return m_pMR->detach ();
}


bool CMessagePacer::waitForEmpty (void)
{
    m_pThread->suspend ();
# if 0
    TRACE ("cmp-wfe\n");
# endif
    bool b = m_pMR->waitForEmpty ();
    m_pThread->resume ();
    return b;
}


