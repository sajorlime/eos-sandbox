/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
* File: MessageQueue.cpp
*
* .........................................................................
*
*  Date     Description
* ------   -----------
*
***************************************************************************
*
* NOTES:
*
*
* Classes/Types:
*
*
* FUNCTIONS:
*
*
*/

# include "MessageQueue.h"

void CMessageQueue::release (CMessage * pMsg)
{
    pMsg->setLength (0);
# if 0
    pMsg->m_buffer[0] = 'R';
# endif
    m_freeQueue.enqueue (pMsg);
    m_access.Lock ();
    m_freeCount++;
    m_access.Unlock ();
}



/*
    CMessageQueue -- construct a message queue.
*/
CMessageQueue::CMessageQueue (int maxMsgSize,
                              int freeCount,
                              int growFactor,
                              int blockFlags
                             )
    : 
      CMessageReceiver (maxMsgSize, freeCount, blockFlags),
      m_waiting (0),
      m_messageEvent (),
      m_msgQueue (),
      m_freeQueue ()
{
    m_freeLimit = m_freeCount * growFactor;
    m_allocCount = m_freeCount;
    for (int element = 0; element < m_freeCount; element++)
    {
        CMessage * pMsg = makeMessage (m_maxMsgSize);
# if 0
        TRACE ("pMsg:%x\n", pMsg);
# endif
        m_freeQueue.enqueue (pMsg);
    }
}

CMessageQueue::~CMessageQueue ()
{
    CMessage * pMsg;

    waitForUsers ();
    while (m_msgQueue.size () > 0)
    {
        TRACE ("Unread messages: %d\n", m_msgQueue.size ());
# if 1
        if (m_msgCount != m_msgQueue.size ())
            TRACE ("Warning queueCount: %d != size:%d\n",
                   m_msgCount, m_msgQueue.size ()
                  );
        m_msgCount--;
# endif
        if (pMsg = m_msgQueue.dequeue ())
        {
# if 0
            TRACE ("delete pMsg:%x\n", pMsg);
# endif
            delete pMsg;
        }
    }
    while (m_freeQueue.size () > 0)
    {
# if 1
        if (m_freeCount != m_freeQueue.size ())
            TRACE ("Warning freeCount: %d != size:%d\n",
                   m_freeCount, m_freeQueue.size ()
                  );
        m_freeCount--;
# endif
        if (pMsg = m_freeQueue.dequeue ())
        {
# if 0
            TRACE ("delete pMsg:%x\n", pMsg);
# endif
            delete pMsg;
        }
    }
}


CMessage * CMessageQueue::allocMessage (void)
{
    CMessage * pMsg;

    m_access.Lock ();
    int free = m_freeCount--;
    m_access.Unlock ();
    if (free > 0)
        pMsg = m_freeQueue.dequeue ();
    else
    {
# if 1
        TRACE ("Out of messages\n");
# endif
        pMsg = 0;
        if (m_allocCount < m_freeLimit)
            pMsg = makeMessage (m_maxMsgSize);
        if (!pMsg)
# if 0
            XException ("No message available");
# else
            m_access.Unlock ();
            return pMsg;
# endif
        // this implementation keeps them on the message list.
        m_access.Lock ();
        m_freeCount++;
        m_allocCount++;
        m_access.Unlock ();
    }
    pMsg->setId (EMT_NoMessage);
    pMsg->setLength (0);
# if 0
    TRACE ("mq-alloc-pMsg:%x\n", pMsg);
# endif

    return pMsg;
}


CMessage * CMessageQueue::receive (void)
{
    CMessage * pMsg;

    m_access.Lock ();
    while (m_msgCount == 0)
    {
        if (!(m_blockFlags & CMessageReceiver::blockOnReceive))
        {
            // satify the receive contract
            notifyEmpty ();
            return static_cast<CMessage *> (0);
        }
        m_waiting++;
        m_access.Unlock ();
        // satisfy the receive contract
        notifyEmpty ();
        m_messageEvent.Lock ();
        m_waiting--;
        m_access.Lock ();
    }
    m_msgCount--;
    if (m_msgCount <= 0)
        notifyEmpty ();
    pMsg = m_msgQueue.dequeue ();
    m_access.Unlock ();
    ASSERT (pMsg);
    if (m_pExcept)
        throw XException (*m_pExcept);
    return pMsg;
}

/* from CMessageReceiver */

inline bool CMessageQueue::send (CMessage * pMsg)
{
    m_access.Lock ();
    m_msgQueue.enqueue (pMsg);
    m_msgCount++;
    if (m_waiting)
    {
        m_messageEvent.SetEvent ();
    }
    m_access.Unlock ();
    if (m_freeCount <= 0)
    {
        TRACE ("cmq-send: blockFlags: %x\n", m_blockFlags);
        if (m_blockFlags & CMessageReceiver::blockOnSendFull)
            waitForEmpty ();
    }
# if 0
    TRACE ("Sent a message");
# endif
    return true;
}


void CMessageQueue::failed (const XException & reason)
{
    if (m_pExcept)
        throw XException ("second call to failed");
    m_pExcept = new XException (reason);
}

