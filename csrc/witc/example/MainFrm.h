/***************************************************************************
 *																									*
 *							SiRF Technology, Inc. GPS Software							*
 *																									*
 *		SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source		*
 *		is the sole property of SiRF Technology, Inc.  Reproduction or			*
 *		utilization of this source in whole or in part is forbidden				*
 *		without the written consent of SiRF Technology, Inc.						*
 *																									*
 ***************************************************************************
 *																									*
 *				(c) SiRF Technology, Inc. 1996 -- All Rights Reserved				*
 *																									*
 ***************************************************************************
 *
 * MAINFRM.H
 *
 * .........................................................................
 *
 * $Workfile:   mainfrm.h  $
 * $Revision:   1.19  $
 *     $Date:   29 Jan 1998 12:30:44  $
 *
 * .........................................................................
 *
 *
 *
 * .........................................................................
 *
 *      $Log:   L:\sirfdemo\ARCHIVE\mainfrm.h_v  $
 * 
 *    Rev 1.19   29 Jan 1998 12:30:44   Srdjan
 * Added OnActionSwitchOpMode
 * 
 *    Rev 1.18   03 Dec 1997 15:12:52   andy
 * 
 * add StoreAlmanac() and StoreEphemeris()
 * 
 *    Rev 1.17   10 Oct 1997 13:31:34   andy
 * 
 * add Poll/Set Ephemeris
 * 
 *    Rev 1.16   25 Jul 1997 18:38:22   andy
 * 
 * modify ShowView() params
 * 
 *    Rev 1.15   19 May 1997 16:19:36   andy
 * 
 * add Poll/SetAlmanac
 * 
 *    Rev 1.14   28 Apr 1997 14:37:12   andy
 * 
 * add SetDGPSPort()
 * 
 *    Rev 1.13   16 Apr 1997 14:27:32   andy
 * 
 * handle new views error and query
 * 
 *    Rev 1.12   15 Apr 1997 17:01:32   srdjan
 * 
 * Last 3 Navigation menu items
 * 
 *    Rev 1.11   14 Apr 1997 20:46:04   srdjan
 * 
 *  Elev and power mask.
 * 
 *    Rev 1.10   14 Apr 1997 16:12:22   srdjan
 * 
 * DGPS menu item hooked up.^D
 * 
 * 
 *    Rev 1.9   12 Apr 1997 14:42:12   andy
 * 
 * add query fns
 * 
 *    Rev 1.8   05 Apr 1997 15:48:48   srdjan
 * nav & dop moding
 * 
 *    Rev 1.6   03 Apr 1997 10:44:34   andy
 * add menu items for SetUser1Protocol and SetSerialPort
 * 
 *    Rev 1.5   14 Dec 1996 14:05:48   glen
 * Move log and map file control to mainfram and map views.
 * 
 *    Rev 1.4   02 Dec 1996 14:09:40   glen
 * Add NMEA capability.
 * 
 *    Rev 1.3   19 Nov 1996 14:38:58   glen
 * Chanfe return type of StopTimer, for convenience.
 * 
 *    Rev 1.2   12 Nov 1996 13:44:54   glen
 * Move user interface stuff around.
 * 
 *    Rev 1.1   07 Oct 1996 17:59:30   glen
 * Add rest button procs.
 * 
 *    Rev 1.0   18 Sep 1996 17:39:36   glen
 * Initial revision.
 * 
 ***************************************************************************
 *
 * NOTES:
 *
 *
 *
 * TYPES:
 *
 *
 *
 * DATA:
 *
 *
 *
 * FUNCTIONS:
 *
 *
 *
 * CLASS EQUIVALENCE:
 *
 *
 *
 ***************************************************************************/

// Author: Steven Mark

#ifndef __MAINFRM_H__
#define __MAINFRM_H__

#include "feeddoc.h"
#include "WinSiRFDemo.h"

#include "timercli.h"
# include "HWndReceiver.h"

class CMainFrame : public CMDIFrameWnd
{                 
   
   // used for calls to ShowView, to force the view to be active/top
   const enum {VIEW_AUTO = 0,VIEW_ON};
   
   friend class CWinSiRFDemoApp;

	public: // create from serialization only
		CStatusBar m_wndStatusBar;

		CMainFrame();
		virtual ~CMainFrame();            
		
		UINT StartTimer(UINT nMsg, CTimerClient* pClient, int nDelay);
		UINT StopTimer(int nTimer);

		void SaveOpenViews();
		void OpenConfiguration(CString configFile);

		CHWndReceiver *GetSiRFMessageReceiver() {
			return m_pHWndRcvr;
		}

		DECLARE_DYNCREATE(CMainFrame)
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL
	protected:
		void CanKillDoc();
		void OpenDoc();
		
      void ShowView(int nView, UINT mode=VIEW_AUTO);
		void UpdateViewControls(int nView, CCmdUI* pCmdUI, BOOL bEnable=TRUE);
		LONG CloseView(int nView);

		int	 m_nSrc;
		char m_szOpenViews[100];

		const enum {MAX_TIMERS=5};
		struct
		{
			UINT		m_nTimerID;
			UINT		m_nMsg;
			CTimerClient *m_pClient;
		} m_aTimer[MAX_TIMERS];

		CPtrList m_apComm;
		
		CToolBar m_wndToolBar;
		BOOL m_bSrcOpen;
		BOOL m_bAppExitting;
		BOOL m_bLogOpen;
		BOOL m_bPauseDisplay;
		CFeedDoc* m_pdoc;
		CFrameWnd*	m_apwndView[CWinSiRFDemoApp::VIEW_COUNT];
		BOOL m_bViewMapEnable;
		
		#ifdef _DEBUG
			virtual void AssertValid() const;
			virtual void Dump(CDumpContext& dc) const;
		#endif

		inline BOOL CMainFrame::GotSource() {return m_pdoc != NULL;};
		BOOL CMainFrame::GotView();


		CHWndReceiver *m_pHWndRcvr;		// Reciver for SirfProtocol messages

		//{{AFX_MSG(CMainFrame)
		afx_msg void OnTimer(UINT nIDEvent);
		afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
		afx_msg LONG OnUpdateOrientation(UINT nOrientation, LONG lFoo);
		afx_msg LONG OnMeasClosed(UINT nOrientation, LONG lFoo);
		afx_msg LONG OnDataReceived(UINT nFoo, LONG lFoo);
		afx_msg void OnFileOpen();
		afx_msg void OnViewSignal();
		afx_msg void OnUpdateViewSignal(CCmdUI* pCmdUI);
		afx_msg LONG OnViewSignalClose(UINT nFoo, LONG lFoo);
		afx_msg void OnViewSignal2();
		afx_msg void OnUpdateViewSignal2(CCmdUI* pCmdUI);
		afx_msg LONG OnViewSignalClose2(UINT nFoo, LONG lFoo);
		afx_msg void OnViewRadar();
		afx_msg void OnUpdateViewRadar(CCmdUI* pCmdUI);
		afx_msg LONG OnViewRadarClose(UINT nFoo, LONG lFoo);
		afx_msg void OnViewMap();
		afx_msg void OnUpdateViewMap(CCmdUI* pCmdUI);
		afx_msg LONG OnViewMapClose(UINT nFoo, LONG lFoo);
		afx_msg void OnViewDebugDataWindow();
		afx_msg void OnUpdateViewDebugDataWindow(CCmdUI* pCmdUI);
		afx_msg LONG OnViewDebugDataWindowClose(UINT nFoo, LONG lFoo);
		afx_msg void OnViewNavMsg();
		afx_msg void OnUpdateViewNavMsg(CCmdUI* pCmdUI);
		afx_msg LONG OnViewNavMsgClose(UINT nFoo, LONG lFoo);
		afx_msg void OnUpdateWindowCloseAll(CCmdUI* pCmdUI);
		afx_msg void OnWindowCloseAll();
      afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
		afx_msg void OnFilePreferences();
		afx_msg LONG OnDocClose(UINT, LONG);
		afx_msg void OnDestroy();
		afx_msg void OnActionInitializeDataSource();
		afx_msg void OnUpdateActionInitializeDataSource(CCmdUI* pCmdUI);
		afx_msg void OnActionSendNMEAOn();
		afx_msg void OnActionSendNMEAOff();
		afx_msg void OnActionSendNMEAEnable();
		afx_msg void OnActionSendNMEADisable();
		afx_msg void OnActionOpenDataSource();
		afx_msg void OnUpdateActionOpenDataSource(CCmdUI* pCmdUI);
		afx_msg void OnActionOpenLogFile();
		afx_msg void OnUpdateActionOpenLogFile(CCmdUI* pCmdUI);
		afx_msg void OnActionPauseDisplay();
		afx_msg void OnUpdateActionPauseDisplay(CCmdUI* pCmdUI);
      afx_msg void OnActionSetSerialPort();
      afx_msg void OnActionSwitchtoUser1Protocol();
	afx_msg void OnNavigationModeControl();
	afx_msg void OnUpdateActionSetSerialPort(CCmdUI* pCmdUI);
	afx_msg void OnUpdateActionNmeaDisable(CCmdUI* pCmdUI);
	afx_msg void OnUpdateActionNmeaEnable(CCmdUI* pCmdUI);
	afx_msg void OnUpdateActionSwitchToUser1Protocol(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNavigationModeControl(CCmdUI* pCmdUI);
	afx_msg void OnNavigationDopControl();
	afx_msg void OnUpdateNavigationDopControl(CCmdUI* pCmdUI);
   afx_msg void OnUpdatePollSWVersion(CCmdUI* pCmdUI);
   afx_msg void OnPollSWVersion();
   afx_msg void OnUpdatePollClockStatus(CCmdUI* pCmdUI);
   afx_msg void OnPollClockStatus();
	afx_msg void OnNavigationDGPSControl();
	afx_msg void OnUpdateNavigationDGPSControl(CCmdUI* pCmdUI);
	afx_msg void OnNavigationElevationMask();
	afx_msg void OnUpdateNavigationElevationMask(CCmdUI* pCmdUI);
	afx_msg void OnNavigationPowerMask();
	afx_msg void OnUpdateNavigationPowerMask(CCmdUI* pCmdUI);
	afx_msg void OnNavigationEditingResidual();
	afx_msg void OnUpdateNavigationEditingResidual(CCmdUI* pCmdUI);
	afx_msg void OnNavigationSteadyStateDetection();
	afx_msg void OnUpdateNavigationSteadyStateDetection(CCmdUI* pCmdUI);
	afx_msg void OnNavigationStaticNavigation();
	afx_msg void OnUpdateNavigationStaticNavigation(CCmdUI* pCmdUI);
	afx_msg void OnViewMessagesError();
	afx_msg void OnUpdateViewMessagesError(CCmdUI* pCmdUI);
   afx_msg LONG OnViewErrorClose(UINT nFoo, LONG lFoo);
	afx_msg void OnViewMessagesQueryResponse();
	afx_msg void OnUpdateViewMessagesQueryResponse(CCmdUI* pCmdUI);
   afx_msg LONG OnViewQueryClose(UINT nFoo, LONG lFoo);
	afx_msg void OnActionSetDGPSPort();
	afx_msg void OnUpdateActionSetDGPSPort(CCmdUI* pCmdUI);
	afx_msg void OnActionSwitchOpMode();
	afx_msg void OnUpdateActionSwitchOpMode(CCmdUI* pCmdUI);
	afx_msg void OnActionSetAlmanac();
	afx_msg void OnUpdateActionSetAlmanac(CCmdUI* pCmdUI);
   afx_msg void OnPollAlmanac();
   afx_msg void OnUpdatePollAlmanac(CCmdUI* pCmdUI);
	afx_msg void OnUpdateActionSetEphemeris(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePollEphemeris(CCmdUI* pCmdUI);
	afx_msg void OnPollEphemeris();
	afx_msg void OnActionSetEphemeris();
   afx_msg LONG OnStoreEphemeris(UINT nOrientation, LONG lFoo);
   afx_msg LONG OnStoreAlmanac(UINT nOrientation, LONG lFoo);
	afx_msg void OnClose();
	//}}AFX_MSG
	afx_msg LRESULT OnSiRFMessage(WPARAM, LPARAM);
		DECLARE_MESSAGE_MAP()
    void HandleMessage(CMessage *pMsg);

	BOOL m_bDontSaveWindowSettings;
	BOOL GetConfigParameters(CString configFile);
};

/////////////////////////////////////////////////////////////////////////////

inline void SetStatusBarText(const CString& str) {((CMainFrame*)AfxGetMainWnd())->m_wndStatusBar.SetWindowText(str);};

//
// Stuff moved here from obsolete srcmeas.cpp
#define FILE_OPEN   TRUE 
#define FILE_SAVE   FALSE
extern CString GetAlmanacFileBrowse(UINT load=FILE_OPEN);
extern CString GetEphemerisFileBrowse(UINT load=FILE_OPEN);

#endif
