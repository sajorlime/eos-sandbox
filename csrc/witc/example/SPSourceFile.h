
/***************************************************************************
 *
 *                          SiRF Technology, Inc. GPS Software
 *                                      Version 1.05
 *
 *      SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source
 *      is the sole property of SiRF Technology, Inc.  Reproduction or
 *      utilization of this source in whole or in part is forbidden
 *      without the written consent of SiRF Technology, Inc.
 *
 ***************************************************************************
 *
 *              (c) SiRF Technology, Inc. 1998 -- All Rights Reserved
 *
 ***************************************************************************
 *
 * SPSourceFile.h
 *
 * .........................................................................
 *
 * $Workfile:$
 * $Revision:$
 *     $Date:$
 *
 * .........................................................................
 *
 * Defines SiRFProtocol Source File class for binary file.
 *
 * .........................................................................
 *
 *      $Log:$
 *
 *  Date     Description
 * ------   -----------
 *
 ***************************************************************************
 *
 * NOTES:
 *
 *
 * Classes/Types:
 *
 *
 * FUNCTIONS:
 *
 *
 ***************************************************************************/


# ifndef __SPSOURCEFILE_H__
# define __SPSOURCEFILE_H__

# include "SPSource.h"
# include <afx.h>
# include <afxmt.h>


class CSPWriteThread : public CThread
{
private:
   // take the function private so that the new start must be called.
   bool start (void) {return false;}

protected:
    HANDLE m_hDestination;
    DWORD m_msSleep;
    BYTE * m_pData;
    int m_nData;
    bool m_bContinuous;
    int m_id;
    
    int thread (void);

public:
    bool setContinuous (bool c) {return m_bContinuous = c;}

    CSPWriteThread (
                    HANDLE hDest,
                    DWORD msSleep = 1000,
                    int id = -1,
                    bool bContinuous = false
                   );
    virtual ~CSPWriteThread ();
    virtual bool terminate (void);
    
    void setDestination (HANDLE hDest);

    
    bool start (BYTE * pData, int len);
};


/*
    CSPSourceFile is a class that implements a SiRF Protocol Communication
    from a data file.
    The inheriting class needs to construct the class and setup

*/
class CSPSourceFile : public CSPSource
{
private:
    CSPWriteThread * m_pWriter;

protected:
    bool m_bContinuous;
    int m_nData;
    BYTE * m_pData;
    DWORD m_msSleep;
    int m_id;
    CMessageReceiver * m_pMP;

public:
    /*
        suspend both threads
    */
    virtual bool suspend (void);

    /*
        resume both threads
    */
    virtual bool resume (void);

    /*
        clean up and terminate threads.
    */
    virtual bool terminate (void);

    /*
        start the the source thread and the writer thread if
        the data is ready.
    */
    virtual bool start (void);

    /*
        startWriter starts the associasted writer thread.
        The thread is started if the source thread is running.
        If it is not started it will be started when start is 
        called.
    */
    virtual bool startWriter (BYTE * pData, int nData);

    CSPWriteThread * getWriter (void) {return m_pWriter;}

    bool setContinuous (bool c) {return m_pWriter->setContinuous (c);}

    CSPSourceFile (CMessageReceiver * mr,
                   HANDLE hSource,
                   HANDLE hDestination,
                   DWORD ms = 1000,
                   bool continuous = false,
                   int waitId = -1
                  );

    virtual ~CSPSourceFile ();
};

# endif