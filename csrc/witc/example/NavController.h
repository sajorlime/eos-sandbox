/***************************************************************************
 *																									*
 *							SiRF Technology, Inc. GPS Software							*
 *																									*
 *		SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source		*
 *		is the sole property of SiRF Technology, Inc.  Reproduction or			*
 *		utilization of this source in whole or in part is forbidden				*
 *		without the written consent of SiRF Technology, Inc.						*
 *																									*
 ***************************************************************************
 *																									*
 *				(c) SiRF Technology, Inc. 1996 -- All Rights Reserved				*
 *																									*
 ***************************************************************************
 *
 * NAVCONTROLLER.H
 *
 * .........................................................................
 *
 * .........................................................................
 *
 *
 *
 * .........................................................................
 *
 * 
 ***************************************************************************
 *
 * NOTES:
 * This class maintains instances of navigation, ioPort interfaces
 * The class is implemented as a singleton
 *
 *
 * TYPES:
 *
 *
 *
 * DATA:
 *
 *
 *
 * FUNCTIONS:
 *
 *
 *
 * CLASS EQUIVALENCE:
 *
 *
 *
 ***************************************************************************/

// Author: Prakash Manghnani

#ifndef __NAVCONTROLLER_H__
#define __NAVCONTROLLER_H__

#include "preferen.h"
#include "IGPSNavigation.h"
#include "logger.h"

class CNavController
{
public:
	static CNavController &get()		// Get the single instance
	{
		if (m_pInstance == NULL) {
			m_pInstance = new CNavController();
		}
		return *m_pInstance;
	}

	EProtocol	initNavigation();		// Initialize the navigation interfaces
	EProtocol terminateNavigation(); 
	IIOProtocol *getIOProtocol() { return m_pGPSProtocol; }
	INavigation *getNavigation() { return m_pNavigation; }

	// Logging stuff
	CLoggerAccess *GetLogger() { return m_pLoggerAccess; }
	BOOL IsLoggingOn() { return m_pLoggerAccess->isLoggingOn(); }
	void StartLogging();
	void StopLogging();
	BOOL ShowLoggingSettings();
	CString &GetStrLogTime();
	CString &GetStrLogDate();
	void LogIncomingMessage(CNavData *pNavData);

	void ReceiverInit();
	void SendNMEA(int iMode);
	void SetSerialPort(void);
	void SetDGPSPort(void);
	void SwitchOpMode(void);
	void SetProtocolUser1(void);
      
	void SetNavMode(void);
	void SetDOP(void);
	void SetDGPS(void);
	void SetElevationMask(void);
	void SetPowerMask(void);
     
	void SetEditingResidual(void); 
	void SetSteadyStateDetection(void);
	void SetStaticNavigation(void);
	void SetAlmanac(CString strAlmanac);
	void SetEphemeris(CString strEphemeris);
	
	// Poll fns
	void PollAlmanac(void);
	void PollEphemeris(void);
	void PollSWVersion(void);
	void PollClockStatus(void);

	void OnUpdatePrefs();
private:

	IIOProtocol		*m_pGPSProtocol;
	IIOProtocol		*m_pDGPSProtocol;
	INavigation		*m_pNavigation;
	CMessageQueue	*m_pNavQueue;
	CLoggerAccess	*m_pLoggerAccess;
	CString			m_strLogTime;
	CString			m_strLogDate;
	BOOL			IsLoggableMessage(CNavData *pNavData);

	CNavController();
	virtual ~CNavController();

	static CNavController *m_pInstance;	// Single instance
};

#endif __NAVCONTROLLER_H__
