
/***************************************************************************
 *
 *                          SiRF Technology, Inc. GPS Software
 *                                      Version 1.05
 *
 *      SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This hSource
 *      is the sole property of SiRF Technology, Inc.  Reproduction or
 *      utilization of this hSource in whole or in part is forbidden
 *      without the written consent of SiRF Technology, Inc.
 *
 ***************************************************************************
 *
 *              (c) SiRF Technology, Inc. 1998 -- All Rights Reserved
 *
 ***************************************************************************
 *
 * SPSource.h
 *
 * .........................................................................
 *
 * $Workfile:$
 * $Revision:$
 *     $Date:$
 *
 * .........................................................................
 *
 * Defines SiRFProtocol Source class.
 *
 * .........................................................................
 *
 *      $Log:$
 *
 *  Date     Description
 * ------   -----------
 *
 ***************************************************************************
 *
 * NOTES:
 *
 *
 * Classes/Types:
 *
 *
 * FUNCTIONS:
 *
 *
 ***************************************************************************/


# ifndef __SPSOURCE_H__
# define __SPSOURCE_H__

# include "ProtocolSource.h"


/*
    CSPSource is a class that implement a SiRF Protocol Communication
    port reading thread.
*/
class CSPSource : public CProtocolSource
{
protected:
    // thread is called by xThread from CThread
    virtual int thread (void);
    virtual bool terminate (void) {return CProtocolSource::terminate ();}

public:
    CSPSource (CMessageReceiver * mr, HANDLE hSource);
    virtual ~CSPSource () {}

};



# endif