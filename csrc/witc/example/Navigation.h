
/***************************************************************************
 *
 *                          SiRF Technology, Inc. GPS Software
 *                                      Version 1.05
 *
 *      SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source
 *      is the sole property of SiRF Technology, Inc.  Reproduction or
 *      utilization of this source in whole or in part is forbidden
 *      without the written consent of SiRF Technology, Inc.
 *
 ***************************************************************************
 *
 *              (c) SiRF Technology, Inc. 1998 -- All Rights Reserved
 *              Developed by Lapel Software, Inc.
 *
 ***************************************************************************
 *
 * Navigation.h
 *
 * .........................................................................
 *
 * $Workfile:$
 * $Revision:$
 *     $Date:$
 *
 * .........................................................................
 *
    Defines the Navigation class. The navigation class inherits
    Theis file defines the CNavigaiton class.  This class exists to
    implement some common required functions into a single class.  To
    does take the class from abstract to actual, but no instances of
    CNavigation exist.
 *
 * .........................................................................
 *
 *      $Log:$
 *
 *  Date     Description
 * ------   -----------
 *
 ***************************************************************************
 *
 * Notes:
    Please see IGPSNavigaiton and CThread for a definition the
    interfaces repeated here.  The most import thing that the
    CNavigation class does is bring in the concept of thread to all of
    the INavigation instances that we develop.
    IGPSNavigation is found in this directory in the file
    IGPSNavigaiton, and CThread if found in ../Utility/Thread.h.
    The other import thing that CNavigation does is marry our
    implementation with the structures in sp_icd.h. (note in the
    future it would be resonable to chage this to CNavData.)
-
    So at this point in the class hierarchy we have at least four
    vital concepts.  These are the CMessageReciever, CThread,
    IIOProtocol, and INavigation classes.  Also an honerable mention
    should go to IIOPort, and ITracker -- not the mention the
    concrete strcutures for carrying information.
 *
 * Classes/Types:
    CNavigation -- A class that marries the CThread class and the
    INavigation class. This class remains abstract, with the
    thread method and two newe methods for handling message
    being abstract.
    The new methods processDataMsg, and processControlMsg, are
    critical to implementing the rest of the hierarchy and
    being able to implement the "smart" behavior of the 
    CSmartNavigation class.
-
    CNavigation::EControlMessage -- defines a mapping between the
    interfaces calls and CMessge messages (see Utility/Message.h) used
    to deliver data from the application to the running thread. 
-
    CTracker -- just a concreate tracker object.  No further
    refinement of this class is required.
 *
 *
 *
 ***************************************************************************/


# ifndef __NAVIGATION_H__
# define __NAVIGATION_H__

# include "IGPSNavigation.h"
# include "sp_icd.h"
# include "Thread.h"
 
class CNavigation : public INavigation, public CMessageDispatch, public CThread
{
protected:
    enum EControlMessage
    {
        ecm_None = 0,
        ecm_NavInitialization,
        ecm_SetAlmanac,
        ecm_SetEphemeris,
        ecm_DifferentialCorrection,
        ecm_DOPConfiguration,
        ecm_RTCMCorrection,
        ecm_NavMode,
        ecm_DGPSConfiguration,
        ecm_ElevationMask,
        ecm_PowerMask,
        ecm_EditingResidual,
        ecm_SteadyStateDetector,
        ecm_StaticNavigation,
        ecm_OperationMode,
        ecm_DirectReckoning,
        ecm_ReportClockStatus,
        ecm_ReportAlmanac,
        ecm_ReportEphemeris,
        ecm_ReportVersion,
        Last_EControlMessage
    };

    IIOProtocol * m_pGPSProtocol;
    IIOProtocol * m_pDGPSProtocol;

    bool m_sendMessages;
    CMessageReceiver * m_pSendTo;

    CMessageQueue * m_pNavMessages;

    ITracker * m_pTracker;

    static SNavInitialization m_init;

    bool sendControl (EControlMessage m, int size, const BYTE * pData);
    
    CNavigation (void);

    // CThread methods
    virtual int thread (void) = 0; // abstract so it needs to be defined
    virtual bool terminate (DWORD ms) {return CThread::terminate (ms);}
    virtual bool suspend (void) {return CThread::suspend ();}
    virtual bool resume (void) {return CThread::resume ();}
    virtual bool sleep (DWORD msSleep) {return CThread::sleep ();}
    virtual bool wake (void) {return CThread::wake ();}


    /*
        sendMessageToApp -- used to send messages up to applicaitons.
        In the future [maybe] this should be generalzied to 
        send messages to any number of connected apps.
    */
    virtual bool sendMessageToApp (CMessage * pMsg)
    {
        if (m_sendMessages == false)
        {
            m_pNavMessages->release (pMsg);
            return false;
        }
        return m_pSendTo->send (pMsg);
    }

    // CMessageDispatch methods
    virtual bool processControlMsg (CMessage * pMsg) = 0;
    virtual bool processDataMsg (CMessage * pMsg) = 0;
    virtual bool processDataContinuationMsg (CMessage * pMsg)
    {
        return processDataMsg (pMsg);
    }
    virtual bool processCriticalDataMsg (CMessage * pMsg)
    {
        return processDataMsg (pMsg);
    }
    virtual bool processDebugMsg (CMessage * pMsg)
    {
        return sendMessageToApp (pMsg);
    }
    virtual bool processWarnMsg (CMessage * pMsg)
    {
        return sendMessageToApp (pMsg);
    }
    virtual bool processErrorMsg (CMessage * pMsg)
    {
        return sendMessageToApp (pMsg);
    }
    virtual bool processFatalMsg (CMessage * pMsg)
    {
        sendMessageToApp (pMsg);
        return false;
    }
    virtual bool processTerminateMsg (CMessage * pMsg)
    {
        m_pNavMessages->release (pMsg);
        return false;
    }
    virtual bool dispatchMsg (CMessage * pMsg)
    {
        return CMessageDispatch::dispatchMsg (pMsg);
    }

public:
    static void
        setNavInitializationDefault (const SNavInitialization * pDefault);
    virtual EProtocol initialize (CMessageReceiver * mr,
                            CMessageQueue * mq,
                            IIOPort * pGPSPort,
                            IIOPort * pDGPSPort = 0,
                            const SNavInitialization * config = 0 
                           );
    

    virtual EProtocol initialize (const SNavInitialization * config);
    

    virtual EProtocol terminate (void);



    // navigation operators
    virtual EProtocol setAlmanac (const SSetAlmanacMsg * almanac);

    virtual EProtocol setEphemeris (const SSetEphemerisMsg * ephemeris);
    
    virtual EProtocol setRTCMCorrection (const SDifferentialCorrectionMsg * corr);

    virtual EProtocol setNavMode (const SNavigationModeMsg * mode);

    virtual EProtocol setDilutionOfPrecisionMode (const SDilutionOfPrecisionMsg * config);
    
    virtual EProtocol setDGPSMode (const SSetDGPSModeMsg * config);
    
    virtual EProtocol setElevationMask (const SSetElevationMaskMsg * mask);
    
    virtual EProtocol setPowerMask (const SSetPowerMaskMsg * mask);
    
    virtual EProtocol setEditingResidual (const SEditResidualThresholdMsg * res);

    virtual EProtocol setSteadyStateDetector (const SSetSteadyStateDetectorMsg * det);

    virtual EProtocol setStaticNavigation (const SSetStaticNavigationMsg * staticNav);

    virtual EProtocol setOperationMode (const SSwitchOpModeMsg * pOpMode);


    virtual EProtocol directReckoning (const SDirectReckoning * pDR);


    virtual EProtocol reportClockStatus (void);

    virtual EProtocol reportAlmanac (void);

    virtual EProtocol reportEphemeris (int svid);

    virtual EProtocol reportVersion (void);
    

    // message delivery control.
    virtual void disableMessages (void);
    

    virtual void enableMessages (void);
    

    virtual ~CNavigation ();
    

private:

};


class CTracker : public ITracker
{
    const char * m_param;
    IIOProtocol * m_pProtocol;

    virtual EProtocol initialize (CMessageReceiver * mr,
                                    IIOPort * pPort
                                   );
    
    virtual EProtocol terminate (void);
    
    virtual EProtocol initialAcquisition (const SInitialAcquisition * iacq);
    
    virtual EProtocol
        reacquireReceiverChannel (int channel,
                                  const SReacquisition * reacq
                                 );
    
    virtual EProtocol terminateChannel (int channel);
    
protected:
    CTracker (void);

    virtual ~CTracker ()
    {
    }

private:
    friend struct ITrackerFactory;
    friend class CTrackerFactory;
};

class CTrackerFactory : public ITrackerFactory
{
public:
    ITracker * trackerFactory (const char * param);
};


# endif

