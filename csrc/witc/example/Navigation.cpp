
/***************************************************************************
 *
 *                          SiRF Technology, Inc. GPS Software
 *                                      Version 1.05
 *
 *      SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source
 *      is the sole property of SiRF Technology, Inc.  Reproduction or
 *      utilization of this source in whole or in part is forbidden
 *      without the written consent of SiRF Technology, Inc.
 *
 ***************************************************************************
 *
 *              (c) SiRF Technology, Inc. 1998 -- All Rights Reserved
 *              Developed by Lapel Software, Inc.
 *
 ***************************************************************************
 *
 * Navigation.cpp
 *
 * .........................................................................
 *
 * $Workfile:$
 * $Revision:$
 *     $Date:$
 *
 * .........................................................................
 *
    Defines the Navigation class.
 *
 * .........................................................................
 *
 *      $Log:$
 *
 *  Date     Description
 * ------   -----------
 *
 ***************************************************************************
 *
 * Notes:
    The CNavigation class is the base class of a family of navigation
    classes the implement the INavigation interface. CNavigation tries
    to implement just those parts of the interface that are unlikely
    to change in the rest of the SiRF implementation.
-
    The CTracker class implemented here is the only ITracker class 
    required by the current SiRF implementation plans.  Only a PC
    based tracker implementation should require an additional tracker
    classes.
-
 *
 * Data:
    CNavigation::m_init -- this is the static member of CNavigation.  If
    its values has been set it is used to intializize the navigaiton
    computation code.
 *
 *
 * Functions:
    CNavigation::setNavInitializationDefault -- is a static function
    that allows an external entity (e.g.  the raw files processor) to
    initalize the state of the navigation on initialization.
-
    CNavigation::sendControl -- is the method used by navigation code
    to send mesages to the navigaiton thread.
-
    CNavigation::CNavigation -- constructor for this class.
-
    CNavigation::initialize -- the primary initializer for this class
    after contruction time.  This function must be called in use the
    class.
-
    -
        Other than the primary initializer all application level
        interaces return a not implemented error.
        I worthwhile optimization would be to move the ModuleNavigation
        implementation of these methods here, removing them from 
        ModuleNavigation.
    -
    CNavigation::initialize -- the secondary intializer that sends the
    navigation initialization to the navigation calculation.
-
    CNavigation::terminate -- terminates/undoes the primary initialize
    operation.
-
    CNavigation::setAlmanac -- application level interface.
-
    CNavigation::setEphemeris -- application level interface.
-
    CNavigation::setRTCMCorrection -- application level interface.
-
    CNavigation::setNavMode -- application level interface.
-
    CNavigation::setDilutionOfPrecisionMode -- application level interface.
-
    CNavigation::setDGPSMode -- application level interface.
-
    CNavigation::setElevationMask -- application level interface.
-
    CNavigation::setPowerMask -- application level interface.
-
    CNavigation::setEditingResidual -- application level interface.
-
    CNavigation::setSteadyStateDetector -- application level interface.
-
    CNavigation::setStaticNavigation -- application level interface.
-
    CNavigation::setOperationMode -- application level interface. 
-
    CNavigation::directReckoning --  application level interface. 
-
    CNavigation::reportClockStatus -- application level interface. 
-
    CNavigation::reportAlmanac -- application level interface. 
-
    CNavigation::reportEphemeris -- application level interface. 
-
    CNavigation::reportVersion -- application level interface. 
-
    CNavigation::disableMessages -- used to disable messages being sent
    to the MessageReceiver passed in the initailization method.
-
    CNavigation::enableMessages -- used to enble messages being sent
    to the MessageReceiver passed in the initailization method. This
    only has meaning after the disableMessgae methdod has been called.
-
    CNavigation::~CNavigation --  the class destructor.
-
    CTracker::initialize -- intializes the tracker interface.
-
    CTracker::terminate -- termiantes/undoes the tracker interface.
-
    CTracker::initialAcquisition -- causes a initial acquistion
    message to be sent to the tracker.
-
    CTracker::reacquireReceiverChannel -- causes a reacquire rcv
    message to be sent to the tracker.
-
    CTracker::terminateChannel -- causes a terminate channel message to be
    sent to the tracker.
-
    CTracker::CTracker -- is the constructor for the tacker class.
-
    ITrackerFactory::trackerFactory -- tacker factory interface.
-
    CTrackerFactory::trackerFactory -- tacker factory interface.
-
 *
 *
 ***************************************************************************/

# include "Navigation.h"
# include "NavData.h"



SNavInitialization CNavigation::m_init
=
{
    0 // filled by external calls.
};

void CNavigation::setNavInitializationDefault (const SNavInitialization * pDefault)
{
    m_init = *pDefault;
}

bool CNavigation::sendControl (EControlMessage m, int size, const BYTE * pData)
{
    return
        m_pNavMessages->CMessageReceiver::send (EMT_Control,
                              static_cast<int> (m),
                              size,
                              pData
                             );
}

/*
    CNavigation::CNavigation -- takes care of the thread part
    of the implemenation, and relies on the initialize call
    for the rest of the initialization, though everything else
    is set to null, to ensure proper error detection.
*/
CNavigation::CNavigation (void)
    :
      CThread (),
      m_pTracker (0),
      m_pGPSProtocol (0),
      m_pDGPSProtocol (0),
      m_pSendTo (0),
      m_sendMessages (true),
      m_pNavMessages (0)
{
}

/*
    CNavigation::initialize -- is the primary initialization
    method for this class, it is expected (required in the
    current SiRF chain) that each inheriting class will call
    this method if they override this method.

    This method ensures that each of the parameters here 
    sets the corresponding internal members. It also ensures
    that each of these parameters is legal.

    The config parameter is saved for later use by opearations
    at the thread level.
*/
EProtocol CNavigation::initialize (CMessageReceiver * pMR,
                                     CMessageQueue * pMQ,
                                     IIOPort * pGPSPort,
                                     IIOPort * pDGPSPort,
                                     const SNavInitialization * config
                                    )
{
    m_pGPSProtocol = static_cast<IIOProtocol *> (pGPSPort);
    TRACE ("m_pGPSProtocol : %x\n", m_pGPSProtocol);
    m_pDGPSProtocol = static_cast<IIOProtocol *> (pDGPSPort);
    TRACE ("m_pDGPSProtocol : %x\n", m_pDGPSProtocol);
    if (config)
        m_init = *config;
    m_pSendTo = pMR;
    m_pSendTo->attach ();
    TRACE ("m_pSendTo : %x\n", m_pSendTo);
    m_pNavMessages = pMQ;
    m_pNavMessages->attach ();
    TRACE ("m_pNavMessages : %x\n", m_pNavMessages);
    if (!m_pGPSProtocol || !m_pSendTo || !m_pNavMessages)
        return EP_InitializationFailed;
    if (m_pDGPSProtocol)
    {
        EProtocol e
            = m_pDGPSProtocol->initialize
                                (pMR, pDGPSPort->defaultPort ());
        if (e != EP_Success)
            TRACE ("DGPS initialize failed\n");
    }
    return EP_Success;
}

EProtocol CNavigation::initialize (const SNavInitialization * config)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::terminate (void)
{
    return EP_NotImplemented;
}

// navigation operaitors
EProtocol CNavigation::setAlmanac (const SSetAlmanacMsg * almanac)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::setEphemeris (const SSetEphemerisMsg * ephemeris)
{
    return EP_NotImplemented;
}


EProtocol CNavigation::setRTCMCorrection (const SDifferentialCorrectionMsg * corr)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::setNavMode (const SNavigationModeMsg * mode)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::setDilutionOfPrecisionMode (const SDilutionOfPrecisionMsg * config)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::setDGPSMode (const SSetDGPSModeMsg * config)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::setElevationMask (const SSetElevationMaskMsg * mask)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::setPowerMask (const SSetPowerMaskMsg * mask)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::setEditingResidual (const SEditResidualThresholdMsg * res)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::setSteadyStateDetector (const SSetSteadyStateDetectorMsg * det)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::setStaticNavigation (const SSetStaticNavigationMsg * staticNav)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::setOperationMode (const SSwitchOpModeMsg * pOpMode)
{
    return EP_NotImplemented;
}


EProtocol CNavigation::directReckoning (const SDirectReckoning * pDR)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::reportClockStatus (void)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::reportAlmanac (void)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::reportEphemeris (int svid)
{
    return EP_NotImplemented;
}

EProtocol CNavigation::reportVersion (void)
{
    return EP_NotImplemented;
}

/*
    CNavigation::disableMessages -- disable by
    disabling protocols.
*/
void CNavigation::disableMessages (void)
{
# if 1  
    if (m_pGPSProtocol)
        m_pGPSProtocol->disable ();
    if (m_pDGPSProtocol)
        m_pDGPSProtocol->disable ();
# endif
   m_sendMessages = false;
}

/*
    CNavigation::enableMessages -- enable by
    enabling protocols.
*/
void CNavigation::enableMessages (void)
{
# if 1  
    if (m_pGPSProtocol)
        m_pGPSProtocol->enable ();
    if (m_pDGPSProtocol)
        m_pDGPSProtocol->enable ();
# endif    
   m_sendMessages = true;
}

CNavigation::~CNavigation ()
{
}





EProtocol CTracker::initialize (CMessageReceiver * mr,
                                IIOPort * pPort
                               )
{
    m_pProtocol = static_cast<IIOProtocol *> (pPort);
    return m_pProtocol->initialize (mr, pPort->defaultPort ());
}

EProtocol CTracker::terminate (void)
{
    return m_pProtocol->terminate ();
}

EProtocol CTracker::initialAcquisition (const SInitialAcquisition * iacq)
{
   CInitialAcquisition initAcq (iacq);
   unsigned char acBuf[sizeof (CInitialAcquisition)
                        + sizeof (struct NAVtoTrkStruct)];

   initAcq.Export(acBuf, sizeof(acBuf));
   return m_pProtocol->sendMessage(acBuf, initAcq.GetPacketSize());
}

EProtocol CTracker::reacquireReceiverChannel (int channel,
                                        const SReacquisition * reacq
                                       ) 
{
   CReacquisition reAcq (reacq);
   unsigned char acBuf[sizeof (CReacquisition)
                        + sizeof (struct NAVtoTrkStruct)];

   reAcq.Export(acBuf, sizeof(acBuf));
   return m_pProtocol->sendMessage(acBuf, reAcq.GetPacketSize());
}

EProtocol CTracker::terminateChannel (int channel) 
{
   CKillChannel termCh (channel);
   unsigned char acBuf[sizeof (termCh)];

   termCh.Export(acBuf, sizeof(acBuf));
   return m_pProtocol->sendMessage(acBuf, termCh.GetPacketSize());
}


CTracker::CTracker (void)
    : m_pProtocol (0), m_param (0)
{
}



ITracker * ITrackerFactory::trackerFactory (const char * pParm)
{
    CTracker * pTT = new CTracker ();
    pTT->m_param = pParm;
    return pTT;
}




ITracker * CTrackerFactory::trackerFactory (const char * pParm)
{
    return ITrackerFactory::trackerFactory (pParm);
}





