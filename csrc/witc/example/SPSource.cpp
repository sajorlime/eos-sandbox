
/***************************************************************************
 *
 *                          SiRF Technology, Inc. GPS Software
 *                                      Version 1.05
 *
 *      SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This hSource
 *      is the sole property of SiRF Technology, Inc.  Reproduction or
 *      utilization of this hSource in whole or in part is forbidden
 *      without the written consent of SiRF Technology, Inc.
 *
 ***************************************************************************
 *
 *              (c) SiRF Technology, Inc. 1998 -- All Rights Reserved
 *
 ***************************************************************************
 *
 * SPSource.cpp
 *
 * .........................................................................
 *
 * $Workfile:$
 * $Revision:$
 *     $Date:$
 *
 * .........................................................................
 *
 * Implements SiRFProtocol read thread class.
 *
 * .........................................................................
 *
 *      $Log:$
 *
 *  Date     Description
 * ------   -----------
 *
 ***************************************************************************
 *
 * NOTES:
 *
 *
 * Classes/Types:
 *
 *
 * FUNCTIONS:
 *
 *
 ***************************************************************************/


# include <afxmt.h>
//# include <windef.h>
# include <string.h>

# include "SPSource.h"
# include "SPFrame.h"
# include "IProtocol.h"
# include "ProtocolComm.h"



static inline void wrapAndPostMessage (CMessageReceiver * pMR,
                                EMessageType valid,
                                BYTE * pBuf,
                                int count
                               )
{
    pMR->send (valid, *pBuf, count, pBuf);
}


CSPSource::CSPSource (CMessageReceiver * pMR, HANDLE hSource)
    :
      CProtocolSource (pMR, hSource)
{
}


//extern "C" int UI_Printf (const char *, ...);

static int spRead (HANDLE hSource, BYTE * pBuf, DWORD len)
{
    BOOL r;
    DWORD readCount;

    r = ReadFile (hSource, pBuf, len, &readCount, (LPOVERLAPPED) 0);
# if 0
    TRACE ("readCount: %d\n", readCount);
# endif
    if (r == 0)
    {
        TRACE ("read failure code-last Error: %d\n",
                GetLastError ());
        return -1;
    }
    if (readCount != len)
    {
# if 0
        UI_Printf ("read count (%d) less then request (%d) handle is %x\n",
                readCount, len, hSource);
# endif
        return readCount;
    }
    return (int) len;
}

# define VERBOSE_CHECK

# ifdef VERBOSE_CHECK
static int validHeader (BYTE * pBuf)
{
    if (*pBuf++ != SP_STX0)
        return -1;
    if (*pBuf++ != SP_STX1)
        return -2;
    int len = *pBuf++ << 8;
    len |= *pBuf;
    if (len & ~SP_EXT_LEN_WMASK)
        return -3;
    return len;
}
# endif

static bool validCheckSum (BYTE * pBuf, int mLen)
{
    for (int ckSum = 0; mLen > 0; mLen--)
        ckSum += *pBuf++;
    ckSum &= SP_CKSUM_MASK;

    unsigned short msgCkSum = *pBuf++ << 8;
    msgCkSum |= *pBuf;

    return ckSum == msgCkSum;
}


static bool validMessage (BYTE * pBuf, int mLen, int count)
{
# ifdef VERBOSE_CHECK
    int len = validHeader (pBuf);
    
    if (len != mLen)
        return false;
    if (len + 8 != count)
        return false;
# endif
    if (!validCheckSum (pBuf + 4, mLen))
        return false;
    pBuf += 4 + mLen + 2;
    if (*pBuf++ != SP_ETX0)
        return false;
    if (*pBuf != SP_ETX1)
        return false;

    return true;
}

/*
    findStart -- looks into the buffer for that SP start pattern.
-
    Look for STX0
*/
static BYTE * findStart (BYTE *  pBuf, int len)
{
    while (len > 0)
    {
        if (*pBuf == SP_STX0)
        {
            break;
        }
        pBuf++;
        if (--len <= 0)
            return (BYTE *) 0;
    }
    if (--len <= 0) // are we on last one?
        return pBuf;
    if (pBuf[1] != SP_STX1)
        return findStart (pBuf + 1, len);  // recurse
    if (--len <= 0) // are we on last one?
        return pBuf;
    if (pBuf[2] & ~SP_EXT_LEN_MASK)
        return findStart (pBuf + 2, len);  // recurse
    return pBuf;
}

# define START_GIVE_UP_LEN (16 * 128)

# ifdef _DEBUG
BYTE dataTrace[START_GIVE_UP_LEN];
# endif
int dataIndex = 0;

static int lineUpStart (BYTE * pBuf, int count)
{
    BYTE * pStart = pBuf;

    TRACE ("lus-entry: [%x %x] %d\n", *pBuf, *(pBuf + 1), count);
    if (dataIndex++ >= START_GIVE_UP_LEN)
    {
# ifdef _DEBUG
        if (dataIndex >= sizeof (dataTrace))
        {
            TRACE ("dump dataTrace\n");
            for (int i = 0; i < dataIndex;)
            {
                for (int j = 0; j < 16; j++)
                {
                    TRACE ("%02x ", dataTrace[i++]);
                }
                TRACE ("\n");
            }
            TRACE ("end dataTrace\n");
        }
# endif
        return -dataIndex;
    }
    pBuf = findStart (pStart, count);
    if (!pBuf) // we didn't find a start candidate 
    {
        // we could deliver bogus data here
        return 0;
    }
    dataIndex = 0;
    // they match so, just return the count.
    if (pBuf == pStart)
        return count;

    /*
        OK, we need to move the remaining bytes to the
        head of the buffers.
    */
    int len = pBuf - pStart;
    count -= len;
    // we could deliver bogus data here
    TRACE ("lus-bogus: [%x %x %x %x] %d\n",
            *pBuf, *(pBuf + 1),
            *(pBuf + 2), *(pBuf + 3),
            count);

    for (len = count; len > 0; len--)
    {
        *pStart++ = *pBuf++;
    }
    return count;
}


# ifndef MAX_BUF_SIZE
#   define MAX_BUF_SIZE 1024
# endif

# ifndef DEFAULT_ERROR_TOLERANCE
#   define DEFAULT_ERROR_TOLERANCE -10
# endif

/*
    The code, in thread implements very directly the code to
    detect a SiRF protocol packet.  When if it finds one, it 
    delivers it to the CMessageReceiver.  For now non-protocol
    characters are simply discarded.
*/
static int spMessages = 0;

int CSPSource::thread (void)
{
    int reMsgBytes = 4;
    m_buffer = new BYTE[MAX_BUF_SIZE];
    int bufferIndex = 0;
    int nDataTimeouts = DEFAULT_ERROR_TOLERANCE;
    char cvStrErrors[128];

    TRACE ("sps-m_hSource: %x\n", m_hSource);
    cvStrErrors[0] = 0;

    while (m_run)
    {
        int count;
        unsigned short mLen;
        
        if (pc_checkCommErrors (m_hSource, cvStrErrors))
        {
            TRACE ("sps-%s: %s\n", cvStrErrors);
            m_pMR->send (EMT_Error,
                         EP_CommunicationError,
                         strlen (cvStrErrors) + 1,
                         (BYTE *) cvStrErrors
                        );
        }
        if (bufferIndex < 4)
        {
            if (bufferIndex < 0) // we have returned failure flag
            {
                char * pErrMsg = "Cannot find start code,"
                                 "check serial parameters and restart"
                                 "communicaiton";
                m_pMR->send (EMT_Error,
                             EP_CommunicationFailed,
                             strlen (pErrMsg) + 1,
                             (BYTE *) pErrMsg
                            );
                nDataTimeouts = DEFAULT_ERROR_TOLERANCE;
            }
            /*
                We will continue to reenter this part of the loop
                as long as we have not read four bytes that
                match the protocol header requirements.
            */
            // read remaining bytes into buffer
            count = spRead (m_hSource, m_buffer + bufferIndex, reMsgBytes);
            if (count < 0)
            {
                // any count other than [0-4] indicates a serious error.
                // probably that comm has been close.
                // exit with 0.
                TRACE ("spThread exiting run:%d, count:%d\n", m_run, count);

                if (pc_checkCommErrors (m_hSource, cvStrErrors))
                {
                    TRACE ("sps-%s: %s\n", cvStrErrors);
                }
                strcat (cvStrErrors, "\nCheck COM parameters and restart");
                m_pMR->send (EMT_Error,
                             EP_CommunicationError,
                             strlen (cvStrErrors) + 1,
                             (BYTE *) cvStrErrors
                            );
                goto SPSourceExit;
            }
            switch (count + bufferIndex)
            {
            case 4:
                // if we have four bytes the calculate message length
                mLen = m_buffer[3];

                if (m_buffer[2] & ~SP_EXT_LEN_MASK)
                {
                    bufferIndex = lineUpStart (m_buffer, count);
                    reMsgBytes = 4 - bufferIndex;
                    continue;
                }
                // get the top part of the message length
                mLen |= m_buffer[2] << 8;

                if (m_buffer[1] != SP_STX1)
                {
                    bufferIndex = lineUpStart (m_buffer, count);
                    reMsgBytes = 4 - bufferIndex;
                    continue;
                }

                if (m_buffer[0] != SP_STX0)
                {
                    bufferIndex = lineUpStart (m_buffer, count);
                    reMsgBytes = 4 - bufferIndex;
                    continue;
                }
                reMsgBytes = mLen + 4;
                nDataTimeouts = DEFAULT_ERROR_TOLERANCE;
                bufferIndex += count;
                continue;
            case 3:
            case 2:
            case 1:
                nDataTimeouts = DEFAULT_ERROR_TOLERANCE;
                bufferIndex += count;
                reMsgBytes -= count;
                continue;
            case 0:
                // the basic data timeout should be one second
                if (nDataTimeouts++ > 0)
                {
                    char * pErrMsg = "No data being received,\n"
                                 "check COM parameters and restart"
                                 " communicaiton";
                    m_pMR->send (EMT_Error,
                                 EP_CommunicationError,
                                 strlen (pErrMsg) + 1,
                                 (BYTE *) pErrMsg
                                );
                    TRACE ("Excess data timeouts: %s\n", pErrMsg);
                    goto SPSourceExit;
                }
                sleep (10);
                continue;
            default:
            {
                // any sum other than [0-4] indicates a serious error.
                char  * pErrMsg = "Read failed \n"
                          "check COM parameters and restart"
                          " communicaiton";
                m_pMR->send (EMT_Error,
                             EP_CommunicationError,
                             strlen (pErrMsg) + 1,
                             (BYTE *) pErrMsg
                            );
                TRACE ("Read failed %s -- count:%d\n", pErrMsg, count);
                goto SPSourceExit;
            }
            }
        }
        if (mLen == 0)
        {
            bufferIndex = 0;
            reMsgBytes = 4;
            continue;
        }
        // Read remaining bytes, length already setup in case 1 above,
        // or below.
        count = spRead (m_hSource, m_buffer + 4, reMsgBytes);
        if (!m_run)
        {
            break;
        }
        if (count < reMsgBytes) // assume it can't be greater!
        {
            if (count == 0)
            {
                if (nDataTimeouts++ > 0) 
                {
                    bufferIndex = 0;
                    reMsgBytes = 4;
                }
                continue;
            }
            if (count < 0)
            {
                char * pErrMsg = "Read failed,\n"
                             "check COM parameters and restart"
                             " communicaiton";
                m_pMR->send (EMT_Error,
                             EP_CommunicationError,
                             strlen (pErrMsg) + 1,
                             (BYTE *) pErrMsg
                            );
                TRACE ("Read failed %s\n", pErrMsg);
                goto SPSourceExit;
            }
            bufferIndex += count;
            reMsgBytes -= count;
            // we should end up at read before this if.
            continue;
        }
        bufferIndex += count;
        if (!validMessage (m_buffer, mLen, bufferIndex))
        {    
# if 1
            TRACE ("sps-invalid msg (%d)\n", spMessages);
# endif
            bufferIndex = lineUpStart (m_buffer + 1, bufferIndex);
# if 1
            TRACE ("sps-invalid bi:%d\n", bufferIndex);
# endif
            if (bufferIndex < 4)
            {
                // we still need a header
                reMsgBytes = 4 - bufferIndex;
            }
            else
            {
                if (m_buffer[2] & ~SP_EXT_LEN_MASK)
                {
                  TRACE ("Bad data, try to restart\n");
                  bufferIndex = 0;
                  reMsgBytes = 4;
                  continue;
                }
                mLen = m_buffer[3];
                mLen |= m_buffer[2] << 8;
                reMsgBytes = (mLen + 4 + 4) - bufferIndex;
            }
# if 1
            TRACE ("sps-invalid re:%d\n", reMsgBytes);
# endif
            continue;
        }
        spMessages++;
# if 1
        if ((spMessages & 0x1f) == 1)
            TRACE ("sps(%d): id = %x\n", spMessages, m_buffer[4]);
# endif
        if (!m_disabled)
            wrapAndPostMessage (m_pMR,
                                EMT_Data,
                                m_buffer + 4,
                                mLen
                               );
        bufferIndex = 0;
        reMsgBytes = 4;
    }
SPSourceExit:
    m_pMR->send (EMT_Terminate, 0, 0, m_buffer);
    delete m_buffer;
    m_buffer = 0;
    TRACE ("exiting SPSource: %x\n", m_hSource);
    CThread::dump ();
    return 0;
}






