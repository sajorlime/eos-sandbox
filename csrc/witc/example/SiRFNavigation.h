
/***************************************************************************
 *
 *                          SiRF Technology, Inc. GPS Software
 *                                      Version 1.05
 *
 *      SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source
 *      is the sole property of SiRF Technology, Inc.  Reproduction or
 *      utilization of this source in whole or in part is forbidden
 *      without the written consent of SiRF Technology, Inc.
 *
 ***************************************************************************
 *
 *              (c) SiRF Technology, Inc. 1998 -- All Rights Reserved
 *              Developed by Lapel Software, Inc.
 *
 ***************************************************************************
 *
 * SiRFNavigation.h
 *
 * .........................................................................
 *
 * $Workfile:$
 * $Revision:$
 *     $Date:$
 *
 * .........................................................................
 *
    Defines the SiRFNavigation class.  This class interacts with an existing
    navigation module and the SiRFDemo program to allow testing of navigation
    utilities.
 *
 * .........................................................................
 *
 *      $Log:$
 *
 *  Date     Description
 * ------   -----------
 *
 ***************************************************************************
 *
 * Notes:
    This file defines the classes CSiRFNavigation, and
    CSmartNavigation.  These classes are the classes that actually
    implement the navigation behavior defined for the WinSiRF
    environment.  The CSmartNavigation class makes a run-time decision
    about wether to implement the ModuleNavigation or the
    SiRFNavigation behavior.  The CSiRFNavigation class implements the
    Trakcker only behavior with the navigation calculation being
    accomplished on under Win32.
-
    CSiRFNavigation interacts with the ui_win32 functions to access the
    navigaiton calculation (NavLib).
 *
 * Classes/Types:
 *
    CSiRFNavigation -- This is the class that interacts with the Tracker
    only module, and the navigation calculation to preform much of the
    GPS navigation function on the PC, under Win32. I.e. this class is
    the goal of the who project.
-
    CSmartNavigation -- this is the class that makes are run time decision
    to behave either like the CModuleNavigation class or like the 
    CSiRFNavigation class. It inherits from CSiRFNavigation which inherits
    from CModuleNavigation.  Its adds member funciton pointers to the
    object with can point to one of its own intializaiton state funtions,
    the SiRFNavigation message handling functions or the ModuleNavigation
    message handling functions. The message handling functions are
    processDataMsg, and processControlMsg.
-
    CSiRFNavigationFactory -- this class creates CSiRFNavigation objects.
    It is exported form the DLL.
-
 *
 ***************************************************************************/


# ifndef __SIRFNAVIGATION_H__
# define __SIRFNAVIGATION_H__

# include "ModuleNavigation.h"

class CSiRFNavigation : public CModuleNavigation
{
protected:
    friend class CSiRFNavSendQueue;
    CSiRFNavSendQueue * m_pNavSQ;
    // CMessageDispatch members
    virtual bool processDataMsg (CMessage * pMsg);
    virtual bool processControlMsg (CMessage * pMsg);
    virtual bool processWarnMsg (CMessage * pMsg);
    virtual bool processErrorMsg (CMessage * pMsg);
    virtual bool dispatchMsg (CMessage * pMsg)
    {
        return CMessageDispatch::dispatchMsg (pMsg);
    }

    CSiRFNavigation (void);
public:

    virtual ~CSiRFNavigation ();
    virtual EProtocol initialize (CMessageReceiver * mr,
                                  CMessageQueue * mq,
                                  IIOPort * pGPSPort,
                                  IIOPort * pDGPSPort = 0,
                                  const SNavInitialization * config = 0 
                                 );

private:
    friend class CSiRFNavigationFactory;
    friend class CNavigationFactory;
};

class CSmartNavigation : public CSiRFNavigation
{
protected:
    CMessage * m_pInitMsg;

    bool (CSmartNavigation::* m_pfProcessDataMsg) (CMessage * pMsg);
    bool (CSmartNavigation::* m_pfProcessControlMsg) (CMessage * pMsg);
    // CMessageDispatch members
    virtual bool processDataMsg (CMessage * pMsg);
    virtual bool processControlMsg (CMessage * pMsg);
    
    virtual bool delegateDataMsgToSiRF (CMessage * pMsg);
    virtual bool delegateControlMsgToSiRF (CMessage * pMsg);
    virtual bool delegateDataMsgToModule (CMessage * pMsg);
    virtual bool delegateControlMsgToModule (CMessage * pMsg);
    virtual bool initialDataMsg (CMessage * pMsg);
    virtual bool initialControlMsg (CMessage * pMsg);
public:

    virtual ~CSmartNavigation ();

private:
    CSmartNavigation (void);
    friend class CSiRFNavigationFactory;
    friend class CNavigationFactory;
};

class AFX_CLASS_EXPORT CSiRFNavigationFactory
{
public:
    INavigation * navigationFactory (const char * pParams = 0);
};





# endif

