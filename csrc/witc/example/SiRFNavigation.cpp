
/***************************************************************************
 *
 *                          SiRF Technology, Inc. GPS Software
 *                                      Version 1.05
 *
 *      SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source
 *      is the sole property of SiRF Technology, Inc.  Reproduction or
 *      utilization of this source in whole or in part is forbidden
 *      without the written consent of SiRF Technology, Inc.
 *
 ***************************************************************************
 *
 *              (c) SiRF Technology, Inc. 1998 -- All Rights Reserved
 *              Developed by Lapel Software, Inc.
 *
 ***************************************************************************
 *
 * SiRFNavigation.cpp
 *
 * .........................................................................
 *
 * $Workfile:$
 * $Revision:$
 *     $Date:$
 *
 * .........................................................................
 *
    Inplements the ModuleNavigation class.  This class interacts with an existing
    navigation module and the SiRFDemo program to allow testing of navigation
    utilities.
 *
 * .........................................................................
 *
 *      $Log:$
 *
 *  Date     Description
 * ------   -----------
 *
 ***************************************************************************
 *
 * Notes:
    I've been a bit sloppy here and implemented SiRF and Smart navigation
    in this single source file.  The Smart navigation waits for the first
    message from a module and implements either the moulde or the SiRf 
    navigation personality via member function pointers.  This implies
    some overhead that can be removed on a less powerfull plateform.
-
    See ModuleNavigation for thread, and navigation access methods.
-
    The startup sequence for Smart navigation is as follows.
        1. Applicaiton does intialization.
        2. Any init data is stored for delivery after mode is determined.
        3. Wait for first (identifiable) message from module.
        4. Set Mode based on Id.
        5. Send init info to appropriate target, module or NavLib.
        6. Operate
-
 *
 * Data:
    umSerialDebugFlag -- a flag required by the navigation computation.
    g_pTracker -- is a pointer to the that tracker object. It is needed for
    computication to the tacker for the navigation computation code.
    g_port[256] = {0};
 *
 * Functions:
 *
    NAV_XferNavToTrkCmd -- this functions does the transfer of data from
    the applicaiton to the tracker.  The Navigation computation code needs
    uses the function directly.  It the case of the SiRFNavigation it
    does this by sending the data to the protocol.
-
    CSiRFNavigation::processDataMsg -- This function along with the control
    function defines the behavior of this class as the "PC" based computation
    class. This function is called by the superclass to process all data
    messages.
-
    CSiRFNavigation::processControlMsg -- This function along with the data
    function defines the behavior of this class as the "PC" based computation
    class. This function is called by the superclass to process all data
    messages.
-
    CSiRFNavigation::processWarnMsg -- This functions processes warning
    messages delivered from the unlying navigation libarary
-
    CSiRFNavigation::processWarnMsg -- This functions processes warning
    messages delivered from the unlying navigation libarary
-
    CSmartNavigation::initialControlMsg -- the initial state of the control
    funciton pointer.  This prevents messges from being sent until a messge
    is received. It save the intialization data for later delivery.
-
    CSmartNavigation::initialDataMsg -- the intial state of the data function
    pointer. It uses the first message to determine the personality of the
    SmartNavigation class.
-
    CSmartNavigation::delegateControlMsgToModule -- delegates to module navigation.
-
    CSmartNavigation::delegateDataMsgToModule -- delegates to module navigation.
-
    CSmartNavigation::delegateControlMsgToSiRF -- delegates to sirf navigation.
-
    CSmartNavigation::delegateDataMsgToSiRF -- delegates to sirf navigation.
-
    CSmartNavigation::~CSmartNavigation -- destructor for smart navigation.
-
    CSmartNavigation::CSmartNavigation -- constructor for smart navigation.
-
    CSiRFNavigation::initialize -- is the intialize method sirf navigation.
    It saves the necessary state to communicate with the computation code.
-
    CSiRFNavigationFactory::navigationFactory -- factory for sirf navigation
-
    CSiRFNavigation::~CSiRFNavigation -- destructor for sirf navigaiton
-
    CSiRFNavigation::CSiRFNavigation -- constructor for sirf navigaiton
-
 *
 ***************************************************************************/

# include "stdafx.h"
# include "NavData.h"
# include "SiRFNavigation.h"
# include "SPFactory.h"
# include "protocol.h"
# include "mi_icd.h"
# include "ui_icd.h"
# include "ui_sirf.h"
# include "ui_msg.h"
# include "ui_rtcm.h"
# include "intrface.h"

# include "nav_if.h"
# include "tr_if.h"
# include "ui_sram.h"

short umSerialDebugFlag = 1;

static ITracker * g_pTracker = 0;


/* 
    CSiRFNavSendQueue -- exists to facilitate communication
    between the NavLib and the application.
    The send operation here check the state of the navigation
    object and its ability to send messages at the current
    time.  The allows the NavLib to operate normally
    when the output to the controlling application
    has been disabled.
*/
class CSiRFNavSendQueue : public CMessageQueue
{
    CSiRFNavigation * m_pSiRFNavigation;
    
public:
    virtual bool send (CMessage * pMsg)
    {
        return m_pSiRFNavigation->sendMessageToApp (pMsg);
    }
    CSiRFNavSendQueue (CSiRFNavigation * pSiRFNavigation)
                      : m_pSiRFNavigation (pSiRFNavigation)
    {
    }

};

WERR NAV_XferNavToTrkCmd (LONG ChannelNo,
                          NAVTRKCMD * pNavCmd,
                          INT16 cmdType
                         )
{
    switch (cmdType)
    {
    case CMD_INITIAL_ACQ:
    case CMD_INITIAL_COLD:
    {
        SInitialAcquisition s;
        s.msgid = MID_InitialAcquisition;
        s.m_pData = pNavCmd;
        g_pTracker->initialAcquisition (&s);
        break;
    }
    case CMD_KILL:
        g_pTracker->terminateChannel (pNavCmd->channel);
        break;
    default:
    {
        SReacquisition s;
        s.msgid = MID_Reacquisition;
        s.m_pData = pNavCmd;
        g_pTracker->reacquireReceiverChannel (pNavCmd->channel, &s);
    }
    }
    return SUCCESS;
}

CSiRFNavigation::CSiRFNavigation (void)
    : CModuleNavigation ()
{
    // The presumptions is that this should only be one of these.

    m_pNavSQ = new CSiRFNavSendQueue (this);
    NavAppMsgInit (m_pNavSQ);
    TRK_IFInit ();
}

CSiRFNavigation::~CSiRFNavigation ()
{
    if (m_run != CThread::ts_terminated)
        terminate ();
    if (m_pTracker)
    {
        m_pTracker->terminate ();
        delete m_pTracker;
        g_pTracker = 0;
    }
    NavAppMsgInit (0);
}


static char g_port[256] = {0};

INavigation * CSiRFNavigationFactory::navigationFactory (const char * params)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

# ifdef BUILD_SiRF_NAVIGATION
    INavigation * pNav = new CSiRFNavigation;
# else
    INavigation * pNav = new CSmartNavigation;
# endif
    if (!pNav)
        throw XException ("new CSiRFNavigation Failed");
    if (params)
        strncpy (g_port, params, sizeof (g_port) - 1);

    return pNav;
}



/*
    processControlMsg -- transfer control message data to the
    navigation computation, starts the navigation computation,
    etc.
*/
bool CSiRFNavigation::processControlMsg (CMessage * pMsg)
{
    switch (pMsg->m_user)
    {
    case ecm_NavInitialization:
    {
        SNavInitialization * pNavInit
            = (SNavInitialization *) pMsg->getBuffer ();
        if (MI_SetInitialization (pNavInit) == SUCCESS)
        {
            setSiRFMsgCntl (pNavInit->resetCfg);
            NAVCheckResetCommand (); // NAVStart(); //  HKH  
            ui_ack (MID_NavigationInitialization);
        }
        else
            ui_nak (MID_NavigationInitialization);
        break;
    }
    case ecm_SetAlmanac:
    {
        SAlmanacMsg * pAlmanac = (SAlmanacMsg *) pMsg->getBuffer ();
        MI_SetAlmanac (pAlmanac->aAlmData);
        if (MI_SetAlmanac (pAlmanac->aAlmData) == SUCCESS)
            ui_ack (MID_SetAlmanac);
        else
            ui_nak (MID_SetAlmanac);
        break;
    }
    case ecm_SetEphemeris:
    {
        SSetEphemerisMsg * pEphemeris
            = (SSetEphemerisMsg *) pMsg->getBuffer ();
        MI_SetEphemeris (&pEphemeris->EphData);
        if (MI_SetEphemeris (&pEphemeris->EphData) == SUCCESS)
            ui_ack (MID_SetEphemeris);
        else
            ui_nak (MID_SetEphemeris);
        break;
    }
    case ecm_DifferentialCorrection:
    {
        SDifferentialCorrectionMsg * pCorr
            = (SDifferentialCorrectionMsg *) pMsg->getBuffer ();
        break;
    }
    case ecm_DOPConfiguration:
    {
        SDilutionOfPrecisionMsg * pConfig
            = (SDilutionOfPrecisionMsg *) pMsg->getBuffer ();
        if (MAX_DOP_SELECTION < pConfig->m_cbDOPSelection
            || MIN_DOP_THRESHOLD > pConfig->m_cbGDOPThresh
            || MIN_DOP_THRESHOLD > pConfig->m_cbPDOPThresh
            || MIN_DOP_THRESHOLD > pConfig->m_cbHDOPThresh
            || MAX_DOP_THRESHOLD < pConfig->m_cbGDOPThresh
            || MAX_DOP_THRESHOLD < pConfig->m_cbPDOPThresh
            || MAX_DOP_THRESHOLD < pConfig->m_cbHDOPThresh
           )
        {
            ui_nak (MID_SET_DOP_MODE);
            break;
        }
        ui_ack (MID_SET_DOP_MODE);
        SetUISRAM (ID_DOP_MODE, (void *) pConfig, 0);
        break;
    }
    case ecm_RTCMCorrection:
    {
        SDifferentialCorrectionMsg * corr
            = (SDifferentialCorrectionMsg *) pMsg->getBuffer ();
        break;
    }
    case ecm_NavMode:
    {
        SNavigationModeMsg * pMode
            = (SNavigationModeMsg *) pMsg->getBuffer ();
        if (MAX_ALT_MODE_SELECTION < pMode->m_cbAltMode
            || MAX_ALT_MODE_SOURCE < pMode->m_cbAltSource
            || MIN_ALT_MODE_ALTITUDE > pMode->m_nAltitude
            || MAX_ALT_MODE_ALTITUDE < pMode->m_nAltitude
            || MAX_COAST_TIMEOUT < pMode->m_cbCoastTimeout
            || MAX_DEGRA_MODE_TIMEOUT < pMode->m_cbDegradedTimeout
            || MAX_DR_MODE_TIMEOUT < pMode->m_cbDRTimeout
           )
        {
            ui_nak (MID_SET_NAV_MODE);
            break;
        }
        SetUISRAM (ID_NAV_MODE, (void *) pMode, 0);
        ui_ack (MID_SET_NAV_MODE);
        break;
    }
    case ecm_DGPSConfiguration:
    {
        SSetDGPSModeMsg * pMode
            = (SSetDGPSModeMsg *) pMsg->getBuffer ();
        if (MAX_DGPS_SELECTION < pMode->m_cbDGPSSelection
            || MIN_DGPS_TIMEOUT > pMode->m_cbDGPSTimeout
            || MAX_DGPS_TIMEOUT < pMode->m_cbDGPSTimeout
           )
        {
            ui_nak (MID_SET_DGPS_MODE);
            break;
        }
        ui_ack (MID_SET_DGPS_MODE);
        SetUISRAM (ID_DGPS_MODE, (void *) pMode, 0);
        break;
    }
    case ecm_ElevationMask:
    {
        SSetElevationMaskMsg * pMask
            = (SSetElevationMaskMsg *) pMsg->getBuffer ();
        if (MIN_ELEV_TRK_MASK > pMask->m_nElevTrkMask
            || MAX_ELEV_TRK_MASK < pMask->m_nElevTrkMask
            || MIN_ELEV_NAV_MASK > pMask->m_nElevNavMask
            || MAX_ELEV_NAV_MASK < pMask->m_nElevNavMask
           )
        {
            ui_nak (MID_SET_ELEV_MASK);
            break;
        }
        ui_ack (MID_SET_ELEV_MASK);
        SetUISRAM (ID_ELEV_MASK, (void *) pMask, 0);
        break;
    }
    case ecm_PowerMask:
    {
        SSetPowerMaskMsg * pMask
            = (SSetPowerMaskMsg *) pMsg->getBuffer ();
        if (MIN_POWER_TRK_MASK > pMask->m_cbPowTrkMask
            || MAX_POWER_TRK_MASK < pMask->m_cbPowTrkMask
            || MIN_POWER_NAV_MASK > pMask->m_cbPowNavMask
            || MAX_POWER_NAV_MASK < pMask->m_cbPowNavMask
           )
        {
            ui_nak (MID_SET_POWER_MASK);
            break;
        }
        ui_ack (MID_SET_POWER_MASK);
        SetUISRAM (ID_POW_MASK, (void *) pMask, 0);
        break;
    }
    case ecm_EditingResidual:
    {
        SEditResidualThresholdMsg * pRes 
            = (SEditResidualThresholdMsg *) pMsg->getBuffer ();
        if (MAX_EDIT_RES_THRESH < pRes->m_nEditingResidualThreshold)
        {
            ui_nak (MID_SET_EDITING_RES);
            break;
        }
        ui_ack (MID_SET_EDITING_RES);
        SetUISRAM (ID_EDITING_RES, (void *) pRes, 0);
        break;
    }
    case ecm_SteadyStateDetector:
    {
        SSetSteadyStateDetectorMsg * pDet
            = (SSetSteadyStateDetectorMsg *) pMsg->getBuffer ();
        if (MAX_SSD_THRESH < pDet->m_cbSSDThreshold)
        {
            ui_nak (MID_SET_SS_DETECTOR);
            break;
        }
        ui_ack (MID_SET_SS_DETECTOR);
        SetUISRAM (ID_SS_DETECTOR, (void *) pDet, 0);
        break;
    }
    case ecm_StaticNavigation:
    {
        SSetStaticNavigationMsg * pStaticNav
            = (SSetStaticNavigationMsg *) pMsg->getBuffer ();
        if (MAX_STATIC_NAV_THRESH < pStaticNav->m_cbStaticNavThreshold)
        {
            ui_nak (MID_SET_STAT_NAV);
            break;
        }
        SetUISRAM (ID_STATIC_NAV, (void *) pStaticNav, 0);
        ui_ack (MID_SET_STAT_NAV);
        break;
    }
    case ecm_OperationMode:
        ui_setOperationMode ((SWITCH_OPMODE_PACKET *) pMsg->getBuffer ());
        break;

    case ecm_DirectReckoning:
        ui_directReckoning ((SDirectReckoning *) pMsg->getBuffer ());
        break;

    case ecm_ReportClockStatus:
        ui_clockStatus ();
        break;

    case ecm_ReportAlmanac:
        ui_almanac ();
        break;

    case ecm_ReportEphemeris:
    {
        int svid = *((int *) pMsg->getBuffer ());
        if (svid)
        {
            ui_ephemeris (svid);
        }
        else
        {
            for (svid = 1; svid < MAX_SVID_CNT; svid++)
            {
                ui_ephemeris (svid);
                sleep (1);
            }
        }
        break;
    }
    case ecm_ReportVersion:
        ui_swVersion ();
        break;
    }
    return true;
}


/*
    processDataMsg -- delivers tracker messages to the navigation
    computation.
*/
bool CSiRFNavigation::processDataMsg (CMessage * pMsg)
{
    int id = pMsg->m_user; 
    BYTE * buffer[256];
    CNavData * pNavData;

# ifdef BUSY_DEBUG
    TRACE ("CSiRFNavigation::Processing id: %s\n",
           mid2str ((SPMessageId) id)
          );
# endif
    switch (id)
    {
    case MID_RawDGPS:
        rtcmRcvBytes (pMsg->getBuffer (), pMsg->getLength ());
        return true;
    case MID_TrkSVData:
    {
        pNavData = importPackedNavMessage (buffer, pMsg);
        CTrackerSVDataMsg * pSVDM 
            = static_cast <CTrackerSVDataMsg *> (pNavData);
        TRK_XferTrkToNavData (pSVDM->data.channel, &pSVDM->data);
        secs_since_cstd = 10;
        
        return true;
    }
    case MID_TrkMeasurement:
    {
        pNavData = importPackedNavMessage (buffer, pMsg);
        CTrackerMeasurementMsg * pTMM 
            = static_cast <CTrackerMeasurementMsg *> (pNavData);
        TRK_XferTrkToNavMeas (pTMM->data.channel, &pTMM->data);
        return true;
    }
    case MID_MeasurementComplete:
        pNavData = importPackedNavMessage (buffer, pMsg);
        CMeasurementCompleteMsg * pMCM 
            = static_cast <CMeasurementCompleteMsg *> (pNavData);
# ifdef ORIGONAL_METHOD_FOR_CALCULATING_TIME
        oneSecondTag += 1000 + pps_command.slew_ms;
        pps_command.slew_ms = 0;
# else
        oneSecondTag = pMCM->nOneSecondTag;
# endif            
        HandleOneSecondUpdate ();
        return true;
    }
    return CModuleNavigation::processDataMsg (pMsg);
}

/*
    processWarnMsg -- handles a warning message for the class
*/
bool CSiRFNavigation::processWarnMsg (CMessage * pMsg)
{
    bool r = true;
    switch (pMsg->m_user)
    {
    case EP_CommunicationError:
# ifdef TRY_RESTART
       if (!m_pGPSProtocol->restart ())
       {
           strcat ((char *) pMsg->getBuffer (), " auto retry failed ");
           r = false;
       }
       else
       {
           strcat ((char *) pMsg->getBuffer (), " auto retry attempted ");
       }
# endif
       break;
    }
    m_pSendTo->send (pMsg);
    return r;
}

/*
    processErrorMsg -- handles a error message for the class
*/
bool CSiRFNavigation::processErrorMsg (CMessage * pMsg)
{
    return m_pSendTo->send (pMsg);
}


EProtocol CSiRFNavigation::initialize (CMessageReceiver * pMR,
                                       CMessageQueue * pMQ,
                                       IIOPort * pGPSPort,
                                       IIOPort * pDGPSPort,
                                       const SNavInitialization * config
                                      )
{
    EProtocol e;

    e = CModuleNavigation::initialize (pMR, pMQ, pGPSPort, pDGPSPort, config);
    if (e != EP_Success)
        return e;

    g_pTracker = m_pTracker;
    NavAppMsgInit (m_pSendTo);

    return EP_Success;
}

CSmartNavigation::CSmartNavigation (void)
    :
        CSiRFNavigation (),
        m_pfProcessDataMsg (CSmartNavigation::initialDataMsg),
        m_pfProcessControlMsg (CSmartNavigation::initialControlMsg),
        m_pInitMsg (0)
{
}

CSmartNavigation::~CSmartNavigation ()
{
}




bool CSmartNavigation::delegateDataMsgToSiRF (CMessage * pMsg)
{
    return CSiRFNavigation::processDataMsg (pMsg);
}

bool CSmartNavigation::delegateControlMsgToSiRF (CMessage * pMsg)
{
    return CSiRFNavigation::processControlMsg (pMsg);
}

bool CSmartNavigation::delegateDataMsgToModule (CMessage * pMsg)
{
    return CModuleNavigation::processDataMsg (pMsg);
}

bool CSmartNavigation::delegateControlMsgToModule (CMessage * pMsg)
{
    return CModuleNavigation::processControlMsg (pMsg);
}

/*
    initialDataMsg -- this method waits for the first message
    from the module and sets the personality based on this
    message.
*/
bool CSmartNavigation::initialDataMsg (CMessage * pMsg)
{
    int id = pMsg->m_user; 

# if 1
    TRACE ("CSmartNavigation::received id: %s\n",
           mid2str ((SPMessageId) id)
          );
# endif
    switch (id)
    {
    /*
        I don't really know what I should do if I get DGPS data.
        But it seems reasonable to assume that I can ignore it
        and wait for some other message.
    */
    case MID_RawDGPS:
        return false;

    case MID_TrkSVData:
    case MID_TrkMeasurement:
    case MID_MeasurementComplete:
        m_pfProcessDataMsg
            = CSmartNavigation::delegateDataMsgToSiRF;
        m_pfProcessControlMsg
            = CSmartNavigation::delegateControlMsgToSiRF;
        /*
            For SiRFNavigation we need to process the 
            init message here if there is one.  In this
            case it will do the nav start call.  If not
            then we need to do the nav start.
            After that process the data message.
        */
        if (m_pInitMsg)
        {
            (this->*m_pfProcessControlMsg) (m_pInitMsg);
            m_pNavMessages->release (m_pInitMsg);
            m_pInitMsg = 0;
        }
        else
        {
            NAVStart (); 
        }
        (this->*m_pfProcessDataMsg) (pMsg);
        break;
            
    case MID_ASCIIData:
        return CModuleNavigation::processDataMsg (pMsg);

    default:
        m_pfProcessDataMsg
            = CSmartNavigation::delegateDataMsgToModule;
        m_pfProcessControlMsg
            = CSmartNavigation::delegateControlMsgToModule;
        /*
            For ModuleNavigation we send the init message to the
            processor, and then send the message to the processor.
        */
        if (m_pInitMsg)
        {
            (this->*m_pfProcessControlMsg) (m_pInitMsg);
            m_pNavMessages->release (m_pInitMsg);
            m_pInitMsg = 0;
        }
        (this->*m_pfProcessDataMsg) (pMsg);
        break;
    }
    return true;
}

bool CSmartNavigation::initialControlMsg (CMessage * pMsg)
{
    switch (pMsg->m_user)
    {
    case ecm_NavInitialization:
    {
        m_pInitMsg = m_pNavMessages->alloc ();
        *m_pInitMsg = *pMsg;
        return true;
    }
    }
    return false;
}

bool CSmartNavigation::processControlMsg (CMessage * pMsg)
{
    return (this->*m_pfProcessControlMsg) (pMsg);
}


bool CSmartNavigation::processDataMsg (CMessage * pMsg)
{
    return (this->*m_pfProcessDataMsg) (pMsg);
}


