
/***************************************************************************
 *
 *                          SiRF Technology, Inc. GPS Software
 *                                      Version 1.05
 *
 *      SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source
 *      is the sole property of SiRF Technology, Inc.  Reproduction or
 *      utilization of this source in whole or in part is forbidden
 *      without the written consent of SiRF Technology, Inc.
 *
 ***************************************************************************
 *
 *              (c) SiRF Technology, Inc. 1998 -- All Rights Reserved
 *              Developed by Lapel Software, Inc.
 *
 ***************************************************************************
 *
 * ModuleNavigation.h
 *
 * .........................................................................
 *
 * $Workfile:$
 * $Revision:$
 *     $Date:$
 *
 * .........................................................................
 *
    Defines the ModuleNavigation class.  This class interacts with an existing
    navigation module and the SiRFDemo program to allow testing of navigation
    utilities.
 
 *
 * .........................................................................
 *
 *      $Log:$
 *
 *  Date     Description
 * ------   -----------
 *
 ***************************************************************************
 *
 * Notes:
    See CNavigation, IGPSNavigation and CThread for a definitions of
    the methods supported by CModuleNavigation.
    This file defines the CModuleNavigation implementation scope.
    the stand alone navigation module.  It subclasses the CNavigation
    It is first complete concrete class in the INavigaiton implementation
    hierarchy.
    Its thread instance is used by all subclasses (SiRFNavigation, and
    SmartNavigation).
    It is really a fairly hollow object in that it only acts a message
    translation point.  It imports packed protocol objects and passes
    them the application and it accepts unpacked data from the application
    and passes it to the protocol.  The protocol packs it ships it to the
    module, Hence the name ModuleNavigation.
    The methods processDataMsg, and processControlMsg are the methods
    that are responsible for handling the two kinds of messages that
    are expeceted at the task level.  Most of the work for the classes
    that inherit from here is only to overide these methods.
 *
 *
 * Classes/Types:
    CModuleNavigation -- the module navigation instanciation of INavigation.
    CModuleNavigationFactory -- the factory for CModuleNavigation classes.
    This class is exported from the DLL.
 *
 *
 *
 ***************************************************************************/


# ifndef __MODULENAVIGATION_H__
# define __MODULENAVIGATION_H__

# include "Navigation.h"
# include "sp_icd.h"

class CModuleNavigation : public CNavigation
{
protected:
    virtual int thread (void);
    virtual bool processDataMsg (CMessage * pMsg);
    virtual bool processControlMsg (CMessage * pMsg);
    virtual bool dispatchMsg (CMessage * pMsg)
    {
        return CMessageDispatch::dispatchMsg (pMsg);
    }

public:
    EProtocol initialize (CMessageReceiver * mr,
                            CMessageQueue * mq,
                            IIOPort * pGPSPort,
                            IIOPort * pDGPSPort,
                            const SNavInitialization * config = 0 
                           );
    virtual EProtocol initialize (const SNavInitialization * config);
    virtual EProtocol terminate (void);

    // navigation operaitors
    virtual EProtocol setAlmanac (const SSetAlmanacMsg * almanac);
    virtual EProtocol setEphemeris (const SSetEphemerisMsg * ephemeris);

    virtual EProtocol setRTCMCorrection (const SDifferentialCorrectionMsg * corr);
    virtual EProtocol setNavMode (const SNavigationModeMsg * mode);
    virtual EProtocol setDilutionOfPrecisionMode (const SDilutionOfPrecisionMsg * config);
    virtual EProtocol setDGPSMode (const SSetDGPSModeMsg * config);
    virtual EProtocol setElevationMask (const SSetElevationMaskMsg * mask);
    virtual EProtocol setPowerMask (const SSetPowerMaskMsg * mask);
    virtual EProtocol setEditingResidual (const SEditResidualThresholdMsg * res);
    virtual EProtocol setSteadyStateDetector (const SSetSteadyStateDetectorMsg * det);
    virtual EProtocol setStaticNavigation (const SSetStaticNavigationMsg * staticNav);
    virtual EProtocol setOperationMode (const SSwitchOpModeMsg * pOpMode);
    virtual EProtocol directReckoning (const SDirectReckoning * pDR);

    virtual EProtocol reportClockStatus (void);
    virtual EProtocol reportAlmanac (void);
    virtual EProtocol reportEphemeris (int svid);
    virtual EProtocol reportVersion (void);

# if 0
    Use the inherited version of CNavigation
    // message delivery control.
    void disableMessages (void);
    void enableMessages (void);
# endif

    virtual ~CModuleNavigation ();
protected:
    CModuleNavigation (void);
    friend class CModuleNavigationFactory;
};

class AFX_CLASS_EXPORT CModuleNavigationFactory
{
public:
    INavigation * navigationFactory (const char * pParams = 0);
};



# endif __MODULENAVIGATION_H__

