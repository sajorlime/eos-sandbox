/***************************************************************************
 *                                                                                              *
 *                      SiRF Technology, Inc. GPS Software                          *
 *                                                                                              *
 *  SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source        *
 *  is the sole property of SiRF Technology, Inc.  Reproduction or      *
 *  utilization of this source in whole or in part is forbidden             *
 *  without the written consent of SiRF Technology, Inc.                        *
 *                                                                                              *
 ***************************************************************************
 *                                                                                              *
 *          (c) SiRF Technology, Inc. 1998 -- All Rights Reserved           *
 *                                                                                              *
 ***************************************************************************
 *
 * NavController.cpp
 *
 * .........................................................................
 *
 * .........................................................................
 *
 * 
 *
 * .........................................................................
 *
 * 
 ***************************************************************************
 *
 * NOTES:
 *
 *
 *
 * TYPES:
 *
 *
 *
 * DATA:
 *
 *
 *
 * FUNCTIONS:
 *
 *
 *
 * CLASS EQUIVALENCE:
 *
 *
 *
 ***************************************************************************/

#include "stdafx.h"
#include "WinSirfDemo.h"
#include <ui_inc.h>
#include "mainfrm.h"
# include "SPFactory.h"
# include "dGPSFactory.h"
#include "NavController.h"
# include "SiRFNavigation.h"
# include "NavData.h"

#include "dlgdss.h"
#ifdef SUPPORTING_NMEA
#include "dlgNMEA.h"
#endif //SUPPORTING_NMEA
#include "dlgNM.h"
#include "dlgDOP.h"
#include "dlgDGPS.h"
#include "dlgEMask.h"
#include "dlgPMask.h"
#include "dlgER.h"
#include "dlgSSD.h"
#include "dlgSN.h"
#include "preferen.h"        
#include "trace.h"
#include "dlgOpMod.h"

#include "dlgSPort.h"
#include "dlglog.h"
#include "miscdefs.h"

#define ELEV_MASK_LSB            10.0001
#define SSD_THRESH_LSB           10.0001
#define STAT_NAV_THRESH_LSB  10.0001


CNavController *CNavController::m_pInstance=NULL;

# define VERBOSE_LOGGING

CNavController::CNavController() :
	m_pGPSProtocol(NULL),
	m_pDGPSProtocol(NULL),
	m_pNavigation(NULL),
	m_pNavQueue(NULL)
{
	CLogger *pPlogger = new CLogger();
# ifdef VERBOSE_LOGGING
	m_pLoggerAccess = new CLoggerAccess(pPlogger, "WinSiRF");
# else
	m_pLoggerAccess = new CLoggerAccess(pPlogger, "");
# endif
}

CNavController::~CNavController()
{
	if (m_pNavigation != NULL) {
		terminateNavigation();
	}
	if (m_pLoggerAccess != NULL) {
		delete m_pLoggerAccess->logger();
		delete m_pLoggerAccess;
	}
}


EProtocol CNavController::initNavigation()
{
	CWaitCursor wait;

	terminateNavigation();		// Make sure previous nav is stopped

	EProtocol eResult;

    if (CPreferences::Get().m_nDataSrc == CDlgDSS::SRC_RANDOM) {
		AfxMessageBox("Random mode not yet implemented");
		return EP_NotImplemented;
	}


	//
	//1) Create protocols
	CSiRFProtocolFactory spCommFactory;
	CdGPSProtocolFactory dgpsCommFactory;
	CString tmpStr;
	switch (CPreferences::Get().m_nMeasSrc)
    {
	case CDlgDSS::SRC_MEAS_BFILE:
		m_pGPSProtocol
            = reinterpret_cast <IIOProtocol *> (spCommFactory.ioPortFactory ("file:"));
		if (!m_pGPSProtocol->setPort (CPreferences::Get().m_strMeasBFile)) {
			return EP_PortInitFailed;
		}
		break;
	case CDlgDSS::SRC_MEAS_SERIAL:
		m_pGPSProtocol = reinterpret_cast<IIOProtocol *> (spCommFactory.ioPortFactory ("com:"));
		tmpStr = CPreferences::Get().m_strMeasPort + ":";	//?? SirfProtocol wants a "COMx:"
		if (!m_pGPSProtocol->setPort(tmpStr)) {
			TRACE("Error setting port: %s\n", tmpStr);
			return EP_PortInitFailed;
		}
		if (!m_pGPSProtocol->setPortOption("baud rate", CPreferences::Get().m_strMeasBaud)) {
			TRACE("Error setting baud rate to: %s\n", CPreferences::Get().m_strMeasBaud);
			return EP_PortInitFailed;
		}
		if (!m_pGPSProtocol->setPortOption("stop bits", CPreferences::Get().m_strMeasSBits)) {
			TRACE("Error setting stop bits to: %s\n", CPreferences::Get().m_strMeasSBits);
			return EP_PortInitFailed;
		}
		// Note: Setting of databits & parity not done (because its not option in dialogbox)
		m_pDGPSProtocol
            = reinterpret_cast <IIOProtocol *> (dgpsCommFactory.ioPortFactory ("file:"));
		break;
	default:
		AfxMessageBox("Only serial and binary file modes currently work");

		return EP_NotImplemented;
	}
	//
	// 2) Create Navigation
# if 0
    // Note: This should move to CModuleNavigation ? what does this mean ?
	CModuleNavigationFactory navFactory;
# else
	CSiRFNavigationFactory navFactory;
# endif
	m_pNavigation = navFactory.navigationFactory ("");
	//
	// Now initialize the nav component
	m_pNavQueue = new CMessageQueue ();
	eResult
        = m_pNavigation->initialize (((CMainFrame *) AfxGetMainWnd())->GetSiRFMessageReceiver(),
                                            m_pNavQueue,
                                            m_pGPSProtocol,
											m_pDGPSProtocol
                                           );
	return eResult;
}

EProtocol CNavController::terminateNavigation()
{
	if (m_pNavigation == NULL)
        return EP_Success;		// Navigation not yet created

	m_pNavigation->terminate();

	delete m_pGPSProtocol;
	delete m_pNavQueue;
	delete m_pNavigation;

	m_pGPSProtocol = NULL;
	m_pNavQueue = NULL;
	m_pNavigation = NULL;
	return EP_Success;
}


void CNavController::ReceiverInit()
{
    CTrace trace("CNavController::ReceiverInit");

    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
    {
        CString strOut;     
        CNavigationInitalize initData;

        /////// Init Packet
        initData.msgid        = MID_NavigationInitialization;
        initData.posX         = CPreferences::Get().m_lRIX;
        initData.posY         = CPreferences::Get().m_lRIY;
        initData.posZ         = CPreferences::Get().m_lRIZ;
        initData.clkOffset     = CPreferences::Get().m_lRIClock;
        if (CPreferences::Get().m_bRIUseDOSTime)
        {                           
            GetSystemTOW(&initData.weekno, &initData.timeOfWeek);
        }
        else
        {
            initData.timeOfWeek  = CPreferences::Get().m_lRITOW;
            initData.weekno = CPreferences::Get().m_nRIWeekNo;
        }
        initData.chnlCnt = (UBYTE) CPreferences::Get().m_nRIChnlCnt;

        switch(CPreferences::Get().m_cRIResetMode)
        {
            case 0:
                initData.resetCfg = RESET_HOT;
                break;
            case 1:
                initData.resetCfg = RESET_WARM_NOINIT;
                break;
            case 2:
                initData.resetCfg = RESET_WARM_INIT;
                break;
            case 3:
                initData.resetCfg = RESET_COLD;
                break;
            default:
                initData.resetCfg = RESET_COLD;
                break;
        } 
            
        if(CPreferences::Get().m_bRIEnableRawTrkMsg)
            initData.resetCfg |= MSG_SEND_RAW_TRACK;

        if(CPreferences::Get().m_bRIEnableDebugMsg)
            initData.resetCfg |= MSG_SEND_DEBUG;

        //if(NMEA is checked)
        // initData.m_ResetParam |= STARTUP_PROTOCOL_NMEA;

        TRACE("[SrcMeas] Reset byte is 0x%02x\n", initData.resetCfg);       

        initData.Trace();
		m_pNavigation->initialize(&initData);
    }
}


//////////////////////////////////////////////////////////////////
void CNavController::SendNMEA(int iMode)
{
#ifdef SUPPORTING_NMEA
    int i;

    CTrace trace("CNavController::SendNMEA");
    
    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL && m_pSrcData != NULL)
    {
        CDlgSelectNMEA dlgNMEA;
        
        SDataSetNMEAMode dataNMEA;
        char acBuf[sizeof (SDataSetNMEAMode) + SP_TRANSPORT_OVERHEAD];
    
        static char *apszMode[] = {"On", "Off", "Enable", "Disable" };

        TRACE("\n[SrcMeas] Setting NMEA Mode: %ld\n", iMode);

        char strData[200];
        
        // clear the NMEACfg[] array
        for(i=0;i<MSG_NMEACNT;i++)
        {
            dataNMEA.m_Pkt.m_NMEACfg[i].m_Rate  =0;
            dataNMEA.m_Pkt.m_NMEACfg[i].m_Chksum=0;
        }
        dataNMEA.m_Pkt.m_BaudRate=9600;     /* desired baud rate    */
        
        // fill the command
        dataNMEA.m_Pkt.m_ID   = MID_SetNMEAMode;
        dataNMEA.m_Pkt.m_iMode = (UBYTE)iMode;  
        

        // do CDlgSelectNMEA for NMEA_ON and NMEA_ENABLE
        switch(iMode)
        {
# if 0
            case NMEA_ON:
            case NMEA_ENABLE:
                if(dlgNMEA.DoModal()==IDOK)     // grab dlg data into dataNMEA.m_Pkt data
                {
                    // fill the NMEACfg portion to select which messages are ON
                    dataNMEA.m_Pkt.m_BaudRate=atoi(dlgNMEA.m_BaudRate);
                    
                    // Get the data from the dlg box
                    for(int i=0;i<MSG_NMEACNT;i++)
                    {
                        dataNMEA.m_Pkt.m_NMEACfg[i].m_Rate  =dlgNMEA.m_Rate[i];
                        dataNMEA.m_Pkt.m_NMEACfg[i].m_Chksum=dlgNMEA.m_useChksum;
                    }
    
                }
                else
                    break;
                
            case NMEA_OFF:
                // Get export string and write it
                dataNMEA.Export((UBYTE*)acBuf,sizeof(acBuf), CPreferences::Get().m_bMeasNBO);
                ((CSrcDataSerial*)m_pSrcData)->SendData(acBuf, dataNMEA.GetPacketSize());
            
                sprintf(strData, "Sent MID_SetNMEAMode Message: %s\n", apszMode[iMode]);
                trace.Log(strData);

// HACK !!!!!!!!!!!!! FIX ME !!!!! FIX ME !!!!! FIX ME !!!
                //if(iMode==NMEA_ENABLE)
                //{
                    // close serial port since sirfdemo doesn't accept NMEA
                //   Close();
                //} 
        
                break;
        
            case NMEA_DISABLE:
                ((CSrcDataSerial*)m_pSrcData)->SendBreak();
                
                // reset baud rate to 9600/8/N/1, save it
                // reset serial port
                // save the new MeasSerialBaud profile string
                theApp.WriteProfileString("Data Sources", "MeasSerialBaud", "9600");
                theApp.WriteProfileString("Data Sources", "MeasSerialDBits","8");
                theApp.WriteProfileString("Data Sources", "MeasSerialParity","N");
                theApp.WriteProfileString("Data Sources", "MeasSerialSBits","1");

                theApp.UpdateTitleBar();
                
                // wait for at least 12/1200 ms for slowest break condition
                theApp.msDelay(50);		//((12/1200.0)*1000)+1);
						
                // close/reopen serial port with new parameters
                Close();
                Open();

                sprintf(strData, "Sent 'Serial Break' to set default protocol\n");
                break;
# endif
            
            default:
                sprintf(strData, "NMEA mode not supported\n");
        }

    // AfxMessageBox(strData);

        theApp.m_LogFile.Write(strData);
    }
#else //SUPPORTING_NMEA
	AfxMessageBox("NMEA not supported in this version");
#endif //SUPPORTING_NMEA
}       

//////////////////////////////////////////////////////////////////
void CNavController::SetSerialPort(void)
{

	CTrace trace("CNavController::SetSerialPort");

	if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
	{
	    CDlgsport dlgSport (m_pGPSProtocol, "Set GPS Serial Port Parameters");
    
	    CSetSerialPortMsg dataSport;   // outgoing packet

	    // fill the command
	    dataSport.msgid  = MID_SetSerialPort;
    
	    // do CDlgsport
		if(dlgSport.DoModal()==IDOK) // grab dlg data into dataSport.m_Pkt data
	    {
            char * strBaud = "baud rate";
            char * strStopBits = "stop bits";

            m_pGPSProtocol->setPortOption (strBaud, dlgSport.m_Baud);
            m_pGPSProtocol->setPortOption (strStopBits, dlgSport.m_Sbits);
        
            // this should be a IIPort instead of a protocol,
            // someone needs to have set the port info elsewhere.
            m_pGPSProtocol->initializeDev ();

			trace.Log ("Sent MID_SetSerialPort Message\n");
    
			//theApp.m_LogFile.Write(strData);

			theApp.UpdateTitleBar ();
            
			// ensure that the command is sent out
//#pragma message ( __FILE__ " Info: This can still be made a lot more general")
		}
	}
}    

//////////////////////////////////////////////////////////////////
void CNavController::SetAlmanac(CString strAlmanac)
{
    BOOL bStop;
    int ix;

    CTrace trace("CNavController::SetAlmanac");

    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
        return;

    CSetAlmanacMsg AlmPkt;     // outgoing packet

    if (strAlmanac.IsEmpty ())
        return;
    
    char acBuf[sizeof (CAlmanacMsg) + SP_TRANSPORT_OVERHEAD];
    char *pData;

    CStdioFile fAlmanac(strAlmanac, CFile::modeRead);
    
    bStop = FALSE;
    
    for(int nLin = 0; nLin<5 && !bStop; nLin++) // read in 5 line file description
    {
        if (!fAlmanac.ReadString (acBuf, sizeof(acBuf)))
            bStop=TRUE;
    }        

    // fill the command
    AlmPkt.msgid  = MID_SetAlmanac;
    
    // file data is MAX_SVID_CNT lines composed of ALMANAC_ENTRY CSV integers
    for (ix=0; ix < MAX_SVID_CNT && !bStop; ix++)
    {
        if (fAlmanac.ReadString(acBuf, sizeof(acBuf)))    // get a line
        {                     
            for(int z=0;z<ALMANAC_ENTRY && !bStop;z++)
            {
                if(z==0)    pData=strtok(acBuf," ,");
                else        pData=strtok(NULL," ,");
                if((pData==NULL) || (*pData=='\n')) 
                    bStop=TRUE;
                else
                    AlmPkt.aAlmData[(ix*ALMANAC_ENTRY)+z] = atoi(pData);
            }
        }
        else
        {
            bStop = TRUE;
        }
    }
    fAlmanac.Close();

    if (bStop)
    {
        AfxMessageBox("Almanac Data File Format Error\nSet Almanac Aborted",MB_ICONEXCLAMATION);
        return;
    }

	// Get export string and write it
    m_pNavigation->setAlmanac(&AlmPkt);
	trace.Log("Set MID_SetAlmanac Message.\n");
	return;
}

//////////////////////////////////////////////////////////////////
void CNavController::PollAlmanac(void)
{

	CTrace trace("CNavController::PollAlmanac");
    m_pNavigation->reportAlmanac();
}    

//////////////////////////////////////////////////////////////////
void CNavController::SetEphemeris(CString strEphemeris)
{
    BOOL bStop;
    int ix;

    CTrace trace("CNavController::SetEphemeris");

    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
        return;

    CSetEphemerisMsg EphPkt;     // outgoing packet

    // note buffer is used to read in line and must be >= line len >= pkt size
    char acBuf[sizeof (CEphemerisMsg) + SP_TRANSPORT_OVERHEAD];

    char strData[200];
    char *pData;
    
    if(strEphemeris.IsEmpty()) return;
    
    CStdioFile fEphemeris(strEphemeris, CFile::modeRead);
    
    bStop = FALSE;
    
    for(int nLin=0;nLin<5 && !bStop;nLin++) // read in 5 line file description
    {
        if (!fEphemeris.ReadString(acBuf, sizeof(acBuf)))
            bStop=TRUE;
    }        

    // fill the command
    EphPkt.msgid  = MID_SetEphemeris;
    
    // file data is MAX_SVID_CNT lines composed of 3*15 CSV integers
    for (ix=0; ix < MAX_SVID_CNT && !bStop; ix++)
    {
        if (fEphemeris.ReadString(acBuf, sizeof(acBuf)))    // get a line
        {                     
            for(int z=0;z<(3*15) && !bStop;z++)
            {
                if(z==0)    pData=strtok(acBuf," ,");
                else        pData=strtok(NULL," ,");
                if((pData==NULL) || (*pData=='\n')) 
                    bStop=TRUE;
                else {
					int x0 = z/15;
					int y0 = z-x0*15;
                    EphPkt.EphData.subframe[x0][y0] = atoi(pData);
				}
            }
        }
        else
        {
            bStop = TRUE;
        }
        
        if (bStop)
        {
           AfxMessageBox("Ephemeris Data File Format Error\nSet Ephemeris Aborted",MB_ICONEXCLAMATION);
           fEphemeris.Close();
           return;
        }
        else
        {
            if((EphPkt.EphData.subframe[0][0]>=1) && (EphPkt.EphData.subframe[0][0]<=MAX_SVID_CNT))
            {// Get export string and write it
			    m_pNavigation->setEphemeris(&EphPkt);
				trace.Log("Set MID_SetEphemeris Message\n");

            }
            else /* invalid svid #, don't send it */
            {
               sprintf(strData, "Didn't Send MID_SetEphemeris Message.SVID:%u\n",EphPkt.EphData.subframe[0][0]);
               trace.Log(strData);
            }   
        }   
    }
    fEphemeris.Close();

   //theApp.msDelay(60);
   return;
}

//////////////////////////////////////////////////////////////////
void CNavController::PollEphemeris(void)
{
	CTrace trace("CNavController::PollEphemeris");

	if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
	{
	    m_pNavigation->reportEphemeris (0);
	}
}    

//////////////////////////////////////////////////////////////////
void CNavController::SetDGPSPort(void)
{
    CTrace trace("CNavController::SetDGPSPort");
    
    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
    {
        CDlgsport dlgSport (m_pGPSProtocol, "Set DGPS Serial Port");

        // do CDlgsport
        if(dlgSport.DoModal()!=IDOK) return;	// user hit cancel
		// grab dlg data into dataSport.m_Pkt data

        CSetDGPSPortMsg dataSport;   // outgoing packet
        unsigned char acBuf[sizeof (CSetDGPSPortMsg) + SP_TRANSPORT_OVERHEAD];
        
        // fill the command
        dataSport.msgid  = MID_SetDGPSPort;
        
#if 1
            dataSport.comm.baud=atol(dlgSport.m_Baud);
            dataSport.comm.bits=atoi(dlgSport.m_Dbits);
            dataSport.comm.stop=atoi(dlgSport.m_Sbits);
            
            if(dlgSport.m_Parity=="NONE") dataSport.comm.parity=UP_None;
            else if(dlgSport.m_Parity=="ODD")  dataSport.comm.parity=UP_Odd;
            else if(dlgSport.m_Parity=="EVEN") dataSport.comm.parity=UP_Even;
            else                                          dataSport.comm.parity=UP_None;
#else
            // fill with invalid data, use this to test messsage NAK
            dataSport.comm.baud=19200;
            dataSport.comm.bits=7;
            dataSport.comm.bits=1;
            dataSport.comm.parity=3; // {UP_None=0,UP_Odd=1,UP_Even=2}
#endif          
            dataSport.comm.pad0=0;

			dataSport.Export(acBuf, sizeof(acBuf));
 			if (m_pGPSProtocol->sendMessage(acBuf, dataSport.GetPacketSize()) != EP_Success) {
				AfxMessageBox("Error setting DGPS Port");
				return;
			}
       
            trace.Log("Sent MID_SetDGPSPort Message\n");
        
            //AfxMessageBox(strData);

            // to log the cmd sent
            //theApp.m_LogFile.Write(strData);
    }
}    


//////////////////////////////////////////////////////////////////
void CNavController::SwitchOpMode(void)
{
    CTrace trace("CNavController::SwitchOpMode");
    
    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
    {
        CDlgOpMode dlgOpMode;
        SSwitchOpModeMsg dataOpMode;   // outgoing packet
    
        char strData[200];
        
        // fill the command   
        dataOpMode.msgid  = MID_SwitchOpMode;
        
        // do CDlgOpMode
        if(dlgOpMode.DoModal()==IDOK) // grab dlg data into dataOpMode.m_Pkt data
        {
            if(dlgOpMode.m_nOpMode_selection == 1)
            	dataOpMode.mode = OP_MODE_TESTING;
            else
            	dataOpMode.mode = OP_MODE_NORMAL; 
            	
            dataOpMode.SVid   = dlgOpMode.m_nOpMode_SVid;
            dataOpMode.period = dlgOpMode.m_nOpMode_period;
            
		    //m_pGPSProtocol->sendMessage (acBuf, dataOpMode.GetPacketSize());
			m_pNavigation->setOperationMode(&dataOpMode);        
        
            sprintf(strData, "Sent MID_SwitchOpMode Message\n");
            trace.Log(strData);
     }  

    }
}    

//////////////////////////////////////////////////////////////////

void CNavController::SetProtocolUser1(void)
{
	ASSERT(0);		// Mode not supported - should never be called
}
/////////////////////////////////////////////////////////////////////

void CNavController::SetNavMode()
{
    CTrace trace("CNavController::SetNavMode");
    
    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
    {
        CNavigationModeMsg dataSetNM;  // outgoing packet
        CDlgNM NMbox;
        
        if(NMbox.DoModal()==IDOK)
        {
            
            // fill the command
            dataSetNM.msgid  = MID_SET_NAV_MODE;

            dataSetNM.m_bEnable3dMode =    (UBYTE)NMbox.m_bEnable3dMode;
            dataSetNM.m_bEnableConAltMode=(UBYTE)NMbox.m_bEnableAltConstraint;
            dataSetNM.m_cbDegradedMode =   (UBYTE)NMbox.m_nDegradedMode; //(UBYTE)NMbox.m_bEnableCurbMode;
            dataSetNM.m_cbTBD = 1;                                       //(UBYTE)NMbox.m_bEnableClockMode;
            dataSetNM.m_bEnableDRMode =    (UBYTE)NMbox.m_bEnableDRMode;
            dataSetNM.m_nAltitude =               NMbox.m_nAltitude;
            dataSetNM.m_cbAltMode =        (UBYTE)NMbox.m_nAltMode;
            dataSetNM.m_cbAltSource =  (UBYTE)abs(NMbox.m_nAltSource);   // goes -1 if grayed out!
            dataSetNM.m_cbCoastTimeout =          NMbox.m_cbCoastTimeout;
            dataSetNM.m_cbDegradedTimeout =       NMbox.m_cbDegradedTimeout;
           	if( NMbox.m_bEnableDRMode )
            	dataSetNM.m_cbDRTimeout =          NMbox.m_cbDRTimeout;
            else
            	dataSetNM.m_cbDRTimeout = 0;
         
            dataSetNM.m_bTrackSmoothMode=NMbox.m_bTrackSmoothMode;

			m_pNavigation->setNavMode(&dataSetNM);        
            trace.Log("Sent MID_SET_NAV_MODE Message\n");
        }
    }
}    


/////////////////////////////////////////////////////////////////////

void CNavController::SetDOP()
{
    CTrace trace("CNavController::SetDOPMask");
    
    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
    {
        SDilutionOfPrecisionMsg dataSetDOP;     // outgoing packet
        CDlgDOP DOPbox;
        
        if(DOPbox.DoModal()==IDOK)
        {
            // fill the command
            dataSetDOP.msgid                =       MID_SET_DOP_MODE;
            dataSetDOP.m_cbDOPSelection = (UBYTE)DOPbox.m_nDOPSelection;  // DOP radio button value
            dataSetDOP.m_cbGDOPThresh   =        DOPbox.m_cbGDOPThresh;
            dataSetDOP.m_cbPDOPThresh   =        DOPbox.m_cbPDOPThresh;
            dataSetDOP.m_cbHDOPThresh   =        DOPbox.m_cbHDOPThresh;

			m_pNavigation->setDilutionOfPrecisionMode(&dataSetDOP);
            trace.Log("Sent MID_SET_DOP_MODE Message\n");
        
            //AfxMessageBox(strData);
    
            //theApp.m_LogFile.Write(strData);
    
        }
    }
}    

/////////////////////////////////////////////////////////////////////

void CNavController::SetDGPS()
{
    CTrace trace("CNavController::SetDGPS");
    
    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
    {
        CSetDGPSModeMsg dataSetDGPS;     // outgoing packet
        CDlgDGPS DGPSbox;
        
        if(DGPSbox.DoModal()==IDOK)
        {
            // fill the command
            dataSetDGPS.msgid                  =           MID_SET_DGPS_MODE;
            dataSetDGPS.m_cbDGPSSelection = (UBYTE)DGPSbox.m_nDGPSSelection;   // DGPS radio button value
            dataSetDGPS.m_cbDGPSTimeout   =           DGPSbox.m_cbDGPSTimeout;

			m_pNavigation->setDGPSMode(&dataSetDGPS);
            trace.Log("Sent MID_SET_DGPS_MODE Message\n");
    
        }
    }
}    

/////////////////////////////////////////////////////////////////////

void CNavController::SetElevationMask()
{
    CTrace trace("CNavController::SetElevationMask");
    
    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
    {
        CSetElevationMaskMsg dataSet;     // outgoing packet
        CDlgEMask EMbox;
        
        if(EMbox.DoModal()==IDOK)
        {
            // fill the command
            dataSet.msgid            = MID_SET_ELEV_MASK;
            dataSet.m_nElevTrkMask = (INT16)(EMbox.m_fElevTrkMask*ELEV_MASK_LSB); // int=float*10
            dataSet.m_nElevNavMask = (INT16)(EMbox.m_fElevNavMask*ELEV_MASK_LSB); // int=float*10

		    m_pNavigation->setElevationMask(&dataSet);
        }
    }
}    

/////////////////////////////////////////////////////////////////////

void CNavController::SetPowerMask()
{
    CTrace trace("CNavController::SetPowerMask");
    
    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
    {
        SSetPowerMaskMsg dataSet;     // outgoing packet
        CDlgPMask PMbox;
        
        if(PMbox.DoModal()==IDOK)
        {
            // fill the command
            dataSet.msgid            = MID_SET_POWER_MASK;
            dataSet.m_cbPowTrkMask = PMbox.m_cbPowTrkMask;     
            dataSet.m_cbPowNavMask = PMbox.m_cbPowNavMask;

			m_pNavigation->setPowerMask(&dataSet);
            trace.Log("Sent MID_SET_POWER_MASK Message\n");
        }
    }
}    

/////////////////////////////////////////////////////////////////////

void CNavController::SetEditingResidual()
{
    CTrace trace("CNavController::SetEditingResidual");
    
    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
    {
        CEditResidualThresholdMsg dataSet;    // outgoing packet
        CDlgER ERbox;
        
        if(ERbox.DoModal()==IDOK)
        {
            
            
// test message only ---------------------------------------------
#ifdef TEST_PACKET            
            char acBuf[sizeof(UINT16)*(ALMANAC_SIZE+100)+3];
            char strData[200];//for debug
            
            // 300 max elements
            ERbox.m_nEditingResidualThresh=min(ERbox.m_nEditingResidualThresh,(ALMANAC_SIZE+100));

            // fill the command
            acBuf[0] = (char)MID_TestPacket;
            acBuf[1] = ((ERbox.m_nEditingResidualThresh)>>8)&0xFF;
            acBuf[2] = (ERbox.m_nEditingResidualThresh) & 0xFF;

            // put # of elements into array
            for(unsigned int i=0;i<ERbox.m_nEditingResidualThresh;i++)
            {
               acBuf[3+(i*2)]=(i>>8)&0xFF;
               acBuf[4+(i*2)]=i&0xFF;
            }

            ((CSrcDataSerial*)m_pSrcData)->SendData(acBuf, 3+(ERbox.m_nEditingResidualThresh*2));
            sprintf(strData, "Sent MID_Test Message\n");
            trace.Log(strData);
        
            //AfxMessageBox(strData);
            //theApp.m_LogFile.Write(strData);
// test message only ---------------------------------------------

#else
         {
            // fill the command
            dataSet.msgid            = MID_SET_EDITING_RES;

            if( ERbox.m_nEditingResidualEnabled )
                dataSet.m_nEditingResidualThreshold = ERbox.m_nEditingResidualThresh;   
            else
                dataSet.m_nEditingResidualThreshold = 0;
			
			m_pNavigation->setEditingResidual(&dataSet);
            trace.Log("Sent MID_SET_EDITING_RES Message\n");
         }
#endif    
        }
    }
}    
/////////////////////////////////////////////////////////////////////

void CNavController::SetSteadyStateDetection()
{
    CTrace trace("CNavController::SetSteadyStateDetection");
    
    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
    {
        CSetSteadyStateDetectorMsg dataSet;     // outgoing packet
        CDlgSSD SSDbox;
        
        if(SSDbox.DoModal()==IDOK)
        {
            // fill the command
            dataSet.msgid = MID_SET_SS_DETECTOR;
            
            if( SSDbox.m_nSSDEnabled )
                dataSet.m_cbSSDThreshold = (UBYTE)(SSDbox.m_fSSDThreshold * SSD_THRESH_LSB); // char=float*10
            else                                                                                                
                dataSet.m_cbSSDThreshold = 0;

			m_pNavigation->setSteadyStateDetector(&dataSet);
            trace.Log("Sent MID_SET_SS_DETECTOR Message\n");
        }
    }
}    
/////////////////////////////////////////////////////////////////////

void CNavController::SetStaticNavigation()
{
    CTrace trace("CNavController::SetStaticNavigation");
    
    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
    {
        CSetStaticNavigationMsg dataSet;    // outgoing packet
        CDlgSN SNbox;
        
        if(SNbox.DoModal()==IDOK)
        {
            // fill the command                           
            dataSet.msgid = MID_SET_STAT_NAV; 
            
            if( SNbox.m_nStaticNavEnabled )
                dataSet.m_cbStaticNavThreshold = (UBYTE)(SNbox.m_fStaticNavThreshold * STAT_NAV_THRESH_LSB); // char=float*10
            else                                                                                                              
                dataSet.m_cbStaticNavThreshold = 0;

			m_pNavigation->setStaticNavigation(&dataSet);
            trace.Log("Sent MID_SET_STAT_NAV Message\n");
        }
    }
}    
//////////////////////////////////////////////////////////////////

void CNavController::PollSWVersion()
{
    CTrace trace("CNavController::PollSWVersion");
    
    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
    {
		m_pNavigation->reportVersion();
    }
}    

//////////////////////////////////////////////////////////////////

void CNavController::PollClockStatus()
{
    CTrace trace("CNavController::PollClockStatus");
    
    if (CPreferences::Get().m_nMeasSrc == CDlgDSS::SRC_MEAS_SERIAL)
    {
		m_pNavigation->reportClockStatus();
        trace.Log("Sent MID_PollClockStatus Message\n");
    }
}    

void CNavController::OnUpdatePrefs()
{
}

void CNavController::StartLogging()
{
	m_pLoggerAccess->startLogging(CPreferences::Get().m_strLogFile);

	char temp[120];
	m_strLogTime = _strtime(temp);
	m_strLogDate = _strdate(temp);
	theApp.UpdateTitleBar();
	

	CString strTimestamp = "log file opened ";
	CString str = "WinSiRF Demo Version 2.0.16 ";
	m_pLoggerAccess->write(str, str.GetLength());
	str = m_strLogDate +" at " + m_strLogTime + "\n\n";
	m_pLoggerAccess->write(str, str.GetLength());
}

void CNavController::StopLogging()
{
	m_pLoggerAccess->stopLogging();
}

BOOL CNavController::ShowLoggingSettings()
{
	BOOL bRet = TRUE;

	CLogFileDlg dlg;

	StopLogging();	// just in case try to overwrite same file !
	if ((bRet = (dlg.DoModal() == IDOK)) == TRUE)
	{
		//xxx CPreferences::Get().LoadLogging();	// Umm - dont think we need this
	}

	return bRet;
}

CString &CNavController::GetStrLogTime()
{
	return m_strLogTime;
}

CString &CNavController::GetStrLogDate()
{
	return m_strLogDate;
}


void CNavController::LogIncomingMessage(CNavData *pNavData)
{
	if (!IsLoggingOn()) return;

	if (!IsLoggableMessage(pNavData)) return;

	CString msgStr;
	pNavData->String(msgStr);
# ifdef VERBOSE_LOGGING
	GetLogger()->printLogMsg("Incomming", msgStr);
# else
	GetLogger()->printLogMsg("", msgStr);
# endif
}

BOOL CNavController::IsLoggableMessage(CNavData *pNavData)
{
	int id = pNavData->GetId();
	if (id < 0) return FALSE;		// Unknown type
	if (id >= 256) return FALSE;	// Just being defensive

	return CPreferences::Get().m_abLogMessage[id];
}
