
/***************************************************************************
 *
 *                          SiRF Technology, Inc. GPS Software
 *                                      Version 1.05
 *
 *      SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source
 *      is the sole property of SiRF Technology, Inc.  Reproduction or
 *      utilization of this source in whole or in part is forbidden
 *      without the written consent of SiRF Technology, Inc.
 *
 ***************************************************************************
 *
 *              (c) SiRF Technology, Inc. 1998 -- All Rights Reserved
 *              Developed by Lapel Software, Inc.
 *
 ***************************************************************************
 *
 * ModuleNavigation.cpp
 *
 * .........................................................................
 *
 * $Workfile:$
 * $Revision:$
 *     $Date:$
 *
 * .........................................................................
 *
    Inplements the ModuleNavigation class.  This class interacts with an existing
    navigation module and the SiRFDemo program to allow testing of navigation
    utilities.
 *
 * .........................................................................
 *
 *      $Log:$
 *
 *  Date     Description
 * ------   -----------
 *
 ***************************************************************************
 *
 * Notes:
    This class is the first completely concrete class in the navigation
    implementation heirarchy.  Inherting classes need only override
    processControlMessage and processDataMessage, and possibly the initalize
    function.  To implement an non-thread bad INavigation the user should
    move back down the hierarchy.
 *
 *
 * Data:
    g_port -- storage for factory parameters, currently unused.
    g_vnId -- storeage for counting the messages that have gone
    bye.  This is a debug aid and can be removed.
 *
 * Private Functions:
    fillBuf -- a routine to fill a buffer.
 * Functions:
    CModuleNavigation::initialize -- the intialize for module navigation, a function
    that must be called.
-
    CModuleNavigation::terminate -- undoes initialize.
-
    -
       methods below all use sendControl method to deliver data to
       thread (see below).  Inheriting class need not override
       this functions, unless they want to check or modify parameters
       values.
    -
-
    CModuleNavigation::initialize -- sends the navigation init info.
-
    CModuleNavigation::setAlmanac -- user level access.
-
    CModuleNavigation::setEphemeris -- user level access.
-
    CModuleNavigation::setRTCMCorrection -- user level access.
-
    CModuleNavigation::setNavMode -- user level access.
-
    CModuleNavigation::setDilutionOfPrecisionMode -- user level access.
-
    CModuleNavigation::setDGPSMode -- user level access.
-
    CModuleNavigation::setElevationMask -- user level access.
-
    CModuleNavigation::setPowerMask -- user level access.
-
    CModuleNavigation::setEditingResidual -- user level access.
-
    CModuleNavigation::setSteadyStateDetector -- user level access.
-
    CModuleNavigation::setStaticNavigation -- user level access.
-
    CModuleNavigation::setOperationMode -- user level access.
-
    CModuleNavigation::directReckoning -- user level access.
-
    CModuleNavigation::reportClockStatus -- user level access.
-
    CModuleNavigation::reportAlmanac -- user level access.
-
    CModuleNavigation::reportEphemeris -- user level access.
-
    CModuleNavigation::reportVersion -- user level access.
-
    CModuleNavigation::CModuleNavigation -- class constructure.
    CModuleNavigation::~CModuleNavigation --class destructure.
    CModuleNavigationFactory::navigationFactory -- factory for class.
-
    CModuleNavigation::processControlMsg -- method that handles all
    incomming control messages. This function is overriden by inheriting
    classes to allow change behavoir.
-
    CModuleNavigation::processDataMsg -- method that handles all
    incomming data messages. This function is overriden by inheriting
    classes to allow change behavoir.
-
    CModuleNavigation::thread -- is the thread for handling navigation
    actions.  The inheriting classes should override processDataMessage
    and processControlMessage to override the behavior of this class.
 *
 ***************************************************************************/

# include "stdafx.h"
# include "NavData.h"
# include "ModuleNavigation.h"
# include "SPFactory.h"
# include "protocol.h"



static char g_port[256] = {0};

EProtocol CModuleNavigation::initialize (CMessageReceiver * pMR,
                                         CMessageQueue * pMQ,
                                         IIOPort * pGPSPort,
                                         IIOPort * pDGPSPort,
                                         const SNavInitialization * config
                                        )
{
    CNavigation::initialize (pMR, pMQ, pGPSPort, pDGPSPort, config);

    CTrackerFactory tf;
    m_pTracker = tf.trackerFactory (g_port);
    TRACE ("m_pTracker : %x\n", m_pTracker);
    EProtocol e = m_pTracker->initialize (m_pNavMessages, m_pGPSProtocol);
    if (e != EP_Success)
        return e;

    if (!m_pTracker)
        return EP_InitializationFailed;
    if (!start ())
    {
        return EP_InitializationFailed;
    }

    if (m_init.msgid != MID_NavigationInitialization)
        return EP_Success;
    return initialize (&m_init);
}


EProtocol CModuleNavigation::terminate (void)
{
    suspend ();
    if (m_pTracker)
    {
        m_pTracker->terminate ();
        delete m_pTracker;
        m_pTracker = 0;
    }
    while (m_pNavMessages && m_pNavMessages->messagesAvailable ())
    {
       try 
       {
           CMessage * pMsg = m_pNavMessages->receive ();
           m_pNavMessages->release (pMsg);
       }
# if _DEBUG
       catch (XException & x)
       {
           TRACE (x.m_pReason);
           TRACE ("received while terminating\n");
       }
# endif
	   catch (...)
	   {
	   }
    }
      
    CThread::terminate (4000);
    return EP_Success;
}

EProtocol CModuleNavigation::initialize (const SNavInitialization * config)
{
    if (sendControl (ecm_NavInitialization,
                     sizeof (SNavInitialization),
                     (const BYTE *) config
                    )
       )
        return EP_Success;
    return EP_InitializationFailed;
}




// navigation operaitors
EProtocol CModuleNavigation::setAlmanac (const SSetAlmanacMsg * almanac)
{
    if (sendControl (ecm_SetAlmanac,
                     sizeof (SAlmanacMsg),
                     (const BYTE *) almanac
                    )
       )
        return EP_Success;
    return EP_SendFailed;
}


EProtocol CModuleNavigation::setEphemeris (const SSetEphemerisMsg * almanac)
{
    if (sendControl (ecm_SetEphemeris,
                     sizeof (SSetEphemerisMsg),
                     (const BYTE *) almanac
                    )
       )
        return EP_Success;
    return EP_SendFailed;
}



EProtocol CModuleNavigation::setRTCMCorrection (const SDifferentialCorrectionMsg * corr)
{
    if (sendControl (ecm_RTCMCorrection,
                     sizeof (SRTCMCorrectionMsg),
                     (const BYTE *) corr
                    )
       )
        return EP_Success;
    return EP_SendFailed;
}


EProtocol CModuleNavigation::setNavMode (const SNavigationModeMsg * mode)
{
    if (sendControl (ecm_NavMode,
                     sizeof (SNavigationModeMsg),
                     (const BYTE *) mode
                    )
       )
        return EP_Success;
    return EP_SendFailed;
}


EProtocol
CModuleNavigation::setDilutionOfPrecisionMode (const SDilutionOfPrecisionMsg * config)
{
    if (sendControl (ecm_DOPConfiguration,
                     sizeof (SDilutionOfPrecisionMsg),
                     (const BYTE *) config
                    )
       )
        return EP_Success;
    return EP_SendFailed;
}


EProtocol CModuleNavigation::setDGPSMode (const SSetDGPSModeMsg * config)
{
    if (sendControl (ecm_DGPSConfiguration,
                     sizeof (SSetDGPSModeMsg),
                     (const BYTE *) config
                    )
       )
        return EP_Success;
    return EP_SendFailed;
}


EProtocol CModuleNavigation::setElevationMask (const SSetElevationMaskMsg * mask)
{
    if (sendControl (ecm_ElevationMask,
                     sizeof (SSetElevationMaskMsg),
                     (const BYTE *) mask
                    )
       )
        return EP_Success;
    return EP_SendFailed;
}


EProtocol CModuleNavigation::setPowerMask (const SSetPowerMaskMsg * mask)
{
    if (sendControl (ecm_PowerMask,
                     sizeof (SSetPowerMaskMsg),
                     (const BYTE *) mask
                    )
       )
        return EP_Success;
    return EP_SendFailed;
}


EProtocol CModuleNavigation::setEditingResidual (const SEditResidualThresholdMsg * res)
{
    if (sendControl (ecm_EditingResidual,
                     sizeof (SEditResidualThresholdMsg),
                     (const BYTE *) res
                    )
       )
        return EP_Success;
    return EP_SendFailed;
}


EProtocol CModuleNavigation::setSteadyStateDetector (const SSetSteadyStateDetectorMsg * det)
{
    if (sendControl ( ecm_SteadyStateDetector,
                      sizeof (SSetSteadyStateDetectorMsg),
                      (const BYTE *) det
                    )
       )
        return EP_Success;
    return EP_SendFailed;
}


EProtocol CModuleNavigation::setStaticNavigation (const SSetStaticNavigationMsg * staticNav)
{
    if (sendControl (ecm_StaticNavigation,
                     sizeof (SSetStaticNavigationMsg),
                     (const BYTE *) staticNav
                    )
       )
        return EP_Success;
    return EP_SendFailed;
}

EProtocol CModuleNavigation::setOperationMode (const SSwitchOpModeMsg * opMode)
{
    if (sendControl (ecm_OperationMode,
                     sizeof (SSwitchOpModeMsg),
                     (const BYTE *) opMode
                    )
       )
        return EP_Success;
    return EP_SendFailed;
}


EProtocol CModuleNavigation::directReckoning (const SDirectReckoning * pDR)
{
    if (sendControl (ecm_DirectReckoning,
                     sizeof (SDirectReckoning),
                     (const BYTE *) pDR
                    )
       )
        return EP_Success;
    return EP_SendFailed;
}


EProtocol CModuleNavigation::reportClockStatus (void)
{
    if (sendControl (ecm_ReportClockStatus, 0, 0))
        return EP_Success;
    return EP_SendFailed;
}


EProtocol CModuleNavigation::reportAlmanac (void)
{
    if (sendControl (ecm_ReportAlmanac, 0, 0))
        return EP_Success;
    return EP_SendFailed;
}

EProtocol CModuleNavigation::reportEphemeris (int svid)
{
    if (sendControl (ecm_ReportEphemeris, sizeof (int), (const BYTE *) &svid))
        return EP_Success;
    return EP_SendFailed;
}


EProtocol CModuleNavigation::reportVersion (void)
{
    if (sendControl (ecm_ReportVersion, 0, 0))
        return EP_Success;
    return EP_SendFailed;
}





CModuleNavigation::CModuleNavigation (void)
    : CNavigation ()
{
}

CModuleNavigation::~CModuleNavigation ()
{
    if (m_run != CThread::ts_terminated)
        terminate ();
    if (m_pTracker)
    {
        m_pTracker->terminate ();
        delete m_pTracker;
    }
}


INavigation * CModuleNavigationFactory::navigationFactory (const char * params)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

    INavigation * pNav = new CModuleNavigation;
    if (!pNav)
        throw XException ("new CModuleNavigation Failed");
    if (params)
        strncpy (g_port, params, sizeof (g_port) - 1);

    return pNav;
}


static int g_vnId[256] = {0};

static void fillBuf (char * pBuf,
                      const char * pName,
                      BYTE id,
                      const BYTE * pData,
                      int len
                     )
{
    g_vnId[id]++;
    sprintf (pBuf, "id:%d, len:%d, name:%s, cnt: %d\r\n",
                    id, len, pName, g_vnId[id], pData
            );
}

/*
    processControlMsg -- process control messages for the module
    navigation.  This consists of exporting the data and sending
    to the protocol.
*/
bool CModuleNavigation::processControlMsg (CMessage * pMsg)
{
	TRACE("In CModuleNavigation::processControlMsg %d, %s\n", pMsg->m_user, 
		mid2str((SPMessageId) pMsg->m_user));
    switch (pMsg->m_user)
    {
    case ecm_NavInitialization:
    {
        CNavigationInitalize NavInit ((SNavInitialization *) pMsg->getBuffer ());
	    unsigned char acBuf[sizeof (CNavigationInitalize)];
		NavInit.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, NavInit.GetPacketSize());
        break;
    }
    case ecm_SetAlmanac:
    {
        CSetAlmanacMsg SetAlmanac ((SSetAlmanacMsg *) pMsg->getBuffer ());
	    unsigned char acBuf[sizeof (CSetAlmanacMsg)];
		SetAlmanac.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, SetAlmanac.GetPacketSize());
        break;
    }
    case ecm_SetEphemeris:
    {
        CSetEphemerisMsg Ephemeris ((SSetEphemerisMsg *) pMsg->getBuffer ());
	    unsigned char acBuf[sizeof (CSetEphemerisMsg)];
		Ephemeris.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, Ephemeris.GetPacketSize());
        break;
    }
# if 0
    case ecm_DifferentialCorrection:
    {
        SDifferentialCorrectionMsg * pCorr
            = (SDifferentialCorrectionMsg *) pMsg->getBuffer ();
        break;
    }
# endif
    case ecm_DOPConfiguration:
    {
        CDilutionOfPrecisionMsg Config
            ((SDilutionOfPrecisionMsg *) pMsg->getBuffer ());
	    unsigned char acBuf[sizeof (CDilutionOfPrecisionMsg)];
		Config.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, Config.GetPacketSize());
        break;
    }
    case ecm_RTCMCorrection:
    {
//#pragma message ( __FILE__ " Warning:!!! Where in the world is this RTCMCorrection msg!!!")
		/*xxx
        CDifferentialCorrectionMsg orr
            ((SDifferentialCorrectionMsg *) pMsg->getBuffer ());
	    unsigned char acBuf[sizeof (CDilutionOfPrecisionMsg)];
		Config.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, Config.GetPacketSize());
		xxx*/
        break;
    }
    case ecm_NavMode:
    {
        CNavigationModeMsg Mode
            ((SNavigationModeMsg *) pMsg->getBuffer ());
	    unsigned char acBuf[sizeof (CNavigationModeMsg)];
		Mode.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, Mode.GetPacketSize());
        break;
    }
    case ecm_DGPSConfiguration:
    {
        CSetDGPSModeMsg Mode
            ((SSetDGPSModeMsg *) pMsg->getBuffer ());
	    unsigned char acBuf[sizeof (CSetDGPSModeMsg)];
		Mode.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, Mode.GetPacketSize());
        break;
    }
    case ecm_ElevationMask:
    {
        CSetElevationMaskMsg Mask
            ((SSetElevationMaskMsg *) pMsg->getBuffer ());
	    unsigned char acBuf[sizeof (CSetElevationMaskMsg)];
		Mask.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, Mask.GetPacketSize());
        break;
    }
    case ecm_PowerMask:
    {
        CSetPowerMaskMsg Mask
            ((SSetPowerMaskMsg *) pMsg->getBuffer ());
	    unsigned char acBuf[sizeof (CSetPowerMaskMsg)];
		Mask.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, Mask.GetPacketSize());
        break;
    }
    case ecm_EditingResidual:
    {
        CEditResidualThresholdMsg Res 
            ((SEditResidualThresholdMsg *) pMsg->getBuffer ());
	    unsigned char acBuf[sizeof (CEditResidualThresholdMsg)];
		Res.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, Res.GetPacketSize());
        break;
    }
    case ecm_SteadyStateDetector:
    {
        CSetSteadyStateDetectorMsg Det
            ((SSetSteadyStateDetectorMsg *) pMsg->getBuffer ());
	    unsigned char acBuf[sizeof (CSetSteadyStateDetectorMsg)];
		Det.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, Det.GetPacketSize());
        break;
    }
    case ecm_StaticNavigation:
    {
        CSetStaticNavigationMsg StaticNav
            ((SSetStaticNavigationMsg *) pMsg->getBuffer ());
	    unsigned char acBuf[sizeof (CSetStaticNavigationMsg)];
		StaticNav.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, StaticNav.GetPacketSize());
        break;
    }

    case ecm_ReportClockStatus:
	{
		CPollClockStatusMsg msg;
	    unsigned char acBuf[sizeof (CPollClockStatusMsg)];
		msg.msgid = MID_PollClockStatus;
		msg.Control = 0;
		msg.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, msg.GetPacketSize());
        break;
	}

    case ecm_ReportAlmanac:
	{
		CPollAlmanacMsg msg;
	    unsigned char acBuf[sizeof (CPollAlmanacMsg)];
		msg.msgid = MID_PollAlmanac;
		msg.Control = 0;
		msg.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, msg.GetPacketSize());
        break;
	}

    case ecm_ReportEphemeris:
    {
		CPollEphemerisMsg msg;
	    unsigned char acBuf[sizeof (CPollEphemerisMsg)];
		msg.msgid = MID_PollEphemeris;
		msg.Control = 0;
		msg.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, msg.GetPacketSize());
        break;
    }
    case ecm_ReportVersion:
	{
		CPollVersionMsg msg;
	    unsigned char acBuf[sizeof (CPollVersionMsg)];
		msg.msgid = MID_PollSWVersion;
		msg.Control = 0;
		msg.Export(acBuf, sizeof(acBuf));
		m_pGPSProtocol->sendMessage(acBuf, msg.GetPacketSize());
        break;
    }
	default:
		TRACE("CModuleNavigation::processControlMsg - Unknown message id: %d\n", pMsg->m_user);
		;;
	}
    return true;
}



/*
    processDataMsg -- process incoming navigation data messages for
    module navigation. It imports the data and sends it on to the
    applicaiton level via the CMessageReceiver passed via the 
    initailization function.
*/
bool CModuleNavigation::processDataMsg (CMessage * pMsg)
{
    int id = pMsg->m_user; 
    int len = pMsg->getLength ();
    BYTE * pBuf = pMsg->getBuffer ();


    try
    {
        if  (m_sendMessages == false)
        {
            m_pNavMessages->release (pMsg);
            return false;
        }
        CMessage * pNavMsg = m_pSendTo->allocMessage ();
        if (!pNavMsg)
            return false;

        CNavData * pNavData
                = importPackedNavMessage (pNavMsg->getBuffer (), pMsg);
        /* 
            if this were SiRFNavigation, we would need to decide if
            it a message handled locally, or by the app.
        */
		  if (!pNavData)
			  return 0;
        pNavMsg->setId (pMsg->getId ());
        // old way return m_pSendTo->send (pNavMsg);
        return sendMessageToApp (pNavMsg);
    }
    catch (XException & X)
    {
        TRACE ("Caught exception ");
        TRACE (X.m_pReason);
        TRACE ("\n");
        m_pSendTo->send (EMT_Error,
                         EP_Failed,
                         strlen (X.m_pReason) + 1,
                         (BYTE *) X.m_pReason
                        );
        return false;
    }
    catch (...)
    {
        TRACE ("Some other excpetion\n");
        return false;
    }
}

/*
    thread -- is the thread function for the navigation implementation.
    While it is in the running state ti waits for messages and then
    dispatches to either the process control or process data methods.
*/
int CModuleNavigation::thread (void)
{
    CMessage * pMsg;

    while (m_run == CThread::ts_running)
    {
        pMsg = m_pNavMessages->receive ();
        if (m_run == CThread::ts_terminated)
            break;
# if 0
        TRACE ("navThread id: %x\n", pMsg->getId ());
# endif
        if (!dispatchMsg (pMsg))
            goto navThreadExit;
# if 0
        switch (pMsg->getId ())
        {
        case EMT_Control:
            processControlMsg (pMsg);
            // Deal with MI input here.
            break;
        case EMT_Data:
        case EMT_DataContinuation:
        case EMT_CriticalData:
            processDataMsg (pMsg);
            break;
        case EMT_Debug:
            m_pSendTo->send (pMsg);
            break;
        case EMT_Warn:
            switch (pMsg->m_user)
            {
            case EP_CommunicationError:
               //m_pTracker->terminate ();
               //m_pTracker->initialize (m_pNavMessages, m_pGPSProtocol);
               strcat ((char *) pMsg->getBuffer (), " auto retry attempted ");
               break;
            }
            // Note we should add members that handle each type.
            m_pSendTo->send (pMsg);
            break;
        case EMT_Error:
        case EMT_Fatal:
            m_pSendTo->send (pMsg);
            goto navThreadExit;
        case EMT_Terminate:
        default:
            goto navThreadExit;
        }
# endif
        try 
        {
            m_pNavMessages->release (pMsg);
        }
        catch (XException & x)
        {
            TRACE (x.m_pReason);
            TRACE (" exiting\n");
            m_pSendTo->send (EMT_Error,
                             EP_Failed,
                             strlen (x.m_pReason) + 1,
                             (BYTE *) x.m_pReason
                            );
            goto navThreadExit;
        }
    }
navThreadExit:
    m_run = CThread::ts_terminated;
    try 
    {
        if (m_pNavMessages)
        {
            m_pNavMessages->release (pMsg);
            while (m_pNavMessages->messagesAvailable ())
            {
                pMsg = m_pNavMessages->receive ();
                m_pNavMessages->release (pMsg);
            }
            m_pNavMessages->detach ();
            m_pNavMessages = 0;
        }
        if (m_pSendTo)
        {
            m_pSendTo->detach ();
            m_pSendTo = 0;
        }
    }
    catch (XException & x)
    {
        TRACE (x.m_pReason);
        TRACE (" exiting anyway\n");
        if (m_pSendTo)
            m_pSendTo->send (EMT_Error,
                             EP_Failed,
                             strlen (x.m_pReason) + 1,
                             (BYTE *) x.m_pReason
                            );
    }
    TRACE ("navThread exiting: %x\n", GetCurrentThreadId ());
    return 0;
}



