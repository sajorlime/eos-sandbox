/***************************************************************************
 *                                                                         *
 *                   SiRF Technology, Inc. GPS Software                    *
 *                                                                         *
 *    SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source     *
 *    is the sole property of SiRF Technology, Inc.  Reproduction or       *
 *    utilization of this source in whole or in part is forbidden          *
 *    without the written consent of SiRF Technology, Inc.                 *
 *                                                                         *
 ***************************************************************************
 *                                                                         *
 *          (c) SiRF Technology, Inc. 1996 -- All Rights Reserved          *
 *                                                                         *
 ***************************************************************************
 *
 * MainFrm.cpp
 *
 * .........................................................................
 *
 * $Workfile$
 * $Revision$
 *     $Date$
 *
 * .........................................................................
 *
 * Implementation of the CMainframe class
 *
 * .........................................................................
 *
 *      $Log$
 * 
 ***************************************************************************
 *
 * NOTES:
 *
 *
 *
 * TYPES:
 *
 *
 *
 * DATA:
 *
 *
 *
 * FUNCTIONS:
 *
 *
 *
 * CLASS EQUIVALENCE:
 *
 *
 *
 ***************************************************************************/

// Author: Steven Mark, modified by Prakash for WinSiRF

#include <afxwin.h>

#include "stdafx.h"
#include "WinSiRFDemo.h"
#include "dlgprefe.h"
#include "dlgdss.h"
#include "dlgri.h"
#include "mainfrm.h"
#include "usrmsg.h"
#include "trace.h"
#include "dlglog.h"

#include "NavData.h"
#include "NavMsgReceiver.h"
#include "miscdefs.h"
#include "NavController.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

const enum
{
   TOOLBAR_BITMAP_COMM_UNCONNECTED=4,
   TOOLBAR_BITMAP_PAUSE_BLACK=5,
   TOOLBAR_BITMAP_PAUSE_RED=8,
   TOOLBAR_BITMAP_COMM_CONNECTED=9
};
const enum
{
   TOOLBAR_ITEM_CONNECTION_INDEX=6,
   TOOLBAR_ITEM_PAUSE_INDEX=7
};

BOOL  bCleanPortOpening = FALSE;

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
   //{{AFX_MSG_MAP(CMainFrame)
   ON_WM_TIMER()
   ON_WM_SHOWWINDOW()
   ON_MESSAGE(WM_USER_UPDATE_PREFS,    OnUpdateOrientation)
   ON_MESSAGE(WM_USER_MEAS_CLOSED,     OnMeasClosed)
   ON_MESSAGE(WM_USER_DATA_RECEIVED,   OnDataReceived)
   ON_MESSAGE(WM_USER_STORE_ALMANAC,   OnStoreAlmanac)
   ON_MESSAGE(WM_USER_STORE_EPHEMERIS, OnStoreEphemeris)
   ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
   ON_COMMAND(IDM_VIEW_SIGNAL, OnViewSignal)
   ON_UPDATE_COMMAND_UI(IDM_VIEW_SIGNAL, OnUpdateViewSignal)
   ON_MESSAGE(WM_USER_CLOSE_SIGNAL_VIEW, OnViewSignalClose)
   ON_MESSAGE(WM_USER_CLOSE_SIGNAL_VIEW2, OnViewSignalClose2)
   ON_COMMAND(ID_VIEW_RADAR, OnViewRadar)
   ON_UPDATE_COMMAND_UI(ID_VIEW_RADAR, OnUpdateViewRadar)
   ON_MESSAGE(WM_USER_CLOSE_RADAR_VIEW, OnViewRadarClose)
   ON_COMMAND(ID_VIEW_MAP, OnViewMap)
   ON_UPDATE_COMMAND_UI(ID_VIEW_MAP, OnUpdateViewMap)
   ON_MESSAGE(WM_USER_CLOSE_MAP_VIEW, OnViewMapClose)
   ON_COMMAND(IDM_DEBUG_DATAWINDOW, OnViewDebugDataWindow)
   ON_UPDATE_COMMAND_UI(IDM_DEBUG_DATAWINDOW, OnUpdateViewDebugDataWindow)
   ON_MESSAGE(WM_USER_CLOSE_DEBUGDATA_VIEW, OnViewDebugDataWindowClose)
   ON_COMMAND(ID_VIEW_MESSAGES_NAVIGATION, OnViewNavMsg)
   ON_UPDATE_COMMAND_UI(ID_VIEW_MESSAGES_NAVIGATION, OnUpdateViewNavMsg)
   ON_MESSAGE(WM_USER_CLOSE_NAVMSG_VIEW, OnViewNavMsgClose)
   ON_UPDATE_COMMAND_UI(ID_WINDOW_CLOSEALL, OnUpdateWindowCloseAll)
   ON_COMMAND(ID_WINDOW_CLOSEALL, OnWindowCloseAll)
   ON_WM_CREATE()
   ON_COMMAND(ID_FILE_PREFERENCES, OnFilePreferences)
   ON_MESSAGE(WM_USER_CLOSE_DOC, OnDocClose)
   ON_WM_DESTROY()
   ON_COMMAND(ID_ACTION_INITIALIZEDATASOURCE, OnActionInitializeDataSource)
   ON_UPDATE_COMMAND_UI(ID_ACTION_INITIALIZEDATASOURCE, OnUpdateActionInitializeDataSource)
// ON_COMMAND(ID_ACTION_NMEA_ON, OnActionSendNMEAOn)
// ON_COMMAND(ID_ACTION_NMEA_OFF, OnActionSendNMEAOff)
   ON_COMMAND(ID_ACTION_NMEA_ENABLE, OnActionSendNMEAEnable)
   ON_COMMAND(ID_ACTION_NMEA_DISABLE, OnActionSendNMEADisable)
   ON_COMMAND(ID_ACTION_OPENDATASOURCE, OnActionOpenDataSource)
   ON_UPDATE_COMMAND_UI(ID_ACTION_OPENDATASOURCE, OnUpdateActionOpenDataSource)
   ON_COMMAND(ID_ACTION_OPENLOGFILE, OnActionOpenLogFile)
   ON_UPDATE_COMMAND_UI(ID_ACTION_OPENLOGFILE, OnUpdateActionOpenLogFile)
   ON_COMMAND(ID_ACTION_PAUSEDISPLAY, OnActionPauseDisplay)
   ON_UPDATE_COMMAND_UI(ID_ACTION_PAUSEDISPLAY, OnUpdateActionPauseDisplay)
   ON_COMMAND(ID_ACTION_SETSERIALPORT, OnActionSetSerialPort)
   ON_COMMAND(ID_ACTION_SWITCHTOUSER1PROTOCOL, OnActionSwitchtoUser1Protocol)
   ON_COMMAND(ID_NAVIGATION_MODECONTROL, OnNavigationModeControl)
   ON_UPDATE_COMMAND_UI(ID_ACTION_SETSERIALPORT, OnUpdateActionSetSerialPort)
   ON_UPDATE_COMMAND_UI(ID_ACTION_NMEA_DISABLE, OnUpdateActionNmeaDisable)
   ON_UPDATE_COMMAND_UI(ID_ACTION_NMEA_ENABLE, OnUpdateActionNmeaEnable)
   ON_UPDATE_COMMAND_UI(ID_ACTION_SWITCHTOUSER1PROTOCOL, OnUpdateActionSwitchToUser1Protocol)
   ON_UPDATE_COMMAND_UI(ID_NAVIGATION_MODECONTROL, OnUpdateNavigationModeControl)
   ON_COMMAND(ID_NAVIGATION_DOPCONTROL, OnNavigationDopControl)
   ON_UPDATE_COMMAND_UI(ID_NAVIGATION_DOPCONTROL, OnUpdateNavigationDopControl)
   ON_UPDATE_COMMAND_UI(ID_POLL_SWVERSION, OnUpdatePollSWVersion)
   ON_COMMAND(ID_POLL_SWVERSION, OnPollSWVersion)
   ON_UPDATE_COMMAND_UI(ID_POLL_CLOCKSTATUS, OnUpdatePollClockStatus)
   ON_COMMAND(ID_POLL_CLOCKSTATUS, OnPollClockStatus)
   ON_COMMAND(ID_NAVIGATION_DGPSCONTROL, OnNavigationDGPSControl)
   ON_UPDATE_COMMAND_UI(ID_NAVIGATION_DGPSCONTROL, OnUpdateNavigationDGPSControl)
   ON_COMMAND(ID_NAVIGATION_ELEVATIONMASK, OnNavigationElevationMask)
   ON_UPDATE_COMMAND_UI(ID_NAVIGATION_ELEVATIONMASK, OnUpdateNavigationElevationMask)
   ON_COMMAND(ID_NAVIGATION_POWERMASK, OnNavigationPowerMask)
   ON_UPDATE_COMMAND_UI(ID_NAVIGATION_POWERMASK, OnUpdateNavigationPowerMask)
   ON_COMMAND(ID_NAVIGATION_EDITINGRESIDUAL, OnNavigationEditingResidual)
   ON_UPDATE_COMMAND_UI(ID_NAVIGATION_EDITINGRESIDUAL, OnUpdateNavigationEditingResidual)
   ON_COMMAND(ID_NAVIGATION_STEADYSTATEDETECTION, OnNavigationSteadyStateDetection)
   ON_UPDATE_COMMAND_UI(ID_NAVIGATION_STEADYSTATEDETECTION, OnUpdateNavigationSteadyStateDetection)
   ON_COMMAND(ID_NAVIGATION_STATICNAVIGATION, OnNavigationStaticNavigation)
   ON_UPDATE_COMMAND_UI(ID_NAVIGATION_STATICNAVIGATION, OnUpdateNavigationStaticNavigation)
   ON_COMMAND(ID_VIEW_MESSAGES_ERROR, OnViewMessagesError)
   ON_UPDATE_COMMAND_UI(ID_VIEW_MESSAGES_ERROR, OnUpdateViewMessagesError)
   ON_MESSAGE(WM_USER_CLOSE_ERROR_VIEW, OnViewErrorClose)
   ON_COMMAND(ID_VIEW_MESSAGES_QUERYRESPONSE, OnViewMessagesQueryResponse)
   ON_UPDATE_COMMAND_UI(ID_VIEW_MESSAGES_QUERYRESPONSE, OnUpdateViewMessagesQueryResponse)
   ON_MESSAGE(WM_USER_CLOSE_QUERY_VIEW, OnViewQueryClose)
   ON_COMMAND(ID_ACTION_SETDGPSSERIALPORT, OnActionSetDGPSPort)
   ON_UPDATE_COMMAND_UI(ID_ACTION_SETDGPSSERIALPORT, OnUpdateActionSetDGPSPort)
   ON_COMMAND(ID_ACTION_SETALMANAC, OnActionSetAlmanac)
   ON_UPDATE_COMMAND_UI(ID_ACTION_SETALMANAC, OnUpdateActionSetAlmanac)
   ON_COMMAND(ID_POLL_ALMANAC, OnPollAlmanac)
   ON_UPDATE_COMMAND_UI(ID_POLL_ALMANAC, OnUpdatePollAlmanac)
   ON_UPDATE_COMMAND_UI(ID_ACTION_SETEPHEMERIS, OnUpdateActionSetEphemeris)
   ON_UPDATE_COMMAND_UI(ID_POLL_EPHEMERIS, OnUpdatePollEphemeris)
   ON_COMMAND(ID_POLL_EPHEMERIS, OnPollEphemeris)
   ON_COMMAND(ID_ACTION_SETEPHEMERIS, OnActionSetEphemeris)
   ON_COMMAND(ID_ACTION_SWITCH_OPMODE, OnActionSwitchOpMode)
   ON_UPDATE_COMMAND_UI(ID_ACTION_SWITCH_OPMODE, OnUpdateActionSwitchOpMode)
   ON_WM_CLOSE()
   //}}AFX_MSG_MAP
   ON_MESSAGE(WM_MSGQUE, OnSiRFMessage)
END_MESSAGE_MAP()

// toolbar buttons - IDs are command buttons
static UINT BASED_CODE buttons[] =
{
   // same order as in the bitmap 'toolbar.bmp'  Remember, you MUST count the separators for item index!
   ID_FILE_OPEN,                       // 0
   ID_SEPARATOR,                    // 1
   IDM_VIEW_SIGNAL,                    // 2
// IDM_VIEW_SIGNAL2,                   // 3
   ID_VIEW_RADAR,                      // 4
   ID_VIEW_MAP,                        // 5
   ID_SEPARATOR,                    // 6
   ID_ACTION_OPENDATASOURCE,           // 7
   ID_ACTION_PAUSEDISPLAY,             // 8
   ID_ACTION_OPENLOGFILE,              // 9
      ID_SEPARATOR,                    // 10
   ID_ACTION_INITIALIZEDATASOURCE      // 11
};

static UINT BASED_CODE indicators[] =
{
   ID_SEPARATOR,           // status line indicator
   ID_INDICATOR_SCRL
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction




CMainFrame::CMainFrame()
{
   int ix;

   m_bAppExitting = FALSE;
   m_bDontSaveWindowSettings=FALSE;

   for(ix=0; ix<CWinSiRFDemoApp::VIEW_COUNT; ix++)
   {
      m_apwndView[ix] = NULL;
   }
   m_pdoc = NULL;
   m_bSrcOpen = FALSE;
   m_bLogOpen = FALSE;
   m_bPauseDisplay = FALSE;

   for(ix = 0; ix < MAX_TIMERS; ix++)
   {
      m_aTimer[ix].m_nTimerID = 0;
   }

   ASSERT(CWinSiRFDemoApp::VIEW_COUNT <= 99);   // If this asserts, adjust the following lines (and the .h) higher
   memset(m_szOpenViews, '0', 99);
   m_szOpenViews[99] = 0;
}

CMainFrame::~CMainFrame()
{
   int ix;

    if (m_pHWndRcvr)
    {
        while (m_pHWndRcvr->messagesAvailable ())
        {
            CMessage * pMsg;
            pMsg = m_pHWndRcvr->receive ();
            m_pHWndRcvr->release (pMsg);
        }
        delete m_pHWndRcvr;
        m_pHWndRcvr = 0;
    }

   for (ix=0; ix < MAX_TIMERS; ix++)
   {
      if (m_aTimer[ix].m_nTimerID != 0)
      {
         StopTimer(ix);
      }
   }

   if (m_pdoc)
        delete m_pdoc;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
   cs.style &= ~(LONG)FWS_ADDTOTITLE;
   return CMDIFrameWnd::PreCreateWindow(cs);
}

BOOL CMainFrame::GotView()
{
   BOOL bRet = FALSE;
   int ix;

   for (ix=0; ix<CWinSiRFDemoApp::VIEW_COUNT; ix++)
   {
      bRet = bRet || (m_apwndView[ix] != NULL);
   }

   return bRet;
}

UINT CMainFrame::StartTimer(UINT nMsg, CTimerClient* pClient, int nDelay)
{
   int ix = -1;
   BOOL bFound = FALSE;

   while (ix < MAX_TIMERS && !bFound)
   {
      ix++;
      bFound = m_aTimer[ix].m_nTimerID == 0;
   }
   ASSERT (bFound);

   if(bFound)
   {
      m_aTimer[ix].m_nMsg = nMsg;
      m_aTimer[ix].m_pClient = pClient;
      m_aTimer[ix].m_nTimerID = SetTimer(ix+1, nDelay, NULL);
      ASSERT(m_aTimer[ix].m_nTimerID >0);
      TRACE("CMainfrm::StartTimer: ix=%d m_nTimerID=%d\n",ix,m_aTimer[ix].m_nTimerID);
      return ix+1;
   }
   else
      return 0;
}

UINT CMainFrame::StopTimer(int nTimer)
{
   if(nTimer<=0)
   {
      ASSERT(0);
      return 0;
   }
   

   if (m_aTimer[nTimer-1].m_nTimerID != 0)
   {
      TRACE("CMainfrm::StopTimer: ix=%d m_nTimerID=%d\n",nTimer-1,m_aTimer[nTimer-1].m_nTimerID);
      KillTimer(m_aTimer[nTimer-1].m_nTimerID);
      m_aTimer[nTimer-1].m_nTimerID = 0;
   }
   return 0;
}  

   static char *viewNames[] = {
      "VIEW_SIGNAL", "VIEW_SIGNAL2", "VIEW_RADAR", 
         "VIEW_MAP", "VIEW_MSG_NAVIGATION", "VIEW_DEBUGDATAWINDOW", 
         "VIEW_QUERY", "VIEW_ERROR", "VIEW_COUNT"};
void CMainFrame::SaveOpenViews()
{
   TRACE("In CMainFrame.SaveOpenViews: ");
   for(int ix=0; ix<CWinSiRFDemoApp::VIEW_COUNT; ix++)
   {
      if (m_szOpenViews[ix] == '1') {
         TRACE("%s ", viewNames[ix]);
      }
   }
   TRACE("\n");

   AfxGetApp()->WriteProfileString("Main", "OpenViews", m_szOpenViews);
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
   CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
   CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnTimer(UINT nIDEvent)
{
   int ix = -1;
   BOOL bFound = FALSE;

   while (ix < MAX_TIMERS && !bFound)
   {
      ix++;
      bFound = nIDEvent == m_aTimer[ix].m_nTimerID;
   }

   if (!bFound)
   {
      CMDIFrameWnd::OnTimer(nIDEvent);
   }
   else
   {
      //if((m_aTimer[ix].m_pClient)!=NULL/* src is open */)
      
      // need to validate that client is still alive ??????
      
      if(m_bSrcOpen/* src is open */)
      {
         (m_aTimer[ix].m_pClient)->OnTimer(m_aTimer[ix].m_nMsg);
      }
      else
      {/* simply stop timer */
         StopTimer(ix);
      }
      
   }
}

void CMainFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
   static BOOL bFirst = TRUE;

   if (bFirst)
   {
      bFirst = FALSE;
      RestoreWindowPlacement(this, "Main");
   }

   CMDIFrameWnd::OnShowWindow(bShow, nStatus);

   //StartTimer();
}

BOOL CMainFrame::GetConfigParameters(CString configFile)
{
   CStdioFile f;
   BOOL bSuccess = f.Open (
                            configFile, 
                            CFile::modeRead
                     );

   if (!bSuccess) return FALSE;

   CString parmName;
   CString parmValue;
   CString str;

   CString dataSource = "";
   int baudRate = -1;
   CString port = "";
   CString fileName = "";
   //
   // Parse all parameters
   while(true) {
      if (!f.ReadString(str)) break;
      if (str == "") continue;
      if (str.GetAt(0) == '#') continue;
      int iPos=str.Find(_T('='));
      if (iPos < 0) continue;
      parmName = str.Left(iPos);
      parmValue = str.Mid(iPos+1, str.GetLength()-1);
      parmName.TrimLeft();
      parmName.TrimRight();
      parmValue.TrimLeft();
      parmValue.TrimRight();
      parmName.MakeUpper();
      TRACE("Param: %s = %s\n", parmName, parmValue);
      if (parmName == "DATASOURCE") {
         dataSource = parmValue;
      }
      else if (parmName == "BAUDRATE") {
         sscanf((const char *) parmValue, "%d", &baudRate);
      }
      else if (parmName == "PORT") {
         port = parmValue;
      }
      else if (parmName == "FILENAME") {
         fileName = parmValue;
      }
   }
   f.Close();
   //
   // Now makesure we have all relavant parameters
   if (dataSource == "File") {
      if (fileName == "") return FALSE;
      TRACE("Setting DataSource - file: %s\n", fileName);
      CPreferences::Get().SetDataSourceFile(fileName);
   }
   else if (dataSource == "Serial") {
      if (baudRate == -1 || port=="") return FALSE;
      TRACE("Setting DataSource - serialport %s, baudRate = %d\n", port, baudRate);
      CPreferences::Get().SetDataSourcePort(port, baudRate);
   }
   else {
      return FALSE;     // No dataSource - Error parsing file
   }
   return TRUE;
}

void CMainFrame::OpenConfiguration(CString configFile)
{
   BOOL showDataSourceDlg=TRUE;

   if (configFile != "") {
      if (GetConfigParameters(configFile)) {
         showDataSourceDlg = FALSE;    // We have all our parms
      }
      else {
         AfxMessageBox("Error in reading configFile: " + configFile);
      }
   }
   if (showDataSourceDlg) {
      CDlgDSS dlg;
      if (dlg.DoModal() != IDOK) return;
   }

   // Close all the views to reset the doc and everything else
   if (m_pdoc)
   {
      if (GotView())
      {
         SaveOpenViews();
         m_bDontSaveWindowSettings=TRUE;     // Make sure we dont forget currently open views
         OnWindowCloseAll();
         DoEvents();
         m_bDontSaveWindowSettings=FALSE;
      }
   }

   // Reopen the doc
   OpenDoc();
   theApp.UpdateTitleBar();

   // Restore desktop
   char szOpenViews[100];
   int ix;
   strcpy(szOpenViews, theApp.GetProfileString("Main", "OpenViews"));
   for (ix = 0; ix < CWinSiRFDemoApp::VIEW_COUNT; ix++)
   {
      if (szOpenViews[ix] == '1')
      {
         ShowView(ix);
      }
   }
}

void CMainFrame::OnFileOpen()
{
   OpenConfiguration("");
}

void CMainFrame::OpenDoc()
{
   if (m_pdoc)
   {
      theApp.m_apDocTemplate[CWinSiRFDemoApp::VIEW_SIGNAL]->RemoveDocument(m_pdoc);
      delete m_pdoc;
      m_pdoc = NULL;
   }
   m_pdoc = (CFeedDoc*)(theApp.m_apDocTemplate[CWinSiRFDemoApp::VIEW_SIGNAL]->CreateNewDocument());
   TRACE("Created a CFeedDoc.\n");
}

LONG CMainFrame::OnDocClose(UINT nFoo, LONG lFoo)
{
   int ix;

   TRACE("In CMainFrame::OnDocClose\n");
   for(ix=0; ix<CWinSiRFDemoApp::VIEW_COUNT; ix++)
   {
      m_apwndView[ix] = NULL;
   }
   
   if(m_bSrcOpen)
   {
      OnActionOpenDataSource();
   }

   if(m_bLogOpen)
   {
      OnActionOpenLogFile();
   }     
   
   if (m_pdoc)
   {
      // delete m_pdoc;    // Note: Now document deleted by FeedDoc's OnCloseDocument
      m_pdoc = NULL;
   }
   return 0L;
}

LONG CMainFrame::OnUpdateOrientation(UINT nOrientation, LONG lFoo)
{
   theApp.PostMessageToAllDocuments(WM_USER_UPDATE_PREFS);
   return 0L;
}

LONG CMainFrame::OnDataReceived(UINT nOrientation, LONG lFoo)
{
   UINT  nID;
   UINT  nStyle;
   int   nImage;
   int   nOldImage;
   int   nNewImage;
   static BOOL bOnOff = FALSE;
#ifdef BLINKING_STAR
   static BOOL bCaptionStar = theApp.m_strWindowsVersion >= "3.95";
#else
   static BOOL bCaptionStar = FALSE;
#endif
   if (m_bPauseDisplay)
   {
      nOldImage = TOOLBAR_BITMAP_PAUSE_BLACK;
      nNewImage = TOOLBAR_BITMAP_PAUSE_RED;

      m_wndToolBar.GetButtonInfo(TOOLBAR_ITEM_PAUSE_INDEX, nID, nStyle, nImage);
      nImage = (nImage == nOldImage ? nNewImage : nOldImage);
      m_wndToolBar.SetButtonInfo(TOOLBAR_ITEM_PAUSE_INDEX, nID, nStyle, nImage);
   }

   if (bCaptionStar)
   {
      bOnOff = !bOnOff;
      /*
      if (bOnOff)
      {
         SetWindowText("SiRFstar Demo *");
      }
      else
      {
         SetWindowText("SiRFstar Demo");
      }
      */
   }

   return 0L;
}

LONG CMainFrame::OnMeasClosed(UINT nOrientation, LONG lFoo)
{
   if (m_bSrcOpen)
   {
      OnActionOpenDataSource();
   }
   return 0L;
}

void CMainFrame::CanKillDoc()
{
   if (!GotView())
   {
      if (m_pdoc)
      {
         delete m_pdoc;
         m_pdoc = NULL;
      }
   }
}

void CMainFrame::OnUpdateWindowCloseAll(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(GotView());
}

void CMainFrame::OnWindowCloseAll()
{
   POSITION pos;
   CView* pView;

   if (m_pdoc)
   {
      for (pos = m_pdoc->GetFirstViewPosition(); pos != NULL; )
      {
         pView = m_pdoc->GetNextView(pos);
         pView->GetParentFrame()->SendMessage(WM_CLOSE);
      }
      //m_pdoc->OnCloseDocument();     // Not closing the views!
      //theApp.m_apDocTemplate[CWinSiRFDemoApp::VIEW_SIGNAL]->RemoveDocument(m_pdoc);
      //delete m_pdoc;
      m_pdoc = NULL;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////////
// View Window Processing

void CMainFrame::ShowView(int nView, UINT mode)
{
   if (m_bDontSaveWindowSettings) return;

   TRACE("In CMainFrame::ShowView: %s\n", viewNames[nView]);
   if (!m_apwndView[nView])
   {
      if (!m_pdoc) 
      {
         OpenDoc();
      }
      m_apwndView[nView] = theApp.m_apDocTemplate[nView]->CreateNewFrame(m_pdoc, NULL);
      if(m_apwndView[nView])
      {
         theApp.m_apDocTemplate[nView]->InitialUpdateFrame(m_apwndView[nView], m_pdoc, TRUE);
         m_szOpenViews[nView] = '1';
      }
   }
   else
   {
      if (m_apwndView[nView]->IsWindowVisible() && (mode==CMainFrame::VIEW_AUTO))
      {
         m_apwndView[nView]->ShowWindow(SW_HIDE);
         m_szOpenViews[nView] = '0';
      }
      else
      {
         m_apwndView[nView]->ShowWindow(SW_RESTORE);
         m_apwndView[nView]->ActivateFrame();
         m_szOpenViews[nView] = '1';
      }  
   }     

// else
// {
//    m_apwndView[nView]->SendMessage(WM_CLOSE);
//    m_apwndView[nView] = NULL;
// }
}

void CMainFrame::UpdateViewControls(int nView, CCmdUI* pCmdUI, BOOL bEnable /*TRUE*/)
{
   BOOL bCheck = FALSE;

   pCmdUI->Enable(bEnable); //GotSource());
   if (m_apwndView[nView] != NULL)
   {
      bCheck = m_apwndView[nView]->IsWindowVisible();
   }
   pCmdUI->SetCheck(bCheck);  
// pCmdUI->SetCheck(m_apwndView[nView]-> != NULL); 
}

LONG CMainFrame::CloseView(int nView)
{
   if (m_bDontSaveWindowSettings) return 0L;

   TRACE("In CMainFrame::CloseView %s\n", viewNames[nView]);
   m_apwndView[nView] = NULL;
   m_szOpenViews[nView] = '0';
// CanKillDoc();
   return 0L;
}
                                                          
////////////////////////////

void CMainFrame::OnViewSignal()
{
   ShowView(CWinSiRFDemoApp::VIEW_SIGNAL);
}

void CMainFrame::OnUpdateViewSignal(CCmdUI* pCmdUI)
{
   UpdateViewControls(CWinSiRFDemoApp::VIEW_SIGNAL, pCmdUI);
}

LONG CMainFrame::OnViewSignalClose(UINT nFoo, LONG lFoo)
{
   return CloseView(CWinSiRFDemoApp::VIEW_SIGNAL);
}

////////////////////////////

void CMainFrame::OnViewSignal2()
{
   ShowView(CWinSiRFDemoApp::VIEW_SIGNAL2);
}

void CMainFrame::OnUpdateViewSignal2(CCmdUI* pCmdUI)
{
   UpdateViewControls(CWinSiRFDemoApp::VIEW_SIGNAL2, pCmdUI);
}

LONG CMainFrame::OnViewSignalClose2(UINT nFoo, LONG lFoo)
{
   return CloseView(CWinSiRFDemoApp::VIEW_SIGNAL2);
}

////////////////////////////

void CMainFrame::OnViewRadar()
{
   ShowView(CWinSiRFDemoApp::VIEW_RADAR);
}

void CMainFrame::OnUpdateViewRadar(CCmdUI* pCmdUI)
{
   UpdateViewControls(CWinSiRFDemoApp::VIEW_RADAR, pCmdUI);
}

LONG CMainFrame::OnViewRadarClose(UINT nFoo, LONG lFoo)
{
   return CloseView(CWinSiRFDemoApp::VIEW_RADAR);
}

////////////////////////////

void CMainFrame::OnViewMap()
{
   ShowView(CWinSiRFDemoApp::VIEW_MAP);
}

void CMainFrame::OnUpdateViewMap(CCmdUI* pCmdUI)
{
   UpdateViewControls(CWinSiRFDemoApp::VIEW_MAP, pCmdUI, TRUE); //m_bViewMapEnable);
}

LONG CMainFrame::OnViewMapClose(UINT nFoo, LONG lFoo)
{
   return CloseView(CWinSiRFDemoApp::VIEW_MAP);
}

////////////////////////////

void CMainFrame::OnViewDebugDataWindow()
{
   ShowView(CWinSiRFDemoApp::VIEW_DEBUGDATAWINDOW);
}

////////////////////////////

void CMainFrame::OnUpdateViewDebugDataWindow(CCmdUI* pCmdUI)
{
   UpdateViewControls(CWinSiRFDemoApp::VIEW_DEBUGDATAWINDOW, pCmdUI);
}

////////////////////////////

LONG CMainFrame::OnViewDebugDataWindowClose(UINT nFoo, LONG lFoo)
{                            
   return CloseView(CWinSiRFDemoApp::VIEW_DEBUGDATAWINDOW);
}

///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnViewNavMsg()
{
   ShowView(CWinSiRFDemoApp::VIEW_MSG_NAVIGATION);
}

void CMainFrame::OnViewMessagesError()
{
   ShowView(CWinSiRFDemoApp::VIEW_ERROR);
}

LONG CMainFrame::OnViewErrorClose(UINT nFoo, LONG lFoo)
{                            
   return CloseView(CWinSiRFDemoApp::VIEW_ERROR);
}


void CMainFrame::OnViewMessagesQueryResponse()
{
   ShowView(CWinSiRFDemoApp::VIEW_QUERY);
}

LONG CMainFrame::OnViewQueryClose(UINT nFoo, LONG lFoo)
{                            
   return CloseView(CWinSiRFDemoApp::VIEW_QUERY);
}


///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnUpdateViewNavMsg(CCmdUI* pCmdUI)
{
   UpdateViewControls(CWinSiRFDemoApp::VIEW_MSG_NAVIGATION, pCmdUI);
}


void CMainFrame::OnUpdateViewMessagesError(CCmdUI* pCmdUI)
{
   UpdateViewControls(CWinSiRFDemoApp::VIEW_ERROR, pCmdUI);
}


void CMainFrame::OnUpdateViewMessagesQueryResponse(CCmdUI* pCmdUI)
{
   UpdateViewControls(CWinSiRFDemoApp::VIEW_QUERY, pCmdUI);
}

///////////////////////////////////////////////////////////////////////////////

LONG CMainFrame::OnViewNavMsgClose(UINT nFoo, LONG lFoo)
{
   return CloseView(CWinSiRFDemoApp::VIEW_MSG_NAVIGATION);
}

///////////////////////////////////////////////////////////////////////////////////

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
   if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
      return -1;

/* if (!m_wndToolBar.Create(this) ||
      !m_wndToolBar.LoadBitmap(IDR_MAINFRAME) ||
      !m_wndToolBar.SetButtons(buttons,
        sizeof(buttons)/sizeof(UINT)))
*/
   if (!m_wndToolBar.Create(this) ||
      !m_wndToolBar.LoadToolBar(IDR_MAINFRAME) )
   {
      TRACE("Failed to create toolbar\n");
      return -1;      // fail to create
   }
   m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
      CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
   m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
   EnableDocking(CBRS_ALIGN_ANY);
   DockControlBar(&m_wndToolBar);

   if (!m_wndStatusBar.Create(this) ||
      !m_wndStatusBar.SetIndicators(indicators,
        sizeof(indicators)/sizeof(UINT)))
   {
      TRACE("Failed to create status bar\n");
      return -1;      // fail to create
   }

   //
   // Now create our window receiver (to get SiRFProtocol messages)
   m_pHWndRcvr = new CHWndReceiver (m_hWnd);
   return 0;
}

///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnFilePreferences()
{
   CDlgPreferences dlg;
   dlg.DoModal();
}

///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnDestroy()
{
   // Save this window's location
   SaveWindowPlacement(this, "Main");

   CMDIFrameWnd::OnDestroy();
}

///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnActionInitializeDataSource()
{
   ASSERT(m_bSrcOpen);

   CDlgRI dlg;
   if (dlg.DoModal() == IDOK)
   {
      m_pdoc->ReceiverInit();
   }
}

///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnActionSendNMEAOn()
{
//   if(m_bSrcOpen)
//    m_pdoc->SendNMEA(NMEA_ON);
}

///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnActionSendNMEAOff()
{
 //  if(m_bSrcOpen)
//    m_pdoc->SendNMEA(NMEA_OFF);
}

///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnActionSendNMEAEnable()
{
//   if(m_bSrcOpen)
//    m_pdoc->SendNMEA(NMEA_ENABLE);
   ASSERT(0);  // Should never be called
}

///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnActionSendNMEADisable()
{
 //  if(m_bSrcOpen)
//    m_pdoc->SendNMEA(NMEA_DISABLE);
   ASSERT(0);  // Should never be called
}

///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnActionOpenDataSource()
{
   int nResponse;

   if (!m_bSrcOpen)
   {
      if (!CNavController::get().IsLoggingOn())
      {
         // only IFF no cmd line param /noask ask about log file
         CString str = theApp.m_lpCmdLine;
         str.MakeLower();
         if(!(str.Find("/noask") >= 0 || str.Find("-noask") >= 0))
            nResponse = AfxMessageBox("Do you want to open a log file?", MB_YESNOCANCEL+MB_ICONQUESTION);
      }
      else
      {
         nResponse = AfxMessageBox("Do you want to open a different log file?", MB_YESNOCANCEL+MB_ICONQUESTION);
      }

      if (nResponse == IDYES)
      {
         if (CNavController::get().ShowLoggingSettings() == TRUE)
         {
            CNavController::get().StartLogging();
         }
      }
      else if (nResponse == IDCANCEL)
      {
         return;
      }

      if (m_pdoc == NULL) {
         // This should not be allowed. Lets try creating a new doc
         OpenDoc();
      }
      m_bSrcOpen = m_pdoc->StartFeed();
      if (!m_bSrcOpen) {
         AfxMessageBox("Error opening dataSource");
      }
   }
   else
   {
      if (CNavController::get().IsLoggingOn())
      {
         nResponse = AfxMessageBox("You are currently logging data.  Do you wish to close the log file?", MB_YESNOCANCEL+MB_ICONQUESTION);

         if (nResponse == IDYES)
         {
            CNavController::get().StopLogging();
         }
         else if (nResponse == IDCANCEL)
         {
            return;
         }
      }

      m_bSrcOpen = !m_bSrcOpen;

      if (m_bPauseDisplay)
      {
         OnActionPauseDisplay();
      }
      m_pdoc->StopFeed();

      bCleanPortOpening = FALSE; // srdjan 10/11/96

      #ifdef BLINKING_STAR
//       SetWindowText("SiRFstar Demo");
      #endif
   }

   UINT  nID;
   UINT  nStyle;
   int   nImage;

   m_wndToolBar.GetButtonInfo(TOOLBAR_ITEM_CONNECTION_INDEX, nID, nStyle, nImage);
   if (m_bSrcOpen)
   {
      m_wndToolBar.SetButtonInfo(TOOLBAR_ITEM_CONNECTION_INDEX, nID, nStyle, TOOLBAR_BITMAP_COMM_CONNECTED);
   }
   else
   {
      m_wndToolBar.SetButtonInfo(TOOLBAR_ITEM_CONNECTION_INDEX, nID, nStyle, TOOLBAR_BITMAP_COMM_UNCONNECTED);
   }
}


///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnActionOpenLogFile()
{
   if (!CNavController::get().IsLoggingOn())
   {
      if (CNavController::get().ShowLoggingSettings() == TRUE)
      {
         CNavController::get().StartLogging();
      }
   }
   else
   {
      CNavController::get().StopLogging();
   }
}

///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnActionPauseDisplay()
{
// ASSERT(m_bSrcOpen);
   m_bPauseDisplay = !m_bPauseDisplay;
   m_pdoc->PauseFeed(m_bPauseDisplay);

   // Reset pause toolbar to black
   if (!m_bPauseDisplay)
   {
      UINT  nID;
      UINT  nStyle;
      int   nImage;

      m_wndToolBar.GetButtonInfo(TOOLBAR_ITEM_PAUSE_INDEX, nID, nStyle, nImage);
      m_wndToolBar.SetButtonInfo(TOOLBAR_ITEM_PAUSE_INDEX, nID, nStyle, TOOLBAR_BITMAP_PAUSE_BLACK);
   }
}

///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnUpdateActionPauseDisplay(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
   pCmdUI->SetCheck(m_bPauseDisplay);
}

///////////////////////////////////////////////////////////////////////////////
void CMainFrame::OnActionSetAlmanac()
{
   if(m_bSrcOpen)
   {
      CString strAlmanac=GetAlmanacFileBrowse(FILE_OPEN);
      if(!strAlmanac.IsEmpty())
         m_pdoc->SetAlmanac(strAlmanac);
   }  
}


///////////////////////////////////////////////////////////////////////////////
void CMainFrame::OnActionSetEphemeris()
{
   if(m_bSrcOpen)
   {
      CString strEph=GetEphemerisFileBrowse(FILE_OPEN);
      if(!strEph.IsEmpty())
         m_pdoc->SetEphemeris(strEph);
   }  
}

///////////////////////////////////////////////////////////////////////////////
void CMainFrame::OnActionSetSerialPort()
{
   if(m_bSrcOpen)
   {
      m_pdoc->SetSerialPort();
   }  
}

///////////////////////////////////////////////////////////////////////////////
void CMainFrame::OnActionSetDGPSPort()
{
      if(m_bSrcOpen)
         m_pdoc->SetDGPSPort();
   
}

///////////////////////////////////////////////////////////////////////////////
void CMainFrame::OnActionSwitchOpMode()
{
      if(m_bSrcOpen)
         m_pdoc->SwitchOpMode();
}

///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnActionSwitchtoUser1Protocol()
{
      if(m_bSrcOpen)
         m_pdoc->SetProtocolUser1();
}

/////////// Navigation ///////////////////////////////////////////////////////

void CMainFrame::OnNavigationModeControl()
{
      if(m_bSrcOpen)
         m_pdoc->SetNavMode();
}

void CMainFrame::OnNavigationDopControl()
{
   if(m_bSrcOpen)
      m_pdoc->SetDOP();
}

void CMainFrame::OnNavigationDGPSControl()
{
   if(m_bSrcOpen)
      m_pdoc->SetDGPS();
}

void CMainFrame::OnNavigationElevationMask()
{
   if(m_bSrcOpen)
      m_pdoc->SetElevationMask();
}


void CMainFrame::OnNavigationPowerMask()
{
   if(m_bSrcOpen)
      m_pdoc->SetPowerMask();
}

void CMainFrame::OnNavigationEditingResidual()
{
   if(m_bSrcOpen)
      m_pdoc->SetEditingResidual();
}


void CMainFrame::OnNavigationSteadyStateDetection()
{
   if(m_bSrcOpen)
      m_pdoc->SetSteadyStateDetection();  
}


void CMainFrame::OnNavigationStaticNavigation()
{
   if(m_bSrcOpen)
      m_pdoc->SetStaticNavigation();   
}

/////////// Poll ////////////////////////////////////////////////////////////
void CMainFrame::OnPollAlmanac()
{
   if(m_bSrcOpen)
   {
      CString strAlmanac=GetAlmanacFileBrowse(FILE_SAVE);
      if(!strAlmanac.IsEmpty())
      {
         theApp.m_strAlmFileName=strAlmanac;   // save the file name
         m_pdoc->PollAlmanac();
      }
      ShowView(CWinSiRFDemoApp::VIEW_QUERY, VIEW_ON);
   }   
}

void CMainFrame::OnPollSWVersion()
{
   if(m_bSrcOpen)  m_pdoc->PollSWVersion();
   ShowView(CWinSiRFDemoApp::VIEW_QUERY, VIEW_ON);
}

void CMainFrame::OnPollClockStatus()
{
   if(m_bSrcOpen)  m_pdoc->PollClockStatus();
   ShowView(CWinSiRFDemoApp::VIEW_QUERY, VIEW_ON);
}

void CMainFrame::OnPollEphemeris()
{
   if(m_bSrcOpen)
   {
      CString strEphemeris=GetEphemerisFileBrowse(FILE_SAVE);
      if(!strEphemeris.IsEmpty())
      {
         theApp.m_strEphFileName=strEphemeris;   // save the file name
         m_pdoc->PollEphemeris();
      }
      ShowView(CWinSiRFDemoApp::VIEW_QUERY, VIEW_ON);
   }   
}

///////////////////////////////////////////////////////////////////////////////
LONG CMainFrame::OnStoreEphemeris(UINT nOrientation, LONG lFoo)
{
   BOOL bStop=FALSE;
   time_t      time_day;
   CString strDesc;

   CStdioFile fEphemeris;
   BOOL bOpen = fEphemeris.Open(theApp.m_strEphFileName, 
            CFile::modeWrite | CFile::modeCreate);
   char acBuf[500];
   
   // write in 5 line file description
   strDesc ="//\n// Ephemeris Collection Time(UTC): ";
   time (&time_day);             /* Gets current system time and date */
   strDesc+=asctime(gmtime (&time_day));/* Converts to UTC time and date  */
   for(int nLin=2;nLin<5;nLin++)
   {
      strDesc+="//\n";
   }        
   fEphemeris.WriteString(strDesc);
   
   // data is MAX_SVID_CNT lines of 3*15 CSV UINT16
   for (int ix=0; ix < MAX_SVID_CNT; ix++)
   {
      int bcnt;
      for(int z=0;z<(3*15) && !bStop;z++)
      {
         bcnt=sprintf(acBuf,"%u",aAllEphData[ix*(3*15)+z]);
         if(z!=((3*15)-1))  sprintf(acBuf+bcnt,", ");
         fEphemeris.WriteString(acBuf);
      }
      fEphemeris.WriteString("\n");
   }

   fEphemeris.Close();
   theApp.MessageBox("Ephemeris Data Written to:\n\n"+theApp.m_strEphFileName,MB_ICONINFORMATION);
   //theApp.MessageBox("Ephemeris Data Rec'd",MB_ICONINFORMATION);         
   theApp.m_strEphFileName="";
   return LONG(0);
}   

///////////////////////////////////////////////////////////////////////////////
LONG CMainFrame::OnStoreAlmanac(UINT nOrientation, LONG lFoo)
{
   BOOL bStop=FALSE;
   time_t      time_day;
   CString strDesc;
   
   CStdioFile fAlmanac;
   BOOL bOpen = fAlmanac.Open(theApp.m_strAlmFileName, 
            CFile::modeWrite | CFile::modeCreate);

   char acBuf[400];
   
   // write in 5 line file description
   strDesc ="//\n// Almanac Collection Time(UTC): ";
   time (&time_day);             /* Gets current system time and date */
   strDesc+=asctime(gmtime (&time_day));/* Converts to UTC time and date  */
   for(int nLin=2;nLin<5;nLin++)
   {
      strDesc+="//\n";
   }        
   fAlmanac.WriteString(strDesc);
   
   // data is MAX_SVID_CNT lines of ALMANAC_ENTRY CSV integers
   for (int ix=0; ix < MAX_SVID_CNT; ix++)
   {
      int bcnt;
      for(int z=0;z<ALMANAC_ENTRY && !bStop;z++)
      {
         bcnt=sprintf(acBuf,"%d",aAllAlmData[ix*ALMANAC_ENTRY+z]);
         if(z!=(ALMANAC_ENTRY-1))  sprintf(acBuf+bcnt,", ");
         fAlmanac.WriteString(acBuf);
      }
      fAlmanac.WriteString("\n");
   }

   fAlmanac.Close();
   theApp.MessageBox("Almanac Data Written to:\n\n"+theApp.m_strAlmFileName,MB_ICONINFORMATION);
   theApp.m_strAlmFileName="";
   return LONG(0);
}   

///////////////////////////////////////////////////////////////////////////////
//
//      UI Update Functions.  Grey out unavailiable menu items 
//
///////////////////////////////////////////////////////////////////////////////

/////////// Action ////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void CMainFrame::OnUpdateActionInitializeDataSource(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdateActionOpenDataSource(CCmdUI* pCmdUI)
{
   pCmdUI->SetCheck(m_bSrcOpen);
}

void CMainFrame::OnUpdateActionOpenLogFile(CCmdUI* pCmdUI)
{
   pCmdUI->SetCheck(CNavController::get().IsLoggingOn());
}

void CMainFrame::OnUpdateActionSetSerialPort(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdateActionNmeaDisable(CCmdUI* pCmdUI)
{
// pCmdUI->Enable(m_bSrcOpen);
   pCmdUI->Enable(FALSE);  // Not supported
}

void CMainFrame::OnUpdateActionNmeaEnable(CCmdUI* pCmdUI)
{
// pCmdUI->Enable(m_bSrcOpen);
   pCmdUI->Enable(FALSE);  // Not supported
}

void CMainFrame::OnUpdateActionSwitchToUser1Protocol(CCmdUI* pCmdUI)
{
// pCmdUI->Enable(m_bSrcOpen);
   pCmdUI->Enable(FALSE);  // Not supported
}

/////////// Navigation ///////////////////////////////////////////////////////

void CMainFrame::OnUpdateNavigationModeControl(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}


void CMainFrame::OnUpdateNavigationDopControl(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdateNavigationDGPSControl(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdateNavigationElevationMask(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdateNavigationPowerMask(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdateNavigationEditingResidual(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdateNavigationSteadyStateDetection(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdateNavigationStaticNavigation(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(FALSE);//m_bSrcOpen);
}


/////////// Poll ////////////////////////////////////////////////////////////

void CMainFrame::OnUpdatePollSWVersion(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdatePollClockStatus(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdateActionSetDGPSPort(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdateActionSwitchOpMode(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdateActionSetAlmanac(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdatePollAlmanac(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdateActionSetEphemeris(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}

void CMainFrame::OnUpdatePollEphemeris(CCmdUI* pCmdUI)
{
   pCmdUI->Enable(m_bSrcOpen);
}


LRESULT CMainFrame::OnSiRFMessage(WPARAM wParam, LPARAM lParam)
{
   CHWndReceiver *pMessageReceiver = reinterpret_cast<CHWndReceiver *> (lParam);

   while (pMessageReceiver->messagesAvailable ())
    {
      CMessage * pMsg;

# if 0
        TRACE ("mf: avail:%d, free:%d\n",
               pMessageReceiver->messagesAvailable (),
               pMessageReceiver->messagesFree ()
              );
                
# endif
      pMsg = pMessageReceiver->receive ();
        switch (pMsg->getId ())
        {
        case EMT_Unallocated:
            break;
        case EMT_NoMessage:
            break;
        case EMT_Control:
            break;
        case EMT_Data:
            HandleMessage (pMsg);
            break;
        case EMT_DataContinuation:
            break;
        case EMT_CriticalData:
            break;
        case EMT_Debug:
            break;
        case EMT_Warn:
        {
            if (!m_bSrcOpen || m_bAppExitting)
            {
               return 1;
            }
            char buf[400];
            sprintf (buf, "Warning %s: %s\n",
                     strEProtocol ((EProtocol) pMsg->m_user),
                     (char *) pMsg->getBuffer ()
                    );
            AfxMessageBox (buf);
            break;
        }
        case EMT_Error:
        {
            if (!m_bSrcOpen || m_bAppExitting)
            {
                return 1;
            }
            if (pMsg->m_user == EP_Restart)
            {
                // tbd: Prakash put restart behavior here.
            }
            char buf[400];
            sprintf (buf, "Error %s: %s\n",
                     strEProtocol ((EProtocol) pMsg->m_user),
                     (char *) pMsg->getBuffer ()
                    );
            AfxMessageBox (buf);
            while (pMessageReceiver->messagesAvailable ())
                pMessageReceiver->release (pMessageReceiver->receive ());
         TRACE("Terminating Nav\n");
         m_pdoc->StopFeed();
         m_bSrcOpen = FALSE;        // to make sure toolbar is popped out
         TRACE("Nav off\n");
            break;
        }
        case EMT_Fatal:
            break;
        case Last_ENormalMessageType:
            break;
        case EMT_Terminate:
            break;
        default:
            break;
        }
      pMessageReceiver->release (pMsg);
   }
   return 1L;
}

void CMainFrame::HandleMessage (CMessage * pMsg)
{
    BYTE * pNavBuf = pMsg->getBuffer ();
    CNavData * pNavData = reinterpret_cast<CNavData *> (pNavBuf);
    
   CNavController::get().LogIncomingMessage(pNavData);
   if (m_bPauseDisplay) {
      return;        // Display paused - We continue logging messages, but donot update views
   }
   CNavMsgReceiver::NotifyViews (pNavData);
}

void CMainFrame::OnClose() 
{
   TRACE("In OnClose\n");
   m_bAppExitting = TRUE;
   CMDIFrameWnd::OnClose();
}
