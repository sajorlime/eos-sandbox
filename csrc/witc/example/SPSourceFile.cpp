
/***************************************************************************
 *
 *                          SiRF Technology, Inc. GPS Software
 *                                      Version 1.05
 *
 *      SiRF Technology, Inc. CONFIDENTIAL AND PROPRIETARY.  This source
 *      is the sole property of SiRF Technology, Inc.  Reproduction or
 *      utilization of this source in whole or in part is forbidden
 *      without the written consent of SiRF Technology, Inc.
 *
 ***************************************************************************
 *
 *              (c) SiRF Technology, Inc. 1998 -- All Rights Reserved
 *
 ***************************************************************************
 *
 * SPSourceFile.cpp
 *
 * .........................................................................
 *
 * $Workfile:$
 * $Revision:$
 *     $Date:$
 *
 * .........................................................................
 *
 * Implements SiRFProtocol read thread class.
 *
 * .........................................................................
 *
 *      $Log:$
 *
 *  Date     Description
 * ------   -----------
 *
 ***************************************************************************
 *
 * NOTES:
 *
 *
 * Classes/Types:
 *
 *
 * FUNCTIONS:
 *
 *
 ***************************************************************************/


# include <afxmt.h>
# include <windef.h>
# include "SPSourceFile.h"
# include "spframe.h"
# include "IGPSNavigation.h"

# include "MessagePacer.h"

CSPWriteThread::CSPWriteThread (
                                HANDLE hDest,
                                DWORD msSleep,
                                int id,
                                bool bContinuous
                               )
    :
     CThread (
              IDLE_PRIORITY_CLASS,
              THREAD_PRIORITY_LOWEST
             ),
      m_hDestination (hDest),
      m_pData (0),
      m_id (id),
      m_nData (0),
      //m_msSleep (1000),
      m_msSleep (msSleep),
      m_bContinuous (bContinuous)
{
    if ((int) hDest < 0)
        throw XException ("Could not open port\n");
}



CSPWriteThread::~CSPWriteThread ()
{
    terminate ();
}


bool CSPWriteThread::terminate (void)
{
    CThread::terminate (3000);
    if ((int) m_hDestination > 0)
    {
        CloseHandle (m_hDestination);
        m_hDestination = INVALID_HANDLE_VALUE;
    }
    return true;
}

int writeMessage (HANDLE dest, BYTE * pData, int len)
{
    BOOL r = 0;
    DWORD written;
# if 0
    TRACE ("writeMsg len: %d\n", len);
# endif
    r = WriteFile (dest, pData, len, &written, 0);
    if (len != written)
    {
# if 1
        TRACE ("len: %d != written: %d\n", len, written);
# endif
    }
    return written;
}


void CSPWriteThread::setDestination (HANDLE hDest)
{
    m_hDestination = hDest;
}

bool CSPWriteThread::start (BYTE * pData, int len)
{
    m_pData = pData;
    m_nData = len;
    return CThread::start ();
}

static int wtMsgNum = 0;

int CSPWriteThread::thread (void)
{
    int count = 0;
    BYTE * pData = m_pData;

# if 0
    TRACE ("m_hDestination: %x, m_msSleep:%d\n",
           m_hDestination,
           m_msSleep
          );
# endif
    while (m_run != CThread::ts_terminated)
    {
        int len = 0;
# if 0
        TRACE ("wthread ms sleep:%d\n", m_msSleep);
# endif
        sleep (m_msSleep);
        if (m_run == CThread::ts_terminated)
            goto outerLoopEnd;
        if (pData)
        {
            count = 0;
            while (m_run)
            {
                BYTE * pLen = pData + 3;
                len = *pLen;
                BYTE * pId = pData + 4;
# if 0
                TRACE ("wthread(%d) %d\n", wtMsgNum++, *pId);
# endif
                writeMessage (m_hDestination, pData, len + 8);
# if 0
                It seems to me that this sleep is not necessary
                as we should block on the write above!
                sleep (10); // give up the process add the time
# endif

                // 4 for prefix and 4 for postfix and length of msg
                pData += len + 4 + 4;
                if (pData >= (m_pData + m_nData))
                {
# if 0
                        TRACE ("End of Data\n");
# endif
                    if (m_bContinuous)
                    {
                        pData = m_pData;
                        break;
                    }
                    else
                    {
# if 1
                        TRACE ("Stop Data\n");
# endif
                        pData = 0;
                        m_run = CThread::ts_terminated;
                        break;
                    }
                }
                if ((m_id == *pId) || (m_id == -1))
                {
# if 0
                    if (m_id != -1)
                        TRACE ("wtid match: %x, ", *pId);
# endif
                    break;
                }
            }
        }
outerLoopEnd:;
    }
    TRACE ("CSPWriteThread exiting: %x\n", GetCurrentThreadId ());
    return 0;
}



bool CSPSourceFile::suspend (void)
{
    bool r = CSPSource::suspend ();
    if (m_pWriter)
        r &= m_pWriter->suspend ();
    return r;
}

bool CSPSourceFile::resume (void)
{
    bool r = CSPSource::resume ();
    if (m_pWriter)
        r &= m_pWriter->resume ();
    return r;
}

bool CSPSourceFile::terminate (void)
{
    bool r = m_pWriter->terminate ();
    r &= CSPSource::terminate ();
    return r;
}

bool CSPSourceFile::startWriter (BYTE * pData, int nData)
{
   m_pData = pData;
   m_nData = nData;
   if (m_run == CThread::ts_running)
       return m_pWriter->start (pData, nData);
   return false;
}

bool CSPSourceFile::start (void)
{
    bool r = CThread::start ();
    if (m_pData)
        r &= m_pWriter->start (m_pData, m_nData);
    return r;
}




CSPSourceFile::CSPSourceFile (CMessageReceiver * pMR,
                              HANDLE hSource,
                              HANDLE hDest,
                              DWORD ms,
                              bool continuous,
                              int waitId
                             )
    :
      CSPSource (pMR, hSource),
      m_msSleep (ms),
      m_pData (0),
      m_nData (0),
      m_bContinuous (continuous)
{
    m_pWriter = new CSPWriteThread (hDest,
                                    ms,
                                    waitId,
                                    m_bContinuous
                                   );
# if 0
    m_pMP = new CMessagePacer (pMR, m_pWriter);
# else
    m_pMP = pMR;
# endif
    TRACE ("SPSF.m_pMP:%x\n", m_pMP);
    m_pMR = m_pMP;
    TRACE ("m_pWriter:%x\n", m_pWriter);
}

CSPSourceFile::~CSPSourceFile ()
{
    terminate ();
    if (m_pData)
        delete m_pData;
    if (m_pWriter)
    {
        m_pWriter->terminate ();
        delete m_pWriter;
    }
//    if (m_pMP)
//        delete m_pMP;
}






