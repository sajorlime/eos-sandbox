
/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
*
    Message.h
*
    Contains defintion of a class that allow inter-thread communication.
*
*
   Date     Description
 --------   -----------
*
*
* NOTES:
    This file defines the CMessage and CMessgeReceiver classes which
    provide an inter-thread communicaiton mechanism.  It defines a
    method of controlling and filling messge buffers and the delivering
    them to the message receiver.  The interface is intended to be
    primarily asynchronous.
-
    This class could be adapted to inter-process communicaiton by using
    CSemaphore instead of a critical section.
-
    CMessageReceiver  is abstract, so to use the class one must first create
    a concrete class to use the interface, but the intended usage follows:
-
        Thread A                    Thread B
            Create Receiver.
            Pass to other
            theards
            on creation,
            or by some other
            method.
                                    Save pointer to thread.
                                    Wait for event.
                                    Fill m_user and messag data.
                                    pMsgQue->send (pMsg)
            pMsg = myMsgQue ()
            ... // use pMsg
            myMsgQue.relese (pMsg)
-
*
* Classes/Types:
    CMessage -- container for a message between threads.
    CMessageReceiver -- object that queues messsages and blocks reciever
    when no messages are availble.
-
    EMessageType -- messge type for internal message types.  The m_user
    field is used for private type information.
*
*/


# ifndef __MESSAGE_H__
# define __MESSAGE_H__

# include "Exception.h"
# include <afxmt.h>

/*
    EMessageType specifies the message values returned
    by a protocol.  
*/
enum EMessageType
{
    EMT_Unallocated = -1,
    EMT_NoMessage = 0,
    EMT_Control,
    EMT_Data,
    EMT_DataContinuation,
    EMT_CriticalData,
    EMT_Debug,
    EMT_Warn,
    EMT_Error,
    EMT_Fatal,
    Last_ENormalMessageType,
    EMT_Terminate = 0x7fff,
    Last_EMessageType
};

const char * strEMessageType (EMessageType t);



class CMessage
{
private:
    friend class CMessageReceiver;
    CMessageReceiver * m_pReceiver;
    CMessage (BYTE * buf, CMessageReceiver * pReceiver);
    EMessageType m_id;
    int m_length;
    BYTE * m_buffer;
    CMessage * operator& (void) {return this;}
public:
    int m_user;
    EMessageType setId (EMessageType id) {return m_id = id;}
    EMessageType getId (void) {return m_id;}
    int setLength (int len) {return m_length = len;}
    int getLength (void) {return m_length;}
    BYTE * getBuffer (void) {return m_buffer;}
    int setMessage (EMessageType id, int user, int len, const BYTE * pBuf);

    CMessage & operator= (CMessage &);
    ~CMessage ();
};

class CMessageDispatch
{
public:
    virtual bool processNoMessage (CMessage * pMsg)
    {
        // note that this can be overridden!
        throw XException ("Cannot dispatch \"EMT_NoMessage\" message type");
        return false;
    }
    virtual bool processControlMsg (CMessage * pMsg) = 0;
    virtual bool processDataMsg (CMessage * pMsg) = 0;
    virtual bool processDataContinuationMsg (CMessage * pMsg) = 0;
    virtual bool processCriticalDataMsg (CMessage * pMsg) = 0;
    virtual bool processDebugMsg (CMessage * pMsg) = 0;
    virtual bool processWarnMsg (CMessage * pMsg) = 0;
    virtual bool processErrorMsg (CMessage * pMsg) = 0;
    virtual bool processFatalMsg (CMessage * pMsg) = 0;
    virtual bool processTerminateMsg (CMessage * pMsg) = 0;
    virtual bool dispatchMsg (CMessage * pMsg)
    {
        switch (pMsg->getId ())
        {
        case EMT_NoMessage:
            return processNoMessage (pMsg);

        case EMT_Control:
            return processControlMsg (pMsg);

        case EMT_Data:
            return processDataMsg (pMsg);

        case EMT_DataContinuation:
            return processDataMsg (pMsg);

        case EMT_CriticalData:
            return processDataMsg (pMsg);

        case EMT_Debug:
            return processDebugMsg (pMsg);

        case EMT_Warn:
            return processWarnMsg (pMsg);

        case EMT_Error:
            return processErrorMsg (pMsg);

        case EMT_Fatal:
            return processFatalMsg (pMsg);

        case EMT_Terminate:
            return processTerminateMsg (pMsg);

        default:
            throw XException ("Cannot dispatch unknown message type");
            return false;
        }
    }
};


# ifndef DEFAULT_MSG_SIZE
#   define DEFAULT_MSG_SIZE 256
# endif
# ifndef DEFAULT_FREE_COUNT
#   define DEFAULT_FREE_COUNT 128
# endif

typedef CEvent CMsgEvent;


/*
    Message Receiver is the base class of a family of inter-thread
    or interprocess communication classes.
*/
class CMessageReceiver
{
    friend class CMessage; // this also serves as a forward reference
    friend class CMessageReceiverFactory;

    /* Don't allow anyone to copy assign a message receiver */
    CMessageReceiver & operator= (CMessageReceiver & msg) {return *this;}

public:
    enum blockFlag
    {
        blockNone = 0,
        blockOnSendAll = 1,
        blockOnSendFull = 2,
        blockOnReceive = 4,
        last_blockFlag
    };

    virtual ~CMessageReceiver ()
    {
# if 0
        TRACE ("~CMessageReceiver\n", this);
# endif
        waitForUsers ();
    }

protected:
    CMsgEvent m_waitEvent;
    int m_emptyWaiters;
    XException * m_pExcept;
    int m_freeCount;
    int m_freeLimit;
    int m_allocCount;
    int m_msgCount;
    CMessage * makeMessage (int size);
    int m_maxMsgSize;
    enum {defMsgSize = DEFAULT_MSG_SIZE, defFreeCount = DEFAULT_FREE_COUNT};
    int m_users;
    DWORD m_blockFlags;

    void waitForUsers (void);
    

    CMessageReceiver (int maxMsgSize = defMsgSize,
                      int freeCount = defFreeCount,
                      int blockFlags = blockOnSendFull | blockOnReceive
                     );

    virtual bool notifyEmpty (void);

public:
    /*
        Allows the sender to tell the receiver that he has failed.
        The failed thread constructs an exception structure with
        the reason for the failure.  The implemation should copy
        the exception and throw it when the receiver trys to receive
        or if it is blocked after the tasks wakes.
    */
    virtual void failed (const XException & reason) = 0;
    
    /*
        get a message container to package the message.
    */
    virtual CMessage * allocMessage (void) = 0;

    /*
        send an already packaged message.
    */
    virtual bool send (CMessage * msg) = 0;

    /* 
        The basic send interface.
        This function is not virtual because it should not be overridden.
        The send (pMsg) function needs to implement the actual send.
    */
    bool send (EMessageType id, int user, int length, const BYTE * pBuf)
    {
        CMessage * pMsg = allocMessage ();
        pMsg->setMessage (id, user, length, pBuf);
        return send (pMsg);
    }

    int maxSize (void) {return m_maxMsgSize;}

    /*
        This function should return the number of message available
        via the receive method.
    */
    virtual int messagesAvailable (void)
    {
        if (m_msgCount == 0)
            notifyEmpty ();
        return m_msgCount;
    }
    /* for debug */
    int messagesFree (void) {return m_freeCount;}

    /*
        The receive method. The semantics of this function
        is simply that it return a messge pointer when called.
        The implementing class should obey the blocking flags
        semantics.  A null pointer therefore my be returned.
        The reciever function should call notifyEmpty () when
        the queue is empty.
    */
    virtual CMessage * receive (void) = 0;

    /* register task as a user of this queue */
    virtual int attach (void)
    {
        return ++m_users;
    }
    
    /* unregister task as a user of this queue */
    virtual int detach (void)
    {
        --m_users;
        m_waitEvent.PulseEvent ();
        return m_users;
    }

    /*
        This function allows any task to wait until the queue
        is empty.
    */
    virtual bool waitForEmpty (void);

};

# undef DEFAULT_MSG_SIZE
# undef DEFAULT_FREE_COUNT


# endif


