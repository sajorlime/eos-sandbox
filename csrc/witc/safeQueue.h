/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
* safeQueue.h
*
* .........................................................................
*
* .........................................................................
*
* 
*
* .........................................................................
*
*
*  Date     Description
* ------   -----------
*
*
*
* Classes/Types:
    SafeQueue -- is a class implements a FIFO queue that is thread safe.
*
*
*/


# ifndef __SAFEQUEUE_H__
# define __SAFEQUEUE_H__

// there should be some ifdef MFC here.
# include <afxmt.h>
# include <queue>
using namespace std;

typedef CMutex CQueueMutex;



template<class _Ty> class SafeQueue
{
private:
    queue<_Ty> m_queue;
    CCriticalSection m_access;

public:
    _Ty dequeue (void)
    {
        _Ty Ty;
        if (m_queue.size () <= 0)
        { // note is Ty is not a pointer we are creating zero objects.
            return (_Ty) 0;
        }
        m_access.Lock ();

        Ty = m_queue.front ();
        m_queue.pop ();

        m_access.Unlock ();
        
        return Ty;
    }
    void enqueue (_Ty Ty)
    {
        m_access.Lock ();

        m_queue.push (Ty);

        m_access.Unlock ();
    }
    int size ()
    {
        return m_queue.size ();
    }
    SafeQueue (void)
        :
          m_access (),
          m_queue ()
    {
    }
};


# endif