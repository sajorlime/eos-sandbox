/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
    MessagePacer.h
*
*
    Contains defintion of a class that paces CMessageReceiver based
    inter-thread communication.
*
*
   Date     Description
  ------   -----------
*
* NOTES:

*
*
* Classes/Types:
    CMessagePacer -- allows the pacing of an unknowing messge producer
    that would otherwise overflow the input processing.
*
*
*/


# ifndef __MESSAGEPACER_H__
# define __MESSAGEPACER_H__

# include "Message.h"
# include "Thread.h"


class CMessagePacer : public CMessageReceiver
{
    CMessageReceiver * m_pMR;
    CThread * m_pThread;


public:
    CMessagePacer (CMessageReceiver * pMR, CThread * pThread);

    virtual bool send (CMessage * pMsg);

    virtual CMessage * receive (void);

    virtual void failed (const XException & reason);

    virtual CMessage * allocMessage (void);

    virtual int messagesAvailable (void);

    virtual int attach (void);

    virtual int detach (void);

    virtual bool waitForEmpty (void);

};


# endif