
/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
*
* Thread.cpp
*
* .........................................................................
*
*  Date     Description
* ------   -----------
*
***************************************************************************
*
* Notes:
*
*
* Classes/Types:
*
*
*
*
*/


# include <afxmt.h>
# include <windef.h>
# include "Exception.h"
# include "Thread.h"


# ifndef DEFAULT_STACK_SIZE
#   define DEFAULT_STACK_SIZE (1 << 10)
# endif


CThread::CThread (DWORD dwPriorityClass,
                  int dwPriority,
                  bool waitForStart
                 )
    :
     m_event (!waitForStart),
     m_wake (),
     m_run (ts_suspended)
{
    DWORD threadId;

    m_hThread = CreateThread (
                               (LPSECURITY_ATTRIBUTES) 0,
                               DEFAULT_STACK_SIZE,
                               xThread,
                               (LPVOID) this,
                               0,
                               &threadId
                             );
    if (m_hThread == NULL)
    {
        throw XException ("CreateThread Failed");
    }
    // note WinCE may not support this.
    SetPriorityClass (m_hThread, dwPriorityClass);
    SetThreadPriority (m_hThread, dwPriority);
    if (!waitForStart)
        start ();
}


CThread::~CThread()
{
    terminate (0);
}

bool CThread::start (void)
{
    m_event.SetEvent ();
    return true;
}

bool CThread::terminate (DWORD msWait)
{
    switch (m_run)
    {
    case ts_terminated:
        return true;
    case ts_sleeping:
        wake ();
        break;
    case ts_suspended:
        resume ();
        break;
    }
    HANDLE v[2];
    m_run = ts_terminated;
    v[0] = m_event;
    v[1] = m_hThread;
    DWORD r = WaitForMultipleObjects (2, v, 0, 2000);
    if (r == WAIT_TIMEOUT)
    {
        TerminateThread (m_hThread, te_failure);
        return false;
    }
    return true;
}

bool CThread::suspend (void)
{
    SuspendThread (m_hThread);
    m_run = ts_suspended;
    return true;
}

bool CThread::resume (void)
{
    m_run = ts_running;
    ResumeThread (m_hThread);
    return true;
}

bool CThread::sleep (DWORD ms)
{
    m_run = ts_sleeping;
    HANDLE v[1];
    v[0] = m_wake;
    // this could use wait for single object, but I might
    // want to extend this later.
    WaitForMultipleObjects (1, v, 0, ms);
    m_run = ts_running;
    return true;
}

bool CThread::wake (void)
{
    if (m_wake.SetEvent ())
        return true;
    return false;
}


void CThread::dump (void)
{
    TRACE ("CThread handle:%x\n", m_hThread);
    TRACE ("CThread m_run:%d\n", m_run);
}

unsigned long WINAPI xThread (LPVOID pParam)
{
    CThread * pT = (CThread *) pParam;
    int r;
    pT->m_event.Lock ();
    pT->m_run = CThread::ts_running;
    try
    {
        TRACE ("CThread %x entry\n", pT);
        r = pT->thread ();
        TRACE ("CThread %x exited with %d\n", pT, r);
    }
    catch (
# ifdef _DEBUG
            XException (&x)
# else
            ...
# endif
	      )
    {
# ifdef _DEBUG
        TRACE ("CThread %x exited with exception: %s\n", pT, x.m_pReason);
# endif
    }
    pT->m_hThread = 0;
    pT->terminate (0);
    pT->m_event.SetEvent ();
    pT->m_run = CThread::ts_terminated;
    return (unsigned long) r;
}



