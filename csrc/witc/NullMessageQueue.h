/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
*
    NullMessageQueue.h
*
*
* Changes:
      Date     Description
    --------   -----------
    08/05/99    Posted
*
*
* Notes:
*
*
* Classes/Types:
    CNullMessageQueue -- this class allows for a 
    when no messages are availble.
*
*/


# ifndef __NULLMESSAGEQUEUE_H__
# define __NULLMESSAGEQUEUE_H__

# include "MessageQueue.h"

typedef CEvent CMsgEvent;

/*
    A message queue is inter-thread communicaiton object.  It allows
    one reciever and multiple senders.
    The class extends CMessageReceiver, which
    is passed to clients to allow them to send messages.

    Issue: The class probably should limit the maximum messages
    that it can allocate.
*/
class CNullMessageQueue : public CMessageQueue
{
private:

protected:

public:
    bool send (EMessageType id, int user, int length, const BYTE * pBuf)
    {
        return true;
    }

    bool send (CMessage * pMsg)
    {
        release (pMsg);
        return true;
    }

    CNullMessageQueue (int maxMsgSize = defMsgSize,
                   int freeCount = 1,
                   int growFactor = defGrowFactor
                  )
        : CMessageQueue (maxMsgSize, freeCount, growFactor)
    {
    }

    CMessage * receive (void)
    {
        return (CMessage *) 0;
    }
};




# endif

