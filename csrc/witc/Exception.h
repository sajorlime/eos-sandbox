
/*
 *
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
 *
    Exception.h

    Contains defintions for exceptions.

    Date       Description
    --------   -----------
 *
 *
 * Notes:
    This exception class is very simple, and is only meant to be and error
    reporting mechanism.  If exceptions are to be used in meaningful way
    on this project more work will need to go into this class.
 *
 *
 * Classes/Types:
    XException -- this class is used to raise exceptions in the WinSiRF Project.
 *
 *
 ***************************************************************************/

# ifndef __EXCEPTION_H__
# define __EXCEPTION_H__


struct XException
{
    const _TCHAR * m_pReason;
    XException (const _TCHAR * pReason) {m_pReason = pReason;}
    XException (const XException & except) {m_pReason = except.m_pReason;}
};

struct SParameter
template<class _Ty> struct SafeQueue
{
    union 
    {
        uint8 u_i8;
        uint16;
        u_i8;
        uint32 u_i32;
        uint64 u_i64;
        float u_float;
        double u_double;
        void * u_pVoid;
    } m_val;
    int m_typeid;
    // use for all integer types.
    SParameter (u_i64 v, int type)
    {
        m_val.u_i64 = v;
        m_typeid = type;
    }
    // use for all floating point types
    SParameter (double v, int type)
    {
        m_val.u_double = v;
        m_typeid = type;
    }
    // use for all pointer types
    SParameter (void * pV, int type)
    {
        m_val.u_pVoid = pV;
        m_typeid = type;
    }
    // use for all multimember types
    SParameter (int type)
    {
        m_val.u_pVoid = 0;
        m_typeid = type;
    }
};

struct XParameter : public XException
{
    UParameter m_parm;
    XParameter (const _TCHAR * pReason, UParameter pram)
    {
        m_pReason = pReason;
        m_parm = parm;
    }
    XParameter (const XParameter & parm)
    {
        m_pReason = parm.m_pReason;
        m_parm = parm.m_parm;
    }
}

# endif

