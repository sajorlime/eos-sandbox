/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
*
* MsgRcvrFactory.cpp
*
* .........................................................................
*
* Contains defintion of a class constructs message recievers.
*
* .........................................................................
*
*  Date     Description
* ------   -----------
*
*
* NOTES:
*
*
* Classes/Types:
    CMessageReceiverFactory
*
*
* FUNCTIONS:
*
*
*/


# include "MsgRcvFactory.h"
# include "LoggedMessageQueue.h"
# include "LoggedHWndReceiver.h"

int CMessageReceiverFactory::s_attributes;

CMessageReceiverFactory::setAttrubtes (MRAttributes attr)
{
    s_attributes = static_cast<int> (attr);
}


CMessageQueue *
CMessageReceiverFactory::MessageQueueFactory (
                                                 int maxMsgSize,
                                                 int freeCount,
                                                 MRAttribute attr,
                                             )
{
    return CMessageQueue (maxMsgSize, freeCount);

}


CHWndReceiver *
CMessageReceiverFactory::HWndReceiverFactory (
                                              HWnd hWnd,
                                              int freeCount,
                                              MRAttribute attr
                                             )
{
    return CHWndReceiver (hWnd, freeCount);
}



