
/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
*
    Thread.h
*
*
* Defines Thread class
*
      Date     Description
    --------   -----------
*
* Notes:
*
*
* Classes/Types:
    CThread -- is a class the encapulates the needs of a non-windows thread.
    It provides the basic thread operations, under Win32.
    CThread::ThreadState -- enumeration of thread states.
    CThread::ThreadExit -- enumeration of the exit codes for a thread.
*
*/


# ifndef __THREAD_H__
# define __THREAD_H__

# ifdef __WIN32
#   include <afxmt.h>

#   define OSHANDLE HANDLE
# endif

/*
    CThread is a class that implements a simplified threading model.
*/
class CThread
{
public:
    /* 
        The thread state values are designed to allow a simple truth
        test on it be a "running thread." OTH, the call of running caller
        of running can discriminate of running or suspended.
    */
    enum ThreadState
    {
        ts_terminated = 0,
        ts_running,
        ts_sleeping,
        ts_suspended,
        last_ThreadState
    };
    enum ThreadExit
    {
        te_ok = 0,
        te_terminated,
        te_failure,
        last_ThreadExit
    };
private:
    OSHANDLE m_hThread;
    CEvent m_event;
    CEvent m_wake;

    friend unsigned long WINAPI xThread (void * pParam);
protected:
    ThreadState m_run;
    // thread is call by xThread, inheriting classes define to run
    virtual int thread (void) = 0;

public:
    void dump (void);

    ThreadState running (void) {return m_run;}

    OSHANDLE getThreadHandle (void) {return m_hThread;}

    virtual bool start (void);

    /*
        If an inheriting class wants to implement class specific termination
        it should overired terminate, to its specific termination and call
        the terminate of its super class.
    */
    virtual bool terminate (DWORD msWait);

    /*
        suspends the process.  A suspend thread will not do anything,
        therefore it should only be called under well structured conditions.
    */
    virtual bool suspend (void);

    /*
        resume -- undoes a suspend.
    */
    virtual bool resume (void);

    /*
        sleep is called by the theard to sleep in a way that allows it
        to be woken with the wake call.  The default is an infinate
        wait which requires that wake be called.
    */
    virtual bool sleep (DWORD msSleep = INFINITE);

    /*
        wake -- cause a thread to leave the sleep state.
    */
    virtual bool wake (void);

    CThread (DWORD dwPriorityClass = NORMAL_PRIORITY_CLASS,
             int dwPriority = THREAD_PRIORITY_NORMAL,
             bool waitForStart = true
            );

    virtual ~CThread ();
};

# endif

