/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
*
    LoggedMessageQueue.h
*
*
    Contains definitions for the LoggedMessageQueue class.
*
*
    Date     Description
  --------   -----------
*
*
* Notes:
*
* Classes/Types:
    CLoggedMessageQueue -- is a class that allows logging of message along
    with delivery.
*
*/


# ifndef __LOGGEDMESSAGEQUEUE_H__
# define __LOGGEDMESSAGEQUEUE_H__

// there should be some ifdef MFC here.
# include <afxmt.h>

# include "MessageQueue.h"
# include "Logger.h"


class CLoggedMessageQueue : public CMessageQueue
{
private:
    CLoggerAccess  m_LA;

protected:
    virtual char * format (char * pDest,
                           EMessageType id,
                           int user,
                           int length,
                           const BYTE * pBuf,
                           int maxSize
                          );

public:
    CLoggedMessageQueue (CLogger * pLogger,
                         const char * pLogSrc,
                         int maxMsgSize = 256,
                         int freeCount = 10
                        );
    virtual ~CLoggedMessageQueue ();
    virtual bool send (CMessage * msg);
    virtual void failed (const XException & reason);
};



# endif