// test enum notions

#include <iostream>
#include <fstream>
#include <cerrno>
#include <cstring>
#include <ctime>
#include <string>

//using namespace std;
//using namespace std::string_literals;

enum xyx
{
    mone = -1,
    mtwo = mone + mone,
    mthree = mtwo + mtwo,
    num_xyx = -mthree
};


int val(xyx x) {
    return -x;
}

std::string raw =  R"raw(muliti-line
example 
string)raw";

long double operator "" _w(long double x) {return x;}
//std::string operator "" _w(const char16_t*, size_t) {return t;}
unsigned operator "" _w(const char* s) {return atoi(s);}




int main(int argc, char * argv[]) 
{
    int y = mthree;
    std::cout << ":=mthree:" << y << std::endl;
    std::cout << "mone:" << mone << std::endl;
    std::cout << "mtwo:" << mtwo << std::endl;
    std::cout << "mthree:" << val(mthree) << std::endl;
    std::cout << "first:" << num_xyx << std::endl;
    std::cout << "1.2_w:" << 1.2_w << std::endl;
    std::cout << "raw:" << raw << std::endl;

}


