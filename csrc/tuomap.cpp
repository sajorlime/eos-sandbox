// test program for an unordered_map.
 

#include <functional>
#include <iostream>
#include <list>
#include <unordered_map>
#include <string>


struct Dev
{
    int devClass;
    unsigned int slaveNum;
    std::string manufacturer;
    std::string model;
    std::string options;
    std::string version;
    std::string serial;
};

struct DInfo
{
    enum Dt {ea, eb, ec};
};

struct MDev
{
    Dev _d;
    DInfo::Dt _t;
};

std::list<int> ids;

// TODO(epr): we can have the hash function compose this,
//            see https://en.cppreference.com/w/cpp/utility/hash
const std::string _equinox_name =   "Schneider:XWPro";
std::string _dummy1_name =   "ESS-MFG:ESS-MFG";

//Dev y = {77, 0, "SunPower", "Equinox", "SuperPowers", "0.01", "SN0.1"};

const std::list<MDev> _equinox_childern = {{{77, 0, "SunPower", "Equinox", "SuperPowers", "0.01", "SN0.1"}, DInfo::ea}};
std::list<MDev> _dummy1_childern = {{{88, 0, "SunPower", "Eq-dummy", "NoPowers", "7.77", "SN1"}, DInfo::ea}};


//struct MDMatch {std::string manufacturer; std::string model;};
// list of multi-device parents, we assume a short list so we can search linearly.
typedef std::unordered_map<std::string, const std::list<MDev>> MDevMap;
static const MDevMap _multiParentsMap = {
    {_equinox_name, _equinox_childern}
};
   // {_dummy1_name, _dummy1_childern}

//std::string _multi_manufacures[] = {
//    "Schnedier", 
//    "EOL"
//};

// Look up this device against the multi parents map.
// TODO(epr): We will need to 
// Args:
//  parent: is a candidate parent device, most devices will not have children
//  rlist: is where it will return the children, if they exist.
// Returns:
//    true iff childern where found.
bool get_dev_children(const Dev & parent, std::list<MDev> &rlist)
{
    std::list<MDev> lmd;
    for (auto x : _multiParentsMap) {
        lmd = x.second;
        std::cout << "first->" << x.first << " sec->" << &lmd << std::endl;
    }
    //lmd = _multiParentsMap.find(_equinox_name)->second;
    //std::cout << &lmd;
    //MDev md = *lmd.begin();
    //std::cout << &md;
    //std::cout << "mpMap[eq-name]->", _multiParentsMap[_equinox_name].begin()._d.manufacturer << std::endl;
    std::string m = parent.manufacturer +  ":" + parent.model;
    //std::cout << "QSTR:" << m << std::jdl;
    auto ii =_multiParentsMap.find(m);
    if (ii == _multiParentsMap.end()) {
        std::cout << "Not found" << std::endl;
        return false;
    }
    std::cout << "Found: " << ii->first << std::endl;
    rlist = ii->second;
    return true;
}

Dev _defaults[] = {
    {77, 0, "Schneider",  "XWPro",   "NoPowers",   "0.01", "SN01"},
    {88, 0, "ESS-MFG",    "ESS-MFG", "some-power", "7.77", "SN001"},
    {88, 0, "Some-other", "ESS-MFG", "some-power", "8.88", "SN002"}
};

int main(int argc, char ** argv)
{
    int index = argc - 1;
    if (index <= 0)
        index = 0;
    if (index > 2)
        index = 2;

    std::list<MDev> nld;
    if (!get_dev_children(_defaults[index], nld)) {
        std::cout << "NO CHILDREN" << std::endl;
        return -1;
    }

    //std::cout << x << y << z.front().manufacturer << std::endl;
    std::cout << "dev children: "
              << ": " << nld.front()._d.devClass
              << ": " << nld.front()._d.slaveNum
              << ": " << nld.front()._d.manufacturer
              << ": " << nld.front()._d.model
              << ": " << nld.front()._d.options
              << ": " << nld.front()._d.version
              << ": " << nld.front()._d.serial
              << ": " << nld.front()._t
              << std::endl;

    int x[] {1, 2, 3, 4, 5};
    ids.push_back(x[2]);
    ids.push_back(x[0]);
    ids.push_back(x[1]);
    ids.push_back(x[2]);
    ids.push_back(x[1]);
    ids.push_back(x[0]);

    for (auto i : ids)
    {
        std::cout << "i0:" << i << std::endl;
        i = -1;
    }
    std::list<int>::iterator li = ids.begin();
    for (; li != ids.end(); li++)
    {
        std::cout << "li:" << *li << std::endl;
    }
    int colon_pos = _equinox_name.find(":");
    std::cout << "EN: " << _equinox_name << " : col_pos: " << colon_pos << std::endl;
    return 0;
}
 

