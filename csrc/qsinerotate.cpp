
/*
 * File: median.cpp
 *
 * Implementation of a median filter.
 *
 */


# include <algorithm>
# include <cassert>
# include <cstdlib>
# include <iostream>
# include <fstream>
# include <iostream>
# include <random>
# include <string>
# include <stdio.h>
# include <time.h>

# include "SineTable.h"

#pragma GCC diagnostic ignored "-Warray-bounds"

# define USE_STD_SORT 0

using namespace std;

typedef uint16_t swtype;


const int sine_sz = 320;
const int sine_half_sz = 320 / 2;
const int sine_quarter_sz = 320 / 4;

int16_t out_table[sine_sz];

void clear_out_table()
{
    for (int i = 0; i < sine_sz; i++)
        out_table[i] = 0;
}

bool cmp_out_table()
{
    int i;
    for (i = 0; i < sine_sz; i++)
    {
        if (out_table[i] != sine_wave_table[i])
        {
            cout << i << ": "
                 << sine_wave_table[i]
                 << " != "
                 << out_table[i]
                 << endl;
            break;
        }
    }
    return i == sine_sz;
}

void out_sine_table()
{
    cout << "out sine table" << endl;
    for (int i = 0; i < sine_sz; i++)
    {
        if ((i % 16) == 0)
            cout << endl << i << ": ";
        cout << sine_wave_table[i] << ", ";
    }
    cout << endl;
}

void out_half_sine_table()
{
    cout << "out half sine table diffs" << endl;
    for (int i = 0; i < sine_half_sz; i++)
    {
        if ((i % 16) == 0)
            cout << endl << i << ": ";
        auto first_half = sine_wave_table[i];
        auto second_half = sine_wave_table[i + sine_half_sz];
        cout << (first_half + second_half) << ", ";
    }
    cout << endl;
}

void out_quarter_sine_table()
{
    cout << "out quarter sine table diffs" << endl;
    for (int i = 0; i < sine_quarter_sz; i++)
    {
        if ((i % 16) == 0)
            cout << endl << i << ": ";
        auto first_q = sine_wave_table[i];
        auto second_q = sine_wave_table[sine_quarter_sz - i - 1];
        auto third_q = -1 * sine_wave_table[i];
        auto forth_q = -1 * sine_wave_table[sine_quarter_sz - i - 1];
        auto sum = first_q + second_q + third_q + forth_q;
        if (sum) 
        {
            cout << "sum-all-4:" << (first_q + second_q + third_q + forth_q) << ", ";
        }
        sum = first_q + third_q;
    }
    cout << "out quarter sine table optimize" << endl;
}

void out_quarter_split_sine_table()
{
    for (int j = 0; j < sine_sz / sine_quarter_sz; j++)
        for (int i = 0; i < sine_quarter_sz; i++)
        {
            //auto k = 4 * j + i;
            if ((i % 16) == 0)
                cout << endl << i << ": ";
            cout << sine_wave_table[i] << ", ";
        }
}

const int gswt_siz = sizeof(quarter_sine_wave_table) \
                         / sizeof(quarter_sine_wave_table[0]);

// The quarter table has 81 elements, because the full table is symetric
// at 1/2.
// Therefore, we need to transverse the table twice, fliping at the end.
// The output comes from couting up to the end, then down w/o repeating
// the end, then at the bigging flip sign on output as we repeat the pattern.
// At the end of each pattern, up/down, flip the output size
void quarter_table()
{
    cout << "out four quarters sine table" << endl;
    int i;
    int j;
    // now count up to end, output value as is sine[i]
    for (i = 0, j = 0; i < gswt_siz; i++, j++)
    {
        if ((j % 16) == 0)
            cout << endl << i << ": ";
        out_table[j] = quarter_sine_wave_table[i];
        cout << out_table[j] << ", ";
    }
    // now count down to begining, output value as is sine[i]
    for (i -= 2; i > 0; i--, j++)
    {
        if ((j % 16) == 0)
            cout << endl << i << ": ";
        out_table[j] = quarter_sine_wave_table[i];
        cout << out_table[j] << ", ";
    }
    // now count up to end, output value as is -sine[i]
    for (; i < gswt_siz; i++, j++)
    {
        if ((j % 16) == 0)
            cout << endl << i << ": ";
        out_table[j] = -quarter_sine_wave_table[i];
        cout << out_table[j] << ", ";
    }
    // now count down to begining, output value as is -sine[i]
    for (i -= 2; i > 0; i--, j++)
    {
        if ((j % 16) == 0)
            cout << endl << i << ": ";
        auto x = quarter_sine_wave_table[i];
        out_table[j] = -x;
        cout << out_table[j] << ", ";
    }
    if (cmp_out_table())
    {
        cout << "all good \n";
    }
}

//int main(int argc, char** argv)
int main()
{
    quarter_table();
    //cout << "out four quarters sine table" << endl;
    cout << endl;
}

