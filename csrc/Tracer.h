/* 
// 
// $Id: //Source/Common/tcp/tracer.h#5 $ 
// 
// Copyright (c) 1999-2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: tracer.h
// 
// Creation Date: 1/19/00
// 
// Last Modified: 
// 
// Author: Prakash Manghnani
// 
// Description: 

  CTracer is a class that provides common tracing functionality.
  CTracer provides a tracing mechanism that allows for trace output to be increased/decreased
  based on 2 dimensions: (subSystem & tracelevel) 
  Tracelevels are integer values (0-4), where 
  0 == Terse output
  4 == Very verbose
  At runtime, the tracelevel for any subSystem can be changed. Initially all subSystems
  default to a tracelevel of "defaultTraceLevel".

  Subsystem is a char *. 
  Each subSystem should define its own unique string.
  CTracer maintains a hashtable of subSystem-traceLevel settings.
  This hashtable grows dynamically each time a new subSystem calls a CTracer method.


  CTracer also allows output device to be set dynamically (file/stdout/other).
  CTracer also provides mechanism to extend Output devices (by subclassing CTraceOutput).
 
  Note: See TestTrace.cpp for working example of CTracer

  Usage Examples:
  - Output
  CTracer::pprintf("dbsim", 0, "Fatal Error: %d\n", n);
  CTracer::pprintf("dbsim", 4, "Entering function foobar");
  CTracer::stream("dbsim", 1) << "Warning: file not found" << endl;
  CTracer::stream("dbsim", 4) << "Exiting function foobar" << endl;

  - To set traceLevel for a subsystem:
  CTracer::setTraceLevel("dbsim", 4);

  - To redirect output:
  CTracer::setOutputStdout()			// Sends output to stdout
  CTracer::setOutputFile("abc.txt")		// Sends output to file "abc.txt"
  CTracer::setOutputNone()				// Turns off all output
  CTracer::setOutputDevice(CTraceOutput *pOutputDev);	// Sends output device pOutputDev (caller creates a concrete subclass of CTracerOutput)

// TBD:
// 1) Current vesion only supports 1 output device. Could be extended to
//    support multiple outputs (to file, and a window)
// 2) Support for different output devices for different subsystems.
//  
*/ 
#if !defined(__TRACER_H__)
#define __TRACER_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

# include "dotcastns.h"

#ifdef _WIN32
#include <wtypes.h>
#endif

#ifdef _WIN32
// Needed for STL - VC++ compiler gives horrible warnings without it
#pragma warning (disable : 4786)
#endif //_WIN32

#include <iostream>
#include <stdarg.h>

__BEGIN_DC_NAME_SPACE


//
// CTraceOutput -
// This class abstracts away actual device trace output is sent to.
class CTraceOutput
{
public:
	virtual void output(const char *s) = 0;
};

/*
	Class for common tracing.
*/
class CTracer  
{
public:
  //
  // Rule 1 of programming: no bare numbers in programs.
  //        To obey rule one, replace the semantically
  //        meaningless traceLevels 'eLevelN' with a set
  //        of mnemonics similar to those used in syslog(2)
  //
  // NOTE: This enum must not use assignment that skips values
  //       and must originate at 0, so that the enum i/o code
  //       below will work.  Also, if the enum is changed,
  //       the implementation of traceLevelOf and traceLevelName
  //       must change to match.
  //
  enum traceLevels {
    et_fatal=0,			// Application has become unusable
    et_alert,			// Error requires immediate intervention
    et_critical,		// Error is likely to become fatal soon
    et_error,			// An error has occured
    et_warning,			// An error is likely to occur
    et_notice,			// A normal, but significant event has occured
    et_info,			// An informational message - mild debugging
    et_debug,			// A debugging message - typical debugging
    et_verbose,			// A verbose debugging message
    et_lastTraceLevel		// ENUM MARKER, do not use
  };
  //
  // Some times it is good to be able to do I/O on enums.  Since
  // tracer level is something we want to be able to do I/O on,
  // we add some operators, below. (outside the class definition,
  // because they are not member functions.)  To do this easily,
  // add string conversion routines:
  //
  static enum CTracer::traceLevels traceLevelOf(const char *const pName);
  static const char * const traceLevelName(CTracer::traceLevels e);

	//
	// Output objects supported:
	enum outputDevice {
		eNone=0,
		eStdout=1,
		eFile=2
	};

#if !defined(_NOTRACING)
	static void pprintf(const char *subSystem, int traceLevel, const char *fmt, ...) {
		va_list args;
		va_start (args, fmt);
		va_pprintf(subSystem, traceLevel, fmt, args);
		va_end(args);
	}
	// print w/default subsystem + trace level
	static void dprintf(const char *fmt, ...) {
		va_list args;
		va_start (args, fmt);
		va_dprintf(fmt, args);
		va_end(args);
	}
	static void printLastError(const char *label, int errNo) {
		privatePrintLastError(label, errNo);
	}
	static std::ostream &stream(const char *subSystem, int level) {
		return privateStream(subSystem, level);
	}
#else // _NOTRACING
	// Generate inline dummies
	// This is what client files with _NOTRACING compile against
	inline static void pprintf(const char *subSystem, int traceLevel, const char *fmt, ...) {}
	inline static void dprintf(const char *fmt, ...) {}
	inline static void printLastError(const char *label, int errNo) {}
	static std::ostream &stream(const char *subSystem, int level) {
		return privateNullStream();
	}
#endif // _NOTRACING

	//
	// Routines to get/set trace level
	static void setTraceLevel(const char *subSystem, int level);
	static int getTraceLevel(const char *subSystem);

	//
	// Methods to set output device
	static void setOutputStdout();
	static void setOutputFile(const char *file);
	static void setOutputNone();
	static void setOutputDevice(CTraceOutput *pOutputDev);

	static CTraceOutput *getOutputDevice() { return sInstance.m_pOutputDev; }
private:
	static const int defaultTraceLevel;

	static void va_pprintf(const char *subSystem, int traceLevel, const char *fmt, va_list args);
	// print w/default subsystem + trace level
	static void va_dprintf(const char *fmt, va_list args);
	static void privatePrintLastError(const char *label, int errNo);
	static std::ostream &privateStream(const char *subSystem, int level);
	static std::ostream &privateNullStream();

	CTracer();
	void output(const char *str);		// Routine that actually does the output to a device
	CTraceOutput *m_pOutputDev;

	//
	// Singleton instance
	static CTracer sInstance;
};


/*
    setTraceLevel (const char *, int) is just in an inline call to the class function.
*/
inline void setTraceLevel(const char * subsystem, int level)
{
    CTracer::setTraceLevel (subsystem, level);
}

/*
    setTraceLevel (const char *, const char *) allows the user to pass in the
    trace subsystem and the level as strings.
    If the level does not match any defined level it will be set to et_error.
*/
void setTraceLevel (const char * subsystem, const char * level);

/*
    setTraceLevel (const char *) accepts the subsystem and level as one string.
    The values are seprated by a colon.  E.g.
        "Foobar:warning"
    This version is intended to make this work easily from a command line arguement.
*/
void setTraceLevel (const char * subsystemAndLevel);

__END_DC_NAME_SPACE

//
// Allow the ability to do I/O on traceLevel enums
//
// outside of the dotcast name space so that istream references are to
// the std facilities.  The implementation of these operators is fragile.
// See the note at the point of definition of the enum.
//

std::istream& operator>>(std::istream&, dotcast::CTracer::traceLevels&);
std::ostream& operator<<(std::ostream&, const dotcast::CTracer::traceLevels&);

#endif // !defined(__TRACER_H__)
