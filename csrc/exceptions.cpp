
#include <stdio.h>
#include <exception>
#include <cstdarg>
#include <cstdlib>
#include <cstring>
#include <iostream>

char buffer[256] = "[31;1m";
const char* norm = "[0m";

char* myprint(const char* fmt, ...)
{
    ulong nred = strlen(buffer);

    va_list args;
    va_start(args, fmt);


    int n;

    printf("%lu F{%s}\n", nred, fmt);
    n = vsnprintf(&buffer[nred], sizeof buffer - nred, fmt, args);
    printf("%d|%lu|%s|T\n", n, strlen(buffer), buffer);
    strcpy(&buffer[n + nred], norm);
    printf("\n|%lu|%s|T\n", strlen(buffer), buffer);

    return buffer;
}

class MyEx : public std::exception {
};

class MyRangeError : public std::range_error {
public:
    MyRangeError(const std::string& w)
     : std::range_error(w)
    {
    }
};

int main(int argc, char** argv)
{
    printf("%d\n", argc);
    if (argc < 3)
    {
        return -1;
    }
    printf("MYP: %s\n", myprint("%s %s %s",  argv[0], argv[1], argv[2]));
    //printf("%s\n", norm);
    std::cout << "someting else" << std::endl;

    int j = 3;
    std::cout << "i:" << j << std::endl;
    for (int i = 0; i < 3;  i++, j++) {
        if (i == 0)
            break;
    }
    std::cout << "J:" << j << std::endl;
    try {
        if (atoi(argv[1]) == 2)
            throw MyEx();
        if (atoi(argv[2]) == 2)
            throw MyRangeError("My range error");
        if (atoi(argv[2]) > 2)
            throw std::range_error("a range error");
        int x = 1;
        int y = 0;
        std::cout << 'D' << x / y;
    }
    catch (MyEx const& mex) {
        std::cout << "MyEx:" << mex.what() << std::endl;
    }
    catch (MyRangeError const& mre) {
        std::cout << "MRE:" << mre.what() << std::endl;
    }
    catch (std::range_error const& re) {
        std::cout << "RE:" << re.what() << std::endl;
    }
    catch (std::exception const & ex) {
        std::cout << "std::ex:" << ex.what() << std::endl;
    }
    catch (...) {
        std::cout << "UNKNOWN" << std::endl;
    }
    return 0;
}

