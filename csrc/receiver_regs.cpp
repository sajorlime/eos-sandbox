

/******************************************************************************
*
* Copyright (C) 2021 oneNav, Inc. All rights reserved.
*
* <<<INSERT oneNav header here -- format TBD >>>
*
******************************************************************************/

// receiver_regs.h
// Defines receiver register constants
//

# include <iostream>
# include <iomanip>
# include <cstdlib>

# include "receiver_regs.h"

using namespace registers;

using namespace registers;

using namespace std;

int main(int argc, char** argv)
{
    DfeType dfe;
    dfe.c0.s.source_select = (int) DfeSourceSelect::kLifSignal;
    dfe.c0.s.gain_step = (int) DfeGainStep::k0Db;
    dfe.c0.s.limit_select =  (int) DfeLimitSelect::kLimitDectector;
    dfe.c0.s.zero_notch_enable = (int) DfeZeroNotch::kNoZeroNotch;

    cout << "c0: " << hex << std::setw(8) << setfill('0') << dfe.c0.u << '\n'
         << "k0: 0x" << hex << std::setw(8) << setfill('0') << kLifMode
         << endl;
    //dfe.c1.s.limit = 0x3FF;
    //dfe.c1.s.threshold = 0x3FE;
    //dfe.c1.s.rem = 0;
    cout << "c1.s.limit: " << hex << std::setw(8) << setfill('0') << dfe.c1.s.limit << '\n'
         << "max limit: 0x" << hex << std::setw(8) << setfill('0') << max_dfe_limit << '\n'
         << "ci.s.threhold: 0x" << hex << std::setw(8) << setfill('0') << dfe.c1.s.threshold << '\n'
         << "max threshold: 0x" << hex << std::setw(8) << setfill('0') << max_dfe_threshold << '\n'
         << endl;
    cout << "c1: " << hex << std::setw(8) << setfill('0') << dfe.c1.u << ' '
         << "k1: 0x" << hex << std::setw(8) << setfill('0') << config1v
         << endl;

}


