/*
// 
// $Id: //Source/Common/libutility/BitArray.cpp#8 $ 
// 
// Copyright (c) 2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: BitArray.cpp
// 
// Creation Date: 11/13/01
// 
// Last Modified: 
// 
// Author: Emil Rojas
// 
// Description: 
// Implements the notion of a bit array that allows arbirtarity bit
// fields to be written.
//  
*/





//# include <stdio.h>


# include "BitArray.h"

__BEGIN_DC_NAME_SPACE

DEFINE_FILE_STRING("$Id", BitArray)



/*
    insertBits -- insert the value (v) or width (bits) into the buffer
    at position (pos).  width <= 32.
    Algorithm.
        First note that bits must be inserted in big-endian order.
        Bits are correct when in register as long as we read and write
        word value with lntohl.
        There are two cases.
        Case 1: bits fit in the word pointed to by pos.
            Create a mask that represents just these bits.
            read the current value (note lntohl).
            mask the old value with the not of the mask.
            Position our new bits where the go in the word and mask
            any bits that don't belong.
            Or results and write back to word.
        Case 2:
            Like case one except for each of two words.
            We need to get high order bits of value for the first
            word and the low order bits for the second. 
            by setting the bit index to 32 after the first word
            we reuse the case 1 code for the second word.
*/

uint32 CBitArray::insertBits (uint32 v, uint32 pos, uint32 bits)
{
    uint32 * pWord = m_pBA + (pos / 32);
    uint32 bIndex = 32 - (pos % 32);
    uint32 mask;
    uint64 mask64;
    uint32 old;

    if (bits > bIndex)
    { // bits go across word boundry
        bits -= bIndex;
        uint32 tv = v >> bits;
        mask = (1 << bIndex) - 1;
        old = lntohl (*pWord);
        old &= ~mask;
        tv &= mask;
        old |= (tv & mask);
        *pWord++ = lntohl (old);
        bIndex = 32;
    }
    mask64 =  ((((uint64) 1) << bits) - ((uint64) 1)) << (bIndex - bits);
    mask = (uint32) mask64;
    old = lntohl (*pWord);
    old &= ~mask;
    v <<= (bIndex - bits);
    v &= mask;
    old |= v;
    *pWord = lntohl (old);

    return pos + bits;
}

/*
    extractBits -- extracts the bits at pos of width bits and returns
    a right justied value for those bits.
*/
uint32 CBitArray::extractBits (uint32 pos, uint32 bits)
{
    uint32 * pWord = m_pBA + (pos / 32);
    uint32 bIndex = 32 - (pos % 32);
    uint32 mask;
    uint32 v = 0;
    uint32 u;

    if (bits > bIndex)
    { // bits go across word boundary
        bits -= bIndex;
        mask = (1 << bIndex) - 1;
        v = lntohl (*pWord++) & mask;
        v <<= bits;
        bIndex = 32;
    }
    mask = ((1 << bits) - 1) << (bIndex - bits);
    u = lntohl (*pWord) & mask;
    u >>= (bIndex - bits);
    v |= u;

    return v;
}



# ifdef _DEBUG
void dumpArray (ofstream & fout, CBitArray * pBA)
{
    const uint8 * pBits = pBA->buffer ();
    uint32 bits = pBA->byteSize () * 8;
    fout << "Bits: " << bits
         << "   bytes: " << (bits / 8)
         << "   rem: " << (bits % 8)
         << endl;
    const char bin[] = "01";
    const char hex[] = "0123456789ABCDEF";
    for (int i = 1; (int) bits > 0; bits -= 8, pBits++, i++)
    {
        fout
              << "("
              << hex [(*pBits >> 4) & 0xf]
              << hex [(*pBits >> 0) & 0xf]
              << ") "
              << bin [(*pBits >> 7) & 1]
              << bin [(*pBits >> 6) & 1]
              << bin [(*pBits >> 5) & 1]
              << bin [(*pBits >> 4) & 1]
              << bin [(*pBits >> 3) & 1]
              << bin [(*pBits >> 2) & 1]
              << bin [(*pBits >> 1) & 1]
              << bin [(*pBits >> 0) & 1]
              << ' '
              ;
        if ((i % 4) == 0)
            fout << endl;
    }
    fout << flush;
}
# endif




__END_DC_NAME_SPACE


