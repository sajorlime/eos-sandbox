// test time stuff

#include <iostream>
#include <fstream>
#include <cerrno>
#include <cstring>
#include <ctime>
#include <string>

using namespace std;

string rstr(uint32_t index)
{
    static const char * mystrs[] =
    {
        "str0",
        "str1",
        "str2",
        "abc",
        0
    };
    if (index >= 5)
        return "too big";
    return mystrs[index];
}

int dummy(int index)
{
    class foo {
    public:
        int _i;
        foo(int i) : _i(i) {}
    } f(index);
    return -f._i;
}


std::string do_file(std::string fstr)
{
    return std::string("dofile \"lua_scripts/") + std::string(fstr) + std::string(".lua\"\n");
}

const std::string 
static_device_info(std::string sindex, int ivalue)
{
    return std::string("static_device_info[\"")
           + sindex
           + std::string("\"].protocol_params.net_ltea_3phsum_kwh.scaler = 1E")
           + std::to_string(ivalue)
           + std::string("\n");
}


std::string
param_sets(int index, std::string dclass)
{
    return std::string("param_sets[") + std::to_string(index) + "] = "
           + "param_ets[\"" + dclass + "\"]\n\n";
}

const std::string
interface_config(string ifname, string type = "", int baud=0)
{
    if (type.empty())
        type = "No type";
    return ifname + "," + type + to_string(baud);
}

class Indent 
{
    int _maxl;
    int _level;
    std::string _tabs;
public:
    Indent(int maxl=3)
    : _maxl(maxl), _level(0), _tabs("\t\t\t\t\t")
    {
        //TODO(epr): create string of arbirtary size.
    }
    int in(int levels=1) {
        _level -= levels;
        if (_level < 0) {
            _level = 0;
        }
        //cout << "i:" << _level << endl;
        return _level;
    }
    int out(int levels=1)
    {
        _level += levels;
        if (_level > _maxl) {
            _level = _maxl;
        }
        //cout << "o:" << _level << endl;
        return _level;
    }
    const std::string tabs(void) const
    {
        return _tabs.substr(0, _level);
        //return _tabs;
    }
    friend std::ostream& operator<< (std::ostream&, const Indent& i);
};
std::ostream & operator << (std::ostream& out, const Indent& i)
{
    out << i.tabs();
    return out;
}

void print_element(int pos, const int * array)
{
    //array[1] = 7;
    cout << "AP:" << array[pos] << endl;
}

# define PE(POS) print_element(POS, x); printf("\"%s\"\n", #POS)


int main(int argc, char * argv[]) 
{
    // current date/time based on current system
    time_t now = time(0);

    // convert now to ttring form
    char* dt = ctime(&now);

    cout << "The local date and time is: " << dt << endl;

    // convert now to tm struct for UTC
    tm *gmtm = gmtime(&now);
    dt = asctime(gmtm);
    cout << "The UTC date and time is:"<< dt << endl;

    cout << "rstr(argc) => " << dummy(argc) << " : " << rstr(argc) << " : " << dummy(argc) << endl;

    cout << static_device_info("SINDEX", 17);

    cout << "IC: " << interface_config("IFa", "atype", 12) << endl;
    cout << "IC: " << interface_config("IFb", "btype") << endl;

    cout << do_file("test-file");
    Indent i;

    cout << "o0" << endl;
    cout << i.out() << endl;
    cout << i << "o1" << endl;
    cout << i.out() << endl;
    cout << i.tabs() << "o2" << endl;
    cout << i.in() << endl;
    cout << i.tabs() << "i1" << endl;
    cout << i.in() << endl;
    cout << i.tabs() << "i0" << endl;
    cout << i.in() << endl;
    cout << i.tabs() << "i0" << endl;


    cout << param_sets(17, "emilio") << endl;

    ofstream of0("bahaha.txt");
    if (of0.is_open()) {
        cout << "I CAN believe it" << endl;
        of0 << "I CAN believe it" << endl;
        of0 << dt << endl;
    }
    else {
        cout << "errno: " << errno << " : " << std::strerror(errno) << endl;
    }
    ofstream of("/bahaha");
    if (of.is_open()) {
        cout << "I can't believe it" << endl;
    }
    else {
        cout << "errno: " << errno << " : " << std::strerror(errno) << endl;
    }
    int x[] = {1, 2, 3};
    print_element(1, x);
    //PE(2, x);
    //PE(0, x);

    //char * pChar = 0;
    //string c = pChar;
    //cout << "NULL STRING" << endl;
    //cout << c << endl;
    char ca1[5] = "";
    char ca2[5] = "";
    ca2[0] = 'E';
    string s1 = ca1;
    string s2 = ca2;
    cout << "|" << s1 << "|" << s1.empty() << endl;
    cout << "|" << s2 << "|" << s2.empty() << endl;

    //std::string str = std:StrFormat("D: %d\n", 10);
    //cout << str;
}


