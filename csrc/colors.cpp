

# include <iostream>
# include <map>
# include <stdio.h>
# include <string.h>

enum Color {
#   define X(a, comment) a,
#   define XV(a, v, comment) a = v,
# include "colors.txt"
    ColoursCount
#   undef X
#   undef XV
};

std::map<std::string, Color> color_str_map =
{
#   define X(a, b) {#a, a},
#   define XV(a, v, comment) {#a, a},
# include "colors.txt"
#   undef X
#   undef XV
};

std::map<Color, std::string> color_map =
{
#   define X(a, b) {a, #a},
#   define XV(a, v, comment) {a, #a},
# include "colors.txt"
#   undef X
#   undef XV
};



int main(int argc, char** argv)
{
    for (auto color : color_str_map)
        std::cout << color.first << '=' << color.second << std::endl;
    std::cout << "Red" << '=' << color_str_map["Red"] << std::endl;
    std::cout << "Magenta" << '=' << color_str_map["Magenta"] << std::endl;
    std::cout << "Red" << '=' << color_map[Red] << std::endl;
    std::cout << "Magenta" << '=' << color_map[Magenta] << std::endl;
    for (auto c : color_map)
        std::cout << c.first << '=' << c.second << std::endl;
}

