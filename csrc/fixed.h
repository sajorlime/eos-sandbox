/*
 * &copyright Copyright (c)1997 BDN Software Inc. (Bruce D. Nilo).
 * All rights reserved.
 *
 * File: fixed.h
 *
 * $Source: /export2/Repositories/bdnswlibs/misc/fixed.h,v $
 *
*/


# ifndef __FIXED_H__
# define __FIXED_H__

# include "common/PrimitiveDefs.h"

# ifdef TEST_FIXED
#   include <iostream.h>
# endif


class fixed
{
    int16 m_sign;
    uint64 m_fxd;

public:
    enum fixedVal
    {
        fixedBits = 64,
        fixedPos = 32,
        fixedPositive = 0,
        fixedNegative = -1,
        lastFixedVal
    };

    fixed (void) {return;}

    fixed (int32 i)
    {
        m_sign = fixedPositive;
        if (i < 0)
        {
            m_sign = fixedNegative;
            i = -i;
        }
        m_fxd = (uint64) i << fixedPos;
    }

# if 0
    fixed (int i)
    {
        m_sign = fixedPositive;
        if (i < 0)
        {
            m_sign = fixedNegative;
            i = -i;
        }
        m_fxd = (uint64) i << fixedPos;
    }
# endif


    fixed (uint32 i)
    {
        m_sign = fixedPositive;
        m_fxd = (uint64) i << fixedPos;
    }

    fixed (uint32 whole, uint32 frac)
    {
        m_sign = fixedPositive;
        m_fxd = (uint64) whole << fixedPos;
        m_fxd += (uint64) frac;
    }

    fixed (double d)
    {
        m_sign = fixedPositive;
        if (d < 0)
        {
            d = -d;
            m_sign = fixedNegative;
        }
        uint32 whole = (uint32) d;
        uint32 frac = (uint32) (d * (1 << fixedBits));
        m_fxd = ((uint64) whole << fixedBits) + frac;
    }

    fixed & operator= (fixed & fxd)
    {
        m_sign = fxd.m_sign;
        m_fxd = fxd.m_fxd;
        return * this;
    }
    
    fixed & operator= (int32 i)
    {
        m_sign = fixedPositive;
        if (i < 0)
        {
            m_sign = fixedNegative;
            i = -i;
        }
        m_fxd = (uint64) i << fixedPos;
        return * this;
    }

    friend fixed operator* (fixed & li1, fixed & li2);


    ~fixed () {return;}

    int32 signof (void)
    {
        return m_sign;
    }

    friend int64 cmp (fixed & fxd1, fixed & fxd2)
    {
        int16 s = fxd1.m_sign - fxd2.m_sign;
        if (s)
            return (int64) s;
        return (int64) fxd1.m_fxd - (int64) fxd2.m_fxd;
    }

    int operator< (fixed & fxd)
    {
        if (cmp (*this, fxd) > 0)
            return 1;
        return 0;
    }

    int operator<= (fixed & fxd)
    {
        if (cmp (*this, fxd) >= 0)
            return 1;
        return 0;
    }

    int operator> (fixed & fxd)
    {
        if (cmp (*this, fxd) < 0)
            return 1;
        return 0;
    }

    int operator>= (fixed & fxd)
    {
        if (cmp (*this, fxd) <= 0)
            return 1;
        return 0;
    }

    int operator== (fixed & fxd)
    {
        if (cmp (*this, fxd) == 0)
            return 1;
        return 0;
    }

    int operator!= (fixed & fxd)
    {
        if (cmp (*this, fxd) != 0)
            return 1;
        return 0;
    }

    friend fixed operator+ (fixed & fxd1, fixed & fxd2);

    friend fixed operator+ (fixed & fxd1, int i)
    {
        fixed fxd2 = (int32) i;
        return fxd1 + fxd2;
    }

    friend fixed operator+ (int i, fixed & fxd1)
    {
        fixed fxd2 = (int32) i;
        return fxd1 + fxd2;
    }

    fixed operator += (fixed & fxd)
    {
        *this = *this + fxd;
        return *this;
    }

    friend fixed operator- (fixed & fxd1, fixed & fxd2)
    {
        return fxd1 + (-fxd2);
    }

    friend fixed operator- (fixed & fxd1, int i)
    {
        fixed fxd2 = (int32) i;
        return fxd1 - fxd2;
    }

    friend fixed operator- (int i, fixed & fxd1)
    {
        fixed fxd2 = (int32) i;
        return fxd1 - fxd2;
    }

    fixed operator -= (fixed & fxd)
    {
        *this = *this - fxd;
        return *this;
    }

    friend fixed operator- (fixed & fxd)
    {
        fixed r = fxd;

        r.m_sign ^= fixedNegative;
        return r;
    }

    friend fixed operator* (fixed & fxd1, fixed & fxd2);
    
    fixed operator *= (fixed & fxd)
    {
        *this = *this * fxd;
        return *this;
    }

    friend fixed operator* (fixed & fxd1, int i)
    {
        fixed t = (int32) i;
        return fxd1 * t;
    }

    friend fixed operator* (int i, fixed & fxd1)
    {
        fixed t = (int32) i;
        return fxd1 * t;
    }
    

    friend fixed operator/ (fixed & numerator, fixed & divisor);
    
    fixed operator /= (fixed & fxd)
    {
        *this = *this / fxd;
        return *this;
    }

    fixed abs (void)
    {
        m_sign = fixedPositive;
        return *this;
    }

    friend fixed abs (fixed fxd)
    {
        fxd.m_sign = fixedPositive;
        return fxd;
    }

    operator int ()
    {
        int whole;
        whole = (int) m_fxd >> fixedPos;
        if (m_sign)
            whole -= whole;
        return whole;
    }

# ifdef TEST_FIXED
    friend ostream & operator<< (ostream & out, fixed & fxd);
    friend char * toDouble (fixed & fxd);
# endif
};

# endif
