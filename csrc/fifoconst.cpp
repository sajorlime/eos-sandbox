
# include <iostream>
# include <string>

constexpr size_t num_samps_per_msec = 20480;

constexpr uint32_t access_rows = 640;
constexpr uint32_t access_uint = 8; // 8 bytes or 64-bits.
constexpr uint32_t access_elements = 8;
constexpr uint32_t access_row_size = access_elements * access_uint;
constexpr uint32_t data_elements = 5;
constexpr uint32_t data_row_size = data_elements * access_uint;
constexpr uint32_t address_space = access_rows * access_row_size;
constexpr uint32_t data_space = access_rows * data_row_size;

static_assert(num_samps_per_msec == 4 * data_space / 5);

constexpr uint32_t msec_row_step = 2 * num_samps_per_msec;

# ifndef READ_POWER
    // default to 1 giga byte
#   define READ_POWER 30
# endif
constexpr uint32_t max_read_space = 1 << READ_POWER;
constexpr uint32_t one_sec_data = 1000 * msec_row_step;
# ifndef SECONDS_TO_READ
#   define SECONDS_TO_READ 1
# endif
constexpr uint32_t seconds_to_read = SECONDS_TO_READ;
constexpr uint32_t milliseconds_to_sleep = (1000 * SECONDS_TO_READ) / 4;
constexpr uint32_t data_to_read = seconds_to_read * one_sec_data;
// We only want multiples of 1 millisecond of data
constexpr uint32_t max_read_millisecs = max_read_space % seconds_to_read;
constexpr uint32_t available_read_space = max_read_millisecs * seconds_to_read;

using namespace std;

# define COUT(Name) cout << #Name << ':' << Name << endl

int main()
{
    COUT(access_row_size);
    COUT(data_row_size);
    COUT(num_samps_per_msec);
    COUT(address_space);
    COUT(data_space);
    COUT(num_samps_per_msec);
    cout << "4XDS:" << 4 * data_space << endl;
    cout << "4/5DS:" << 4 * data_space / 5 << endl;
    cout << "4/5ASP:" << 4 * address_space / 5 << endl;
    cout << endl;
    COUT(max_read_space);
    COUT(one_sec_data);
    COUT(seconds_to_read);
    COUT(milliseconds_to_sleep);
    COUT(data_to_read);
    COUT(max_read_millisecs);
    COUT(available_read_space);
    COUT(milliseconds_to_sleep);
}

