

/******************************************************************************
*
* Copyright (C) 2021 oneNav, Inc. All rights reserved.
*
* <<<INSERT oneNav header here -- format TBD >>>
*
******************************************************************************/

// receiver_regs.h
// Defines receiver register constants
//

# ifndef FW_INC_HWREGS_RECEIVER_H_

namespace registers {

// copied from
// https://onenavai.sharepoint.com/sites/Engineering/Shared%20Documents/ASIC/document/Front%20End%20Module%20Descriptions%20V1.5.pdf
// Title: "Front End Module Descriptions For Version 1.5.0"
// Relative Address Field
// Position Name Description
// 0 DFE_CONFIG_0_REG[5:0] DFE Configuration Register
//  [5] ZERO_NOTICH_FILTER_ENABLE Enable zero Frequency Notch Filter
//  --      to Remove any Residual Mean Offset
//  [4] DFE_HARD_LIMT_SELECT Linear or Hard limit mode
//      0: Linear Mode with Limit Detector
//      1: Forced Hard Limit mode to the LIMIT_VALUE
//  [3:2] DFE_GAIN_STEP DFE Gain step at output of DFE
//      0: 0 db
//      1: +6db
//      2: +9.5db
//      3: +12 db
//  [1:0] DFE_SOURCE_SELECT DFE Input Select at start of next
//  --      millisecond.
//      0: No Input
//      1: RFS ADC Signal select (ADC0810000)
//      2: LIF ADC Signal select (ADC9600)
//      3: SSG Signal select

enum class DfeR0BitPos {
    kSourceSelect = 0,
    kGainStep = 2,
    kLimitSlecect = 4,
    kZeroNotchFilterEnable = 5,
    kEndDefR0BitPos
};

enum class DfeR0BitWidth {
    kSourceSelect = 2,
    kGainStep = 2,
    kHardLimitSlecect = 1,
    kZeroNotchFilterEnable = 1,
    kEndDefR0BitWidth
};

union DfeReg0
{
    struct 
    {
        uint32_t source_select:2;
        uint32_t gain_step:3;
        uint32_t limit_select:1;
        uint32_t zero_notch_enable:1;
        } s;
    uint32_t u;
    DfeReg0() : u(0) {}
};

enum class DfeSourceSelect {
    kNoInput = 0,
    kRfsSignal = 1,
    kLifSignal = 2,
    kSsgSignal = 3,
    kEndDefSourceSelect
};

enum class DfeGainStep {
    k0Db = 0,
    k6Db = 1,
    k9_5Db = 2,
    k12Db = 3,
    kEndDefGainSteop
};

enum class DfeLimitSelect {
    kLimitDectector = 0,
    kForceHardLimit = 1,
    kEndDfeLimitSelect
};

enum class DfeZeroNotch {
    kNoZeroNotch = 0,
    kZeroNotch = 1,
    kEndDfeZeroNotch
};


//
// 1 DFE_CONFIG_1_REG[19:0]
//  [19:10] DFE_THRESHOLD[9:0] Threshold for maximum limit detection.
//  --          If exceeded, then set sample to LIMIT_VALUE.
//  [9:0] DFE_LIMIT_VALUE [9:0] Limit value when sample exceeds
//          threshold. Set to expected RMS level for RF front end noise.
//          Sample > +THRESHOLD : sample = +LIMIT_VALUE
//          sample < -THRESHOLD : sample = -LIMIT_VALUE


enum class DfeR1BitPos {
    kLimit = 0,
    kThreshold = 10,
    kEndDfeR1BitPos
};

enum class DfeR1Width {
    kLimit = 10,
    kThreshold = 10,
    kEndDfeR1Width
};

constexpr uint32_t max_dfe_limit
    = (1 << (int) DfeR1Width::kLimit) - 1;
constexpr uint32_t max_dfe_threshold
    = (1 << (int) DfeR1Width::kThreshold) - 2;
constexpr uint32_t config1v
    = (max_dfe_threshold << (int) DfeR1BitPos::kThreshold)
        | (max_dfe_limit << (int) DfeR1BitPos::kLimit);

union DfeReg1 {
    struct s {
        uint32_t limit:10;
        uint32_t threshold:10;
        uint32_t rem:12;
        s() : limit(max_dfe_limit),
              threshold(max_dfe_threshold),
              rem(0)
        {}
    }__attribute__((packed)) s;
    uint32_t u;
    DfeReg1() :  s()
    {}
};


//
// 2 DFE_CONFIG_2_REG[19:0] Signed 10-bit integer bias values to
// --   subtract from the I and Q signals after ADC input. Update
// --   takes effect at next period start
//  [19:10] DFE_BIAS_VALUE_I[9:0] Type is 10-bit integer,
//  --      6-bit fraction.
//  [9:0] DFE_BIAS_VALUE_Q[9:0] Type is 10-bit integer, 6-bit fraction.

// TDB(epr): add reg 2 def


// DFE Readback Status Registers (V1.5.0) Unspecified bits are 0’s
// Field, Position Name Description
// 0 DFE_CONFIG_0_REG[5:0] command echo
// 1 DFE_CONFIG_1_REG[19:0] command echo
// 2 DFE_CONFIG_2_REG[19:0] command echo
// 3 DFE_CONFIG_4_STATUS[31:0] Sum of all samples over 20 ms period for a
//      result that is scaled by 20*samples_ms / 2^15.
//      Normalize with multiply by 2^9 /
//      20*samples_ms for mean value of 10-bit
//      integer, 6-bit fraction.
// [31:16] EST_MEAN_I_STATUS[15:0] I Mean Estimate, Signed value
// [15:0] EST_MEAN_Q_STATUS[15:0] Q Mean Estimate. Signed value

// TDB(epr): add reg 3 def

//
// 4 DFE_CONFIG_5_STATUS[31:16] Sum of magnitudes of all samples over 20 ms
//      period with result scaled by 20*samples_ms /
//      2^14.
//  [31:16] EST_MEANMAG_I_STATUS[15:0] I Mean Magnitude Estimate. Unsigned value
//      Actual STDEV = Estimate / 0.7979
//  [15:0] EST_MEANMAG_Q_STATUS[15:0] Q Mean Magnitude Estimate. Unsigned value
//      Actual STDEV = Estimate / 0.7979

// TDB(epr): add reg 4 def

//
// 5 LIMIT_COUNT_STATUS_REG[17:0] LIF I&Q limit detections. Total Detected over
//      a msec period.

// TDB(epr): add reg 5 def

struct DfeType
{
    DfeReg0 c0;
    DfeReg1 c1;
    uint32_t config2;
};


// DIF register defitions
////
// DIF_CONFIG_0_REG[11:0] DIF Configuration Register
// Field position, Name Description
// [11:10] DIF_GAIN_STEP_SIZE Gain Step Size Setting
//  0: 0 db
//  1: +6db
//  2: +9.5db
//  3: +12 db
// [9] DIF_TEST_SIGNAL_INIT Clears FIFO Errors and Reset the FIFOs
// [8] DIF_TEST_SIGNAL_IN_ENABLE Playback FIFO Input Enable
//      Effective at next msec
// [7] DIF_TEST_SIGNAL_OUT_ENABLE Capture FIFO Output Enable
//      Effective at next msec
// [6:5] DIF_TEST_SIGNAL_IN_SELECT[1:0] Downconverter Input select
//  0: Selects DFE input
//  1: Selects Notch Filter Input
// [4:2] DIF_TEST_SIGNAL_OUT_SELECT[2:0] Capture FIFO Data Select
//  6: Selects BSB Data Output
//  5: Selects ASB Data Output
//  4: Selects Gain Step Output
//  3: Selects Notch Filter Output
//  2: Selects Down Converter Output
//  1: Selects Down Converter input
// [1] DIF_INIT initialize the DIF at start of next
//      millisecond
// [0] DIF_ENABLE DIF Operational Enable
//      Effective at next msec

enum class DifR0BitPos {
    kOperationalEnable = 0,
    kInit = 1,
    kTestSignalOutSelect = 2, // three bits
    kTestSignalInSelect = 5, // two bits
    kCaptureFifoOutputEnable = 7,
    kPlaybackFifoOutputEnable = 8,
    kTestSignalInit = 9,
    kGainStepSize = 10, // two bits
    end_dif_bit_pos
};

enum class DifR0BitWidth {
    kTestSignalOutSelect = 3,
    kTestSignalInSelect = 2,
    kGainStepSize = 2,
    kEndDifBitWidth
};

enum class TSigOut {
    kDownConverterInput = 1,
    kDownConverterOutput = 2,
    kNotchFilterOutput = 3,
    kGainStepOutput = 4,
    kAsbDataOutPut = 5,
    kBsbDataOutPut = 6,
    kEndTSigOut
};
enum class TSigIn {
    kDfeInput,
    kNotchFilterInput = 1,
    kEndTSigIn
};

// DIF_CONFIG_1_REG[23:0] LIF to ZIF offset frequency. Twoscompliment value.
// [23:0] DIF_DOWNCONV_FREQ[23:0] FREQ = MOD(ROUND(-fo/fs * 2^24),2^24)
// where, fo = RF Center Frequency offset,
//      fs = IF clock rate, E5typ = 66.25 Msps.
//      Value is effective at next msec.
//      Typ(E5LIF = -0.705MHz) => 0x02B967
//      Typ(E5LIF = +1.795MHz) => 0xF91058
//      Typ(E5RIF = +0.314MHz) => 0xFEC962

// DIF_CONFIG_2_REG[23:0] A & B sideband split downconverter
//      frequency. Twos-compliment value.
//   [23:0] DIF_SIDEBAND_FREQ[23:0] FREQ = MOD(ROUND(fc/fs * 2^24),2^24)
//      where fc = Sideband center frequency, fs
//      = IF clock rate, E5typ = 66.25 Msps. Value
//      is effective at next msec.
//      Typ(E5LIF = +15.346MHz) => 0x3B4B9F
//      Typ(E5RFS) = TBD

// DIF_CONFIG_3_REG[15:0] Programmable coefficients for anti-alias
//      lowpass filter before fractional rate
//      decimator. Recommended coefficient set
//      for fs=66.25 => [6 -5 -18 8 78 118]
//   [15:8] DIF_ALPF_H0[7:0] Recommended coefficient set for 6
//   [7:0] DIF_ALPF_H1[7:0] Recommended coefficient set for -5

// DIF_CONFIG_4_REG[31:0] Programmable coefficients for anti-alias
//      lowpass filter before fractional rate
//      decimator. Recommended coefficient set
//      for fs=66.25 => [6 -5 -18 8 78 118]
//   [31:24] DIF_ALPF_H2[7:0] Recommended coefficient set for -18
//   [23:16] DIF_ALPF_H3[7:0] Recommended coefficient set for 8
//   [15:8] DIF_ALPF_H4[7:0] Recommended coefficient set for 78
//   [7:0] DIF_ALPF_H5[7:0] Recommended coefficient set for 118

// DIF_CONFIG_5_REG[31:0] Complex Equalizer Coefficients
//   [31:24] DIF_EQ_H0I[7:0] EQ H0 I Coefficient
//   [23:16] DIF_EQ_H0Q[7:0] EQ H0 Q Coefficient
//   [15:8] DIF_EQ_H1I[7:0] EQ H1 I Coefficient
//    [7:0] DIF_EQ_H1Q[7:0] EQ H1 Q Coefficient

// DIF_CONFIG_6_REG[31:0]
//   [31:24] DIF_EQ_H2I[7:0] EQ H2 I Coefficient
//   [23:16] DIF_EQ_H2Q[7:0] EQ H2 Q Coefficient
//   [15:8] DIF_EQ_H4I[7:0] EQ H4 I Coefficient **
//   [7:0] DIF_EQ_H4Q[7:0] EQ H4 Q Coefficient

// DIF_CONFIG_7_REG[31:0]
//   [31:24] DIF_EQ_H5I[7:0] EQ H5 I Coefficient
//   [23:16] DIF_EQ_H5Q[7:0]
//   [15:8] DIF_EQ_H6I[7:0] EQ H6 I Coefficient
//   [7:0] DIF_EQ_H6Q[7:0]

// DIF_CONFIG_8_REG[19:0] Notch 1 Filter Configuration.
//   [19] NOTCH_1_ENABLE Notch 1 Enable
//   [18:16] NOTCH_1_BW[2:0] Notch 1 Bandwidth setting.
//      Notch filter 20 dB 2-sided bandwidth in
//      Hz based on sample rate fs in MHz
//       0 => 62*fs,
//       1 => 126*fs,
//       2 => 254*fs,
//       3 => 514*fs,
//       4 => 1062*fs,
//       5 => 2280*fs
//   [15:0] DIF_NOTCH_1_FREQ[15:0] Notch 1 filter center frequency setting,
//      FREQ_CFG = dec2hex(mod(round(fc/fs * 2^16), 2^16), 4),
//      where fs is sample rate
//      set to Rec IF clock rate, default is
//      66,250 Ksps

// DIF_CONFIG_9_REG[19:0] Notch 2 Filter Configuration.
//   [19] DIF_NF_2_ENABLE Notch 2 Enable
//   [18:16] DIF_NF_2_BW[2:0] Notch 2 Bandwidth setting.
//      Notch filter 20 dB 2-sided bandwidth in
//      Hz based on sample rate fs in MHz
//       0 => 62*fs,
//       1 => 126*fs,
//       2 => 254*fs,
//       3 => 514*fs,
//       4 => 1062*fs,
//       5 => 2280*fs
//   [15:0] DIF_NF_2_FREQ[15:0] Notch 2 filter center frequency setting,
//      FREQ_CFG = dec2hex(mod(round(fc/fs *
//      2^16), 2^16), 4), where fs is sample rate
//      set to Rec IF clock rate, default is
//      66,250 Ksps

// DIF_CONFIG_10_REG[19:0] Notch 3 Filter Configuration
//   [19] DIF_NF_3_ENABLE Notch 3 Enable
//   [18:16] DIF_NF_3_BW[2:0] Notch 3 Bandwidth setting.
//      Notch filter 20 dB 2-sided bandwidth in
//      Hz based on sample rate fs in MHz
//       0 => 62*fs,
//       1 => 126*fs,
//       2 => 254*fs,
//       3 => 514*fs,
//       4 => 1062*fs,
//       5 => 2280*fs
//  [15:0] DIF_NF_3_FREQ[15:0] Notch 3 filter center frequency setting,
//      FREQ_CFG = dec2hex(mod(round(fc/fs *
//      2^16), 2^16), 4), where fs is sample rate
//      set to Rec IF clock rate, default is
//      66,250 Ksps
// DIF_CONFIG_11_REG[19:0] Notch 4 Filter Configuration
//   [19] DIF_NF_4_ENABLE Notch 4 Enable
//   [18:16] DIF_NF_4_BW[2:0] Notch 4 Bandwidth setting.
//      Notch filter 20 dB 2-sided bandwidth in
//      Hz based on sample rate fs in MHz
//       0 => 62*fs,
//       1 => 126*fs,
//       2 => 254*fs,
//       3 => 514*fs,
//       4 => 1062*fs,
//       5 => 2280*fs
//   [15:0] DIF_NF_4_FREQ[15:0] Notch 4 filter center frequency setting,
//      FREQ_CFG = dec2hex(mod(round(fc/fs *
//      2^16), 2^16), 4), where fs is sample
//      rate set to Rec IF clock rate, default
//      is 66,250 Ksps
//      Field, Position, Name Description
//  DIF_CONFIG_0_REG[23:0]
//   [23:16] TEST_SIGNAL_STATUS[7:0] External FIFO Status
//   [23] [7] FIFO_WRITE_ERROR Any Capture FIFO Write error since
//      last DIF_Init Command. Cleared with a
//      FIFO_Init Command
//   [22] [6] FIFO_WRITE_ALMOST_FULL Capture FIFO almost full status
//   [21] [5] FIFO_WRITE_DATA_READY Capture FIFO Ready to Accept Data
//      status
//   [20] [4] TEST_SIGNAL_OUT_ENABLE Capture FIFO output Enable status
//   [19] [3] FIFO_READ_ERROR Any Playback FIFO Read error since
//      last DIF_Init Command. Cleared with a
//      FIFO_Init Command
//   [18] [2] FIFO_READ_ALMOST_EMPTY Playback FIFO almost Empty status
//   [17] [1] FIFO_READ_DATA_VALID Playback FIFO Data Ready to output
//      status
//   [16] [0] TEST_SIGNAL_IN_ENABLE Playback FIFO Input Enable status
//   [11:0] DIF_CONFIG_0_REG[11:0] command echo

// DIF_CONFIG_1_REG[23:0] command echo
// DIF_CONFIG_2_REG[23:0] command echo
// DIF_CONFIG_3_REG[15:0] command echo
// DIF_CONFIG_4_REG[31:0] command echo
// DIF_CONFIG_5_REG[31:0] command echo
// DIF_CONFIG_6_REG[31:0] command echo
// DIF_CONFIG_7_REG[31:0] command echo
// DIF_CONFIG_8_REG[19:0] command echo
// DIF_CONFIG_9_REG[19:0] command echo
// DIF_CONFIG_10_REG[19:0] command echo
// DIF_CONFIG_11_REG[19:0] command echo
//
struct DifType // digital interface type
{
    uint32_t difConfig0;
    uint32_t difConfig1;
    uint32_t difConfig2;
    uint32_t difConfig3;
    uint32_t difConfig4;
    uint32_t difConfig5;
};

// TODO(epr) define DPB
struct DpbType
{
    uint32_t dbpConfig0;
    uint32_t dbpConfig1;
    uint32_t dbpConfig2;
    uint32_t dbpConfig3;
    uint32_t dbpConfig4;
    uint32_t dbpConfig5;
    uint32_t dbpConfig6;
};


struct SsgType
{
    uint32_t config0;
    uint32_t config1;
    uint32_t config2;
    uint32_t config3;
    uint32_t config4;
};

constexpr uint32_t kLifMode = ((int) DfeSourceSelect::kLifSignal
                               << (int) DfeR0BitPos::kSourceSelect);
constexpr uint32_t kRfsMode = ((int) DfeSourceSelect::kRfsSignal
                               << (int) DfeR0BitPos::kSourceSelect);
static_assert((int) kLifMode == 2);
static_assert((int) kRfsMode == 1);

} // namespace registers

# endif // FW_INC_HWREGS_RECEIVER_H_
