
/*
    Define the message structures.
 */

# include <algorithm>
# include <cassert>
# include <cstdlib>
# include <deque>
# include <iostream>
# include <iomanip>
# include <string>
#include <typeinfo>
# include <vector>

# include <unistd.h>



class Ticks
{
public:
    struct Node
    {
        int dticks;
        const char* thing;
        int repeat;
        Node()
          : dticks(-1), thing(nullptr), repeat(0)
        { }
    };
    Node* nodes;

    std::deque<Node*> freeNodes;
    std::deque<Node*> schedule;

    explicit Ticks() {
        nodes = new Node[NODES];
        Node* np = nodes;
        for (int i = 0; i < NODES; i++, np++) {
           freeNodes.push_back(np);
        }
    }

    bool queNode(Node* np)
    {
        // needs lock
        assert(schedule.size() + freeNodes.size() == (unsigned) NODES - 1);
        auto dt = np->dticks;
        if (!schedule.empty()) {
            //std::cout << "q:" << ':' << np->dticks << std::endl;
            // walk list, until dticks <= 0;
            for (auto si = schedule.begin(); si != schedule.end(); si++) {
                dt -= (*si)->dticks;
                //std::cout << "c:" << (*si)->dticks << ':' << np->dticks << ':' << dt << ':' << std::endl;
                if (dt <= 0) { // insert here!
                    np->dticks = (*si)->dticks + dt;
                    (*si)->dticks = -dt;
                    schedule.insert(si, np);
                    //std::cout << "i:" << np << ':' << np->dticks << ':' << dt << ":T:" << std::endl;
                    return true;
                }
            }
        }
        else {
            // special case for empty queue.
            dt = np->dticks;
        }

        // at end!
        np->dticks = dt;
        schedule.push_back(np);
        //std::cout << "e:" << np << ':' << np->dticks << ':' << dt << ":T:" << std::endl;
        return true;
    }

    size_t room()
    {
        //std::cout << ":room:" << freeNodes.size() << std::endl;
        return freeNodes.size();
    }

    size_t scheduled()
    {
        //std::cout << ":A:" << schedule.size() ;
        return schedule.size();
    }

    int take(const char** rv)
    {
        // needs lock
        Node* np = schedule.front();
        assert(np != nullptr);
        int rticks = np->dticks;
        *rv = np->thing;
        schedule.pop_front();
        if (np->repeat) {
            np->dticks = np->repeat;
            //schedule.push_back(np);
            return queNode(np);
        }
        else {
            freeNodes.push_back(np);
        }
        return rticks;
    }

    bool schedTick(int dticks, const char* thing, bool repeat=false)
    {
        // needs lock
        assert(!freeNodes.empty());
        Node*& np = freeNodes.front();
        np->dticks = dticks;
        np->thing = thing;
        if (repeat)
            np->repeat = dticks;
        freeNodes.pop_front();
        return queNode(np);
    }

    bool findTick(const char* thing)
    {
        auto matchit = [thing](auto node)->bool {
            std::cout << "MI:" << ':' << thing << std::endl;
            if (node == nullptr)
                return false;
            return thing == node->thing;
        };
        auto findit = [matchit](std::deque<Node*> dq) {
            auto it =  find_if(dq.begin(), dq.end(), matchit);
            //std::cout << "FI:" << (*it)->thing << std::endl;
            return it;
        };
        auto ni = findit(schedule);
        std::cout << "CMP" << (ni == schedule.end()) << std::endl;
        if ((*ni) != nullptr) {
            std::cout << "NI:" << typeid(ni).name() << std::endl;
            std::cout << std::setw(4) << (*ni)->dticks
                                      << ':' << (*ni)->repeat
                                      << ':' << (*ni)->thing
                                      << std::endl;
        }
        else
            std::cout << thing << " not found" << std::endl;
        return true;
    }


    void dump_sched()
    {
        //std::cout << "SFree:" << freeNodes.size() << std::endl;
        //std::cout << "SSched:" << schedule.size() << std::endl;
        //std::cout << "\n:" << ": D: ";
        for (auto si = schedule.begin(); si != schedule.end(); si++) {
            assert(*si != nullptr);
            std::cout << std::setw(4) << (*si)->dticks
                                      << ':' << (*si)->repeat
                                      << ':' << (*si)->thing
                                      ;
        }
        std::cout << "\n";
    }
private:
    static constexpr int NODES = 10;
};


int main(int argc, char** argv)
{
    Ticks ticks = Ticks();
    std::vector<uint32_t> first_dts= {1, 2, 3, 7, 9};
    const char* first_names[] = {"A", "B", "C", "G", "I", "X"};
    for (size_t j = 0; j < first_dts.size(); j++) {
        ticks.schedTick(first_dts[j], first_names[j], true);
    }
    ticks.dump_sched();
    //std::string in;
    //std::cin >> in;
    //std::cout << "<enter>" << std::flush;
    for (int i = 0; i < 30; i++) {
        const char* rthing = nullptr;
        int stks = ticks.take(&rthing);
        std::cout << stks << ':' << rthing << ".." << std::flush;
        ticks.dump_sched();
        std::cout << "FIND" << first_names[4] << std::endl << std::flush;
        ticks.findTick(first_names[4]);
        std::cout << "FIND" << first_names[5] << std::endl << std::flush;
        ticks.findTick(first_names[5]);
    }
    return 0;
        
# if 0
    for (int i = 0; i < 30000; i++) {
        int rv = rand() % ticks.room();
        //std::cout << "loop:" << i << ':' << ticks.room() <<  ":RV:" << rv << " tschedts:" << ticks.scheduled() << std::endl;
        for (; rv > 0; rv--) {
            int dticks = rand() % 19;
            bool r = ticks.schedTick(dticks, argv++);
            std::cout << "R:" << dtick << ':' << std::endl;
            assert(r);
        }
        ticks.dump_sched();
        rv = rand() % ticks.scheduled();
        //std::cout << "TAKE:(" << ticks.scheduled() << "):RV:" << rv << "..." << std::flush;
        //return 0;
        for (; rv > 0; rv--) {
            char* rthing = nullptr;
            int stks = ticks.take(rthing);
            //if (stks == 0)
            //    std::cout << "---" << std::flush;
            std::cout << stks << ':' << rthing << ".." << std::flush;
            sleep(stks);
            std::cout << "<enter>" << std::flush;
            std::cin >> in;
        }
        std::cout << "EL:" << i << std::endl;
        ticks.dump_sched();
# if 0
        char c;
        std::cout << "<enter>" ;
        std::cin >> c;
# endif
    }
    //std::cout << "ETAKE:(" << ticks.scheduled() << "):RV:" <<std::flush;
    ticks.dump_sched();
    int x = 0;
    while (ticks.scheduled()) {
        int stks = ticks.take();
        std::cout << stks << "::";
        if ((x & 1) == 0) {
            x++;
            std::cout << "sleep:" << stks << std::flush;
            sleep(stks);
            ticks.dump_sched();
            std::cout << "<enter>" << std::flush;
            std::cin >> in;
        }
    }
# endif

}

