/*
//
// $Id: //Source/Common/thread/ThreadPool.h#12 $
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Author: Gordie
//
// Brief Description: Thread Pool and related
//
*/

#ifndef __THREADPOOL_H
#define __THREADPOOL_H

# include "dotcastns.h"
# include "dctype.h"
# include "dcexception.h"

__BEGIN_DC_NAME_SPACE

//
// Forward references
//
class PWorkerThread;
class PThreadPoolHelper;
class PWorkerThreadDelegate;

//
// Protocol representing a Pool of Threads which sit around
// and wait for requests to do work. Requests are made to the
// ThreadPool with context data releated to the specific work
// request. On a request for work, the ThreadPool will wake up
// the next available thread after handing it the context data
// for the request.
//
// When a ThreadPool is constructed, a fixed number of WorkerThread
// Objects are created and added to the Pool. For each WorkerThread,
// a WorkerThreadDelegate instance is also created. The exact class
// of the WorkerThreadDelegate is specified through a Template
// parameter to the CThreadPool<T> constructor as the parameter T
// (see below on how to construct a ThreadPool). If the optional
// initialization callback and initialization data are passed in
// to the CThreadPool constructor, the callback is invoked for each
// delegate, passing in the ThreadPool Object, the delegate, and the
// initialization data.
//
// (Note the callback is a function or static class method, not
// an instance method).
//
// To cleanup and free a ThreadPool, the client should delete 
// the PThreadPool Object, which will notify all of the Threads
// in the ThreadPool to exit and delete themselves (and their
// WorkerThreadDelegate Object). Note that Threads which are
// currently busy will not exit until after they complete
// whatever work they are engaged in. If such threads need to
// be interrupted, a protocol particular to the ThreadWorkerDelegate
// must be used.
//
// Instances of this class cannot be created directly, clients of
// a ThreadPool must create a CThreadPool<T> template class with
// the template parameter specifying the PWorkerThreadDelegate
// class that performs work for each of the ThreadPool's threads.
//
// As an example, to create a ThreadPool with one MyWorker Object
// per thread, use the following:
//
// PThreadPool pool =
//	new CThreadPool<MyWorker>(numThreads, initCallback, initData);
//
class PThreadPool
{
public:
	// Request a Thread in the ThreadPool to call the ThreadWorkerDelegate
	// to do work using the context data.
	virtual PWorkerThread* requestWork (void* contextData, bool block = true, bool create = false) = 0;

	// Destructor. Note that deleting a ThreadPool will cause
	// it to stop all Threads after they have finished any
	// work they are currently engaged in, and delete all
	// of the WorkerThread and WorkerThreadDelegate Objects.
	virtual ~PThreadPool () {}

	// Internal routines, not part of the public protocol
	// but C++ sometimes coerces one into non optimal confusion
	// such as putting these definitions here.
	virtual PWorkerThreadDelegate* createWorker() const = 0;

protected:
	PThreadPool () {}

private:
	PThreadPool (const PThreadPool& rhs);
	PThreadPool& operator=(const PThreadPool& rhs);
};

//
// Protocol for actual Worker Objects that are invoked
// by ThreadPool WorkerThreads when they are woken up.
//
// There is a one to one correspondance between a WorkerThread
// in a ThreadPool and a WorkerThreadDelegate Object. When
// a request is made to the ThreadPool to do work (by calling
// the method requestWork), a WorkerThread Object is returned 
// which has a corresponding WorkerThreadDelegate which will
// be called from within the WorkerThread's thread after context
// data is set by calling the method setData.
//
class PWorkerThreadDelegate
{
public:
	PWorkerThreadDelegate () {}
	virtual ~PWorkerThreadDelegate () {}

	// Method to set per invocation data for 
	// each invocation of work. This is called
	// from the requester's thread, and is guaranteed
	// to only be called when the worker is idle.
	virtual void setData (void* contextData) = 0;

	// Method to actually perform work, called from the
	// ThreadPool's WorkerThread corresponding to this
	// Worker Object.
	virtual void work () = 0;

	// Called when an uncaught exception occurs during
	// a call to work(). Returns whether or not to 
	// exit the mainloop and destroy the thread (removing
	// it from the ThreadPool. Note that the XException will
	// be deleted by the caller, so if it is to be retained, a
	// copy must be made.
	virtual bool handleException(XException* pex) { return false; /* Keep running by default */ }
	virtual bool handleUnknownException() { return false; /* Keep running by default */ }

private:
	PWorkerThreadDelegate (const PWorkerThreadDelegate& rhs);
	PWorkerThreadDelegate& operator=(const PWorkerThreadDelegate& rhs);
};

//
// Protocol representing the actual Thread that does work. This
// is exposed at the protocol level to the client since it is
// returned from the ThreadPool requestWork() method in order to
// let the client have their way with the WorkerThreadDelegate,
// which is typically a class of their own design.
//
class PWorkerThread
{
public:
	// Gets the delegate Object
	virtual PWorkerThreadDelegate* getDelegate () const = 0;

	PWorkerThread () {}
	virtual ~PWorkerThread () {}

private:
	PWorkerThread (const PWorkerThread& rhs);
	PWorkerThread& operator=(const PWorkerThread& rhs);
};

//
// Function called for each WorkerThread to initialize state
//
typedef void (*ThreadPoolInitializationCallback) (const PThreadPool* pool, PWorkerThreadDelegate* worker, void* data);

//
// Protocol for internal Class used by ThreadPool to encapsulate
// the use of threads and monitors/etc. to implement the private
// guts of the Pool.
//
// This is not part of the public API and is present in this file only because
// CThreadPool (which must be in this header file as it has template methods)
// uses it.
//
class PThreadPoolHelper
{
public:
	static PThreadPoolHelper* CreateThreadPoolHelper (size_t size, const PThreadPool* pool, 
		ThreadPoolInitializationCallback callback, void* initializationData);
	PThreadPoolHelper () {}
	virtual ~PThreadPoolHelper () {}

	virtual PWorkerThread* CreateEphemeralWorkerThread(void* contextData, ThreadPoolInitializationCallback callback, void* data) const = 0;
	virtual PWorkerThread* getWorkerThread(void* contextData, bool block) = 0;
	virtual void shutdown () = 0;

private:
	PThreadPoolHelper (const PThreadPoolHelper& rhs);
	PThreadPoolHelper& operator=(const PThreadPoolHelper& rhs);

};

//
// This is the actual ThreadPool class instantiated by
// clients (or rather it is the Template for such
// classes) and causes no end of grief trying to hide
// the implementation details of the class yet still 
// expose the Template to allow ThreadPools based on
// a specified WorkerThreadDelegate class to be created.
//
// Oh, for a proper Class Object to parameterize this
// class with!
//
// Although this is the class clients will use, they need
// not look at the interface here as they will only use the
// interface defined in PThreadPool, and the constructor for
// this class as documented in PThreadPool.
//
template <class T>
class CThreadPool : public PThreadPool
{
public:
	CThreadPool (size_t size, ThreadPoolInitializationCallback callback = 0, void* initializationData = 0) :
	  m_pCallback(callback), m_pInitializationData(initializationData)
	{
		m_pHelper = PThreadPoolHelper::CreateThreadPoolHelper(size, this, callback, initializationData);
	}
	virtual ~CThreadPool () { m_pHelper->shutdown(); delete m_pHelper; }

	virtual PWorkerThread* requestWork (void* contextData, bool block = true, bool create = false) {
		PWorkerThread* thread = m_pHelper->getWorkerThread(contextData, block);
		if ((thread == 0) && (create == true)) {
			thread =  m_pHelper->CreateEphemeralWorkerThread(contextData, m_pCallback, m_pInitializationData);
		}
		return thread;
	}

	virtual PWorkerThreadDelegate* createWorker() const {
		T* worker = new T();
		return worker;
	}

private:
	CThreadPool (const CThreadPool& rhs);
	CThreadPool& operator=(const CThreadPool& rhs);

	PThreadPoolHelper* m_pHelper;
	ThreadPoolInitializationCallback m_pCallback;
	void* m_pInitializationData;

};

__END_DC_NAME_SPACE

#endif // __THREADPOOL_H

