# Microsoft Developer Studio Generated NMAKE File, Based on thread.dsp
!IF "$(CFG)" == ""
CFG=thread - Win32 Debug
!MESSAGE No configuration specified. Defaulting to thread - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "thread - Win32 Release" && "$(CFG)" != "thread - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "thread.mak" CFG="thread - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "thread - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "thread - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "thread - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\thread.lib"


CLEAN :
	-@erase "$(INTDIR)\message.obj"
	-@erase "$(INTDIR)\messageQueue.obj"
	-@erase "$(INTDIR)\ThreadPool.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\thread.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /GX /O2 /I ".\\" /I "..\exception" /I "..\headers" /I "win32" /D "NDEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\thread.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"$(OUTDIR)\thread.lib" 
LIB32_OBJS= \
	"$(INTDIR)\message.obj" \
	"$(INTDIR)\messageQueue.obj" \
	"$(INTDIR)\ThreadPool.obj"

"$(OUTDIR)\thread.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "thread - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\thread.lib"


CLEAN :
	-@erase "$(INTDIR)\message.obj"
	-@erase "$(INTDIR)\messageQueue.obj"
	-@erase "$(INTDIR)\ThreadPool.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\thread.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Gm /GX /ZI /Od /I ".\\" /I "..\exception" /I "..\headers" /I "win32" /D "_DEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\thread.bsc" 
BSC32_SBRS= \
	
LIB32=link.exe -lib
LIB32_FLAGS=/nologo /out:"$(OUTDIR)\thread.lib" 
LIB32_OBJS= \
	"$(INTDIR)\message.obj" \
	"$(INTDIR)\messageQueue.obj" \
	"$(INTDIR)\ThreadPool.obj"

"$(OUTDIR)\thread.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("thread.dep")
!INCLUDE "thread.dep"
!ELSE 
!MESSAGE Warning: cannot find "thread.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "thread - Win32 Release" || "$(CFG)" == "thread - Win32 Debug"
SOURCE=.\message.cpp

"$(INTDIR)\message.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\messageQueue.cpp

"$(INTDIR)\messageQueue.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ThreadPool.cpp

"$(INTDIR)\ThreadPool.obj" : $(SOURCE) "$(INTDIR)"



!ENDIF 

