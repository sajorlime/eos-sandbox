/*
//
// $Id$
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Filename: messageQueue.h
//
// Creation Date: 01-16-00
//
// Last Modified:
//
// Author: emil
//
// Description:
    Defines concrete class for message receivers using a safeFIFO.
   Note:
//
// 
*/

/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
*
    MessageQueue.h
*
*
    Contains defintion of a class that allow inter-thread communication.
*
*
*
     Date     Description
    ------   -----------
*
*
* NOTES:
    This class implements a concrete CMessageReceiver class for inter-thread
    communicaiton.
    This class could be adapted to inter-process communicaiton by using
    CSemaphore instead of a critical section.
*
*
* Classes/Types:
    CMessageQueue -- object that queue messsages and blocks reciever
    when no messages are availble.
*
*/


# ifndef __MESSAGEQUEUE_H__
# define __MESSAGEQUEUE_H__

# include "message.h"
# include "safeFIFO.h"


__BEGIN_DC_NAME_SPACE

/*
    A message queue is inter-thread communicaiton object.  It allows
    one reciever and multiple senders.
    The class extends CMessageReceiver, which
    is passed to clients to allow them to send messages.

    Issue: The class probably should limit the maximum messages
    that it can allocate.
*/
class CMessageQueue : public CMessageReceiver
{
    friend class CMessageReceiverFactory;
private:
    TSafeFIFO<CMessage *> m_msgQueue;
    TSafeFIFO<CMessage *> m_freeFIFO;
    int m_waiting;
    PEvent * m_pMessageEvent;
    PCriticalSection * m_pAccess;

protected:
    enum {defGrowFactor = 2};

public:
    // from CMessageReceiver
    virtual CMessage * allocMessage (void);
    virtual void release (CMessage * pMsg);
    virtual bool send (CMessage * msg);
    virtual CMessage * receive (void);
    virtual void failed (const XException & reason);

    // CMessageQueue extentions
    CMessageQueue (int maxMsgSize = defMsgSize,
                   int freeCount = defFreeCount,
                   int growFactor = defGrowFactor,
                   int blockFlags = CMessageReceiver::blockOnSendFull
                                    | CMessageReceiver::blockOnReceive
                  );
    virtual ~CMessageQueue ();
    CMessage * alloc (void) {return allocMessage();}
    virtual bool waitForEmpty (void)
    {
        return CMessageReceiver::waitForEmpty ();
    }
};

__END_DC_NAME_SPACE

# endif

