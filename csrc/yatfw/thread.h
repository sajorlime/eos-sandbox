
/*
//
// $Id$
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Filename: thread.h
//
// Creation Date: 01-12-00
//
// Last Modified: 01-19-00
//
// Author: emil
//
// Description:
    Defines the synchronization primatives used in doccast software.
    The defi
   Note:
    Each platform will have a seperate implementation for these classes.
    These implementations are located in a subdirectory of with the platform name.       
//
// Change History:
//
// 01/19/00 MJF: added Linux/Pthread conditional compiliation  
*/


/* original header
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
*
    Thread.h
*
*
* Defines Thread class
*
      Date     Description
    --------   -----------
*
* Notes:
*
*
* Classes/Types:
    CThread -- is a class that encapsulates a simple notion of a thread.
    CThread::ThreadState -- enumeration of thread states.
    CThread::ThreadExit -- enumeration of the exit codes for a thread.
    CThreadList -- is a class that keeps track of all threads (Linux only)
*
*/


# ifndef __THREAD_H__
# define __THREAD_H__

# include "dctype.h"
# include "dotcastns.h"
# include "dcsynchronization.h"

__BEGIN_DC_NAME_SPACE


# define INHERIT_THREAD_PRIORITY ((uint32) -1)
# define NORMAL_THREAD_PRIORITY ((uint32) -2)

class CThread;
// Notification handler returns true to indicate it
// deleted the CThread Object, false to indicate not.
typedef bool (*ThreadCallback) (CThread* thread);

/*
    CThread is a class that implements a simplified threading model.
*/
class CThread
{
public:
    /* 
        The thread state values are designed to allow a simple truth
        test on it be a "running thread."
    */
    enum ThreadState
    {
        ts_terminated = 0,
        ts_running,
        ts_sleeping,
		ts_suspended,
		ts_initing,
        last_ThreadState
    };
    enum ThreadExit
    {
        te_ok = 0,
        te_terminated,
        te_failure,
        last_ThreadExit
    };
private:
    static int next_id;
    int m_id;
    OSHANDLE m_hThread;
    unsigned long  m_idThread;
    int            m_RetCode;

    PEvent * m_event;
    PEvent * m_wake;
	PEvent * m_xThreadEntered;
	ThreadCallback m_pCallback;
	static bool DefaultThreadNotificationHandler (CThread *)
    {
        return false;
    }

protected:
    void keepalive() { m_increment++; }
    unsigned int m_increment;
    ThreadState m_run;
    // thread is call by xThread, inheriting classes define to run
    virtual int thread (void) = 0;

public:

  int id() const { return m_id; }
  unsigned int status() const { return m_increment; }

    static unsigned long xThread (void * pParam);

	/*
	 Final method (don't override) to set callback handler
	 called when Thread exits. Previous callback handler
	 is returned, so they can be chained together.

  	 It is safe to delete the Thread from inside this method,
	 as long as you know that no other threads are waiting on
	 a join() call to this thread.

	 Return true to indicate you deleted the thread and it is
	 no longer safe to mess with it.
	 
	 When calling the previous callback in the chain, perform
	 all of your work before calling the previous callback,
	 and then wait until it returns to delete the Object. (If the
	 previous callback returns true, then don't delete it as the
	 previous callback did.)

	 Word to the wise: Don't declare a CThread Object on the stack
	 and then register a notification handler that frees it.
	 */
	ThreadCallback setNotificationHandler (ThreadCallback handler);

    /*
        dumps information about the thread state.
    */
    void dump (void);

    ThreadState running (void) {return m_run;}

    OSHANDLE getThreadHandle (void) {return m_hThread;}

    virtual bool start (void);

    /*
    Wait until the thread has terminated.  Returns false if
	the thread was detached. Returns true if it has terminated.
	the optional pointer returns the int value returned by
	the thread when it exited.
    */
    virtual bool join(int *pReturn = 0);

    /*
        sleep is called by the thread to sleep in a way that allows it
        to be woken with the wake call.  The default is an infinite
        wait which requires that wake be called.
    */
    virtual bool sleep (uint32 msSleep = INFINITE_TIMEOUT);

    /*
        wake -- cause a thread to leave the sleep state.
    */
    virtual bool wake (void);

    /*
        The constructor's first parameter is the threads priority, it
        can be a small valued integer which is system dependent or it
        can be either INHERIT_THREAD_PRIORITY or NORMAL_THREAD_PRIORITY.
        Any integer that is not in the legal range for the system will
        be interpreted at NORMAL_THREAD_PRIORITY.  Check with the
        system specific library documentation for the legal values.
    */
    CThread (uint32 priority = INHERIT_THREAD_PRIORITY, bool waitForStart = true);

    virtual ~CThread ();

    /*
        Yield to other threads and processes.
    */
    virtual void yield(void);
};

# if defined(__unix__)

//
// HACK: Keep track of all of the CThreads running in the system.
//
// The implementation here is very simple minded.
//

#define NTHREADS 100

class CThreadList 
{
  CThreadList();
  CThread *m_pThreads[NTHREADS];
  PRwLock *m_pRwlock;
public:
  ~CThreadList();
  static CThreadList s_ThreadList;
  void addThread(CThread *thread);
  void deleteThread(CThread *thread);
  CThread * const * threads() const
  {
      return m_pThreads;
  }
};

#endif

__END_DC_NAME_SPACE


# endif

