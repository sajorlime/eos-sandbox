/*
//
// $Id: //Source/Common/thread/VxWorks/VxWorksSynchronization.cpp#9 $
//
// Copyright (c) 1999-2002 Dotcast Inc.
// All rights reserved.
//
// Filename: winsynchronization.cpp
//
// Creation Date: 07-31-02
//
// Last Modified: 08-01-02
//
// Author: emil
//
// Description:
    Implements dotcast synchronization primatives for VxWorks
//
// 
*/

# include "dctype.h"
# include "dotcastns.h"
# include "dcsynchronization.h"
# include <vxWorks.h>
# include <taskLib.h>
# include <semLib.h>
# include <string.h>

# ifndef MS_TO_TICKS 
// the default behavior is to assume ticks are milliseconds
#   define MS_TO_TICKS(Pms) (Pms)
# endif

__BEGIN_DC_NAME_SPACE

class cCriticalSection : public PCriticalSection
{
    friend class FSyncObject;

    SEM_ID m_id;

    cCriticalSection (void) throw (XSynchronization)
    {
        /*
            create a binary semaphore.
            Use a PRIORITY queue and make it inversion safe.
            Inversion safe means that if a higher priority
            thread blocks on the critical section the priority
            of the holding thread will be bumped up (temporarily)
            to the blocked thread.  This is good.
            The critical section is initially available.
            For mutual exclusion semaphores we must us a priority
            queue to make it inversion safe.
            I'm a little uncertian if a mutual-exclusion semaphore
            or a priority semaphore is best here. I have chosen
            a binary semaphore because this is the same behavior
            as the other critical sections in other implementations.
        */
        m_id = ::semBCreate (SEM_Q_FIFO | SEM_INVERSION_SAFE, SEM_FULL);
        if (!m_id)
            throw XSynchronization ("failed  to create critical section");
    }

public:
    bool take (void)
    {
        if (::semTake (m_id, WAIT_FOREVER) == OK)
            return true; // it should ALWAYS exit here!
        return false; // it should never exit here!
    }

    bool tryToTake (void)
    {
        if (::semTake (m_id, NO_WAIT) == OK)
            return true; // exit here if we got it
        return false; // exit here if we did not!
    }

    bool release (void) 
    {
        if (::semGive (m_id) == OK)
            return true; // it should ALWAYS exit here!
        return false; // it should never exit here!
    }

    ~cCriticalSection ()
    {
        ::taskLock ();
        ::semMGiveForce (m_id); // get rid of anyone waiting
        ::semDelete (m_id); // get rid of that thing!
        ::taskUnlock ();
    }
};

/*
        note this does not current have a notion of owner ship should fix!
*/
class cMutex : public PMutex
{
    friend class FSyncObject;

    SEM_ID m_id;
    const char * m_pName;

    cMutex (bool initiallyOwn, const char * pName) throw (XSynchronization)
        :
            m_pName (pName) 
    {
        /*
            create a binary semaphore.
            Use a PRIORITY queue and make it inversion safe.
            Inversion safe means that if a higher priority
            thread blocks on the critical section the priority
            of the holding thread will be bumped up (temporarily)
            to the blocked thread.  This is good.
            For mutual exclusion semaphores we must us a priority
            queue to make it inversion safe.
            I'm a little uncertian if a mutual-exclusion semaphore
            or a priority semaphore is best here. I have chosen
            a binary semaphore because this is the same behavior
            as the other mutexes in other implementations.
        */
        m_id = ::semBCreate (SEM_Q_FIFO | SEM_INVERSION_SAFE,
                           initiallyOwn ? SEM_FULL : SEM_EMPTY);
        if (!m_id)
            throw XSynchronization ("failed  to create mutex");
    }

public:
    bool take (uint32 msTimeout)  
    {
        if (::semTake (m_id, MS_TO_TICKS (msTimeout)) == OK)
            return true;
        return false;
    }

    bool tryToTake (void) 
    {
        if (::semTake (m_id, NO_WAIT) == OK)
            return true;
        return false;
    }

    bool release (void) 
    {
        if (::semGive (m_id) == OK)
            return true;
        return false; // should never get here.
    }

    ~cMutex () 
    {
        taskLock ();
        ::semFlush (m_id); // get rid of everyone
        ::semDelete (m_id); // then delete it
        taskUnlock ();
    }
};

class cSemaphore : public PSemaphore
{
    // FSyncObject::semaphore(..) creates PCriticalSection object.
    friend class FSyncObject;

    SEM_ID m_id;
    const char * m_pName;

    cSemaphore (int32 initialCount,
                int32 maxCount,
                const char * pName) throw (XSynchronization)
        :
            m_pName (pName)
    {
        m_id = ::semCCreate(SEM_Q_PRIORITY, initialCount);
        if (!m_id)
            throw XSynchronization ("failed to create semaphore");
    }

public:
    bool take (uint32 msTimeout) 
    {
        if (::semTake (m_id, (MS_TO_TICKS (msTimeout))) == OK)
            return true; // should always exit here!
        return false;
    }

    bool release (void)  
    {
        if (::semGive (m_id) == OK)
            return true; // should always exit here!
        return false;
    }

    bool release (int32 count, int32 * pPrevCount) throw (XSynchronization)
    {
        throw XSynchronization ("release (int32, int32 *) not implemented");
    }

    ~cSemaphore () 
    {
        taskLock ();
        ::semFlush (m_id); // get rid of everyone
        ::semDelete (m_id); // then delete it
        taskUnlock ();
    }
};


class cEvent : public PEvent
{
    // FSyncObject::event(..) creates a PEvent object.
    friend class FSyncObject;

    SEM_ID m_id;
    const char * m_pName;
    bool m_state;

    cEvent (const char * pName) throw (XSynchronization)
        :
            m_pName (pName)
    {
        m_state = false;
        m_id = ::semBCreate (SEM_Q_FIFO | SEM_INVERSION_SAFE, SEM_EMPTY);
        if (!m_id)
            throw XSynchronization ("failed to create event");
    }

public:
    bool set (void) 
    {
        m_state = true;
        ::semFlush (m_id);
        return true;
    }

    bool pulse (void) 
    {
        m_state = false;
        ::semFlush (m_id);
        return true;
    }

    bool reset (void) 
    {
        m_state = false;
        return true;
    }

    /*
        wait in VxWorks presents a problem because we cannot
        enter the kernel and release a semaphore at the same time.
        Therefor there is a timing whole between the check of
        state and the entering of semTake.  We avoid this
        by waking up periodicly to check the value of m_state.
        Note that if semTake returns with OK we just return true
        because this should only happen if semFlush was called.
    */
    bool wait (uint32 msTimeout) 
    {
        taskLock ();
        if (m_state)
        {
            taskUnlock ();
            return true;
        }
        if (::semTake (m_id, MS_TO_TICKS (msTimeout)) == OK)
        {
            // problem here. Is the task lock on when I return from the wait?
            m_state = false; // release the event
            taskUnlock ();
            return true; // we got it
        }
        taskUnlock ();
        return false; // we didn't get it
    }

    ~cEvent (void) 
    {
        taskLock ();
        ::semFlush (m_id);
        ::semDelete (m_id);
        taskUnlock ();
    }
};


# if 0
/*
 * See Marty for an informal discussion of Reader/Writer lock semantics.
 *
 * cRwLock implements a Writer Priority Reader/Writer Lock, using
 * Windows primitives. This may not be precisely the same method
 * as described in Andrews because I'm emulating Marty's Linux implementation
 * which makes this same unclaim.  
 *
 * See the comments before takeForRead and takeForWrite
 * for a discussion of the specific algorithms.
 *
 */
class cRwLock : public PRwLock
{
	// FSyncObject::newRwLock(...) creates a PRwLock object.
	friend class FSyncObject;
	cRwLock() throw(XSynchronization) ;
	
public:
	virtual void takeForRead() throw(XSynchronization);
	virtual void takeForWrite()throw(XSynchronization);
	virtual void unlock() throw(XSynchronization);
	virtual ~cRwLock() throw(XSynchronization);
	
private:
	unsigned int m_readers;	// # of current readers
	unsigned int m_readersWaiting; // # of current readers
	unsigned int m_writers;	// # of current writers
	unsigned int m_writersWaiting; // # of current writers
	HANDLE m_hBaton;	// protect access to the above
	HANDLE m_hWaitingReader; // Wait for a chance to read
	HANDLE m_hWaitingWriter; // Wait for a chance to write
};

cRwLock::cRwLock() throw(XSynchronization)
{
	m_readers = 0;
	m_readersWaiting = 0;
	m_writers = 0;
	m_writersWaiting = 0;
	m_hBaton = ::CreateMutex(0, FALSE /* Not initially owned */, 0);
	if (m_hBaton == 0) {
		throw XSynchronization ("CreateMutex returned invalid handle");
	}
	m_hWaitingReader = ::CreateEvent(0, FALSE /* Not manual reset */, FALSE /* Not initially owned */, 0);
	if (m_hWaitingReader == 0) {
		::CloseHandle (m_hBaton);
		throw XSynchronization ("CreateEvent returned invalid handle");
	}
	m_hWaitingWriter = ::CreateEvent(0, TRUE /* Manual reset */, FALSE /* Not initially owned */, 0);
	if (m_hWaitingWriter == 0) {
		::CloseHandle (m_hBaton);
		::CloseHandle (m_hWaitingReader);
		throw XSynchronization ("CreateEvent returned invalid handle");
	}
}

cRwLock::~cRwLock()
{
	if (m_hBaton != 0) {
		::CloseHandle (m_hBaton);
	}
	if (m_hWaitingReader != 0) {
		::CloseHandle (m_hWaitingReader);
	}
	if (m_hWaitingWriter != 0) {
		::CloseHandle (m_hWaitingWriter);
	}
}

//
// Any # of readers are allowed, but writing is exclusive of reading.
//
// takeForRead grabs the baton, so that it can exclusively update
// the readersWaiting and readers counts.
//
// It then enters a loop, waiting for there to be no writers.
// The while loop avoids a possible race as a result of the
// return from the cond_wait: a writer may have appeared between
// when the wait started to return and when it reobtained the baton.
//
// Exiting the loop means that there are no writers, and this reader
// may continue, so increment the # of readers and free the baton.
//
// precondition  ( (m_writers == 1 && m_readers == 0) ||
//                 (m_writers == 0 && m_readers >= 0) )
// postcondition (m_readers > 0 && m_writers == 0)
//
void cRwLock::takeForRead() throw(XSynchronization)
{
	DWORD r;
	r = ::WaitForSingleObject (m_hBaton, INFINITE_TIMEOUT);
	if (r == WAIT_FAILED)
    {
		throw XSynchronization ("Wait on Mutex for RwLock failed");
	}
	m_readersWaiting++;
	while (m_writers) 
    {
		r = ReleaseMutexAndWaitOnEvent(m_hBaton, m_hWaitingReader);
	}
	m_readersWaiting--;
	if (r == WAIT_FAILED) 
    {
		throw XSynchronization("Wait for Read Condition failed.");
	}
	m_readers++;
	::ReleaseMutex(m_hBaton);
	return;
}

//
// Only 1 writer is allowed, exclusive of all other writers and
// all readers.
//
// takeForWrite grabs the baton, so that it can exclusively update
// the writers and writersWaiting counts.
//
// It then enters a loop, waiting for there to be no writers and
// no readers.  The loop avoids a similar race to that described above.
//
// Exiting the loop means that there are no readers and no writers,
// so this writer may continue.  Increment the count of writers, and
// free the baton.
//
// precondition  ( (m_writers == 1 && m_readers == 0) ||
//                 (m_writers == 0 && m_readers >= 0) )

// postcondition (m_readers == 0 && m_writers == 1)
//
void cRwLock::takeForWrite() throw(XSynchronization)
{
	DWORD r;	
	r = ::WaitForSingleObject (m_hBaton, INFINITE_TIMEOUT);
	if (r == WAIT_FAILED) 
    {
		throw XSynchronization ("Wait on Mutex for RwLock failed");
	}
	m_writersWaiting++;
	while (m_writers > 0 ||  m_readers > 0) 
    {
		r = ReleaseMutexAndWaitOnEvent(m_hBaton, m_hWaitingReader);
	}
	m_writersWaiting--;
	if (r == WAIT_FAILED) 
    {
		throw XSynchronization("Wait for Write Condition failed.");
	}
	m_writers++;
	::ReleaseMutex(m_hBaton);
	return;
}

//
// unlock the Reader/Writer lock
//
// grab the baton, so that counts can be exclusively updated
//
// if there were any writers, then it is a writer that is freeing the
// lock, so decrement m_writers. Otherwise, it was one of the one or
// more current readers, so decrement the readers count.
//
// If there are writers waiting and this was the last reader,
//   signal one writer to continue.
// else if there are no writers but there are readers
//   signal all readers to continue.
//
// free up the baton
//
// precondition  ( (m_writers == 1 && m_readers == 0) ||
//                 (m_writers == 0 && m_readers > 0) )
// postcondition   (m_writers == 0 && m_readers >= 0 )
//
void cRwLock::unlock() throw(XSynchronization)
{
	DWORD r;	
	r = ::WaitForSingleObject (m_hBaton, INFINITE_TIMEOUT);
	if (r == WAIT_FAILED) 
    {
		throw XSynchronization ("Wait on Mutex to release RwLock failed");
	}
	if (m_writers) 
    {		// Current holder was the only writer
		m_writers--;
	} 
    else 
    {
		m_readers--;		// Current holder was a reader
	}
	if ((m_writersWaiting > 0) && (m_readers == 0)) 
    {
		::SetEvent(m_hWaitingWriter);
	}
	else if ((m_writersWaiting == 0) && (m_writers == 0)
		 && (m_readersWaiting > 0)) 
    {
		::PulseEvent(m_hWaitingReader);
	}
	::ReleaseMutex(m_hBaton);
	return;
}

# endif

PCriticalSection * FSyncObject::newCriticalSection (void) throw (XSynchronization)
{
    return new cCriticalSection ();
}

PMutex * FSyncObject::newMutex (bool initiallyOwn, const char * pName) throw (XSynchronization)
{
    return new cMutex (initiallyOwn ? 1 : 0, pName);
}

PRwLock * FSyncObject::newRwLock() throw (XSynchronization)
{
  return 0;
}

PSemaphore * FSyncObject::newSemaphore (int32 initialCount, int32 maxCount, const char * pName)
                    throw (XSynchronization)
{
    return new cSemaphore (initialCount, maxCount, pName);
}

PEvent * FSyncObject::newEvent (const char * pName) throw (XSynchronization)
{
    return new cEvent (pName);
}




__END_DC_NAME_SPACE


