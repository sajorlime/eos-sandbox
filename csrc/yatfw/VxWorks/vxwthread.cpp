/*
//
// $Id: //Source/Common/thread/VxWorks/vxwthread.cpp#4 $
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Filename: winsynchronization.cpp
//
// Creation Date: 12-27-99
//
// Last Modified:
//
// Author: emil
//
// Description:
    Implements dotcast threading primatives for VxWorks
//
// 
*/


# include "dctype.h"
# include "dotcastns.h"

# include "dcexception.h"
# include "dcsynchronization.h"
# include "thread.h"
# include "string.h"

# include <map>

# include <vxWorks.h>
# include <taskLib.h>

int dcxThread (...)
{
    return CThread::xThread (0);
}

__BEGIN_DC_NAME_SPACE

DEFINE_FILE_STRING("$Id", vxwthread);

struct SVXWThreadInfo
{
    int m_id;
    int m_exitCode;
    PEvent * m_pExitedEvent;
    PEvent * m_pJoinedEvent;

    SVXWThreadInfo (int id)
        :
            m_id (id),
            m_exitCode (0),
            m_pExitedEvent (FSyncObject::newEvent ()),
            m_pJoinedEvent (FSyncObject::newEvent ())
    {
    }
};

typedef std::map< int, SVXWThreadInfo > InfoMap;

// consider also using thead variables in VxWorks instead of map
InfoMap g_threadInfo;
PCriticalSection * g_tics = FSyncObject::newCriticalSection ();

# ifndef DEFAULT_STACK_SIZE
#   define DEFAULT_STACK_SIZE (1 << 10)
# endif


static char * s_baseName = "tdcmb";
static char s_tid = 'A';
static char * genTaskName (void)
{
    int len = strlen (s_baseName) + 2;
    char * pName = new char [len];
    strcpy (pName, s_baseName);
    pName[len] = s_tid++;
    pName[len + 1] = 0;
    return pName;
}

# ifndef  VXW_THREAD_OPTIONS
#   define  VXW_THREAD_OPTIONS 0
# endif

CThread::CThread (uint32 priority, bool waitForStart)
    :
     m_event (FSyncObject::newEvent ()),
     m_wake (FSyncObject::newEvent ()),
     m_xThreadEntered (FSyncObject::newEvent ()),
	 m_pCallback (DefaultThreadNotificationHandler),
     m_run (ts_initing)
{
    if ((int32) priority <= 0)
    {
        taskPriorityGet (m_idThread, (int*) &priority);
    }

    char * pTaskName = genTaskName ();

    m_idThread = taskSpawn (pTaskName, priority,
                            VXW_THREAD_OPTIONS, DEFAULT_STACK_SIZE,
                           ::dcxThread,
                           1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    if (m_idThread <= 0)
    {
        throw XException ("CreateThread Failed");
    }
    {
        CCriticalBlock cb (g_tics);
        g_threadInfo.insert (InfoMap::value_type (m_idThread,
                                                 SVXWThreadInfo ((int) m_idThread)));
    }
    if (!waitForStart)
        start ();
}



CThread::~CThread()
{
	if (m_run == CThread::ts_initing)
    {
		m_run = ts_terminated;
		m_xThreadEntered->wait();
	}
	taskDelete (m_idThread);
	delete m_event;
    delete m_wake;
    delete m_xThreadEntered;
}

bool CThread::start (void)
{
    m_event->set ();
    return true;
}

# ifdef DONT_DEFINE_TERMINATE
bool CThread::terminate (uint32 msWait)
{
    switch (m_run)
    {
    case ts_terminated:
        return true;
    case ts_sleeping:
        wake ();
        break;
    }
    m_run = ts_terminated;
    m_event.wait (msWait);
    if (taskDelete (m_idThread) == OK)
        return true;
    return false;
}
# endif

bool CThread::join (int * pReturn) 
{
	if (m_run != ts_terminated) 
    {
        InfoMap::iterator imi;
        {
            CCriticalBlock cb (g_tics);
            imi = g_threadInfo.find (m_idThread);
        }
        if (imi != g_threadInfo.end ())
        {
            SVXWThreadInfo & ti = (*imi).second;
            ti.m_pExitedEvent->wait ();
            ti.m_pJoinedEvent->set ();
            return true;
        }
        return false;
	}
	if (pReturn)
    {
        CCriticalBlock cb (g_tics);
        InfoMap::iterator imi = g_threadInfo.find (m_idThread);
        if (imi != g_threadInfo.end ())
        {
            SVXWThreadInfo & ti = (*imi).second;
            *pReturn = ti.m_exitCode;
        }
        else
            *pReturn = 0;
	}
	return true;
}

ThreadCallback CThread::setNotificationHandler (ThreadCallback handler) 
{
	ThreadCallback temp = m_pCallback;
	m_pCallback = ((handler == 0) ? DefaultThreadNotificationHandler : handler);
	return temp;
}

bool CThread::sleep (uint32 ms)
{
    m_run = ts_sleeping;
    m_wake->wait (ms);
    m_run = ts_running;
    return true;
}

bool CThread::wake (void)
{
    return m_wake->pulse ();
}


void CThread::dump (void)
{
# if 0
    TRACE ("CThread handle:%x\n", m_hThread);
    TRACE ("CThread m_run:%d\n", m_run);
# endif
}



unsigned long CThread::xThread (void * pParam)
{
	int r;
    CThread * pT = (CThread *) pParam;
	if (pT->m_run != ts_initing) {
		pT->m_xThreadEntered->set();
		return te_failure;
	}
    pT->m_run = CThread::ts_suspended;
    pT->m_event->wait ();
    pT->m_run = CThread::ts_running;
    try
    {
# if 0
        DC_DEUBG ("CThread %x entry\n", pT);
# endif
        r = pT->thread ();
# if 0
        DC_DEUBG ("CThread %x exited with %d\n", pT, r);
# endif
    }
    catch (XException (&x))
    {
# if 0
        DC_DEUBG ("CThread %x exited with exception: %s\n", pT, x.reason ());
# endif
        r = x.m_errorCode;
    }
    catch (...)
    {
        r = -1;
    }
    pT->m_run = CThread::ts_terminated;
	pT->m_pCallback (pT);
    InfoMap::iterator imi;
    {
        CCriticalBlock cb (g_tics);
        imi = g_threadInfo.find (pT->m_idThread);
    }
    if (imi != g_threadInfo.end ())
    {
        SVXWThreadInfo & ti = (*imi).second;
        ti.m_exitCode = r;
        ti.m_pExitedEvent->set ();
        ti.m_pJoinedEvent->wait ();
        {
            CCriticalBlock cb (g_tics);
            g_threadInfo.erase (imi);
        }
    }
    return (unsigned long) r;
}

void CThread::yield(void)
{
    ::taskDelay (1);
}


__END_DC_NAME_SPACE

