/*
//
// $Id$
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Filename: loggedMessageQueue.h
//
// Creation Date: 01-16-00
//
// Last Modified:
//
// Author: emil
//
// Description:
    Implements a class that allows logging of messages.
   Note:
//
// 
*/


/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
*
    LoggedMessageQueue.cpp
*
*
* .........................................................................
*
    Contains the implemenation of the LoggedMessageQueue class.
*
* .........................................................................
*
*   Date       Description
*  --------    -----------
    06/21/99    Release
*
*
* NOTES:
*
*
* Classes/Types:
*
*
* FUNCTIONS:
*
*
*/


# include "loggedMessageQueue.h"


# include "dotcastns.h"

DEFINE_FILE_STRING("$Id", loggedMessageQueue);


CLoggedMessageQueue::CLoggedMessageQueue (CLogger * pLogger,
                                         const char * pLogSrc,
                                         int maxMsgSize,
                                         int freeCount
                                        )
    :
     m_LA (pLogger, pLogSrc),
     CMessageQueue (maxMsgSize, freeCount)
{
}

CLoggedMessageQueue::~CLoggedMessageQueue ()
{
    m_LA.stopLogging ();
}


bool CLoggedMessageQueue::send (CMessage * msg)
{
    char buf[MAX_LOG_MSG_LEN];

    format (buf,
            msg->getId (),
            msg->m_user,
            msg->getLength (),
            msg->getBuffer (),
            sizeof (buf)
           );
    m_LA.printLogMsg ("", buf);
    return CMessageQueue::send (msg);
}

void CLoggedMessageQueue::failed (const XException & reason)
{
    CMessageQueue::failed (reason);
}

static char vToHex[] = "0123456789ABCDEF";
static inline char * toHex (char * pDest, unsigned char b)
{
    *pDest++ = vToHex [(b >> 4) & 0xf];
    *pDest++ = vToHex [b & 0xf];
    return pDest;
}

static inline char * iToHex (char * pDest, int i)
{
    pDest = toHex (pDest, i >> 24);
    pDest = toHex (pDest, i >> 16);
    pDest = toHex (pDest, i >> 8);
    pDest = toHex (pDest, i >> 0);
    return pDest;
}

/*
    default formating, does a hex dump of the data.
*/
char * CLoggedMessageQueue::format (char * pDest,
                                          EMessageType id,
                                          int user,
                                          int length,
                                          const BYTE * pBuf,
                                          int maxSize
                                         )
{
    const char * pId = strEMessageType (id) + 4;
    char c;
    maxSize--; // leave room for null
    while (c = *pId++)
    {
        *pDest++ = c;
        maxSize--;
    }
    *pDest++ = ':';
    *pDest++ = ' ';
    pDest = iToHex (pDest, user);
    *pDest++ = ':';
    maxSize -= 6;
    while (length-- > 0)
    {
        *pDest++ = ' ';
        pDest = toHex (pDest, *pBuf++);
        maxSize -= 3;
        if (maxSize <= 0)
            break;
    }
    *pDest++ = 0;
    return pDest;
}


