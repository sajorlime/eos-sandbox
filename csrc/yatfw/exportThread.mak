
COPY=cp

HEADERSDIR=../headers

HEADERS= \
    $(HEADERSDIR)/dcsynchronization.h \
    $(HEADERSDIR)/message.h \
    $(HEADERSDIR)/messageQueue.h \
    $(HEADERSDIR)/nullMessageQueue.h \
    $(HEADERSDIR)/safeFIFO.h \
    $(HEADERSDIR)/thread.h \
    $(END_LIST)

all: $(HEADERS)

$(HEADERSDIR)/dcsynchronization.h: dcsynchronization.h
    $(COPY) dcsynchronization.h $(HEADERSDIR)

$(HEADERSDIR)/message.h: message.h
    $(COPY) message.h $(HEADERSDIR)

$(HEADERSDIR)/messageQueue.h: messageQueue.h
    $(COPY) messageQueue.h $(HEADERSDIR)

$(HEADERSDIR)/nullMessageQueue.h: nullMessageQueue.h
    $(COPY) nullMessageQueue.h $(HEADERSDIR)

$(HEADERSDIR)/safeFIFO.h: safeFIFO.h
    $(COPY) safeFIFO.h $(HEADERSDIR)

$(HEADERSDIR)/thread.h: thread.h
    $(COPY) thread.h $(HEADERSDIR)

