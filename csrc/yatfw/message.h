/*
//
// $Id$
//
// Copyright (c) 2000 Dotcast Inc.
// All rights reserved.
//
// Filename: message.h
//
// Creation Date: 01-16-00
//
// Last Modified:
    12-08-00: trying to improveme usage comments.
//
// Author: emil
//
// Description:
    Defines a class for talking sending messages between threads.
   Note:
//
// 
*/


/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
*
    Message.h
*
    Contains defintion of a class that allow asynchronous communication.
*
*
   Date     Description
 --------   -----------
*
*
* Usage:
    This file defines the CMessage and CMessgeReceiver classes which
    define an asynchornous communication interface that is very well
    suited as an inter-thread communicaiton mechanism.  It defines a
    method of controlling and filling messge buffers and the
    delivering them to the "message receiver."  The interface is
    intended to be primarily asynchronous, but can be easily made
    synchronous by restricting the implementation (see MessageSingle). 
-
    The implementations available as of 12/8/00 provide for single
    process communication, The implemenations (see MessageQueue) could
    be adapted to inter-process communicaiton by using CSemaphore
    instead of a critical section.
-
    CMessageReceiver is abstract, so to use the class one must first
    create a concrete class to use the interface (see other message
    classes in this directory), but a typical usage follows:
-
        Thread A                        Thread B
            Create other threads or otherwise
            establish thread identity.
            Create Message Receiver.
            Pass to other threads
            on creation, or by some other
            method.
                                        Save pointer to message receiver object.
                                        Wait for a message (internally this is an event).
                                        Fill m_user to identify the peer-to-peer message type
                                        and the message data.
                                        pMsgQue->send (pMsg) -- send the message 
            pMsg = myMsgQue ()
            ... // use pMsg
            myMsgQue.relese (pMsg)
-

  Classes/Types:
    CMessage -- container for a message within a MessageReceiver, i.e.
    the buffer that stores the bits and bytes of the message and the
    message meta data.
-
    CMessageReceiver -- This is the class the is the container for both queued messages and
    for empty message containers.  The MessageReceiver is responsible for implementing a number 
    symantics that allow for safe and easy asychronous and synchronous communication between 
    seperate threads of control. These are:
        - An underlying queueing mecahanism that is thread safe.  The
        actual rules of the queueing will be dependent on the
        subclass.
        - Peer to peer meanings for messages.  See m_user field.
        - Finite and control resource pool for messages (i.e.  malloc
        or new is not used for each message allocation.) E.g.  class
        may be block caller if message pool is exhausted.
        - Multiple senders is supported.
        - A single reader is assumed by the API but implementation is
        safe for multiple readers.
        - The MessageReceiver is responsible for all memory resource
        associated with the receiver.  I.e.  the CMessage buffers
        belong to it and are deleted iff the MessageReceiver deletes
        them or when it is deleted itself.  Consequently, the
        MessageReceive assumes that references to its CMessage buffers
        are not held by the application.
        
-
    EMessageType -- messge type for internal message types.  The m_user
    field is used for private type information.  
-
    CMessageDispatch -- Untility class for dispatching messages by internal type. This allows
    for the creation of a mechanism handling message differently by internal type.

*/


# ifndef __MESSAGE_H__
# define __MESSAGE_H__


# include "dctype.h"
# include "dcsynchronization.h"
# include "dcexception.h"

__BEGIN_DC_NAME_SPACE

/*
    EMessageType specifies the message values returned by a protocol. 
    These values might better be private to the MessageReceiver but
    they where not implemented this way so that the MessageDispatcher
    could be created without being a friend to the MessageReceiver. 
    The idea here is to differentiate the perpose of a message
    seperately from its content.  This then allows the internal
    handling of messages to differ based on a notion of priorty
    inferred by these names.  In the case of the Terminate message a
    mechanism can be create to insure termination of the receiver.
*/
enum EMessageType
{
    EMT_Unallocated = -1,
    EMT_NoMessage = 0,
    EMT_Control,
    EMT_Data,
    EMT_DataContinuation,
    EMT_CriticalData,
    EMT_Debug,
    EMT_Warn,
    EMT_Error,
    EMT_Fatal,
    Last_ENormalMessageType,
    EMT_Terminate = 0x7fff,
    Last_EMessageType
};

/*
    strEMessageType -- a utility function for printing the internal type.
*/
const char * strEMessageType (EMessageType t);



/*
    CMessage -- this is the underlying buffering mechanism for message receivers.
    It supposes only a few points.
    - A CMessage belongs to a CMessageReceiver
    - It contains a delivery type and a user defined type.
    - If it additional data is associated with the message it has a
    pointer to that data and it knows it length.
    - It has methods all virtual methods buts its not clear this is
    necessary.

*/
class CMessage
{
private:
    /*
        the CMessageReceiver has direct access the the members of the buffer.
    */
    friend class CMessageReceiver;
    /*
        The constructor being private can only be called by the MessageReceiver.
        It sets buffer and the receiver. (It probably should set the size also!)
        This implementation requires that all buffer be the same size in 
        a MessageReceiver.
    */
    CMessage (uint8 * buf, CMessageReceiver * pReceiver);

    /*
        The buffer contains the private data (usually a class or structure)
        associated with a message.
    */
    uint8 * m_buffer;
    /* the type of message */
    EMessageType m_id;
    /* length is the actual length of a message, length <= size */
    int m_length;
    /* pReceiver points back to the owning MessageReceiver. */
    CMessageReceiver * m_pReceiver;

    CMessage * operator& (void) {return this;}

    /*
        user is the user defined type.  This type has meaning only be mutual
        aggreement between sender and receiver.
    */
    int m_user;
public:
    virtual EMessageType setId (EMessageType id) {return m_id = id;}

    virtual EMessageType getId (void) {return m_id;}

    /* sets the internal length of message, called by sender */
    virtual int setLength (int len) {return m_length = len;}

    /* gets the internal length of message, called by receiver */
    virtual int getLength (void) {return m_length;}

    /* returns a pointer the of message, called by receiver and sender */
    virtual uint8 * getBuffer (void) {return m_buffer;}

    /*
        setMessage is the one call does it all method of defining the contents
        of a message that should later be sent.
    */
    virtual int setMessage (EMessageType id, int user, int len, const uint8 * pBuf);

    /* set the user field, called by sender */
    virtual int setUser (int userId) {return m_user = userId;}

    /* get the user field, called by receiver */
    virtual int getUser (void) {return m_user;}

    /*
        assignment operator makes a copy of message.
    */
    virtual CMessage & operator= (CMessage &);

    /*
        destructor -- deletes buffer.  Should this be private or protected?
    */
    virtual ~CMessage ();
};

/*
    CMessageDispatch -- provides a simple framework for handling messages by
    internal type.
*/
class CMessageDispatch
{
public:
    virtual bool processNoMessage (CMessage * pMsg)
    {
        // note that this can be overridden!
        throw XException ("Cannot dispatch \"EMT_NoMessage\" message type");
        return false;
    }
    virtual bool processControlMsg (CMessage * pMsg) = 0;
    virtual bool processDataMsg (CMessage * pMsg) = 0;
    virtual bool processDataContinuationMsg (CMessage * pMsg) = 0;
    virtual bool processCriticalDataMsg (CMessage * pMsg) = 0;
    virtual bool processDebugMsg (CMessage * pMsg) = 0;
    virtual bool processWarnMsg (CMessage * pMsg) = 0;
    virtual bool processErrorMsg (CMessage * pMsg) = 0;
    virtual bool processFatalMsg (CMessage * pMsg) = 0;
    virtual bool processTerminateMsg (CMessage * pMsg) = 0;
    virtual bool dispatchMsg (CMessage * pMsg)
    {
        switch (pMsg->getId ())
        {
        case EMT_NoMessage:
            return processNoMessage (pMsg);

        case EMT_Control:
            return processControlMsg (pMsg);

        case EMT_Data:
            return processDataMsg (pMsg);

        case EMT_DataContinuation:
            return processDataMsg (pMsg);

        case EMT_CriticalData:
            return processDataMsg (pMsg);

        case EMT_Debug:
            return processDebugMsg (pMsg);

        case EMT_Warn:
            return processWarnMsg (pMsg);

        case EMT_Error:
            return processErrorMsg (pMsg);

        case EMT_Fatal:
            return processFatalMsg (pMsg);

        case EMT_Terminate:
            return processTerminateMsg (pMsg);

        default:
            throw XException ("Cannot dispatch unknown message type");
            return false;
        }
    }
};


# ifndef DEFAULT_MSG_SIZE
#   define DEFAULT_MSG_SIZE 256
# endif
# ifndef DEFAULT_FREE_COUNT
#   define DEFAULT_FREE_COUNT 128
# endif




/*
    Message Receiver is the base class of a family of inter-thread
    or interprocess communication classes.

    A MessageReceiver is at the name implies an ojbect that allows
    asynchornous reception of messges (CMessage).  The MessageReceiver
    class does other things also.  It is responsible for managing the
    storage assoicated with messages as well controling the interfaces
    that enable sending, receiving, replying, returning and otherwise
    dealing with messages.
*/
class CMessageReceiver
{
    /*
        Allow the CMessage class access to CMessageReceiver private
        and protected members.
    */
    friend class CMessage;

    /* Don't allow anyone to copy assign a message receiver */
    CMessageReceiver & operator= (CMessageReceiver & msg) {return *this;}

public:
    /*
        blockFlag is used to define the behavior of a message receiver.
        The options are no blocking, on send (i.e. synchronous), on the queue
        being full, and on trying to receive if there are no messages.
    */
    enum blockFlag
    {
        blockNone = 0,
        blockOnSendAll = 1, // make sends synchronous
        blockOnSendFull = 2, // block when all buffers used.
        blockOnReceive = 4, // block when no message waiting.
        last_blockFlag
    };

    /*
        destructor for a message receiver.
        Delete the internally allocated event, after all the users of the
        receiver have disconnected.
    */
    virtual ~CMessageReceiver ()
    {
# if 0
        TRACE ("~CMessageReceiver\n", this);
# endif
        waitForUsers ();
        delete m_pWaitEvent;
    }

protected:
    PEvent * m_pWaitEvent; // internal event for waiting for a message

    int m_emptyWaiters; // waiters on the all messages to be read.

    XException * m_pExcept; // a place to keep an exception to be delivered to receiver.

    int m_freeCount; // the free count of available messages.

    int m_freeLimit; // the allocation limit for messages.

    int m_allocCount; // the allocation increment

    int m_msgCount; // the number of wating messages for a receiver.

    int m_users; // the number of threads sharing this resource.

    uint32 m_blockFlags; // see enum blockFlag, determines blocking behavior

    enum {defMsgSize = DEFAULT_MSG_SIZE, defFreeCount = DEFAULT_FREE_COUNT};

    int m_maxMsgSize; // the size of a message for this receiver

    CMessage * makeMessage (); // the only maker of CMessage buffers.

    virtual void waitForUsers (void); // a hack to allow users to disconnect.
    

    /*
        Constructor:
            maxMsgSize -- defines the maximum size of a message for this receiver.
            freeCount -- the initial free count for this receiver.
            blockFlags -- defines the blocking behavior for this receiver.
    */
    CMessageReceiver (int maxMsgSize = defMsgSize,
                      int freeCount = defFreeCount,
                      int blockFlags = blockOnSendFull | blockOnReceive
                    );


    /*
        utility function for waking up empty waiters
    */
    virtual bool notifyEmpty (void);

public:
    /*
        Allows the sender to tell the receiver that he has failed.
        The failed thread constructs an exception structure with
        the reason for the failure.  The implemation should copy
        the exception and throw it when the receiver trys to receive
        or if it is blocked after the tasks wakes.
    */
    virtual void failed (const XException & reason) = 0;
    
    /*
        get a message container to package the message.
        The message buffer is part of the pool allocated by this receiver and
        must be released back to this receiver.
    */
    virtual CMessage * allocMessage (void) = 0;

    /*
        send an already packaged message.
        The message is sent to it this receiver.
    */
    virtual bool send (CMessage * msg) = 0;

    /* 
        The basic send interface.
        This function is not virtual because it should not be overridden, and inline.
        The send (CMessage *) function needs to implement the actual send.
        The message is sent to it this receiver.
    */
    bool send (EMessageType id, int user, int length, const uint8 * pBuf)
    {
        CMessage * pMsg = allocMessage ();
        pMsg->setMessage (id, user, length, pBuf);
        return send (pMsg);
    }

    /*
        maxSize is an accessor function.
    */
    int maxSize (void) {return m_maxMsgSize;}

    /*
        This function should return the number of messages available
        via the receive method.  I.e. it send the number of messages that have
        already sent.

        If a thread does not want to block it can do the following:
            while (pRcv->messagesAvailable ())
            {
                pMsg = pRcv->receive ();
                // handle message
                pRcv->release (pMsg);
            }

    */
    virtual int messagesAvailable (void)
    {
        if (m_msgCount == 0)
            notifyEmpty ();
        return m_msgCount;
    }

    /* for debug only */
    int messagesFree (void) {return m_freeCount;}

    /*
        The receive method. The semantics of this function
        is simply that it return a messge pointer when called.
        The implementing class should obey the blocking flags
        semantics.  A null pointer therefore my be returned.
        The reciever function should call notifyEmpty () when
        the queue is empty.
    */
    virtual CMessage * receive (void) = 0;

    /* register task as a user of this queue */
    virtual int attach (void)
    {
        return ++m_users;
    }
    
    /* unregister task as a user of this queue */
    virtual int detach (void)
    {
        --m_users;
        m_pWaitEvent->pulse ();
        return m_users;
    }

    /*
        This function allows any task to wait until the queue
        is empty.
    */
    virtual bool waitForEmpty (void);

    /*
        release -- should release the message to some appropriate free list.
    */
    virtual void release (CMessage * pMsg) {}
};

# undef DEFAULT_MSG_SIZE
# undef DEFAULT_FREE_COUNT

/*
    The TMessage
    This template is untested.
    The idea here is/was to make a template that allowed one the send
    messages of a particular type, but I think the implementation is 
    broken as of 12/08/00.  I.e. instanciate this message type
    and then use the receiver to get it back and forth.
*/
template<class Ty> class TMessage
{
    Ty * m_pT;
    int m_user;
    EMessageType m_id;
public:
    EMessageType id (void) {return m_id;}
    EMessageType user (void) {return m_user;}
    Ty * type (void) {return m_pT;}
    TMessage (CMessage * pMsg, Ty & ty, int user, EMessageType id = EMT_Data)
        : m_pT (static_cast<Ty *> (pMsg->getBuffer ())),
          m_user (user), m_id (id)
    {
        *m_pT = ty;
    }
    TMessage (CMessage * pMsg, Ty * pTy, int user, EMessageType id = EMT_Data)
        : m_pT (static_cast<Ty *> (pMsg->getBuffer ())),
          m_user (user), m_id (id)
    {
        *m_pT = *pTy;
    }
};

/*
    the tSendMessage template allows one to send a type directly to the
    a message receiver as long as the type supports the operations id,
    and user.

*/
template<class Ty> bool tSendMessage (CMessageReceiver * pRcv, Ty * pTy)
{
    return pRcv->send (pTy->id (), pTy->user (), sizeof (Ty), static_cast<uint8 *> (pTy));
}

template<class Ty> bool tSendMessage (CMessageReceiver * pRcv, Ty & ty)
{
    return pRcv->send (ty.id (), ty.user (), sizeof (Ty), static_cast<uint8 *> (&ty));
}

/*
    tReceiveMessage template allows one to receiver the contents of a next
    message in a buffer without dealing with the message structure.
    I need to think about what RTTI does to this.
*/
template<class Ty> void tReceiveMessage (Ty & ty, CMessageReceiver * pRcv)
{
    CMessage * pMsg;

    pMsg = pRcv->receive ();
    ty = *(reinterpret_cast<Ty *> (pMsg->getBuffer ()));
    pRcv->release (pMsg);
}


__END_DC_NAME_SPACE

# endif


