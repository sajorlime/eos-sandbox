/*
//
// $Id$
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Filename: message.cpp
//
// Creation Date: 01-16-00
//
// Last Modified:
//
// Author: emil
//
// Description:
    Implements message classes.
   Note:
//
// 
*/

/* original
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
*
* Message.cpp
*
* .........................................................................
*
*
    Contains implemtaion for the CMessage class.
*
* .........................................................................
*
*  Date        Description
*  ------      -----------
    06/21/99    Release
*
*
* NOTES:
*
*
* Classes/Types:
*
*
*
*
*/

# include <string.h>
# include <stdio.h>

# include "dctype.h"
# include "dotcastns.h"

# include "message.h"

__BEGIN_DC_NAME_SPACE

DEFINE_FILE_STRING("$Id", message);



CMessage::CMessage (uint8 * pBuf, CMessageReceiver * pReceiver)
    : m_buffer (pBuf),
      m_id (EMT_Unallocated),
      m_length (-1),
      m_pReceiver (pReceiver)
{
# if 0
    TRACE ("cmcon-%x\n", this);
# endif
}


CMessage::~CMessage ()
{
    if (m_buffer)
    {
# if 0 && defined (_DEBUG)
        m_buffer[0] = 'D';
        TRACE ("delete m_buffer:%x, for %x\n", m_buffer, this);
# endif
        delete [] m_buffer;
        m_buffer = (uint8 *) 0;
    }
}


int CMessage::setMessage (EMessageType id, int user,
                                 int len, const uint8 * pBuf
                                )
{
    if (len > m_pReceiver->m_maxMsgSize)
    {
        throw XException ("Illegal message length >= max size");
        return -1;
    }
    if (id <= 0)
    {
        throw XException ("Illegal message Id <= 0");
        return -1;
    }
    m_id = id;
    m_user = user;
    m_length = len;
    if (len > 0 && pBuf)
        memcpy (m_buffer, pBuf, len);

    return len;
}


/*
    The assignment operator can be used to copy a message
    so that it can be sent to another message queue.
    I.e. You have a message for queue A that you want to
    deliver to queue B, allocate a message for queue B
    and assign the value to of the queue A message to the
    allocated message, and then send it.
*/
CMessage & CMessage::operator= (CMessage & rhs)
{
    setMessage (rhs.m_id, rhs.m_user, rhs.m_length, rhs.m_buffer);

    return *this;
}


CMessage * CMessageReceiver::makeMessage ()
{
    uint8 * pBuf = new uint8[m_maxMsgSize];
# if 0 && defined (_DEBUG)
        memset (pBuf, 'A', m_maxMsgSize - 1);
        pBuf[0] = 'A';
        TRACE ("mm-pBuf:%x\n", pBuf);
# endif
    return new CMessage (pBuf, this);
}


void CMessageReceiver::waitForUsers (void)
{
    m_users--;
    while (m_users > 0)
    {
        uint32 r = m_pWaitEvent->wait ();	// was 1000
        if (!r)
            m_users--;
    }
}

CMessageReceiver::CMessageReceiver (int maxMsgSize,
                                    int freeCount,
                                    int blockFlags
                                   )
    :
     m_pWaitEvent (FSyncObject::newEvent ()),
     m_emptyWaiters (0),
     m_pExcept (0),
     m_freeCount ((freeCount <= 0) ? defFreeCount : freeCount),
     m_freeLimit (0),
     m_allocCount (0),
     m_msgCount (0),
     m_users (1),
     m_blockFlags (blockFlags),
     m_maxMsgSize ((maxMsgSize <= 0) ? defMsgSize : maxMsgSize)
{
}


bool CMessageReceiver::notifyEmpty (void)
{
    if (m_emptyWaiters)
    {
        m_pWaitEvent->pulse ();
        //m_emptyWaiters--;
        return true;
    }
    return false;
}


bool CMessageReceiver::waitForEmpty (void)
{
    // to do this right we need to block until we sleep,
    // I don't know how to do this in windows.
    // we could raise our priorty, but I want to avoid that
    // for the moment.  I think this will meet the needs
    // of the SiRF project, but it should be looked at again.
    while (m_freeCount < m_msgCount)
    {
        m_emptyWaiters++;
# if 0
        TRACE ("waitForEmpty: %d, %d\n", m_msgCount, m_emptyWaiters);
# endif
        bool r = m_pWaitEvent->wait ();
        if (!r)
        {
# if 0
            TRACE ("exit waitForEmpty: %d, %d\n", m_msgCount, m_emptyWaiters);
# endif
            return true; // event was plused get out of here!
        }
# if 0
        TRACE ("timeout waitForEmpty: %d, %d\n", m_msgCount, m_emptyWaiters);
# endif
        m_emptyWaiters--;
    }
    return false;
}


static const char * vEMT[]
=
{
    "EMT_NoMessage",
    "EMT_Control",
    "EMT_Data",
    "EMT_DataContinuation",
    "EMT_CriticalData",
    "EMT_Debug",
    "EMT_Warn",
    "EMT_Error",
    "EMT_Fatal"
};

static char emtSpace[48];

const char * strEMessageType (EMessageType t)
{
    if ((unsigned) t < Last_ENormalMessageType)
        return vEMT[t];
    if (t == EMT_Unallocated)
        return "EMT_Unallocated";
    if (t == EMT_Terminate)
        return "EMT_Terminate";
    sprintf (emtSpace, "Unsupported message type: 0x%x]n", t);
    return emtSpace;
}

__END_DC_NAME_SPACE

