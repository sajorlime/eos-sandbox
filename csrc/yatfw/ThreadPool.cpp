/*
//
// $Id: //Source/Common/thread/ThreadPool.cpp#14 $
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Author: Gordie
//
// Brief Description: 
//
*/

# include "dotcastns.h"
# include "dctype.h"
# include "ThreadPool.h"
# include "thread.h"

# include <iostream>
# include <vector>

__BEGIN_DC_NAME_SPACE

class CThreadPoolHelper;

class CEphemeralWorkerThread : public PWorkerThread, CThread
{
public:
	CEphemeralWorkerThread (PWorkerThreadDelegate* delegate, void* contextData, const PThreadPool* pool, 
		ThreadPoolInitializationCallback callback, void* data) : m_pDelegate(delegate)
	{
		if (callback != 0) {
			(*callback)(pool, m_pDelegate, data);
		}
		m_pDelegate->setData(contextData);
		setNotificationHandler(WorkerThreadDelete);
		this->start();
	}

	virtual ~CEphemeralWorkerThread () { delete m_pDelegate; }

	virtual PWorkerThreadDelegate* getDelegate () const { return m_pDelegate; }
	
	virtual int thread () {
		try {
			m_pDelegate->work();
		} catch (XException* pex) {
			std::cerr << "Exception \"" << pex->reason() << "\" caught in worker thread: " << this << std::endl;
			m_pDelegate->handleException(pex);
			delete pex;
		} catch (...) {
			std::cerr << "Unknown exception caught in worker thread: " << this << std::endl;
			m_pDelegate->handleUnknownException();
		}
		return 0;
	}

private:
	CEphemeralWorkerThread (CEphemeralWorkerThread& rhs);
	CEphemeralWorkerThread& operator=(CEphemeralWorkerThread& rhs);

	static bool WorkerThreadDelete (CThread* thread) {
		delete thread;
		return true;
	}

	PWorkerThreadDelegate* m_pDelegate;
};

//
// Concrete WorkerThread class. This class runs the actual Thread for each 
// ThreadPool entry, and manages that thread's lifetime, as well as
// handles execution of work and thread death.
//
class CWorkerThread : public PWorkerThread, CThread
{
public:
	CWorkerThread (PWorkerThreadDelegate* delegate, CThreadPoolHelper* helper,
		const PThreadPool* pool,
		ThreadPoolInitializationCallback callback, void* initializationData)
		: m_pDelegate(delegate), m_pThreadPoolHelper(helper), m_quit(false)
	{
		m_pEvent = FSyncObject::newEvent(0);
		m_instanceNumber = InstanceCount++;
		if (callback != 0) {
			(*callback)(pool, delegate, initializationData);
		}
		setNotificationHandler(WorkerThreadDelete);
		this->start();
	}

	virtual ~CWorkerThread () {
		delete m_pEvent;	
		delete m_pDelegate;	
	}

	virtual int thread ();

	virtual void quit () {
		m_pThreadPoolHelper = 0;
		m_quit = true;
		m_pEvent->set();
	}

	virtual void wakeup () {
		m_pEvent->set();
	}

	virtual PWorkerThreadDelegate* getDelegate () const { return m_pDelegate; }

private:
	CWorkerThread (CWorkerThread& rhs);
	CWorkerThread& operator=(CWorkerThread& rhs);

	static bool WorkerThreadDelete (CThread* thread) {
		delete thread;
		return true;
	}

	static int InstanceCount;

	int m_instanceNumber;
	PWorkerThreadDelegate* m_pDelegate;
	CThreadPoolHelper* m_pThreadPoolHelper;
	bool m_quit;
	PEvent* m_pEvent;
};

int CWorkerThread::InstanceCount = 0;

//
// ThreadPoolHelper concrete class, creates pool of concrete WorkerThread
// objects, and manages them using synchronization primitives.
//
// This class does all of the actual work for a PThreadPool, which is only
// an interface to ThreadPool management and doesn't have any actual 
// thread/synchronization smarts.
//
class CThreadPoolHelper : public PThreadPoolHelper
{
public:
	CThreadPoolHelper (size_t size, const PThreadPool* pool,
		ThreadPoolInitializationCallback callback, void* initializationData) : m_pThreadPool(pool)
	{
		CWorkerThread* thread;
		m_pMutex = FSyncObject::newMutex(false, 0);
		m_pEvent = FSyncObject::newEvent(0);
		for (size_t i = 0; i < size; i++) {
			PWorkerThreadDelegate* delegate = m_pThreadPool->createWorker();
			thread = new CWorkerThread(delegate, this, pool, callback, initializationData);
			m_threads.push_back(thread);
		}
	}

	virtual ~CThreadPoolHelper () {
		delete m_pMutex;
		delete m_pEvent;	
	}

	virtual PWorkerThread* getWorkerThread(void* contextData, bool block)
	{
		CWorkerThread* thread = 0;
		while (true) {
			m_pMutex->take();
			if (m_inactiveList.size() == 0) {
				thread = 0;
			}
			else {
				thread = m_inactiveList.back();
				m_inactiveList.pop_back();
				if (m_inactiveList.size() == 0) {
					m_pEvent->reset();
				}
			}
			m_pMutex->release();
			if ((thread == 0) && (block == true)) {
				m_pEvent->wait(5000);
			}
			else {
				break;
			}
		}
		if (thread != 0) {
			thread->getDelegate()->setData(contextData);
			thread->wakeup();
		}
		return thread;
	}

	virtual void shutdown () {
		std::vector<CWorkerThread*>::iterator iterator;
		CWorkerThread* thread;
		m_pMutex->take();
		for (iterator = m_threads.begin(); iterator != m_threads.end(); iterator++) {
			thread = *iterator;
			thread->quit();
		}
		m_pMutex->release();
	}

	virtual void addToInactiveList (CWorkerThread* thread) {
		// TODO: Make sure not in list?
		m_pMutex->take();
		m_inactiveList.push_back(thread);
		if (m_inactiveList.size() == 1) {
			m_pEvent->set();
		}
		m_pMutex->release();
	}

	virtual void removeThread (CWorkerThread* thread) {
		std::vector<CWorkerThread*>::iterator iterator;
		m_pMutex->take();
		for (iterator = m_threads.begin(); iterator != m_threads.end(); iterator++) {
			if ((*iterator) == thread) {
				m_threads.erase(iterator);
				break;
			}
		}
		for (iterator = m_inactiveList.begin(); iterator != m_inactiveList.end(); iterator++) {
			if ((*iterator) == thread) {
				m_threads.erase(iterator);
				break;
			}
		}
		m_pMutex->release();
	}

	virtual PWorkerThread* CreateEphemeralWorkerThread(void* contextData, ThreadPoolInitializationCallback callback, void* data) const {
		PWorkerThreadDelegate* worker = m_pThreadPool->createWorker();
		return new CEphemeralWorkerThread(worker, contextData, m_pThreadPool, callback, data);
	}

private:
	CThreadPoolHelper (CThreadPoolHelper& rhs);
	CThreadPoolHelper& operator=(CThreadPoolHelper& rhs);

	std::vector<CWorkerThread*> m_threads;
	std::vector<CWorkerThread*> m_inactiveList;
	const PThreadPool* m_pThreadPool;
	PMutex* m_pMutex;
	PEvent* m_pEvent;
};

//
// Static method on the PThreadPoolHelper Protocol to create
// a concrete CThreadPoolHelper.
//
PThreadPoolHelper* PThreadPoolHelper::CreateThreadPoolHelper (size_t size, const PThreadPool* pool,
	ThreadPoolInitializationCallback callback, void* initializationData)
{
	return new CThreadPoolHelper(size, pool, callback, initializationData);
}

//
// CWorkerThread mainloop
//
// Note: This method can't be put in the definition of the 
// CWorkerThread class, since the CThreadPoolHelper class
// is defined after the CWorkerThread class, hence the methods 
// called on the m_pThreadPoolHelper Object won't have been defined.
//
int CWorkerThread::thread () {
	bool quit;
	while (m_quit == false) {
		try {
			if (m_pThreadPoolHelper != 0) {
				m_pThreadPoolHelper->addToInactiveList(this);
			}
			m_pEvent->wait();
			m_pEvent->reset();
			if (m_quit == false) {
				m_pDelegate->work();
			}
		} catch (XException* pex) {
			std::cerr << "Exception \"" << pex->reason() << "\" caught in worker thread: " << this << std::endl;
			quit = m_pDelegate->handleException(pex);
			std::cerr << "Delegate says " << (quit ? "exit thread" : "keep running") << std::endl;
			delete pex;
			m_quit = quit;
		} catch (...) {
			std::cerr << "Unknown exception caught in worker thread: " << this << std::endl;
			quit = m_pDelegate->handleUnknownException();
			std::cerr << "Delegate says " << (quit ? "exit thread" : "keep running") << std::endl;
			m_quit = quit;
		}
	}
	if (m_pThreadPoolHelper != 0) {
		m_pThreadPoolHelper->removeThread(this);
	}
	return 0;
}

__END_DC_NAME_SPACE

