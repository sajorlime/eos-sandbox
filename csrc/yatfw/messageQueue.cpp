/*
//
// $Id$
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Filename: messageQueue.cpp
//
// Creation Date: 01-16-00
//
// Last Modified:
//
// Author: emil
//
// Description:
    Implementation for a message receiver using safeFIFO
   Note:
//
// 
*/

/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
* File: MessageQueue.cpp
*
* .........................................................................
*
*  Date     Description
* ------   -----------
*
***************************************************************************
*
* NOTES:
*
*
* Classes/Types:
*
*
* FUNCTIONS:
*
*
*/

# include "dctype.h"
# include "dotcastns.h"

# include "dcsynchronization.h"
# include "messageQueue.h"

__BEGIN_DC_NAME_SPACE

DEFINE_FILE_STRING("$Id", messageQueue);


void CMessageQueue::release (CMessage * pMsg)
{
    pMsg->setLength (0);
# if 0
    pMsg->m_buffer[0] = 'R';
# endif
    m_freeFIFO.enqueue (pMsg);
    m_pAccess->take ();
    m_freeCount++;
    m_pAccess->release ();
}



/*
    CMessageQueue -- construct a message queue.
*/
CMessageQueue::CMessageQueue (int maxMsgSize,
                              int freeCount,
                              int growFactor,
                              int blockFlags
                             )
    : 
      CMessageReceiver (maxMsgSize, freeCount, blockFlags),
      m_msgQueue (),
      m_freeFIFO (),
      m_waiting (0),
      m_pMessageEvent (FSyncObject::newEvent ()),
      m_pAccess (FSyncObject::newCriticalSection ())
{
    m_freeLimit = m_freeCount * growFactor;
    m_allocCount = m_freeCount;
    for (int element = 0; element < m_freeCount; element++)
    {
        CMessage * pMsg = makeMessage ();
# if 0
        TRACE ("pMsg:%x\n", pMsg);
# endif
        m_freeFIFO.enqueue (pMsg);
    }
}



CMessageQueue::~CMessageQueue ()
{
    CMessage * pMsg;

    waitForUsers ();
    m_pAccess->take ();
    while (m_msgQueue.size () > 0)
    {
# if 0
        TRACE ("Unread messages: %d\n", m_msgQueue.size ());
# endif
# if 0
        if (m_msgCount != m_msgQueue.size ())
            TRACE ("Warning queueCount: %d != size:%d\n",
                   m_msgCount, m_msgQueue.size ()
                  );
        m_msgCount--;
# endif
        if ((pMsg = m_msgQueue.dequeue ()))
        {
# if 0
            TRACE ("Delete pMsg:%x\n", pMsg);
# endif
            delete pMsg;
        }
    }
    while (m_freeFIFO.size () > 0)
    {
# if 0
        if (m_freeCount != m_freeFIFO.size ())
            TRACE ("Warning freeCount: %d != size:%d\n",
                   m_freeCount, m_freeFIFO.size ()
                  );
        m_freeCount--;
# endif
        if ((pMsg = m_freeFIFO.dequeue ()))
        {
# if 0
            TRACE ("Delete pMsg:%x\n", pMsg);
# endif
            delete pMsg;
        }
    }
    m_pAccess->release ();
    delete m_pAccess;
    delete m_pMessageEvent;

    if (m_pExcept)
    {
		delete m_pExcept;
	}
}


CMessage * CMessageQueue::allocMessage (void)
{
    CMessage * pMsg;

    m_pAccess->take ();
    int free = m_freeCount--;
    m_pAccess->release ();
    if (free > 0)
        pMsg = m_freeFIFO.dequeue ();
    else
    {
# if 0
        TRACE ("Out of messages\n");
# endif
        pMsg = 0;
        if (m_allocCount < m_freeLimit)
            pMsg = makeMessage ();
        if (!pMsg)
# if 0
            XException ("No message available");
# else
            m_pAccess->take ();
            return pMsg;
# endif
        // this implementation keeps them on the message list.
        m_pAccess->take ();
        m_freeCount++;
        m_allocCount++;
        m_pAccess->release ();
    }
    pMsg->setId (EMT_NoMessage);
    pMsg->setLength (0);
# if 0
    TRACE ("mq-alloc-pMsg:%x\n", pMsg);
# endif

    return pMsg;
}


CMessage * CMessageQueue::receive (void)
{
    CMessage * pMsg;

    m_pAccess->take ();
    while (m_msgCount == 0)
    {
        if (!(m_blockFlags & CMessageReceiver::blockOnReceive))
        {
            // satify the receive contract
            notifyEmpty ();
            m_pAccess->release ();
            return static_cast<CMessage *> (0);
        }
        m_waiting++;
        // unlock access while we are notifying.
        m_pAccess->release ();
        // satisfy the receive contract
        notifyEmpty ();
        m_pMessageEvent->wait ();
        m_waiting--;
        // relock access
        m_pAccess->take ();
    }
    m_msgCount--;
    if (m_msgCount <= 0)
        notifyEmpty ();
    pMsg = m_msgQueue.dequeue ();
    m_pAccess->release ();
# if 0
    ASSERT (pMsg);
# endif 
    if (m_pExcept)
        throw XException (*m_pExcept);

    return pMsg;
}

/* from CMessageReceiver */

inline bool CMessageQueue::send (CMessage * pMsg)
{
    m_pAccess->take ();
    m_msgQueue.enqueue (pMsg);
    m_msgCount++;
    if (m_waiting)
    {
        m_pMessageEvent->pulse ();
    }
    m_pAccess->release ();
    if (m_freeCount <= 0)
    {
# if 0
        TRACE ("cmq-send: blockFlags: %x\n", m_blockFlags);
# endif
        if (m_blockFlags & CMessageReceiver::blockOnSendFull)
            waitForEmpty ();
    }
# if 0
    TRACE ("Sent a message");
# endif
    return true;
}


void CMessageQueue::failed (const XException & reason)
{
    if (m_pExcept)
        throw XException ("second call to CMessageQueue::failed");
    m_pExcept = new XException (reason);
}

__END_DC_NAME_SPACE

