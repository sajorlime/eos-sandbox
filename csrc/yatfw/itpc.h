
/*

    Lapel Software, Inc. 2002.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.


    File: itpc.h

    Contains defintion that implements inter-thread procedure calls on top of
    messagine.


    Classes/Types:

        

*/


# ifndef __ITPC_H__
# define __ITPC_H__


# include "privatens.h"
# include "exttype.h"
# include "message.h"
# include "xception.h"

# ifdef __USE_MAPPING__
#   include <map>
# endif

// global Msg Id must define IdUType, IdUTypeSize, IdUClass, and IdUClassSize
# include "globalMsgId.h"


__BEGIN_PRIVATE_NAMESPACE

typedef bool (* ITPCHandler) (void * pMsgData, int msgLen);

struct SHandlerEntry 
{
    ITPCHandler m_h;
    IdUClass m_idClass;
    IdUType m_idType;
    uint m_msgSize;
    bool m_copy;

    SHandlerEntry (ITPCHandler h, IdUClass idClass, IdUType idType,
                   msgSize = 0, copy = false)
        :
            m_h (h),
            m_idClass (idClass),
            m_idType (idType),
            m_msgSize (msgSize),
            m_copy (copy)
    {
    }
};

# ifdef __USE_MAPPING__
    typedef std::map<int, SHandlerEntry *> HandlerMap;
# endif

class CitpcFuncTable
{
    SHandlerEntry * m_pEntries;
    int m_entries;
# ifdef __USE_MAPPING__
#   HandlerMap m_hMap;
# endif

    CitpcFuncTable & operator= (CitpcFuncTable & t) {return *this;}

public:
    explicit CitpcFuncTable (SHandlerEntry * pEntries, int entries)
        :
            m_pEntries (pEntries),
            m_entries (entries)
    {
# ifdef __USE_MAPPING__
        for (int i = 0; i < entries; i++)
# endif
    }

    
};


__END_PRIVATE_NAMESPACE


# endif

