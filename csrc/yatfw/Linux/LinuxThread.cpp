/*
//
// $Id$
//
// Copyright (c) 2000 Dotcast Inc.
// All rights reserved.
//
// Filename: LinuxThread.cpp
//
// Creation Date: 17 Jan 2000
//
// Last Modified:
//
// Author: marty@dotcast.net
//
// Description:
    Implements dotcast synchronization primatives for Linux using PThreads
//
// 
*/

# include <pthread.h>

# include "dctype.h"
# include "dotcastns.h"

# include "dcexception.h"
# include "../thread.h"
# include "Tracer.h"

__BEGIN_DC_NAME_SPACE

DEFINE_FILE_STRING("$Id", LinuxThread);

static const char *TraceName = "cthread";

CThreadList CThreadList::s_ThreadList;

CThreadList::CThreadList()
{
    m_pRwlock = FSyncObject::newRwLock();
    for (int i = 0; i < NTHREADS; i++)
      m_pThreads[i] = 0;
}

CThreadList::~CThreadList()
{
}

void CThreadList::addThread(CThread *thread)
{
  m_pRwlock->takeForWrite();
  for (int i = 0; i < NTHREADS; i++) {
    if (m_pThreads[i] == 0) {
      m_pThreads[i] = thread;
      break;
    }
  }
  m_pRwlock->unlock();
}

void CThreadList::deleteThread(CThread *thread)
{
  m_pRwlock->takeForWrite();
  for (int i = 0; i < NTHREADS; i++)
    if (m_pThreads[i] == thread)
      m_pThreads[i] = 0;
  m_pRwlock->unlock();
}


int CThread::next_id = 0;

//
// Wrapping xThread this way simplifies thread.h while
// maintaining consistency with pthread standard
//

extern "C" void *CThreadCWrapper(void *pThis);

CThread::CThread (uint32 priority, bool waitForStart)
    :
  m_id(next_id++),
	 m_pCallback(DefaultThreadNotificationHandler),
	 m_increment(0),
	 m_run (ts_initing)
{

 CTracer::pprintf(TraceName, CTracer::et_debug, "CThread(%d,%s)\n",
		 priority, waitForStart ? "True" : "False");

 // m_event is used to cause the thread to start.  The xThread
 // routine starts up and then waits for m_event to be set. This is
 // either set att the end of this constructor or by an explicit
 // call to the start() function.

 m_event = FSyncObject::newEvent();

 // m_wake is used to cause the thread to wake up when it is
 // sleeping.  It is waited on in sleep() and pulsed in wake()
 
 m_wake =  FSyncObject::newEvent();

 // m_xThreadEntered is used to catch the problem of a thread
 // object going away before xThread has been called by the
 // system thread.  A better solution is to not create the
 // system thread in the constructor but vie an explicit start()
 // invocation.  The problem is that if waitForStart is false then
 // the start() method is called from the constructor and we have the
 // same problem.  To do this right we are going to have to remove
 // waitForStart from the constructor.
 
 m_xThreadEntered =  FSyncObject::newEvent();

 // the actual thread object, which is a pthread_t in a pthreads
 // implementation.

 m_hThread = new pthread_t;

 if (pthread_create(m_hThread, 0, CThreadCWrapper, (void *)this))
   throw XException ("pthread_create failed");
 
 if (!waitForStart)
   start ();

 CTracer::pprintf(TraceName, CTracer::et_debug, "Cthread() done.\n");
 CThreadList::s_ThreadList.addThread(this);

}

CThread::~CThread() {

 CTracer::pprintf(TraceName, CTracer::et_debug, "~CThread(%x)\n", this);

 // Wait for system thread call to complete
 if (m_run == CThread::ts_initing) {
	 m_run = ts_terminated;
	 m_xThreadEntered->wait();
 }

 // Since we are about to lose this thread's handle, we need to
 // detach.  detach is harmless if the thread has already been
 // joined, and reaps the thread state if it isn't going to
 // be joined.

 CThreadList::s_ThreadList.deleteThread(this);

 if (m_hThread)
   pthread_detach(*m_hThread);
 delete m_hThread;
 delete m_event;
 delete m_wake;
 delete m_xThreadEntered;
    
}

bool CThread::start (void) {
 CTracer::pprintf(TraceName, CTracer::et_debug, "%x->start()\n", this);
 m_event->set ();
 return true;
}

/*
//
// NOTE: terminate does not cause an immediate termination in
//       pthreads.  See man pthread_cancel for a discussion of
//       the very gory details.
//

bool CThread::terminate (uint32 msWait) {

 CTracer::pprintf(TraceName, CTracer::et_debug, "%x->terminate(%d)\n",
		  this, msWait);

 if (msWait != INFINITE_TIMEOUT)
   throw XSynchronization("Unwise parameter usage");

  if (pthread_cancel(*m_hThread)) {
    throw XException("terminate failed");
  }

  return true;

}
*/

ThreadCallback CThread::setNotificationHandler (ThreadCallback handler) {
	ThreadCallback temp = m_pCallback;
	m_pCallback = ((handler == 0) ? DefaultThreadNotificationHandler : handler);
	return temp;
}

bool CThread::join(int *pReturn) {
  int i;

  CTracer::pprintf(TraceName, CTracer::et_debug, "%x->join(%x)\n",
		 this, pReturn);

  if (pthread_join(*m_hThread, (void **)&i))
    throw XException("Join failed");

  CTracer::pprintf(TraceName, CTracer::et_debug, "join returns %d\n", i);

  if (pReturn)
    *pReturn = i;

  return true;
}

bool CThread::sleep (uint32 ms)
{

  CTracer::pprintf(TraceName, CTracer::et_debug, "%x->sleep(%d)\n",
		   this, ms);

  m_run = ts_sleeping;
  m_wake->wait (ms);

  CTracer::pprintf(TraceName, CTracer::et_debug, "%x->sleep(%d) done\n",
		   this, ms);

  m_run = ts_running;
  return true;
}

bool CThread::wake (void) {
  CTracer::pprintf(TraceName, CTracer::et_debug, "%x->wake()\n", this);
  return m_wake->pulse ();
}


void CThread::dump (void) {
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "CThread handle: %x\n", m_hThread);
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "CThread m_run: %d\n", m_run);
}



unsigned long CThread::xThread(void *pParam)
{
  CThread * pT = (CThread *) pParam;
  //Check if the thread object has been deleted
  if (pT->m_run != ts_initing) {
	  pT->m_xThreadEntered->set();
	  return te_failure;
  }
  pT->m_run = CThread::ts_suspended;
  int r;
  pT->m_event->wait ();
  pT->m_run = CThread::ts_running;
  try  {
    CTracer::pprintf(TraceName, CTracer::et_debug,
		     "CThread %x entry\n", pT);
    r = pT->thread ();
    CTracer::pprintf(TraceName, CTracer::et_debug,
		     "CThread %x exited with %d\n", pT, r);
  } catch (XException (&x)) {
    CTracer::pprintf(TraceName, CTracer::et_debug,
		     "CThread %x exited with exception: %s\n",
		     pT, x.reason ());
  } catch (...) {
    CTracer::pprintf(TraceName, CTracer::et_debug,
		     "CThread %x exited with an unexpected exception.\n");
  }

  pT->m_pCallback(pT);

  pT->m_run = CThread::ts_terminated;

  return r;

}

void CThread::yield(void)
{
  sched_yield();
}

__END_DC_NAME_SPACE
