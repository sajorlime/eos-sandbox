#include <errno.h>
#include <pthread.h>
#include <string.h>

#include "dctype.h"
#include "dotcastns.h"
#include "dcsynchronization.h"
#include "Tracer.h"

__BEGIN_DC_NAME_SPACE

static char *TraceName = "sync";

class cCriticalSection : public PCriticalSection {
  friend class FSyncObject;

  pthread_mutex_t * m_pMutex;

  cCriticalSection (void) throw (XSynchronization) {
	pthread_mutexattr_t attr;
	attr.__mutexkind = PTHREAD_MUTEX_RECURSIVE_NP;

    m_pMutex = new pthread_mutex_t;
    if (pthread_mutex_init(m_pMutex, &attr)) {
      delete m_pMutex;
      throw XSynchronization("cCriticalSection::cCriticalSection - pthread_mutex_init failed");
    }
  }

public:
  virtual bool take (void) throw (XSynchronization) {
    if (pthread_mutex_lock(m_pMutex))
      throw XSynchronization("cCriticalSection::take - pthread_mutex_lock failed");
    return true;
  }

  virtual bool tryToTake (void) throw (XSynchronization) {
    int e = pthread_mutex_trylock(m_pMutex);
    if (e == EBUSY)
      return false;
    if (e)
      throw XSynchronization("cCriticalSection::tryToTake - pthread_mutex_trylock failed");
    return true;
  }

  virtual bool release (void) throw (XSynchronization) {
    if (pthread_mutex_unlock(m_pMutex))
      throw XSynchronization("cCriticalSection::release - pthread_mutex_unlock failed");
    return true;
  }

  virtual ~cCriticalSection () {
    int e = pthread_mutex_destroy(m_pMutex);
    delete m_pMutex;
    if (e)
      throw XSynchronization("cCriticalSection::~cCriticalSection - pthread_mutex_destroy failed");      
  }
};


class cMutex : public PMutex
{
  friend class FSyncObject;
  pthread_mutex_t * m_pMutex;

  cMutex (bool initiallyOwn, const char * pName)  throw (XSynchronization) {
	pthread_mutexattr_t attr;
    m_pMutex = new pthread_mutex_t;

	attr.__mutexkind = PTHREAD_MUTEX_RECURSIVE_NP;

    if (pthread_mutex_init(m_pMutex, &attr)) {
      delete m_pMutex;
      throw XSynchronization("cMutex::cMutex - pthread_mutex_init failed");
    }
    if (initiallyOwn) {
	  if (pthread_mutex_lock(m_pMutex)) {
		pthread_mutex_destroy(m_pMutex);
		delete m_pMutex;
		throw XSynchronization("cMutex::cMutex - pthread_mutex_lock failed");
	  }
    }
  }

public:
  virtual bool take (uint32 msTimeout) throw (XSynchronization) {
	  //unwise parameter usage
    if (msTimeout != INFINITE_TIMEOUT)
      throw XSynchronization("cMutex::take - Unwise parameter usage");
    if (pthread_mutex_lock(m_pMutex))
      throw XSynchronization("cMutex::take - pthread_mutex_lock failed");
    return true;
  }

  virtual bool tryToTake (void) throw (XSynchronization) {
    int e = pthread_mutex_trylock(m_pMutex);
    if (e == EBUSY)
      return false;
    if (e)
      throw XSynchronization("cCriticalSection::tryToTake - pthread_mutex_trylock failed");
    return true;
  }

  virtual bool release (void) {
    if (pthread_mutex_unlock(m_pMutex))
      throw XSynchronization("cMutex::release - pthread_mutex_unlock failed");
    return true;
  }

  virtual ~cMutex () {
    int e = pthread_mutex_destroy(m_pMutex);
    delete m_pMutex;
    if (e)
      throw XSynchronization("cMutex::~cMutex - pthread_mutex_destroy failed");
  }
};

class cSemaphore : public PSemaphore
{
  // FSyncObject::semaphore(..) creates PCriticalSection object.
  friend class FSyncObject;

  pthread_cond_t * m_pCond;
  pthread_mutex_t * m_pMutex;
  int m_currentCount;
  int m_maxCount;
  const char * _pName;

  cSemaphore (int32 initialCount, int32 maxCount, const char * pName)
    throw (XSynchronization) : m_currentCount(initialCount),
			       m_maxCount(maxCount), _pName(pName)
  {
    if (initialCount < 0 || initialCount > maxCount)
      throw XSynchronization("cSemaphore::cSemaphore bad count");
    m_pCond = new pthread_cond_t;
    if (pthread_cond_init(m_pCond, 0)) {
      delete m_pCond;
      throw XSynchronization("cSemaphore::cSemaphore - pthread_cond_init failed");
    }
    m_pMutex = new pthread_mutex_t;
    if (pthread_mutex_init(m_pMutex, 0)) {
      pthread_cond_destroy(m_pCond);
      delete m_pCond;
      delete m_pMutex;
      throw XSynchronization("cSemaphore::cSemaphore - pthread_mutex_init failed");
    }
  }

public:
  virtual bool take (uint32 msTimeout)
    throw(XSynchronization) {
    int e = 0;
    if (msTimeout != INFINITE_TIMEOUT)
      throw XSynchronization("cSemaphore::take - Unwise parameter usage");
    if (pthread_mutex_lock(m_pMutex))
      throw XSynchronization("cSemaphore::take - pthread_mutex_lock failed");
    while (m_currentCount == 0)
      e = pthread_cond_wait(m_pCond, m_pMutex);
    m_currentCount--;
    pthread_mutex_unlock(m_pMutex);
    if (e)
      throw XSynchronization("cSemaphore::take - pthread_cond_wait failed");
    return true;
  }

  virtual bool release (int32 count, int32 * pPrevCount)
    throw(XSynchronization) {
    if (count <= 0)
      throw XSynchronization("cSemaphore::release - release: nonpositive count");
    *pPrevCount = m_currentCount;
    if (count + m_currentCount > m_maxCount)
      return false;
    m_currentCount += count;
    if (*pPrevCount == 0) {
      if (pthread_cond_signal(m_pCond))
	throw XSynchronization("cSemaphore::release - pthread_cond_signal failed");
    }
    return true;
  }

  virtual bool release (void) throw(XSynchronization) {
    int pPrevCount = m_currentCount;
    if (m_currentCount + 1 > m_maxCount)
      return false;
    m_currentCount++;
    if (pPrevCount == 0) {
      if (pthread_cond_signal(m_pCond))
	throw XSynchronization("cSemaphore::release - pthread_cond_signal failed");
    }
    return true;
  }
	
  virtual ~cSemaphore () {
    int ec = pthread_cond_destroy(m_pCond);
    int em = pthread_mutex_destroy(m_pMutex);
    delete m_pCond;
    delete m_pMutex;
    if (ec)
      throw XSynchronization("cSemaphore::~cSemaphore - pthread_cond_destroy failed.");
    if (em)
      throw XSynchronization("cSemaphore::~cSemaphore - pthread_mutex_destroy failed.");
  }
};

class cEvent : public PEvent
{
  // FSyncObject::event(..) creates a PEvent object.
  friend class FSyncObject;

  pthread_cond_t * m_pCond;
  pthread_mutex_t * m_pMutex;
  char * m_pName;
  bool m_state;

  cEvent (const char * pName) throw (XSynchronization)  {
    m_state = false;
    if (pName) {
      m_pName = new char[strlen(pName)+1];
      strcpy(m_pName, pName);
    } else {
      m_pName = 0;
    }
    m_pCond = new pthread_cond_t;
    if (pthread_cond_init(m_pCond, 0)) {
      delete m_pCond;
      throw XSynchronization("cEvent::cEvent - pthread_cond_init failed");
    }
    m_pMutex = new pthread_mutex_t;
    if (pthread_mutex_init(m_pMutex, 0)) {
      pthread_cond_destroy(m_pCond);
      delete m_pCond;
      delete m_pMutex;
      throw XSynchronization("cEvent::cEvent - pthread_mutex_init failed");
    }
  }
    
public:
  virtual bool set (void) throw (XSynchronization) {

    if (pthread_mutex_lock(m_pMutex))
      throw XSynchronization("cEvent::set - pthread_mutex_lock failed");

    if (pthread_cond_broadcast(m_pCond))
      throw XSynchronization("cEvent::set - pthread_cond_broadcast failed");

    m_state = true;

    if (pthread_mutex_unlock(m_pMutex))
      throw XSynchronization("cEvent::set - pthread_mutex_unlock failed");

    return m_state;
  }

  virtual bool pulse (void) throw(XSynchronization) {

    if (pthread_mutex_lock(m_pMutex))
      throw XSynchronization("cEvent::pulse - pthread_mutex_lock failed");

    if (pthread_cond_broadcast(m_pCond))
      throw XSynchronization("cEvent::pulse - pthread_cond_broadcast failed");

    m_state = false;


    if (pthread_mutex_unlock(m_pMutex))
      throw XSynchronization("cEvent::pulse - pthread_mutex_unlock failed");

    return true;
  }

  virtual bool reset (void) {
    m_state = false;
    return true;
  }

  virtual bool wait (uint32 msTimeOut) {
    if (msTimeOut != INFINITE_TIMEOUT)
      throw XSynchronization("cEvent::wait - Unwise parameter usage");

    if (pthread_mutex_lock(m_pMutex))
      throw XSynchronization("cEvent::wait - pthread_mutex_lock failed");

    if (!m_state) {

		if (pthread_cond_wait(m_pCond, m_pMutex))
		  throw XSynchronization("cEvent::wait - pthread_cond_wait failed");

    }

    if (pthread_mutex_unlock(m_pMutex))
      throw XSynchronization("cEvent::wait - pthread_mutex_unlock failed");

    return true;

  }

  virtual ~cEvent (void) {
    int ec = pthread_cond_destroy(m_pCond);
    int em = pthread_mutex_destroy(m_pMutex);
    delete m_pCond;
    delete m_pMutex;
    if (ec)
      throw XSynchronization("cEvent::~cEvent - pthread_cond_destroy failed.");
    if (em)
      throw XSynchronization("cEvent::~cEvent - pthread_mutex_destroy failed.");
  }
};


/*
 * See "Concurrent Programming: Principles and Practice", by Gregory R.
 * Andrews for a formal discussion of Reader/Writer lock semantics.
 *
 * cRwLock implements a Writer Priority Reader/Writer Lock, using the
 * pthread primitive library. This is not precisely the same method
 * as described in Andrews because pthreads doesn't provide some of the
 * primitives.  See the comments before takeForRead and takeForWrite
 * for a discussion of the specific algorithms.
 *
 */
class cRwLock : public PRwLock
{
  // FSyncObject::newRwLock(...) creates a PRwLock object.
  friend class FSyncObject;
  cRwLock() throw(XSynchronization) ;

public:
  virtual void takeForRead() throw(XSynchronization);
  virtual void takeForWrite()throw(XSynchronization);
  virtual void unlock() throw(XSynchronization);
  virtual ~cRwLock() throw(XSynchronization);

private:
  unsigned int m_readers;	// # of current readers
  unsigned int m_readersWaiting; // # of current readers
  unsigned int m_writers;	// # of current writers
  unsigned int m_writersWaiting; // # of current writers
  pthread_mutex_t *m_pBaton;	// protect access to the above
  pthread_cond_t *m_pWaitingReader; // Wait for a chance to read
  pthread_cond_t *m_pWaitingWriter; // Wait for a chance to write
};

cRwLock::cRwLock() throw(XSynchronization)
{
  m_readers = 0;
  m_readersWaiting = 0;
  m_writers = 0;
  m_writersWaiting = 0;
  m_pBaton = new pthread_mutex_t;
  m_pWaitingWriter = new pthread_cond_t;
  m_pWaitingReader = new pthread_cond_t;
  if (pthread_mutex_init(m_pBaton, 0)) {
    goto failed;
  }
  if (pthread_cond_init(m_pWaitingReader, 0)) {
    pthread_mutex_destroy(m_pBaton);
    goto failed;
  }
  if (pthread_cond_init(m_pWaitingWriter, 0)) {
    pthread_mutex_destroy(m_pBaton);
    pthread_cond_destroy(m_pWaitingReader);
  }
  return;

 failed:
  delete m_pBaton;
  delete m_pWaitingWriter;
  delete m_pWaitingReader;
  throw XSynchronization("pthread_mutex_init failed");
}

cRwLock::~cRwLock() throw(XSynchronization)
{
  int em = pthread_mutex_destroy(m_pBaton);
  int ec1 = pthread_cond_destroy(m_pWaitingReader);
  int ec2 = pthread_cond_destroy(m_pWaitingWriter);
  delete m_pBaton;
  delete m_pWaitingReader;
  delete m_pWaitingWriter;
  if (ec1 || ec2)
    throw XSynchronization("pthread_cond_destroy failed.");
  if (em)
    throw XSynchronization("pthread_mutex_destroy failed.");
}

//
// Any # of readers are allowed, but writing is exclusive of reading.
//
// takeForRead grabs the baton, so that it can exclusively update
// the readersWaiting and readers counts.
//
// It then enters a loop, waiting for there to be no writers.
// The while loop avoids a possible race as a result of the
// return from the cond_wait: a writer may have appeared between
// when the wait started to return and when it reobtained the baton.
//
// Exiting the loop means that there are no writers, and this reader
// may continue, so increment the # of readers and free the baton.
//
// precondition  ( (m_writers == 1 && m_readers == 0) ||
//                 (m_writers == 0 && m_readers >= 0) )
// postcondition (m_readers > 0 && m_writers == 0)
//
void cRwLock::takeForRead() throw(XSynchronization)
{
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "takeForRead()\n");
  pthread_mutex_lock(m_pBaton);
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "takeForRead() readers = %d, writers = %d\n",
		   m_readers, m_writers);
  m_readersWaiting++;
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "takeForRead() waiter # %d\n",
		     m_readersWaiting);
  while (m_writers) {
    CTracer::pprintf(TraceName, CTracer::et_debug,
		     "takeForRead() waiter # %d waiting\n",
		     m_readersWaiting);
    pthread_cond_wait(m_pWaitingReader, m_pBaton);
  }
  m_readersWaiting--;
  m_readers++;
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "takeForRead() waiter # %d is now reader %d\n",
		   m_readersWaiting+1, m_readers);
  pthread_mutex_unlock(m_pBaton);
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "takeForRead() done\n");
  return;
}

//
// Only 1 writer is allowed, exclusive of all other writers and
// all readers.
//
// takeForWrite grabs the baton, so that it can exclusively update
// the writers and writersWaiting counts.
//
// It then enters a loop, waiting for there to be no writers and
// no readers.  The loop avoids a similar race to that described above.
//
// Exiting the loop means that there are no readers and no writers,
// so this writer may continue.  Increment the count of writers, and
// free the baton.
//
// precondition  ( (m_writers == 1 && m_readers == 0) ||
//                 (m_writers == 0 && m_readers >= 0) )

// postcondition (m_readers == 0 && m_writers == 1)
//
void cRwLock::takeForWrite() throw(XSynchronization)
{
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "takeForWrite()\n");
  pthread_mutex_lock(m_pBaton);
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "takeForWrite() readers = %d, writers = %d\n",
		   m_readers, m_writers);
  m_writersWaiting++;
  while (m_writers > 0 || m_readers > 0) {
    CTracer::pprintf(TraceName, CTracer::et_debug,
		     "takeForWrite() waiter # %d waiting\n",
		     m_writersWaiting);
    pthread_cond_wait(m_pWaitingWriter, m_pBaton);
  }
  m_writersWaiting--;
  m_writers++;
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "takeForWrite() waiter # %d is now writer %d\n",
		   m_writersWaiting+1, m_writers);
  pthread_mutex_unlock(m_pBaton);
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "takeForWrite() done\n");
  return;
}

//
// unlock the Reader/Writer lock
//
// grab the baton, so that counts can be exclusively updated
//
// if there were any writers, then it is a writer that is freeing the
// lock, so decrement m_writers. Otherwise, it was one of the one or
// more current readers, so decrement the readers count.
//
// If there are writers waiting and this was the last reader,
//   signal one writer to continue.
// else if there are no writers but there are readers
//   signal all readers to continue.
//
// free up the baton
//
// precondition  ( (m_writers == 1 && m_readers == 0) ||
//                 (m_writers == 0 && m_readers > 0) )
// postcondition   (m_writers == 0 && m_readers >= 0 )
//
void cRwLock::unlock() throw(XSynchronization)
{
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "unlock()\n");
  pthread_mutex_lock(m_pBaton);
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "unlock() readers = %d, writers = %d\n",
		   m_readers, m_writers);
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "         waiting readers = %d, waiting writers = %d\n",
		   m_readersWaiting, m_writersWaiting);

  if (m_writers) {		// Current holder was the only writer
    CTracer::pprintf(TraceName, CTracer::et_debug,
		     "unlock() lock freed by writer.\n");
    m_writers--;
  } else {
    CTracer::pprintf(TraceName, CTracer::et_debug,
		     "unlock() lock freed by reader.\n");
    m_readers--;		// Current holder was a reader
  }

  if (m_writersWaiting && m_readers == 0) {
    CTracer::pprintf(TraceName, CTracer::et_debug,
		     "unlock() writer waiting, no readers active.\n");
    pthread_cond_signal(m_pWaitingWriter);
  } else if (m_writersWaiting == 0 && m_writers == 0 && m_readersWaiting) {
    CTracer::pprintf(TraceName, CTracer::et_debug,
		     "unlock() readers waiting, no writers active.\n");
    pthread_cond_broadcast(m_pWaitingReader);
  }

  pthread_mutex_unlock(m_pBaton);
  CTracer::pprintf(TraceName, CTracer::et_debug,
		   "unlock() done.\n");
  return;
}

PCriticalSection * FSyncObject::newCriticalSection ()
  throw (XSynchronization)
{
    return new cCriticalSection ();
}

PMutex * FSyncObject::newMutex (bool initiallyOwn, const char * pName)
{
    return new cMutex (initiallyOwn ? 1 : 0, pName);
}

PRwLock * FSyncObject::newRwLock() throw(XSynchronization)
{
  return new cRwLock();
}

PSemaphore * FSyncObject::newSemaphore (int32 initialCount,
					int32 maxCount,
					const char * pName)
  throw (XSynchronization)
{
    return new cSemaphore (initialCount, maxCount, pName);
}

PEvent * FSyncObject::newEvent (const char * pName)
throw (XSynchronization)
{
    return new cEvent (pName);
}

__END_DC_NAME_SPACE
