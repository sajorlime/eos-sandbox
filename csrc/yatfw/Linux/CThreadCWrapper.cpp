#ifdef USE_TAU
#include <Profile/Profiler.h>
#endif

# include <pthread.h>

# include "dctype.h"
# include "dotcastns.h"

# include "dcexception.h"
# include "../thread.h"
# include "Tracer.h"


__BEGIN_DC_NAME_SPACE

DEFINE_FILE_STRING("$Id", CThreadCWrapper);


extern "C" void *CThreadCWrapper(void *pThis) {

#ifdef USE_TAU
  TAU_REGISTER_THREAD();
  TAU_PROFILE_TIMER(t1,"CThreadCWrapper", "void * ( void *pThis)",
		    TAU_DEFAULT);
  TAU_PROFILE_START(t1);
#endif

  unsigned long i = CThread::xThread(pThis);

#ifdef USE_TAU
  TAU_PROFILE_STOP(t1);
#endif

  return (void *)i;
}

__END_DC_NAME_SPACE
