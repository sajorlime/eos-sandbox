/*
//
// $Id$
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Filename: winsynchronization.cpp
//
// Creation Date: 12-27-99
//
// Last Modified:
//
// Author: emil
//
// Description:
    Implements dotcast synchronization primatives for win32
//
// 
*/


#   include <stdarg.h>
#   include <wtypes.h>
#   include <native.h>
#   include <winbase.h>

# define INFINITE_TIMEOUT ((unsigned) -1)

#if (_WIN32_WINNT >= 0x0400)
DWORD ReleaseMutexAndWaitOnEvent (HANDLE lock, HANDLE condition) {
	DWORD r;
	r = ::SignalObjectAndWait(lock, condition, INFINITE_TIMEOUT, FALSE);
	if (r == WAIT_FAILED) {
		return WAIT_FAILED;
	}
	r = ::WaitForSingleObject(lock, INFINITE_TIMEOUT);
	return r;
}
#else
DWORD ReleaseMutexAndWaitOnEvent (HANDLE lock, HANDLE condition) {
	DWORD r;
	::ReleaseMutex(lock);
	r = ::WaitForSingleObject(condition, INFINITE_TIMEOUT);
	if (r == WAIT_FAILED) {
		return WAIT_FAILED;
	}
	r = ::WaitForSingleObject(lock, INFINITE_TIMEOUT);
	return r;
}
#endif

# include "dctype.h"
# include "dotcastns.h"

# include "dcexception.h"
# include "dcsynchronization.h"

__BEGIN_DC_NAME_SPACE

DEFINE_FILE_STRING("$Id", winsynchronization);

class cCriticalSection : public PCriticalSection
{
    friend class FSyncObject;

    CRITICAL_SECTION m_cs;

    cCriticalSection (void)
    {
        InitializeCriticalSection (&m_cs);
    }

public:
    virtual bool take (void) 
    {
        EnterCriticalSection (&m_cs);
        return true;
    }

    virtual bool tryToTake (void) 
    {
#if(_WIN32_WINNT >= 0x0400)
        if (TryEnterCriticalSection (&m_cs))
            return true;
# else // Not NT > 4.0
		throw XSynchronization ("tryToTake method called, but not implemented on this platform");
# endif
        return false;
    }

    virtual bool release (void) 
    {
        LeaveCriticalSection (&m_cs);
        return true;
    }

    virtual ~cCriticalSection ()
    {
        DeleteCriticalSection (&m_cs);
    }
};


class cMutex : public PMutex
{
    friend class FSyncObject;

    HANDLE m_h;

    cMutex (bool initiallyOwn, const char * pName) throw (XSynchronization)
    {
        /* 
            We are starting with a null security descriptor
            but we may need to change this if we want to 
            inherit this mutex in another process.
        */
        m_h = CreateMutex (0, initiallyOwn, pName);
        if (m_h == 0)
        {
            throw XSynchronization ("CreateMutex returned invalid handle");
        }
    }

public:
	virtual bool take (uint32 msTimeout)
    {
        DWORD r = ::WaitForSingleObject (m_h, msTimeout);
        if (r == WAIT_OBJECT_0)
            return true;
        return false;
    }

    virtual bool tryToTake (void) 
    {
        DWORD r = ::WaitForSingleObject (m_h, 0);
        if (r == WAIT_FAILED) {
			throw XSynchronization ("tryToTake method errored");
		}
		if (r == WAIT_OBJECT_0) {
			return true;
		}
		return false;
    }

	virtual bool release (void)
    {
        if (ReleaseMutex (m_h))
            return true;
        return false;
    }

	virtual ~cMutex ()
    {
        if (m_h != 0)
            CloseHandle (m_h);
    }
};

class cSemaphore : public PSemaphore
{
    // FSyncObject::semaphore(..) creates PCriticalSection object.
    friend class FSyncObject;

    HANDLE m_h;

    cSemaphore (int32 initialCount, int32 maxCount, const char * pName) throw (XSynchronization)
    {
        m_h = CreateSemaphore (0, initialCount, maxCount, pName);
        if (m_h == 0)
        {
            throw XSynchronization ("CreateSemaphore returned invalid handle");
        }
    }

public:
	virtual bool take (uint32 msTimeout)
    {
        DWORD r = ::WaitForSingleObject (m_h, msTimeout);
        if (r == WAIT_OBJECT_0)
            return true;
        return false;
    }
	virtual bool release (int32 count, int32 * pPrevCount)
    {
        if (ReleaseSemaphore (m_h, count, pPrevCount))
            return true;
        return false;
    }
	virtual bool release (void)
    {
        if (ReleaseSemaphore (m_h, 1, 0))
            return true;
        return false;
    }
	virtual ~cSemaphore ()
    {
        if (m_h != 0)
            CloseHandle (m_h);
    }
};

class cEvent : public PEvent
{
    // FSyncObject::event(..) creates a PEvent object.
    friend class FSyncObject;

    HANDLE m_h;

    cEvent (const char * pName) throw (XSynchronization)
    {
        m_h = CreateEvent (0, TRUE, FALSE, pName);
        if (m_h == 0)
        {
            throw XSynchronization ("CreateEvent returned invalid handle");
        }
    }
    
public:
	virtual bool set (void)
    {
        if (SetEvent (m_h))
            return true;
        return false;
    }

	virtual bool pulse (void)
    {
        if (PulseEvent (m_h))
            return true;
        return false;
    }

	virtual bool reset (void)
    {
        if (ResetEvent (m_h))
            return true;
        return false;
    }

	virtual bool wait (uint32 msTimeOut)
    {
        DWORD r = ::WaitForSingleObject (m_h, msTimeOut);
        if (r == WAIT_OBJECT_0)
            return true;
        return false;
    }

	virtual ~cEvent (void)
    {
        if (m_h != 0)
            CloseHandle (m_h);
    }
};


/*
 * See Marty for an informal discussion of Reader/Writer lock semantics.
 *
 * cRwLock implements a Writer Priority Reader/Writer Lock, using
 * Windows primitives. This may not be precisely the same method
 * as described in Andrews because I'm emulating Marty's Linux implementation
 * which makes this same unclaim.  
 *
 * See the comments before takeForRead and takeForWrite
 * for a discussion of the specific algorithms.
 *
 */
class cRwLock : public PRwLock
{
	// FSyncObject::newRwLock(...) creates a PRwLock object.
	friend class FSyncObject;
	cRwLock() throw(XSynchronization) ;
	
public:
	virtual void takeForRead() throw(XSynchronization);
	virtual void takeForWrite()throw(XSynchronization);
	virtual void unlock() throw(XSynchronization);
	virtual ~cRwLock() throw(XSynchronization);
	
private:
	unsigned int m_readers;	// # of current readers
	unsigned int m_readersWaiting; // # of current readers
	unsigned int m_writers;	// # of current writers
	unsigned int m_writersWaiting; // # of current writers
	HANDLE m_hBaton;	// protect access to the above
	HANDLE m_hWaitingReader; // Wait for a chance to read
	HANDLE m_hWaitingWriter; // Wait for a chance to write
};

cRwLock::cRwLock() throw(XSynchronization)
{
	m_readers = 0;
	m_readersWaiting = 0;
	m_writers = 0;
	m_writersWaiting = 0;
	m_hBaton = ::CreateMutex(0, FALSE /* Not initially owned */, 0);
	if (m_hBaton == 0) {
		throw XSynchronization ("CreateMutex returned invalid handle");
	}
	m_hWaitingReader = ::CreateEvent(0, FALSE /* Not manual reset */, FALSE /* Not initially owned */, 0);
	if (m_hWaitingReader == 0) {
		::CloseHandle (m_hBaton);
		throw XSynchronization ("CreateEvent returned invalid handle");
	}
	m_hWaitingWriter = ::CreateEvent(0, TRUE /* Manual reset */, FALSE /* Not initially owned */, 0);
	if (m_hWaitingWriter == 0) {
		::CloseHandle (m_hBaton);
		::CloseHandle (m_hWaitingReader);
		throw XSynchronization ("CreateEvent returned invalid handle");
	}
}

cRwLock::~cRwLock()
{
	if (m_hBaton != 0) {
		::CloseHandle (m_hBaton);
	}
	if (m_hWaitingReader != 0) {
		::CloseHandle (m_hWaitingReader);
	}
	if (m_hWaitingWriter != 0) {
		::CloseHandle (m_hWaitingWriter);
	}
}

//
// Any # of readers are allowed, but writing is exclusive of reading.
//
// takeForRead grabs the baton, so that it can exclusively update
// the readersWaiting and readers counts.
//
// It then enters a loop, waiting for there to be no writers.
// The while loop avoids a possible race as a result of the
// return from the cond_wait: a writer may have appeared between
// when the wait started to return and when it reobtained the baton.
//
// Exiting the loop means that there are no writers, and this reader
// may continue, so increment the # of readers and free the baton.
//
// precondition  ( (m_writers == 1 && m_readers == 0) ||
//                 (m_writers == 0 && m_readers >= 0) )
// postcondition (m_readers > 0 && m_writers == 0)
//
void cRwLock::takeForRead() throw(XSynchronization)
{
	DWORD r;
	r = ::WaitForSingleObject (m_hBaton, INFINITE_TIMEOUT);
	if (r == WAIT_FAILED) {
		throw XSynchronization ("Wait on Mutex for RwLock failed");
	}
	m_readersWaiting++;
	while (m_writers) {
		r = ReleaseMutexAndWaitOnEvent(m_hBaton, m_hWaitingReader);
	}
	m_readersWaiting--;
	if (r == WAIT_FAILED) {
		throw XSynchronization("Wait for Read Condition failed.");
	}
	m_readers++;
	::ReleaseMutex(m_hBaton);
	return;
}

//
// Only 1 writer is allowed, exclusive of all other writers and
// all readers.
//
// takeForWrite grabs the baton, so that it can exclusively update
// the writers and writersWaiting counts.
//
// It then enters a loop, waiting for there to be no writers and
// no readers.  The loop avoids a similar race to that described above.
//
// Exiting the loop means that there are no readers and no writers,
// so this writer may continue.  Increment the count of writers, and
// free the baton.
//
// precondition  ( (m_writers == 1 && m_readers == 0) ||
//                 (m_writers == 0 && m_readers >= 0) )

// postcondition (m_readers == 0 && m_writers == 1)
//
void cRwLock::takeForWrite() throw(XSynchronization)
{
	DWORD r;	
	r = ::WaitForSingleObject (m_hBaton, INFINITE_TIMEOUT);
	if (r == WAIT_FAILED) {
		throw XSynchronization ("Wait on Mutex for RwLock failed");
	}
	m_writersWaiting++;
	while (m_writers > 0 ||  m_readers > 0) {
		r = ReleaseMutexAndWaitOnEvent(m_hBaton, m_hWaitingReader);
	}
	m_writersWaiting--;
	if (r == WAIT_FAILED) {
		throw XSynchronization("Wait for Write Condition failed.");
	}
	m_writers++;
	::ReleaseMutex(m_hBaton);
	return;
}

//
// unlock the Reader/Writer lock
//
// grab the baton, so that counts can be exclusively updated
//
// if there were any writers, then it is a writer that is freeing the
// lock, so decrement m_writers. Otherwise, it was one of the one or
// more current readers, so decrement the readers count.
//
// If there are writers waiting and this was the last reader,
//   signal one writer to continue.
// else if there are no writers but there are readers
//   signal all readers to continue.
//
// free up the baton
//
// precondition  ( (m_writers == 1 && m_readers == 0) ||
//                 (m_writers == 0 && m_readers > 0) )
// postcondition   (m_writers == 0 && m_readers >= 0 )
//
void cRwLock::unlock() throw(XSynchronization)
{
	DWORD r;	
	r = ::WaitForSingleObject (m_hBaton, INFINITE_TIMEOUT);
	if (r == WAIT_FAILED) {
		throw XSynchronization ("Wait on Mutex to release RwLock failed");
	}
	if (m_writers) {		// Current holder was the only writer
		m_writers--;
	} else {
		m_readers--;		// Current holder was a reader
	}
	if ((m_writersWaiting > 0) && (m_readers == 0)) {
		::SetEvent(m_hWaitingWriter);
	}
	else if ((m_writersWaiting == 0) && (m_writers == 0)
		 && (m_readersWaiting > 0)) {
		::PulseEvent(m_hWaitingReader);
	}
	::ReleaseMutex(m_hBaton);
	return;
}

PCriticalSection * FSyncObject:: newCriticalSection (void)
{
    return new cCriticalSection ();
}

PMutex * FSyncObject::newMutex (bool initiallyOwn, const char * pName)
{
    return new cMutex (initiallyOwn ? 1 : 0, pName);
}

PRwLock * FSyncObject::newRwLock() throw(XSynchronization)
{
  return new cRwLock();
}

PSemaphore * FSyncObject::newSemaphore (int32 initialCount, int32 maxCount, const char * pName)
{
    return new cSemaphore (initialCount, maxCount, pName);
}

PEvent * FSyncObject::newEvent (const char * pName)
{
    return new cEvent (pName);
}

__END_DC_NAME_SPACE

