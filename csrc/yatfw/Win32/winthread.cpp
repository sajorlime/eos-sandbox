/*
//
// $Id$
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Filename: winsynchronization.cpp
//
// Creation Date: 12-27-99
//
// Last Modified:
//
// Author: emil
//
// Description:
    Implements dotcast synchronization primatives for win32
//
// 
*/


# include "dctype.h"
# include "dotcastns.h"

# include "dcexception.h"
# include "thread.h"

#   include <stdarg.h>
#   include <wtypes.h>
#   include <native.h>
#   include <winbase.h>


unsigned long WINAPI dcxThread (LPVOID pParam)
{
    return CThread::xThread (pParam);
}

__BEGIN_DC_NAME_SPACE

DEFINE_FILE_STRING("$Id", winthread);


# ifndef DEFAULT_STACK_SIZE
#   define DEFAULT_STACK_SIZE (1 << 10)
# endif

# define MAX_WT_PRIORITY 7

# if !defined (WINAPI) || !defined (_WIN32)
error in make
# endif

/*
    Note on priorities for Win32.
    For Win32 the threads in a process can have at most seven 
    priority levels, but depending on the Priority-Class of the
    process the number of levels may be reduced by one or two.
    Only the REAL_TIME_PRIORITY_CLASS provides a full seven levels
    and these are all at higher priorities than other classes. 

    Clearly the RT-Class should only be used with great care.

    The story of priority is further complicated by the fact that
    for classes other than RT priorities overlap in complex and
    not very obvious ways.  NTL, within one process there is generally
    seperation in priority between threads that are seperated
    by more than one priority level and always for three priority
    levels.

    So if an application consists of a single process the first
    thing to consider is the priority-class of that process.  You
    should have a good reason for choosing any thing other than
    the normal priority class.

    If you think you need the RT Class consider making your application
    with more than one process so that you can seperate real-time from
    non-RT issues.
*/

CThread::CThread (uint32 priority, bool waitForStart)
    :
     m_event (FSyncObject::newEvent ()),
     m_wake (FSyncObject::newEvent ()),
     m_xThreadEntered (FSyncObject::newEvent ()),
     m_run (ts_initing),
	 m_pCallback(DefaultThreadNotificationHandler)
{
    DWORD threadId;
    DWORD dwPriority = THREAD_PRIORITY_NORMAL;

    if ((int32) priority <= 0)
    {
        if (priority == INHERIT_THREAD_PRIORITY)
        {
            dwPriority = GetThreadPriority (GetCurrentThread ());
            if (dwPriority == 0)
                dwPriority = THREAD_PRIORITY_NORMAL;
        }
    }
    else
    {
        switch (priority)
        {
        case 1:
            dwPriority = THREAD_PRIORITY_IDLE;
            break;
        case 2:
            dwPriority = THREAD_PRIORITY_LOWEST;
            break;
        case 3:
            dwPriority = THREAD_PRIORITY_BELOW_NORMAL;
            break;
        case 4:
            dwPriority = THREAD_PRIORITY_NORMAL;
            break;
        case 5:
            dwPriority = THREAD_PRIORITY_ABOVE_NORMAL;
            break;
        case 6:
            dwPriority = THREAD_PRIORITY_HIGHEST;
            break;
        case 7:
            dwPriority = THREAD_PRIORITY_TIME_CRITICAL;
            break;
        default:
            dwPriority = THREAD_PRIORITY_NORMAL;
            break;
        }
    }

    m_hThread = CreateThread ((LPSECURITY_ATTRIBUTES) 0, DEFAULT_STACK_SIZE, ::dcxThread, (LPVOID) this, 0, &threadId);
    if (m_hThread == NULL)
    {
        throw XException ("CreateThread Failed");
    }
    // note WinCE may not support this.
    SetThreadPriority (m_hThread, dwPriority);
    if (!waitForStart)
        start ();
}



CThread::~CThread()
{
	if (m_run == CThread::ts_initing) {
		m_run = ts_terminated;
		m_xThreadEntered->wait();
	}
	CloseHandle(m_hThread);
	delete m_event;
    delete m_wake;
    delete m_xThreadEntered;
}

bool CThread::start (void)
{
    m_event->set ();
    return true;
}

/*
bool CThread::terminate (uint32 msWait)
{
    switch (m_run)
    {
    case ts_terminated:
        return true;
    case ts_sleeping:
        wake ();
        break;
    }
    HANDLE v[2];
    m_run = ts_terminated;
    v[0] = m_event;
    v[1] = m_hThread;
    uint32 r = WaitForMultipleObjects (2, v, 0, msWait);
    if (r == WAIT_TIMEOUT)
    {
        TerminateThread (m_hThread, te_failure);
        return false;
    }
    return true;
}
*/

bool CThread::join(int *pReturn) {
	if (m_run != ts_terminated) {
		uint32 r = WaitForSingleObject(m_hThread, INFINITE_TIMEOUT);
		if (r == WAIT_TIMEOUT) {
			return false;
		}
	}
	if (pReturn != 0) {
		DWORD threadReturnValue;
		GetExitCodeThread(m_hThread, &threadReturnValue);
		*pReturn = (int)threadReturnValue;
	}
	return true;
}

ThreadCallback CThread::setNotificationHandler (ThreadCallback handler) {
	ThreadCallback temp = m_pCallback;
	m_pCallback = ((handler == 0) ? DefaultThreadNotificationHandler : handler);
	return temp;
}

bool CThread::sleep (uint32 ms)
{
    m_run = ts_sleeping;
    m_wake->wait (ms);
    m_run = ts_running;
    return true;
}

bool CThread::wake (void)
{
    return m_wake->pulse ();
}


void CThread::dump (void)
{
# if 0
    TRACE ("CThread handle:%x\n", m_hThread);
    TRACE ("CThread m_run:%d\n", m_run);
# endif
}



unsigned long CThread::xThread (void * pParam)
{
	int r;
    CThread * pT = (CThread *) pParam;
	if (pT->m_run != ts_initing) {
		pT->m_xThreadEntered->set();
		return te_failure;
	}
    pT->m_run = CThread::ts_suspended;
    pT->m_event->wait ();
    pT->m_run = CThread::ts_running;
    try
    {
# if 0
        TRACE ("CThread %x entry\n", pT);
# endif
        r = pT->thread ();
# if 0
        TRACE ("CThread %x exited with %d\n", pT, r);
# endif
    }
    catch (
# if 0
            XException (&x)
# else
            ...
# endif
	      )
    {
# ifdef _DEBUG
# if 0
        TRACE ("CThread %x exited with exception: %s\n", pT, x.reason ());
# endif
# endif
    }
    pT->m_run = CThread::ts_terminated;
	pT->m_pCallback(pT);
    return (unsigned long) r;
}

void CThread::yield(void)
{
    Sleep(1);
}

__END_DC_NAME_SPACE

