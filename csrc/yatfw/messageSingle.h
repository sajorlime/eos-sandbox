/*
//
// $Id$
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Filename: messageSingle.h
//
// Creation Date: 01-16-00
//
// Last Modified:
//
// Author: emil
//
// Description:
    Defines concrete class for message receivers that support a single
    message.
   Note:
//
// 
*/



# ifndef __MESSAGESINGLE_H__
# define __MESSAGESINGLE_H__

# include "dotcastns.h"
# include "dctype.h"
# include "message.h"
//# include "safeFIFO.h"


__BEGIN_DC_NAME_SPACE

/*
    A message single is inter-thread communication object.
    It allows for only one message buffer,
    The class extends CMessageReceiver, which
    is passed to clients to allow them to send messages.

*/
class CMessageSingle : public CMessageReceiver
{
    friend class CMessageReceiverFactory;
private:
    CMessage * m_pTheMessage;
    CMessage * m_pAvailMessage;
    PEvent * m_pMessageEvent;
    PCriticalSection * m_pAccess;
    virtual bool waitForEmpty (void) {return false;}

public:
    CMessage * allocMessage (void);
    // from CMessageReceiver
    virtual CMessage * receive (void);
    bool send (CMessage * pMsg);
    virtual void release (CMessage * pMsg);
    virtual void failed (const XException & reason);

    // CMessageSingle extentions
    CMessageSingle (int msgSize, int blockFlags = CMessageReceiver::blockOnReceive);
    virtual ~CMessageSingle ();
};

__END_DC_NAME_SPACE

# endif

