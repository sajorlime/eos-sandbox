/* 
// 
// $Id$ 
// 
// Copyright (c) 1999-2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: safeFIFO.h
// 
// Creation Date: 12/21/99
// 
// Last Modified: 
// 
// Author: Emil Rojas
// 
// Description: 
//	Implements a thread safe FIFO queue.
// 
//  
*/ 


# ifndef __SAFEFIFO_H__
# define __SAFEFIFO_H__


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



/* original header
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
* safeQueue.h
*
* .........................................................................
*
*  Date     Description
* ------   -----------
*
*
*
* Classes/Types:
    TSafeFIFO -- is a class implements a FIFO queue that is thread safe.
*
*
*/


# include <queue>
using namespace std;

# include "dctype.h"
# include "dcsynchronization.h"
# include "dcexception.h"

__BEGIN_DC_NAME_SPACE

/*
    TSafeFIFO -- is a template class that allows one to define a queue of
    an type that can be accessed by multile threads of control.

    Usage:
        TSafeFIFO<CMessage *> m_msgQueue;
*/



template<class _Ty> class TSafeFIFO
{
private:
    PCriticalSection * m_pAccess;
    queue<_Ty> m_queue;

public:
    _Ty dequeue (void)
    {
        _Ty Ty;
        if (m_queue.size () <= 0)
        { // note is Ty is not a pointer we are creating zero objects.
            return (_Ty) 0;
        }
        m_pAccess->take ();

        Ty = m_queue.front ();
        m_queue.pop ();

        m_pAccess->release ();
        
        return Ty;
    }
    void enqueue (_Ty Ty)
    {
        m_pAccess->take ();

        m_queue.push (Ty);

        m_pAccess->release ();
    }
    int size ()
    {
        return m_queue.size ();
    }
    TSafeFIFO (void)
        :
          m_pAccess (FSyncObject::newCriticalSection ()),
          m_queue ()
    {
    }
    TSafeFIFO (PCriticalSection * pCS)
        :
          m_pAccess (pCS),
          m_queue ()
    {
    }
};

__END_DC_NAME_SPACE


# endif
