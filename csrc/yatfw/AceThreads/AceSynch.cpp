////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 1999-2001 Dotcast Inc.
// All rights reserved.
//
// Filename: AceSynch.cpp
//
// Creation Date: 7-09-01
//
// Last Modified:
//
// Author: Victor Bernard
//
// Description:
//   Implements dotcast synchronization primatives for ACE
//
////////////////////////////////////////////////////////////////////////////


#include "ace/Synch.h"
#include "ace/Thread.h"


# include "dctype.h"
# include "dotcastns.h"

# include "dcexception.h"
# include "dcsynchronization.h"

__BEGIN_DC_NAME_SPACE

class cCriticalSection : public PCriticalSection
{
    friend class FSyncObject;

    ACE_Recursive_Thread_Mutex m_h;

    cCriticalSection (void)
    {
    }

public:
    virtual bool take (void) 
    {
        m_h.acquire();
        return true;
    }

    virtual bool tryToTake (void) 
    {
        int retCode = m_h.tryacquire();
        if( retCode == 0 )
            return true;
        return false;
    }

    virtual bool release (void) 
    {
        int retCode = m_h.release();
        if( retCode == 0 )
            return true;
        return true;
    }

    virtual ~cCriticalSection ()
    {
    }
};


class cMutex : public PMutex
{
    friend class FSyncObject;

    ACE_Thread_Mutex m_h;

    cMutex (bool initiallyOwn, const char * )  throw (XSynchronization)
    {
        if( initiallyOwn )
            m_h.acquire();
    }

public:
	virtual bool take (uint32 msTimeout )
    {
        if( msTimeout != INFINITE_TIMEOUT )
          throw XSynchronization("cMutex::take - Unwise parameter usage");

        int retCode = m_h.acquire();

        if( retCode != 0 )
            throw XSynchronization("cMutex::take - Ace_Thread_Mutex::acquire() failed");

        return true;
    }

    virtual bool tryToTake (void) 
    {

        int retCode = m_h.tryacquire();
        if( retCode == 0 )
            return true;

        return false;
    }

	virtual bool release (void)
    {
        if( m_h.release() == 0 )
            return true;
        return false;
    }

	virtual ~cMutex ()
    {
    }
};

class cSemaphore : public PSemaphore
{
    // FSyncObject::semaphore(..) creates PCriticalSection object.
    friend class FSyncObject;

    ACE_Semaphore m_h;

    cSemaphore( int32 initialCount, int32 maxCount, const char * pName ) throw (XSynchronization) :
        m_h( (u_int) initialCount, USYNC_THREAD, pName, NULL, (int) maxCount )
    {
    }

public:
	virtual bool take (uint32 msTimeout)
    {

        ACE_Time_Value tv( (msTimeout/1000), 1000*(msTimeout%1000) );
        int retCode = m_h.acquire( tv );
        if( retCode == 0 )
            return true;
        return false;
    }
	virtual bool release (int32 count, int32 * pPrevCount)
    {
        if( pPrevCount != NULL )
            throw XSynchronization ("ACE version of cSemaphore does not support ret the previous count");

        int retCode = m_h.release( (size_t) count );
        if( retCode == 0 )
            return true;

        return false;
    }
	virtual bool release (void)
    {
        int retCode = m_h.release();
        if( retCode == 0 )
            return true;
        return false;
    }
	virtual ~cSemaphore ()
    {
    }
};

class cEvent : public PEvent
{
    // FSyncObject::event(..) creates a PEvent object.
    friend class FSyncObject;

    ACE_Event m_h;

    cEvent (const char * pName)  throw (XSynchronization)
        : m_h( 0, 0, USYNC_THREAD,  pName )
    {
    }
    
public:
	virtual bool set (void)
    {
        int retCode = m_h.signal();
        if( retCode == 0 )
            return true;
        return false;
    }

	virtual bool pulse (void)
    {
        int retCode = m_h.pulse();
        if( retCode == 0 )
            return true;
        return false;
    }

	virtual bool reset (void)
    {
        int retCode = m_h.reset();
        if( retCode == 0 )
            return true;
        return false;
    }

	virtual bool wait (uint32 msTimeout)
    {
        ACE_Time_Value tv( (msTimeout/1000), 1000*(msTimeout%1000) );
        int retCode = m_h.wait( &tv );
        if( retCode == 0 )
            return true;
        return false;
    }

	virtual ~cEvent (void)
    {
    }
};


/*
 * See Marty for an informal discussion of Reader/Writer lock semantics.
 *
 * cRwLock implements a Writer Priority Reader/Writer Lock, using
 * Windows primitives. This may not be precisely the same method
 * as described in Andrews because I'm emulating Marty's Linux implementation
 * which makes this same unclaim.  
 *
 * See the comments before takeForRead and takeForWrite
 * for a discussion of the specific algorithms.
 *
 */
class cRwLock : public PRwLock
{
	// FSyncObject::newRwLock(...) creates a PRwLock object.
	friend class FSyncObject;
	cRwLock() throw(XSynchronization) ;
	
public:
	virtual void takeForRead() throw(XSynchronization);
	virtual void takeForWrite()throw(XSynchronization);
	virtual void unlock() throw(XSynchronization);
	virtual ~cRwLock();
	
private:
    ACE_RW_Mutex    m_h;
};

cRwLock::cRwLock() throw(XSynchronization)
{
}

cRwLock::~cRwLock()
{
}

//
void cRwLock::takeForRead() throw(XSynchronization)
{
    int retCode = m_h.acquire_read();
    if( retCode != 0 )
		throw XSynchronization("Wait for Read Condition failed.");
}

//
void cRwLock::takeForWrite() throw(XSynchronization)
{
    int retCode = m_h.acquire_write();
    if( retCode != 0 )
		throw XSynchronization("Wait for Write Condition failed.");
}

//
// unlock the Reader/Writer lock
//
void cRwLock::unlock() throw(XSynchronization)
{
    int retCode = m_h.release();
    if( retCode != 0 )
		throw XSynchronization("Release Mutex failed.");

	return;
}

PCriticalSection * FSyncObject:: newCriticalSection (void) throw (XSynchronization)
{
    return new cCriticalSection ();
}

PMutex * FSyncObject::newMutex (bool initiallyOwn, const char * pName)
{
    return new cMutex (initiallyOwn ? 1 : 0, pName);
}

PRwLock * FSyncObject::newRwLock() throw(XSynchronization)
{
  return new cRwLock();
}

PSemaphore * FSyncObject::newSemaphore (int32 initialCount, int32 maxCount, const char * pName) throw(XSynchronization)
{
    return new cSemaphore (initialCount, maxCount, pName);
}

PEvent * FSyncObject::newEvent (const char * pName) throw(XSynchronization) 
{
    return new cEvent (pName);
}

__END_DC_NAME_SPACE


