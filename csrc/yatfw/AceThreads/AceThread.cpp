/*
//
// $Id: //Source/Common/thread/AceThreads/AceThread.cpp#5 $
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Filename: AceThread.cpp
//
// Creation Date: 7/6/01
//
// Last Modified:
//
// Author: Victor Bernard
//
// Description:
//   Implements dotcast synchronization primatives for ACE
//
// 
*/


# include "dctype.h"
# include "dotcastns.h"

# include "dcexception.h"
# include "thread.h"
# include "ace/ACE.h"
# include "ace/Thread.h"
# include "ace/Synch.h"
# include "ace/Thread_Manager.h"

__BEGIN_DC_NAME_SPACE

DEFINE_FILE_STRING("$Id", winthread);


#if defined(VXWORKS)
   #define THREAD_PRIORITY_IDLE             255
   #define THREAD_PRIORITY_LOWEST           230
   #define THREAD_PRIORITY_BELOW_NORMAL     210
   #define THREAD_PRIORITY_NORMAL           190
   #define THREAD_PRIORITY_ABOVE_NORMAL     170
   #define THREAD_PRIORITY_HIGHEST          150
   #define THREAD_PRIORITY_TIME_CRITICAL    130
#endif
#if defined(LINUX)
   #define THREAD_PRIORITY_IDLE             255
   #define THREAD_PRIORITY_LOWEST           230
   #define THREAD_PRIORITY_BELOW_NORMAL     210
   #define THREAD_PRIORITY_NORMAL           190
   #define THREAD_PRIORITY_ABOVE_NORMAL     170
   #define THREAD_PRIORITY_HIGHEST          150
   #define THREAD_PRIORITY_TIME_CRITICAL    130
#endif


/*
*/

CThread::CThread (uint32 priority, bool waitForStart)
    :
     m_event (FSyncObject::newEvent ()),
     m_wake (FSyncObject::newEvent ()),
     m_xThreadEntered (FSyncObject::newEvent ()),
	 m_pCallback(DefaultThreadNotificationHandler),
     m_run (ts_initing)
{
    ACE_Thread_Manager *thr_mgr = ACE_Thread_Manager::instance ();
    ACE_thread_t threadId;
    ACE_hthread_t threadHand;

    int dwPriority = ACE_DEFAULT_THREAD_PRIORITY;

    if ((int32) priority <= 0)
        {
        if (priority == INHERIT_THREAD_PRIORITY)
            {
            ACE_hthread_t mySelf;
            ACE_Thread::self(mySelf);
            ACE_Thread::getprio( mySelf, dwPriority );
            }
        }
    else
        {
        switch (priority)
            {
            case 1: dwPriority = THREAD_PRIORITY_IDLE;          break;
            case 2: dwPriority = THREAD_PRIORITY_LOWEST;        break;
            case 3: dwPriority = THREAD_PRIORITY_BELOW_NORMAL;  break;
            case 4: dwPriority = THREAD_PRIORITY_NORMAL;        break;
            case 5: dwPriority = THREAD_PRIORITY_ABOVE_NORMAL;  break;
            case 6: dwPriority = THREAD_PRIORITY_HIGHEST;       break;
            case 7: dwPriority = THREAD_PRIORITY_TIME_CRITICAL; break;
            default: dwPriority = THREAD_PRIORITY_NORMAL;       break;
            }
        }


     int 
     retCode = thr_mgr->spawn( (ACE_THR_C_FUNC) xThread, (void *) this, THR_NEW_LWP | THR_JOINABLE,
                                   &threadId, &threadHand, dwPriority );
     if (retCode < 0 )
         throw XException ("CreateThread Failed");

     m_hThread  = (OSHANDLE) threadHand;
     m_idThread = threadId;

     if (!waitForStart)
         start ();
}



CThread::~CThread()
{
	if (m_run == CThread::ts_initing) 
        {
		m_run = ts_terminated;
		m_xThreadEntered->wait();
	    }
    ACE_Thread::kill( m_idThread, SIGINT );
	delete m_event;
    delete m_wake;
    delete m_xThreadEntered;
}

bool CThread::start (void)
{
    m_event->set ();
    return true;
}

bool CThread::join(int *pReturn) 
{
  bool retFlag = false;
  if (m_run != ts_terminated) 
    {
    void * status;
    int retCode = ACE_Thread::join( m_hThread, &status );
    if( retCode == 0 )
        retFlag =  true;
    }
  if( pReturn != NULL )
      *pReturn = m_RetCode;

  return retFlag;
}

ThreadCallback CThread::setNotificationHandler (ThreadCallback handler) {
	ThreadCallback temp = m_pCallback;
	m_pCallback = ((handler == 0) ? DefaultThreadNotificationHandler : handler);
	return temp;
}

bool CThread::sleep (uint32 ms)
{
                   
    // Milli-secs divided by 1000 gives seconds
    // Milli-secs modulus by 1000 gives remaining milli-secs and multipied by 1000 gives micro-secs
    ACE_Time_Value tv( (ms/1000), 1000*(ms%1000) );

    ACE_OS::sleep( tv );

    return true;
}

bool CThread::wake (void)
{
    return m_wake->pulse ();
}


void CThread::dump (void)
{
# if 0
    TRACE ("CThread handle:%x\n", m_hThread);
    TRACE ("CThread m_run:%d\n", m_run);
# endif
}



unsigned long CThread::xThread (void * pParam)
{
    CThread * pT = (CThread *) pParam;

    pT->m_event->wait ();

	int r;
	if (pT->m_run != ts_initing) 
        {
		pT->m_xThreadEntered->set();
		return te_failure;
	    }

    pT->m_run = CThread::ts_running;

    try
    {
# if 0
        TRACE ("CThread %x entry\n", pT);
# endif
        r = pT->thread ();
# if 0
        TRACE ("CThread %x exited with %d\n", pT, r);
# endif
    }
    catch (
# if 0
            XException (&x)
# else
            ...
# endif
	      )
    {
# ifdef _DEBUG
# if 0
        TRACE ("CThread %x exited with exception: %s\n", pT, x.reason ());
# endif
# endif
    }
    pT->m_RetCode = r;
    pT->m_run = CThread::ts_terminated;
	pT->m_pCallback(pT);
    return (unsigned long) r;
}

void CThread::yield(void)
{
    sleep(1);
}

__END_DC_NAME_SPACE

