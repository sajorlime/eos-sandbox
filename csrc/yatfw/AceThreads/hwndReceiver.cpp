/*
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
*
* HWndReceiver.cpp
*
* .........................................................................
*
*
    Contains defintions for the interfaces message receiver class
    for windows message receivers.  This class is the class that
    connects a family of windows independent threads to a windows
    GUI thread.
*
* .........................................................................
*
*    Date     Description
*   ------    -----------
    06/21/99    Release
*
*
* Notes:
    This class adds sending or posting a messge to window handle to
    CMessageQueue.  
*
*
* Functions:
    CHWndReceiver::CHWndReceiver -- the constructor for the class.
    CHWndReceiver::~CHWndReceiver -- the destrucutor for the class.
    CHWndReceiver::syncNotify -- notifies the window in synchronous manor.
    CHWndReceiver::asyncNotify --  notifies the window in asynchronous manor.
    CHWndReceiver::send -- sends message to the queue and window.
*
*
*/


# include "dotcastns.h"

# include "hwndReceiver.h"

__BEGIN_DC_NAME_SPACE

DEFINE_FILE_STRING("$Id", hwndReceiver);


# ifndef WM_MSGQUE
#   define WM_MSGQUE (WM_USER + 1)
# endif

/*
    CHWndReceiver -- construct a message queue.
*/
CHWndReceiver::CHWndReceiver (HWND hWnd, bool block, int msgSize, int freeCount)
    : 
      CMessageQueue (msgSize, freeCount, 3, CMessageReceiver::blockOnSendFull),
      m_wmMsg (WM_MSGQUE),
      m_block (block),
      m_hWnd (hWnd)
{
    LRESULT r;
    r = PostMessage (m_hWnd, m_wmMsg, (WPARAM) EMT_Control, (LPARAM) this);
}

CHWndReceiver::~CHWndReceiver ()
{
    waitForUsers ();
}



bool CHWndReceiver::syncNotify (CMessage * pMsg)
{
    EMessageType id = pMsg->getId ();
    if (bool b = !CMessageQueue::send (pMsg))
    {
        return b;
    }
    LRESULT r;
    r = SendMessage (m_hWnd, m_wmMsg, (WPARAM) id, (LPARAM) this);
    if (!r)
    {
        DWORD e = GetLastError ();
        throw XException ("Send Message Failed");
    }
    return true;
}

bool CHWndReceiver::asyncNotify (CMessage * pMsg)
{
    EMessageType id = pMsg->getId ();
    /*
        If the send is asynchornous, then we need to throtle
        messages when we get too far ahead.  We only throtle
        data messages, since they are the most common, and
        we don't want to loose other messages.  There is also
        an implicit admission that we are dealing with non-critical
        data here.
    */
    if ((m_freeCount == 0) && (id == EMT_Data))
    {
        // try to wake that guy up!
        PostMessage (m_hWnd, m_wmMsg, (WPARAM) EMT_NoMessage, (LPARAM) this);
    }
    if (bool b = !CMessageQueue::send (pMsg))
    {
        return b;
    }

    LRESULT r;
    r = PostMessage (m_hWnd, m_wmMsg, (WPARAM) id, (LPARAM) this);
    if (!r)
    {
        DWORD e = GetLastError ();
        throw XException ("Post Message Failed");
    }
    return true;
}


/* from CMessageReceiver */


bool CHWndReceiver::send (CMessage * pMsg)
{
    if (m_block)
        return syncNotify (pMsg);
    return asyncNotify (pMsg);
}


__END_DC_NAME_SPACE

