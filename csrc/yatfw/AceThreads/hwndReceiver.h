/*
//
// $Id: //Source/Common/thread/AceThreads/hwndReceiver.h#4 $
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Filename: hwndReceiver.h
//
// Creation Date: 01-16-00
//
// Last Modified:
//
// Author: emil
//
// Description:
    Defines a class for talking sending messages to a Win32 Window.
   Note:
//
// 
*/


/* original header
*
    Lapel Software, Inc. 1999.
    software@lapel.com
    Developed by Lapel Software, Inc.
    Rights to copy, modify, and use are here by granted.
*
    HWndReceiver.h
*
    Defines the HWndReceiver class.  The HWndReceiver class is enables
    communication between the non-windows and windows classes.
    The class allows any thread to send messages to MSWindows window,
    without being depenent on the knowing about windows.
*
* .........................................................................
*
*      $Log:$
*
*  Date     Description
* ------   -----------
*
***************************************************************************
*
* Notes:
-
    The file does have a depenency on afxwin.h in order to define WM_USER
    to create a default value for windows message identifer.  This depency
    can be remvoved by requiring the application to define the value.
*
* Data:
    WM_MSGQUE -- defines the default value for returning a message to the
    CWindow class.
*
* Classes/Types:
    CHWndReceiver -- object that sends messages as a windows WM_COPYDATA
    messages to a MSWindows window handle.  The reciever must implement
    and register an "OnCopyData" method in the MSWindows way.  
    The data in a PMessage Class is mapped on the COPYDATASTRUCT as follows:
        dwData = m_user
        cbData = m_length
        lpData = m_pBuffer
    Likewise the send interface maps in the same way.
*
*
*/


# ifndef __HWNDRECEIVER_H__
# define __HWNDRECEIVER_H__

//# include <afxwin.h>
# include <stdarg.h>
# include <wtypes.h>
# include <native.h>
# include <winbase.h>

# include "messageQueue.h"

__BEGIN_DC_NAME_SPACE



# define WM_MSGQUE (WM_USER + 1)

/*
    A HWnd Reciever is an object that uses the Message receiver
    interface to package and send data as MSWindows messages.
    
    The purpose is to use a common interface, and to hide the
    MSWindows implementation/and dependancy from the send.
*/
class CHWndReceiver : public CMessageQueue
{
    friend class CMessageReceiverFactory;
private:
    HWND m_hWnd;
    UINT m_wmMsg;
    bool m_block;

protected:
    // class extension
    void overrideWMMsg (UINT wmMsg) {m_wmMsg = wmMsg;}

    /*
        allows inheriting class to override actual send method.
        The HWndReceiver class sends the message queue and
        using SendMessage.
    */
    virtual bool syncNotify (CMessage * pMsg);
    /*
        allows inheriting class to override actual post method.
        The HWndReceiver class sends the message queue and
        using PostMessage.
    */
    virtual bool asyncNotify (CMessage * pMsg);

public:
    // from CMessageReceiver
    virtual bool send (CMessage * pMsg);

    CHWndReceiver (HWND hWnd,
                   bool block = false,
                   int msgSize = defMsgSize,
                   int freeCount = defFreeCount
                  );
    // CHWndReceiver extentions
    virtual ~CHWndReceiver ();

    virtual bool waitForEmpty (void)
    {
        return CMessageQueue::waitForEmpty ();
    }
};

__END_DC_NAME_SPACE

# endif