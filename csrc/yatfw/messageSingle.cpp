/*
//
// $Id$
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Filename: messageSingle.cpp
//
// Creation Date: 02-02-00
//
// Last Modified:
//
// Author: emil
//
// Description:
    Implementation for a message receiver that handles exactly one message.
   Note:
//
// 
*/


# include "dctype.h"
# include "dotcastns.h"

# include "dcsynchronization.h"
# include "messageSingle.h"

__BEGIN_DC_NAME_SPACE

DEFINE_FILE_STRING("$Id", messageSingle);


void CMessageSingle::release (CMessage * pMsg)
{
    if (m_pAvailMessage)
    {
        throw (XSynchronization ("Releasing: message while its available"));
    }
    if (pMsg != m_pTheMessage)
    {
        throw (XSynchronization ("Not releasing our message"));
    }
    m_pAvailMessage = pMsg;
}



/*
    CMessageSingle -- construct a message queue.
*/
CMessageSingle::CMessageSingle (int msgSize, int blockFlags)
    : 
      CMessageReceiver (msgSize, 1, blockFlags),
      m_pTheMessage (makeMessage ()),
      m_pAvailMessage (m_pTheMessage),
      m_pMessageEvent (FSyncObject::newEvent ()),
      m_pAccess (FSyncObject::newCriticalSection ())
{
}



CMessageSingle::~CMessageSingle ()
{
	delete m_pMessageEvent;
	delete m_pAccess;
	delete m_pTheMessage;
}


CMessage * CMessageSingle::allocMessage (void)
{
    CMessage * pMsg;

    if ((pMsg = m_pAvailMessage))
    {
        m_pAvailMessage = 0;
    }
    return pMsg;
}


CMessage * CMessageSingle::receive (void)
{
    m_pMessageEvent->wait ();
    return m_pTheMessage;
}

/* from CMessageReceiver */

inline bool CMessageSingle::send (CMessage * pMsg)
{
    m_pMessageEvent->set ();
    return true;
}


void CMessageSingle::failed (const XException & reason)
{
    if (m_pExcept)
        throw XException ("second call to CMessageSingle::failed");
    m_pExcept = new XException (reason);
}

__END_DC_NAME_SPACE

