/*
//
// $Id$
//
// Copyright (c) 2000 Dotcast Inc.
//
// Description:  Test for rwlock
//
//
*/

//
// This test is rather subtle in that it relies on a tricky form
// of synchronization to make sure that a writer is the initial
// owner of the lock.
//
// The volatile variable 'flag' is basically used as a barrier synchronizer
// to guarantee that no thread other than the initial writer thread
// gets to the lock before the initial writer thread.  Once that thread
// has the lock, it works long enough so that it will be quantum scheduled
// and the other threads have a chance of running. THIS WILL ONLY WORK IF
// THE UNDERLYING SCHEDULER WILL SWITCH A RUNNING THREAD OUT.  This may
// not work on all uniprocessor tread schedulers.
//
// NOTE: output through cout is not thread safe, but this test is
//       relying on certain bits of magic so that it will "mostly"
//       work.  All of the cout i/o should be redirected to a
//       thread safe i/o facility.
//

#ifdef _WIN32
#pragma warning (disable : 4786)
#endif //_WIN32

#include "dcsynchronization.h"
#include "dcexception.h"
#include "thread.h"
#include "Tracer.h"

#include <iostream>

#ifndef NWRITERS
#define NWRITERS 3
#endif /* NWRITERS */


#ifndef NREADERS
#define NREADERS 4
#endif /* NREADERS */

static int c_writers = NWRITERS;
static int c_readers = NREADERS;

volatile int flag = 0;

//
// Hack to "do work" for 1 second
//
#include <time.h>
void dowork(bool writer, int id)
{
  clock_t now = clock();
  clock_t done = now + CLOCKS_PER_SEC;

  if (writer)
    std::cout << "writer ";
  else
    std::cout << "reader ";

  std::cout << id;

  std::cout << " working for 1 second.\n";
  while (done > clock())
    ;

  if (writer)
    std::cout << "writer ";
  else
    std::cout << "reader ";

  std::cout << id;
  std::cout << " done\n";
}

class writerThread : public dotcast::CThread
{
public:
  writerThread(int id, dotcast::PRwLock *lock) : m_id(id), m_pLock(lock) {};
private:
  int thread();
  int m_id;
  dotcast::PRwLock *m_pLock;
};

class readerThread : public dotcast::CThread
{
public:
  readerThread(int id, dotcast::PRwLock * lock) : m_id(id), m_pLock(lock) {};
private:
  int thread();
  int m_id;
  dotcast::PRwLock *m_pLock;
};

int writerThread::thread()
{
  if (m_id == 0) {
    std::cout << "writer 0 phase 1\n";
    m_pLock->takeForWrite();
    std::cout << "writer 0 owns the lock \n";
    flag = 1;
    dowork(true,m_id);
    m_pLock->unlock();
    std::cout << "writer 0 freed the lock\n";
  } else {
    while (flag < 2)
      ;
    m_pLock->takeForWrite();
    std::cout << "writer " << m_id << " owns the lock\n";
    dowork(true,m_id);
    m_pLock->unlock();
  }
  return m_id;
}

int readerThread::thread()
{
  while (flag == 0)
    ;
  std::cout << "reader " << m_id << " trying for lock\n";
  if (m_id == 0)
    flag = 2;
  m_pLock->takeForRead();
  std::cout << "reader " << m_id << " shares the lock\n";
  dowork(false,m_id);
  m_pLock->unlock();

  return m_id;
}

typedef writerThread * writer_t;
typedef readerThread * reader_t;

//
// Main is, for the most part, a no op.  It starts the threads
// and then waits for them to die.
//

int main(int argc, char *argv[]) {

  dotcast::CTracer::setTraceLevel("cthread",
				 dotcast::CTracer::et_info);

  dotcast::CTracer::setTraceLevel("sync",
				 dotcast::CTracer::et_verbose);

  writer_t *pWriter = new writer_t[c_writers];
  reader_t *pReader = new reader_t[c_readers];

  dotcast::PRwLock * lock = dotcast::FSyncObject::newRwLock();

  for (int i = 0; i < c_writers; i++)
    pWriter[i] = new writerThread(i,lock);
  for (int i = 0; i < c_readers; i++)
    pReader[i] = new readerThread(i,lock);

  for (int i = 0; i < c_writers; i++)
    pWriter[i]->start();
  for (int i = 0; i < c_readers; i++)
    pReader[i]->start();

  for (int i = 0; i < c_writers; i++) {
    int j;
    pWriter[i]->join(&j);
    std::cout << "Writer(" << i << ") returns " << j << std::endl;
  }

  for (int i = 0; i < c_readers; i++) {
    int j;
    pReader[i]->join(&j);
    std::cout << "Reader(" << i << ") returns " << j << std::endl;
  }

  std::cout << "Done\n";
}
