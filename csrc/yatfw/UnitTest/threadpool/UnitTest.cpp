/*
//
// $Id:$
//
// Copyright (c) 2000 Dotcast Inc.
//
// Description:  Test for threadpool
//
//
*/

#ifdef _WIN32
#pragma warning (disable : 4786)
#endif //_WIN32

#include "dcsynchronization.h"
#include "dcexception.h"
#include "ThreadPool.h"
#include "thread.h"

#include <iostream>

__USING_DC_NAME_SPACE;

static PMutex* TheMutex;
static PEvent* TheEvent;

class Worker : public PWorkerThreadDelegate
{
public:
	Worker () {
		num = ++ThreadCount;
		std::cout << "Worker thread " << num << " created" << std::endl;
		m_pData = 0;
	}

	virtual ~Worker () {
		TheMutex->take();
		std::cout << "Worker " << num << " being deleted " << std::endl;
		if ((--ThreadCount) == 0) {
			TheEvent->set();
		}
		TheMutex->release();
	}

	virtual void setData (void* data) {
		m_pData = static_cast<char*>(data);	
	}

	virtual void work () {
		TheMutex->take();
		std::cout << "Work is printing this: " << m_pData << std::endl;
		TheMutex->release();
	}

private:
	char* m_pData;
	int num;
	static int ThreadCount;
};

int Worker::ThreadCount = 0;

#define WORK_SIZE 6

char* WorkStrings [WORK_SIZE] = 
{
	"Hello there",
	"Boy oh boy",
	"Winner takes all",
	"Eclectic but tight",
	"Once upon a time",
	"That's all folks"
};

int main (int argc, char** argv) {
	std::cout << "Starting ThreadPool Unit Test" << std::endl;
	TheMutex = FSyncObject::newMutex(false, 0);
	TheEvent = FSyncObject::newEvent(0);
	TheEvent->reset(); // Initially off

	std::cout << "Creating Thread Pool" << std::endl;
	PThreadPool* pool = new CThreadPool<Worker>(5);
	
	for (int i = 0; i < 10; i++) {
		TheMutex->take();
		std::cout << "Doing some work" << std::endl;
		TheMutex->release();
		for (int j = 0; j < WORK_SIZE; j++) {
			pool->requestWork(WorkStrings[j]);
		}
	}
	
	TheMutex->take();
	std::cout << "Deleting Thread Pool" << std::endl;
	TheMutex->release();
	delete pool;
	
	TheEvent->wait(); // Wait until all Workers exit
	std::cout << "Finished ThreadPool Unit Test" << std::endl;
	std::cout << std::endl << "Press enter to destroy the universe." << std::endl;
	std::cout << "(Some versions may behave slightly differently): ";
	(void)std::cin.get();

	return 0;
}

