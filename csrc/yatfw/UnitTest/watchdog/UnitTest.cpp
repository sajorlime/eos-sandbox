/*
//
// $Id$
//
// Copyright (c) 2000 Dotcast Inc.
//
// Description:  Test for watchdog
//
//
*/

#ifdef _WIN32
#pragma warning (disable : 4786)
#endif //_WIN32

#include "dcsynchronization.h"
#include "dcexception.h"
#include "thread.h"
#include "Tracer.h"

#include <iostream>

#ifndef NWORKERTHREADS
#define NWORKERTHREADS 3
#endif //  NWORKERTHREADS

const int c_workers = NWORKERTHREADS;

#ifndef NPASSES
#define NPASSES 2
#endif /* NPASSES */

const unsigned int c_passes = NPASSES;

#ifndef MAXTICKS
#define MAXTICKS 3
#endif /* MAXTICKS */

const int c_lotsOfTicks = MAXTICKS;

//
// Hack to "do work" for n seconds
//
#include <time.h>
void dowork(int n)
{
  clock_t now = clock();
  clock_t done = now + (CLOCKS_PER_SEC * n);

  while (done > clock())
    ;

}

class workerThread : public dotcast::CThread
{
public:
  workerThread() : m_done(false) {};
  bool isDone() const { return m_done; }
private:
  bool m_done;
  int thread() {
    std::cout << "thread " << id() << " starting." << std::endl;
    while (status() < c_passes) {
      keepalive();
      dowork(2*id()+1);
    }
    std::cout << "thread " << id() << " ending." << std::endl;
    m_done = true;
    return id();
  }
};

class watchdogThread : public dotcast::CThread
{
public:
  watchdogThread() {};
private:
  int thread();
};

static dotcast::CThread * const * pThreads;
static dotcast::CThread *savedThreads[NTHREADS];
static int savedId[NTHREADS];
static unsigned int savedStatus[NTHREADS];
static int savedTicks[NTHREADS];

void printThread(int i)
{
  std::cout << "Thread " << i << " id = ";
  if (pThreads[i]) {
    std::cout  << pThreads[i]->id()
	  << " status " << pThreads[i]->status();
  } else {
    std::cout << "??? status ???";
  }
  std::cout << " saved ID " << savedId[i]
       << " saved status " << savedStatus[i]
       << " saved Ticks  " << savedTicks[i]
       << std::endl;
}

void printThreads()
{
  for (int i = 0; i < NTHREADS; i++)
    if (pThreads[i])
      printThread(i);
}

int watchdogThread::thread()
{
  //
  // Initialize the snapshot copies
  //
  pThreads =
    dotcast::CThreadList::s_ThreadList.threads();
  for (int i = 0; i < NTHREADS; i++) {
    savedTicks[i] = 0;
    savedThreads[i] = pThreads[i];
    if (pThreads[i]) {
      savedId[i] = pThreads[i]->id();
      savedStatus[i] = pThreads[i]->status();
    } else {
      savedId[i] = -1;
      savedStatus[i] = 0;
    }
  }
  printThreads();
  // infinite loop watching threads -- needs a sleep
  while (true) {
    dowork(1);
    std::cout << std::endl;
    printThreads();
    for (int i = 0; i < NTHREADS; i++) {
      // this position has been reused for a different thread
      // *or* the thread has died.
      // (This should be very rare in the dotbox)
      //
      if (savedThreads[i] != pThreads[i]) {
	std::cout << "Thread " << i << " State change:  ";
	printThread(i);
	savedThreads[i] = pThreads[i];
	savedTicks[i] = 0;
	if (pThreads[i]) {
	  savedId[i] = pThreads[i]->id();
	  savedStatus[i] = pThreads[i]->status();
	} else {
	  savedId[i] = -1;
	  savedStatus[i] = 0;
	}
      } else {
	//
	// this position still refers to the same thread ID
	// or is empty
	//
	if (pThreads[i]) {
	  //
	  // same thread id as last time, but has that
	  // id been reused?  (should be very rare)
	  //
	  if (savedId[i] != pThreads[i]->id()) {
	    std::cout << " Thread " << i << " State change:  ";
	    printThread(i);
	    savedTicks[i] = 0;
	    savedId[i] = pThreads[i]->id();
	    savedStatus[i] = pThreads[i]->status();
	  } else {
	    //
	    // same thread as last time and same object as last time
	    //
	    if (savedStatus[i] == pThreads[i]->status()) {
	      //
	      // No state change
	      //
	      savedTicks[i]++;
	      if (savedTicks[i] > c_lotsOfTicks) {
		std::cout << " Thread " << i << " may be dormant.\n";
		printThread(i);
		savedTicks[i] = 0;
	      }
	    } else {
	      //
	      // state change
	      //
	      savedTicks[i] = 0;
	    }
	  }
	}
      }
    }
  }
}

//
// Main is, for the most part, a no op.  It starts the threads
// and then waits for them to die.
//

typedef workerThread * worker_t;

int main(int argc, char *argv[]) {

  worker_t *pWorker = new worker_t[c_workers];

  for (int i = 0; i < c_workers; i++)
    pWorker[i] = new workerThread;

  for (int i = 0; i < c_workers; i++)
    pWorker[i]->start();


  watchdogThread *pWatchdog = new watchdogThread;
  pWatchdog->start();

  while (true) {
    dowork(2*c_workers+1);
    for (int i = 0; i < c_workers; i++) {
      if (pWorker[i]->isDone())
	delete pWorker[i];
    }
  }

  std::cout << "Done\n";
}
