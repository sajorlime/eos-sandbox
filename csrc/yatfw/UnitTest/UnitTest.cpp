// UnitTest.cpp : Test Thread synchronization classes.
//

#define TRY
#ifdef _WIN32
#pragma warning (disable : 4786)
#if(_WIN32_WINNT < 0x0400)
#undef TRY
#endif //_WIN32_WINNT >= 0x0400
#endif //_WIN32

#define TRY_ENTER_CRITICAL_SECTION

#include "dotcastns.h"
#include "dcsynchronization.h"
#include "dcexception.h"
#include "thread.h"
#include "../../libutility/Tracer.h"

#include <iostream>
#include <strstream>
#include <deque>
#include <string>

using std::cin;
using std::cout;
using std::deque;
using std::endl;
using std::string;

__USING_DC_NAME_SPACE;

static const char *ThreadTrace = "cthread";

PEvent* TheEvent;
bool HandlerCalled;

typedef int (*TPROC)(void* data);

PCriticalSection* outCs = FSyncObject::newCriticalSection();

bool DeletingNotificationHandler (CThread* thread) {
	outCs->take();
	cout << "\t*** Deleting Notification Handler called." << endl;
	outCs->release();
	delete thread;
	HandlerCalled = true;
	TheEvent->set();
	return true;
}

bool NonDeletingNotificationHandler (CThread* thread) {
	outCs->take();
	cout << "\t--- NonDeleting Notification Handler called." << endl;
	outCs->release();
	HandlerCalled = true;
	TheEvent->set();
	return false;
}


class ProcThread : public CThread
{
public:
	ProcThread (TPROC proc, void* data)
		: CThread(NORMAL_THREAD_PRIORITY, true), m_proc(proc), m_data(data) {}
	virtual ~ProcThread () { 
		outCs->take();
		cout << "\tProcThread destructor called" << endl; 
		outCs->release();
	}
protected:
	int thread () {
		return m_proc(m_data);
	}
private:
	TPROC m_proc;
	void* m_data;
};

class MessageQueue
{
public:
	MessageQueue () {
		sema = FSyncObject::newSemaphore(0, 100, 0);
		mutex = FSyncObject::newMutex(false, 0);
	}

	virtual ~MessageQueue () {
		if (sema != 0) delete sema;
		if (mutex != 0) delete mutex;
	}

	const string nextEvent () throw (XSynchronization) {
		bool rc;
		bool haveEntry = false;
		string msg;
		while (true) {
			rc = sema->take();
			if (rc == false) {
				return 0;
			}
			rc = mutex->take();
			if (rc == false) {
				return 0;
			}
			if (msgs.empty() == false) {
				msg = msgs.front();
				msgs.pop_front();
				haveEntry = true;
			}
			mutex->release();
			if (haveEntry == true) {
				return msg;
			}
		}
	}

	bool enqueue (const string msg) {
		bool rc = mutex->take();
		if (rc == false) {
			return false;
		}
		msgs.push_back(msg);
		sema->release();
		mutex->release();
		return true;
	}

private:
	PSemaphore* sema;
	PMutex* mutex;
	deque<string> msgs;
};

bool foo_succeed;

int Foo (void* data) {
	foo_succeed = false;
	MessageQueue* queue = (MessageQueue*)data;
	outCs->take();
	cout << "\tEvent Queue Thread starting" << endl;
	outCs->release();
	while (true) {
		string msg;
		try {
			msg = queue->nextEvent();
		}  catch(XException& x) {
			outCs->take();
			cout << "\tException caught in Foo: " << x.reason() << endl;
			outCs->release();

			return 0;
		}
		if (msg == "") {
			outCs->take();
			cout << "\tReceived end message" << endl;
			outCs->release();
			break;
		}
		else {
			outCs->take();
			cout << "\tReceived message: " << msg << endl;
			outCs->release();
		}
	}
	foo_succeed = true;
	delete queue;
	return 0;
}

int Fun (void* data) {
	outCs->take();
	cout << "\tFun Thread started with arg \"" << static_cast<char*>(data) << "\"" << endl;
	outCs->release();
	return 1234;
}

int TryMutex (void* data) {
	int result = 0;
	PMutex* mutex = (PMutex*)data;
	try {
		result = mutex->tryToTake();
	} catch (XException& x) {
		cout << "Exception caught in mutexTryToTake: " << x.reason() << endl;
		return -1;
	}
	if (result) {
		cout << "\tTook mutex" << endl;
		mutex->release();
	} else {
		cout << "\tCan't take mutex" << endl;
	}
	return result;
}

#ifdef TRY
bool csTryToTakeTest () {
	bool rc;
	PCriticalSection* pcs = FSyncObject::newCriticalSection();
 	cout << "\tUsing csTryToTake to try and enter a critical section...";
	try {
		rc = pcs->tryToTake();
	}
	catch(XException& x) {
		cout << "Exception caught in csTryToTake: " << x.reason() << endl;
		rc = false;
	}
	if (rc == true) {
		pcs->release();
		cout << "Succeeded" << endl;
		delete pcs;
		return true;
	} else {
		cout << "Failed" << endl;
		delete pcs;
		return false;
	}
}
#endif //TRY

bool csTakeTest() {
	bool rc;
	PCriticalSection* pcs = FSyncObject::newCriticalSection();
	cout << "\tEntering critical section...";
	rc = pcs->take();
	if (rc == true) {
		pcs->release();
		cout << "Succeeded" << endl;
		delete pcs;
		return true;
	}
	else {
		cout << "Failed" << endl;
		delete pcs;
		return false;
	}
}

#ifdef TRY
bool csRecursiveTakeTest(bool recursive) {
	bool rc;
	try {
		PCriticalSection* pcs = FSyncObject::newCriticalSection();
		cout << "\tEntering critical section...";
		rc = pcs->take();
		if (rc == true) {
			cout << "Succeeded" << endl;
		} else {
			cout << "Failed" << endl;
			return false;
		}
		cout << "\tTrying to Renter critical section...";
		rc = pcs->tryToTake();
		if (rc == true) {
			pcs->release();
			cout << "Succeeded" << endl;
		} else {
			cout << "Failed" << endl;
		}
		pcs->release();
		delete pcs;
	} catch(XException& x) {
		cout << "Exception caught in csRecursiveTakeTest: " << x.reason() << endl;
		rc = false;
		return false;
	}
	return (recursive == rc);
}
#endif //TRY

bool mutexTakeTest() {
	bool rc;
	PMutex* pcs = FSyncObject::newMutex(false, "Test Mutex");
	cout << "\tTaking mutex...";
	rc = pcs->take();
	if (rc == true) {
		pcs->release();
		cout << "Succeeded" << endl;
		delete pcs;
		return true;
	}
	else {
		cout << "Failed" << endl;
		delete pcs;
		return false;
	}
}

bool mutexTryToTakeTest () {
	bool rc;
	PMutex* pcs = FSyncObject::newMutex(false, "Test Mutex");
 	cout << "\tUsing csTryToTake to try and enter a critical section...";
	try {
		rc = pcs->tryToTake();
	}
	catch(XException& x) {
		cout << "Exception caught in mutexTryToTake: " << x.reason() << endl;
		rc = false;
	}
	if (rc == true) {
		pcs->release();
		cout << "Succeeded" << endl;
		delete pcs;
		return true;
	} else {
		cout << "Failed" << endl;
		delete pcs;
		return false;
	}
}

bool mutexRecursiveTakeTest(bool recursive) {
	bool rc;
	try {
		PMutex* pcs = FSyncObject::newMutex(false, "Test Mutex");
		cout << "\tTaking mutex...";
		rc = pcs->take();
		if (rc == true) {
			cout << "Succeeded" << endl;
		} else {
			cout << "Failed" << endl;
			return false;
		}
		cout << "\tRecursively taking mutex...";
		rc = pcs->tryToTake();
		if (rc == true) {
			pcs->release();
			cout << "Succeeded" << endl;
		} else {
			cout << "Failed" << endl;
		}
		pcs->release();
		delete pcs;
	} catch(XException& x) {
		cout << "Exception caught in mutexRecursiveTakeTest: " << x.reason() << endl;
		rc = false;
		return false;
	}
	return (recursive == rc);
}
	
bool readWriteLockTest() {
	try {
		PRwLock* rwlock = FSyncObject::newRwLock();
		rwlock->takeForRead();
		rwlock->unlock();
		rwlock->takeForWrite();
		rwlock->unlock();
		delete rwlock;
	} catch (XException& x) {
		cout << "Exception caught in readWRiteLockTest: " << x.reason() << endl;
		return false;
	}
	return true;
}

bool basicThreadMessageEventTest() {
	try {
		TheEvent = FSyncObject::newEvent();
		MessageQueue* queue = new MessageQueue();
		ProcThread pt(Foo, queue);
		pt.setNotificationHandler(NonDeletingNotificationHandler);
		pt.start();
		outCs->take();
		cout << "\tTrying to enqueue a message" << endl;
		outCs->release();
		queue->enqueue("Testing");
		outCs->take();
		cout << "\tTrying to enqueue end message" << endl;
		outCs->release();
		queue->enqueue("");
		outCs->take();
		cout << "\tWaiting for thread to end" << endl;
		outCs->release();
		TheEvent->wait();
		delete TheEvent;
	} catch (XException& x) {
		outCs->take();
		cout << "Exception caught in basicThreadMessageEventTest: " << x.reason() << endl;
		outCs->release();
		return false;
	}
	return foo_succeed;
}

bool threadNotificationTest() {
	HandlerCalled = false;
	ProcThread* ft;
	try {
		TheEvent = FSyncObject::newEvent();
		ft = new ProcThread(Fun, (void*)"this is the notification thread");
		ft->setNotificationHandler(DeletingNotificationHandler);
		cout << "\tStarted Notification Thread" << endl;
		ft->start();
		TheEvent->wait();
		delete TheEvent;
	} catch (XException& x) {
		cout << "Exception caught in threadNotificationTest: " << x.reason() << endl;
		return false;
	}
	return HandlerCalled;
}

bool threadJoinTest() {
	ProcThread* ft;
	try {
		int funReturn = -9999;
		ft = new ProcThread(Fun, (void*)"this is the join thread");
		cout << "\tStarted Join Thread..." <<endl;
		ft->start();
		ft->join(&funReturn);
		delete ft;
	} catch (XException& x) {
		outCs->take();
		cout << "\tException caught in threadNotificationTest: " << x.reason() << endl;
		outCs->release();
		return false;
	}
	cout << "\tThread done." << endl;
	return true;
}

bool mutexTryToTakeBlockTest () {
	bool rc;
	ProcThread* ft;
	PMutex* pcs = FSyncObject::newMutex(false, "Test Mutex");
 	cout << "\tTesting mutex exclusion..." << endl;
	try {
		rc = pcs->tryToTake();
		if (!rc)  return false;
		int funReturn = -9999;
		ft = new ProcThread(TryMutex, (void*)pcs);
		ft->start();
		ft->join(&funReturn);
		pcs->release();
		delete ft;
		if (funReturn < 0) return false;
		return (funReturn==0);
	} catch(XException& x) {
		cout << "Exception caught in mutexTryToTake: " << x.reason() << endl;
		return false;
	}
}

int main(int argc, char* argv[])
{
	bool result;
	bool finalResult=true;

	//	CTracer::setOutputStdout();
	//	CTracer::setTraceLevel(ThreadTrace, CTracer::et_debug);

	outCs->take();
	cout << "Thread synchronization Unit Test starting..." << endl;
	outCs->release();

	
#ifdef TRY
	cout << endl << "Running cCriticalSection tryToTakeTest..." << endl;
	result = csTryToTakeTest();
	finalResult = finalResult & result;
	cout << "result: " << (result ? "Success":"Fail") << endl;
#endif

	cout << endl << "Running cCriticalSection takeTest..." << endl;
	result = csTakeTest();
	finalResult = finalResult & result;
	cout << "result: " << (result ? "Success":"Fail") << endl;

#ifdef TRY
	cout << endl << "Running cCriticalSection recursiveTakeTest..." << endl;
	result = csRecursiveTakeTest(true);
	finalResult = finalResult & result;
	cout << "result: " << (result ? "Success":"Fail") << endl;
#endif

	cout << endl << "Running cMutex takeTest..." << endl;
	result = mutexTakeTest();
	finalResult = finalResult & result;
	cout << "result: " << (result ? "Success":"Fail") << endl;

	cout << endl << "Running cMutex recursiveTakeTest..." << endl;
	result = mutexRecursiveTakeTest(true);
	finalResult = finalResult & result;
	cout << "result: " << (result ? "Success":"Fail") << endl;

	cout << endl << "Running cMutex exclusion Test..." << endl;
	result = mutexTryToTakeBlockTest();
	finalResult = finalResult & result;
	cout << "result: " << (result ? "Success":"Fail") << endl;

	cout << endl << "Running read/write lock test..." << endl;
	result = readWriteLockTest();
	finalResult = finalResult & result;
	cout << "result: " << (result ? "Success":"Fail") << endl;

	cout << endl << "Running basic Thread/Message/Event test..." << endl;
	result = basicThreadMessageEventTest();
	finalResult = finalResult & result;
	cout << "result: " << (result ? "Success":"Fail") << endl;

	cout << endl << "Running thread joining tests..." << endl;
	result = threadJoinTest();
	finalResult = finalResult & result;
	cout << "result: " << (result ? "Success":"Fail") << endl;

	cout << endl << "Running thread notification tests..." << endl;
	result = threadNotificationTest();
	finalResult = finalResult & result;
	cout << "result: " << (result ? "Success":"Fail") << endl;

	cout << endl << "Thread synchronization Unit Test complete." << endl;
	cout << "Final Result: ";
	if (finalResult) {
		cout << "PASS! PASS! PASS!" << endl;
	} else {
		cout << "FAIL! FAIL! FAIL!" << endl;
	}
	cout << endl << "Press enter to destroy the universe." << endl;
	cout << "(Some versions may behave slightly differently): ";
	(void)cin.get();
	return 0;
}

