#include <iostream>

#include <cstdint>

// Constants
const int ACCELEROMETER_BITS = 8; // Number of bits in the accelerometer output
const float G = 9.80665f; // Acceleration due to gravity (m/s^2)
const float SR = 4.0;
const float FR = 2 * SR;
const int MAXV = (1 << (ACCELEROMETER_BITS - 1)) - 1;
//const float LSB = FR / ((2 * MAXV) + 1);
const float LSB = FR / (1 << ACCELEROMETER_BITS);


// Function to calculate acceleration
float calculate_acceleration(int16_t iv, float lsb) {
    return iv * LSB * G;
}



int main() {

    int16_t inval = (int16_t) (G / (G * LSB));
    std::cout << "inval:" << inval << std::endl;
    std::cout << "MAXV:" << MAXV << std::endl;
    std::cout << "SR:" << SR << std::endl;
    std::cout << "FR:" << FR << std::endl;
    std::cout << "LSB:" << LSB << std::endl;
    std::cout << "OV:" << inval << std::endl;
    //int16_t output_value = 32;
    float accel = calculate_acceleration(inval, LSB);

    std::cout << "Accel for output value " << inval << ": " << accel<< " m/s^2" << std::endl;

    return 0;
}
