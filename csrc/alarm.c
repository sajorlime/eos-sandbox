#include <signal.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>

volatile sig_atomic_t print_flag = false;

typedef struct sometype SomeType;
struct sometype
{
    int a;
    int b;
};

SomeType x;

void handle_alarm( int sig ) {
    print_flag = true;
}

int main() {
    x.a = 1;
    x.b = 2;
    printf("x:%d %d\n", x.a, x.b);
    signal( SIGALRM, handle_alarm ); // Install handler first,
    alarm( 1 ); // before scheduling it to be called.

    for (;;) {
        if ( print_flag ) {
            printf( "Hello\n" );
            print_flag = false;
            alarm( 1 );
        }
    }
}

