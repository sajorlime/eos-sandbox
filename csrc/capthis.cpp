

#include <functional>
#include <string>
#include <iostream>

struct A {
    std::function<std::string ()> f;
};

struct B {
    std::string name;

    B(std::string n, A& a)
        : name(n)
    {
        std::function<std::string ()> f = [this]()->std::string {return this->name;};
        a.f = f;
    }
};

int main()
{
    A a;

    B b("this is b", a);
    std::cout << a.f() << std::endl;
}

