
/*
    down_converstion.cpp -- calculate ssg freq config values.
    Emilio Rojas
*/

# include <iostream>
# include <iomanip>
//# include <math.h>
# include <sstream>
# include <iostream>
# include <string>
# include <cstdlib>

using namespace std;


constexpr double _FS = 132.5e6;
constexpr uint32_t _freq_width = 24; // bits
constexpr uint32_t _freq_scale = 1 << _freq_width;
constexpr double GRef = 1191.795E6; // Galileo frequency reference

uint32_t config_down_conversion(double lo)
{
    //cout << "((lo - GRef) / (_FS / 2 )):" << ((lo - GRef) / (_FS / 2 )) << endl;
    //cout << "(((lo - GRef) / (_FS / 2 )) * _freq_scale):" << (((lo - GRef) / (_FS / 2 )) * _freq_scale) << endl;
    //cout << "round(((lo - GRef) / (_FS / 2 )) * _freq_scale) % _freq_scale:"
    //     << ((uint32_t) (((lo - GRef) / (_FS / 2 )) * _freq_scale)) % _freq_scale
    //     << endl;
    auto round = [](double f) { return (uint32_t) (f + 0.5); };
    return round(((lo - GRef) / (_FS / 2 )) * _freq_scale) % _freq_scale;
}


int main(int argc, char** argv)
{
    cout << "Freq    down conv" << endl;
    for (int i = 1; i < argc; i++) {
        double freq = stod(argv[i]);
        //cin >> freq;
        uint32_t cdc = config_down_conversion(freq);
        cout << freq << " : 0x" << hex << setw(24/4) << setfill('0')
             << cdc
             << endl;
    }
}

