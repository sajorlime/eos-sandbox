/* 
// 
// $Id$ 
// 
// Copyright (c) 1999-2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: serverSocket.h
// 
// Creation Date: 11/11/99
// 
// Last Modified: 
    11/19/99 emil Converted to names space, names changed, etc.
// 
// Author: Prakash Manghnani
// 
// Description: 
// This is a server socket. It has methods to wait for incoming client connecctions.
// On successfull client connection, it creates a new CTcpSocket object, that represents 
// a connected socket.
// 
//  
*/ 
// serverSocket.h: interface for the CServerSocket class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __SERVERSOCKET_H__
#define __SERVERSOCKET_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "tcpSocket.h"

__BEGIN_DC_NAME_SPACE

class CServerSocket  
{
protected:
	CTcpSocket m_listenerSocket;

public:
	CServerSocket();
	virtual ~CServerSocket();

	CTcpSocket *accept(int portNo);
};

__END_DC_NAME_SPACE

#endif

