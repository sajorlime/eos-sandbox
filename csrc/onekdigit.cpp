
/*
    explore digits
*/

# include<cassert>
# include<chrono>
# include<cstdio>
# include <iomanip>
# include<iostream>
# include<thread>
# include<string>
# include<sstream>

using namespace std;

string digits(uint64_t x)
{
    auto y = x / 1000;
    if (y == 0) {
        stringstream sout;
        sout << setfill('0');
        sout << setw(3);
        sout << x;
        return sout.str();
    }
    x = x - 1000 * y;
    return digits(y) + ',' + digits(x);
}

string print_digits(const char* name, uint64_t v)
{
    string s = digits(v);
    if (s[0] == '0') {
        if (s[1] == '0') {
            return string(name) + ':' + digits(v).substr(2);
        }
        return string(name) + ':' + digits(v).substr(1);
    }
    return string(name) + ':' + digits(v);
}

FILE* _fp;

bool mount_sdcard()
{
    return true;
}

constexpr size_t num_samps_per_msec = 20480;
# define NUMBER_OF_SAMPLES_IN_A_MSEC num_samps_per_msec

# ifndef READ_POWER
    // default to 1 giga byte
#   define READ_POWER 30
# endif
constexpr uint32_t max_read_space = 1 << READ_POWER;

constexpr uint32_t access_rows = 640;
constexpr uint32_t access_uint = 8; // 8 bytes or 64-bits.
constexpr uint32_t access_elements = 8;
constexpr uint32_t access_row_size = access_elements * access_uint;
constexpr uint32_t data_elements = 5;
constexpr uint32_t data_row_size = data_elements * access_uint;
constexpr uint32_t address_space = access_rows * access_row_size;
constexpr uint32_t data_space = access_rows * data_row_size;

static_assert(num_samps_per_msec == 4 * data_space / 5);

// The data-space is times 2 for A and B samples.
constexpr uint32_t msec_row_step = 2 * num_samps_per_msec;


constexpr uint32_t one_sec_data = 1000 * msec_row_step;
# ifndef SECONDS_TO_READ
#   define SECONDS_TO_READ 1
# endif
constexpr uint32_t seconds_to_read = SECONDS_TO_READ;
constexpr uint32_t milliseconds_to_sleep = (1000 * SECONDS_TO_READ) / 4;
constexpr uint32_t data_to_read = seconds_to_read * one_sec_data;
// We only want multiples of 1 millisecond of data
constexpr uint32_t max_read_millisecs = max_read_space / seconds_to_read;
constexpr uint32_t available_read_space = max_read_millisecs * seconds_to_read;

int main(int argc, char** argv)
{
    cout << print_digits("milliseconds_to_sleep", milliseconds_to_sleep) << endl;
    cout << print_digits("available_read_space", available_read_space) << endl;
    cout << print_digits("max_read_millisecs", max_read_millisecs) << endl;
    cout << print_digits("data_to_read", data_to_read) << endl;
    for (int i = 1; i < argc; i++) {
        auto k = atoi(argv[i]);
        cout << print_digits(argv[i], k) << endl;
    }
}


