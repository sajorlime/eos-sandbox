
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <string.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/ioctl.h>


int main( int argc, char **argv )
{

    pid_t pid;
    if((pid =fork())<0) return -1;
    else if(pid!=0)
        exit(0);
    
    close(0);
    close(1);
    close(2);
    
    setsid();
//    signal(SIGHUP, SIG_IGN);

   
    int     handle;
    struct  termios  oldtio,newtio;
    
    char    *TitleMessage = "Welcome Serial Port\n";
    char    Buff[256];
    int     RxCount;
    int     loop;
    int     ending;
    
    static unsigned long time;
    char t_hi[1],t_low1[1],t_low0[1];    
    
            
    handle = open( "/dev/ttyS1", O_RDWR | O_NOCTTY );
    if( handle < 0 ) 
    {
        printf( "Serial Open Fail [/dev/ttyS1]\r\n "  );
        exit(0);
    }
    
    tcgetattr( handle, &oldtio );  // 

    memset( &newtio, 0, sizeof(newtio) );
    newtio.c_cflag = B38400 | CS8 | CLOCAL | CREAD ; 
    newtio.c_iflag = IGNPAR;		//parity error
    newtio.c_oflag = 0;

    //set input mode (non-canonical, no echo,.....)
    newtio.c_lflag = 0;
    
    newtio.c_cc[VTIME] = 30;    // time-out
    newtio.c_cc[VMIN]  = 0;     // MIN rea
    
    tcflush( handle, TCIFLUSH );
    tcsetattr( handle, TCSANOW, &newtio );
    
    //write( handle, TitleMessage, strlen( TitleMessage ) );
    
    ending = 0;
    
    int wheel_ctr=1;
    
    
    while(!ending)
    {
//	  write( handle, TitleMessage, strlen( TitleMessage ) );

    
        time=2293;        
        time=time%4096;
        
        
        *t_hi=time >>8;
         *t_hi=*t_hi+'0'+(*t_hi>9)*7;
       
        *t_low1=(time & 0xff) >>4;
        *t_low0=(time & 0x0f) ;
        //printf("hex description = %c%c\n", hi+'0'+(hi>9)*7,low+'0'+(low>9)*7);  
       
        *t_low1=*t_low1+'0'+(*t_low1>9)*7;
        *t_low0=*t_low0+'0'+(*t_low0>9)*7;
        
        write(handle,"T",1);
        write(handle,t_hi,1);
        write(handle,t_low1,1);
        write(handle,t_low0,1);
        
        if(wheel_ctr==1)
        {
                write(handle,"B",1);
                write(handle,"0",1);
                write(handle,"0",1);
                write(handle,"1",1);
                wheel_ctr=0;
        }else   //click wheel up
        {       
                 write(handle,"B",1);
                write(handle,"0",1);
                write(handle,"0",1);
                write(handle,"2",1);
                wheel_ctr=1;
         }            
        

        //RxCount = read( handle, Buff, 1 );
        /*
        if( RxCount == 0 ) 
        {
               // printf( "Receive Time Over\n" );
                continue;
        }        
        if( RxCount < 0 ) 
        {
                //printf( "Read Error\n" );
                break;
        }
        for( loop = 0; loop < RxCount; loop++ )
        {
           //printf( "Read Data [%c]\n", Buff[loop] );
//	    write(handle,Buff[loop],1);
          //  if( Buff[loop] == 0x1b ) ending = 1;
        }   */
       
	sleep(1);
       	
        //write( handle, Buff, RxCount );

        
    }
    
    tcsetattr( handle, TCSANOW, &oldtio );
    
    close( handle );
    
    return 0;    
}

                    



 

 

