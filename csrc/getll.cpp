#include <iostream>
#include <cstdlib>
#include <map>

const std::string  get_log_level()
 {
     const char* llp = std::getenv("XXX");                                                                                                                if (!llp)
     if (!llp)
        return "";
     return llp;
}

namespace blt {
enum severity_level
{
    sl0,
    trace,
    debug,
    info,
    warning,
    error,
    fatal,
    critical,
    end_sl
};
}

const std::map<std::string, blt::severity_level> slm
    =
    {
        {"trace", blt::trace},
        {"debug", blt::debug},
        {"info", blt::info},
        {"warning", blt::warning},
        {"error", blt::error},
        {"fatal", blt::fatal},
        {"critical", blt::critical}
    };
 
int main(int argc, char** argv)
{
    if (argc != 2) {
        std::cout << "usage: getll level" << std::endl;
    }
    std::string ev = argv[1];
    for (auto sli : slm) {
        std::cout << sli.first<< ':' << sli.second << std::endl;
    }
    auto evv = slm.find(ev);
    std::cout << ev << ':' << evv->second << std::endl;
    blt::severity_level sl = evv->second;
    //if #(ev.empty()) std::cout << "----";
    std::cout << "XXX : |" << get_log_level() << '|' << sl << std::endl;
    return 0;
}

