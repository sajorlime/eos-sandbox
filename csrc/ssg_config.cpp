
/*
    Copyright (C) 2021 oneNav, Inc. All rights reserved.

    ssg_config.cpp -- calculate ssg freq config values.
    emilior@onenav.com (Emilio Rojas)
*/



# include <iostream>
# include <iomanip>
//# include <math.h>
# include <sstream>
# include <iostream>
# include <string>
# include <cstdlib>

using namespace std;

double Ceil(double d)
{
    return (uint32_t) (d + 0.5);
}

constexpr double _FS = 132.5e6;
constexpr uint32_t _freq_width = 24; // bits
constexpr uint32_t _freq_scale = 1 << _freq_width;
constexpr double _asideband = -15 * 1.023E6;
constexpr double _bsideband = +15 * 1.023E6;

const char*_config_format = "0x9"; // 0x9 is the scale
constexpr int _config_width = 6; //  is the scale

double asideband(double ofreq)
{
    return _asideband + ofreq;
}

double bsideband(double ofreq)
{
    return _bsideband + ofreq;
}

uint32_t f2config(double fc)
{
    uint32_t f0 = (uint32_t) Ceil((fc * _freq_scale) / _FS);
    uint32_t f1 = f0 % _freq_scale;
    return 0x9000000 + f1;
    //stringstream sout;
    //sout << _config_format << hex << setw(_config_width ) << f1;
    //return sout.str();
}


uint32_t aconfig(double freq)
{
    double fc = asideband(freq);
    return f2config(fc);
}

uint32_t bconfig(double freq)
{
    double fc = bsideband(freq);
    return f2config(fc);
}


int main(int argc, char** argv)
{
    cout << "Freq    aconfig   bconfig" << endl;
    for (int i = 1; i < argc; i++) {
        double freq = stod(argv[i]);
        //cin >> freq;
        cout << "+F: " << freq << " : " << hex << setw(_config_width) << aconfig(freq) << " : " << bconfig(freq) << endl;
        cout << "-F: " << freq << " : " << hex << setw(_config_width) << aconfig(-1 * freq) << " : " << bconfig(-1 * freq) << endl;
    }
}
