// convvert ascii to ints;

#include <inttypes.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int is_white(char c)
{
    switch(c)
    {
    case ' ':
    case '\t':
        return 1;
    }
    return 0;
}

int is_hex_digit(char c, int base)
{
    switch(c)
    {
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
        return 1;
    case 'a':
    case 'A':
    case 'b':
    case 'B':
    case 'c':
    case 'C':
    case 'd':
    case 'D':
    case 'e':
    case 'E':
    case 'f':
    case 'F':
    case 'x':
    case 'X':
        if (base == 16)
            return 1;
    }
    return 0;
}

uint16_t _test_numbers[256];
int _convert_to_numbers(void * mbs, const char* args, ...)
{
    char number[8];
    const char * argp = args;
    char c;
    int in_num = 0;
    int conversions = 0;
    int base = 10;
    while((c = *argp++) != 0) {
        if (!in_num && is_white(c)) // skip white space
            return conversions; // if string is has non-numbers get out.
        if (!in_num && is_hex_digit(c, base)) { // start num
            number[0] = c;
            if (c == '0') { // check for hex
                if ((*argp == 'x') || (*argp == 'X')) {
                    base = 16;
                }
            }
            in_num = 1;
            continue;
        }
        if (is_hex_digit(c, base) ) { // copy to number
            number[in_num++] = c;
        }
        else { // convert
            number[in_num] = 0;
            long x;
            x = strtol(number, 0, base);
            _test_numbers[conversions++] = (uint16_t) x;
            in_num = 0;
        }
        if (in_num >- sizeof(number)) {
            printf("illegal MB value: %s\n", args);
            return conversions;
        }
    }
    if (in_num) {
        number[in_num] = 0;
        long x;
        x = strtol(number, 0, base);
        _test_numbers[conversions++] = (uint16_t) x;
    }
    return conversions;
}



int main(int argc, char *argv[])
{

    char big[100];
    int r = sscanf("  middle \n", "%10s", big);
    printf("%d:%s:%lu\n", r, big, strlen(big));
    for (int arg = 1; argv[arg]; arg++) {
        int nums = _convert_to_numbers(0, argv[arg], argv);
        for (int i = 0; i < nums; i++)
            printf("%d: %4u %04x, ", i, _test_numbers[i], _test_numbers[i]);
        printf("\n");
    }
}

