/*
 * &copyright Copyright (c)1997 BDN Software (Bruce D. Nilo). All rights reserved.
 *
 * $Id$
 * $Source$
 *
 * This object provides a very rudimentary read FileObject wrapper around
 * the Standard C Library file API.
 * $Log$
 */

#ifndef _SIMPLEFILE_H__
#define _SIMPLEFILE_H__
#ifdef _MSVER
#pragma once
#endif

#include "common/PrimitiveDefs.h"
#include <stdio.h>


class SimpleBasicBuffer
{
  void *bufaddress ;
  uint32 bufcount ;

public:

  SimpleBasicBuffer(void* addr, uint32 count) : bufaddress(addr), bufcount(count) {}
  void *getBuffer() { return bufaddress ; }
  uint32 bufferCount() { return bufcount ; }
  bool operator==(SimpleBasicBuffer& buf) { return buf.bufaddress == bufaddress ; }
  virtual size_t elemSize() { return sizeof(uint8) ; }
};

template<class T>
class BasicBuffer : public SimpleBasicBuffer
{
public:
  BasicBuffer(void* addr, uint32 count) : SimpleBasicBuffer(addr,count) {}
  size_t elemSize() { return  sizeof(T) ; }
} ;


class SimpleFile 
{
  const char* pathname ;
  FILE *fp ;

public:

  enum exceptionCodes {
    EndOfFile = 0,
    UsageError,
    OSError,
  } ;

  SimpleFile() {
    pathname = NULL ;
    fp = NULL ;
  }

  SimpleFile(const char* file) : pathname(file) {
    fp = fopen(file,"rb") ;
  }

  ~SimpleFile() {
    close() ;
  }

  void fileError() {
    if(fp) {
      if(ferror(fp))
	throw OSError ;
    }
  }

  void open(const char* file) {
    if(fp)
      close() ;
    pathname = file ;
    fp = fopen(file,"rb") ;
  }

  void close() {
    pathname = NULL ;
    if(fp) {
      fclose(fp) ;
      fp = NULL  ;
    }
  }

  uint32 bytesRemaining()  ;
  uint32 read(SimpleBasicBuffer& buf) ;

} ;


#endif

