﻿// deinterleave.cpp -- generate test cases and preformace tests.


#include <stdlib.h>
#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <cstdint>
#include <complex>
#include <iostream>
#include <vector>

#include <chrono>

#include "thread_pool.hpp"

thread_pool _thpool{std::thread::hardware_concurrency() - 1};

// ScopeProfile prints out the time spent in a certain scope to the debug
// log level
class ScopeProfile
{
    using tres = std::chrono::microseconds;
    using ttype = std::chrono::time_point<std::chrono::steady_clock>;
    using clock = std::chrono::steady_clock;
    using dmsg = std::pair<std::string, ttype>;

   public:
    ScopeProfile(const std::string& sname)
        : scope_name_{sname},
          begin_{clock::now()}
    {
    }

    ~ScopeProfile() {
        auto end = clock::now();
        if (scope_name_ != "")
            std::cout << scope_name_ << ", ";
        std::cout
            << std::chrono::duration_cast<tres>(end - begin_).count()
            << std::endl;
    }
    ScopeProfile(const ScopeProfile &) = delete;
    ScopeProfile &operator=(const ScopeProfile &) = delete;

   private:
    const std::string scope_name_;
    const std::chrono::steady_clock::time_point begin_;
};

using sc16 = std::complex<int16_t>;
using Res = std::vector<std::vector<sc16>>;

typedef Res (*DeInt)(void*, size_t, size_t, const std::string&);

/*
    deinterleave_frames -- generate test cases and preformace tests
    for delinterleaving I/Q interleaved samples.
    The I/Q samples come in the order:
           S0i, S1i, S0i+1, S1i+1, S0i+2, ... S1n
    We want to deinterleave to
           S0i, S0i+1, S0i+2, ... S0n
           S1i, S1i+1, S1i+2, ... S1n
*/
Res
deinterleave_frames(void* src_ptr,
                    size_t num_rx,
                    size_t num_elems,
                    const std::string& name)
{
    ScopeProfile x{name};
    std::vector<std::vector<sc16>> res{};
    const sc16* ptr = reinterpret_cast<const sc16* >(src_ptr);

    for (size_t ct = 0; ct < num_rx; ++ct) {
        std::vector<sc16> val(num_elems);
        for (size_t ct0 = 0; ct0 < num_elems; ++ct0) {
            val[ct0] = ptr[num_rx * ct0 + ct];
        }
        res.push_back(val);
    }

    return res;
}


Res
dif1(void* src_ptr, size_t num_rx, size_t num_elems, const std::string& name)
{
    ScopeProfile x{name};
    std::vector<std::vector<sc16>> res{};
    const sc16* ptr = reinterpret_cast<const sc16* >(src_ptr);

    using V_t = std::vector<sc16>;

    auto pull0 = [ptr, num_elems](V_t& val) {
        for (size_t ct0 = 0; ct0 < num_elems; ++ct0) {
            val[ct0] = ptr[ct0 << 1];
        }
    };
    auto pull1 = [ptr, num_elems](V_t& val) {
        for (size_t ct0 = 0; ct0 < num_elems; ++ct0) {
            val[ct0] = ptr[(ct0 << 1) + 1];
        }
    };

    std::vector<sc16> val0(num_elems);
    pull0(val0);
    res.push_back(val0);
    std::vector<sc16> val1(num_elems);
    pull1(val1);
    res.push_back(val1);
    return res;
}


Res
difp(void* src_ptr, size_t num_rx, size_t num_elems, const std::string& name)
{
    ScopeProfile x{name};
    std::vector<std::vector<sc16>> res{};
    const sc16* ptr = reinterpret_cast<const sc16* >(src_ptr);

    using V_t = std::vector<sc16>;

    auto ppull0 = [](V_t& val, const sc16* ptr, size_t elems) {
        std::this_thread::yield();
        for (size_t ct0 = 0; ct0 < elems; ++ct0) {
            val[ct0] = ptr[ct0 << 1];
        }
    };
    auto ppull1 = [](V_t& val, const sc16* ptr, size_t elems) {
        std::this_thread::yield();
        for (size_t ct0 = 0; ct0 < elems; ++ct0) {
            val[ct0] = ptr[(ct0 << 1) + 1];
        }
    };

    //std::cout << "TC: " << _thpool.get_thread_count() << std::endl;
    std::vector<sc16> val0(num_elems);
    auto fu_p0 = _thpool.submit(ppull0,
                                std::ref(val0),
                                std::ref(ptr),
                                std::ref(num_elems));
    //std::cout << "TQ: " << _thpool.get_tasks_queued() << std::endl;
    std::vector<sc16> val1(num_elems);
    auto fu_p1 = _thpool.submit(ppull1,
                                std::ref(val1),
                                std::ref(ptr),
                                std::ref(num_elems));
    //std::cout << "TR: " << _thpool.get_tasks_running() << std::endl;
    fu_p1.get();
    fu_p0.get();
    res.push_back(val0);
    res.push_back(val1);
    return res;
}

Res
difpt(void* src_ptr, size_t num_rx, size_t num_elems, const std::string& name)
{
    ScopeProfile x{name};
    std::vector<std::vector<sc16>> res{};
    const sc16* ptr = reinterpret_cast<const sc16* >(src_ptr);

    using V_t = std::vector<sc16>;

    auto ppull0 = [](V_t& val, const sc16* ptr, size_t elems) {
        for (size_t ct0 = 0; ct0 < elems; ++ct0) {
            val[ct0] = ptr[ct0 << 1];
        }
    };
    auto ppull1 = [](V_t& val, const sc16* ptr, size_t elems) {
        for (size_t ct0 = 0; ct0 < elems; ++ct0) {
            val[ct0] = ptr[(ct0 << 1) + 1];
        }
    };

    std::vector<sc16> val0(num_elems);
    _thpool.push_task(ppull0,
                      std::ref(val0),
                      std::ref(ptr),
                      std::ref(num_elems));
    std::vector<sc16> val1(num_elems);
    _thpool.push_task(ppull1,
                      std::ref(val1),
                      std::ref(ptr),
                      std::ref(num_elems));
    _thpool.wait_for_tasks();
    res.push_back(val0);
    res.push_back(val1);
    return res;
}

Res
difw(void* src_ptr, size_t num_rx, size_t num_elems, const std::string& name)
{
    ScopeProfile x{name};
    std::vector<std::vector<sc16>> res{};
    //const sc16* ptr = reinterpret_cast<const sc16* >(src_ptr);
    uint64_t* p64 = reinterpret_cast<uint64_t*>(src_ptr);

    std::vector<sc16> val0(num_elems);
    std::vector<sc16> val1(num_elems);
    const size_t ne = num_elems >> 1;
    for (size_t ct0 = 0; ct0 < ne; ct0 += 2) {
        uint64_t v = *p64++;
        val0[ct0] = (sc16) v;
        val1[ct0] = (sc16) (v >> 32);
    }
    res.push_back(val0);
    res.push_back(val1);

    return res;
}


Res
dif4_null(void* src_ptr, size_t num_rx, size_t num_elems, const std::string& name)
{
    ScopeProfile x{name};
    std::vector<std::vector<sc16>> res{};
    std::vector<sc16> val0(0);
    res.push_back(val0);
    res.push_back(val0);
    return res;
}

constexpr size_t SRATE = 6000000;
constexpr size_t STIME = 128; // ms
constexpr size_t SSIZE = STIME * SRATE / 1000;
constexpr size_t BUFFERS = 2;

alignas(1<<16)
sc16 _raw[SSIZE * BUFFERS];

// fill raw samples with two streams
//  S1k, S1k+1, S1+k+2, ..., S1+N
//  S2k, S2k+1, S2+k+2, ..., S2+N
// that are interleaved
//  S1k, S2k, S1k+1, S2k+1, S1k+2, ... S2k+N
// We want to distinguish the two streams, so will make
// S1 values range over the positive ints, or [1, 32767], and
// S2 values ran ge over the negative ints, or [32769, 65535]
void fill_raw()
{
    //ScopeProfile x{"fraw"};
    constexpr uint16_t mask = (uint16_t) (1 << 15) - 1;
    int16_t ref = 1;
    for (size_t i = 0; i < SSIZE; i += 2) {
        sc16 x = sc16{ref, ref};
        sc16 y = -x;
        _raw[i] = x;
        _raw[i + 1] = y;
        ref += 1;
        ref &= mask;
    }
}

bool validate_interleave(std::vector<sc16>& iv1,
                         std::vector<sc16>& iv2,
                         const std::string& name)
{
    //assert(iv1.size() == iv2.size());
    size_t end = iv1.size();
    for (size_t i = 0; i < end; i++) {
        if (iv1[i] != -iv2[i]) {
            std::cerr << name << ": "
                      << "iv1[" << i << "]"
                      << " != "
                      << "-iv2[" << i << "]"
                      << " : "
                      << "iv1[" << i << "] = " << iv1[i]
                      << " iv2[" << i << "] = " << iv2[i]
                      << " i:" << i 
                      << std::endl;
            return false;
        }
    }
    return true;
}

bool run_test(DeInt f, const std::string& name, int repeat=1)
{
    fill_raw(); // fill raw only needs to be called once, but ...
    bool br{false};
    Res res;
    for (; repeat > 0; repeat--) {
        res = f(_raw, BUFFERS, SSIZE, name);
        br = validate_interleave(res[0], res[1], name);
    }
    return br;
}

struct onetest {
    DeInt func_;
    const std::string name_;
    size_t repeat_;
}
_methods[32]
=
{
    {deinterleave_frames, "df0", 4},
    {dif1, "df1", 4},
    {difp, "dfp", 4},
    {difpt, "dfpt", 4},
    {difw, "dfpw", 4},
    {dif4_null, "dfnull", 1},
    {nullptr, "", 0}
};

int main(int argc, char** argv)
{
    uint32_t rflag = 0xff; // enable all
    bool use_name = true;
    if (argc > 1)
        rflag = atoi(argv[1]);
    size_t repeat = 0;
    if (argc > 2)
        repeat = atoi(argv[2]);
    if (argc > 3)
        use_name = false;
    uint32_t rmatch = 1;
    for (int mi = 0; _methods[mi].func_ != nullptr; mi++) {
        if (rflag & rmatch) {
            auto trepeat = _methods[mi].repeat_;
            if (repeat != 0) {
                trepeat = repeat;
            }
            auto tname = _methods[mi].name_;
            if (!use_name) {
                tname = "";
            }
            run_test(_methods[mi].func_, tname, trepeat);
        }
        rmatch <<= 1;
    }
    return 0;
}

