// downcast map members
#include <iostream>
#include <string>
#include <unordered_map>
#include <map>
#include <memory>
#include <tuple>


struct Limit
{
    int _min;
    int _max;
    Limit(int min, int max)
        : _min(min), _max(max)
    {
    }
};

Limit l10(10, 20);
Limit l20(20, 30);
Limit l30(30, 40);
Limit l40(40, 50);

struct TwoLimit : public Limit
{
    Limit _l2;
    TwoLimit(Limit const& l1, Limit const& l2)
        :
        Limit(l1), _l2(l2)
    {
    }
};

TwoLimit tl2(l10, l20);

struct ThreeLimit : public TwoLimit
{
    Limit _l3;
    ThreeLimit(TwoLimit const& twl, Limit const& l3)
        :
        TwoLimit(twl), _l3(l3)
    {
    }
    ThreeLimit(ThreeLimit const& th)
        :
        TwoLimit(th), _l3(th._l3)
    {
    }
};

ThreeLimit tl3g(tl2, l30);

ThreeLimit ctl3g(
        TwoLimit(l10, l20),
        Limit(100, 200)
 );

ThreeLimit tlxg(ctl3g);

std::shared_ptr<Limit> mkl1_from_l3(ThreeLimit const& l3)
{
   std::shared_ptr<ThreeLimit> lp3 = std::make_shared<ThreeLimit>(l3);
   return std::static_pointer_cast<Limit>(lp3);
}

std::map<std::string, std::shared_ptr<Limit>> _slmap
=
{
    {"a", std::make_shared<Limit>(Limit(l40))},
    {"b", mkl1_from_l3(ThreeLimit(
                        TwoLimit(Limit(l30),
                                 Limit(l20)),
                         Limit(137, 237))
                    )
    },
    {"c", std::make_shared<Limit>(ThreeLimit(ctl3g))},
    {"d", std::make_shared<Limit>(Limit(l40))},
};

std::map<std::string, Limit*> _lmap
=
{
    {"a", new Limit(l40)},
    {"b", new ThreeLimit(
                        TwoLimit(Limit(l30),
                                 Limit(l20)),
                         Limit(l10))
    },
    {"c", new ThreeLimit(ctl3g)},
    {"d", new Limit(l40)},
};




int main ()
{
    {
        ThreeLimit b = *((ThreeLimit*) _lmap["b"]);
        std::cout << "bp{:" << b._min << '<' << b._max
                       << ", " << b._l2._min << '<' << b._l2._max
                       <<  ", " << b._l3._min << '<' << b._l3._max
                  << '}' << std::endl;
    }
    {
        ThreeLimit b = *(std::static_pointer_cast<ThreeLimit>(_slmap["b"]));
        std::cout << "bs{:" << b._min << '<' << b._max
                       << ", " << b._l2._min << '<' << b._l2._max
                       <<  ", " << b._l3._min << '<' << b._l3._max
                  << '}' << std::endl;
    }
    ThreeLimit c = *((ThreeLimit*) _lmap["c"]);
    std::cout << "c{:" << c._min << '<' << c._max
                   << ", " << c._l2._min << '<' << c._l2._max
                   <<  ", " << c._l3._min << '<' << c._l3._max
              << '}' << std::endl;
    Limit d = *_lmap["d"];
    std::cout << "d:" << d._min << '<' << d._max << std::endl;


    return 0;
}
