
/*
    Threading definition
 */
# ifndef _MTHREAD_H_
# define _MTHREAD_H_

# include <chrono>
# include <map>
# include <thread>
# include <string>
# include <iostream>

# include "message_id.h"
# include "message.h"
# include "mqueue.h"

class MeThread; // forward ref

typedef std::map<std::string, MeThread*> ThreadMap;

class MeThread
{
    std::string _me;
    MSG::MsgQueue _myq;
    //TODO(epr): we need prioity
protected:
    static std::map<std::string, MeThread*> _thread_map;
public:
    virtual void run() = 0;
    MSG::MsgQueue* get_myq()
    {
        return &_myq;
    }

    MeThread(std::string th_name)
      : _me(th_name),
        _myq(th_name),
        _running(true)
    {
        _thread_map[th_name] = this;
    }

    const std::string& th_name()
    {
        return _me;
    }

    void process_ping(MSG::UMsg_t msg,
                      const std::string& rcvr)
    {
        MSG::MsgQueue* retq = msg->reply_to();
        std::string s = rcvr
                        +  " rcvd ping from: "
                        +  retq->name()
                        +  '\n';
        std::cout << s << std::flush;
        auto ack = std::make_unique<MSG::Ack>(get_myq());
        retq->send(std::move(ack));
    }

    void process_ack(MSG::UMsg_t msg,
                      const std::string& rcvr)
    {
        MSG::MsgQueue* retq = msg->reply_to();
        std::string s = rcvr
                        + " rcvd ack from: "
                        + retq->name()
                        + '\n';
        std::cout << s << std::flush;
    }

    void process_unknown(MSG::UMsg_t msg, const std::string& rcvr)
    {
        std::string s = rcvr
                        + " rcvd:"
                        + (char) msg->id()
                        + ':'
                        + msg->reply_to()->name()
                        + '\n';
        std::cout << s << std::flush;
    }

    void show_name()
    {
        std::string s = "Thread:" + th_name() + '\n';
        std::cout << s << std::flush;
    }

    static ThreadMap& get_thread_map()
    {
        return _thread_map;
    }

    bool _running;
};

class Host : public MeThread
{
public:
    Host() 
      : MeThread("Host")
    {
    }

    void run() override
    {
        show_name();
        MSG::MsgQueue* myq = get_myq();
        for (auto th : MeThread::_thread_map) {
            MSG::MsgQueue* sq = th.second->get_myq();
            if (sq == myq)
                continue;

            std::string s = "send ping to:" 
                            + sq->name() 
                            +  '\n';
            std::cout << s << std::flush;
            auto ping = std::make_unique<MSG::Ping>(myq);
            sq->send(std::move(ping));
        }
        // give up the CPU so threads can reply
        std::this_thread::sleep_for(std::chrono::milliseconds(700));
        std::cout << "Host getting replies\n" << std::flush;
        while (!myq->empty()) {
            auto msg = myq->receive();
            switch (msg->id()) {
            case MSG::MsgId::ack:
                process_ack(std::move(msg), th_name());
                break;
            default:
                process_unknown(std::move(msg), th_name());
                break;
            }
        }
        std::cout << "Host sending Quit\n" << std::flush;
        for (auto th : MeThread::_thread_map) {
            MSG::MsgQueue* sq = th.second->get_myq();
            if (sq == myq)
                continue;
            auto quit = std::make_unique<MSG::Quit>(myq);
            sq->send(std::move(quit));
        }
    }
};

class RxMan : public MeThread
{
public:
    RxMan() 
      : MeThread("RxMan")
    {
    }

    void run() override
    {
        show_name();
        while (_running) {
            MSG::UMsg_t msg = get_myq()->receive();
            switch (msg->id()) {
            case MSG::MsgId::ping:
                process_ping(std::move(msg), th_name());
                break;
            case MSG::MsgId::quit:
                std::cout << "RxMan rcvd Quit\n" << std::flush;
                _running = false;
                break;
            default:
                process_unknown(std::move(msg), th_name());
                break;
            }
        }
        std::cout << "sleep:" << th_name() << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        std::cout << "Leaving:" << th_name() << std::endl;
    }
};

class TrackEng : public MeThread
{
public:
    TrackEng() 
      : MeThread("TrackEng")
    {
    }

    void run() override
    {
        show_name();
        while(_running) {
            MSG::UMsg_t msg = get_myq()->receive();
            switch (msg->id()) {
            case MSG::MsgId::ping:
                process_ping(std::move(msg), th_name());
                break;
            case MSG::MsgId::quit:
                std::cout << "TrackEng rcvd Quit\n" << std::flush;
                _running = false;
                break;
            default:
                process_unknown(std::move(msg), th_name());
                break;
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
        std::cout << "Leaving:" << th_name() << std::endl;
    }
};

class AcqEng : public MeThread
{
public:
    AcqEng() 
      : MeThread("AcqEng")
    {
    }

    void run() override
    {
        show_name();
        while(_running) {
        MSG::UMsg_t msg = get_myq()->receive();
            switch (msg->id()) {
            case MSG::MsgId::ping:
                process_ping(std::move(msg), th_name());
                break;
            case MSG::MsgId::quit:
                std::cout << "AcqEngine rcvd Quit\n" << std::flush;
                _running = false;
                break;
            default:
                process_unknown(std::move(msg), th_name());
                break;
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(800));
        std::cout << "Leaving:" << th_name() << std::endl;
    }
};

# endif // _MTHREAD_H_

