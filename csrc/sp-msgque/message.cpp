
/*
!Still in process of being defined
A message should be a base class with the following properties.
1. An instantiation of a message has a particular type, but its storage
    comes from an underlying pool that is the size of the message type.
    TBD (or maybe we can get away with unique pointers).
2. when it is released/deleted its memory is returned the pool.
3. A message is delivered via queue shared by two or more threads.
4. A messages knows if it sync or async. (TBD)
5. A message is a unique_ptr or maybe a weak_ptr while being delivered. i.e.,
   a messaged object is owned in one of two places, a message_queue, or a thread
   manipulating message contents. (weak_ptr implies shared_ptr, both have
   issues if we manage our own pool.)
6. A message's memory goes back to the pool when it is destroyed.
7. The message contents should be savable back the <template type> 
Still in process of being defined!
 */

# include <condition_variable>
# include <chrono>
# include <map>
# include <mutex>
# include <queue>
# include <thread>
# include <string>
# include <iostream>

# include "message_id.h"
# include "message.h"
# include "mqueue.h"

class MyThread; // forward ref

// TODO(epr): make thread a unique_ptr?
std::map<std::string, MyThread*> _thread_map;

class MyThread
{
    std::string _me;
    MSG::MsgQueue _myq;
    //TODO(epr): we need prioity
public:
    virtual void run() = 0;
    MSG::MsgQueue* get_me()
    {
        return &_myq;
    }
    MyThread(std::string me)
      : _me(me),
        _myq(me)
    {
        _thread_map[me] = this;
    }
    const std::string& me()
    {
        return _me;
    }
    // TODO(epr) message creator, takes care of sender, template?
};

class Host : public MyThread
{
public:
    Host() 
      : MyThread("Host")
    {
    }

    void run() override
    {
        std::cout << "Thread:" << me() << std::endl;
        MSG::MsgQueue* myq = get_me();
        for (auto th : _thread_map) {
            MSG::MsgQueue* sq = th.second->get_me();
            //UMsg_t ping = std::make_unique<MSG::Ping>(myq);
            auto ping = std::make_unique<MSG::Ping>(myq);

            sq->send(std::move(ping));
        }
        // give up the CPU so threads can reply
        //this->sleep_for(std::chrono::milliseconds(10));
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        while (!myq->empty()) {
            auto msg = myq->receive();
            switch (msg->id()) {
            case MSG::MsgId::ack:
                std::cout << "Host rcvd ack from:" 
                          << msg->reply_to()->name()
                          << std::endl;
                break;
            default:
                std::cout << "Host rcvd ?:" 
                          << msg->mid()
                          << std::endl;
                break;
            }
        }
        for (auto th : _thread_map) {
            MSG::MsgQueue* sq = th.second->get_me();
            auto quit = std::make_unique<MSG::Quit>(myq);
            sq->send(std::move(quit));
        }
    }
};

// could be a member function
void process_ping(std::unique_ptr<MSG::Msg_t> msg,
                  const std::string& rcvr)
{
    MSG::MsgQueue* retq = msg->reply_to();
    std::cout << rcvr
              << " rcvd ping from: "
              << retq->name()
              << std::endl;
}

void process_unknown(MSG::UMsg_t msg, const std::string& rcvr)
{
    std::cout << rcvr
              << " rcvd:"
              << (int) msg->id()
              << ':'
              << msg->reply_to()->name()
              << std::endl;
}

class RxMan : public MyThread
{
public:
    RxMan() 
      : MyThread("RxMan")
    {
    }

    void run() override
    {
        std::cout << "Thread:" << me() << std::endl;
        MSG::UMsg_t msg = get_me()->receive();
        switch (msg->id()) {
        case MSG::MsgId::ping:
            process_ping(std::move(msg), me());
            break;
        default:
            process_unknown(std::move(msg), me());
            break;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        //this->sleep_for(std::chrono::milliseconds(1000));
        std::cout << "Leaving:" << me() << std::endl;
    }
};

class TrackEng : public MyThread
{
public:
    TrackEng() 
      : MyThread("TrackEng")
    {
    }

    void run() override
    {
        std::cout << "Thread:" << me() << std::endl;
        MSG::UMsg_t msg = get_me()->receive();
        switch (msg->id()) {
        case MSG::MsgId::ping:
            process_ping(std::move(msg), me());
            break;
        default:
            process_unknown(std::move(msg), me());
            break;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        //this->sleep_for(std::chrono::milliseconds(1000));
        std::cout << "Leaving:" << me() << std::endl;
    }
};

class AcqEng : public MyThread
{
public:
    AcqEng() 
      : MyThread("TrackEng")
    {
    }

    void run() override
    {
        std::cout << "Thread:" << me() << std::endl;
        MSG::UMsg_t msg = get_me()->receive();
        switch (msg->id()) {
        case MSG::MsgId::ping:
            process_ping(std::move(msg), me());
            break;
        default:
            process_unknown(std::move(msg), me());
            break;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        //this->sleep_for(std::chrono::milliseconds(1000));
        std::cout << "Leaving:" << me() << std::endl;
    }
};

int main(int argc, char** argv)
{
     // start the threads
     // they will have references in _thread_map, which we will use to join.
     // Start host last for this version
     RxMan rxm;
     std::thread trxm(&RxMan::run, &rxm); // run rxm
     TrackEng track;
     std::thread ttrack(&TrackEng::run, &track); // run track
     AcqEng acq;
     std::thread tacq(&AcqEng::run, &acq); // run acq
     Host host;
     std::thread thost(&Host::run, &host); // run host

     //for (auto th : _thread_map) {
        //th.second->join();
     //}
     trxm.join();
     ttrack.join();
     tacq.join();
     thost.join();
}


