
/*
    Define the message structures.
 */

# ifndef _MQUEUE_H_
# define _MQUEUE_H_

# include <condition_variable>
# include <mutex>
# include <queue>
# include <string>

# include "message.h"

namespace MSG {

// message queue handling

class MsgQueue : public std::queue<UMsg_t>
{
    std::mutex _mutex;
    std::condition_variable _cv;
    std::string _name;

public:
    MsgQueue(std::string name)
     : _name(name)
    {}

    void send(UMsg_t msg)
    {
        std::unique_lock<std::mutex> lck(_mutex);
        push(std::move(msg));
        _cv.notify_one();
    }

    UMsg_t receive()
    {
        std::unique_lock<std::mutex> lck(_mutex);
        _cv.wait(lck, [this]()->bool {return !empty();});

        UMsg_t msg = std::move(front());
        pop();
        return msg;
    }

    const std::string name() const
    {
        return _name;
    }
};

} // MSG

# endif // _MQUEUE_H_

