
/*
    Message identifiers.
 */

# ifndef _MESSAGE_ID_H
# define _MESSAGE_ID_H

namespace MSG {

constexpr uint32_t mid_span = 32; // distance between message groups
constexpr uint32_t nextgroup(uint32_t v)
{
    auto x = v / mid_span;
    return mid_span * (x + 1);
}

enum class MsgId
{
    // ids for system management
    unused = 0,
    ping, // echo back threads name
    ack, // generic ack, included for ping
    quit, // sent to any thread's queue to request that it quit.
    end_local_mids,

    first_input_mid = nextgroup(end_local_mids) + 1,
    adjust_time = first_input_mid,
    set_dwell,
    set_power_mode,
    report_time,
    set_sv_status,
    pre_position,
    rcvr_reset,
    log_level,
    capture,
    playback,
    hw_update,
    jtag_control,
    security_mode,
    status_request,
    end_input_mids, // insert new inputs before here

    first_output_mid = nextgroup(end_input_mids) + 1,
    time_report = first_output_mid,
    strobe_message,
    measurement_data,
    nav_data_bits,
    hardware_config,
    status_report,
    end_output_mids, // insert new outputs before here

    first_internal_mid = nextgroup(end_output_mids) + 1,
    placeholder = first_internal_mid, // TODO(epr): change placeholder
    end_internal_mids, // insert new internal messages here

    end_mids
};

} // namespace MSG

# endif // _MESSAGE_ID_H

