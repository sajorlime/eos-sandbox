
/*
    Define the message structures.
 */

# ifndef _MESSAGE_H_
# define _MESSAGE_H_

namespace MSG {

class MsgQueue; // forward ref

/* @brief base class for messages delivered through queues.
 * The base contains three elements the message identifier and the senders
 * queue for ease an consistency of replying, and the synchronous flag.
 * Messages delivered with the sync flag are replied to with an ack message.
 */
class Msg {
private:
    MsgId _id;
    MsgQueue* _sender;
    bool _sync;

public:
    Msg(MsgId id, MsgQueue* sender, bool sync=false)
      : _id(id),
        _sender(sender),
        _sync(sync)
    {};

    auto mid() {return static_cast<int>(_id);}
    auto id() {return _id;}
    MsgQueue* reply_to()
    {
        return _sender;
    }
};

typedef class Msg Msg_t;
//using std::unique_ptr<Msg_t> = UMsg_t;
typedef std::unique_ptr<Msg_t> UMsg_t;

// management messages
struct Ping : public Msg
{
    Ping(MsgQueue* sender)
      : Msg(MsgId::ping, sender, true)
    {
    }
};

struct Ack : public Msg
{
    Ack(MsgQueue* sender)
      : Msg(MsgId::ack, sender)
    {
    }
};

struct Quit : public Msg
{
    Quit(MsgQueue* sender)
      : Msg(MsgId::quit, sender)
    {
    }
};

// input messages. These should get their own header file.
struct AdjustTime : public Msg
{
    AdjustTime(MsgQueue* sender, int ms)
      : Msg(MsgId::adjust_time, sender),
        _d_ms(ms)
    {}
    int _d_ms; // delta ms
};

struct SetDwell : public Msg
{
    SetDwell(MsgQueue* sender, int ms_offset, int dwell_period)
      : Msg(MsgId::set_dwell, sender),
        _ms_offset(ms_offset), _dwell_period(dwell_period)
    {}

    int _ms_offset;
    int _dwell_period;
};

// output message, will get their own header file.

struct TimeReport : public Msg
{
    int _unadjusted_ms;
    int _adjustable_ms;
    int _week;
    int _ms;
};

} // MSG

# endif // _MESSAGE_H_

