
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace outer {

    namespace inner {
        struct s {
            int i;
        };
    }
}


using In = outer::inner::s;

int main(int argc, char** argv)
{
    In x = {7};
    std::cout << x.i << std::endl;
}


