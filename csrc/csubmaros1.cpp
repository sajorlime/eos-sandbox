

# include <iostream>
# include <map>
# include <stdio.h>
# include <string.h>
# include "ansi_colors.h"

std::map<std::string, std::string> color_map = {
  ANSI_COLOR_MAP("black"),
  ANSI_COLOR_MAP("red"),
  ANSI_COLOR_MAP("green"),
  ANSI_COLOR_MAP("yellow"),
  ANSI_COLOR_MAP("blue"),
  ANSI_COLOR_MAP("magenta"),
  ANSI_COLOR_MAP("cyan"),
  ANSI_COLOR_MAP("white"),
  ANSI_COLOR_MAP("bright_black"),
  ANSI_COLOR_MAP("bright_red"),
  ANSI_COLOR_MAP("bright_green"),
  ANSI_COLOR_MAP("bright_yellow"),
  ANSI_COLOR_MAP("bright_blue"),
  ANSI_COLOR_MAP("bright_magenta"),
  ANSI_COLOR_MAP("bright_cyan"),
  ANSI_COLOR_MAP("bright_white")
};

enum class AnsiColor {
  Black,
  Red,
  Green,
  Yellow,
  Blue,
  Magenta,
  Cyan,
  White,
  BrightBlack,
  BrightRed,
  BrightGreen,
  BrightYellow,
  BrightBlue,
  BrightMagenta,
  BrightCyan,
  BrightWhite,
};

struct ABC
{
    int a;
    int b;
    int c;
};

# define MAKESYM(X, A) X ## _name_ ## A

void show_abc(ABC x)
{
    std::cout << x.a << std::endl;
    std::cout << x.b << std::endl;
    std::cout << x.c << std::endl;
}


# define _TicksInMsec_(Pv) (10 * Pv)
# define MVAL(Pmem, Pval) ((long) Pmem - (long) Pval)
# define FORWARD_MSECS(Pstruct, Pmember, Pforward) Pstruct.Pmember \
    _TicksInMsec_(MVAL(Pstruct.Pmember, Pforward) \
                    ? (MVAL((*Pmember, Pforward) \
                        > 17) : \
                        MVAL(Pstruct.Pmember, Pforward))
# define VOLTS_FORWARD_MSECS(Pname, Pforward) \
    FORWARD_MSECS(&abd. Pname, Pforward)
# define OV2_FORWARDED_MSECS \
    VOLTS_FORWARD_MSECS(c, ((7 * 17) + 8))


# define VAL(Pvalue) Pvalue - (long) 10
# define ASSIGN_ABC(Pstruct, Pmember, Pvalue) \
    Pstruct.Pmember = VAL(Pvalue) < 11? Pvalue : 10
# define GET_M(Pstruct, Pmember, Pvalue) \
    Pstruct.Pmember - (VAL(Pvalue) < 33 ? Pvalue : 32)

int main(int argc, char** argv)
{
# if 0
    int MAKESYM(x, e) = 6;
    int MAKESYM(x, f) = 7;
    std::cout << x_name_e << std::endl;
    std::cout << x_name_f << std::endl;
# endif
    ABC abc;
    abc.a = 4;
    abc.b = 5;
    abc.c = 6;
    int x = FORWARD_MSECS(&abc, b, 45);
    int y = VOlTS_FORWARD_MSECS(&abc, b, 45);
    int z = OV2_FORWARDED_MSECS(&abc, b, 45);
    std::cout << x << std::endl;
# if 0
    show_abc(abc);
    std::cout << (int) GET_M(abc, c, 25) << std::endl;
    ASSIGN_ABC(abc, a, 33);
    show_abc(abc);
    ASSIGN_ABC(abc, b, 77);
    show_abc(abc);
    std::cout << abc.a << std::endl;
    std::cout << abc.b << std::endl;
# endif
    return 0;
    for (int i = 1; i < argc; i++)
    {
        auto icm = color_map.find(argv[i]);
        if (icm == color_map.end())
        {
            std::cout << "opps: " << argv[i] << " not in the map" << std::endl;
            
        }
        std::cout << "Found: " << icm->second << argv[i] << ANSI_RESET << std::endl;
        return 0;
    }
    for (const auto& [color, name] : color_map)
    {
        std::cout << color << " " << name << std::endl;
    }
    return 0;
}

