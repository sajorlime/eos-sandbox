// unordered_map::find
#include <iostream>
#include <string>
#include <unordered_map>
#include <map>
#include <tuple>

enum EventCode {
    e
};

class CodeTuple {
public:
    EventCode c;
    std::string s;
    int truth;
    CodeTuple(EventCode c, std::string s, int truth)
        : c(c), s(s), truth(truth)
    {
    }
};

typedef int (* _check)(CodeTuple* ctp);



typedef std::tuple<std::string, int, int> Mt;
typedef std::tuple<int ,std::string, int> tM;

int f1(CodeTuple* ct) {return ct->truth;}
int f2(CodeTuple* ct) {return -ct->truth;}


struct info {int i; _check cf;};

info infos[] = {
    {0, f1},
    {1, f2},
    {2, [](CodeTuple* ct){return 77*ct->truth;}},
};


union U {int i; float f;
         struct bt {unsigned zero:1; unsigned one:1;} bit;
         U(int i) : i(i) {}};

U u(7);

typedef U::bt mbt;

int main ()
{
  std::cout << "UI:" << u.i << " UF:" << u.f << ':' << u.bit.zero << std::endl;
  u.f = 0.1;
  std::cout << "UI:" << u.i << " UF:" << u.f << ':' << u.bit.zero << std::endl;
  mbt y = u.bit;
  std::cout << "mbt:" << y.zero << std::endl;
  CodeTuple ct(e, "e", 1);
  std::cout << "I0:" << infos[0].cf(&ct) << std::endl;
  std::cout << "I1:" << infos[1].cf(&ct) << std::endl;
  std::cout << "I2:" << infos[2].cf(&ct) << std::endl;

  std::unordered_map<std::string,double> mymap = {
     {"mom",5.4},
     {"dad",6.1},
     {"bro",5.9} };

  tM A = std::make_tuple(5, "mom", 4);
  tM B = std::make_tuple(6, "dad", 1);
  tM C = std::make_tuple(5, "bro", 9);
  std::map<tM, int> mzmap = {
     {A,7},
     {B,2},
     {C,3} };

  for ( auto mi : mzmap ) {
    std::cout << std::get<0>(mi.first) << ':' << std::get<1>(mi.first) << "==" << mi.second << std::endl;
  }

  Mt a=std::make_tuple("mom", 5,4);
  Mt b=std::make_tuple("dad",6,1);
  Mt c=std::make_tuple("bro",5,9);
  std::map<Mt, int> mxmap = {
     {a,7},
     {b,2},
     {c,3} };

  for ( auto mi : mxmap ) {
    std::cout << std::get<0>(mi.first) << ':' << std::get<1>(mi.first) << "==" << mi.second << std::endl;
  }

  //std::tuple<std::string, int, char> tv;
  Mt tv = std::make_tuple("sis", 5, 'A');
  std::cout << std::get<0>(tv) << ':' << std::get<1>(tv) << ':' << std::get<2>(tv) << "==" << mxmap[tv] << std::endl;
  tv = std::make_tuple("mom", 5, 4);
  std::cout << std::get<0>(tv) << ':' << std::get<1>(tv) << ':' << std::get<2>(tv) << "==" << mxmap[tv] << std::endl;
  tv = std::make_tuple("mom", 5, 3);
  std::cout << std::get<0>(tv) << ':' << std::get<1>(tv) << ':' << std::get<2>(tv) << "==" << mxmap[tv] << std::endl;
  tv = std::make_tuple("dad", 6, 2);
  std::cout << std::get<0>(tv) << ':' << std::get<1>(tv) << ':' << std::get<2>(tv) << "==" << mxmap[tv] << std::endl;
  tv = std::make_tuple("dad", 6, 1);
  std::cout << std::get<0>(tv) << ':' << std::get<1>(tv) << ':' << std::get<2>(tv) << "==" << mxmap[tv] << std::endl;
  tv = std::make_tuple("mom", 5, 9);
  std::cout << std::get<0>(tv) << ':' << std::get<1>(tv) << ':' << std::get<2>(tv) << "==" << mxmap[tv] << std::endl;
  tv = std::make_tuple("bro", 5, 9);
  std::cout << std::get<0>(tv) << ':' << std::get<1>(tv) << ':' << std::get<2>(tv) << "==" << mxmap[tv] << std::endl;
  int x;
  x = std::get<1>(tv);
  std::cout << "X:" <<  x << std::endl;

  std::string input;
  std::cout << "who? ";
  getline (std::cin,input);

  //std::unordered_map<std::string,double>::const_iterator got = mymap.find (input);
  auto got = mymap.find(input);

  if ( got == mymap.end() )
    std::cout << "not found";
  else
    std::cout << got->first << " is " << got->second;

  std::cout << std::endl;

  return 0;
}
