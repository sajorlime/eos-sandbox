
# include <stdlib.h>
# include <iostream>

void* myalloc(size_t size)
{
    return malloc(size);
}

void myfree(void* vp)
{
    free(vp);
}

int cntsteps(int steps)
{
    if (steps == 1)
        return 1;
    if (steps == 2)
        return 2;
    return cntsteps(steps - 2) + cntsteps(steps -  1);
}

int main(int argc, char** argv)
{
    for (int n = 1; n < 15; n++)
        std::cout << "n:" << n << " -> "  << cntsteps(n) << std::endl;
}



