/*
// 
// $Id: //Source/Common/libutility/BitBuffer.h#6 $ 
// 
// Copyright (c) 2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: BitBuffer.h
// 
// Creation Date: 9/18/01
// 
// Last Modified: 
// 
// Author: Emil Rojas
// 
// Description: 
// Implements the notion of a bit buffer that allows arbirtarity bit
// fields to be written sequentually. 
//  
*/


# ifndef __BITBUFFER_H__
# define __BITBUFFER_H__

# include <fstream.h> // needed only for debug function
# include <stdlib.h> // need for strtol.

# include "dctype.h"
# include "dotcastns.h"
# include "BitArray.h"

# ifndef _WIN32
# include <sys/types.h>
# include <netinet/in.h>
# include <inttypes.h>
# endif




/*
    intValue -- is utility function for converting a string to ascii value
    pointed to by the string.  See strtol for details.  
    Takes both octal, decimal, & hexadecimal strings.
    Stings not pointing to a "number" get zero.
    This function could easily go somewhere else.
*/
inline int intValue (const char * pV)
{
    char * pStop = "";

    return strtol (pV, &pStop, 0);
}



/*
    CBitBuffer -- the primary bit-buffer-class.
    Class responsibilities:
        It provides operations to construct the bit buffer of the desired size,
        to gain access to the bit-size and raw buffer data,
        to write-bits, bytes, for convience write-zero-bits,
        and to insert data at an arbritary position in the bit buffer.
        It manages the current position of the bit buffer.
        Writes add to the bits in the buffer, while insert only changes the buffer
        representation.

        If class allocates buffer then it frees the buffer when class is destroyted.

    Subclass responsibilities:
        The bit-buffer class is intended to be sub-classed by the section-buffer-class,
        the mp2-buffer-class, and the base64-buffer-class.  These classes and all
        sub-classes must follow the client responsibilities.  

        
    Client responsibilities:
        Ensure that s/he does not run off the end of the allocated buffer.
        Class is not thread safe, should be used accordingly.
        Responsbile for memory if external memory passed to bit-buffer,
        likewise cannot free memory if class allocated.


*/

class CBitBuffer : public CBitArray
{
protected:
    int m_bitPos;
    int m_getPos;

public:
    explicit CBitBuffer (int byteSize)
        :
            CBitArray (byteSize),
            m_bitPos (0),
            m_getPos (0)
    {
    }

    explicit CBitBuffer (uint32 * pBitBuffer, int size = 0, bool deleteIt = false)
        :
            CBitArray (pBitBuffer, size, deleteIt),
            m_bitPos (0),
            m_getPos (0)
    {
    }

    virtual ~CBitBuffer () {}

    /*
        bits -- returns the number of bits in the buffer.
    */
    int bits (void) {return m_bitPos;}

    /*
        writeBits -- write a value of size <= 32 bits to the bit buffer.
        The value v is right justified in v.  The value is then appended to
        the end of the bit buffer, changing the number of bits, etc.
        Bits is <= 32.
        Note: no check is made to insure that the user does not run off the end
        of the bit store allocated at construction time.
        returns bit position at end of write.
    */
    uint32 writeBits (uint32 v, uint32 bits);
    

    /*
        writeZero -- writes-bits of zero.
        returns bit position at end of write.
        Any number of zero bits may be written.
    */
    uint32 writeZero (uint32 bits);

    /*
        writeOne -- writes-bits of one.
        returns bit position at end of write.
        Any number of zero bits may be written.
    */
    uint32 writeOne (uint32 bits);

    /*
        writeStrInt -- allows the transfer, at the current position 
        of the integer value of the string of bits width.
        returns bit position at end of write.
    */
    uint32 writeStrInt (const char * pV, uint32 bits);

    /*
        writeBytes -- allows the transfer, at the current position 
        of bytes to the bit buffer.
        returns bit position at end of write.
    */
    uint32 writeBytes (uint8 * pV, uint32 bytes);
    
    /*
        readBits -- returns the bits at position in the bit-buffer right justified
        to bits.  Bits <= 32.
    */
    uint32 readBits (uint32 position, uint32 bits) {return extractBits (position, bits;}
    
    /*
        getBits -- returns the bits at getPosition in the bit-buffer right justified
        to bits.  Bits <= 32.
    */
    uint32 getBits (uint32 bits);

    uint32 ungetBits (uint32 bits) {return m_getPos -= bits;}
    
    /*
        getPosition -- returns the current get position of the buffer.
    */
    int32 getPosition (void) {return m_getPos;}

};


# ifdef _DEBUG
/* 
    dumpBuffer -- a untility function for debugging a bit buffer.
*/
void dumpBuffer (ofstream & fout, CBitBuffer * pBB);
# endif

/* 
    writeBuffer -- a untility function for writting a bit buffer.
*/
void writeBuffer (ofstream & fout, CBitBuffer * pBB);


# endif



# endif

