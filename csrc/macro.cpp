
#include <stdio.h>
#include <cstdarg>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>

using namespace std;

# define TXTY(P) #P
# define VER 377
# define DEVVER(P) "EPR" TXTY(P) 
# define CS(Pclocks) "asm vol (disi #" TXTY(Pclocks) ")"

# define SA(V, Name) static_assert(V, #Name)

#define ZCU111 0
#define ZYBO 1
#define BOARD_TYPE ZYBO
# ifndef BOARD_TYPE
    static_assert(0, "BOARD_TYPE must by defined")
# endif
# ifdef INCLUDE_USB 
    static_assert(0, "can't be defined")
# endif

# if BOARD_TYPE==ZCU111
#   define INCLUDE_USB 0
# elif BOARD_TYPE==ZYBO
#   define INCLUDE_USB 1
# else
# error "##BOARD_TYPE must by ZCU111 or ZYBO"
# endif
SA(INCLUDE_USB, BOARD_TYPE);
//static_assert(INCLUDE_USB, "what##BOARD_TYPE");



struct Command
{
    string name;
    string (*f)(string);
    int _a;
    int _b;
    int _c;
};

#define CVWRITE(Param) cond_value_write(ss, ind, #Param, _#Param);
    
string t1_command(string s) {return s;}
string t2_command(string s) {return s;}
string t3_command(string s) {return s;}

# if 0
typedef void * (rcv_state) (Mbs * mbs,
                            uint8_t const* data,
                            uint16_t size,
                            uint64_t ms);
# endif



//# define CMD(Name) { #Name, Name ## _command, _1, 2, 3 }

# define CMD(Name, Var, Val) { #Name, Name ## _command, ._ ## Var = Val}
Command cmds[] =
{
    //CMD(t1, a, 1),
    //CMD(t2, b, 2),
    //CMD(t3, c, 3)
    {"t1", t1_command, 1, 2, 3},
    {"t2", t2_command, 1, 2, 3},
    {"t1", t3_command, 1, 2, 3}
};

char info[] = "ABCEDFGHIJKLMNOLPRSTUVWZYZ";

void write_out_1(char* msg, size_t& osize)
{
    std::cout << *(msg + osize++) << '|';
}

void write_out_2(char* msg, size_t& osize)
{
    std::cout << *(msg + osize);
    std::cout << *(msg + osize + 1) << '|';
    osize += 2;
}

void write_out_3(char* msg, size_t& osize)
{
    std::cout << *(msg + osize++);
    std::cout << *(msg + osize++);
    std::cout << *(msg + osize++) << '|';
}

# define WRITE_BUF(Pitem, Psize) write_out_##Psize(Pitem, osize)

typedef void (*OnTick)(uint32_t tick);
void f(uint32_t ticks) {std::cout << "0:" << ticks << std::endl;}
void _0(uint32_t ticks) {std::cout << "0:" << ticks << std::endl;}
void _1(uint32_t ticks) {std::cout << "1:" << ticks << std::endl;}
void _2(uint32_t ticks) {std::cout << "2:" << ticks << std::endl;}
void _3(uint32_t ticks) {std::cout << "3:" << ticks << std::endl;}

OnTick _0fp = _0;
OnTick _1fp = _0;
OnTick _2fp = _0;
OnTick _3fp = _0;

bool on_mstick(OnTick ontick, int pri)
{
    // lambda
    auto update_tickp = [ontick](OnTick& dstfp) {
        dstfp = ontick;
        std::cout << "cache flush:" << (void*)dstfp << ':' << (void*)ontick << std::endl;
    };
    switch (pri) {
    case 1:
        update_tickp(_1fp);
        //_rx_ms_fp = ontick;
        break;
    case 2:
        update_tickp(_2fp);
        //_ae_ms_fp = ontick;
        break;
    case 3:
        update_tickp(_3fp);
        //_te_ms_fp= ontick;
        break;
    default:
        std::cout << "NO!" << std::endl;
    }
    std::cout << (void*)_0fp << ':' << (void*)_1fp << ':' << (void*)_2fp << ':' << (void*)_3fp << ':' << std::endl;
    return true;
}


using namespace std;

int main(int argc, char ** argv)
{
    cout << "VER:" << VER << endl;
    cout << "DEVVER:" << DEVVER(VER) << endl;
    cout << "CS 5:" << CS(5) << endl;
    //cout << "MYVER:" << MYVER << endl;
    (void) argc;
    (void) argv;
    return 0;
    on_mstick(_1, 1); _1(1);
    on_mstick(_2, 2); _2(2);
    on_mstick(_3, 3); _3(3);
    // lambda
   auto big16 = [](uint8_t a, uint8_t b) {
   //uint16_t big16 = [](uint8_t a, uint8_t b) {
        return (uint16_t) ((a << 8) | b);
    };
    auto myout = [] (uint16_t x) {
        cout << "MYOUT:" << hex << x << dec << endl;
    };
    for (int i = 0; i < 3; i++) {
        cout << cmds[i].f(cmds[i].name) << endl;
    }
    myout(17);
    myout(big16('A', 'B'));
    myout(big16('0', '1'));
    std::string xs;
    //xs << std::cin;
    //cin >> xs;
    cout << xs << endl;

    size_t osize = 0;
    WRITE_BUF(info, 1);
    WRITE_BUF(info, 2);
    WRITE_BUF(info, 3);
    WRITE_BUF(info, 2);
    WRITE_BUF(info, 1);
    std::cout << '\n' << osize << std::endl;
    return 0;
}

