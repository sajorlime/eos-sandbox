
/*
 * File: median.cpp
 *
 * Implementation of a median filter.
 *
 */


# include <algorithm>
# include <cassert>
# include <cstdlib>
# include <iostream>
# include <fstream>
# include <iostream>
# include <random>
# include <string>
# include <stdio.h>
# include <time.h>

#pragma GCC diagnostic ignored "-Warray-bounds"

# define USE_STD_SORT 0

typedef uint32_t mftype;
typedef int mfindex;


// Generate some random noise for input, this is a hack for testing
// not a stotasticly meaningful value
// Args:
//  ratio: the max noise level ratio
// Return:
//   random ratio, where random is [-0.5, 0.5]
float some_noise(float ratio=0.1)
{
    float f = static_cast<float>(rand()) / RAND_MAX;
    float fr = f - 0.5;
    //std::cout << ocol(std::cout, f) << ocol(fr) << std::endl;
    return ratio * fr;
}

# ifndef MF_SIZE 
#   define MF_SIZE 31
# endif
const int mfsize = MF_SIZE;
const int mfm_pos = mfsize >> 1;
// store the last mfsize values, in rotating buffer
mfindex mfl_pos = 0;
mftype mf_last[mfsize];
// note wie fille both mf_last and mf_sort with a single value to start!
mftype mf_sorted[mfsize]; // value of 16 bits or less
mftype* mf_median = &mf_sorted[mfm_pos];
mftype* mf_mend = &mf_sorted[mfsize];

void show_sorted(const char* info="Sorted: ")
{
    std::cout << info;
    for (int l = 0; l < mfsize; l++) {
        if (l == (mfsize >> 1))
            std::cout << "**" << mf_sorted[l] << "**, ";
        else
            std::cout << mf_sorted[l] << ", ";
    }
    std::cout << std::endl;
# if 1
    std::cout << "last(" << mfl_pos << "): ";
    mfindex lpos = mfl_pos;
    for (int l = 0; l < mfsize; l++) {
        //std::cout << lpos << ":" << mf_last[lpos] << ", ";
        lpos++;
        if (lpos >= mfsize)
            lpos = 0;
        if (l == mfl_pos)
            std::cout << "**" << mf_last[l] << "**, ";
        else
            std::cout << mf_last[l] << ", ";
    }
    std::cout << std::endl;
    std::cout << std::endl;
# endif
}

# define DEBUG_FIND_POS 0
/* find the position val in the sorted array
 * Args:
 *  val is the value you are searching for
 *  front: is the current front of the sorted array
 *  len: is the current length of the sorted array
 *  sa: is the actual sorted array array, used to return position
*/
mfindex find_pos(mftype val, mftype* front, mfindex len, mftype* sa)
{
    mfindex mid = len >> 1;
# if DEBUG_FIND_POS
    std::cout << "FP: " << val << ", mid: " << mid
              << ", diff: " << (front - sa)
              << ", len: " << len << std::endl;
# endif
    assert(len >= 1);
    if (mid == 0) {
# if DEBUG_FIND_POS
        std::cout << "FPret: " << (front - sa) << " sa[mid}: " << sa[mid] << std::endl;
# endif
        return front - sa;
    }
    if (val < front[mid])
    {
# if DEBUG_FIND_POS
        std::cout << " << : ";
# endif
        return find_pos(val, front, mid, sa);
    }
    if (val == front[mid])
    {
# if DEBUG_FIND_POS
        std::cout << " ==: " << &front[mid] - sa;
# endif
        return &front[mid] - sa;
    }
# if DEBUG_FIND_POS
    std::cout << " >> : ";
# endif
    return find_pos(val, front + mid + 1, mid, sa);
}


/* initialize the median filter data structures.
 * mf_last holds the recent mfsize values.
 * mf_sorted is the array that is always sorted at the end of each call 
 * to mfilter.
*/
void mf_init(mftype sv)
{
    for (int i = 0; i < mfsize; i++)
    {
        mf_last[i] = sv;
        mf_sorted[i] = sv;
    }
    mfl_pos = 0;
    //show_sorted("Init: ");
}

/* validates that the filter is sorted, and optionally that values
 * are extactly zero or one appart for certain tests.
 * Return:
 *  true of filter is sorted, otherwise false.
*/
bool mf_validate(bool exact=false)
{
    for (int l = 1; l < mfsize; l++)
    {
        int d = ((int) mf_sorted[l]) - ((int) mf_sorted[l - 1]);
        if ((d < 0)
            || (exact && (abs(d) > 1))
           )
        {
            std::cout << "invalid: " << d << ":";
            std::cout << l << ":"
                      << mf_sorted[l - 1]  << " > "
                      << mf_sorted[l]
                      << std::endl;
            show_sorted("S-invalid: ");
            return false;
        }
    }
    return true;
}

# define DEBUG_PUSH_UP 0

/* pushed the data up from the insertion put to the end of the filter.
 * Args:
 *  ppush: a pointer to the insertion point that points to the inserted value
 *  ppush_end: the end of the data to be moved
*/
void mf_push_up(mftype* ppush, mftype* ppush_end)
{
    // pick up the current location. we want to bubble it up to
    // restore a sorted list.
    // Note(epr): this linear array clearly has some deficiencies,
    //  but a tree structure requires more space and is not clearly
    //  more efficient for small sizes.
    //  If the process has a move data operation, a binary search for
    //  the end might be worthwhile.
    mftype vplace = *ppush; // the value to place
# if DEBUG_PUSH_UP
    std::cout << "\nmfpu-ppush: " << (ppush - mf_sorted)
              << ", pend: " << (ppush_end - mf_sorted)
              << ", vplace: " << vplace
              << std::endl;
# endif
    mftype v1;
    for (; ppush < ppush_end - 1; ppush++)
    {
        v1 = *(ppush + 1);
        *ppush = v1; // move it down
        if (vplace <= v1)
        {
            // we are done, place it.
            *ppush = vplace;
# if DEBUG_PUSH_UP
            std::cout << "mfpu-place in: " << (ppush - mf_sorted)
                      << ", vplace: " << vplace
                      << ", v1: " << v1
                      << std::endl;
            show_sorted("\nmfpu-place in:");
# endif
            return;
        }
    }
# if DEBUG_PUSH_UP
    std::cout << "mfpu-place AFTER: " << (ppush - mf_sorted)
              << ", vplace: " << vplace
              << std::endl;
# endif
    assert(ppush < mf_mend);
    *ppush = vplace;
# if DEBUG_PUSH_UP
    show_sorted("\nmfpu-place-AT=end: ");
# endif
}

# define DEBUG_PUSH_DOWN 0
// see notes at push-up
void mf_push_down(mftype* pdown, mftype* pdown_end)
{
    mftype vplace = *pdown;
    mftype v1;
# if DEBUG_PUSH_DOWN
    std::cout << "mfpd-pdown: " << (pdown - mf_sorted)
              << ", pstart: " << (pdown_end - mf_sorted)
              << std::endl;
# endif
    for (; pdown > pdown_end; pdown--)
    {
        v1 = *(pdown - 1);
        *pdown = v1; // move it down
        if (vplace >= v1)
        {
# if DEBUG_PUSH_DOWN
            std::cout << "mfpd-place in: " << (pdown - mf_sorted)
                      << ", vplace: " << vplace
                      << std::endl;
# endif
            // we are done, place it.
            *pdown = vplace;
            return;
        }
    }
# if DEBUG_PUSH_DOWN
    std::cout << "mfpd-place after: " << (pdown - mf_sorted)
              << ", vplace: " << vplace
              << std::endl;
# endif
    assert(pdown >= mf_sorted);
    *pdown = vplace;
}

/* mfilter -- implements at median filter on a linear array of sorted
 * mftype.  mftype is assumed to be a integer type, but could be any type
 * with < and == operators.
 * Algorithm:  The data is initialized with an expected value, see mf_init(),
 * and therefor is by default sorted.  There are two arrays of mfsize, 
 * mf_sorted and mf_last.  mf_last is a round-robin buffer of the last filtered
 * values. 
 * When I new value is to be filtered, we pick up the oldest value from mf_last
 * and find it in the mf_sorted, and replace that location with the new value.
 * This takes care of the history issue in a median filter, i.e., the median 
 * is defined on the last N samples.
 * We now have an unsorted list in one position, namely the position we just
 * replace.  Depending on its relative value compared to the replaced value
 * we need to leave it, push it down, or push it up.
 * Args:
 *  y: the new value to be placed in the filter
 *  show: a debugging flag, when true prints the filter values.
*/

int mfilter(const mftype y, bool show=false)
{
    if (show)
    {
        show_sorted("S-mf: ");
    }
    mftype rm_val = mf_last[mfl_pos];
    mf_last[mfl_pos++] = y;
    if (mfl_pos >= mfsize)
        mfl_pos = 0;

    mftype rm_pos = find_pos(rm_val, mf_sorted, mfsize, mf_sorted);
    //std::cout << "rm_pos:" << rm_pos << std::endl;
    mf_sorted[rm_pos] = y;
    if (y == rm_val)
    {
        //std::cout << "y == rm_pos: " << y << std::endl;
        return *mf_median;
    }
    if (y < rm_val)
    {
        //std::cout << "y(" << y << ") <= rm_pos(" << rm_pos << ")" << std::endl;
        mf_push_down(&mf_sorted[rm_pos], mf_sorted);
    }
    else
    {
        //std::cout << "y(" << y << ") > rm_pos(" << rm_pos << ")" << std::endl;
        mf_push_up(&mf_sorted[rm_pos], mf_mend);
    }

    return *mf_median;
}


bool test_find_pos()
{
    mftype tsorted[mfsize];
    for (mftype i = 0; i < mfsize; i++)
    {
        tsorted[i] = 6000 + i;
    }
    for (mftype i = 0; i < mfsize; i++)
    {
        mftype rm_pos = find_pos(6000 +i , tsorted, mfsize, tsorted);
# if DEBUG_INSERT_ARROUND
        std::cout << i << ": " << rm_pos
                  << " ts[i]: " << tsorted[i] << std::endl;
# endif
        assert(tsorted[rm_pos] == 6000 + i);
    }
    for (mftype i = 0; i < mfsize; i++)
    {
        tsorted[i] = 6000 + 5 * i;
    }
    for (mftype i = 0; i < mfsize; i++)
    {
        mftype rm_pos = find_pos(6000 + 5 * i, tsorted, mfsize, tsorted);
# if DEBUG_INSERT_ARROUND
        std::cout << i << ": " << rm_pos
                  << " ts[i]: " << tsorted[i] << std::endl;
# endif
        assert(tsorted[rm_pos] == 6000 + 5 * i);
    }
    return true;   
}

# define DEBUG_INSERT_NEG 0
// note, this is different than the other tests, as it testings an 
// illegal condition.
bool test_insert_neg()
{
# if DEBUG_INSERT_NEG 
    mftype y0 = 100000;
    mf_init(y0);
    mfilter(y0 + 1);
    show_sorted("\nneg-place +1:");
    for (int i = -1; i > -100; i--)
    {
        mfilter((mftype) i);
        if (mf_validate(false))
        {
            std::cout << i << ": " << std::endl;
            show_sorted("err-neg-neg:");
            return false;
        }
    }
    show_sorted("neg-neg:");
# endif
    return true;   
}

bool test_insert_at_middle()
{
    mftype y0 = *mf_median;
    for (int i = 0; i < mfsize + 1; i++)
    {
        //std::cout << "tiam: " << i << ", y0: " << y0 << std::endl;
        mfilter(y0);
        if (not mf_validate(false))
        {
            std::cout << i << ": " << std::endl;
            show_sorted("err-insert-mid:");
            return false;
        }
    }
    //show_sorted("end-mid:");
    return true;   
}

bool test_insert_at_end()
{
    //std::cout << "at-end" << std::endl;
    mftype y0 = 100000;
    mftype y1;
    mf_init(y0);
    int i;
    for (i = 0; i < (mfsize / 2); i++)
    {
        y1 = mfilter(y0 + i);
        assert(y1 == y0);
        if (not mf_validate(true))
        {
            show_sorted("err-end-mid:");
            return false;
        }
    }
    y1 = mfilter(y0 + i);
    assert(y1 == y0);
    if (not mf_validate(true))
    {
        show_sorted("err-end-mid1:");
        return false;
    }
    mf_validate(true);
    i++;
    y1 = mfilter(y0 + i);
    assert(y1 == (y0 + 1));
    if (not mf_validate(true))
    {
        show_sorted("err-end-mid-2:");
        return false;
    }
    i++;
    for (; i < 4 * mfsize + 1; i++)
    {
        y1 =  mfilter(y0 + i);
        //std::cout << "loop-end:" << i << ":" << y1 << std::endl;
        if (not mf_validate(true))
        {
            std::cout << "invalid @ " << i << std::endl;
            show_sorted("inv-end-At-End: ");
            return false;
        }
    }
    //show_sorted("end-At-End: ");
    test_insert_at_middle();
    return true;   
}

bool test_insert_at_front()
{
    mftype y0 = 100000;
    mf_init(y0);
    int i;
    for (i = 0; i < (mfsize / 2); i++)
    {
        mfilter(y0 - i);
        if (not mf_validate(true))
        {
            show_sorted("err-front-early:");
            return false;
        }
    }
    mfilter(y0 - i);
    if (not mf_validate(true))
    {
        show_sorted("err-front-mid:");
        return false;
    }
    i++;
    mfilter(y0 - i);
    if (not mf_validate(true))
    {
        show_sorted("err-front-mid-2:");
        return false;
    }
    i++;
    for (; i < 4 * mfsize + 1; i++)
    {
        mfilter(y0 - i);
        if (not mf_validate(true)) 
        {
            show_sorted("err-front-mid_3: ");
            std::cout << "Invalid @ " << i << std::endl;
            std::cout << "y0 - i @ " << y0 - i << std::endl;
            return false;
        }
    }
    //show_sorted("end-At-front: ");
    if (not test_insert_at_middle())
        return false;
    return true;   
}

bool test_insert_updown()
{
    mftype y0 = 100000;
    mf_init(y0);
    mftype i;
    for (i = 0; i < (y0 / 3); i++)
    {
        if (i & 1)
            mfilter(y0 - i);
        else
            mfilter(y0 + i);
        if (not mf_validate(false))
        {
            std::cout << "\nInvalid @ " << i << std::endl;
            show_sorted("\nerr-updown:");
            return false;
        }
//        if ((i & 0x1ff) == 1)
//            show_sorted("updown-first:");
    }
    for (i = 0; i < (y0 / 3); i++)
    {
        if (i & 1)
            mfilter(y0 + 2 * i);
        else
            mfilter(y0 - 2 * i);
        if (not mf_validate(false))
        {
            std::cout << "\nInvalid @ " << i << std::endl;
            show_sorted("\nerr-updown:");
            return false;
        }
//        if ((i & 0x1ff) == 1)
//            show_sorted("updown-second:");
    }
    //show_sorted("end-At-updown: ");
    if (not test_insert_at_middle())
        return false;
    return true;   
}

# define DEBUG_INSERT_ARROUND 0

bool test_insert_around()
{
    const mftype y0 = 100000;
    mftype min = 100000 - 599;
    mftype max = 100000 + 591;
    mf_init(y0);
    mftype i;
    mftype y1 = y0;
    float factor = 1.0;
    for (i = 0; i < 32 * mfsize; i++)
    {
        for (auto j = i; j < (i + (i >> 1)); j++)
        {
            if (factor > 2.0)
                factor /= 1.5;
            else if (factor < 0.001)
                factor *= 100;
            mftype k = j;
            while (k > 100)
                k /= 3;
            if (j & 1)
            {
                y1 -= k + ((int) (factor * i));
            }
            else
            {
                y1 += k - ((int) (factor * i));
            }
            if (y1 > max)
            {
                mftype dy = max - y1;
                if ((int) dy < 0)
                {
# if DEBUG_INSERT_ARROUND
                    std::cout << "neg dy(" << dy << "), ";
# endif
                    dy = (max - y0) / 3;
# if DEBUG_INSERT_ARROUND
                    std::cout << "new dy(" << dy << "): ";
# endif
                }
                if ((3 * dy) > y1)
                {
# if DEBUG_INSERT_ARROUND
                    std::cout << "dy(" << dy << ") too big: ";
# endif
                    dy /= 10;
                }
                factor *= 0.99;
# if DEBUG_INSERT_ARROUND
                std::cout << "y1(" << y1 << ") > "
                          << "max(" << max << ")"
                          << ", i: " << i
                          << ", j: " << j
                          << ", k: " << k
                          << ", ->y1': " << y1 - (dy * 3)
                          << ", f': " << factor
                          << std::endl;
# endif
                y1 = y1 - (dy * 3);
            }
            else if (y1 < min)
            {
                factor *= 1.01;
                mftype dy = min - y1;
                if (dy > (y0 / 2))
                    dy = y0 - min;
# if DEBUG_INSERT_ARROUND
                std::cout << "y1(" << y1 << ") < "
                          << "min(" << min << ")"
                          << ", i: " << i
                          << ", j: " << j
                          << ", k: " << k
                          << ", ->y1': " << y1 + 3 * dy
                          << ", f': " << factor
                          << std::endl;
# endif
                y1 = y1 + 3 * dy;
            }
            if ((int) y1 < 0)
            {
                std::cout << "Overflow @ " << y1
                          << ", i: " << i
                          << ", j: " << j
                          << std::endl;
                show_sorted("\noverflow-around:");
                return false;
            }
            mfilter(y1);
            if (not mf_validate(false))
            {
# if DEBUG_INSERT_ARROUND
                std::cout << "\nInvalid @ " << i
                          << ", j: " << j
                          << std::endl;
                show_sorted("\nerr-around:");
# endif
                return false;
            }
# if DEBUG_INSERT_ARROUND
            if ((i & 0xf) == 0)
            {
                std::cout << "around  @ " << i
                          << ", j: " << j
                          << std::endl;
                show_sorted("around: ");
            }
# endif
        }
    }
    //show_sorted("end-At-around: ");
    return true;   
}

# define TEST_FIND_POS 1
# define TEST_FRONT 1
# define TEST_END 1
# define TEST_UPDOWN 1
# define TEST_AROUND 1
# define TEST_NEG 1
# define TEST_F_AND_E_ONLY 0

int main(int argc, char** argv)
{
    // this is suppose to give a new random seed each run.
    srand(time(0));
    // Run a seriesof debugging tests and exit if we fail any test.
    // Can en/disable above.
# if TEST_FIND_POS
    if (not test_find_pos())
        return 1;
# endif
# if TEST_FRONT
    if (not test_insert_at_front())
        return 1;
# endif
# if TEST_END
    if (not test_insert_at_end())
        return 1;
# endif
# if TEST_UPDOWN
    if (not test_insert_updown())
        return 1;
# endif
# if TEST_AROUND
    if (not test_insert_around())
        return 1;
# endif
# if TEST_NEG
    if (not test_insert_neg())
        return 1;
# endif
# if TEST_F_AND_E_ONLY
    return 1;
# endif
    // A hack for getting optional arguments.
    if (argc < 2)
    {
        std::cout << "usage: ofile, start, slope, pct-noise, limit, scale" << std::endl;
        return 1;
    }
    int argi = 1;
    std::ofstream csv(argv[argi++]); // create the CSV file output.
    mftype start = 600; // set arg defaults as we go.
    if (argi < argc)
        start = (mftype) std::stoi(argv[argi++]);
    float m = 0.1;
    if (argi < argc)
        m = std::stof(argv[argi++]);
    float r = 0.1;
    if (argi < argc)
         r = std::stof(argv[argi++]);
    int limit = 1024;
    if (argi < argc)
         limit = std::stoi(argv[argi++]);
    float scale = 100;
    if (argi < argc)
         scale = std::stof(argv[argi++]);
    mf_init(scale * start);
    csv << " N,  ln,   Y,    m,   noise,    y + n,    y0,   mfo, ";
# if RECORD_FILTER_VALUES && (MF_SIZE == 15)
    csv << "  fudge, ";
    csv << " smin,  s1,   s2,   s3,    s4,   s5,    s6,   smedian, ";
    csv << "  s8,   s9,   s10,    s11,   s12,    s13,   smax";
# else
# endif
    csv << std::endl;
    float fudge = 0.0;
    float last_y = start;
    int last_i = 0;
    for (int i = 0; i < limit; i++)
    {
        if (fudge != 0.0)
        {
            m = -m;
        }
        float y = last_y + m * last_i + fudge;
        last_y = y;
        last_i++;
        float n = some_noise(r);
        float z = y + n;
        mftype y0 =  (int) ((scale * z) + 0.5);
        mftype y1 = mfilter(y0, false);
        csv << i << ", "
            << last_i << ", "
            << y << ", "
            << m << ", "
            << n << ", "
            << z << ", "
            << y0 << ", "
            << y1 << ", "
# if RECORD_FILTER_VALUES && (MF_SIZE == 15)
            << fudge << ", ";
        for (int l = 0; l < mfsize; l++) {
            csv << mf_sorted[l] << ", ";
        }
# else
            // end line above
            ;
# endif
        csv << std::endl;
        if (fudge != 0.0)
            fudge = 0.0;
        if (not mf_validate())
        {
            std::cout << "NOT SORTED: " << i << std::endl;
            std::cout << "y:" << y << ", z:" << z << ", y0:" << y0 << std::endl;
            std::cout << "y1: " << y1 << ", fudge: " << fudge << std::endl;
            show_sorted("main-not-valid: ");
            return 1;
        }
        if (y > (1.5 * start))
        {
            fudge = -3.0 * (y - (1.5 * start));
            last_i = 1;
            if ((y + fudge) > (1.5 * start))
            {
                std::cout << "failed to adjust: "
                    << y << " > "
                    << (1.5 * start)
                    << std::endl;
                    return 1;
            }
# if DEBUG_FUDGE
            std::cout << "failed to adjust: "
                << y << " > "
                << (1.5 * start)
                << " fudge: " << fudge
                << std::endl;
            //return 1;
# endif
        }
        if (y < (0.5 * start))
        {
            fudge = 3.0 * ((0.5 * start) - y);
            last_i = 1;
# if DEBUG_FUDGE
            std::cout << "failed to adjust: "
                << y << " < "
                << (0.5 * start)
                << " fudge: " << fudge
                << std::endl;
            //return 1
# endif
        }
    }
    show_sorted("end-mfilter: ");

    return 0;
    //  run m.csv 60 0.01 0.01 100
}

