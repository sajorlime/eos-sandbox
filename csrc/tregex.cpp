
#include <string>
#include <iostream>
#include <regex>

using namespace std;

std::regex pvs6model("PVS6[0-9][0-9][0-9][0-9]c");
std::regex pvs6models("PVS6([0-9]){4}c");
std::regex pvs6serial("PVS6[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]c");
std::regex pvs6serials("PVS6([0-9]){8}c");

std::regex pvs6_c("PVS6[0-9]*c");
std::regex pvs6_p("PVS6[0-9]*p");

//std::regex anydev(".*(BATTERY\\.|ESS\\.|HUB\\.).*");
std::regex anydev(".*(BATTERY\\.|ESS\\.|HUB\\.).*");

int main(int argc, char ** argv)
{
    string input;
    smatch pieces;
    input = "PVS61234c";
    cout << input << "wp:c:" << regex_match(input, pieces, pvs6_c) << ":p:" << regex_match(input, pieces, pvs6_p) << "::" << endl;
    cout << input << "np:c:" << regex_match(input, pvs6_c) << ":p:" << regex_match(input, pieces, pvs6_p) << "::" << endl;
    input = "PVS61234p";
    cout << input << ":c:" << regex_match(input, pieces, pvs6_c) << ":p:" << regex_match(input, pieces, pvs6_p) << "::" << endl;
    input = "PVS612344321c";
    cout << input << ":c:" << regex_match(input, pieces, pvs6_c) << ":p:" << regex_match(input, pieces, pvs6_p) << "::" << endl;
    input = "PVS612344321p";
    cout << input << ":c:" << regex_match(input, pieces, pvs6_c) << ":p:" << regex_match(input, pieces, pvs6_p) << "::" << endl;

    std::string inputs[] = {"abcefg BATTERY.xyz", "zmdngy ESS.xyz", "bcefp HUB.xyz", "dafesdf BOGUS.dafd"};
    input = "abcefg BATTERY.xyz";
    for (const auto &inp : inputs) {
        bool match = std::regex_search(inp, pieces, anydev); std::ssub_match sub_match;
        if (match) {
            cout << inp << " -- has match" << endl;
            cout << "prx:" << pieces.prefix() << endl;
            cout << "sfx:" << pieces.suffix() << endl;
            for (size_t i = 0; i < pieces.size(); ++i) {
                sub_match = pieces[i];
                cout << "m:" << i << " =" << sub_match.str() << endl;
            }
        }
        else {
            cout << inp << " -- NO match" << endl;
        }
    }
    return 0;
}

