#include <iostream>
#include <functional>
 
struct ipoint
{
    int x;
    int y;
    int z;
};

class Point
{
private:
    double m_x, m_y, m_z;
    struct ipoint _ip;
 
public:
    Point(double x=0.0, double y=0.0, double z=0.0): m_x(x), m_y(y), m_z(z)
    {
    }
    Point(ipoint& ip): _ip(ip)
    {
        m_x = _ip.x;
        m_y = _ip.y;
        m_z = _ip.z;
    }

    friend std::ostream& operator<< (std::ostream &out, const Point &point);
};
 
std::ostream& operator<< (std::ostream &out, const Point &point)
{
    // Since operator<< is a friend of the Point class, we can access Point's members directly.
    out << "Point(" << point.m_x << ", " << point.m_y << ", " << point.m_z << ")"; // actual output done here
 
    return out; // return std::ostream so we can chain calls to operator<<
}
 
int main()
{
    // lambda
    auto cf = [&](Point& p, const char* "name") -> int {
        std::cout << name << ':' << p << std::endl;
        }
    Point point1(1.0, 3.0, 4.0);
    Point points[] = {{2.0, 3.0, 4.0}, {4.0, 3.0, 2.0}} ;
    ipoint ip = {2, 3, 4};
    ipoint xp = {.x = 3, .y = 5, .z = 6};
    ipoint yp = {.x = 4, .y = 22, .z = 33};
    ipoint zp = {0};
    Point point2(ip);
    Point point3(xp);
    Point point4(yp);
    Point point5(zp);
    int pi = 0;
    int* ppi = &pi;
    auto f1 = cr(point1, "P1");
    auto f2 = cr(point1, "P1");
 
    std::cout << '1' << point1 << std::endl;
    std::cout << '2' << point2 << std::endl;
    std::cout << '3' << point3 << std::endl;
    std::cout << '4' << point4 << std::endl;
    std::cout << '5' << point5 << std::endl;
    std::cout << 'i' << pi << ':' << points[pi] << std::endl;
    std::cout << pi << ':' << points[++*ppi] << std::endl;
    std::cout << pi << ':' << points[pi] << std::endl;
 
    return 0;
}
