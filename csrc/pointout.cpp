#include <iostream>
#include <map>
 
struct ipoint
{
    int x;
    int y;
    int z;
};

class Point
{
private:
    double m_x, m_y, m_z;
    struct ipoint _ip;
 
public:
    Point(double x=0.0, double y=0.0, double z=0.0): m_x(x), m_y(y), m_z(z)
    {
    }
    Point(ipoint& ip): _ip(ip)
    {
        m_x = _ip.x;
        m_y = _ip.y;
        m_z = _ip.z;
    }

    friend std::ostream& operator<< (std::ostream &out, const Point &point);
};

using Vpair = std::pair<std::string, ipoint>;

std::map<std::string, ipoint> _vmap;

 
std::ostream& operator<< (std::ostream &out, const Point &point)
{
    // Since operator<< is a friend of the Point class, we can access Point's members directly.
    out << "Point(" << point.m_x << ", " << point.m_y << ", " << point.m_z << ")"; // actual output done here
 
    return out; // return std::ostream so we can chain calls to operator<<
}

enum T {X, Y, Z};

std::map<T, int> tmap = {
    {T::X, 7},
    {T::Y, 17},
    {T::Z, 27}
};
 
int main()
{
    Point point1(1.0, 3.0, 4.0);
    Point points[] = {{2.0, 3.0, 4.0}, {4.0, 3.0, 2.0}} ;
    ipoint ip = {2, 3, 4};
    ipoint xp = {.x = 3, .y = 5, .z = 6};
    ipoint yp = {.x = 4, .y = 22, .z = 33};
    ipoint zp = {0};
    Point point2(ip);
    Point point3(xp);
    Point point4(yp);
    Point point5(zp);
    int pi = 0;
    int* ppi = &pi;
 
    for (char i = '1'; i < '4'; i++)
        std::cout << (std::string("TEST") + i) << std::endl;
    std::cout << '1' << point1 << std::endl;
    std::cout << '2' << point2 << std::endl;
    std::cout << '3' << point3 << std::endl;
    std::cout << '4' << point4 << std::endl;
    std::cout << '5' << point5 << std::endl;
    std::cout << 'i' << pi << ':' << points[pi] << std::endl;
    std::cout << pi << ':' << points[++*ppi] << std::endl;
    std::cout << pi << ':' << points[pi] << std::endl;

    std::cout << "X:" << tmap[T::X] << std::endl;
    std::cout << "Z:" << tmap[T::Z] << std::endl;
    std::cout << "Y:" << tmap[T::Y] << std::endl;
    std::cout << "M:" << tmap[(T)5] << std::endl;
    std::map<T, int>::iterator it;
    it = tmap.find(T::X);
    if (it != tmap.end())
        std::cout << "Found:" << T::X << it->second << std::endl;
    else
        std::cout << "Not Found:" << T::X << std::endl;
    it = tmap.find((T)6);
    if (it != tmap.end())
        std::cout << "Found:" << (T)6 << std::endl;
    else
        std::cout << "Not Found:" << (T)6 << std::endl;
    const char * t = "test";
    Vpair vp = {"test", {1, 2, 3}};
    std::cout << vp.first << ':' << vp.second.x << std::endl;
    vp = {t, {7, 2, 3}};
    std::cout << vp.first << ':' << vp.second.x << std::endl;
    _vmap.emplace<Vpair>({"a test", {1, 2, 3}});
    _vmap.emplace<Vpair>({"b test", {4, 5, 6}});
    for (auto xp : _vmap) {
        std::cout << xp.first << ':' << xp.second.x << std::endl;
    }
 
    return 0;
}
