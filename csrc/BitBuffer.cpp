/*
// 
// $Id: //Source/Common/libutility/BitBuffer.cpp#8 $ 
// 
// Copyright (c) 2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: BitBuffer.cpp
// 
// Creation Date: 9/18/01
// 
// Last Modified: 
// 
// Author: Emil Rojas
// 
// Description: 
// Implements the notion of a bit buffer that allows arbirtarity bit
// fields to be written sequentually. 
//  
*/


# include "BitBuffer.h"

DEFINE_FILE_STRING("$Id", BitBuffer)

CBitBuffer::CBitBuffer (int byteSize)
    :
      m_pBB (new uint32 [(byteSize + sizeof (uint32) - 1) / sizeof (uint32)]),
      m_bitPos (0),
      m_getPos (0),
      m_bytes ((byteSize + (sizeof (uint32) - 1)) & ~(sizeof (uint32) - 1))
{
}

CBitBuffer::CBitBuffer (uint32 * pBB)
    :
      m_pBB (pBB),
      m_bitPos (0),
      m_getPos (0),
      m_bytes (0)
{
}


uint32 CBitBuffer::getBits (uint32 bits)
{
    uint32 v = readBits (m_getPos, bits);
    m_getPos += bits;
    return v;
}

/*
    writeBits -- write a value of size <= 32 bits to the bit buffer.
    The value v is right justified in v.
    Bits is <= 32.
*/
uint32 CBitBuffer::writeBits (uint32 v, uint32 bits)
{
    if ((int) bits > 0)
        insertBits (v, m_bitPos, bits);
    return m_bitPos += bits;
}

uint32 CBitBuffer::writeZero (uint32 bits)
{
    while (bits > 32)
    {
        writeBits (0, 32);
        bits -= 32;
    }
    return writeBits (0, bits);
}

uint32 CBitBuffer::writeOne (uint32 bits)
{
    while (bits > 32)
    {
        writeBits (0xffffffff, 32);
        bits -= 32;
    }
    return writeBits (0xffffffff, bits);
}

uint32 CBitBuffer::writeBytes (uint8 * pV, uint32 bytes)
{
    for (; bytes > 0; bytes--)
        writeBits ((uint32) *pV++, 8);
    return m_bitPos;
}

uint32 CBitBuffer::writeStrInt (const char * pV, uint32 bits)
{
    return writeBits (intValue (pV), bits);
}

void dumpBuffer (ofstream & fout, CBitBuffer * pBB)
{
    const uint8 * pBits = pBB->buffer ();
    uint32 bits = pBB->bits ();
    fout << "Bits: " << bits
         << "   bytes: " << (bits / 8)
         << "   rem: " << (bits % 8)
         << endl;
    const char bin[] = "01";
    const char hex[] = "0123456789ABCDEF";
    for (int i = 1; (int) bits > 0; bits -= 8, pBits++, i++)
    {
        fout
              << "("
              << hex [(*pBits >> 4) & 0xf]
              << hex [(*pBits >> 0) & 0xf]
              << ") "
              << bin [(*pBits >> 7) & 1]
              << bin [(*pBits >> 6) & 1]
              << bin [(*pBits >> 5) & 1]
              << bin [(*pBits >> 4) & 1]
              << bin [(*pBits >> 3) & 1]
              << bin [(*pBits >> 2) & 1]
              << bin [(*pBits >> 1) & 1]
              << bin [(*pBits >> 0) & 1]
              << ' '
              ;
        if ((i % 4) == 0)
            fout << endl;
    }
    fout << flush;
}



void writeBuffer (ofstream & fout, CBitBuffer * pBB)
{
    //cerr << "writeBuffer of bits " << pBB->bits () << endl;
    fout.write (pBB->buffer (), pBB->bits () / 8);
}




