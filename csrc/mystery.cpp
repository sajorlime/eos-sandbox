
# include <cassert>
# include <iostream>
# include <iomanip>
# include <stdlib.h>

uint32_t fails(uint32_t sb)
{
     return (((sb + 4) & 0xFF) << 0)
             + (((sb + 5) & 0xFF) << 8)
             + (((sb + 6) & 0xFF) << 16)
             + (((sb + 7) & 0xFF) << 24);

#if 0
dataExpected = (((sampleByte + 4) & 0xFF) << 0)
+ (((sampleByte + 6) & 0xFF) << 16)
+ (((sampleByte + 7) & 0xFF) << 24);
#endif
}

uint32_t works(uint32_t sb)
{
     uint32_t dataExpected = (((sb + 4) & 0xFF) << 0);
     dataExpected += (((sb + 5) & 0xFF) << 8);
     dataExpected +=             (((sb + 6) & 0xFF) << 16);
     dataExpected += (((sb + 7) & 0xFF) << 24);
     return dataExpected;
}

using namespace std;

int main(int argc, char**argv)
{
    for (int i = 1; i < argc; i++) {
        int sb = atoi(argv[i]);
        cout << "F:" << i << ':' << argv[i] << ':' << hex << fails(sb) << endl;
        cout << "W:" << i << ':' << argv[i] << ':' << hex << works(sb) << endl;
    }
    for (int i = 1; i < 255; i++) {
        assert(fails(i) == works(i));
    }

    return 0;
}

