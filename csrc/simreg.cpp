


# include <iostream>
# include <iomanip>
# include <cstdlib>




union Reg {
    struct s {
        uint32_t a:10;
        uint32_t b:10;
        uint32_t rem:12;
        s() : a(0x3FF),
              b(0x3FE),
              rem(0)
        {}
    }__attribute__((packed)) s;
    uint32_t u;
    Reg() : s()
    {}
};


using namespace std;

int main(int argc, char** argv)
{
    Reg r;
    cout << "r.s.a: 0x" << hex << std::setw(8) << setfill('0') <<  r.s.a << '\n'
         << "r.s.b: 0x" << hex << std::setw(8) << setfill('0') << r.s.b << '\n'
         << "r.u: 0x" << hex << std::setw(8) << setfill('0') << r.u << '\n'
         << endl;
    r.s.a = 0x3FF;
    r.s.b =  0x3FE;
    cout << "r.s.a: 0x" << hex << std::setw(8) << setfill('0') << r.s.a << '\n'
         << "r.s.b: 0x" << hex << std::setw(8) << setfill('0') << r.s.b << '\n'
         << "r.u: 0x" << hex << std::setw(8) << setfill('0') << r.u << '\n'
         << endl;

}


