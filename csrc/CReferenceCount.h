/*
 * &copyright Copyright (c)1997 BDN Software Inc. (Bruce D. Nilo). 
 * All rights reserved.
 *
 * $Source: /export2/Repositories/bdnswlibs/common/CReferenceCount.h,v $
 *
 * DESCRIPTION: A very simple reference count object to be used as a
 * mixin or base class. I will likely expand its interface
 * overtime. For calling convenience I could make this a template and
 * allow increment to return the derived object whose reference was
 * being incremented. However it irks me that this simple syntactic
 * convenience would result in code bloat. Objects are created with
 * their reference count set to 1.  Delete is called immediately when
 * an objects reference count goes to zero. (This is a limitation of
 * this implementation.) Because of this be careful that all
 * references to an allocated object are in existence before it is
 * decremented.
 *
 * LIMITATIONS: It would be better if these "freed" object were placed
 * into a container to be collected at specifiable point in the
 * programs execution.
 *
 * TO DO:
 *
 * USAGE:
 * */

#ifndef _CREFERENCECOUNT_H__
#define _CREFERENCECOUNT_H__

class CReferenceCountProtocol {
public:
  virtual void decrement() = 0 ;
  virtual void increment() = 0 ;
} ;

class CReferenceCount : public CReferenceCountProtocol {

private:
  int refcnt ;

protected:
  CReferenceCount() { refcnt = 1 ; }
  virtual ~CReferenceCount() {}

public:
  
  virtual void decrement() {
    refcnt-- ;
    if(refcnt <= 0)
      delete this ;
  }

  virtual void increment() {
    refcnt++ ;
  }


} ;

#endif
