// mtmd_to_fp.cc : This file contains the 'main' function. 
// Program execution begins and ends there.
//

// Converts MTMD ADC data to freqPlan input format

// MTM-D output data (unsigned samples)
// -----------------------------------------------
// | 24-bit  | 10-bit | 10-bit | 10-bit | 10-bit |
//------------------------------------------------ 
// | Counter | i0     | q0     | i1     | q1     |
// ----------------------------------------------- 

// freqPlan input data (signed)
// -----------------------------------------------
// | 16-bit | 16-bit |
//------------------------------------------------ 
// | i0     | q0     | --> sample 0 (word 0)   
// | i1     | q1     | --> sample 1 (word 1) 
// ----------------------------------------------- 

// Note : The input is biased (+512) 
//        Output is zero mean (subtract 512 from the input)    


#include <iostream>
#include <fstream>
#include <string>
#include <sys/stat.h>
#include <filesystem>

using namespace std;

namespace fs = std::filesystem;

#define PRINT_LOSS_LOC 0
#define LOG_LOSS_LOC 1

struct MtmdOut {
    uint64_t q1 : 10;
    uint64_t i1 : 10;
    uint64_t q0 : 10;
    uint64_t i0 : 10;
    uint64_t counter : 24;
};

struct freqPlanIn {
    int16_t i;
    int16_t q;
};

string remove_extension(const string& filename)
{
    size_t lastdot = filename.find_last_of(".");
    if (lastdot == string::npos)
        return filename;
    return filename.substr(0, lastdot);
}

int main(int argc, char* argv[])
{
    if ((argc < 2) || (string(argv[1]) == "-h") || (argv[1] == string("--help"))) {
        cout << " USAGE: mtmd_to_fp inputFile [outputFile]" << endl;
        cout << " NOTE1: Default output file is \"inputFile\"-fP.bin" << endl;
        cout << " NOTE2: Sample loss log file is \"inputFile\"-loss.csv" << endl;
        return -1;
    }

    const string iFileName = argv[1];
    string oFileName;
    if (argc == 3) {
        oFileName = argv[2];
    }
        
    ifstream inFile;
    inFile.exceptions(ifstream::failbit | ifstream::badbit);
    inFile.open(iFileName, ios::binary);

    ofstream outFile;

    uint64_t inpSize = fs::file_size(iFileName);
    if ((inpSize % 8) != 0){
        cout << "Last entry incomplete" << endl;
        return -2;
    }
    char* data = new char[inpSize];
    char* dp = data;
    inFile.read(data, inpSize);

    MtmdOut* ivp = reinterpret_cast<MtmdOut*>(data);
    int64_t prev_counter = (int) ivp->counter;
        
    uint64_t missed_samples = 0;

    const int64_t samples = inpSize / 8;

    ivp += 8;
    for (int64_t k = 0; k < samples - 1; k++, dp += 8) {

        ivp = reinterpret_cast<MtmdOut*>(dp);
        if (ivp->counter != (prev_counter + 2)) {
            missed_samples++;
        }
        prev_counter = ivp->counter;
    }

    cout << "missed_samples:" << missed_samples << " / " << 2 * samples << endl;
    cout << "% missed_samples:" << (100.0 *  missed_samples / (2 * samples)) << endl;
    inFile.close();
}

