﻿/*
 * From google Bard
    This implementation uses a sorted tree structure to store the elements.
    The sorted tree is a binary tree,
        with each node containing a value and two children,
        a left child and a right child.
    The elements are inserted into the sorted tree by starting
        at the root and then traversing down the tree
        until the element's value is found.
    If the element's value is not found,
        a new node is created and inserted into the tree
        at the appropriate location.

    The median of the elements in the sorted tree is
        found by starting at the root and traversing down the tree
        until the element with the median value is found.
        The median value is then returned.
*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

// A sorted tree structure
class SortedTree
{
public:
    // Create a sorted tree structure
    SortedTree() {
    }

    // Insert an element into the sorted tree
    void insert(int element) {
        // Create a new node to store the element
        auto node = new Node(element);

        // Insert the node into the sorted tree
        node->insert(root);
    }

    // Remove an element from the sorted tree
    void remove(int element) {
        // Find the node that contains the element
        Node* node = find(element);

        // Remove the node from the sorted tree
        node.remove();
    }

    // Find the median of the elements in the sorted tree
    int median() {
        // Find the root of the sorted tree
        root = find(median());

        // Return the median
        return root.value;
    }

private:
    // A node in the sorted tree
    struct Node
    {
        // The element stored in the node
        int value;

        // The left and right children of the node
        Node* left;
        Node* right;

        Node(int i)
        {
            this->value = i;
            this->left = this->right = (Node*) 0;
        }

        // Insert the node into the sorted tree
        void insert(Node* parent) {
            // If the node is empty,
            if (this == (Node*) 0) {
                // Make the parent the root of the tree
                parent = this;
                return;
            }

            // If the element is greater than the parent's element,
            if (value > parent->value) {
                // Insert the node as the right child of the parent
                parent->right = this;
            } else {
                // Insert the node as the left child of the parent
                parent->left = this;
            }
        }

        // Remove the node from the sorted tree
        void remove() {
            // If the node has no children,
            if (left == (Node*) 0 && right == (Node*) 0) {
                // Delete the node
                delete this;
                return;
            }

            // If the node has only one child,
            if (left == NULL || right == NULL) {
                // Remove the node and make its child the root of the tree
                if (left != NULL) {
                    root = left;
                } else {
                    root = right;
                }
            }

            // Otherwise,
            else {
                // Remove the node and merge its children
                left->right = right;
                right->left = left;
                delete this;
            }
        }

        // Find the node that contains the element
        Node* find(int element)
        {
            // Iterate through the tree
            if (element == root->value)
                return root;
            if (element < root->value)
            if (this->left && this->left->value < element)
            {
                if (root->left->value < element)
                    return root->left->find(element);
                else
                    return root->left->find(element);
                if (element < root->value)
                {
                    return root->left->find(element);
                }
                // else
                return root->left->find(element);
            }
            // else
            return root->left->find(element);
            return root;
        }
};

private:
    // The root of the sorted tree
    SortedTree::Node* root;
};

int main() {
    // Create a sorted tree structure
    SortedTree tree;

    // Insert some elements into the sorted tree
    tree.insert(10);
    tree.insert(5);
    tree.insert(2);
    tree.insert(7);

    // Remove some elements from the sorted tree
    tree.remove(5);

    // Print the median of the elements in the sorted tree
    cout << "The median is " << tree.median() << endl;
}



