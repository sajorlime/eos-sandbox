
#include <iostream>

class MyS {
public:
    int _x;
    int _y;
    MyS(int x, int y) : _x(x), _y(y)
    { }
    MyS() : _x(0), _y(0) { }
};

struct Id { Id() {};};

template<typename S>
struct Struct : public MyS {
    Struct(int x, int y) : MyS(x, y)
    { }
    Struct() : MyS() { }
    S* s;
};

struct Z {
    int _z;
    Z(int a) : _z(a) {}
};

class StructId : public Struct<Id> {
public:
    StructId() : Struct<Id>()
    { }
};

class StructZ : public Struct<Z> {
public:
    StructZ(int a)
        : Struct<Z>(a, a)
    { }
};

StructZ _sz = {3};

StructId _i;



int main ()
{
    //_sz._x = 13;
    //_sz._y = 17 ;
    _i._x = 19;
    _i._y = 23;

    std::cout << "SX:" << _sz._x << ':' << _sz._y << std::endl;
    std::cout << "SI:" << _i._x << ':' << _i._y << std::endl;
    std::cout << "size Z:" << sizeof(_sz) << std::endl;
    std::cout << "size I:" << sizeof(_i) << std::endl;
}

