/*
// 
// Copyright (c) 1999-2000 Dotcast Inc. 
// All rights reserved. 
// 
// Filename: Tracer.cpp
// 
// Creation Date: 1/19/00
// 
// Last Modified: 
// 
// Author: Prakash Manghnani
// 
// Description: 
//	Class for providing common tracing functionality. See Tracer.h for usage/description.
// 
//  
*/ 

// This is for Tracer.h
////#define _NOTRACING

# include "dotcastns.h"
# include "dctype.h"

#if defined(__linux__) || defined(__FreeBSD__)
#define _GNU_SOURCE
#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#else
#ifdef _WIN32
#include <wtypes.h>
#include <native.h>
#include <winbase.h>
#include <windows.h>
#define vsnprintf _vsnprintf
#endif
#endif

DEFINE_FILE_STRING("$Id", Tracer);

#include "Tracer.h"

#include <string>
#include <map>

using namespace std;

__BEGIN_DC_NAME_SPACE

//
// Class for stream access - only used by CTraceOutputStream
class CTraceOutputStreamBuf : public streambuf {
protected:
	CTraceOutputStreamBuf() {
		cp = 0;
#ifndef _NOTRACING
		bOutputOn = true;
#else
		bOutputOn = false;
#endif
	}
	void setOutputOn(bool b) {
#ifndef _NOTRACING
		bOutputOn = b;
#endif
	}
	virtual int overflow(int c) {
		if (!bOutputOn) return c;			// ignore
		if (c != EOF) {
			if ((int) (cp + 1) >= (int) (sizeof(buf)-1)) {		// check for overflow
				flush();
			}
			buf[cp++] = c;
			if (c == '\n') flush();
		}
		return c;
	}

	virtual std::streamsize xsputn(const char *s, std::streamsize n) {
		if (!bOutputOn) return n;			// ignore
		if ((int) (cp + n) >= (int) (sizeof(buf)-1)) {		// check for overflow
			flush();
		}
		memcpy(&(buf[cp]), s, n);
		cp += n;
		flush();
		return n;
	}

	CTraceOutputStreamBuf& flush() {
		if (cp == 0) return(*this);		// No chars to flush
		buf[cp] = 0;				// Null terminate
		CTracer::getOutputDevice()->output(buf);
		cp = 0;			// reset write pointer
		return (*this);
	}
private:
	char buf[512];
	int cp;			// Cur write pos (in buf)
	bool bOutputOn;
	friend class CTraceOutputStream;
};

class CTraceOutputStream : public ostream {
public:
	CTraceOutputStream() : ostream(&m_streamBuf) {
	}
	void setOutputOn(bool b) {
		m_streamBuf.setOutputOn(b);
	}
	void flush() {
		m_streamBuf.flush();
	}
		
private:
	CTraceOutputStreamBuf m_streamBuf;
};

CTraceOutputStream sOutputStream;		// Single instance of CTraceOutputStream

//
// CTraceOutput subclasses:
// These are predefined concrete instances of CTraceOutput.


//
// Class to send output to nowhere
class CNullTraceOutput : public CTraceOutput
{
public:
	void output(const char *s) {}
};

//
// Class to send output to default - for console app, sent to stdout, for
// windows app, output is sent to debug window.
class CDefaultTraceOutput : public CTraceOutput
{
public:
	void output(const char *s) {
#ifdef WIN32
#ifdef _WINDOWS
 #define __USEWINDOWSTRACE
#endif // _WINDOWS
#endif // WIN32

#ifdef __USEWINDOWSTRACE
		OutputDebugString(s);
#else
		fputs(s, stdout);
#endif
	}
};

//
// Class to send output to a file
class CFileTraceOutput : public CTraceOutput
{
public:
	CFileTraceOutput() {
		fp = NULL;
	}
	void output(const char *s) {
		if (fp != NULL) {
			fputs(s, fp);
			fflush(fp);
		}
	}
	void setOutputFile(const char *fileName) {
		if (fp != NULL) {
			// Close old file
			fclose(fp);
		}
		fp = fopen(fileName, "a");
	}
private:
	FILE *fp;
};

// Instances of intrinsically supported output devices.
// Note: Caller can define his own, by subclassing CTraceOutput, and calling
// CTracer::setOutputDevice()
static CNullTraceOutput sNullOutput;
static CDefaultTraceOutput sDefaultOutput;
static CFileTraceOutput sFileOutput;

//
//	// Map containing - subSystem->traceLevel mapping.
typedef std::map<std::string, int> TraceLevelMap;
static TraceLevelMap m_traceLevels;

//
// CTracer class methods

const char defaultSubSystem[] = "DefaultSubsystem";
const int CTracer::defaultTraceLevel = 2;

CTracer CTracer::sInstance;

// CTracerMutex used to protect access to internal static data structures
#if defined(__linux__) || defined(__FreeBSD__)
class CTracerMutex {
public:
	static CTracerMutex* pMutex;

	CTracerMutex (bool initiallyOwn, const char * pName = 0) {
		m_pMutex = new pthread_mutex_t;
		if (pthread_mutex_init(m_pMutex, 0)) {
			delete m_pMutex;
		}
		if (initiallyOwn) {
			if (pthread_mutex_lock(m_pMutex)) {
				pthread_mutex_destroy(m_pMutex);
				delete m_pMutex;
			}
		}
	}

	virtual bool take () {
		if (pthread_mutex_lock(m_pMutex))
			return false;
		else
			return true;
	}
	
	virtual bool release (void) {
		if (pthread_mutex_unlock(m_pMutex))
			return false;
		else
			return true;
	}
  
	virtual ~CTracerMutex ()
    {
		pthread_mutex_destroy(m_pMutex);
		delete m_pMutex;
    }
private:
	pthread_mutex_t* m_pMutex;
};
#else
#define INFINITE_TIMEOUT (-1)
class CTracerMutex {
public:
    CTracerMutex (bool initiallyOwn, const char * pName = 0)
    {
        m_h = ::CreateMutex (0, initiallyOwn, pName);
    }

	virtual bool take ()
    {
        DWORD r = ::WaitForSingleObject (m_h, INFINITE_TIMEOUT);
        if (r == WAIT_OBJECT_0)
            return true;
        return false;
    }

	virtual bool release (void)
    {
        if (::ReleaseMutex (m_h))
            return true;
        return false;
    }

	virtual ~CTracerMutex ()
    {
        if (m_h != 0)
            ::CloseHandle (m_h);
    }

	static CTracerMutex* pMutex;

private:
    HANDLE m_h;
};
#endif

CTracerMutex* CTracerMutex::pMutex = new CTracerMutex(false);

CTracer::CTracer()
{
	m_pOutputDev = &sDefaultOutput;		// Output to default device
}


int CTracer::getTraceLevel(const char *subSystem)
{
	int tracelevel;
	//
	// 1) Search for subSystem in map
	CTracerMutex::pMutex->take();
	TraceLevelMap::iterator it = m_traceLevels.find(subSystem);
	if (it == m_traceLevels.end()) {
		// Not found - insert new subSystem into map
		m_traceLevels.insert(TraceLevelMap::value_type (subSystem, defaultTraceLevel));
		tracelevel = defaultTraceLevel;
	}
	else {
		tracelevel = (*it).second;
	}
	CTracerMutex::pMutex->release();
	return tracelevel;
}

void CTracer::setTraceLevel(const char *subSystem, int level)
{
	CTracerMutex::pMutex->take();
	//
	// If value already in map - erase it
	TraceLevelMap::iterator it = m_traceLevels.find(subSystem);
	if (it != m_traceLevels.end()) {
		m_traceLevels.erase(it);
	}
	//
	// Just insert new value - will overwrite old
	m_traceLevels.insert(TraceLevelMap::value_type (subSystem, level));
	CTracerMutex::pMutex->release();
}

ostream &CTracer::privateStream(const char *subSystem, int traceLevel)
{
	sOutputStream.setOutputOn(getTraceLevel(subSystem) >= traceLevel);
	return sOutputStream;
}

ostream &CTracer::privateNullStream()
{
	sOutputStream.setOutputOn(false);
	return sOutputStream;
}

//
// pprintf... all inlined if _NOTRACING is set
#ifndef _NOTRACING

const int bufsize = 4096;

void CTracer::va_pprintf(const char *subSystem, int traceLevel, const char *fmt, va_list args)
{
	if (getTraceLevel(subSystem) < traceLevel) return;

	int nBuf;
	char szBuffer[bufsize];

#if 0
	nBuf = vsnprintf(szBuffer, bufsize, fmt, args);
#else
	nBuf = vsprintf(szBuffer, fmt, args);
#endif

	if (nBuf <= 0) {
		// Error in vsnprintf	???
		CTracer::sInstance.output("...\n");
	}
	else {
		CTracer::sInstance.output(szBuffer);
	}
}

void CTracer::va_dprintf(const char *fmt, va_list args)
{
	if (getTraceLevel(defaultSubSystem) < defaultTraceLevel) return;

	int nBuf;
	char szBuffer[bufsize];

#if 0
	nBuf = vsnprintf(szBuffer, bufsize, fmt, args);
#else
	nBuf = vsprintf(szBuffer, fmt, args);
#endif

	if (nBuf <= 0) {
		// Error in vsnprintf	???
		CTracer::sInstance.output("...");
	}
	else {
		CTracer::sInstance.output(szBuffer);
	}
}

void CTracer::privatePrintLastError(const char *label, int errNo)
{
	CTracer::pprintf(defaultSubSystem, 0, "%s : %d, %s\n", label, errNo, strerror(errNo));
}

void CTracer::output(const char *s)
{
	sOutputStream.flush();			// To keep ostream output & pprintf in sync
	m_pOutputDev->output(s);
}

#else // _NOTRACING
// These seemingly useless methods are here so clients who compiled without
// NOTRACING can still link against this library
void CTracer::va_pprintf(const char *subSystem, int traceLevel, const char *fmt, va_list args) { }
void CTracer::va_dprintf(const char *fmt, va_list args) { }
void CTracer::privatePrintLastError(const char *label, int errNo) { }
void CTracer::output(const char *s) { }
#endif //_NOTRACING

void CTracer::setOutputStdout()
{
	setOutputDevice(&sDefaultOutput);
}

void CTracer::setOutputFile(const char *file)
{
	sFileOutput.setOutputFile(file);
	setOutputDevice(&sFileOutput);
}

void CTracer::setOutputNone()
{
	setOutputDevice(&sNullOutput);
}

void CTracer::setOutputDevice(CTraceOutput *pOutputDev)
{
	sInstance.m_pOutputDev = pOutputDev;
}

//
// The next two functions are one of many ways to convert
// between a char string and an enum, for the purpose of doing
// i/o on the enum. They do not assume continuity in the enum,
// but are fragile in relying on the struct in two ways:
//
//    * The struct is assumed to be incomplete, in that it must
//      have an entry for every element in the enum
//    * The last entry in the struct must have an m_pName field
//      of 0.
//

struct traceLevelDecode {
  CTracer::traceLevels m_level;
  char * m_pName;
} traceNames[] = { { CTracer::et_fatal, "fatal" },
		   { CTracer::et_alert, "alert" },
		   { CTracer::et_critical, "critical" },
		   { CTracer::et_error, "error" },
		   { CTracer::et_warning, "warning" },
		   { CTracer::et_notice, "notice" },
		   { CTracer::et_info, "info" },
		   { CTracer::et_debug, "debug" },
		   { CTracer::et_verbose, "verbose" },
		   { CTracer::et_lastTraceLevel, 0 }
};


enum CTracer::traceLevels CTracer::traceLevelOf(const char *const pName)
{
  int i;
  for (i = 0; traceNames[i].m_pName; i++)
    if (strcmp(pName, traceNames[i].m_pName) == 0)
      break;
  return traceNames[i].m_level;
}

const char * const CTracer::traceLevelName(CTracer::traceLevels e)
{
  int i;
  for (i = 0; traceNames[i].m_pName; i++)
    if (e == traceNames[i].m_level)
      return traceNames[i].m_pName;
  return 0;
}


void setTraceLevel (const char * pSubSystem, const char * pLevel)
{
    CTracer::traceLevels level = CTracer::traceLevelOf (pLevel);
    if (level == CTracer::et_lastTraceLevel)
        level =  CTracer::et_error;
    CTracer::setTraceLevel (pSubSystem, level);
}

void setTraceLevel (const char * pSubSystemAndLevel)
{
    char subSystem[64];
    char * pLevel;
    strncpy (subSystem, pSubSystemAndLevel, sizeof (subSystem));
    pLevel = strchr (subSystem, ':');
    if (pLevel)
    {
        *pLevel++ = 0;
    }
    else
    {
        pLevel = "error";
    }
    setTraceLevel (subSystem, pLevel);
}


__END_DC_NAME_SPACE

istream& operator>>(istream& s, dotcast::CTracer::traceLevels& e)
{
  string input;
  s >> input;
  e = dotcast::CTracer::traceLevelOf(input.c_str());
  return s;
}

ostream& operator<<(ostream& o, const dotcast::CTracer::traceLevels& e)
{
  o << dotcast::CTracer::traceLevelName(e);
  return o;
}
