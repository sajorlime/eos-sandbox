/*
//
// $Id$
//
// Copyright (c) 1999-2000 Dotcast Inc.
// All rights reserved.
//
// Filename: argsget.c
//
// Creation Date: 12-07-99
//
// Last Modified: 12-07-99
//
// Author: emil
//
// Description: brought in from ICTV code release for public domain.
//
// 
*/


# include <string.h>
# include <cstdio>
# include <cstdlib>
# include <cstdint>
# include <ctype.h>


/* Original header
**
** Project: Utility Library
**
** Permission to copy given to and distribute give to Lapel Software 1997.
**
** File: argsget.c
**
** Author: emil
**
** Purpose:
**  Code to get the command line options according to a string.
**
** Class/Typedefs:
**
** Functions:
**  arg_OptGet -- gets an option that matches the string an option.
**
**  arg_OptRemove -- removes all remaining option from the arg list,
**  and copy them (optionally) to a string.
**
**  arg_OptAdjust -- compresses the options after OptGet, OptRemove
**  and Next have depleted the arg list.
**
**  arg_Next -- get the next argument nulls string in the list.
**
**  arg_Last -- get the last argument and nulls string in the list.
**
**  arg_Copy -- copies agument list to string.
**
** Staic Functions:
**  kmatch -- return the length of a beging match or a key string.
**
** Notes:
**
**
*/

# define NO_ARGSGET_INLINE
# include "argsget.h"

# define __ARGSGET_C__
/*
** Return the length of the match a the begining of pKey and pSS.
*/
static int kmatch (char * pKey, char * pSS)
{
    int len;
    int i;
    
    if (!(pKey && pSS))
        return 0;
    if (!(*pKey & *pSS))
        return 0;
    i = len = strlen (pKey);
    while (i-- > 0)
        if (*pKey++ != *pSS++)
            return 0;
    return len;
}


/*
**  arg_OptGet -- gets an option that matches the string an option.
**  
**  The option will retrieve Value if either of the two positibilites exist
        -oValue
        -o Value
    
    If copy is true the value is copied into the pOpt location.
*/
char * arg_OptGet (int argc, char ** argv, char * pOpt, int copy)
{
    int i;
    int j;
    int len;
    char b;

    for (i = 1; i < argc; i++)
    {
        j = i;
        if ((len = kmatch (pOpt, argv[i])) != 0)
        {
            if (argv[i][len] != 0)
            {
                if (copy > 0)
                {
                    strncpy (pOpt, &argv[i++][len], copy);
                    pOpt[copy-1] = 0;
                    argv[j] = 0;
                }
                else
                    pOpt = &argv[i][len];
            }
            else
            {
                if (argv[i + 1] && (argv[i + 1][0] != '-'))
                {
                    if (copy > 0)
                    {
                        /*
                            copy the value.
                        */
                        strncpy (pOpt, argv[++i], copy);
                        argv[j] = 0;
                        pOpt[copy-1] = 0;
                        argv[i++] = 0;
                    }
                    else
                    {
                        /*
                            In this case there is nothing to copy, but we want to
                            return the value.
                        */
                        pOpt = argv[i + 1];
                    }
                }
                else
                {
                    /*
                        if copy then indicate that value was missing.
                    */
                    if (copy)
                        *pOpt = 0;
                    argv[i] = 0;
                }
            }
            return pOpt;
        }
    }
    if (!copy) /* check for boolean flags in a list of boolean options */
    {
        if ((j = strlen (pOpt)) <= 2) /* it is a single character option */
        {
            b = pOpt[j - 1];
            for (i = 1; i < argc; i++)
            {
                if (argv[i] && (argv[i][0] == '-'))
                {
                    if (strchr (argv[i], b))
                        return pOpt;
                }
            }
        }
    }
    return (char *) 0;
}

/*
** arg_OptRemove -- removes the any remainging options from the
** arg list, copying them to the optList string if it is not null.
*/
int arg_OptRemove (int argc, char ** argv, char * optList)
{
    int i;
    int cnt;

    if (optList)
        *optList = 0;
    for (cnt = 0, i = 1; i < argc; i++)
    {
        if (argv[i] && (argv[i][0] == '-'))
        {

            if (optList)
                strcat (optList, argv[i]);
            argv[i] = 0;
            cnt++;
        }
    }
    return cnt;
}


/*
** arg_OptAdjust -- compresses, moves them forward, the options after
** OptGet, OptRemove
*/
int arg_OptAdjust (int argc, char ** argv)
{
    int i;
    int j;
    int cnt;

    /* init cnt, and find first hole */
    for (cnt = i = 1; i < argc; i++)
    {
        if (argv[i])
            cnt++;
        else
            break;
    }
    /* for the rest of the list copy up,
    ** if we find more holes skip them */ 
    for (j = i++; i < argc; i++)
    {
        if (argv[i])
        {
            argv[j++] = argv[i];
            cnt++;
        }
    }
    return cnt;
}




/*
**  arg_Next -- get the next none null string in the list.
*/
char * arg_Next (int argc, char ** argv)
{
    int i;
    char * cp;

    for (i = 1; i < argc; i++)
    {
        if ((cp = argv[i]) != 0)
        {
            argv[i] = 0;
            return cp;
        }
    }
    return (char *) 0;
}




/*
** arg_Last -- get the last argument and null string in the list.
*/
char * arg_Last (int argc, char ** argv)
{
    int i;
    char * cp;

    for (i = argc - 1; i > 0; i--)
    {
        if ((cp = argv[i]) != 0)
        {
            argv[i] = 0;
            return cp;
        }
    }
    return (char *) 0;
}

/*
** arg_Position -- returns the arugument in positon (pos).
*/
char * arg_Position (int pos, char ** argv)
{
    char * cp;

    if ((cp = argv[pos]) != 0)
    {
        argv[pos] = 0;
    }
    return cp;
}



/*
** arg_Copy -- makes a copy of the arguments into a single string
*/
char * arg_Copy (int argc, char ** argv, char * pCopy, int n)
{
    int i;
    int len;

    if (!pCopy)
    {
        return (char *) 0;
    }
    *pCopy = 0;
    n--;
    for (i = 0, len = 0; i < argc; i++)
    {
        if (argv[i])
        {
            len += strlen (argv[i]) + 1;
            if (len <= n)
            {
                /* this is expensive but who cares (AYLWIN?) */
                strcat (pCopy, argv[i]);
                strcat (pCopy, " ");
            }
        }
    }
    return pCopy;
}



char * arg_getIValue (int argc, char ** argv, const char * optp, uint32_t * piDest)
{
    static char tempStr[64];
    char * cp;

    strcpy (tempStr, optp);
    if ((cp = arg_OptGet(argc, argv, tempStr, sizeof (tempStr))))
    {
        if (piDest)
        {
            *piDest = (uint32_t) strtol (tempStr, 0, 0);
        }
    }
    return cp;
}

char * arg_getBValue (int argc, char ** argv, const char * optp, bool * dbp)
{
    char * cp;

    if ( (cp = arg_OptGet (argc, argv, (char *) optp, 0)) )
    {
        if (dbp)
            *dbp = true;
    }
    return cp;
}

char* strdup(const char* s)
{
    auto l = strlen(s);
    auto *r = (char*) new char*[l];
    return r;
}

Arguments::Arguments (int argc, char** argv, bool dup)
{
    // for now ignore the free pointer, we just loose the data
    m_free = 0;
    if (dup == true)
    {
        char** pArgs = (char**) new char *[argc];
        for (int i = 0; i < argc; i++)
        {
            pArgs[i] = strdup(argv[i]);
        }
        argv = pArgs;
    }
    m_argc = argc;
    m_argv = argv;
}

Arguments::Arguments (char * pCommand)
{
    int spaces;
    char * pStr;

    for (spaces = 0, pStr = pCommand; *pStr != 0; pStr++)
    {
        pStr = strchr (pStr, ' ');
    }
    m_argv = (char **) new char * [spaces];
    for (int i = 0; i < spaces; i++)
    {
        pStr = strchr (pStr, ' ');
        if (pStr)
            pStr++;
    }
    m_argc = spaces - 1;
}

Arguments::~Arguments ()
{
#if 0
    if (m_free)
        delete m_free;
#endif
}

    


