
/* 
// 
// $Id$ 
// 
// Filename: exception.cpp
// 
// Creation Date: 11/19/99
// 
// Last Modified: 
// 
// Author: Emil P. Rojas
// 
// Description: 
// 
//  
*/

# include "dotcastns.h"

DEFINE_FILE_STRING("$Id", dexception);

# include "exception.h"
# include "stdarg.h"
# include "stdio.h"

enum {buffSize = 1024};

XException::XException (const char * pReasonFormat, ...)
{
	va_list args;
	va_start (args, pReasonFormat);

	int nBuf;
	char * pBuffer = new char[buffSize];

	nBuf = vsprintf (pBuffer, pReasonFormat, args);
    m_pReason = pBuffer;

	va_end(args);
	//TRACE("%s", pBuffer);
}


XException::~XException ()
{
    if (m_pReason)
        delete (char *) m_pReason;
}



