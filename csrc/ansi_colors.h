﻿#ifndef ANSI_COLORS_H
#define ANSI_COLORS_H

#define ANSI_COLOR_BLACK "\u001b[30m"
#define ANSI_COLOR_RED "\u001b[31m"
#define ANSI_COLOR_GREEN "\u001b[32m"
#define ANSI_COLOR_YELLOW "\u001b[33m"
#define ANSI_COLOR_BLUE "\u001b[34m"
#define ANSI_COLOR_MAGENTA "\u001b[35m"
#define ANSI_COLOR_CYAN "\u001b[36m"
#define ANSI_COLOR_WHITE "\u001b[37m"
#define ANSI_COLOR_BRIGHT_BLACK "\u001b[90m"
#define ANSI_COLOR_BRIGHT_RED "\u001b[91m"
#define ANSI_COLOR_BRIGHT_GREEN "\u001b[92m"
#define ANSI_COLOR_BRIGHT_YELLOW "\u001b[93m"
#define ANSI_COLOR_BRIGHT_BLUE "\u001b[94m"
#define ANSI_COLOR_BRIGHT_MAGENTA "\u001b[95m"
#define ANSI_COLOR_BRIGHT_CYAN "\u001b[96m"
#define ANSI_COLOR_BRIGHT_WHITE "\u001b[97m"

#define ANSI_RESET "\u001b[0m"
#define ANSI_COLOR_MAP(color) \
  { \
    color, "\u001b["#color"m" \
  }

#endif
