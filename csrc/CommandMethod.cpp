
#ifdef _WIN32
#pragma warning (disable : 4786)
#endif //_WIN32

#include "CommandMethod.h"

# include <map>
# include <string>

__BEGIN_DC_NAME_SPACE

typedef std::map<std::string, PCommandMethodHandler*> CommandInfoMap;
typedef CommandInfoMap::const_iterator CommandInfoIterator;

class CCommandManager : public PCommandManager
{
public:	
	CCommandManager () {}
	virtual ~CCommandManager () {}

	virtual bool processCommand (const char* commandName, int id, void* sender, CommandArgument* args, size_t argc, CommandArgument& retval);
	virtual void registerCommandForObject (const char* commandName, PCommandMethodHandler* handler);

private:
	CommandInfoMap m_map;

	CCommandManager (CCommandManager& rhs);
	CCommandManager& operator=(CCommandManager& rhs);
};

PCommandManager* PCommandManager::CreateCommandManager() {
	return new CCommandManager();
}

bool CCommandManager::processCommand (const char* commandName, int id, void* sender, CommandArgument* args, size_t argc, CommandArgument& retval)
{
	// Look up in map. If there, call, and return true, setting retval to what it returns.
	// If not there, return false
	CommandInfoIterator iterator = m_map.find(commandName);
	if (iterator != m_map.end()) {
		PCommandMethodHandler* cmh = iterator->second;
		retval = cmh->handleCommand(commandName, id, sender, args, argc);
		return true;
	}
	else {
		return false;
	}
}

void CCommandManager::registerCommandForObject (const char* commandName, PCommandMethodHandler* handler)
{
	if (handler == 0) {
		m_map.erase(commandName);
	}
	else {
		m_map[commandName] = handler;
	}
}

const char* ConvertArgumentTypeToString (CommandArgument::Type type) {
	const char* retval;
	switch (type) {
	case CommandArgument::Int:
		retval = "int";
		break;
	case CommandArgument::Double:
		retval = "double";
		break;
	case CommandArgument::Bool:
		retval = "bool";
		break;
	case CommandArgument::String:
		retval = "string";
		break;
	default:
		retval = "unknown";
	}
	return retval;
}

__END_DC_NAME_SPACE

