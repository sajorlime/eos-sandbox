
/*
 * File: heap.cpp
 *
 * Implementation of a smalll heap for keeping a shorted list.
 * See median.cpp
 *
 */


# include <algorithm>
# include <array>
# include <cstdlib>
# include <iostream>
# include <fstream>
# include <iostream>
# include <random>
# include <string>
# include <stdio.h>
 
void siftUp(uint16_t* a, uint8_t start, uint8_t end)
{
    // start represents the limit of how far up the heap to sift.
    // end is the node to sift up.
    uint8_t child = end ;
    while (child > start)
    {
        uint8_t parent = (child - 1) / 2;
        if (a[parent] < a[child])
        { // then (out of max-heap order) so swap
            uint16_t tmp = a[parent];
            a[parent] = a[child];
            a[child] = tmp;
            child = parent; // repeat to continue sifting up the parent now
            continue;
        }
        return;
    }
}

void heapify(uint16_t* a, const uint8_t count)
{
    //(end is assigned the index of the first (left) child of the root)
    uint8_t end = 1;

    while (end < count)
    {
        // sift up the node at index end to the proper place such
        // that all nodes above the end index are in heap order)
        siftUp(a, 0, end);
        end = end + 1;
        // after sifting up the last node all nodes are in heap order
    }
}

void ocol(std::ostream& ofs, float x, int z)
{
    ofs << x << ", ";
}

void ocol(std::ostream& ofs, int x, int z)
{
    ofs << x << ", ";
}

// Generate some random noise from input, this is a hack for testing
// not a stotasticly meaningful value
// Args:
//  ratio: the max noise level ratio
// Return:
//   randomr, where random is [-0.5, 0.5]
float some_noise(float ratio=0.1)
{
    float f = static_cast<float>(rand()) / RAND_MAX;
    float fr = f - 0.5;
    //std::cout << ocol(std::cout, f) << ocol(fr) << std::endl;
    return ratio * fr;
}

void generate_ramp(void (*fofx)(int xs))
{
}

const int heap_size = 15;
const int heap_loc = 7;
const uint16_t heap_end = heap_size - 1;
uint16_t heapi = heap_end;
uint16_t the_heap[heap_size]; // value of 16 bits or less
uint16_t heap_index[heap_size]; // indices for the values, sorted

void show_heap_last()
{
    std::array<uint16_t, heap_size> theap;
    std::cout << "offset:";
    for (int l = 0; l < heap_size; l++) {
        std::cout << the_heap[l] << ", ";
        theap[l] = the_heap[l];
    }
    std::cout << std::endl;
    std::sort(theap.begin(), theap.end());
    std::cout << "sorted: " ;
    for (auto t : theap)
        std::cout << t << ", ";
    std::cout << std::endl;
}

void show_heap_current(uint8_t mloc)
{
    std::cout << "mloc:" << mloc << ": ";
    for (int l = 0; l < mloc; l++) {
        std::cout << heap_index[l] << ",";
    }
    std::cout << std:: endl;
}

void heap_init(uint16_t sv)
{
    for (int i = 0; i < heap_size; i++)
        the_heap[i] = sv;
    for (int i = 0; i < heap_size; i++)
        heap_index[i] = i;
    heapify(the_heap, heap_size);
    heapify(heap_index, heap_size);
}

uint16_t heap_insert(uint16_t nv)
{
    std::cout << "insert: " << nv << std::endl;
    show_heap_last();
    the_heap[heapi++] = nv;
    if (heapi > heap_end)
        heapi = 0;
    return nv;
}


int main (int argc, char** argv)
{
    if (argc < 2)
    {
        std::cout << "usage: ofile, start, slope, ratio, limit scale" << std::endl;
        return 1;
    }
    int argi = 1;
    std::ofstream csv(argv[argi++]);
    uint16_t start = 60;
    if (argi > argc)
        start = (uint16_t) std::stoi(argv[argi++]);
    float m = 0.01;
    if (argi > argc)
        m = std::stof(argv[argi++]);
    float r = 0.01;
    if (argi > argc)
         r = std::stof(argv[argi++]);
    int limit = 1024;
    if (argi > argc)
         limit = std::stoi(argv[argi++]);
    float scale = 100;
    if (argi > argc)
         scale = std::stof(argv[argi++]);
    heap_init((uint16_t) (scale * start));
    show_heap_last();
    show_heap_current(heap_size);
    csv << "N,   Y,    m, noise, y + n, y0, mfo" << std::endl;
    for (int i = 0; i < limit; i++)
    {
        //float y0 = genline(m, i);
        float y = start + m * i;
        float n = some_noise(r);
        float z = y + n;
        //std::cout << n << ", " << z << std::endl;
        uint16_t y0 =  (uint16_t) ((scale * z) + 0.5);
        std::cout << i << ": " << y0 << std::endl;
        uint16_t y1 = heap_insert(y0);
        csv << i << ", " 
            << y << ", " 
            << m << ", " 
            << n << ", " 
            << z << ", " 
            << y0 << ", " 
            << y1 << std::endl;
    }
    
    return 0;
    //  run h.csv 60 0.01 0.01 100
}

