// read dmp file
// format:
// decimal0addr hex-short
// e.g.
// 40001 5375 (21365)
// 40002 6e53 (28243)
// # model 1
// 40003 0001 (1)
// 40004 0042 (66)
// 40005 4855 (18517)
// ...

#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main(int argc, char * argv[]) 
{
    if (argc <= 1) {
        cout << "Usage: " << argv[0] << " dmpfile" << endl;
    }
    //ifstream ins = ifstream(argv[1]);
    ifstream ins(argv[1]);
    uint32_t addr;
    uint16_t val;
    size_t val_pos;
    string line;

    auto rline = [&] {
         val_pos = line.find(' ', 1) + 1;
         addr = stoi(line.substr(0, val_pos - 1));
         val = stoi(line.substr(val_pos), 0, 16);
        };
    getline(ins, line);
    rline();
    //cout << "start-addr:" << dec << addr << " : " << hex << val << endl;
    cout << "start-addr:" << dec << addr << " : "
         << ((char) val)  << ((char) (val >> 8)) << endl;
    cout << dec << setw(6) << addr << ": ";
    cout << setfill('0');
    cout << hex;
    cout << setw(4);
    cout << val;
    cout << ':';
    while (!ins.eof()) {
        getline(ins, line);
        if (!isdigit(line[0])) {
            continue;
        }
        rline();
        cout << setfill('0');
        cout << setw(4);
        cout << hex;
        cout << val;
        if ((addr & ((1 << 4) - 1)) == 15) {
            cout << setfill(' ');
            cout << '\n' << dec << setw(6) << addr << ": ";
        } else
            cout << ':';
    }
    cout << "\nend-addr:" << dec << addr << " : " << val << endl;
}

