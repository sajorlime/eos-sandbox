

# include <iostream>
# include <map>
# include <stdio.h>
# include <string.h>
# include "ansi_colors.h"

std::map<std::string, std::string> color_map = {
  ANSI_COLOR_MAP("black"),
  ANSI_COLOR_MAP("red"),
  ANSI_COLOR_MAP("green"),
  ANSI_COLOR_MAP("yellow"),
  ANSI_COLOR_MAP("blue"),
  ANSI_COLOR_MAP("magenta"),
  ANSI_COLOR_MAP("cyan"),
  ANSI_COLOR_MAP("white"),
  ANSI_COLOR_MAP("bright_black"),
  ANSI_COLOR_MAP("bright_red"),
  ANSI_COLOR_MAP("bright_green"),
  ANSI_COLOR_MAP("bright_yellow"),
  ANSI_COLOR_MAP("bright_blue"),
  ANSI_COLOR_MAP("bright_magenta"),
  ANSI_COLOR_MAP("bright_cyan"),
  ANSI_COLOR_MAP("bright_white")
};

enum class AnsiColor {
  Black,
  Red,
  Green,
  Yellow,
  Blue,
  Magenta,
  Cyan,
  White,
  BrightBlack,
  BrightRed,
  BrightGreen,
  BrightYellow,
  BrightBlue,
  BrightMagenta,
  BrightCyan,
  BrightWhite,
};

struct ABC
{
    int a;
    int b;
    int c;
};

# define MAKESYM(X, A) X ## _name_ ## A

void show_abc(ABC x)
{
    std::cout << x.a << std::endl;
    std::cout << x.b << std::endl;
    std::cout << x.c << std::endl;
}


# define _TicksInMsec_(Pv) (10 * Pv)
#define FORWARD_MSECS(Pstruct, Pmember, Pforward) \
  (((Pstruct)->Pmember - Pforward) > 17) ? \
       _TicksInMsec_((Pstruct)->Pmember - Pforward) \
       : \
       _TicksInMsec_(17)

# define VOLTS_FORWARDED_MSECS(Pname, Pforward) \
    FORWARD_MSECS(&abc, Pname, Pforward)
# define OV2_FORWARDED_MSECS \
    VOLTS_FORWARDED_MSECS(c, 5 * 17)

int main()
{
    ABC abc;
    abc.a = 145;
    abc.b = 155;
    abc.c = 165;
    std::cout << abc.b - 45 << std::endl;
    int x = FORWARD_MSECS(&abc, b, 3 * 17);
    int y = VOLTS_FORWARDED_MSECS(b, 4 * 17);
    int z = OV2_FORWARDED_MSECS;
    std::cout << x << std::endl;
    std::cout << y << std::endl;
    std::cout << z << std::endl;
    return 0;
}

