// downcast shared pointer
#include <iostream>
#include <string>
#include <memory>

struct X
{
    int _x;
    X(int x) : _x(x) {}
    X() : _x() {}
};

struct X2 : public X
{
    X _x2;
    X2(X const& l1, X const& l2) : X(l1), _x2(l2) {}
};

struct X3 : public X2
{
    X _x3;
    X3(X2 const& tlx, X const& x3) : X2(tlx), _x3(x3) {}
    X3(X3 const& thx) : X2(thx), _x3(thx._x3) {}
};


void print_x(std::string id, X& x) { std::cout << id << ':' << x._x << std::endl; }
void print_x3(std::string id, X3& x3) { std::cout << id << ":{" << x3._x << ", " << x3._x2._x <<  ", " << x3._x3._x << '}' << std::endl; }


int main ()
{
    X* xarray = new X[3];
    xarray[0]._x  = 1;
    xarray[1]._x = 2;
    xarray[2]._x = 3;
    std::cout << "XA:" << xarray[2]._x << xarray[1]._x << xarray[0]._x << std::endl;
    X* yarray{ new X[3]{4, 5, 6}};
    std::cout << "YA:" << yarray[2]._x << yarray[1]._x << yarray[0]._x << std::endl;
    X* x11_3 = new X3(X2(11, 13), 17);
    print_x3("reg-ptr-down-cast", *(static_cast<X3*>(x11_3)));
    std::shared_ptr<X> sx11_3p = std::make_shared<X>(X3(X2(11, 13), 17));
    X3 sx11_3 = *(std::static_pointer_cast<X3>(sx11_3p));
    print_x3("shrd-ptr-down-cast", sx11_3);
    std::shared_ptr<X3> sx3p = std::make_shared<X3>(X3(X2(11, 13), 17));
    print_x3("shrd-ptr-w/o-cast", *sx3p);
    std::shared_ptr<X> sx11_1 = std::static_pointer_cast<X>(sx3p);
    print_x("shrd-ptr-upo-cast", *sx11_1);
    std::shared_ptr<X3> sx11_3dn = std::static_pointer_cast<X3>(sx11_1);
    print_x3("shrd-ptr-dn/up-cast", *sx11_3dn);

    return 0;
}
