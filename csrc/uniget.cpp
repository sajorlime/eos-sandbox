// unique_ptr::get vs unique_ptr::release
#include <iostream>
#include <memory>
#include <typeinfo>

class MyS {
public:
    int _x;
    int _y;
    MyS(int x, int y)
      : _x(x), _y(y)
    {
    }
    MyS()
      : _x(0), _y(0)
    {
    }
};

struct Id { Id() {};};

template<class S>
struct Struct : public MyS
{
    static S _mine[10];
    Struct(int x, int y)
      : MyS(x, y)
    {
    }
    Struct()
      : MyS()
    {
    }
};

struct Z {
    int _z;
    Z(int a) : _z(a) {}
};

template Struct<Z> {
    Struct<z>(int a)
       Stuct<z>() _z(a) : 
    {
    }
};

Struct<Z> _sz = {1, 2, 3};

Struct<Id> _i;



int main ()
{
    std::cout << "SX:" << _szyz._x << ':' << _szyz._z << std::endl;
    std::cout << "SI:" << _i._x << ':' << _i._y << std::endl;
                                           // foo   bar    p
                                           // ---   ---   ---
  std::unique_ptr<int> foo;                // null
  std::unique_ptr<int> bar;                // null  null
  int* p = nullptr;                        // null  null  null

  foo = std::unique_ptr<int>(new int(10)); // (10)  null  null
  bar = std::move(foo);                    // null  (10)  null
  p = bar.get();                           // null  (10)  (10)
  std::cout << "bar:" << ':' << *bar << ':' << typeid(bar).name() << std::endl;
  std::cout << "bar.get:" << p << ':' << *p << ':' << std::endl;
  *p = 20;                                 // null  (20)  (20)
  std::cout << "p:" << p << ':' << *p << ':' << std::endl;
  p = nullptr;                             // null  (20)  null
  std::cout << "bar:" << p << ':' << *bar << ':' << std::endl;

  MyS* yp = new MyS();
  *yp = {17, 18};
  std::unique_ptr<MyS> xp;
  xp = std::unique_ptr<MyS>(new(yp) MyS(*yp));
  xp->_x = 2;
  xp->_y = 3;
  std::cout << "xp:" << ':' << xp->_x << ':' << xp->_y << std::endl;
  yp = xp.release();
  std::cout << "xp:" << ':' << yp->_x << ':' << yp->_y << std::endl;

  foo = std::unique_ptr<int>(new int(30)); // (30)  (20)  null
  p = foo.release();                       // null  (20)  (30)
  *p = 40;                                 // null  (20)  (40)
  std::cout << "p:" << p << ':' << *p << ':' << typeid(p).name() << std::endl;

  std::cout << "foo: ";
  if (foo) std::cout << *foo << '\n'; else std::cout << "(null)\n";

  std::cout << "bar: ";
  if (bar) std::cout << *bar << '\n'; else std::cout << "(null)\n";

  std::cout << "p: ";
  if (p) std::cout << *p << '\n'; else std::cout << "(null)\n";
  std::cout << '\n';

  delete p;   // the program is now responsible of deleting the object pointed to by p
              // bar deletes its managed object automatically

  return 0;
}
