
#include <iostream>
#include <cassert>
#include <cctype>
#include <cstdlib>
#include <cerrno>
#include <string>
#include <list>
#include <iterator>
#include <map>
#include <vector>
#include <string.h>


using namespace std;



int
parse_enum(const char* str, size_t sz, char const* refStr, int sep)
{
    char const* end = refStr;
    size_t      len;
    int         x;

    if (str == 0 || sz == 0)
        return -1;
    for (int k=0; ; ++k) {
        // Find end of next ref sub-string.
        for (;; ++end)
            if ((x=*end)==0 || x==sep)
                break;
        len = end - refStr;
        // Compare sub-substring.
# if 0
        bool b = (0 == memcmp(refStr, str, len));
        if (b)
            cout << string(refStr).substr(0, len) << ':' << b
                 << " l: " << len
                 << " bl: " << (len && b)
                 << " sz: " << sz
                 << endl;
# endif
        if (sz == len && memcmp(refStr, str, len) == 0)
            return k;
        if (x == 0)
            break;
        refStr = ++end;
    }
    cout << str << ": at end PE" << endl;
    return -1;
}


static char const* const deviceClassRef =
    "FRONIUS_IG,ION-8600,SATCON-PVS,SHARK-100,SMANET-1,SPWR-MET-1,SANTERNO-SW-M,"
    "SANTERNO-SW-TG,IME-CE4D,WATTNODE,SOLARBRIDGE,KACO-00,KACO-TL3,VERIS-H8036,"
    "SMA-SC,MET-CVAR1,MET-CVAR2,MET-CVAR3,MET-CVAR4,MET-CVAR5,POWER_ONE,SATCON_AE,"
    "SMA-TP,SCHNEIDER-GT,GFM-E1242,SMA-SCCP,SMA-CCTRL,PVS5-METER-P,PVS5-METER-C,"
    "SUNSPEC-IV-1PH,SUNSPEC-IV,SUNSPEC-IV-3PH,SUNSPEC-ACMETER,"
    "POWELEC-HEC-US,DELTA-ESS,LOCKHEED-ESS,SANTERNO-SW-TG-T,EQUINOX-ESS,"
    "BATTERY,"
    "HUB+,";

const char* search_items[]=
{
    "SANTERNO-SW-TG",
    "GFM-E1242",
    "HUB+",
    "FRONIUS_IG",
    "GFM-E1242",
    "bar",
    0
};

const std::string _items[]=
{
    "SANTERNO-SW-TG",
    "GFM-E1242",
    "HUB+",
    "FRONIUS_IG",
    "GFM-E1242",
    "bar",
    ""
};

constexpr std::string const& lookup(int i)
{
    return _items[i];
}

struct DataPoint {
    double value;
    time_t timestamp;
};

struct Data {
    DataPoint _current;
    DataPoint _last;
};


enum DType {D_HUB, D_ESS, D_BATTERY};
enum Ec {null_event, another};
enum CkRet {ecr_clear, ecr_event};

typedef CkRet (* eg_limits_check)(Data * data);


CkRet _dummy_check(Data* data)
{
    if (data)
        return ecr_clear;
    return ecr_event;
}

// Some types we are going to need
struct egd_info; // event generator info, forward ref
typedef struct egd_info Egdi;
typedef std::map<const char *, Egdi*> EgdiMap;
typedef EgdiMap::iterator EgdiMapI;


// structure for holding state of a device.
// TODO(epr): use this structure for basis of DB storage.
struct egd_info {
    DType _type; // the device type
    const char* _getStr; // string that identifies this device to the exif.
    Ec _ec; // The current event code
    bool _timedout; // flag for timedout
    Data _data; // storage for saving the current value and the last value
    CkRet _ck;
    eg_limits_check _lcf;
};

#define ANINFO(Ptype, Pstr, Pfunc) \
{\
    ._type = Ptype, \
    ._getStr = Pstr,\
    ._ec = Ec::null_event, \
    ._timedout = false, \
    ._data = {._current = {.value = 0.0, .timestamp = time(0)}, \
           ._last = {.value = 0.0, .timestamp = time(0)}}, \
    ._ck = ecr_clear, \
    ._lcf = Pfunc \
}


Egdi _hub_egd_info[] = {
    ANINFO(D_HUB, "contactor_state", _dummy_check),
    ANINFO(D_HUB, "t_degc", _dummy_check),
    ANINFO(D_HUB, "humidity", _dummy_check),
    ANINFO(D_HUB, "v1n_grid_v", _dummy_check),
    ANINFO(D_HUB, "v2n_grid_v", _dummy_check),
    ANINFO(D_HUB, "v1n_v", _dummy_check),
    ANINFO(D_HUB, "v2n_v", _dummy_check),
    ANINFO(D_HUB, "v_supply", _dummy_check),
    {(DType) -1} // use an illegal type to terminate list
};

# if 1
Egdi _ess_egd_info[] = {
    ANINFO(D_ESS, "V", _dummy_check),
    ANINFO(D_ESS, "W", _dummy_check),
    ANINFO(D_ESS, "TmpSnk", _dummy_check),
    ANINFO(D_ESS, "SoC", _dummy_check),
    ANINFO(D_ESS, "SoH", _dummy_check),
    ANINFO(D_ESS, "State", _dummy_check), // 802:22
    ANINFO(D_ESS, "Evt1", _dummy_check), // 802:26
    {(DType) -1} // use an illegal type to terminate list
};

Egdi _bat_egd_info[] = {
    ANINFO(D_BATTERY, "V", _dummy_check),
    ANINFO(D_BATTERY, "I", _dummy_check),
    {(DType) -1} // use an illegal type to terminate list
};
# endif


struct X {
    std::map<const char *, int> _imap;
    std::map<const char *, int>::iterator _imapi;
};

X _x =
{
    {{"A", (int) 'a'},
    {"B", (int) 'b'},
    {"C", (int) 'c'}},
    _imap.begin()
};


std::vector<int> _ivector = {17, 77, 177};


typedef int (* next_int)();

auto make_next_int(

    auto cf = [&](Point& p, const char * "name") -> int {
        std::cout << name << ':' << p << std::endl;
    _imapi = _imap.begin();
    auto nif = [&](){
        if (_imapi == _imap.end())
            return 0;
        int i = _imapi.second;
        _imapi++;
        return i + t;
    };
    return nif;
):


int main(int argc, char ** argv)
{
    int r;
    char rStr[256];

    if (argc > 1) {
        r = parse_enum(argv[1], strlen(argv[1]), deviceClassRef, ',');
        cout << argv[1] << " gets |" << rStr << "| " << r << std::endl;
    }
    for (const char** cp = search_items; *cp; cp++) {
        strcpy(rStr, *cp);
        r = parse_enum(rStr, strlen(*cp), deviceClassRef, ',');
        cout << *cp << " gets :" << rStr << ": " << r << std::endl;
    }
    for (int i = 0; i < 6; i++) {
        const std::string sr = _items[i];
        std::cout << sr << std::endl;
        std::cout << lookup(i) << std::endl;
    }
    for (Egdi* p = _hub_egd_info; p->_type != (DType) -1; p++)
    {
        std::cout << p->_type << ':' << p->_getStr << std::endl;
    }
# if 1
    auto nif = make_next_int(0);
    int i;
    while ((i = nif()) != 0) {
        std::cout << "I:" << i << std::endl;
    }
# endif

    return 0;
}



