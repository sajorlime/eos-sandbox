#include <iostream>
#include <istream>
#include <fstream>

#include "minijson_reader.hpp"

//void f() {
char bat_discription[] =
# if 0
R"batinfo({
    "batteries" : [
         "bat": { "pos" : 1,
          "serial_number" : "ser1",
          "sw_version" : "sw1",
          "hw_version" : "hw1"
        },
         "bat": { "pos" : 2,
          "serial_number" : "ser2",
          "sw_version" : "sw2",
          "hw_version" : "hw2"
        }
    ]
})batinfo";
# else
R"batinfo({
    "nbats" : 2,
    "batteries" : [
         { "pos" : 1,
          "serial_number" : "ser1",
          "sw_version" : "sw1",
          "hw_version" : "hw1"
        },
        { "pos" : 2,
          "serial_number" : "ser2",
          "sw_version" : "sw2",
          "hw_version" : "hw2"
        }
    ]
})batinfo";
# endif

struct bat_type {
    std::string serial_number;
    std::string sw_version;
    std::string hw_version;
};
struct BatVS
{
    int nbats;
    std::vector<bat_type> batteries;
};

BatVS _buf_bats;

using namespace minijson;

void convert_buffer()
{
    buffer_context ctx(bat_discription, sizeof(bat_discription) - 1);

    parse_object(ctx, [&](const char* k, value v)
    {
        dispatch (k)
            <<"nbats">> [&]{ _buf_bats.nbats = v.as_long(); }
            <<"batteries">> [&]
        {
            parse_array(ctx, [&](value v)
            {
                bat_type b;
                parse_object(ctx, [&](const char* k, value v)
                {
                    long x;
                    dispatch (k)
                        <<"pos">> [&]{ x = v.as_long(); }
                        <<"serial_number">> [&]{ b.serial_number = v.as_string(); }
                        <<"sw_version">>    [&]{ b.sw_version = v.as_string(); }
                        <<"hw_version">>    [&]{ b.hw_version = v.as_string(); };
                });
                    _buf_bats.batteries.push_back(b);
            });
        };
    });
}

BatVS _istrm_bats;

void convert_istream(std::istream& istrm)
{
    istream_context ctx(istrm);

    parse_object(ctx, [&](const char* k, value v)
    {
        dispatch (k)
            <<"nbats">> [&]{ _istrm_bats.nbats = v.as_long(); }
            <<"batteries">> [&]
        {
            parse_array(ctx, [&](value v)
            {
                bat_type b;
                parse_object(ctx, [&](const char* k, value v)
                {
                    long x;
                    dispatch (k)
                        <<"pos">> [&]{ x = v.as_long(); }
                        <<"serial_number">> [&]{ b.serial_number = v.as_string(); }
                        <<"sw_version">>    [&]{ b.sw_version = v.as_string(); }
                        <<"hw_version">>    [&]{ b.hw_version = v.as_string(); };
                });
                    _istrm_bats.batteries.push_back(b);
            });
        };
    });
}





int main(int argc, char** argv)
{
    convert_buffer();

   std::cout <<  "nbats:" << _buf_bats.nbats << std::endl;
    for (auto& b : _buf_bats.batteries)
    {
       std::cout <<  b.serial_number << ':' << b.sw_version << ':' << b.hw_version << std::endl;
    }
    if (argc <= 1)
        return 0;
    std::ifstream jfile(argv[1], std::ifstream::in);
    if (!jfile.is_open()) {
        std::cout << "Not open:" << argv[1] << std::endl;
        return -1;
    }
    convert_istream(jfile);
   std::cout <<  "fnbats:" << _istrm_bats.nbats << std::endl;
    for (auto& b : _istrm_bats.batteries)
    {
       std::cout <<  b.serial_number << ':' << b.sw_version << ':' << b.hw_version << std::endl;
    }
}

