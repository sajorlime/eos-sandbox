#include "minijson_reader.hpp"
#include <iostream>

# if 0
    "{ \"field1\": 42, \"array\" : [ 1, 2, 3 ], \"field2\": \"asd\", "
    "\"nested\" : { \"field1\" : 42.0, \"field2\" : true, "
    "\"ignored_field\" : 0, "
    "\"ignored_object\" : {\"a\":[0]} },"
    "\"ignored_array\" : [4, 2, {\"a\":5}, [7]] }";
# endif

void f() {
char json_obj[] =
R"foo({"field1": 42,
        "array" : [ 1, 2, 3 ],
        "field2": "asd",
        "nested" : {"field1" : 42.0,
                    "field2" : true,
                    "ignored_field" : 0,
                    "ignored_object" : {"a":[0]}
        },
        "ignored_array" : [4, 2, {"a":5}, [7]] }")foo";

struct bobj {int a; std::string b;};

struct obj_type
{
    long field1 = 0;
    std::string field2; // you can use a const char*, but
                        // in that case beware of lifetime!
    struct
    {
        double field1 = 0;
        bool field2 = false;
    } nested;
    std::vector<bobj> array;
};

obj_type obj;

using namespace minijson;

buffer_context ctx(json_obj, sizeof(json_obj) - 1);

parse_object(ctx, [&](const char* k, value v)
{
    dispatch (k)
    <<"field1">> [&]{ obj.field1 = v.as_long(); }
    <<"field2">> [&]{ obj.field2 = v.as_string(); }
    <<"nested">> [&]
    {
        parse_object(ctx, [&](const char* k, value v)
        {
            dispatch (k)
            <<"field1">> [&]{ obj.nested.field1 = v.as_double(); }
            <<"field2">> [&]{ obj.nested.field2 = v.as_bool(); }
            <<any>> [&]{ ignore(ctx); };
        });
    }
    <<"array">> [&]
    {
        parse_array(ctx, [&](value v)
        {
            parse_object(ctx, [&](const char* k, value v)
            {
            bobj b;
            dispatch (k)
            <<"a">> [&]{ b.a = v.as_long(); }
            <<"b">> [&]{ b.b = v.as_string(); }
            obj.array.push_back(b);
            }
        });
    }
    <<any>> [&]{ ignore(ctx); };
});

  std::cout << "f1:" << obj.field1 << std::endl;
  std::cout << "v[0]:" << obj.array[0].b << std::endl;

}



int main(int argc, char** argv)
{
    f();
}

