#include "minijson_reader.hpp"
//#include "../fw/src/validation/minijson_reader.hpp"
#include <iostream>
#include <iomanip>
#include <exception>


struct RecMsg
{
    std::string action;
    struct
    {
        int msecs = 0;
        int pkt_size = -1;
        size_t bytes = 0;
        uint64_t location = 0;
        bool write = 0;
        std::string mode =  "xxx";
    } parameters;
};


using namespace minijson;

constexpr char const* _action = "action";
constexpr char const* _params = "parameters";
constexpr char const* _msecs = "msecs";
constexpr char const* _bytes = "bytes";
constexpr char const* _location = "location";
constexpr char const* _pkt_size = "pkt_size";
constexpr char const* _mode = "mode";
constexpr char const* _write = "write";

constexpr char const* _adc9600 = "Adc9600";
constexpr char const* _data = "data";

void f(RecMsg& obj, char* json_obj, size_t len)
{
    buffer_context ctx(json_obj, len - 1);

    try {
        parse_object(ctx, [&](const char* k, value v)
        {
            dispatch (k)
            <<_action>> [&]{ obj.action = v.as_string(); }
            <<_params>> [&]
            {
                parse_object(ctx, [&](const char* k, value v)
                {
                    dispatch (k)
                    <<_msecs>> [&]{ obj.parameters.msecs = v.as_long(); }
                    <<_pkt_size>> [&]{ obj.parameters.pkt_size = v.as_long(); }
                    <<_bytes>> [&]{ obj.parameters.bytes = v.as_long(); }
                    <<_location>> [&]{ obj.parameters.location = v.as_long(); }
                    <<_write>> [&]{ obj.parameters.write = v.as_bool(); }
                    <<_mode>> [&]{ obj.parameters.mode = v.as_string(); }
                    <<any>> [&]{ ignore(ctx); };
                });
            }
            <<any>> [&]{ ignore(ctx); };
        });
        if (obj.parameters.mode == _adc9600) {
            std::cout << "yea! 9600" << std::endl;
        }
        if (obj.parameters.mode == _data) {
            std::cout << "yea! data" << std::endl;
        }
    }
    catch (...) {
        std::cout << "Exception" << std::endl;
        throw std::exception();
    }
}


char capture_buf[] =
R"foo({"action": "capture",
        "parameters" : {"msecs" : 37,
                        "pkt_size" : 4096,
                        "mode" : "Adc9600"}
        }")foo";

char playback_buf[] =
R"foo({"action": "playback",
        "parameters" : {"msecs" : 17,
                        "pkt_size" : 4096,
                        "write": true,
                        "mode" : "Adc9600"}
        }")foo";
char data_buf[] =
# if 1
R"foo({"action": "data",
        "parameters" : {"bytes" : 17,
                        "location" : 34376515584,
                        "write" : false,
                        "mode" : "Data"}
        }")foo";
# else
R"foo({"action": "playback", "parameters": {"bytes": 1048576, "pkt_size": 4096, "mode": "Data"}}")foo";
# endif

char echo_buf[] =
R"foo({"action": "echo"
        }")foo";

char stop_buf[] =
R"foo({"action": "stop"
        }")foo";

char bad_buf[] =
R"foo({"action": "stop"
        }")foo";

char bogus_buf[] = R"foo({bogus dataadfds fdsafdsfdsfjksdfdsk)foo";

int main(int argc, char** argv)
{
    RecMsg obj;
    std::string xp = "none";
    if (argc < 2) {
        f(obj, stop_buf, sizeof(stop_buf));
        xp = "stop:";
    }
    else if (argv[1] == std::string("c")) {
        f(obj, capture_buf, sizeof(capture_buf));
        xp = "capt:";
    }
    else if (argv[1] == std::string("d")) {
        f(obj, data_buf, sizeof(data_buf));
        xp = "datat:";
    }
    else if (argv[1] == std::string("p")) {
        f(obj, playback_buf, sizeof(playback_buf));
        xp = "playt:";
    }
    else {
        std::cout << "usabe: recorder_msg [c|d|p]" << std::endl;
    }

    std::cout << "Size:" << sizeof(capture_buf) << std::endl;
    std::cout << xp << obj.action << '{' << std::endl;
    std::cout << "    msecs: " << obj.parameters.msecs << std::endl;
    std::cout << "    mkt_size: " << obj.parameters.pkt_size << ',' << std::endl;
    std::cout << "    bytes: " << std::hex << obj.parameters.bytes << std::endl;
    std::cout << "    write: " << (obj.parameters.write ? "True" : "False") << ',' << std::endl;
    std::cout << "    location: " << std::hex << obj.parameters.location << ',' << std::endl;
    std::cout << "    mode: " << obj.parameters.mode << '}' << std::endl;

# if 0
    try {
        f(obj, bogus_buf , sizeof(bogus_buf));
    }
    catch (...) {
        std::cout << "bogus caught" << std::endl;
    }
# endif
}

