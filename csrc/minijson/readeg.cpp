#include <iostream>
#include <istream>
#include <fstream>
#include <vector>
#include <map>
#include <string>

#include "minijson_reader.hpp"


struct limits {
    double min;
    double max;
};

struct measurement {
    bool enabled;
    double last;
    std::string event;
};

struct subsystem {
    std::map<std::string, limits> lmap;
    std::map<std::string, measurement> mmap;
};

using namespace minijson;

std::map<std::string, subsystem*> subsystems;

void initss()
{
    subsystems["ess"] = new subsystem();
    subsystems["hub+"] = new subsystem();
    subsystems["battery"] = new subsystem();
}


//void parse_limit(Context& ctx, char& c bool& must_read)
//{
//}

void convert_istream(std::istream& istrm)
{
    istream_context ctx(istrm);

    parse_object(ctx, [&](const char* k, value v)
    {
        dispatch (k)
            <<"subsystems">> [&]
        {
            std::cout << "S:" << v.as_string() << std::endl;
            parse_array(ctx, [&](value v)
            {
                subsystem* ss = nullptr;
                //std::cout << "A:" << v.as_string() << std::endl;
                parse_object(ctx, [&](const char* k, value v)
                {
                    dispatch (k)
                        <<"name">> [&] {
                            ss = subsystems[v.as_string()];
                            std::cout << "Name:" << k << ':' << v.as_string() << ':' << ss << std::endl;
                            ignore(ctx);
                        }
                        <<"limits">> [&]{
                            //std::cout << "limits:" << v.as_string() << ':' << ss << std::endl;
                            parse_object(ctx, [&](const char* k, value v) {
                                dispatch (k)
                                <<any>> [&] {
                                    //std::cout << "L:" << k << ':' << v.as_string() << std::endl;
                                    limits l;
                                    parse_object(ctx, [&](const char* k, value v) {
                                        dispatch (k)
                                        <<"min">> [&] {
                                            l.min = v.as_double();
                                            //std::cout << "n:" << k << ':' << v.as_string() << std::endl;
                                        }
                                        <<"max">> [&] {
                                            l.max = v.as_double();
                                            //std::cout << "x:" << k << ':' << v.as_string() << std::endl;
                                        };
                                    });
                                    ss->lmap[k] = l;
                                };
                            });
                        }
                        <<"measurements">> [&]{
                            std::cout << "measurements:" << v.as_string() << ':' << ss << std::endl;
                            parse_object(ctx, [&](const char* k, value v) {
                                dispatch (k)
                                <<any>> [&] {
                                    //std::cout << "L:" << k << ':' << v.as_string() << std::endl;
                                    measurement m;
                                    parse_object(ctx, [&](const char* k, value v) {
                                        dispatch (k)
                                        <<"enabled">> [&] {
                                            m.enabled = v.as_bool();
                                            std::cout << "e:" << k << ':' << v.as_string() << std::endl;
                                        }
                                        <<"last">> [&] {
                                            m.last = v.as_double();
                                            std::cout << "l:" << k << ':' << v.as_string() << std::endl;
                                        }
                                        <<"event">> [&] {
                                            m.event = v.as_string();
                                            std::cout << "v:" << k << ':' << v.as_string() << std::endl;
                                        };
                                    });
                                    ss->mmap[k] = m;
                                };
                            });
                        }
                        <<any>> [&] { ignore(ctx); };
                });
            });
        };
    });
}





int main(int argc, char** argv)
{
    if (argc <= 1)
        return 0;
    initss();
    std::ifstream jfile(argv[1], std::ifstream::in);
    if (!jfile.is_open()) {
        std::cout << "Not open:" << argv[1] << std::endl;
        return -1;
    }
    convert_istream(jfile);
    for (auto& ss : subsystems)
    {
       std::cout << "SS:" << ss.first << ':' << ss.second << std::endl;
       for (auto li : ss.second->lmap) {
           limits l = li.second;
           std::cout <<  ss.first << "L:" << li.first << ':' << l.min << '<' << l.max << std::endl;
       }
       for (auto mi : ss.second->mmap) {
           measurement m = mi.second;
           std::cout <<  ss.first << "M:" << mi.first << ':' << m.enabled << ':' << m.last << ':' << m.event << std::endl;
       }
    }
}

