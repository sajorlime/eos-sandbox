
/*
    Define the message structures.
 */

# include <algorithm>
# include <cassert>
# include <cstdlib>
# include <deque>
# include <iostream>
# include <iomanip>
# include <string>
#include <typeinfo>
# include <complex>
# include <vector>

# include <unistd.h>

void* _payload = nullptr;
size_t _pllen = 0;

template <typename T>
bool update_payload(T* vec,
                    size_t len_vec)
{
    _payload = vec;
    _pllen = len_vec;
    std::cout << 'B'
              << "@ "
              << _payload
              << " of: "
              << _pllen
              << std::endl;
    return true;
}

template <typename T>
bool update_payload(std::vector<T>& vec)
{
    std::cout << 'A';
    return update_payload(vec.data(), vec.size());
}

bool update_payload(void* ptr,
                    size_t bytes)
{
    _payload = ptr;
    _pllen = bytes;
    std::cout << "X"
              << "@ "
              << _payload
              << " of: "
              << _pllen
              << std::endl;
    return true;
}


std::vector<std::complex<int>> xvec{{0, 1}, {0, 1}};
std::vector<std::complex<int>>& rvec{xvec};

int main(int argc, char** argv)
{
    double z{0};
    auto x = rvec[0];
    auto y = (decltype(std::real(z))) std::imag(rvec[0]);
    std::cout << x << std::endl;
    std::cout << y << std::endl;
    std::cout << z << std::endl;
    std::cout << xvec.capacity() << std::endl;
    std::vector<std::complex<int>> yvec(8);
    std::vector<std::complex<int>> zvec{};
    zvec.reserve(10);
    for (auto&  yi : yvec) {
        std::cout << "yvcap: " << yvec.capacity() << " yvsiz: " << yvec.size() << std::endl;
        yi = {1, 0};
        zvec.push_back({0, 1});
        std::cout << "zvcap: " << zvec.capacity() << " yvsiz: " << zvec.size() << std::endl;
    }
    for (auto&  yi : yvec) {
        std::cout << yi << std::endl;
    }
    for (auto&  zi : zvec) {
        std::cout << zi << std::endl;
    }
    std::vector<std::complex<int>> nvec{};
    nvec.push_back({0, 1});
    nvec.push_back({0, 1});
    std::cout << "nvec =? xvec: " << (nvec == xvec) << std::endl;
    update_payload(nvec);
    //update_payload(nvec, (size_t) 10);
    update_payload(nvec.data(), 15);
    auto xp = &nvec[0];
    update_payload(xp, 20);


    return 0;
}

