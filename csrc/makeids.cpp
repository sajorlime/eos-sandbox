
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

using namespace std;

// dummy stuff

struct queue_stuff
{
    void* front;
    void* back;
    int count;
    void init(void* qdata)
    {
      front = qdata;
      back = qdata;
      count = 0;
    }
};

// constexpr std:vector does not compile.
// constexpr std::vector coming in C++20.
constexpr const char * names[] =
{
    "abc",
    "efg",
    "hih",
    ""
};

constexpr size_t q_entries = 7;

constexpr size_t nqueues = sizeof(names) / sizeof(names[0]) - 1;

constexpr auto q_space_size = nqueues * (sizeof(queue_stuff)
                                       + sizeof(void*) * q_entries
                                       + 8); // buffer space
uint8_t qspace[q_space_size];

//using qpair = std::pair<std::string, queue_stuff*>;
//
std::map<const std::string, queue_stuff*> q_map;

int create_sqs()
{
    uint8_t* qp = qspace;
    size_t i = 0;
    for (; i < nqueues; i++) {
        queue_stuff* qsp = (queue_stuff*) qp;
        void* qdata = (void*)(qp + 4); // extra 
        qsp->init(qdata);
        q_map.emplace<std::string, queue_stuff*>(std::string(names[i]),
                                   std::move(qsp));
        qp += q_space_size - 4; // 
        
    }
    return i;
}


namespace { // anonymous
constexpr uint32_t higher16(uint32_t v)
{
    auto x = v / 16;
    return 16 * (x + 1);
}
}

namespace NS {

enum class mid
{
    ping = 0, // echo back threads name
    first_input_mid,
    adjust_time = first_input_mid,
    set_dwell,
    set_power_mode,
    report_time,
    set_sv_status,
    pre_position,
    rcvr_reset,
    log_level,
    capture,
    playback,
    hw_update,
    jtag_control,
    security_mode,
    status_request,
    end_input_mids, // insert new inputs before here

    first_output_mid = higher16((uint32_t)end_input_mids) + 1,
    time_report = first_output_mid,
    strobe_message,
    measurement_data,
    nav_data_bits,
    hardware_config,
    status_report,
    end_output_mids, // insert new outputs before here

    first_internal_mid = higher16((uint32_t)end_output_mids) + 1,
    // define internal mids below.
    quit, // sent to any thread's queue to request that it quit.
    end_internal_mids
};

} // namespace NS

int main(int argc, char** argv)
{
    cout << "end_input_mids:" << higher16((uint32_t) NS::mid::end_input_mids) << endl;
    cout << "first_output_mid :" << higher16((uint32_t) NS::mid::first_output_mid ) << endl;
    cout << "input:";
    for (int x = (int)NS::mid::first_input_mid; x < (int)NS::mid::end_input_mids; x++)
    {
        cout << (int) x << " : ";
    }
    cout << endl;
    cout << "output:";
    for (int x = (int)NS::mid::first_output_mid; x < (int)NS::mid::end_output_mids; x++)
    {
        cout << (int) x << " : ";
    }
    cout << endl;
    cout << "also" << endl;
    for (int x = (int)NS::mid::first_internal_mid; x < (int)NS::mid::end_internal_mids; x++)
    {
        cout << (int) x << " : ";
    }
    cout << endl;
    int qs = create_sqs();
    cout << "Queues:" << qs << ", sqs:" << sizeof(queue_stuff) << endl;
    for (auto xq : q_map) {
        cout << xq.first << ':' << xq.second << endl;
    }
}


