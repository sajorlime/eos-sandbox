#include <thread>
#include <chrono>
#include <iostream>
#include <vector>

using namespace std;
 

volatile int seconds;
volatile int ticks;

constexpr int sleep = 10;
constexpr int sdiv = 1000 / sleep;

void threadFunc(int limit, bool* stop)
{
    ticks = 0;
	while (true)
	{
		// Print Thread ID and Counter i
		//std::cout<<std::this_thread::get_id()<<" :: ";
 
		// Sleep this thread for 100 MilliSeconds
		std::this_thread::sleep_for(std::chrono::milliseconds(sleep));
        ++ticks;
        if ((ticks % sdiv) == 0)
            seconds++;
        if (ticks > limit) {
            //cout << ticks << ' ' << flush;
            break;
        }
	}
    *stop = true;
    cout << seconds << "end-ticks:" << ticks << endl;
    //seconds++;
}
using tres = std::chrono::microseconds;
using ttype = std::chrono::time_point<std::chrono::steady_clock>;
using sclock = std::chrono::steady_clock;
using dmsg = std::pair<std::string, ttype>;

std::vector<dmsg> _debugv;
 
int main()
{
    auto start = sclock::now();
    volatile int i = 0;
    while (i < (1 << 25)) i++;
    auto np = dmsg("abc", sclock::now());
    cout << np.first
         << " @ "
         << std::chrono::duration_cast<tres>(np.second - start).count()
         << endl;
    while (i < (1 << 23)) i++;
    _debugv.push_back(dmsg("xyz", sclock::now()));
    cout << _debugv[0].first
         << " @ "
         << std::chrono::duration_cast<tres>(_debugv[0].second - np.second).count()
         << endl;
    bool stop = false;
	std::thread th(&threadFunc, 200, &stop);
    int lsecs = seconds;
    while (!stop) {
        while ((lsecs == seconds))
        {
            if (stop)
                break;
        }
        lsecs = seconds;
        cout << stop << ':';
        cout << seconds << ':' << ticks << " .. " << std::flush;
    }
    cout << "stop:"  << stop << endl;
	th.join();
	return 0;
}

