#include <iostream>
#include <istream>
#include <fstream>
#include <utility>
#include <map>
#include <string>

#include "minijson_reader.hpp"

struct limit_values {
    std::string under;
    std::string over;
};

std::map<std::string, limit_values> lv_map;


struct limits {
    double min;
    double max;
    std::string etype;
    std::string under;
    std::string over;
};


struct subsystem {
    std::map<std::string, limits> lmap;
};

using namespace minijson;

std::map<std::string, subsystem*> subsystems;

void init_maps()
{
    subsystems["ess"] = new subsystem();
    subsystems["hub+"] = new subsystem();
    subsystems["battery"] = new subsystem();
}


//void parse_limit(Context& ctx, char& c bool& must_read)
//{
//}

void convert_istream(std::istream& istrm)
{
    istream_context ctx(istrm);

    parse_object(ctx, [&](const char* k, value v)
    {
        dispatch (k)
        <<"etype_map">> [&]
        {
            //std::cout << "TM:" << ':' << k << ':' << v.as_string() << std::endl;
            parse_object(ctx, [&](const char* pk, value v)
            {
                //std::cout << "TM:" << pk << ':' << v.as_string() << std::endl;
                limit_values lv;
                //std::string u;
                //std::string o;
                dispatch (k)
                <<any>> [&] {
                    parse_object(ctx, [&](const char* k, value v)
                    {
                        dispatch (k)
                        <<"under">> [&] {
                            //std::cout << "tm:" << k << ':' << v.as_string() << std::endl;
                            lv.under = v.as_string();
                        }
                        <<"over">> [&] {
                            //std::cout << "tm:" << k << ':' << v.as_string() << std::endl;
                            lv.over = v.as_string();
                        };
                    });
                    //std::cout << "TM:" << pk << ':' << lv.under << ':' << lv.over << std::endl;
                    lv_map[pk] = lv;
                };
            });
            //ignore(ctx);
        }
        <<"subsystems">> [&]
        {
            //std::cout << "S:" << k << ':' << v.as_string() << std::endl;
            parse_array(ctx, [&](value v)
            {
                subsystem* ss = nullptr;
                //std::cout << "A:" << v.as_string() << std::endl;
                parse_object(ctx, [&](const char* k, value v)
                {
                    dispatch (k)
                        <<"name">> [&] {
                            ss = subsystems[v.as_string()];
                            //std::cout << "Name:" << k << ':' << v.as_string() << ':' << ss << std::endl;
                            ignore(ctx);
                        }
                        <<"limits">> [&]{
                            //std::cout << "limits:" << v.as_string() << ':' << ss << std::endl;
                            parse_object(ctx, [&](const char* k, value v) {
                                if (v.type() == Array) {
                                    //std::cout << "IS ARRAY:" << v.as_string() << ':' << k << std::endl;
                                    parse_array(ctx, [&](value v)
                                    {
                                        //const char* p = k;
                                        dispatch (k)
                                        <<any>> [&] {
                                            std::cout << "L:" << k << ':' << v.as_string() << std::endl;
                                            limits l;
                                            parse_object(ctx, [&](const char* k, value v) {
                                                dispatch (k)
                                                <<"min">> [&] {
                                                    l.min = v.as_double();
                                                    //std::cout << p << ":n:" << k << ':' << v.as_string() << std::endl;
                                                }
                                                <<"max">> [&] {
                                                    l.max = v.as_double();
                                                    //std::cout << p << ":x:" << k << ':' << v.as_string() << std::endl;
                                                }
                                                <<"etype">> [&] {
                                                    l.etype = v.as_string();
                                                    //std::cout << p << ":t:" << k << ':' << v.as_string() << std::endl;
                                                };
                                            });
                                            std::cout << "k:" << k << ':' << l.min << ',' << l.max << ',' << l.etype << std::endl;
                                            ss->lmap[k] = l;
                                        };
                                    });
                                    //ignore(ctx);
                                }
                                else {
                                    dispatch (k)
                                    <<any>> [&] {
                                        //std::cout << "L:" << k << ':' << v.as_string() << std::endl;
                                        limits l;
                                        parse_object(ctx, [&](const char* k, value v) {
                                            dispatch (k)
                                            <<"min">> [&] {
                                                l.min = v.as_double();
                                                //std::cout << "n:" << k << ':' << v.as_string() << std::endl;
                                            }
                                            <<"max">> [&] {
                                                l.max = v.as_double();
                                                //std::cout << "x:" << k << ':' << v.as_string() << std::endl;
                                            }
                                            <<"etype">> [&] {
                                                l.etype = v.as_string();
                                                //std::cout << "t:" << k << ':' << v.as_string() << std::endl;
                                            };
                                        });
                                        ss->lmap[k] = l;
                                    };
                                }
                            });
                        }
                        <<any>> [&] { ignore(ctx); };
                });
            });
        };
    });
}





int main(int argc, char** argv)
{
    if (argc <= 1)
        return 0;
    init_maps();
    std::ifstream jfile(argv[1], std::ifstream::in);
    if (!jfile.is_open()) {
        std::cout << "Not open:" << argv[1] << std::endl;
        return -1;
    }
    convert_istream(jfile);
    //std::cout << "size lv_map:" << lv_map.size() << std::endl;
    for (auto& mtype : lv_map)
    {
        std::cout << "TM:" << mtype.first
                  << "{u: " << mtype.second.under << ", "
                  << "o: " << mtype.second.over
                  << "}" << std::endl;
    }
    for (auto& ss : subsystems)
    {
       std::cout << "SS:" << ss.first << ':' << ss.second << std::endl;
       for (auto li : ss.second->lmap) {
           limits l = li.second;
           std::cout <<  ss.first << "L:" << li.first << ':' << l.min << '<' << l.max << std::endl;
       }
    }
}

