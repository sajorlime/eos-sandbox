cat /proc/cmdline
sudo -u sugarcube bash
# set 8-bit, db8 to db15
pushd /sys/class/gpio/
echo 15 > export
sleep 1
echo out > gpio15/direction
sleep 1
echo 1 > gpio15/value  # or 0 if you want it low
sleep 1
echo 15 > unexport
popd

pushd /sys/class/gpio/
echo 14 > export
sleep 1
echo out > gpio14/direction
sleep 1
echo 1 > gpio14/value  # or 0 if you want it low
sleep 1
echo 14 > unexport
popd

# reset
pushd /sys/class/gpio/
echo 14 > export
sleep 1
echo out > gpio14/direction
sleep 1
echo 1 > gpio14/value  # or 0 if you want it low
sleep 1
echo 14 > unexport
popd
# unreset
pushd /sys/class/gpio/
echo 14 > export
sleep 1
echo out > gpio14/direction
sleep 1
echo 0 > gpio14/value  # or 0 if you want it low
sleep 1
echo 14 > unexport
popd

#unbind
echo -n "fb_display.0" >  /sys/bus/platform/drivers/fb_display/unbind

#bind
echo -n "fb_display.0" >  /sys/bus/platform/drivers/fb_display/bind

fbset -fb /dev/fb1 -i -v


sudo bash
set -o vi

busybox find /sys | busybox grep -i display

sudo bash
set -o vi
dmesg | busybox less

sudo bash
set -o vi
dmesg | busybox egrep "XEPR|fb|display"

/opt/sugarcube/src/sugarcube.py system rebind_fb

dd if=/dev/urandom of=/dev/fb1 bs=1024 count=320

put a pullup on IM0

cat /sys/module/fbtft/parameters/dma
echo 0 >  /sys/module/fbtft/parameters/dma

dd if=/dev/urandom of=/dev/fb1 bs=1024 count=320

open("/dev/fb1", "wb").write("\xf0\x00" * (320*240))


mv /tmp/fim /run/user/1000/
chmod +x /run/user/1000/fim

on host:

cat /path/to/fim  | ssh sweet@SC "cat > /tmp/fim"

on cube:

sudo bash
set -o vi

mv /dev/fb1 /dev/fb0

mv /tmp/fim /run/user/1000/
chmod +x /run/user/1000/fim

sudo bash
set -o vi
dmesg | busybox egrep "XEPR|fb|display"

sudo bash
set -o vi

echo -n "fb_display.0" >  /sys/bus/platform/drivers/fb_display/unbind
echo -n "fb_display.0" >  /sys/bus/platform/drivers/fb_display/bind

rm /dev/fb0
ln /dev/fb1 /dev/fb0

/run/user/1000/fim  --device /dev/fb0 /opt/sugarcube/src/static/sugarcube_logo.png
/run/user/1000/fim  --device /dev/fb0 /opt/sugarcube/src/static/*.png

