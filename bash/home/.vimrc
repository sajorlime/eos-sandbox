set shiftwidth=4
set tabstop=4
set expandtab
set tags=tags,sh-tags,py-tags,h-tags,c-tags,html-tags,css-tags,sh-tags,xc-tags,cpp-tags,css-tags,s-tags,/home/eo/XMOS/xTIMEcomposer/Community_14.3.0/h-tags

set ai
set noswapfile
syn keyword type uint uchar byte uint32 uint16 uint8

function ShowSpaces(...)
  let @/='\v(\s+$)|( +\ze\t)'
  let oldhlsearch=&hlsearch
  if !a:0
    let &hlsearch=!&hlsearch
  else
    let &hlsearch=a:1
  end
  return oldhlsearch
endfunction

function TrimSpaces() range
  let oldhlsearch=ShowSpaces(1)
  execute a:firstline.",".a:lastline."substitute ///gec"
  let &hlsearch=oldhlsearch
endfunction

function StripTrailingWhitespace()
  if !&binary && &filetype != 'diff'
    normal mz
    normal Hmy
    %s/\s\+$//e
    normal 'yz<CR>
    normal `z
  endif
endfunction



