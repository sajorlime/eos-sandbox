colorscheme desert
set autoindent
set tabstop=4
set shiftwidth=4
set showmatch
set report=2
set showmatch
set expandtab 
set noignorecase
set ruler
set showmode
set tagrelative
set tagstack
set noerrorbells
set autoindent
set autowrite
set syntax=OFF
set tags=tags,py-tags,sh-tags,h-tags,c-tags,cc-tags,cs-tags,s-tags,csv-tags,txt-tags,json-tags,js-tags,mk-tags,yml-tags,/usr/include/all-tags,$HOME/src/py/py-tags,$HOME/src/sh-tags,using-tags,bb-tags,conf-tags,inc-tags
set tagrelative
set tagstack
set noerrorbells
set autoindent
set autowrite
set updatecount=0
set bomb
set cmdheight=2
set laststatus=2
set nowrap
"set clipboard^=unnamedplus
"set clipboard=unnamedplus
syntax enable
"let g:is_bash = 1
set lazyredraw
"display incomplete commands
set showcmd
"always display the status line
set laststatus=2
"highlight current line
set cursorline
"display text width column
set colorcolumn=80
"new splits will be at the bottom or to the right side of the screen
"set splitabove
set splitright
set fileformat=unix
set hlsearch
set fileencoding=latin1

"use netrw?
"set nocp
"filetype plugin on xfoobar

setlocal spell spelllang=en_us
set nospell
"set spelloptions=camel
set modeline

set mouse=v
"set switchbuf=usetab,newtab
"let NERDTreeSortOrder=['\.cpp$', '\.hpp$']

command! VTerm vertical terminal


"execute pathogen#infect()
"call pathogen#helptags()
