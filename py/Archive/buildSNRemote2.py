#!/usr/bin/env python3
""" buildSNRemote2 Send list for installation on server
JohnB 20May08
"""

import sys
import urllib.request, urllib.error, urllib.parse
from urllib.parse import urlencode
from urllib.parse import urlparse
from getpass import getpass
import zipfile
import io
import time

def Spaced(seq, sep=" "):
    return sep.join(str(x) for x in seq)

def Tabbed(seq):
    return Spaced(seq, sep="\t")

def do_psswrd(url,username=None,password=None):
    """ Install the username and password info for this url

    Ask the user if username or password are None
    """
    if username is None or password is None:
        print("Username:")
        username = sys.stdin.readline().strip()
        password = getpass("Password:")
    top_level_url = "http://" + urlparse(url)[1]
    password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
    password_mgr.add_password(None, top_level_url, username, password)
    handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
    opener = urllib.request.build_opener(handler)
    urllib.request.install_opener(opener)

########## This is the key function ##############
def make(url, name, template, **tables):
    """ Get the VSDB files made
    
    url: the URL of the CGI script that makes the Task
    name: the name to give to the VMB, ODL and GML
    template: the template as bytes.
        Note that the template can only refer to one table
        and that table must be referred to as "table"
    table: The table, as bytes

    Return the bytes of a logfile
    """
    vars = dict(name=name,
                template=template,
                **tables)
    if pr:
        print('make:')
        print(' url:',url)
        print(' vars:')
        for k in sorted(vars):
            print(' ',k,len(vars[k]))
    return urllib.request.urlopen(url, urlencode(vars))


def parselog(loglines):
    """Parse (some of) the line of a (successful) logfile

    returns tuple (ops, guessed) where:
    ops is a dict of times
    guessed is a dict of pronguessed words
    """
    ops = dict()
    guessed = dict()
    loglines = iter(loglines)
    for line in loglines:
        if line.endswith("automatically generated:"):
            for line in loglines:
                if not line: break
                word, pron = line.split("=", 1)
                guessed[word.strip()] = pron.strip()
        if line.endswith("seconds") and " took " in line:
            op, secs = line.split("took", 1)
            op = op.strip()
            secs = float(secs.split("seconds")[0])
            ops[op] = secs
    return ops, guessed

url="https://aviga.novaurisnet.com/builder/buildAndInstall.py"
username="aviga"
password="sFg34%LcA"

def dsn_remote( name, template, table=None,
          wordprons=None, logfile=None,
          pr=0):
    """
    name: the name of the task to build the grammar for
    template: the name of the template file.
        Note that the template can only refer to one table
        and that table must be referred to as "table"
    table: The name of the table file, or None
    wordpron:  The name of a file of alternative pronunciations, or None.
        Used in place of those in the GML or produced by the pronguesser.
        Each line of the wordprons file should comprise
            the word for which the pronunciation is being overridden,
            a tab character
            and then the new pronunciation.
        The pronunciation should take the form of one or more words
        and can be a simple regular expression (SRE).
        All words used in the pronunciation should be in the GML
        - no attempt will be made to pronguess them.
        The pronguesser will not use these pronunciations for words
        that still need to be guessed.    
    """
    logfile = logfile or name+'.log'
    if pr: print("name=%s template=%s table=%s log=%s"%(name,template,table,logfile))
    do_psswrd(url,username=username,password=password)
#    print "thanks"
    if pr: print("password done")
    starttime=time.time()
    template = file(template).read()
    if pr: print("templates:",len(template))
    if table:
        table_bytes = file(table).read()
        if pr: print("table data:",len(table_bytes))
        result = make( url, name, template=template, table=table_bytes)
    else:
        result = make( url, name, template)

    result = result.read()
    if pr: print("Result data:\n",len(result))

    # Write the log to a file
    log = open(logfile,'w')
    print(result, file=log)
    log.close()

    loglines = result.splitlines()
    successline = name+' updated on the server.'
    for line in loglines:
        if line.startswith(successline):
            print(line)
            break
    else:
        print("There seems to have been a problem!")
        for line in loglines:
            print(line)
        return

    print("Elapsed time: %4.2f seconds"%(time.time()-starttime))
    ops, guessed = parselog(loglines)
    print("Guessed", len(guessed), "words")

clpfunctions = "main test".split()
pr=0
if __name__ == "__main__":
    import nlib.clp

