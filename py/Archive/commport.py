#!/usr/bin/env python3
#
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

"""
    commport.py write/read comm port
    
"""

__author__ = 'erojas@gmail.com (Emilio Rojas)'


import argparse
import os
import os.path
import re
import sys
import subprocess
import textwrap
import serial
import io

import myreadline

rline = myreadline.myreadline

def write_read_serial(ser):
    while True:
        print('input:', end='')
        s = rline()
        if s in ['x', 'X']:
            break
        #print('S:', s)
        ser.write(s)
        s = ser.read(10)
        print('S:', s)


if __name__=='__main__':
    parser = argparse.ArgumentParser(description='factorial')

    parser.add_argument('port',
                        type=int,
                        default=3,
                        help='comm port to open and then read/write')
    args = parser.parse_args()

    ser = serial.Serial('/dev/com%d' % args.port, 115200, timeout=1)
    print('SER:', ser)
    write_read_serial(ser)


