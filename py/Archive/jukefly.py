#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

"""
    jukefly
    contains classes useful to jukfly data apps.
    
    See usage.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import os
import os.path
import pickle
#from sets import Set



class Song:
    """
    The Song class represents all of the data we can extract
    from a song_info XML description.
    <?xml version="1.0" encoding="UTF-8"?>
    <response>
      <song>
        <artist>Carpenters</artist>
        <artist-id>752</artist-id>
        <genre-id>16</genre-id>
        <album-id>1085</album-id>
        <genre>Pop</genre>
        <album>Billboard Top 100 of 1971</album>
        <song-id>2988</song-id>
        <song>Superstar</song>
      </song>
      <result>success</result>

    </response>

    Members:
        _owner:
        _artist:
        _artist_id:
        _song_id:
        _genre:
        _genre_id:
        _album:
        _title:
    """
    def __init__(self, song_node, uid):
        """
        The constructor takes an XML song node and extract
        the info.  The user id initializes the owners list.
        Paramters:
            song_node: a JukeFly XML song info node.
            uid: the user id that had this as a song.
        """
        #print 'Sinit: %s' % song_node
        self._owners = set([uid])
        self._artist = None
        self._artist_id = None
        self._song_id = None
        self._genre = None
        self._genre_id = None
        self._album = None
        self._album_id = None
        self._title = None
        for node in song_node.childNodes:
            if node.nodeName == 'artist':
                if node.firstChild:
                    self._artist = node.firstChild.data
            elif node.nodeName == 'artist-id':
                if node.firstChild:
                    self._artist_id = node.firstChild.data
            elif node.nodeName == 'song-id':
                if node.firstChild:
                    self._song_id = node.firstChild.data
            elif node.nodeName == 'genre':
                if node.firstChild:
                    self._genre = node.firstChild.data
            elif node.nodeName == 'genre-id':
                if node.firstChild:
                    self._genre_id = node.firstChild.data
            elif node.nodeName == 'album':
                if node.firstChild:
                    self._album = node.firstChild.data
            elif node.nodeName == 'album-id':
                if node.firstChild:
                    self._album_id = node.firstChild.data
            elif node.nodeName == 'song':
                if node.firstChild:
                    self._title = node.firstChild.data
        return

    def artist(self):
        return self._artist

    def artist_id(self):
        if self._artist_id:
            return self._artist_id
        return -1

    def song_id(self):
        if self._song_id:
            return self._song_id
        return -1

    def genre(self):
        return self._genre

    def genre_id(self):
        if self._genre_id:
            return self._genre_id
        return -1

    def album(self):
        return self._album

    def album_id(self):
        if self._album_id:
            return self._album_id
        return -1

    def title(self):
        return self._title

    def owners(self):
        ol = list(self._owners)
        return ol

    def num_owners(self):
        return len(self._owners)

    def inc_owners(self, uid):
        self._owners.add(uid)
        return self._owners


class Songs:
    """
    The Songs class contians the dictionary of songs and
    supports operations on this dictionary.
    """
    def __init__(self, pickle_file = None):
        if pickle_file:
            try:
                plf = open(pickle_file, 'r')
                self._song_dict = pickle.load(plf)
                plf.close()
            except (EOFError, IOError):
                print('STARTING with empty Songs')
                self._song_dict = {}
        else:
            self._song_dict = {}
        self._count = 0

    def save(self, pickle_file = None):
        if pickle_file:
            pdf = open(pickle_file, 'w')
            #print pdf
            pickle.dump(self._song_dict, pdf)
            pdf.close()


    def songs(self):
        return self._song_dict

    def new_id(self, song_id):
        """
            Return true iff song ids in not in dictionary.
        """
        return song_id not in self._song_dict

    def get_id(self, song_id):
        """
            Return song from dictionary
        """
        if song_id not in self._song_dict:   
            return none
        return self._song_dict[song_id]

    def inc_id(self, song_id, uid):
        """
            adds uid to the list of ids that own this song.
        """
        if song_id not in self._song_dict:   
            print('inc ids:%s not in dict' % ids)
            return None
        song = self._song_dict[song_id]
        return song.inc_owners(uid)

    def add_song(self, song_id, asong):
        """
            adds the song info the the list of songs
        """
        if song_id in self._song_dict:
            return
        self._song_dict[song_id] = asong

    def artists(self):
        """
            returns a sorted list of unique artists assoicated with these songs.
        """
        dartists = {}
        for song in list(self._song_dict.values()):
            artist = song.artist()
            id = song.artist_id()
            if artist and artist != '':
                dartists[artist] = (artist, id)
        artists = list(dartists.keys())
        artists.sort()
        return artists

    def titles(self):
        """
        returns a sorted list of unique titles assoicated with these songs.
        """
        dtitles = {}
        for song in list(self._song_dict.values()):
            title = song.title()
            id = song.song_id()
            if title and title != '' :
                dtitles[title] = (title, id)
        titles = list(dtitles.keys())
        titles.sort()
        return titles

    def genres(self):
        """
        returns a sorted list of unique geners assoicated with these songs.
        """
        dgenres = {}
        for song in list(self._song_dict.values()):
            genre = song.genre()
            id = song.genre_id()
            if genre and genre != '' :
                dgenres[genre] = (genre, id)
        genres = list(dgenres.keys())
        genres.sort()
        return genres

    def albums(self):
        """
        returns a sorted list of unique albums assoicated with these songs.
        """
        dalbums = {}
        for song in list(self._song_dict.values()):
            album = song.album()
            id = song.album_id()
            if album and album != '' :
                dalbums[album] = (album, id)
        albums = list(dalbums.keys())
        albums.sort()
        return albums


