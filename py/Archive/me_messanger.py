#!/usr/bin/env python3
#

'''
    Copyright (C) 2020 oneNav, Inc. All rights reserved.

    me_messanger.py a couple of cooperating threads for sending and
    receiving message from the ME over USB.
'''

__author__ = 'emilior@onenav.com (Emilio Rojas)'


import argparse
import datetime
import io
import os
import sys
import serial
import time
import threading
import random

from signal import signal, SIGINT
from sys import exit

import csv_filter
from mestruct import MeStruct, read_struct

_reader_complete = None

# global flags that indicate the thread is running
_reader=True
_writer=True

def sig_handler(signal_received, frame):
    global _reader, _writer
    # Handle any cleanup here
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    ser.close()
    _reader = False
    _writer = False
    #exit(0)

# output files
_ofile = sys.stdout
# logfile
_lfile = sys.stdout

# outut values
_last_time = time.time()
_first_time = time.time()

# adjust time values
_week_num = 0
_at_delta = 0

def hrms(dtime):
    dsecs = int(dtime)
    mins = int(dsecs / 60)
    dsecs = dsecs - (60 * mins)
    hrs = int(mins / 60)
    mins = mins - (60 * hrs)
    #return '%.3f -> %03d:%02d:%02d' % (dtime, hrs, mins, dsecs)
    return '%03d:%02d:%02d' % (hrs, mins, dsecs)


def write_out(mid, *args):
    global _first_time

    if mid == None:
        return
    print(mid, *args, hrms(time.time() - _first_time), file=_ofile)
    _ofile.flush()

''' read a big-endian word
'''
def read4(data):
    ival = (data[0] << 0) \
             | (data[1] << 8) \
             | (data[2] << 16) \
             | (data[3] << 24)
    return ival

''' read a big-endian short
'''
def read2(data):
    ival = (data[0] << 0) \
            | (data[1] << 8) 
    return ival

''' read a big-endian short
'''
def read1(data):
    return data[0]

'''
Receive handers go here.  The have the form <tag>_handler.
RcvData: <start><length><tag><payload>
    start: 0x01
    length: is the length of data to be read.
    tag: is a two character abbreviation that represents the message type
    payload: the big-endian packed data structure.
'''


''' handler for any unknown but defined received commands
    @param rx_data is binary data received over the serial port.
'''
def unknown_bytes_handler(rx_data):
    print('UBH:', end='')
    return rx_data

''' handler for any unexpected received commands
    @param rx_data is binary data received over the serial port.
'''
def error_handler(rx_data):
    print('UEXPECTED TYPE')
    unknown_bytes_handler(rx_data)
    sys.exit(3)


''' handles the reception of time-report messages
    @param rx_data is binary data received over the serial port.
'''
def tr_handler(rx_data):
    global _last_time, _first_time
    #print(rx_data, end=':')
    unadj = read4(rx_data)
    adj = read4(rx_data[4:])
    if adj == 0:
        adj = 1
    wn = read2(rx_data[6:])
    rr = read2(rx_data[8:])
    ms = unadj % 1000
    secs = (unadj - ms) / 1000
    mins = int(int(secs) / 60)
    secs -= 60 * mins
    return ('tr',
            '%3d' % mins,
            '%2d' % secs,
            '%04d' % ms,
            adj,
            wn)

''' handles the reception of strobe messsages
    @param rx_data is binary data received over the serial port.
'''
def sb_handler(rx_data):
    global _last_time, _first_time, _at_delta
    #print(rx_data, end=':')
    seq = read1(rx_data)
    unadj = read4(rx_data[1:])
    adj = read4(rx_data[5:])
    _at_delta = unadj - adj
    if adj == 0:
        adj = 1
    wn = read2(rx_data[9:])
    ms = int(unadj % 1000)
    secs = (unadj - ms) / 1000
    mins = int(int(secs) / 60)
    secs -= int(60 * mins)
    hrs = mins / 60
    mins -= 60 * hrs
    return (('sb:%02x' % seq,
            'ut: %08x'  % unadj,
            'at: %08x'  % adj,
            'wn: %d'  % wn),
            '%d:%02d:%02d:%03d' % (hrs, mins, secs, ms))

class MeasData(object):
    def __init__(self, rx_data):
        #print('MDo')

        self._sequence = read1(rx_data[0:]);
        self._system = read1(rx_data[1:])
        self._prn = read1(rx_data[2:]);
        self._channel_unadjusted_timer = read4(rx_data[3:]);
        self._code_phase_0 = read4(rx_data[7:]);
        self._codephase_sum_of_sums = read4(rx_data[11:]);
        self._codephase_sum_of_sums_count = read2(rx_data[15:]);
        self._channel_doppler_sum = read4(rx_data[17:]);
        self._channel_doppler_count = read1(rx_data[21:]);
        self._channel_snr_count = read1(rx_data[22:]);
        self._channel_start_carrier_phase = read4(rx_data[23:]);
        self._channel_end_carrier_phase = read4(rx_data[27:]);
        self._channel_carrier_cycles = read4(rx_data[31:]);
        self._carrier_phase_discriminator_sum = read2(rx_data[35:]);
        self._carrier_phase_discriminator_sum2 = read2(rx_data[37:]);
        self._punctual_power_sum = read4(rx_data[39:]);
        self._noise_power_sum = read4(rx_data[43:]);

    def pretty(self):
        s = ''.join([
                     '_seq:%d\n',
                     '_system:%d\n',
                     '_prn:%d\n',
                     '_channel_unadjusted_timer:%d\n',
                     '_code_phase_0:%d\n',
                     '_codephase_sum_of_sums:%d\n',
                     '_codephase_sum_of_sums_count:%d\n',
                     '_channel_doppler_sum:%d\n',
                     '_channel_doppler_count:%d\n',
                     '_channel_snr_count:%d\n',
                     '_channel_start_carrier_phase:%d\n',
                     '_channel_end_carrier_phase:%d\n',
                     '_channel_carrier_cycles:%d\n',
                     '_carrier_phase_discriminator_sum:%d\n',
                     '_carrier_phase_discriminator_sum2:%d\n',
                     '_punctual_power_sum:%d\n',
                     '_noise_power_sum:%d\n',
                    ])
        return s % (
                    self._sequence,
                    self._prn,
                    self._channel_unadjusted_timer,
                    self._code_phase_0,
                    self._codephase_sum_of_sums,
                    self._codephase_sum_of_sums_count,
                    self._channel_doppler_sum,
                    self._channel_doppler_count,
                    self._channel_snr_count,
                    self._channel_start_carrier_phase,
                    self._channel_end_carrier_phase,
                    self._channel_carrier_cycles,
                    self._carrier_phase_discriminator_sum,
                    self._carrier_phase_discriminator_sum2,
                    self._punctual_power_sum,
                    self._noise_power_sum,
                   )


    def __str__(self):
        s = ''.join(['{',
                     '_seq:%02x, ',
                     '_sys:%d, ',
                     'prn:%d, ',
                     '%x, ',
                     '%d, ',
                     '%d, ',
                     '%d, ',
                     '%d, ',
                     '%d, ',
                     '%d, ',
                     '%d, ',
                     '%d, ',
                     '%d, ',
                     '%d, ',
                     '%d, ',
                     '%d, ',
                     '%d}',
                    ])
        return s % (
                    self._sequence,
                    self._system,
                    self._prn,
                    self._channel_unadjusted_timer,
                    self._code_phase_0,
                    self._codephase_sum_of_sums,
                    self._codephase_sum_of_sums_count,
                    self._channel_doppler_sum,
                    self._channel_doppler_count,
                    self._channel_snr_count,
                    self._channel_start_carrier_phase,
                    self._channel_end_carrier_phase,
                    self._channel_carrier_cycles,
                    self._carrier_phase_discriminator_sum,
                    self._carrier_phase_discriminator_sum2,
                    self._punctual_power_sum,
                    self._noise_power_sum,
                   )

''' handles the reception of time-report messages
    @param rx_data is binary data received over the serial port.
'''
def md_handler(rx_data):
    try:
        me = MeasData(rx_data)
    except IndexError as iex:
        #print('IndexError:', iex)
        return '%s' % iex
    return me

''' handles log mesages
    @param rx_data is binary data received over the serial port.
'''
def lg_handler(rx_data):
    try:
        logmsg =  rx_data.decode('utf-8')
        print(logmsg, end='', file=_lfile)
    except:
        print('LG: decode error', rx_data, file=_lfile)
    _lfile.flush()
    return None

_max_msg_size = 100

''' A class represents the data by id.
    it holds the, long name, the tag and its associated numeric id, 
    and the size of the transmitted object.
    These are created when the mid.csv files is process and placed
    in dictionaries to allow easy processing of data.
'''
class Mid(object):
    def __init__(self, mid, value, tag, size):
        global _max_msg_size
        try:
            self._mid = mid.strip()
            self._value = int(value)
            self._tag = tag.strip().encode('ascii')
            self._size = int(size)
            if tag:
                try:
                    self._handler  = eval('%s_handler' % tag)
                    #print('Found handler:', self._handler)
                except NameError:
                    #print('Found bypes:', tag)
                    self._handler = unknown_bytes_handler
            else:
                #print('Empty tag error:', tag)
                self._handler = error_handler
            _max_msg_size = max(_max_msg_size, self._size)
        except:
            print('Unexpected error:', sys.exc_info()[0])
            print(mid, value, tag, size, type(tag))
            #sys.exit(1)

    def mid(self):
        return self._mid
 
    def value(self):
        return self._value

    def tag(self):
        return self._tag

    def size(self):
        return self._size

    def handler(self):
        return self._handler

    def __str__(self):
        h = 'H'
        if self._handler == unknown_bytes_handler:
            h = 'B'
        elif self._handler == error_handler:
            h = 'E'
        return '{%s, %d, %s, %d, %s}' % (self._mid,
                                     self._value,
                                     self._tag,
                                     self._size,
                                     h)

# output methods should be placed below here.

# store four bytes in big-endian order output byte-array
def write4(odata, v):
    assert(type(odata) == bytearray)
    odata.append((v >> 24) & 0xff)
    odata.append((v >> 16) & 0xff)
    odata.append((v >> 8) & 0xff)
    odata.append((v >> 0) & 0xff)

# store two bytes in big-endian order output byte-array
def write2(odata, v):
    assert(type(odata) == bytearray)
    odata.append((v >> 8) & 0xff)
    odata.append((v >> 0) & 0xff)

''' add the header to the output data
    @param odata byte array we will send
    @param mid is an Mid class instances that holds info we need for the header.
    @return updates odata
'''
def add_header(odata, mid):
    odata.append(0x01)
    siz = mid.size() + len(mid.tag())
    odata.append(int(siz))
    odata.append(mid.tag()[0])
    odata.append(mid.tag()[1])

''' pre_position message writer
    @param ser is the port with a write method.
    @param mid, is an instance of the Mid class
    @param args, is the command  arguments
    @param param an unused parameter to modify the methods behavior
    @return the bytes written and the output data
'''
def pp_handler(ser, mid, args, params=None):
    pp = read_struct(args.pp, args.ppi)
    #print(pp)
    odata = bytearray();
    add_header(odata, mid)
    odata = pp.out_data(odata)
    l = len(odata)
    #print(l)
    w = ser.write(odata)
    return w, odata

''' adjust time  message writer
    @param ser is the port with a write method.
    @param mid, is an instance of the Mid class
    @param args, is the command  arguments
    @param ms is used to override the delta-ms sent to the ME
    @return the bytes written and the output data
'''
def at_handler(ser, mid, args, ms=None):
    global _at_delta
    week_num = datetime.date.today().isocalendar()[1]
    odata = bytearray();
    add_header(odata, mid)
    if ms:
        write4(odata, ms)
    else:
        write4(odata, _at_delta)
        _at_delta = 0
    write2(odata, week_num)
    w = ser.write(odata)
    return w, odata


''' report time message writer
    @param ser is the port with a write method.
    @param mid is an instance of the Mid class
    @param args is the command  arguments
    @return the bytes written and the output data
'''
def rt_handler(ser, mid, args):
    odata = bytearray();
    add_header(odata, mid)
    w = ser.write(odata)
    return w, odata


''' looks up Mid and gets the appropriate write handler and calls it.
    @param ser is the port with a write method.
    @param sendif is a string that is used to look up an Mid
    @param args is the command  arguments
    @return the bytes written and the output data
'''
def send_cmd(ser, sendid, args):
    global _idict, _tdict
    print('SC:', sendid, file=_ofile)
    mid = _idict[sendid]
    handler = mid.handler()
    return handler(ser, mid, args)

busy_str = '-\\|/'

''' in a single thread writes, then, read, then write, then reads
        Sends a report-time, receives time-report, then sens an adjust-time
        and reads the second time-report.
    @param ser is the port with a write method.
    @param sendif is a string that is used to look up an Mid
    @param args is the command  arguments
    @return the first bytes written and the output data
'''
def run_test(ser, args):
    mid = _idict['report_time']
    w, oout = rt_handler(ser, mid, args)
    rtuple = read_msg(ser, args)
    print(rtuple, file=_ofile)
    mid = _idict['adjust_time']
    ms = args.dms + rtuple[1]
    at_handlr(ser, mid, args, ms)
    rtuple = read_msg(ser, args)
    print(rtupl, file=_ofile)
    return w, oout

''' writ serial thread method
    @param ser is the port with a write method.
    @param args is the command  arguments
'''
def write_serial(ser, args):
    global _reader_complete, _reader, _writer
    tdtw = 0.0
    twtotal = 0
    _writer = True
    while _writer:
        if _reader_complete:
            w, odata = send_cmd(ser, 'adjust_time', args)
            w, odata = send_cmd(ser, 'pre_position', args)
            w, odata = send_cmd(ser, args.sendid, args)
        _reader_complete = False
        time.sleep(args.sleep)


def read_sync(ser, args):
    global _idict, _tdict, _reader_complete, _reader, _writer
    # lookfor first bytes
    print('FBt wait for sync')
    while True: # infinite loop until we return below
        firstbyte = ser.read(1);
        print('FB1:', firstbyte)
        ifb = firstbyte[0]
        while ifb != 0x01:
            print('FBi:', ifb, ifb == 0x10, ifb == b'\x01')
            firstbyte = ser.read(1);
            print('FB2:', firstbyte)
            ifb = firstbyte[0]
            print(ifb)
        size_tag = ser.read(3); # read the size, + the code
        print('size_tag type:', type(size_tag[0]), size_tag[0])
        print('size_tag bytes:', size_tag)
        msize = size_tag[0]
        raw_id = size_tag[1:3]
        print('SID:', raw_id)
        try:
            mid = _tdict[raw_id]
        except KeyError:
            #print('KeyError:', raw_id)
            continue
        if msize != mid.size():
            print('RS: bad size', msize, mid.size())
            continue
        if msize >= 1 and msize <= _max_msg_size:
            rs = ser.read(msize)
        else:
            continue
        handler = mid.handler()
        print('sync:', handler(rs[2:]))
        break
    return

''' reads a single message
    @param ser is the port with a write method.
    @param args is the command  arguments
'''
def read_msg(ser, args):
    global _idict, _tdict, _reader, _writer, _last_time, _fist_times
    try:
        header  = ser.read(2);
        rsize = header[1]
        rs = ser.read(rsize)
        raw_id = rs[0:2]
    except serial.serialutil.SerialException as se:
        _reader = False
        _writer = False
        return None
    try:
        mid = _tdict[raw_id]
    except KeyError:
        #print('KeyError:', raw_id)
        return raw_id
    try:
        handler = mid.handler()
    except IndexError as iex:
        print('IndexError:', iex)
        return raw_id

    try:
        return handler(rs[2:])
    except TypeError as tex:
        print('TypeError:', tex)
        return raw_id

''' read serial data thread method
    @param ser is the port with a write method.
    @param args is the command  arguments
'''
def read_serial(ser, args):
    global _idict, _tdict, _reader_complete, _reader, _writer, _last_time 
    cnt = 0
    rtotal = 0
    trtotal = 0
    _first_time = time.time()
    _last_time = _first_time
    _reader = True
    while _reader:
        rtup = read_msg(ser, args)
        write_out(rtup)
        _reader_complete = True
        cnt += 1
        _last_time = time.time()
    ser.close()


#constexpr uint32_t mid_span = 32; // distance between message groups
#constexpr uint32_t nextgroup(uint32_t v)
#{
#    auto x = v / mid_span;
#    return mid_span * (x + 1);
#}
def nextgroup(cvalue):
    x = int(cvalue / 32)
    return int(32 * (x + 1))

''' create two dictionaries from the ids file.
    @param ids is the name of the file containing mid values
    @param args is the command  arguments
    #return two dictionaires, one that indexed by the full name
        and a second which is indexed by the tag.
'''
def create_mid_dicts(ids, args):
    cols, csv_reader = csv_filter.csv_open(ids)
    if args.idonly:
        for col in cols:
            print(col, end=' ')
        print('')
    indicies = csv_filter.gen_csv_column_indicies(cols,
                                                  ['mid',
                                                   'value',
                                                   'tag',
                                                   'size'])
    if args.idonly and False:
        print('I', indicies)
    row_filters = csv_filter.gen_csv_row_filters(cols, None)
    if args.idonly and False:
        print('F', row_filters)
    col_xforms = csv_filter.gen_csv_col_xforms(cols, None)
    if args.idonly and False:
        print('X', col_xforms)
    rows = csv_filter.filter_csv(csv_reader,
                                 columns=indicies,
                                 row_filters=row_filters,
                                 col_xforms=col_xforms,)
    first_value = int(rows[0][1])
    last_value = first_value
    mid_name = rows[0][0].strip()
    value = int(rows[0][1].strip())
    tag = rows[0][2].strip()
    size = int(rows[0][3].strip())
    #print('Z', mid_name, value, tag, size)
    mid = Mid(mid_name, value, tag, size)
    tag_dict = {tag : mid}
    item_dict = {mid.mid() : mid}
    for row in rows[1:]:
        mid_name = row[0].strip()
        if not mid_name: # allow empty rows
            continue
        if 'first_' in mid_name: # compute next value
            if 'nextgroup' not in row[1]:
                print('UNEXPECTED Value:', mid_name, row[1])
                sys.exit(1)
            # the enum always has + 1
            value = nextgroup(last_value) + 1
            if args.idonly and False:
                print('NG:', last_value, value, row)
            mid = Mid(mid_name, value, row[2], row[3])
            if mid.tag():
                tag_dict[mid.tag()] = mid
            item_dict[mid_name] = mid
            last_value = value
            continue
        if row[1]: # value exits it must be "next..." or "first..."
            row_value = row[1].strip()
            if 'first_' in row_value:
                if args.idonly and False:
                    print('R1', row_value, item_dict[row_value])
                value = item_dict[row_value].value()
                mid = Mid(mid_name, value, row[2].strip(), int(row[3]))
                if mid.tag():
                    tag_dict[mid.tag()] = mid # replaces first_...
                item_dict[mid_name] = mid
                last_value = value
            else:
                print('Non-first UNEXPECTED Value:%s, %s' % (mid_name, row_value))
                sys.exit()
        else:
            value = last_value + 1
            mid = Mid(mid_name, value, row[2], row[3])
            item_dict[mid_name] = mid
            if mid.tag():
                tag_dict[mid.tag()] = mid
            last_value = value
    return (item_dict, tag_dict)


if __name__=='__main__':
    signal(SIGINT, sig_handler)
    parser = argparse.ArgumentParser(description='send host messages to ME.',
                                     epilog='can run in cygwin or DOS')

    #TODO(epr): add sub-commands if this grows in complexity
    parser.add_argument('--port',
                        type=str,
                        default='S2',
                        help='comm port to open and then read/write, default: %(default)s')
    parser.add_argument('--dos',
                        type=int,
                        default=None,
                        help='DOS comm port, 3 is a good pick')
    parser.add_argument('--ids',
                        type=str,
                        default='mid.csv',
                        help='mid csv file: default: %(default)s')
    parser.add_argument('--idonly',
                        action='store_true',
                        help='dump mids and exit')
    #parser.add_argument('--listenonly', '-L',
    #                    action='store_true',
    #                    help='dump mids and exit')
    parser.add_argument('--run_test', '-t',
                        action='store_true',
                        help='run the canned test, rt, at, pp')
    parser.add_argument('--sendid',
                        type=str,
                        default='report_time',
                        help='command id to send')
    parser.add_argument('--dms',
                        type=int,
                        default=7,
                        help='delta ms: %(default)s')
    parser.add_argument('--wk',
                        type=int,
                        default=17,
                        help='week no, default: %(default)s')
    parser.add_argument('--sleep',
                        type=float,
                        default=5.25,
                        help='sleep time def: %(default)s')
    parser.add_argument('--srep',
                        type=int,
                        default=1,
                        help='send repeat count, default: %(default)s')
    parser.add_argument('--pp', '-p',
                        type=str,
                        default='pp.csv',
                        help='pre-pos file if sendid is pre_position, default: %(default)s')
    parser.add_argument('--ppi', '-i',
                        type=int,
                        default=1,
                        help='pre position index into file, default: %(default)s')
    parser.add_argument('--out',
                        type=str,
                        default='-',
                        help='where to write output "-" goes to stdout: %(default)s')
    parser.add_argument('--log',
                        type=str,
                        default='-',
                        help='where to write log data "-" goes to stdout: %(default)s')
    args = parser.parse_args()

    if args.out == '-':
        _ofile = sys.stdout
    else:
        _ofile  = open(args.out, 'a')
    if args.log == '-':
        _lfile = sys.stdout
    else:
        _lfile  = open(args.log, 'a')
    _idict, _tdict = create_mid_dicts(args.ids, args)
    if args.idonly:
        print('ITEMS')
        for item in _idict:
            print(item, _idict[item])
        print('TAGS')
        for tag in _tdict:
            print(tag, _tdict[tag], _idict[_tdict[tag].mid()])
        print('')
        sys.exit()

    if args.dos:
        ser = serial.Serial('COM%d' % args.dos, 115200, timeout=None)
    else:
        ser = serial.Serial('/dev/tty%s' % args.port, 115200, timeout=None)
    if args.run_test:
        run_test(ser, args)
        ser.close()
        sys.exit(0)
    print('\nStart ME-MESSAGER\n', file=_ofile)
    recvt = threading.Thread(target=read_serial, name='read', args=(ser, args))
    recvt.start()
    #if not args.listenonly:
    sendt = threading.Thread(target=write_serial, name='send', args=(ser, args))
    sendt.start()
    #_writer = False
    #print('waiting for threads to complete')
    recvt.join()
    _reader = False
    #if not args.listenonly:
    sendt.join()
    #_writer = False
    ser.close()


