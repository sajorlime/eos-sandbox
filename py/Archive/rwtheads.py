#!/usr/bin/env python3
#
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

"""
    rwthread.py a couple of threads for reading and writing a come comm port
                in cygwin or linux.
    
"""

__author__ = 'erojas@gmail.com (Emilio Rojas)'


import argparse
import os
import os.path
import re
import sys
import subprocess
import textwrap
import serial
import io
import time
import threading
import random

#import myreadline

#rline = myreadline.myreadline

ostr = '!abcedfghijklmnopqurstvuwxyzABCDEFGHIJkLMNOPQRSTUVWXYZ0123456789'

obytes = b'abcedfghijklmnopqurstvuwxyzABCDEFGHIJkLMNOPQRSTUVWXYZ0123456789' \
         b'abcedfghijklmnopqurstvuwxyzABCDEFGHIJkLMNOPQRSTUVWXYZ0123456789' \
         b'abcedfghijklmnopqurstvuwxyzABCDEFGHIJkLMNOPQRSTUVWXYZ0123456789' \
         b'abcedfghijklmnopqurstvuwxyzABCDEFGHIJkLMNOPQRSTUVWXYZ0123456789' \
         b'abcedfghijklmnopqurstvuwxyzABCDEFGHIJkLMNOPQRSTUVWXYZ0123456789' \
         b'abcedfghijklmnopqurstvuwxyzABCDEFGHIJkLMNOPQRSTUVWXYZ0123456789'
istr = ''

tw = None
tr = None
calc = False

def calc_rate(total, dt):
    rate = 8 * (int(total / dt))
    millions = rate / 1000000
    thousands = (rate / 1000) % 1000
    hundreds = rate % 1000
    return '%d,%03d,%03d bps' % (millions, thousands, hundreds)

def write_serial(ser, args):
    global tw, calc, tr
    cntdown = args.count_down
    tdtw = 0.0
    twtotal = 0
    while True:
        calc = False
        tw = time.perf_counter();
        wtotal = 0
        limit = args.rwsize
        #print('wsize:', limit)
        # create a work with 0x01<size> in two bytes
        head_i = 0x0100 + limit;
        header = head_i.to_bytes(2, 'big')
        for i in range(args.wrep):
            odata = header + obytes[0:limit]
            w = ser.write(odata[0:limit + 2])
            #print("W1:", w)
            wtotal += limit;
        tr = time.perf_counter()
        dtw = tr - tw
        tdtw = tdtw + dtw
        twtotal = twtotal + wtotal
        if cntdown > 0:
            cntdown = cntdown - 1
        else:
            srate = calc_rate(wtotal, dtw)
            print('dTw:%1.5f -> %s' % (dtw, srate))
            srate = calc_rate(twtotal, tdtw)
            print('TdTw:%1.5f -> %s' % (tdtw, srate))
            tdtw = 0.0
            twtotal = 0
            cntdown = args.count_down
        calc = True
        time.sleep(0.1)
        #for b in s:
        #    print(chr(b), end=',')


def read_serial(ser, args):
    global tw, calc, tr
    cnt = 0
    rtotal = 0
    #dtList = []
    cntdown = args.count_down
    trtotal = 0
    tdtr = 0.0
    while True:
        if calc and (ser.in_waiting == 0) and (cntdown == 0):
            srate = calc_rate(rtotal, dtr)
            print('dTr:%1.6f -> %s' % (dtr, srate), 'rtotal:', rtotal, 'cnt:', cnt)
            rtotal = 0
            srate = calc_rate(trtotal, tdtr)
            print('TdTr:%1.5f -> %s' % (tdtr, srate))
            cntdown = args.count_down
            calc = False
        header  = ser.read(2);
        dtr = time.perf_counter() - tw
        tdtr = tdtr + dtr
        rsize = header[1]
        rtotal += 2
        s = ser.read(rsize)
        rtotal += rsize
        trtotal = trtotal + rtotal
        #print('rs:', rsize, 'rt:', rtotal, 'h0:', header[0],  'h1:', header[1])
        cnt = cnt + 1
        if cntdown > 0:
            cntdown = cntdown - 1
        #dtList.append(time.perf_counter() - tw)


if __name__=='__main__':
    parser = argparse.ArgumentParser(description='comm port beat')

    parser.add_argument('--port',
                        type=str,
                        default='S2',
                        help='comm port to open and then read/write')
    parser.add_argument('--speed',
                        action='store_true',
                        help='run the speed test version')
    parser.add_argument('--rwsize',
                        type=int,
                        default=64,
                        help='read/write size of theads')
    parser.add_argument('--wrep',
                        type=int,
                        default=5,
                        help='write repeats')
    parser.add_argument('--count_down',
                        type=int,
                        default=1000,
                        help='count down before write results')
    args = parser.parse_args()


    ser = serial.Serial('/dev/tty%s' % args.port, 115200, timeout=None)
    #print('SER:', ser, args.rwsize)
    recvt = threading.Thread(target=read_serial, name='read', args=(ser, args))
    sendt = threading.Thread(target=write_serial, name='send', args=(ser, args))
    recvt.start()
    sendt.start()
    print("wait for threads to complete")
    recvt.join()
    sendt.join()


