"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

This is just a text file

sudo bash
set -o vi
python

import time
from time import sleep
wf=open("/dev/fb1", "wb", 0)
rf=open("/dev/fb1", "rb", 0)
psize = 2
xres = 320
yres = 240

def bitstopixel(pix_bits):
    return chr((pix_bits >> 0) & 0xff) + chr((pix_bits >> 8) & 0xff)

b2p=bitstopixel 

# colors not correct!
red_bits=0xf800
red = b2p(red_bits)
green_bits = 0x07E0
green = b2p(green_bits)
blue_bits = 0x001f
blue = b2p(blue_bits)
black = b2p(0)
white = b2p(0xffff)

def draw_pixel(x, y, pixel):
   wf.seek(psize * (xres * y + x), 0)
   wf.write(pixel)

dp=draw_pixel

def draw_line((x1,y1), (x2,y2), pixel):
  #print ((x1, y1), (x1, y2), pixel)
  dx = x2 - x1
  xstart, xend = (x1, x2) if dx > 0 else (x2, x1)
  dy = y2 - y1
  if dy == 0:
    for x in range(xstart, xend):
      draw_pixel(x, y1, pixel)
    return
  if dx == 0:
    y1, y2 = (y1, y2) if y2 > y1 else (y2, y1)
    for x in range(y1, y2):
      draw_pixel(x1, x, pixel)
    return
  m = dy/float(dx)
  c = float(y1) - m * float(x1)
  for x in range(xstart, xend):
    y = m * float(x) + c
    if y >= yres:
      if x < x2:
        continue
      break
    draw_pixel(x, int(y), pixel)

dl=draw_line

dl((0, 0), (xres - 1, yres - 1), red)


def draw_box((x1,y1), (x2,y2), pixel):
   draw_line((x1, y1), (x1, y2), pixel)
   draw_line((x1, y2), (x2, y2), pixel)
   draw_line((x2, y2), (x2, y1), pixel)
   draw_line((x2, y1), (x1, y1), pixel)
   draw_line((x1, y1), (x2, y2), pixel)
   draw_line((x1, y2), (x2, y1), pixel)

db=draw_box

def db2(center, side, color):
  x1, y1 = center
  x1 -= side / 2
  y1 -= side / 2
  x2 = x1 + side
  y2 = y1 + side
  draw_box((x1, y1), (x2, y2), color)


def dflush():
  wf.seek(2*yres*xres)
  wf.write(white)

def seek_xy(x, y):
    wf.seek(x * y * yres)

def fill_blocks():
    wf.seek(140*yres)
    for i in range(10*yres):
      wf.write(red)
    for i in range(10*yres):
      wf.write(green)
    for i in range(10*yres):
      wf.write(blue)
   wf.close()


def fill_screen(pixel, dev='/dev/fb1'):
    wf.seek(0)
    for i in range(xres*yres):
        wf.write(pixel)
    wf.close()

fs=fill_screen

def build_line(pixel, dx):
    xstr = ''
    for x in range(dx):
        xstr += pixel
    return xstr

bl=build_line


def explore_colors(first_color=1, last_color=15, second_color=0, dev='/dev/fb1', seek=True, wait=False):
    if seek:
        wf.seek(0)
    def fill_color(cpix, fill_rows):
        pout = bitstopixel(cpix)
        fcols = xres/2
        bcols = fcols
        pline = build_line(pout, fcols)
        #pline = build_line(pout, xres)
        if second_color:
          pout = bitstopixel(second_color)
          bline = build_line(pout, bcols)
        else:
          bline = pline
        for i in range(fill_rows):
            wf.write(pline)
            wf.write(bline)
        return cpix << 1
    fill_rows = yres / (last_color - first_color + 1)
    for i in range(first_color, last_color + 1):
        pixel_pos = 1 << (i - 1)
        print 'ecx:%x %r' % (pixel_pos, bitstopixel(pixel_pos))
        fill_color(pixel_pos, fill_rows)
        if wait:
          print '<retturn>',
          r = sys.stdin.readline()
          if r[0] == 'q':
            break
    wf.close()

ec=explore_colors
def ecc(c):
  ec(c, c)

_pline = ''
def fill_colors(dev='/dev/fb1'):
    def fill_color(cpix):
        pout = bitstopixel(cpix)
        _pline = pline = build_line(pout, xres)
        print 'pout:', pout, len(pline)
        for i in range(15):
            wf.write(pline)
        return cpix << 1
    wf = open(dev, 'wb', 0)
    wf.seek(0)
    pixel_pos = 1
    i = 1
    while pixel_pos & 0xffff:
        print 'fcx:%d w/%x %s' % (i, pixel_pos, bitstopixel(pixel_pos))
        pixel_pos = fill_color(pixel_pos)
        i = i + 1
    wf.close()


fc=fill_colors

# end

def draw_block((x0, y0), (x1, y1), pixel):
    dx = x1 - x0 - 1
    asset(dx <= xres)
    dy = y1 - y0 - 1
    xstr = build_line(pixel, dx)
    wf.seek(spixel * yres * y0 + spixel * x0, 0)
    for y in range(dy):
        wf.write(xstr)
        wf.seek(spixel * yres - x0, 1) # move to next row

def pixel_pos_in(ppos):
    if ppos & red_bits:
        return 'red'
    if ppos & blue_bits:
        return 'blue'
    if ppos & green_bits:
        return 'green'

import time
import sys
def slow_fill(stime=4, match_pixel=black):
    pixel_pos = 0x0001
    while pixel_pos & 0xffff:
        fill_screen(bitstopixel(pixel_pos))
        print 'pp:%0x in %s' % (pixel_pos, pixel_pos_in(pixel_pos))
        if bitstopixel(pixel_pos) == match_pixel:
            break;
        time.sleep(stime)
        pixel_pos = pixel_pos << 1
        print '<return>',
        l = sys.stdin.readline()
        if l[0] == 'q':
            break

sf=slow_fill

 
def db2s((xstart, xend), ystart=-1, xstep=10, ystep=10, width=10, color=white, st=0.5):
    mod = (xend - xstart) % xstep
    xend = xend - mod
    x = xstart
    y = ystart if ystart != -1 else xstart
    while not (x == xend):
      db2((x, y), width, color)
      sleep(st)
      x = x + xstep
      y = y + ystep
      if (x < 0) or (x + width > xres):
        print x, y
        break
      if (y < 0) or (y + width > yres):
        print x, y
        break

fs(black)

dl((20, 20), (20, 100), blue)
dl((20, 100), (120, 100), green)
dl((20, 10), (120, 100), red)
dl((120, 100), (220, 50), white)
dl((320, 200), (20, 50), white)

db((10, 10), (310, 230), white)

db2((xres/2, yres/2), 10, white)


db2s((10, 300))
db2s((300, 10), 10)
db2s((10, 310), 10, xstep=-13, st=0.1)

