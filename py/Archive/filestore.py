#!/usr/bin/env python3
#

""" file store
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import time

from utils.record import Record


class FileStore(Record):
    def __init__(self, filepath, gametime=time.time(), mode='w'):
        handle = open(filepath, mode, buffering=4096)
        Record.__init__(self, handle, gametime)

    def send(self, stuff):
        self._handle.write(stuff)

    def send_chunk(self, name, json_chunk):
        """
        Send JSON data to a file that needs to be a complete JSON file.
        """
        if name:
            self.send('{"%s": %s},\n' % (name,  json_chunk))
            return
        self.send(json_chunk)
        self.send(',\n')

    def start(self, stime=None):
        """
        start sending data, possibily overriding game time
        """
        self.send('"ss":\n')
        Record.start(self, stime)
        self.send(',\n')

    def end(self):
        self.send('"ets":\n')
        Record.end(self)
        self.send('}\n}\n')

