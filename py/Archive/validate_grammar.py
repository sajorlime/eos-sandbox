#!/usr/bin/env python3
#

""" extract-songs
    Reads a songs grammar file and generates string for speech to text
"""

"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import codecs
import copy
import os
import os.path
import shutil
import sys

import args
import client
import convert_grammar
import filefilter
import generate_wav
import recognition_response


usage = """
validate-grammar [-gammar=avigaNN] 
                 [-tmp=temp-dir] grammar.txt
                 [-log=log_file.txt] [-fail=fail_log.txt] [-concern=concern_log.txt]
                 [+/-longest]
                 [+/-verbose]
                 [-filter=log-file]
                 [-start=line#]
                 [-end=line#]

takes the grammar.txt file and write to string for each possible way to say
the grammar line.
    
    -grammar: defaults to aviga01
    -tmp: defaults to /tmp
    -log: defaults to standard out.
    -fail: defaults to no special logging for failures.
           If sepecified recognition failures are sent to both the log output
           and the fail log.
    -concern: defaults to no special logging for concerns.
           If sepecified recognition concerns are sent to both the log output
           and the concern log. Concerns are those replies that do not clearly
           map to either failure or success
    +longest: for each entry says only the longest phrase
    +verbose: prints extra info, defaults to -verbose
    -filter: defaults to pass-all, takes the log file and picks the line number
             from the beginning of the entry (for fail and concern files only)
             and uses that number a pass filter.
    -start: start at this line in the file
    -end: end at this line in the file
"""


if __name__ == '__main__':

    args = args.Args(usage)
    grammar_version = args.get_arg('grammar', 'aviga01')
    tmp_dir = args.get_arg('tmp', '/tmp')
    longest = bool(args.get_arg('longest', False))
    skip_start = int(args.get_arg('start', 0))
    skip_end = int(args.get_arg('end', -1))

    log_file_name = args.get_arg('log', default = '-')
    if log_file_name == '-':
        log_file = sys.stdout
    else:
        log_file = codecs.open(log_file_name, encoding='utf-8', mode='w+')

    fail_file_name = args.get_arg('fail', default = None)
    if fail_file_name:
        fail_file = codecs.open(fail_file_name, encoding='utf-8', mode='w+')

    concern_file_name = args.get_arg('concern', default = None)
    if concern_file_name:
        concern_file = codecs.open(concern_file_name, encoding='utf-8', mode='w+')

    filter_file_name = args.get_arg('filter', default = None)
    if filter_file_name:
        filter_list = filefilter.ReadFile(filter_file_name,
                                          interest = lambda x : x[0] == '#',
                                          xform = lambda x, n : int(x[1:].strip().split(':')[0])
                                          )
        filter = lambda x : x not in filter_list
    else:
        filter = lambda x : False

    # whatever is left is the grammar file
    grammar_file = args.get_value()
    if not grammar_file:
        args.help("grammar file argument is required")
    args.verbose() # we need to call before check for args consummed

    args.error_if_unused_args()

    pair_num = 1
    phrase_pairs = convert_grammar.read_grammar_file(grammar_file)
    if skip_start > 0:
        pair_num += skip_start
    phrase_pairs = phrase_pairs[skip_start:skip_end]
        
    for phrase_pair in phrase_pairs:
        if list(filter(pair_num)):
            pair_num += 1
            continue
        if args.verbose():
            if longest:
                print('\n---\n%7d:find longest for: %s' % (pair_num, phrase_pair), file=log_file)
            else:
                print('\n---\n%7d:find all for: %s' % (pair_num, phrase_pair), file=log_file)
        phrase_list = convert_grammar.all_phrases(phrase_pair[0], longest)
        if not phrase_list:
            #print 'skip'
            continue
        phrase_list.sort()
        last_phrase = ''
        for phrase in phrase_list:
            if phrase == None:
                continue
            if phrase != last_phrase:
                if args.verbose(): print('phrase: %s' % phrase, file=log_file)
                text_file_name = tmp_dir + '/' + phrase.replace(' ', '-') + '.txt'
                text_file_name = text_file_name.replace('\'', '-')
                path = generate_wav.save_text(phrase, text_file_name)
                out_path = generate_wav.generate_wav(path)

                #print path
                response = client.nsAvigaClient(grammar_version, out_path)
                if args.verbose(): print('server response:\n%s' % response, file=log_file)
                # check result here.
                try:
                    rec_resp = recognition_response.recognition_response(response)
                except ValueError:
                    print("response failed to parse:\n%s" % response, file=log_file)
                    continue
                recog = recognition_response.validate_response(rec_resp,
                                                               phrase_pair[1],
                                                               longest)
                print('#%7d: %s' % (pair_num, recog), file=log_file)
                print(file=log_file)
                if recog.failure() and fail_file_name:
                    print('#%7d: %s' % (pair_num, recog), file=fail_file)
                if recog.concern() and concern_file_name:
                    print('#%7d: %s' % (pair_num, recog), file=concern_file)

                # do something!
            last_phrase = phrase
        if args.verbose(): print('---', file=log_file)
        print(file=log_file)
        pair_num += 1


