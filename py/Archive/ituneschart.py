#!/usr/bin/env python3
#

"""test program
   the the itunes char file and parse it.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import os
import os.path
import urllib
import shutil
import sys
from BeautifulSoup import BeautifulSoup


url = urllib.urlopen("http://www.apple.com/itunes/charts/songs/")

print url

header =  "%s" % url.info()

print header

soup = BeautifulSoup(url)

for row in soup('table', 

#Content-Disposition: attachment; filename=video.avi
content_disposition_pos = header.find("Content-Disposition: ")
try:
  file_name = sys.argv[2]
  print 'output filename: %s' % sys.argv[2]
except IndexError:
  if content_disposition_pos < 0:
    print "usage: getrul url [output-file-name]"
    print ("       if header has the field: "
           "Content-Disposition: attachment; filename=somename.ext\n"
           "then filename is used and the output-file-name "
           "parameters is not needed")
    sys.exit(-1)

if content_disposition_pos > 0:
  try:
    file_name_pos = header.index("filename=",
                                 content_disposition_pos,
                                 content_disposition_pos + 50)
    end_of_line_pos = header.index("\n", file_name_pos)
    file_name = header[file_name_pos + 9:end_of_line_pos - 1]
    print "extracted file name: %s" % file_name
  except ValueError:
    pass

print "\n"
print "cd: %d" % content_disposition_pos

wf = open(file_name, 'w')
print wf
print "\n"
wf.write(url.read())

sys.exit(0)

