#!/usr/bin/env python3
""" nsclient_wf.py
Novauris NovaServer client (using authentication)

Look at the code for more information
 or contact John.Bridle@novauris.com
"""
# John Bridle Novauris Technologies
# May be suitable as an example of how to use the client library
# May throw exceptions and print tracebacks if it runs into a problem.

# From Python standard library
import sys
import os
import time
import urllib.request, urllib.parse, urllib.error
import wave

# Novauris specials
from comlib import NovaAPI # The Novauris Python module containing the interface code 
from nlib.nsutils import getwavefiledata, getwavehttpdata, readlst

# For details of the use of the Python comlib, other client libraries, or the line protocol itself
#   see the NovaSystem Documentation, available from Novauris

##################################################################

# Some Constants
#hostname = 'aviga.novaurisnet.com' 
hostname = os.getenv("NOVA_HOST")
if not hostname:
    hostname = '192.168.1.201' 
    #hostname = '64.79.115.23' 
portnum = os.getenv("NOVA_PORT")
if not portnum:
    portnum = 5667
portnum = int(portnum)


# Authentication info
username = 'aviga'
password = 'je9:3d[q7"43jYcv+aQ2'

# Specifics for this task
taskname = 'aviga01'  # will be specified
samrate = 16000

    
###################################################################
def escape(s, quote=None):
    """Replace special characters '&', '<' and '>' by SGML entities."""
    s = s.replace("&", "&amp;") # Must be done first!
    s = s.replace("<", "&lt;")
    s = s.replace(">", "&gt;")
    if quote:
        s = s.replace('"', "&quot;")
    return s

def unescape(s, quote=None):
    """Replace special characters '&', '<' and '>' by SGML entities."""
    s = s.replace("&lt;", "<")
    s = s.replace("&gt;", ">",)
    if quote:
        s = s.replace("&quot;",'"')
    s = s.replace("&amp;","&") # Must be done last!
    return s

class NSClient(object):
    ''' Simple dedicated client, to make it easy to use from other modules '''
    def __init__(self,taskname=taskname,print_messages=False):
        self.taskname=taskname
        self.print_messages=print_messages
        try:
            self.nc = NovaAPI.NovaClient(host=hostname, port=portnum, user=username, passwd=password)
        except NovaAPI.NovaServerError as e:
            print('Could not connect to server',hostname,'on port',portnum,'as user %r'%username, file=sys.stderr)
            print(' Server says: "%s"'%e, file=sys.stderr)
            sys.exit(1)
    def status(self):
        # Find out whether our task is available and how loaded it is (optional)
        status,del1,del2 = self.nc.task_stat(self.taskname)
        res = []; p=res.append
        p('#Task: %r,  status: %r  delays: (%s,%s)'%(self.taskname,status,del1,del2))
        if status=='unknown':
            print('Sorry, the NovaServer does not know about task %r'%self.taskname, file=sys.stderr)
            sys.exit(1)

        p('#time: %s\n'%time.ctime())
        return '\n'.join(res)

    def rec(self,data,uttname=None,text=None):
        ''' Recognize the complete waveform, provide a report '''
        res = []; p = res.append
        starttime = time.time()
        if uttname:
            p('Uttname: %s'%uttname)
        ##p('Filename: %s'%filename)
        if text:
            p('Ref: %s'%text)
        #p('Datime: %s'%time.ctime())
        
        self.nc.newwavedata(self.taskname)  # Announce that we are about to send waveform, and what to do with it
        self.nc.feedwavedata(data)     # Send all in one chunk (with real-time input we would send in chunks)
        self.nc.endwavedata()          # Tell it to get we have no more to send

        results = self.nc.get_results()    # Wait for main result and receive it as one string
        messages = self.nc.get_messages()  # Messages (that we may not be interested in)
        endtime = time.time()
        self.nc.channel_reset()        # Forget anything it may have learnt about the channel or speaker

        #p('Time: %s'%(endtime-starttime))
        if self.print_messages:
            p(unescape(messages.strip()))
        #p('Results:')
        p(unescape(results))    # One line per answer, with percentage confidences in []
        return '\n'.join(res)
    
    def close(self):           
        self.nc.close()

#####################################################################################

def main(taskname,wavefile):
    ''' Process a file using a given Task name'''
    #print 'taskname %s' % taskname
    nc = NSClient(taskname)
    #Read all the bytes of the file, and check that the header looks right
    data = None
    if 'http:' in wavefile:
        data = getwavehttpdata(wavefile, samrate)
    else:
        data = getwavefiledata(wavefile, samrate)
    if data == None:
        nc.close()
        print('<error>no data returned</error>')
        sys.exit(-1)
    #Perform the recognition
    res = nc.rec(data)
    print(res)
    nc.close()
    sys.exit(0)
    
if __name__=='__main__':
    from nlib import clp  # Call main with args from the commandline

