#!/usr/bin/env python3

"""
    m4v2wmv.py -- finds filtered list of m4v files within directory tree
    and converts them into another tree.
    The program is passed both a source tree root and a destination
    tree root.  One can filter the files, positively and negatively.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import codecs
import datetime
import os
import queue
import re
import subprocess
import sys
import threading
import time

import regexfilter
import tree


# Do not use multiprocessing for this code - just threading. However
# it is nice to know how many cpus there are.
import multiprocessing
cpu_count=multiprocessing.cpu_count
del multiprocessing

class M4VHandler(tree.TreeItemHandler):
    def __init__(self, src, dst, filters, qsize):
        """
        Get the filtered list of mv4 files.
        Args:
            src: the root directory for the tree_files.
            dst: the dst directory for the converted files.
            filters: the list of filters
            qsize: the size of the queue to create.
        """
        tree.TreeItemHandler.__init__(self, src, filters, queue.Queue(qsize))
        self._dest = dst
        self._qsize = qsize
        self._terminated = False

    def get(self):
        return self._alist.get(True, 10)
        
    def append(self, path):
        if not self._terminated:
            self._alist.put(path, True)

    def items(self):
        print('items() disabled', file=sys.stderr)
        return []

    def terminate(self):
        self._terminated = True
        try:
            while self._alist.get(False):
                pass
        except queue.Empty:
            pass
        for i in range(self._qsize):
            self.append(None)


_just_leave = False

def m4v2wmv():
    parser = argparse.ArgumentParser(description='convert the m4v files to wmv files into parallel tree',
                                     epilog='see tree.py --help for example filters, ffmpeg for video params.\n'
                                              'Note that entire path is copied, so DST is typically the same as the '
                                              'current directory in another tree.  Use root to remove part of '
                                              'SRC path')
    parser.add_argument(dest='src',
                        metavar='SRC',
                        type=str,
                        help='the source directory')
    parser.add_argument('--root',
                        type=str,
                        default='',
                        help='the source directory root, i.e. that part of the path not copied to destination')
    parser.add_argument(dest='dst',
                        metavar='DST',
                        type=str, default='',
                        help='the destination directory')
    parser.add_argument('--filter', '-f', 
                        dest='filters',
                        action='append',
                        type=regexfilter.SRegexFilter,
                        default=[regexfilter.SRegexFilter('+\\.m4v\\Z|\\.mp4\\Z|\\.mp3\\Z')],
                        help='add a filter, , defaults to files ending in \".m4v\"')
    parser.add_argument('--force',
                        action='store_true',
                        default=False,
                        help='force file conversion, default %(default)s')
    parser.add_argument('--warn',
                        action='store_true',
                        default=True,
                        help='warn on directory creation, default %(default)s')
    parser.add_argument('--fps', '-r',
                        dest='fps',
                        type=int,
                        default=15,
                        help='the frames/second of the output video')
    parser.add_argument('--threads',
                        type=int,
                        default=cpu_count(),
                        help='the number of threads to use, default %(default)s')
    parser.add_argument('--bitrate', '-b',
                        dest='bitrate',
                        type=int,
                        default=1000000,
                        help='the bitrate of the output video')
    parser.add_argument('--vcodec',
                        dest='vcodec',
                        type=str,
                        default='wmv2',
                        help='the bitrate of the output video')
    parser.add_argument('--time', '-t',
                        dest='vsecs',
                        type=float,
                        default=60.0,
                        help='the bitrate of the output video')
    myargs = parser.parse_args()

    if not os.path.exists(myargs.src):
        print(sys.stderr >> 'The path %s must exist' % myargs.src)
        sys.exit(1)
    if not os.path.exists(myargs.dst):
        print(sys.stderr >> 'The path %s must exist' % myargs.dst)
        sys.exit(2)

    #for f in myargs.filters:
    #    print f._mode, f._rex.pattern


    #            '-s', myargs.vsize,           # output video size
    def subffmpeg(spath, dpath):
        args = ['/usr/local/bin/ffmpeg',         # 0
                '-y',                            # 1: overwrite
                '-i', spath,                     # 2, 3: input file
                '-t', '%f' % myargs.vsecs,       # 4, 5: length in secs of output
                '-vcodec', '%s' % myargs.vcodec, # 6, 7: video codec
                '-b', '%d' % myargs.bitrate,     # 8, 9: bitrate in bits/sec of output
                '-r', '%d' % myargs.fps,         # 10, 11: frames/ec of output
                dpath]                           # 12
        print('run %s %s %s %s %s %s %s %s %s %s %s %s %s' % (args[0],
                                                        args[1],
                                                        args[2], args[3],
                                                        args[4], args[5],
                                                        args[6], args[7],
                                                        args[8], args[9],
                                                        args[10], args[11],
                                                        args[12]))
        mif = subprocess.Popen(args,
                             stdout = subprocess.PIPE,
                             stderr = subprocess.PIPE)
        out, err = mif.communicate()
        if mif.returncode:
            raise Exception("ffmpeg failed code %d out\n%s\nerr\n%s" % (mif.returncode, out, err))


    def workerfn(m4vh, num, rootpath, dstdir, sema):
        global _just_leave
        while True:
            try:
                path = m4vh.get()
                if not path:
                    return
                ## call the conversion
                sdir = os.path.dirname(path)
                target = os.path.basename(path)
                if rootpath:
                    rpos = path.find(rootpath)
                    if rpos != 0:
                        print('bogus path(%s) and root(%s) no conversion' % (path, rootpath))
                        continue
                    ddir = os.path.join(dstdir, sdir[len(rootpath):])
                else:
                    ddir = os.path.join(dstdir, sdir)
                if not os.path.exists(ddir):
                    sema.acquire()
                    if _just_leave:
                        sema.release()
                        #print >> sys.stderr, '\njust leaving ', num
                        return
                    if myargs.warn:
                        print('need to make directory: ', ddir, file=sys.stderr)
                        print('type <enter> to exit, y<enter>" to continue: ', file=sys.stderr)
                        input = sys.stdin.readline()
                        if 'y' not in input:
                            _just_leave = True
                            #print >> sys.stderr, '\ngot the y now term ', num
                            m4vh.terminate()
                            #print >> sys.stderr, '\ngot the y now release ', num
                            sema.release()
                            #print >> sys.stderr, '\nreleased now leaving ', num
                            return
                    sema.release()
                    try:
                        os.makedirs(ddir)
                    except OSError as x:
                        if not os.path.exists(ddir):
                            raise x
                        pass # race conditions may allow this exception

                if '.m4v' in target:
                    dpath = os.path.join(ddir, target).replace('.m4v', '.wmv')
                elif '.mp4' in target:
                    dpath = os.path.join(ddir, target).replace('.mp4', '.wmv')
                elif '.mp3' in target:
                    dpath = os.path.join(ddir, target)
                else:
                    # Do I wan't to punt here
                    dpath = os.path.join(ddir, target)
                if not os.path.exists(dpath) or myargs.force:
                    subffmpeg(path, dpath)
            except Exception as x:
                print('at Exception x')
                print(x)
                pass

    handler = M4VHandler(myargs.src, myargs.dst, myargs.filters, myargs.threads)
                
    mysema = threading.Semaphore()
    workers = []
    for w in range(myargs.threads):
        workers.append(threading.Thread(target=workerfn, args=(handler, w, myargs.root, myargs.dst, mysema)))
    #workers = [threading.Thread(target=workerfn, args=(handler, myargs.root, myargs.dst, mysema)) for _ in range(myargs.threads)]
    for worker in workers:
        worker.daemon = False
        worker.start()
    tree.tree_files(myargs.src, handler)


    for w in workers:
        w.join()

if __name__=='__main__':
    m4v2wmv()

