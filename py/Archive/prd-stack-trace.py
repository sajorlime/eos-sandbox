#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

""" prd-stack-trace

    Takes in a log file from the MediaPortal containing a Segmantaion fault and prints out a
    symbolically stack trace resolved version.

    The log file must contain a version number, stack trace, and the 1ibrary map, as per
    the standard log file output format.

    Usage:
        prd-stack-trace.py [override defaults] log.txt
"""



__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import os
import os.path
import shutil
import stat
import re
import sys
import copy

import args

usage = """
prd-stack-trace [-server=servername] [-mapfile=override.map] logfile.txt

"""

args = args.Args(usage)

# MediaPortal.map file
MP_MAP = args.get_arg('mapfile', 'MediaPortal.map')

# build server
BUILD_SERVER = args.get_arg('server', 'nesaru.2wire.com')


class MemoryAddress:
    """
    A class that represents a memory location as a value, string pair.
    Usage:
        ma = MemoryAddress(address, 'foobar')

        print ma.show()
    """
    def __init__(self, address,
                       resolves_to_library = "", lib_offset = 0, 
                       resolves_to_map = "", map_offset = 0):
        # this should be an integer value.
        self._address = address
        self._resolves_to_library = resolves_to_library
        # resolves to can be string the resolves to a location in a map
        self._resolves_to_map = resolves_to_map
        # resolves to can be an integer offset to a library
        self._lib_offset = lib_offset
        # resolves to can be an integer offset to function in a library
        self._map_offset = map_offset

    def address(self):
        return self._address

    def resolves_to_library(self):
        return self._resolves_to_library

    def set_resolves_to_library(self, resolves_to_library):
        self._resolves_to_library = resolves_to_library

    def lib_offset(self):
        return self._lib_offset

    def set_lib_offset(self, lib_offset):
        self._lib_offset = lib_offset

    def resolves_to_map(self):
        return self._resolves_to_map

    def set_resolves_to_map(self, resolves_to_map):
        self._resolves_to_map = resolves_to_map

    def map_offset(self):
        return self._lib_offset

    def set_map_offset(self, map_offset):
        self._map_offset = map_offset

    def show(self):
        if self._resolves_to_map:
            return '[%08x:+%08x=%s]' % (self._address, self._map_offset, self._resolves_to_map)
        if self._resolves_to_library:
            return '[%08x:%s+%08x]' % (self._address, self._resolves_to_library, self._lib_offset)
        return '[%08x:unresolved]' % self._address
        

class CpuRegisters:
    """
    A class that represents the registers as defined in a log file.
    Usage:
        regs = CpuRegisters(reg_dump)
        
        v = regs.regNumber(7)
    """
    def __init__(self, reg_dump):
        """
        hack it right now.
        Parameters:
           reg_dump is an line of 32 hex values each eight characters wide.
             00000000 fffffffb 00000018 2bb44d14 2bb23a54 00000001 00000000 00000000 \
             00000000 006e006e 0074006f 00620020 00200065 00680063 006e0061 00650067 \
             101b1f00 00000006 00000018 2bb44c30 2bb44d2c 00000001 2bb44bd0 2bb23b9c \
             00000b6f 2b926b2c 00000000 00000000 2bb2cf10 7fff75b8 7fff75b8 2b91e4dc
        """
        self._regs = reg_dump.split()

    def regNumber(self, num):
        return int(self._regs[num:num+1])


class CpuStack:
    """
    A class that represents the stack values parsed from a log file.
    Usage:
        stack = CpuStack(stack_dump)

        stack.show(7)
    """
    def __init__(self, stack_dump):
        """
            Parameters:
                stack_dump is a list of items fo the form:
                    [2b920f4c]
                    [2b91e4dc]
                    [2b91611c]
                    ...
        """
        self._stack = []
        for call_value in stack_dump:
            ma = MemoryAddress(int(call_value[1:9], 16))
            self._stack.append(ma)

    def stack(self):
        return self._stack


    def depth(self):
        return len(self._stack)

    def frame(self, frame_number):
        if unsigned(frame_number) > len(self._stack):
            return None
        return self._stack[frame_number]

    def show_frame(self, frame_number):
        if unsigned(frame_number) > len(self._stack):
            return 'out-of-range'
        return '%s' % self._stack[frame_number].show()

    def print_stack(self):
        for frame in self._stack:
            print('%s' % frame.show())


class MapInfo:
    """
    Class that encapsulates a map file line.
    """
    def __init__(self, map_line):
        """
        Args:
            map_line: is a line from a media portal map file
        Returns:
            MapInfo object with offset, and location_name set.
        """
        colon_pos = map_line.find(':')
        offset_pos = colon_pos + 1
        self._offset = int(map_line[offset_pos:offset_pos+8], 16)
        next_pos = colon_pos + 10
        # we assume that offsets are less than 1Gbyte
        if map_line[next_pos:next_pos + 1] == '0':
            self._length = int(map_line[next_pos:next_pos + 8], 16)
            next_pos = next_pos + 9
        self._type = map_line[next_pos:next_pos + 1]
        self._location_name = map_line[next_pos + 3:]


    def offset(self):
        return self._offset

    def location_name(self):
        return self._location_name

    def length(self):
        return self._length

    def type(self):
        return self._type


class LibraryReference:
    """
        Takes the input string from the dump and turns it into a used
        data structure or understanding a library reference.
    """
    def __init__(self, reference):
        """
        Args:
            referecne: is a string that has an address range, some info and a library path.
        """
        self._ref_info = reference.split()
        range = self._ref_info[0].split('-')
        self._low_range = int(range[0], 16)
        self._high_range = int(range[1], 16)
        self._lib_list = []

    def low_range(self):
        return self._low_range

    def high_range(self):
        return self._high_range

    def in_range(self, address):
        return address >= self._low_range and address <= self._high_range

    def library(self):
        return self._ref_info[5]

    def get_match(self, map_fh, mem_item):
        """
        Extracts from the map file handle the list of referecnes for this memory item,
        and finds the closet item, modifying the mem_item info.

        Note: the routine will attempt to cache a list of items so that the file
        does not need to be searched multiple times.  It is not clear to me how this
        will effect performance as the map file is very large.

        Args:
            map_fh: the opened memory map file.
            mem_item: A MemoryAddress with resolved library and offset.
        Returns:
            Nada, but the mem_item will be modified.
        """
        map_fh.seek(0)
        if not self._lib_list:
            library = mem_item.resolves_to_library()
            rpos = library.rfind('/')
            library = library[rpos + 1:]
            for line in map_fh :
                line = line.strip()
                pos = line.find(library)
                if pos >= 0:
                    map_info = MapInfo(line)
                    self._lib_list.append(map_info)
        # Note: some other structure ought to be used here than a list
        # but we are not resolving very many items (I think) and so I won't 
        # optimize this now.
        offset = mem_item.lib_offset()
        for minfo in self._lib_list:
            f_offset = offset - minfo.offset()
            if f_offset <= 0:
                mem_item.set_resolves_to_map('%s' % (minfo.location_name()))
                mem_item.set_map_offset(-f_offset)
                return
            
        print('Not good, did not find anything for %s' % mem_item.show())


class LibraryList:
    """
    Creates a list of library referecnes.
    """
    def __init__(self, references):
        """
        Args:
            references: is the list of strings extracted from the dump log.
        """
        self._references = {}
        for ref in references:
            lr = LibraryReference(ref)
            self._references[lr.library()] = lr

    def references(self):
        return self._references

    def find(self, address):
        """
        Find the library reference that contatins this memory address.
        Args:
            address: an integer address
        Returns:
            a LibraryReference
        """
        for ref in list(self._references.values()):
            if ref.in_range(address):
                return ref
        return ""

    def get_match(self, map_fh, mem_item):
        try:
            lr = self._references[mem_item.resolves_to_library()]
        except KeyError:
            return
        if lr:
            lr.get_match(map_fh, mem_item)


# these are required to be global until I can figure out how to use
# lambda functions.
_parse_phase = None
_reg_line = 0
_version = ""
_registers = ""
_stack = []
_stack_elements = 0
_object_map = []

def parse_crash_log(log_file):
    global _parse_phase, _reg_line, _version, _registers, _stack, _object_map, _stack_elements
    """
    We are interested in four pieces of the log file, the build version,
    the register dump value, the stack dump, and the MP object list.

    The states are handled by several state function within the proc that
    set the next state when then complete.

    TODO(erojas): do this with lambda functions

    Args:
        log_file: the file name of a log file. '-' to read stdin
    Returns:
        a list of four stings
    """

    def version_parse(line):
        global _parse_phase, _version
        """
        """
        vpos = line.find('MP version ')
        if vpos > 10:
            vpos += 11
            sp_pos = line.find(' ', vpos)
            _version = line[vpos: sp_pos - 1]
            _parse_phase = skip_to_dump

    def skip_to_dump(line):
        global _parse_phase
        if line.find('*** Segmentation fault') == 0:
            _parse_phase = register_parse
        return

    def register_parse(line):
        global _parse_phase, _reg_line, _registers
        _reg_line += 1
        if _reg_line <= 2:
            return
        if _reg_line == 3:
            _registers = line[6:len(line) - 2]
            return
        if _reg_line <= 6:
            _registers = '%s%s' % (_registers, line[6:len(line) - 2])
            return
        if _reg_line <= 7:
            return
        _parse_phase = stack_parse
        return

    def stack_parse(line):
        global _parse_phase, _stack, _stack_elements
        if _stack_elements == 0:
            _stack_elements = 1
            return None
        _stack_elements += 1
        if line[0:1] != '[':
            object_map_parse(line)
            _parse_phase = object_map_parse
            return
        _stack.append(line[0:10])
        return
        
    def object_map_parse(line):
        global _parse_phase, _object_map
        #print 'omp: %s' % line
        _object_map.append(line)
        return

    _parse_phase = version_parse
    lfh = open(log_file);
    for line in lfh:
        _parse_phase(line)
    if _parse_phase != object_map_parse:
        print('pp: %s ' % _parse_phase)
    	print('Wrong phase at end')
    	raise

    return [_version, _registers, _stack, _object_map]

def resolve_to_library(mem_list, library_list):
    """
    Resolve the memory list items to there respective libraries.
    Args:
        mem_list: a list of Memory itemsj
        mp_objects: is a Library list.

    Nothing is returned but the mem_list is modified.
    """
    for mem_item in mem_list:
        addr = mem_item.address()
        library = library_list.find(addr)
        if library:
            mem_item.set_resolves_to_library(library.library())
            mem_item.set_lib_offset(addr - library.low_range())
    return None


def retrieve_map(sever, version):
    """
    Gets a file handle for the map file for this version.

    Normally retrieves a copy of the build map file for the given version 
    from the build server and saves it to the local file system.
    It then returns a file handle.

    Args:
        server: the name of the build server.
            describe how it can be constructed.
        version: the sting version of the build, e.g. 4.1.0.39

    """

    return open(MP_MAP)



if __name__ == '__main__':
    log_file = args.get_value()
    print('log file: %s' % log_file)
    if not args.empty():
        print('args len: %d' % args.len())
        print('unrecognized arguments')
        print(args.report())
        print(usage)
        sys.exit(3)

    # we care about four seperate pieces of the map file.
    ver, regs, stk, objects = parse_crash_log(log_file)

    registers = CpuRegisters(regs)
    stack = CpuStack(stk)
    mp_objects = LibraryList(objects)

    #resolve_to_library(registers, mp_objects)
    #stack.print_stack()
    resolve_to_library(stack.stack(), mp_objects)
    stack.print_stack()

    # get a file handle the build  map.
    build_fh = retrieve_map(BUILD_SERVER, ver)

    for mem_item in stack.stack():
        mp_objects.get_match(build_fh, mem_item)
    stack.print_stack()
        
        

