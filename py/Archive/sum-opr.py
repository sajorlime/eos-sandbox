#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

"""sum-opr
   reads the records that are output from opreport and sums up the counts and %
   cat opr-file | sum-opr
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import os
import os.path
import shutil
import stat
import re
import getopt
import sys
import datetime
import time
import pickle
import copy

import opr
import args

ifile = '-'

options = False

args = args.Args()

#print "argslen: %d" % args.len()

if args.len() > 1:
    try:
        ifile = args.get_value(0)
        #print "ifile: %s" % ifile
    except IndexError:
        print('Opps bad # agrs %d' %  args.len())
        sys.exit(1)

def match_item(item):
    match = False
    if image_match:
        if item.image_name() != image_match:
            return False
        match = True
    if app_match:
        if item.app_name() != app_match:
            return False
        match = True
    if symbol_match:
        if item.symbol_name() != symbol_match:
            return False
        match = True
    return match

    

opr_data = opr.ReadOPRFile(ifile)

image_match = args.get_arg('image')
app_match = args.get_arg('app')
symbol_match = args.get_arg('symbol')

if image_match or app_match or symbol_match:
    options = True

samples = 0
percent = 0.0
for opr_item in opr_data:
   # add filters here
   if not options or match_item(opr_item):
       samples += opr_item.samples()
       percent += opr_item.percent()

print("Total Samples: %d" % samples)
print("Total Percent: %f" % percent)

