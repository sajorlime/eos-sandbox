#!/usr/bin/env python3

import aiohttp
import asyncio
import atexit
import sys
import time
import threading

"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""





class AsyncHttpService(object):
    """
    Excapsaltes the async http service so that all requests can use by creating this object and calling loop_forever.
    """
    def __init__(self, name, api_url, amsgq, arep, headers, send_tasks=6, *args):
        """
        Args:
            name: thread name
            api_uri: is the complete uri for the AAF API.
            amsgq: is an asyncio.Queue() that allows an wait operation
                   items in the queue are dictionaries of the form {'m': httpbody, 'r': boolean reply, 't': optional timestamp}
                   the dict can also be {'exit': True} which causes the send service to exit.
            arepq: the asyncio.Queue() to be used for replies.
            headers: The HTTP headers to be used.
            send_tasks: the number of sender tasks to create
            *args: a list of other async coroutine eservices to run on the aysnc loop.
        """
        self._api_url = api_url
        self._amsgq + amsgq 
        self._arepq = arepq 
        self._headers = headers
        self._arequesterf = arequesterf
        self._sender_tasks = sends_tasks
        self._ev_loop = None
        self._ev_loop = asyncio.get_event_loop()
        self._send_request_tasks = []
        # place all of the futures in the loop.
        for _ in range(0, self._sender_tasks):
            send_request_task = self._ev_loop.create_task(send_async_http_req)
            self._send_request_tasks.append(send_request_task)
        for afuture in args:
            afuture_task = self._ev_loop.create_task(afuture)
            self._send_request_tasks.append(afuture_task)
        atexit.register(self.exit)

    @asyncio.coroutine
    async def send_async_http_req():
        """
        An asyncio aiohttp client coroutine.  The as
        """
        client = aiohttp.ClientSession(headers=self._headers)
        count = 0
        try:
            while True:
                mdict = await self._amsgq.get()
                if mdict.get('exit', False):
                    break
                resp = await client.post(self._api_uri, data=mdict['m'])
                # some http requests need the reply, if True return req the reply queue.
                if mdict.get('r', False):
                    self._arepq.put_no_wait(resp)
        except asyncio.CancelledError:
            pass
        finally:
            await client.close()
            print('%s requests sent.' % (count))

    def exit(self):
        for _ in range (0, self._sender_tasks):
            self._amsgq.put_no_wait({'exit' : True})


    def loop_forever(self):
        """
        thread that runs the aysnc service
        """
        #self._ev_loop = asyncio.get_event_loop()
        try:
            self._ev_loop.run_forever()
        except KeyboardInterrupt:
            print('\n\nAsyncService received CTL-C, exiting ...')
        finally:
            self._arequesterf.cancel()
            for task in send_request_tasks:
                task.cancel()
            pending = asyncio.Task.all_tasks()
            #self._ev_loop.run_forever()
            self._ev_loop.close()


