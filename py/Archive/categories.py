#!/usr/bin/env python3
#
#

"""
  categories.py reads in a category file in the format:
      Id, super category/sub-category 1/sub-category 2/terminal category
  It converts this to:
      ('id', ('super category', 'sub-category 1', 'sub-category 2', 'terminal category'))
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import codecs
import os
import os.path
import re
import sys

import filefilter

def categories():
    parser = argparse.ArgumentParser(description = 'take a category files and convert it to python')
    parser.add_argument('--input', '-i',
                        type = str,
                        default = None,
                        help = 'input file')
    parser.add_argument('--output', '-o',
                        type = str,
                        default = 'categories.txt',
                        help = 'input file %(default)s')
    myargs = parser.parse_args()

    if not os.path.exists(myargs.input):
        parser.help()
        sys.exit(-1)

    ofile = codecs.open(myargs.output, encoding='utf-8', mode='w+')
    def split_key(line, n):
        pos = line.find(', ')
        return [line[0:pos], line[pos + 2:]]
    cat_list = filefilter.ReadFile(myargs.input, xform = split_key)
    #print('read file')
    for cat in cat_list:
        cat_id = cat[0]
        #print(cat_id)
        cat_names = cat[1].strip().split('/')
        cat_out = "('%s', (%s))," % (cat_id, ''.join(["'%s', " % x for x in cat_names]))
        print(cat_out, file = ofile)

if __name__=='__main__':
    categories()

