#!/usr/bin/env python3


"""
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import os
import os.path
import urllib.request, urllib.parse, urllib.error
import shutil
import sys

basename = sys.argv[1]
index = sys.argv[2]
server = sys.argv[3]
stime = sys.argv[4].split(':')
dtime = sys.argv[5].split(':')
id = sys.argv[6]
etime = []
for s in stime:
    etime.append(s)
"""
 note, this version is hack that only does seconds.
 otherwise we need to take all of the time components
 convert them.
"""
secs = float(etime[2])
dt = float(dtime[2])
etime[2] = '%d' % (secs  + dt)


# just hack the path for now
MCLI = '/home/eo/src/matching/src/cli.py'

#CMDROOT = '%s db-content-landmarks --server=%s --start=%s:%s:%s --end=%s:%s:%s --index=%s %s' % (MCLI, server, stime[0], stime[1], stime[2], etime[0], etime[1], etime[2], index, id)
CMDROOT = '%s db-content-landmarks --start=%s:%s:%d --end=%s:%s:%d --index=%s %s' % (MCLI, stime[0], stime[1], float(stime[2]), etime[0], etime[1], float(etime[2]), index, id)

REDIRECT = ' > content-landmarks/%s-%s-lm.csv' % (basename, id)
#print CMDROOT
#print REDIRECT
#print MCLI
#print id
#print stime
#print etime
CMD = '%s %s' % (CMDROOT, REDIRECT)
print(CMD)
ret = os.system(CMD)

