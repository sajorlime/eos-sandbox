#!/usr/bin/env python3
#
#
"""
  conflictedfilter.py takes the output of svn status and returns 
  a list of conflicted files.
    
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import os
import os.path
import re
import sys


import filefilter

def conflictedfilter():
    parser = argparse.ArgumentParser(description = 'a simple filter to get file names from svn status returns')
    parser.add_argument('--input-file', '-i',
                        type = str,
                        dest = 'ifile',
                        default = '-',
                        help = 'the input file , - uses stdin, %(default)s')
    parser.add_argument('--output-file', '-o',
                        type = str,
                        dest = 'ofile',
                        default = '-',
                        help = 'the output file , - uses stdout, %(default)s')
    parser.add_argument('--type-tag',
                        type = str,
                        dest = 'typetag',
                        default = '   C ',
                        help = 'the status type we are looking for, %(default)s')
    myargs = parser.parse_args()

    if myargs.ofile is '-':
      wf = sys.stdout
    else:
      wf = open(myargs.ofile, 'w');

    def is_conflicted(line):
      if line.find(myargs.typetag) >= 0:
        return True
      return False

    def split_conflicted(line, line_number):
      cpos = line.find(myargs.typetag)
      return line[cpos + len(myargs.typetag):].rstrip()

    flist = filefilter.ReadFile(myargs.ifile, interest = is_conflicted, xform = split_conflicted)

    for file in flist:
      print(file, file=wf)
      
if __name__=='__main__':
    conflictedfilter()


