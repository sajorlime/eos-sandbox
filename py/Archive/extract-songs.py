#!/usr/bin/env /usr/bin/python3
#

""" convert-songs
    Reads a songs csv files and writes a database item file.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import codecs
import os
import os.path
import shutil
import sys

import args
import filefilter

usage = """
convert-songs song-id.csv

takes the song-id.csv file and writes a song-id-item.xml file.
"""

args = args.Args(usage)
verbose = bool(args.get_arg('verbose', False))

def song_csv_to_xml_xform(line, n):
    """
        Take the song line from the CSV and transform it XML characters
        Pameters:
            line -- an input line read form a CSV file for song data.
        Returns:
            A line that has been fixed up to XML happy.
    """
    line = line.rstrip()
    return line;


if __name__ == '__main__':

    csv_file = args.get_value()
    item_file_list = csv_file.split('.')
    item_file_name = '%s-item.xml' % item_file_list[0]
    item_file = codecs.open(item_file_name, encoding='utf-8', mode='w+')

    lines = filefilter.ReadFile(csv_file, xform = song_csv_to_xml_xform)
    print('<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>', file=item_file)
    print('<MPL Version=\"2.0\" Title=\"Library\">', file=item_file)

    for line in lines:
        sinfo = line.split('\t')
        print('<Item>', file=item_file)
        print('  <Field Name=\"Artist\">%s</Field>' % sinfo[2], file=item_file)
        print('  <Field Name=\"Album\">%s</Field>' % sinfo[4], file=item_file)
        print('  <Field Name=\"Name\">%s</Field>' % sinfo[0], file=item_file)
        print('  <Field Name=\"Genre\">%s</Field>' % sinfo[6], file=item_file)
        print('  <Field Name=\"JF_ArtistID\">%s</Field>' % sinfo[3], file=item_file)
        print('  <Field Name=\"JF_AlbumID\">%s</Field>' % sinfo[5], file=item_file)
        print('  <Field Name=\"JF_TrackNameID\">%s</Field>' % sinfo[1], file=item_file)
        print('  <Field Name=\"JF_GenreID\">%s</Field>' % sinfo[7], file=item_file)
        print('</Item>', file=item_file)

    print('</MPL>', file=item_file)


