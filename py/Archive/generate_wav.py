#!/usr/bin/env python3
#

""" generate_wav.py takes an input text file path.txt
    and generates path.wav.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""


__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import codecs
import os
import os.path
import shutil
import sys

import args
import filefilter

_TEXT2WAV = 'text2wave'

def generate_wav(input_path, output_path = None, text2wav = None):
    """ 
        Take the input path that points at a text file and pass it 
        to text2wav with the output file path.
    Args:
        input_path:  the file path of the input text.
        output_path:  the file path of the output wave file
        text2wav:  the path the executable
    Returns:
        Always returns False, i.e. never put anything it the list
    """

    if not text2wav:
        text2wav = _TEXT2WAV
    if not output_path:
        output_path = input_path.split('.')[0] + '.wav'
    command = '%s -o %s %s' % (text2wav, output_path, input_path)

    #print command
    ret = os.system(command)
    if ret:
        sys.exit(ret)
    return output_path



def save_text(text, output_path):
    """ 
        Write text to output_path.
    Args:
        text: the text to be saved to output_path.
        output_path:  the path to be used for the text file.
    Returns:
        returns output_file or None
    """

    if not output_path:
        raise ValueError

    text_file = codecs.open(output_path, encoding='utf-8', mode='w+')

    print(text, file=text_file)

    return output_path




if __name__ == '__main__':

    usage = """
    generate_wav path.txt [-o=outfile-path ] [-text2wav=overridepath]

    Takes the path.txt file creates path.wav, 
    unless -o is specified then it creates 
    a wave file at otufile.path.  Extension must
    be specified if it is desired.
    """
    args = args.Args(usage)
    verbose = bool(args.get_arg('verbose', False))
    input_path = args.get_value()
    output_path = args.get_arg('o', None)
    use_args = bool(args.get_arg('text', False))

    if use_args:
        text = ''.join(args.get_values())
        print('text is: %s' % text)
        save_text(text, input_path)


    text2wav_path = args.get_arg('text2wav', None)

    generate_wav(input_path, output_path, text2wav_path)

