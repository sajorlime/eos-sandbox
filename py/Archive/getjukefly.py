#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

"""
    getjukefly
    get the xml data from the JukyFly database
    
    See usage.
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import codecs
import os
import os.path
import pickle
import urllib.request, urllib.parse, urllib.error
import shutil
import sys
import time
from xml.dom import minidom

import args
import jukefly

Song = jukefly.Song
Songs = jukefly.Songs

usage="""
getjukefly [-base=base-uri] \\
           [-page_size=#] \\
           [-user_start_page=#] \\
           [-uid=#] \\
             | [-id_min=#] \\
               [-id_max=#] \\
           [-id_file=output-file] \\
           [songs_file=output-file] \\
           [artists_file=output-file] \\
           [genres_file=output-file] \\
           [albums_file=output-file] \\
           [+/-ids_pickle]
           [-ids_pickle_file=ids-pickle-file]
           [+/-song_pickle]
           [-songs_pickle_file=songs-pickle-file]

    -base = bash-uri is the base URI for accessing the jukefly server.
             befaults to http://jukefly.com/external
    -page_size the number of IDs/users_songs/songs to read at one time
    -uid you can set uid or id min/max
      |  -id_min is the first user id to include
      |  -id_max is the last user id to include
    +/-ids_pickle if TRUE(+) it uses the ids_pickle_file to try an read
        the ids, thereby avoiding access to the server for this largely
        redundant task.  If the file does not exist, it will be created
        after the id list is read from the server.
        Defaut is TRUE.
    -ids_pickle_file=ids-pickle-file is the name/path to the file used
        to de/pickle the ID list.
    +/-song_pickle when TRUE(+) loads and stores song info to and
        from this the song_pickle_file.  Using the song_pickle_file
        can reduce the time to load file information.
    -songs_pickle_file=songs-pickle-file is the name/path to file used
        to de/pickle song info.

Running getjukyfly will first retieve the set of all user ids and
the the list of songs for each user and finally the the songs info
for song id.  When all is said and done it writes several files.
A song.csv file, and text files with artists, genres, and albums.

If you run getjukyfly with no parameters it will attempt to get first
all of the user ids and then for each user all of their songs from the
JukeFly database.  Unfortunately, this is a bad idea!  It is a bad
idea because the program will be running for several days.  It is very
likely to fail in that time and all results will be lost.  Also it has
never run that long and may fail due to space or some bug.  In the
future a new call should be added by JukeFly to allow a list of
song_ids to be retrieve, replacing the one at a time requirement now
in place.

Partial results can be achieved by using the id_min and id_max
parameters and retrieve data for a few users at a time.  You
may also want to specify unique names for the out files when
you do this.
"""

args = args.Args(usage)

# options
_URI_BASE = args.get_arg('base', 'http://jukefly.com/external')
_PAGE_SIZE = int(args.get_arg('page_size', 200))
_ID_FILE = args.get_arg('id_file', 'user_ids.txt')
_SONGS_FILE = args.get_arg('songs_file', 'songs.csv')
_ARTISTS_FILE = args.get_arg('artists_file', 'artists.txt')
_ALBUMS_FILE = args.get_arg('albums_file', 'albums.txt')
_GENRES_FILE = args.get_arg('genres_file', 'genres.txt')
_ID_MIN = int(args.get_arg('id_min', 0))
_ID_MAX = int(args.get_arg('id_max', 0))
_UID = int(args.get_arg('uid', 0))
_SONGS_PICKLE = args.get_arg('songs_pickle_file', 'songs.pkl')
_IDS_PICKLE = args.get_arg('ids_pickle_file', 'ids.pkl')
_USE_SONGS_PICKLE = bool(args.get_arg('song_pickle', False))
_USE_IDS_PICKLE = bool(args.get_arg('ids_pickle', False))

# URI name components
_GET_USERS = 'get_users'
_GET_USER_SONGS = 'get_user_songs'
_GET_SONGS = 'get_songs'
_GET_SONG = 'get_song'


def read_data(uri):
    """
    A utility function to retrieve data from the user.

    Returns:
        a string contiaining the the result of an HTTP GET from the
        URI.
    """
    #print 'rd uri: ', uri

    try:
        url = urllib.request.urlopen(uri)
        #header =  '%s' % url.info()
        #print 'header:'
        #print header
        uri_data = url.read()
    except IOError:
        print('IOError in read_data')
        return None
    return uri_data

def save_data(data, file):
    """
    A utility function to save data to a file.
    """
    wf = open(file, 'w')
    wf.write(data)

            
def get_song_info(song_id):
    """
    Creates an URI and retrievs data for the song_id URI.
    It then parses the returned value and extracts the 
    song info XML.
        
    Returns:
        a string cotaining the XML for song info
        
    """
    #print 'gsi: %d' % song_id
    uri = '%s/%s?song_id=%s' % (_URI_BASE, _GET_SONG, song_id)
    uri_data = read_data(uri)
    if not uri_data:
        # retry one time
        uri_data = read_data(uri)
        if not uri_data:
            return None
    #print 'gsi: %s' % uri_data
    doc = minidom.parseString(uri_data)
    response_el = doc.documentElement
    for doc_node in response_el.childNodes:
        #print 'gsi name: %s' % doc_node.nodeName
        if doc_node.nodeName == 'song':
            return doc_node
    print('gfi error no song')
    return None

def get_total_items(uri):
    """
        takes the uri retrieves the data and looks for the
        'total-items' element, and extracts the number
        from that element.
    """
    print('gti: %s' % uri)
    uri_data = read_data(uri)
    #print 'gti: %s' % uri_data
    doc = minidom.parseString(uri_data)
    response_el = doc.documentElement
    total_items = -1;
    for doc_node in response_el.childNodes:
        #print doc_node.nodeName
        if doc_node.nodeName == 'total-items':
            total_items = int(doc_node.firstChild.data)
            break;
    print('    has total items: %d' % total_items)
    return total_items


def get_partial_users(base, count):
    """
    retreives from the jukefly server a partial list of the current user ids

    Returns:
        a list of integers that represent user ids
    """
    uri = '%s/%s?page=%s&page_size=%s' % (_URI_BASE, _GET_USERS, base, count)
    uri_data = read_data(uri)
    # now we parse it can get a list of all the user ids.
    user_doc = minidom.parseString(uri_data)
    response_el = user_doc.documentElement
    users_el = None
    for doc_node in response_el.childNodes:
        #print 'f-users: %s' % doc_node.nodeName
        if doc_node.nodeName == 'users':
            users_el = doc_node
            #print users_el.nodeName
            break;
    user_ids = []
    if users_el:
        for users_node in users_el.childNodes:
            if users_node.nodeName == 'user':
                users_nodes = users_node.childNodes
                for user_node in users_nodes:
                    if user_node.nodeName == 'user-id':
                        id = int(user_node.firstChild.data)
                        #print 'user id: %d min:%d max:%d' % (id, _ID_MIN, _ID_MAX)
                        user_ids.append(id)
    #print 'user ids: %s' % user_ids
    return user_ids


def get_users():
    """
    retrieves from the jukefly server the current list of user ids
    As a side effect it writes the data to the ids.xml file

    Returns:
        a list of integers that for all of the user ids.

    """
    uri = '%s/%s?page=%s&page_size=0' % (_URI_BASE, _GET_USERS, 0)
    items = get_total_items(uri);
    #print 'total items: %d' % count

    user_ids = []
    if _USE_IDS_PICKLE:
        try:
            plf = open(_IDS_PICKLE, 'r')
            user_ids = pickle.load(plf)
            plf.close()
            return user_ids
        except (EOFError, IOError):
            print('STARTING with empty ids')
    # we can get here two ways
    page = 0
    while page * _PAGE_SIZE < items:
        new_ids = get_partial_users(page, _PAGE_SIZE)
        page += 1
        user_ids += new_ids
    try:
        pdf = open(_IDS_PICKLE, 'w')
        #print pdf
        pickle.dump(user_ids, pdf)
        pdf.close()
    except:
        print('someting went wrong with saving ids')
    return user_ids


_repeated_songs = 0
_new_songs = 0


def get_partial_user_songs(user_id, base, size, songs, user_songs):
    global _repeated_songs, _new_songs
    """
    Get a parial list of song info from the jukefly database.  
    The jukefly database is designed to return partial requests
    and this function satifies this constraitint.

    Parameters:
        user_id: the value of the user id of the info to be retrieved
        base: the staring play in the list of ids for this users.
        size: the number starting at base to retrieve
        songs: a dictionay object in which to save all songs.
        user_songs: a list to save the songs retrieved for this user
    Returns
        Does not return anything directly buts acts on songs, and user_songs
        to acquire the requied information.
    """
    quary_uri = '%s/%s?user_id=%s&page=%d&page_size=%s' \
                    % (_URI_BASE, _GET_USER_SONGS, user_id, base, size)
    uri_data = read_data(quary_uri)
    #print 'user data: %s' % uri_data
    if uri_data:
        user_doc = minidom.parseString(uri_data)
    else:
        print('gpus: user data: None')
        return
    response_el = user_doc.documentElement
    #print 'user %d songs: %s' % (user_id, response_el.nodeName)
    for response_node in response_el.childNodes:
        if response_node.nodeName == 'user-songs':
            songs_nodes = response_node.childNodes
            for song_node in songs_nodes:
                if song_node.nodeName == 'user-song':
                    song_id_nodes = song_node.childNodes
                    for song_id_node in song_id_nodes:
                        if song_id_node.nodeName == 'song-id':
                            song_id = int(song_id_node.firstChild.data)
                            if (songs.new_id(song_id)):
                                _new_songs = _new_songs + 1
                                song_info = get_song_info(song_id)
                                asong = Song(song_info, user_id)
                                songs.add_song(song_id, asong)
                                user_songs.append(asong)
                                #print 'user:%d song_id: %d' % (user_id, song_id)
                            else:
                                songs.inc_id(song_id, user_id)
                                _repeated_songs = _repeated_songs + 1
                                user_songs.append(songs.get_id(song_id))
                                #print 'user:%d old song_id: %d' % (user_id, song_id)


def get_user_songs(user_id, songs):
    global _repeated_songs
    """
    get the songs for this user and add them to the songs dictionary
    Returns:
        nada, song dictionary is updated.
    """
    stime = time.time()
    quary_uri = '%s/%s?user_id=%d&page=0&page_size=0' \
                    % (_URI_BASE, _GET_USER_SONGS, user_id)
    total_size = get_total_items(quary_uri)
    page = 0
    user_songs = []
    while page * _PAGE_SIZE < total_size:
        reft = time.time()
        get_partial_user_songs(user_id, page, _PAGE_SIZE, songs, user_songs)
        print('[%5.2f] user %d got songs page:%d of %d pages with %d songs' % (time.time() - reft,
                                                                           user_id,
                                                                           page,
                                                                           total_size / _PAGE_SIZE,
                                                                           total_size))
        page += 1
        sys.stdout.flush()
    print('[%5.2f] to get new songs(%d) repeated songs(%d) at user %d' % (time.time() - stime,
                                                                          _new_songs,
                                                                          _repeated_songs,
                                                                          user_id))
    return user_songs


def get_songs(user_ids, songs_dict):
    """
    """
    for id in user_ids:
        if id < _ID_MIN:
            continue
        if id > _ID_MAX:
            continue
        user_songs = get_user_songs(id, songs_dict)
        song_file_name = 'songs-%d.csv' % id
        song_file = codecs.open(song_file_name, encoding='utf-8', mode='w+')
        print('len(user_songs): %d' % len(user_songs))
        for song in user_songs:
            print('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s' % (song.title(),
                                                                            song.song_id(),
                                                                            song.artist(),
                                                                            song.artist_id(),
                                                                            song.album(),
                                                                            song.album_id(),
                                                                            song.genre(),
                                                                            song.genre_id()
                                                                           ), file=song_file)
        song_file.close()
    print('got songs, total: new songs(%d) repeated songs(%d)' % (_new_songs, _repeated_songs))


if __name__ == '__main__':
    """
    The main body gets the list of user ids and saves it to a file.
    It then gets the songs for each user,
    and finally saves the results to various files.
    """
    args.error_if_unused_args()
    user_ids = get_users()
    print('# user ids: %s' % len(user_ids))

    songs_file = None
    if _USE_SONGS_PICKLE:
        songs_file = _SONGS_PICKLE

    songs_dict = Songs(songs_file)
    if _UID > 0:
        print('uid:%d in user ids? %s' % (_UID, _UID in user_ids))
        _ID_MIN = _UID
        _ID_MAX = _UID
    get_songs(user_ids, songs_dict)

    # Note(epr): we need to codec code to not get an exception for chars > 127
    song_file = codecs.open(_SONGS_FILE, encoding='utf-8', mode='w+')
    print(song_file)
    our_songs = list(songs_dict.songs().values())
    print('length of our_songs: %d' % len(our_songs))
    print('Song Title\tsong-id\tArtist\tartist-id\tAlbum\talbum-id\tGenre\tgenre-id\t# owners\towner list', file=song_file)
    for song in our_songs:
        print('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%d\t%s' % (song.title(),
                                                                        song.song_id(),
                                                                        song.artist(),
                                                                        song.artist_id(),
                                                                        song.album(),
                                                                        song.album_id(),
                                                                        song.genre(),
                                                                        song.genre_id(),
                                                                        song.num_owners(),
                                                                        song.owners(),
                                                                       ), file=song_file)

    artist_file = codecs.open(_ARTISTS_FILE, encoding='utf-8', mode='w+')
    artists = songs_dict.artists()
    for artist_name in artists:
        print(artist_name, file=artist_file)

    genre_file = codecs.open(_GENRES_FILE, encoding='utf-8', mode='w+')
    genres = songs_dict.genres()
    for genre in genres:
        print(genre, file=genre_file)

    albums_file = codecs.open(_ALBUMS_FILE, encoding='utf-8', mode='w+')
    albums = songs_dict.albums()
    for album in albums:
        print(albums, file=albums_file)

    songs_dict.save(songs_file)

