#!/usr/bin/env python3.6

import numpy as np

def rotate(m):
    shape = np.shape(m)
    if len(shape) != 2:
        print("I don't know how to do that")
        return None
    nshape = (shape[1], shape[0])
    oa = np.zeros(nshape)
    xlimit = shape[0]
    ylimit = shape[1]
    for x in range(xlimit):
        for y in reversed(range(ylimit)):
            oa[y, xlimit - 1  - x] = m[x, y]
    return oa

M = np.array([ [1,2,3], [4,5,6], [7,8,9]] )
print(M)
R = rotate(M)
print(R)

M = np.array([ [1,2,3, 10], [4,5,6, 11], [7,8,9, 12]] )
print(M)
R = rotate(M)
print(R)

M = np.array([ [1,2,3, 10], [4,5,6, 11], [7,8,9, 12], [13, 14, 15, 16] ] )
print(M)
R = rotate(M)
print(R)
