#!/usr/bin/env python3

"""
    podcasts2m3u.py -- finds the wmv files in subdirectories of
    /AvigaMedia/Podcasts and write m3u files to 
    /AvigaPro/metadata/Podcasts
    The m3u files contain a sorted list of the content such
    that the newest podcast appears at first location.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import codecs
import datetime
import os
import queue
import re
import subprocess
import sys
import threading
import time

import regexfilter
import tree



class WMVHandler(tree.TreeItemHandler):
    def __init__(self, src, filters):
        """
        Get the filtered list of mv4 files.
        Args:
            src: the root directory for the tree_files.
            dst: the dst directory for the converted files.
            fitlers: the list of filters
            qsize: the size of the queue to create.
        """
        tree.TreeItemHandler.__init__(self, src, filters)
        
    def accept(self, item):
        """
            accept a directory item.
            Args:
                item: [dir, [files]]
            Appends modified file list if dir passes filter.
            Only those files that match remain in list.
        """
        if regexfilter.ftest(item[0], self._filters, reject = True):
            #print 'reject dir ', item[0]
            return
        item[1] = [f for f in item[1] if regexfilter.ftest(item[0] + f, self._filters) ]
        if len(item[1]) > 0:
            print('append', item[0])
            self.append(item)
        


def podcasts2m3u():
    parser = argparse.ArgumentParser(description='find the wmv podcasts.wmv files and create m3u files per aviga demo reqs',
                                     epilog='see tree.py --help for example filters')
    parser.add_argument('--src', '-s',
                        dest='src',
                        type=str,
                        default='Podcasts/',
                        help='the source directory')
    parser.add_argument('--root', '-r',
                        type=str,
                        default='/AvigaMedia/',
                        help='the source directory from windows, default %(default)s')
    parser.add_argument('--dst', '-d',
                        dest='dst',
                        type=str,
                        default='../AvigaPro/metadata/Podcasts/',
                        help='the destination directory, default %(default)s')
    parser.add_argument('--filter', '-f', 
                        dest='filters',
                        action='append',
                        type=regexfilter.SRegexFilter,
                        default=[regexfilter.SRegexFilter('+\\.m4v\\Z|\\.mp4\\Z|\\.mp3\\Z|\\.wmv')],
                        help='add a filter, default +\\.m4v\\Z|\\.mp4\\Z|\\.mp3\\Z|\\.wmv')
    myargs = parser.parse_args()

    if not os.path.exists(myargs.src):
        print(sys.stderr >> 'The path %s must exist' % myargs.src)
        sys.exit(1)
    if not os.path.exists(myargs.dst):
        print(sys.stderr >> 'The path %s must exist' % myargs.dst)
        sys.exit(2)

    #for f in myargs.filters:
    #    print f._mode, f._rex.pattern

    pch = WMVHandler(myargs.src, myargs.filters)
                
    tree.tree_dirs(myargs.src, pch)

    for dir, files in list(pch.items()):
        # for now assume a simple sort by name will suffice
        flist = sorted(files, reverse = True)
        if not os.path.exists(dir):
            print('%s does not exist' % dir)
            continue
        dosdir = os.path.join(myargs.root, dir).replace('/', '\\')
        bname = os.path.basename(dir)
        m3u_file = os.path.join(myargs.dst, '%s~%s.m3u' % (bname, bname))
        print('out:', m3u_file)
        m3uh = codecs.open(m3u_file, encoding='utf-8', mode='w+')
        for file in flist:
            s = '%s\\%s' % (dosdir, file)
            us = s.decode("utf8")
            print(s)
            #print '%d %d' % (int(s[112]), int(s[112]))
            #m3uh.write(us)
            print(us, file=m3uh)

if __name__=='__main__':
    podcasts2m3u()

