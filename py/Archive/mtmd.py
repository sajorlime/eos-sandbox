#!/usr/bin/env python3
#
#

"""
    mtmd.py print mtmd memor facts
    
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import argparse
import os
import os.path
import re
import sys
import subprocess
import textwrap

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='factorial')

    parser.add_argument('--bufsize',
                        type=int,
                        default=1 << 15,
                        help='buffer size')
    args = parser.parse_args()
    ddr_base = 0x800000000
    ddr_size = 16 * (1 << 30)
    bd_size = 64
    usb_rate = 133 * (10 ** 6)
    tx_buf_size = 1 << 15
    ddr_avail = 15 * (1 << 30)
    tx_buffer = (1 << 28)
    tx_secs = tx_buffer / usb_rate

    items = []
    items.append(('ddr_base', ddr_base))
    items.append(('ddr_size', ddr_size))
    items.append(('bd_size', bd_size))
    items.append(('usb_rate', usb_rate))
    items.append(('tx_buf_size', tx_buf_size))
    items.append(('ddr_avail', ddr_avail))
    items.append(('tx_buffer', tx_buffer))
    items.append(('tx_secs', tx_secs))

    for i in items:
        print(i[0])
    for i in items:
        print(i[1])

