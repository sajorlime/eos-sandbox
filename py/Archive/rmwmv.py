#!/usr/bin/env python3

"""
    rmwmv.py -- finds filtered list of wmv files within directory tree
    and converts and removes them
    The program is passed both a source tree root and a destination
    tree root.  One can filter the files, positively and negatively.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import codecs
import datetime
import os
import queue
import re
import subprocess
import sys
import threading
import time

import regexfilter
import tree


# Do not use multiprocessing for this code - just threading. However
# it is nice to know how many cpus there are.
import multiprocessing
cpu_count=multiprocessing.cpu_count
del multiprocessing

class WMVHandler(tree.TreeItemHandler):
    def __init__(self, src, filters, qsize):
        """
        Get the filtered list of mv4 files.
        Args:
            src: the root directory for the tree_files.
            fitlers: the list of filters
            qsize: the size of the queue to create.
        """
        tree.TreeItemHandler.__init__(self, src, filters)
        # TODO(epr): do something smart about size of queue
        self._plist = queue.Queue(2 * qsize)
        self._qsize = qsize

    def get(self):
        return self._plist.get(True, 10)
        
    def append(self, path):
        self._plist.put(path, True)

    def accept(self, path):
        accept_it = True
        for filter in self._filters:
            if not filter.accept(path):
                accept_it = False
                break;
        if accept_it:
            self._plist.put(path)

    def terminate(self):
        for i in range(2 * self._qsize + 1):
            self.append(None)



def rmwvm():
    parser = argparse.ArgumentParser(description='remove the wmv files in this tree',
                                     epilog='%(prog)s files')
    parser.add_argument(dest='src',
                        metavar='SRC',
                        type=str,
                        help='the source directory')
    parser.add_argument('--filter', '-f', 
                        dest='filters',
                        action='append',
                        type=regexfilter.SRegexFilter,
                        default=[regexfilter.SRegexFilter('+\\.wmv\\Z')],
                        help='add an filter, defaults to files ending in .wmv')
    myargs = parser.parse_args()

    def subrm(path):
        args = ['/bin/rm', path]
        #print 'csp %s' % args
        mif = subprocess.Popen(args,
                             stdout = subprocess.PIPE,
                             stderr = subprocess.PIPE)
        out, err = mif.communicate()
        if mif.returncode:
            raise Exception("rm failed code %d out\n%s\nerr\n%s" % (mif.returncode, out, err))


    def workerfn(m4vh, num):
        while True:
            try:
                path = m4vh.get()
                if not path:
                    return
                ## call the conversion
                subrm(path)
            except Exception as x:
                print(x)

    handler = WMVHandler(myargs.src, myargs.filters, cpu_count())
                
    workers = []
    for w in range(cpu_count()):
        workers.append(threading.Thread(target=workerfn, args=(handler, w)))
    for worker in workers:
        worker.daemon = False
        worker.start()
    tree.tree_files(myargs.src, handler)


    for w in workers:
        w.join()

if __name__=='__main__':
    rmwvm()

