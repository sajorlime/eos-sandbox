#!/usr/bin/env python3
#

""" gglck.py
    Take a file that contains cut --fields=9 of IMPRESSION_V2 log records
    and looks at the statistics of the gglck id with the purpose of
    generating an appropriate hash.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import hashlib
import os
import os.path
import shutil
import subprocess
import sys

import filefilter


usage = """
gglck.py [--file=file-name] --ration=N 

"""

def gglck():
    parser = argparse.ArgumentParser(description='take a gglck file and generate histogram count')
    parser.add_argument('--file', '-f',
                        type=str,
                        default='-',
                        help='input file, the default, %(default)s, uses stdin ')
    parser.add_argument('--ratio', '-t',
                        type=int,
                        default=10,
                        help='histogram width, default %(default)s')
    parser.add_argument('--hwidth', '-w',
                        type=int,
                        default=5,
                        help='characters at end of id to be used for hash, default %(default)s')
    parser.add_argument('--rindex', '-r',
                        type=int,
                        default=0,
                        help='characters to start from end %(default)s')
    parser.add_argument('--limit', '-l',
                        type=int,
                        default=2^32,
                        help='characters to start from end %(default)s')
    parser.add_argument('--md5', '-m',
                        action='store_true',
                        default=False,
                        help='use md5d %(default)s')
    parser.add_argument('--surfer', '-s',
                        action='store_true',
                        default=False,
                        help='use b_surfer c code %(default)s')
    parser.add_argument('--spath',
                        type=str,
                        default='b_surfer',
                        help='the path to the b_surfer program or any other program to test an alternative')
    myargs = parser.parse_args()

    gsize = 27
    def extract_id(line, n):
        ggpos = line.find('gglck=')
        ll = len('gglck-')
        return line[ggpos + ll : ggpos + ll + gsize]
    gglck_list = filefilter.ReadFile(myargs.file, xform = extract_id)

    primes = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 91, 97, 101, 103, 107, 109, 111)
    hist = {}
    for i in range(myargs.ratio):
        hist[i] = 0

    limit = myargs.limit
    gindex = gsize - myargs.rindex

    proc = None

    if myargs.md5:
        def b_surfer(cookie):
            m = hashlib.md5()
            m.update(cookie.encode('utf-8'))
            ms = m.digest()
            #h = ms[0]
            # py2.x needs ord: h = sum([ord(ms[i]) for i in range(len(ms))])
            return sum([ms[i] for i in range(len(ms))])

    elif myargs.surfer:
        proc = subprocess.Popen(myargs.spath,
                               shell=True,
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE)
        print(proc)
        def b_surfer(cookie):
            print('in:', cookie)
            proc.stdin.write('%s\n' % cookie)
            out = proc.stdout.readline()
            print('out:', out)
            return int(out)
        #print('sh: ', h)
    else:
        def b_surfer(cookie):
            clist = [primes[x] * ord(cookie[x]) for x in range(gindex - myargs.hwidth, gindex)]
            return sum(clist)

    for gglck in gglck_list:
        m = b_surfer(gglck) % myargs.ratio
        hist[m] = hist[m] + 1
        #print(gglck.strip(), len(gglck), h, m)
        limit -= 1
        if limit <= 0:
            break
    if proc:
        proc.terminate()
    print(hist)
    hsorted = sorted(hist.values())
    diff = hsorted[len(hsorted) - 1] - hsorted[0]
    pos = hsorted.index(hist[0])
    print(hsorted, pos, diff)

if __name__=='__main__':
    gglck()


