#!/usr/bin/env python3
#

"""
    Copyright (C) 2020 oneNav, Inc. All rights reserved.

    preppos.py get dummy preposition data
"""

__author__ = 'emilior@onenav.com (Emilio Rojas)'


import os
import sys
import serial
import io
import time
import threading
import random

import csv_filter

_debug = False
wrlen = 0

def _dprint(*pargs):
    if not _debug:
        return
    print(pargs)

def write4(odata, v):
    global wrlen
    assert(type(odata) == bytearray)
    odata.append((v >> 24) & 0xff)
    odata.append((v >> 16) & 0xff)
    odata.append((v >> 8) & 0xff)
    odata.append((v >> 0) & 0xff)
    wrlen += 4

def write2(odata, v):
    global wrlen
    assert(type(odata) == bytearray)
    odata.append((v >> 8) & 0xff)
    odata.append((v >> 0) & 0xff)
    wrlen += 2


def output_u8(outdata, e):
    outdata.append(e)

def output_u16(outdata, e):
    _dprint('u16e:', e)
    write2(outdata, e)

def output_u32(outdata, e):
    _dprint('u32e:', e)
    write4(outdata, e)

"""  output an array of u8s
    @param outdata a binary array
    @param array of bytes
"""
def output_array8(outdata, array):
    global wrlen
    for b in array:
        outdata.append(b)
        wrlen += 1

def output_array16(outdata, array):
    global wrlen
    for sw in array:
        write2(sw)
        wrlen += 2

def output_array32(outdata, array):
    global wrlen
    for w in array:
        write4(w)
        wrlen += 4

def output_struct(outdata, array):
    elements = array[0]
    values = array[2]
    i = 0
    mems = []
    while i < len(elements):
        utype, uname = elements[i].split(' ')
        v = values[i]
        mems.append([utype, uname, v])
        i += 1
    for m in mems:
        output_member(outdata, m)

def output_enum(outdata, enum_info):
    global wrlen
    e = enum_info[-1]
    v = 0
    for ev in enum_info[0]:
        if '=' in ev:
            x,vs = ev.split('=')
            v = int(vs)
            if x == e:
                ev = x
                break
            continue # we don't want to inc v
        else:
            if e == ev:
                break;
        v += 1
    outdata.append(v)
    wrlen += 1

def output_member(outdata, mem):
    assert(type(mem) == list)
    if mem[0] == 'u8':
        return output_u8(outdata, mem[2])
    if mem[0] == 'u16':
        return output_u16(outdata, mem[2])
    if mem[0] == 'u32':
        return output_u32(outdata, mem[2])
    if mem[0] == 'a8':
        return output_array8(outdata, mem[3])
    if mem[0] == 'a16':
        return output_array16(outdata, mem[3])
    if mem[0] == 'a32':
        return output_array32(outdata, mem[3])
    if mem[0] == 'struct':
        return output_struct(outdata, mem[1:])
    if mem[0] == 'enum':
        return output_enum(outdata, mem[2:])

class MeStruct(object):
    def __init__(self, st_items):
        self._members = st_items;

    def members(self):
        return self._members

    def out_data(self, odata):
        global wrlen
        for mem in self._members:
            output_member(odata, mem)
            _dprint(wrlen, mem)
        _dprint('WRLEN:', wrlen)
        return odata


    def __str__(self):
        return '\n'.join(['%s' % m for m in self._members])

def to_int(s):
    if s.startswith('0x'):
        return int(s, 16)
    else:
        return int(s, 10)

def clean(elements):
    #print('Es:', elements)
    if type(elements) == str:
        return elements.strip()
    if type(elements) == list and len(elements) == 1:
        return clean(elements[0])
    l = []
    for e in elements:
        #print('e:', e, type(e))
        if type(e) == str:
            e = e.strip()
            #print('estrip:%s:' % e.strip(), len(e))
            if e and e[0].isdigit():
                e = to_int(e)
                #print('eint:%d:' % e)
        if type(e) == list:
            #print('L:', e)
            e = clean(e)
        l.append(e)
    #t = tuple(l)
    #print('T:', t)
    return l

def read_struct(ppfile, sat=1, debug=False):
    # Note(epr): that the delimiter is change from the default comma.
    cols, csv_reader = csv_filter.csv_open(ppfile, delimiter=':')
    pplist = []
    found_sat = False
    slength = 0 # calculate data length
    for ll in csv_reader:
        # TODO(epr): we need different key words for different types
        if ll and ll[0] == 'satellite' and not found_sat:
            si = ll[1]
            if si[1].isdigit() and int(si) == sat:
                found_sat = True
        elif found_sat:
            # grab files from here to blank
            if not ll:
                break;
            pplist.append(ll)
    st_elements = []
    for thing in pplist:
        ltype = thing[0].strip()
        lval = thing[1]
        # we have enum, struct {}, u8, u16, u32, u8[size]
        if ltype.startswith('u8['): # do this first to avoid conflict
            pni = ltype.find(']')
            asize = int(ltype[3:pni])
            name = ltype[pni + 1:].strip()
            elements = clean(lval.split(','))
            st_elements.append(clean(['a8', name, asize, elements]))
            _dprint(slength, slength + asize)
            slength += asize
            continue
        if ltype.startswith('u8 '):
            st_elements.append(clean(['u8', ltype.split(' ')[1], lval]))
            _dprint(slength, slength + 1)
            slength += 1
            continue
        if ltype.startswith('u16 '):
            st_elements.append(clean(['u16', ltype.split(' ')[1], lval]))
            _dprint(slength, slength + 2)
            slength += 2
            continue
        if ltype.startswith('u32 '):
            st_elements.append(clean(['u32', ltype.split(' ')[1], lval]))
            _dprint(slength, slength + 4)
            slength += 4
            continue
        if ltype.startswith('{'):
            sti = ltype.find('}')
            members = ltype[1:sti].split(',');
            m = clean(members)
            name = ltype[sti + 1:].split(' ')[1]
            elements = ['struct', m, name]
            vals = clean(lval.split(','))
            elements.append(vals)
            st_elements.append(clean(elements))
            _dprint(slength, slength + 4)
            slength += 4
            continue
        if ltype.startswith('enum'):
            name = ltype.split(' ')[1]
            vals = clean(lval.split(','))
            v = vals[-1]
            enums = vals[0:-1]
            t = clean(["enum", name, enums, v])
            st_elements.append(t)
            _dprint(slength, slength + 1)
            slength += 1
            continue
    _dprint('SLEN:', slength)
    return MeStruct(st_elements)


if __name__=='__main__':
    import argparse

    parser = argparse.ArgumentParser(description='reads a CSV "structure" file',
                                     epilog='contains methods used by me_messenger')

    parser.add_argument('--struct', '-t',
                        type=str,
                        default='pp.csv',
                        help='ME message structure values, default: %(default)s')
    parser.add_argument('--sat', '-s',
                        type=int,
                        default=1,
                        help='satellite info to extract, default: %(default)s')
    parser.add_argument('--write_import', '-i',
                        action='store_true',
                        help='write info in format to import from serial')
    parser.add_argument('--write_export', '-x',
                        action='store_true',
                        help='write info in format to export to serial')
    parser.add_argument('--read_import', '-r',
                        action='store_true',
                        help='write info in format to export to serial')
    parser.add_argument('--show', '-S',
                        action='store_true',
                        help='print add the encode structure members and exit')
    parser.add_argument('--debug', '-D',
                        action='store_true',
                        help='set debug flag')
    args = parser.parse_args()

    struct = read_struct(args.struct, args.sat, True)

    if args.show:
        for m in struct.members():
            print(m)
        sys.exit()

    if args.read_import and args.write_export:
        parser.print_help()
        print('import and export can not both be set')
        sys.exit(1)

    #print(struct)
    if args.write_import:
        _debug = True
        def print_member(mem):
            name = mem[1]
            if mem[0] == 'enum':
                print('%s = (uint8_t) (*data++);' % name)
            if mem[0] == 'u8':
                print('%s = (uint8_t) (*data++);' % name)
            if mem[0] == 'u16':
                print('%s = read_big_2(data);' % (name))
                print('data += 2;')
            if mem[0] == 'u32':
                print('%s = read_big_4(data);' % name)
                print('data += 4;')
            if mem[0] == 'a8':
                mlen = mem[2]
                print('memcpy(%s, data, %d);' % (name, mlen))
                print('data += %d;\n' % mlen)
            if mem[0] == 'a16':
                mmlen = mem[2]
                print('memcpy(%s, data, 2 & %d);' % (name, mlen))
                print('data += %d;\n' % mlen)
            if mem[0] == 'a32':
                mlen = mem[2]
                print('memcpy(%s, data, 4 * %d);' % (name, mlen))
                print('data += %d;\n' % mlen)
            if mem[0] == 'struct':
                lelements = mem[1]
                name = mem[2]
                lvalues = mem[3]
                i = 0
                lmems = []
                while i < len(lelements):
                    utype, uname = lelements[i].split(' ')
                    v = lvalues[i]
                    xname = "%s.%s" % (name, uname)
                    lmems.append([utype, xname, v])
                    i += 1
                for lm in lmems:
                    print_member(lm)
        for mem in struct.members():
            print_member(mem)

    if args.write_export:
        _debug = True
        def print_member(mem):
            mtype = mem[0]
            name = mem[1]
            if mem[0] == 'enum':
                print('WRITE_BUF(%s, 1);' % name)
            if mem[0] == 'u8':
                print('WRITE_BUF(%s, 1);' % name)
            if mem[0] == 'u16':
                print('WRITE_BUF(%s, 2);' % name)
            if mem[0] == 'u32':
                print('WRITE_BUF(%s, 4);' % name)
            if mem[0] == 'a8': # TDB
                print('WRITE_BUF(%s, 1);' % name)
            if mem[0] == 'a16': # TDB
                print('WRITE_BUF(%s, 1);' % name)
            if mem[0] == 'a32': # TDB
                print('WRITE_BUF(%s, 1);' % name)
        for mem in struct.members():
            print_member(mem)

    if args.read_import:
        _debug = True
        def print_member(mem, cnt):
            mtype = mem[0]
            name = mem[1]
            if mem[0] == 'enum':
                print('%s = read1(rx_data[%d:])' % (name, cnt))
                return 1
            if mem[0] == 'u8':
                print('%s = read1(rx_data[%d:]);' % (name, cnt))
                return 1
            if mem[0] == 'u16':
                print('%s = read2(rx_data[%d:]);' % (name, cnt))
                return 2
            if mem[0] == 'u32':
                print('%s = read4(rx_data[%d:]);' % (name, cnt))
                return 4
            if mem[0] == 'a8': # TDB
                print('%s = read1(rx_data[%d:]);' % (name, cnt))
                return 1
            if mem[0] == 'a16': # TDB
                print('%s = read1(rx_data[%d:]);' % (name, cnt))
                return 2
            if mem[0] == 'a32': # TDB
                print('%s = read1(rx_data[%d:]);' % (name, cnt))
                return 4
            return cnt
        cnt = 0
        for mem in struct.members():
            cnt += print_member(mem, cnt)


