#!/usr/bin/env python3
"""
A simple client to work with the grammar builder.
Rob 17Oct08
"""

import sys
import urllib.request, urllib.error, urllib.parse
from urllib.parse import urlencode
from urllib.parse import urlparse
from getpass import getpass
from time import sleep

def build_sn_remote(name, template, out_file, wordprons=None, tables=None, 
    probwrdact="dropitem", 
    url="https://aviga.novaurisnet.com/builder/buildBG.py", 
    fetchGrammarURL="https://aviga.novaurisnet.com/builder/checkBGGrammar.py",
    fetchInterval=20, **kwtables):
    """
    "name" the name to give to the VMB, ODL and GML.

    "template" is the template file.

    "out_file" is the filename to write the output to.

    "url" is the URL of the CGI script

    "wordprons" can be a file containing prons to use instead of those in the 
    dictionary. Each line should have the word that the pron is for, a TAB and
    then the new pron as a sequence of words. All words used in the prons must
    be in the dictionary - no attempt will be made to pronguess them.

    "tables" is an optional zip file containing the tables referred to in the
    template. 

    "probwrdact" is an optional flag to set the action the server should take 
    if it encounters words that it cannont generate a pronunciation for. 
    It can be set to "fail" or "dropitem"

    if "use_password" is set to True the user will be prompted for a username 
    and password to login to the webserver.

    "fetchGrammarURL" is an URL to use to fetch the grammar when using a URL that returns a grammar
    ID rather than the actual grammar. The ID is sent to fetchGrammarURL, with responds with a 
    status message if the grammar is not ready or return a zip file containing the grammar.

    "fetchInterval" is the delay in seconds to leave between attempts to fetch 
    the grammar from the server using the fetchGrammarURL.

    The "kwtables" keyword arguments provides an alternative method of uploading
    the tables. The argument name should be the name by which the table is 
    referred to in the template.
    """
    # Set up urllib to use a user name and password
    username = "aviga"
    pword = "sFg34%LcA"
    top_level_url = "http://" + urlparse(url)[1]
    password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
    password_mgr.add_password(None, top_level_url, username, pword)
    handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
    opener = urllib.request.build_opener(handler)
    urllib.request.install_opener(opener)

    # Get the variables that we are going to post to the server.
    vars = {}
    vars["name"] = name
    vars["template"] = file(template).read()
    vars["probwrdact"] = probwrdact
    if wordprons:
        vars["wordprons"] = file(wordprons).read()
    if tables:
        vars["tables"] = file(tables, "rb").read()
    reserved_names = "name template tables wordprons probwrdact".split()
    for key, val in kwtables.items():
        assert key not in reserved_names, "You cannot have a table called %r" % key
        vars[key] = file(val).read()

    # Post the vars to the URL and write the response to the output file.
    response = urllib.request.urlopen(url, urlencode(vars)).read()
    if fetchGrammarURL:    
        # We've been given a URL to use to fetch the grammar
        status  = None
        id = response
        response = "status:"
        startingCount = 0
        while response.startswith("status:"):
            response = urllib.request.urlopen(fetchGrammarURL + "?id=" + id.replace(" ", "+")).read()
            if response.startswith("error: "):
                print(response)
                sys.exit(1)
            elif response.startswith("status:"):
                newStatus = response.split(":", 1)[1].strip()
                if newStatus != status:
                    print(newStatus)
                    status = newStatus
                if newStatus == "Build starting":
                    startingCount += 1
                    if startingCount > 3:
                        print("Failed to start build")
                        sys.exit(1)
                try:
                    sleep(fetchInterval)
                except:
                    # Presumably fetchInterval was invalid - use 20 seconds.
                    sleep(20)
            #else:
                # response must be the grammar
    file(out_file, "w").write(response)
    print("done")


if __name__ == "__main__":
    import nlib.clp
