#!/usr/bin/env python3
#

""" import_fifo.py
    Imports the fifo data from an text file.
    Values are sequential and hex and comma separated.

"""

__author__ = 'emiliorojas@onenav.ai (Emilio Rojas)'

import os
import os.path
import shutil
import sys

# fword assignment
# assign adc_data_tdata_o     = {sample_read_cnt[23:40], adc2axi_async_fifo_dout[39:20], adc2axi_async_fifo_dout[19:0]};
# fword one sample assignment
# assign adc_data_tdata_o     = {sample_read_cnt[65:20], adc2axi_async_fifo_dout[19:0]};

def mask(bits):
    return ((1 << bits) - 1)

def value(val, pos, mask):
    return ((val >> pos) & mask)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='import fifo data')
    parser.add_argument('fifo_file',
                        type=argparse.FileType('r'),
                        help='the hex dump of FIFO data.')
    parser.add_argument('--csv', action="store_true")
    parser.add_argument('--ofile', "-o",
                        type=argparse.FileType('w'),
                        default=sys.stdout,
                        help='output file, defaults to stdout')
    parser.add_argument('--one_sample', '-1', action="store_true")
    args = parser.parse_args()

    # get the values
    ivals = []
    _wbits = 64
    _sampbits = 20
    _qbits = 10
    _ibits = 10
    if args.one_sample:
        _cntpos = _sampbits
        _cntbits = _wbits - _cntpos
        _cntmask = mask(_cntbits)
        _samp0pos = 0
        _sampmask = mask(_sampbits)
        _ipos = _qbits
        _qpos = 0
        _imask = mask(_ibits)
        _qmask = mask(_qbits)
        assert(_imask == _qmask)
    else:
        _cntpos = 2 * _sampbits
        _cntbits = _wbits - _cntpos
        _cntmask = mask(_cntbits)
        _samp0pos = _sampbits
        _samp1pos = 0
        _sampmask = mask(_sampbits)
        _ipos = _qbits
        _qpos = 0
        _imask = mask(_ibits)
        _qmask = mask(_qbits)
        assert(_imask == _qmask)
    if args.one_sample:
        for line in args.fifo_file:
            sline = line.strip()
            hline  = sline[0:4]
            mline  = '0000'
            if not hline:
                #print('skip empty')
                continue
            #print(hline, type(hline), len(hline), len(mline), hline == mline)
            if hline != mline:
                #print('skip:', hline)
                continue
            #print('keep:', sline)
            vline = sline.rstrip(',')
            #print('V:', vline)
            for v in vline.split(','):
                ivals.append(v.strip())
    else:
        for line in args.fifo_file:
            sline = line.strip()
            vline = sline.rstrip(',')
            for v in vline.split(','):
                ivals.append(v.strip())

    #print(ivals)
    # turn the into ints
    ovals = [ int(v, 16) for v in ivals ] 
    #sys.exit(0)

    i = 0
    lcnt = 0
    if args.one_sample:
        def otuple(outv):
            cnt = value(outv, _cntpos, _cntmask)
            samp0 = value(outv, _samp0pos, _sampmask)
            #print(cnt, 'S0:%05x' % samp0)
            i0 = value(samp0, _ipos, _imask)
            q0 = value(samp0, _qpos, _qmask)
            return (outv, cnt, i0, q0)
    else:
        def otuple(outv):
            cnt = value(outv, _cntpos, _cntmask)
            samp0 = value(outv, _samp0pos, _sampmask)
            #print('S0:%05x' % samp0)
            samp1 = value(outv, _samp1pos, _sampmask)
            #print('S1:%05x' % samp1)
            i0 = value(samp0, _ipos, _imask)
            q0 = value(samp0, _qpos, _qmask)
            i1 = value(samp1, _ipos, _imask)
            #print('i1:', i1)
            q1 = value(samp1, _qpos, _qmask)
            return (outv, cnt, i0, q0, i1, q1)
    ltuple = otuple(ovals[0])
    if args.one_sample:
        lcnt = ltuple[1] - 1
        diff = 1
    else:
        lcnt = ltuple[1] - 2
        diff = 2
    if args.csv:
        print('cnt, I, Q', file=args.ofile)
        for ov in ovals:
            ot = otuple(ov)
            print('%d, %d, %d' % (ot[1], ot[2], ot[3]), file=args.ofile)
            if not args.one_sample:
                print('%d, %d, %d' % (ot[1], ot[4], ot[5]), file=args.ofile)
    else:
        if args.one_sample:
            print('oword(64),      44 cnt, {i0, q0}', file=args.ofile)
        else:
            print('oword(64),      24 cnt, {i0, q0},  {i1, q1}', file=args.ofile)
        for ov in ovals:
            ot = otuple(ov)
            cnt = ot[1]
            if cnt - lcnt != diff:
                print('*', end='', file=args.ofile)   
            lcnt = cnt
            if args.one_sample:
                print('%016x, %011x, {%03x, %03x}' % ot, file=args.ofile)
            else:
                print('%016x, %06x, {%03x, %03x}, {%03x, %03x}' % ot, file=args.ofile)


