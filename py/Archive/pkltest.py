#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

"""
    pkltest
    
    See usage.
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import codecs
import os
import os.path
import pickle
import urllib.request, urllib.parse, urllib.error
import shutil
import sys
import time
from xml.dom import minidom

import args

usage="""
pkltest [-pickle_file=file]
"""

args = args.Args(usage)

# options
_PICKLE = args.get_arg('pickle_file', 'pt.pkl')


def open_pickle():
    my_dict = {}
    try:
        pfd = open(_PICKLE, 'r')
        my_dict = pickle.load(pfd)
        pfd.close()
    except TypeError:
        pass
    except FileNotFoundError:
        print('STARTING with empty pickle')
        pass
    return my_dict

def save_pickle(my_dict):
    pfd = open(_PICKLE, 'wb')
    print(pfd)
    pickle.dump(my_dict, pfd)
    pfd.close()



if __name__ == '__main__':
    """
    """
    #args.error_if_unused_args()
    print(_PICKLE)
    a_dict = open_pickle()
    print(a_dict)

    a_dict['a'] = 1
    a_dict['b'] = 2
    a_dict['c'] = 3
    a_dict['d'] = 4
    a_dict['e'] = 5
    a_dict['f'] = 6

    print(a_dict)

    save_pickle(a_dict)

