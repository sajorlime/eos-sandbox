#!/usr/bin/env python3
#

""" normalize-csv.py
    Reads in file with an array of csv and normaizes to N samples.
    normalize-csv csv-file
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'



import os
import os.path
import shutil
import stat
import re
import getopt
import sys
import datetime
import time
import pickle
import copy
import string
import Numeric

import args
import filefilter

ff = filefilter


usage = """
normalize-csv [-osamples=NNN] [-input=csv-in-file] [-output=csv-out-file]
    -osamples defaults to the min of the csv vectors that are read in and 100.
    -*-file: to use stdin or stdout file is '-', both default to '-'

Read in a file of comma seperated values (csv) and writes the normalized output.

"""


def vector_interest(csv_line, n):
    if not csv_line:
        return False
    if not csv_line[0:1] in string.digits:
        return False
    if not ',' in csv_line:
        return False
    return True

_max_vector_len = 100

def vector_xform(csv_line):
    global _max_vector_len
    vector = []
    values = csv_line.split(',')
    vector_len = 0
    for value in values:
        vector.append(float(value))
        vector_len += 1

    if vector_len > _max_vector_len:
        max_vector_len = vector_len
    return vector

def vector_print(vector, fh):
    for v in vector:
        print("%1.4f," % v, end=' ', file=fh)
    print(file=fh)
        
#print "outmain: %s" % __name__

if __name__ == "__main__":
    #print 'in main'
    args = args.Args(usage)
    osamples = int(args.get_arg('osamples', 100))
    _max_vector_len = osamples
    input_file = args.get_arg('input', '-')
    output_file = args.get_arg('output', '-')
    args.error_if_unused_args()

    vectors = ff.ReadFile(input_file,
                         vector_xform,
                         vector_interest)
    if output_file == '-':
        ofh = sys.stdout
    else:
        ofh = open(output_file, 'w')
    for vector in vectors:
        vector_print(vector, ofh)

