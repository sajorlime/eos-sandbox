#!/usr/bin/env python3
"""
    iphone_csv.py -- class that reads in a csv from the iphone
    app "export contacts"

"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import os
import os
import cgi
import codecs
import random
import sys

# local
from email_csv import email_csv

iphone_fields = [
                 email_csv._GivenName,
                 "middleName",
                 email_csv._"lastName",
                 "prefix",
                 "suffix",
                 "nickname",
                 "organization",
                 "jobTitle",
                 "department",
                 "birthday",
                 email_csv._E_mail1_Value
                 email_csv._E_mail1_Type
                 email_csv._E_mail2_Value
                 email_csv._E_mail2_Type
                 "email3",
                 ._"emailLabel3",
                 "note",
                 "creationDate",
                 "modificationDate",
                 "mobilePhone",
                 "homePhone",
                 "workPhone",
                 "mainPhone",
                 "homeFaxPhone",
                 "workFaxPhone",
                 "pagerPhone",
                 "otherPhone",
                  email_csv._Address1_Type,
                  email_csv._Address1_Street,
                  email_csv._Address1_City,
                  email_csv._Address1_Region,
                  email_csv._Address1_Postal_Code,
                  email_csv._Address1_Country,
                 "address1CountryCode",
                 "address2Type",
                 "address2Street",
                 "address2City",
                 "address2State",
                 "address2Zip",
                 "address2Country",
                 "address2CountryCode",
                 "address3Type",
                 "address3Street",
                 "address3City",
                 "address3State",
                 "address3Zip",
                 "address3Country",
                 "address3CountryCode,
                ]


class iphone_csv(email_csv):
    """
    encapsulates the contents of a iphone csv file.
    """
    def __init__(self, addresses):
        email_csv.__init__(self, address_fields, addresses)

    # we need to override phones in iphone because of differing modles.
    # each phone type has a slot in iphone, but only two phone with types
    # in e/gmail
    def phone1(self, position):
        if position > len(self._addresses):
            return None
        addr = self._addresses[position]
        phone = addr[self._af.value('mobilePhone')]
        if phone:
            return phone
        phone = addr[self._af.value('homePhone')]
        if phone:
            return phone
        phone = addr[self._af.value('workPhone')]
        if phone:
            return phone
        phone = addr[self._af.value('otherPhone')]
        if phone:
            return phone
        return None

    def phone2(self, position):
        if position > len(self._addresses):
            return None
        addr = self._addresses[position]
        return addr[self._af.value('E_mail1_Value')]



if __name__ == '__main__':
    import args
    import filefilter

    args = args.Args("iphone_csv (test only) iphone.csv-file")
    addresses = filefilter.ReadFile(args.get_value())

    csv = iphone_csv(addresses)

    print csv

    i = 0; print '%d: name: %s, %s' % (i, csv.name(i), csv.email1(i))
    i = 1; print '%d: name: %s, %s' % (i, csv.name(i), csv.email1(i))
    i = 3; print '%d: name: %s, %s' % (i, csv.name(i), csv.email1(i))
    i = 8; print '%d: name: %s, %s' % (i, csv.name(i), csv.email1(i))

