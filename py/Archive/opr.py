#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

"""opr.py
   OPRData class and untilitis.  A class that reads OProfile output files.
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'



import os
import os.path
import shutil
import stat
import re
import getopt
import sys
import datetime
import time
import pickle
import copy

def opr_fixup(line):
    """ fix up a OPR file output so that things annoying to further analysis
        are removed.  Extra spaces and parenthesis are particularly
        problamatic.
        ', ' -> ','
        '* ' -> '*'
        '(no symbols)' -> 'zz_no_symbol'
        if there are parenthesis then
          '> ' -> '>'
          '< ' -> '<'
          ' ' -> '_'
        if it is a MediaPortal line
          ' ' -> '-'
    Returns:
        the fixed up line
    """
    line = line.replace(', ', ',')
    line = line.replace('* ', '*')
    #print 'no comma-space:' + line
    line = line.replace('(no symbols)', 'zz_no_symbol')
    #print 'no no-symbols:' + line
    lparen_pos = line.find('(')
    if lparen_pos < 0:
        return line
    rparen_pos = line[lparen_pos:].find(')')
    if rparen_pos < 0:
        raise ValueError
    rparen_pos += lparen_pos
    line = line.replace('anon (', 'anon(')
    #print 'before:' + line
    #print "replace spaces in [%d:%d] %s" % (lparen_pos, rparen_pos, line[lparen_pos:rparen_pos])
    #print "replace spaces in " + line[lparen_pos:rparen_pos]
    line = (line[:lparen_pos]
            + line[lparen_pos:rparen_pos].replace(' ', '_')
            + line[rparen_pos:])
    std_pos = line.find('std:')
    if std_pos > 0:
        line = line[:std_pos] + line[std_pos:].replace('< ', '<')
        line = line[:std_pos] + line[std_pos:].replace(' >', '>')
        line = line[:std_pos] + line[std_pos:].replace(' ', '_')
    mp_pos = line.find('MediaPortal              ')
    if mp_pos > 0:
        mp_pos += 26
        line = line[:mp_pos] + line[mp_pos:].replace(' ', '-')
    return line

class OPRData:
    """oprofile report data
        OPRData(input_line) -- translate all of the values in the Oprofile data
            to their nominal meanings.
        Args:
            input_line: a output line from an oprofiles report.
                    it should have samples, percent, image-name, app-name, symbol-name
    """
    def __init__(self, input_line):
        #print input_line
        try:
            samples, percent, image_name, app_name, symbol_name = input_line.split()
        except ValueError:
            #print "funky line: %s" % input_line
            input_line = opr_fixup(input_line)
            #print "fixup line: %s" % input_line
            samples, percent, image_name, app_name, symbol_name = input_line.split()
           
        self._samples = int(samples)
        self._percent = float(percent)
        self._sort_value = self._percent
        self._image_name = image_name
        self._app_name = app_name
        self._symbol_name = symbol_name

    def samples(self):
        return self._samples

    def percent(self):
        return self._percent

    def image_name(self):
        return self._image_name

    def app_name(self):
        return self._app_name

    def symbol_name(self):
        return self._symbol_name

    def key(self):
        return "%s:%s:%s" % (self._symbol_name, self._app_name, self._image_name)

    def show(self, msg):
        return "%s:%4d\t%2.4f\t%24s\t%16s\t%s" % (msg,
                                          self._samples,
                                          self._percent,
                                          self._image_name,
                                          self._app_name,
                                          self._symbol_name)

    def set_sort(self, new_sort_value):
        self._sort_value = new_sort_value
    
def _Exclude(str, exclude):
    for exclusion in exclude:
        if str.find(exclusion) > 0:
            return True
    return False

def ReadOPRFile(opr_file_path, exclude = []):
    """ read an oprofile report file
    Args:
        opr_file_path: file path
    Return:
        a list of OPRData
    """
    opr_data = []

    if opr_file_path == '-':
        opr_file = sys.stdin
    else:
        opr_file = open(opr_file_path)
    for line in opr_file:
        if line[0:1].isdigit():
            if not _Exclude(line, exclude):
                opr_data.append(OPRData(line))

    return opr_data

def opr_cmp(left, right):
    c = cmp(left.symbol_name(), right.symbol_name())
    if c == 0:
        c = cmp(left.app_name(), right.app_name())
    if c == 0:
        c = cmp(left.image_name(), right.image_name())
    return c

def SortBySymbol(opr_data):
    opr_data.sort(opr_cmp)


def GenerateDict(opr_data):
    """
    From an OPR data list, create a OPR dictionary.
    The key for the dictionary is the concatened and fixed-up
    symbol:app:image

    Args
        opr_data: list of OPRData
    Returns:
        a dictionary with key and OPRData as value 
    """
    opr_dict = {}
    for item in opr_data:
        if item.key() in opr_dict:
            #print "dup item: %s" % item.show("debug")
            pass
        else:
            opr_dict[item.key()] = item

    return opr_dict

