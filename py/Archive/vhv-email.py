#!/usr/bin/env python3
#

""" 
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""


__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import os
import os.path
import shutil
import sys

import adjust_path
import argparse
import csv_filter


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='read vhv-names')
    parser.add_argument('--col', '-c',
                        dest='columns',
                        type=str,
                        action='append',
                        default=['Name', 'E-mail 1 - Type', 'E-mail 1 - Value','E-mail 2 - Type', 'E-mail 2 - Value'],
                        help='CSV columns to accept, either names or indexes')
    parser.add_argument('--ocol',
                        dest='ocolumns',
                        type=str,
                        action='append',
                        default=['Name', 'E-mail 1 - Value', 'E-mail 2 - Value',],
                        help='CSV columns to output, either names or indexes, must be subset of columns')
    parser.add_argument('--ctype', '-t',
                        dest='col_types',
                        type=str,
                        action='append',
                        default=[],
                        help='from the possible  columns, the column type, default col type is str.  e.g. product_type:float. ')
    parser.add_argument('--filter', '-f',
                        dest='filters',
                        type=str,
                        action='append',
                        default=[],
                        help='from the possible  columns, criteria by column for accepting rows, which can be a regular expression.  e.g. product_type:shoe. ')
    parser.add_argument('--sort', '-s',
                        dest='sort_cols',
                        type=str,
                        action='append',
                        default=[],
                        help='from the selected columns,  sort the output in the order presented.  e.g. --sort=cname ')
    parser.add_argument('--icsv', '-i',
                        dest='icsv',
                        type=str,
                        default=None,
                        help='the input CSV file path')
    parser.add_argument('--ocsv', '-o',
                        dest='ocsv',
                        type=str,
                        default='-',
                        help='the output CSV file path (needed to write utf-8 characters, "-" indicates stdout which is the default')
    parser.add_argument('--delimiter', '-d',
                        dest='delimiter',
                        type=str,
                        default=',',
                        help='the CSV column delimiter, default: "%(default)s", use "\\t" for tab')
    parser.add_argument('--max-lines', '-m',
                        dest='max_lines',
                        type=int,
                        default=sys.maxsize,
                        help='the maximum lines to output, default: "%(default)s"')
    parser.add_argument('--rotate', '-r',
                        dest='rotate',
                        action='store_true',
                        default=False,
                        help='rotate output: column: [values]')
    parser.add_argument('--index', '-I',
                        dest='index',
                        action='store_true',
                        default=False,
                        help='print the index ')
    parser.add_argument('--count',
                        dest='count',
                        action='store_true',
                        default=False,
                        help='count and print the number of returned lines')
    options = parser.parse_args()

    email_cols, email_reader = csv_filter.csv_open(options.icsv)
    email_indicies = csv_filter.gen_csv_column_indicies(email_cols, options.columns)
    o_indicies = csv_filter.gen_csv_column_indicies(email_cols, options.ocolumns)
    email_addresses = csv_filter.filter_csv(email_reader, columns=email_indicies)
    csv_filter.csv_write(options.ocsv, email_addresses, o_indicies, email_cols)

