#!/usr/bin/env python3
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import csv
import json
import os.path
import mx.DateTime
import sys
import urllib2

import cv.everest.core_infra.apis.app_locations.app_locations
import cv.everest.logger
from cv.everest.search_engine import *

_RUBI_URI = 'http://api.rubiconproject.com/demand/ops/v1/reports/delivery/2181?when=last+30'
_RUBI_USER = 'aaeb53b9dfd5288f5754e7af99f70a9e8d17d7b9'
_RUBI_PASSWD = '77a4bd5a4213aafb874ec69ac7569351'

_ONE_DAY = 24 * 60 * 60

def impressions_on_date(date, timestamp, groups):
  """
    count the impressions on date.
    Args:
      date: YYYY-MM-DD for printing
      timestamp: ticks
    Returns:
      integer count of impressions for all sizes for the date.
  """
  syear, smonth, sday = date.split('-')
  impressions = 0
  clicks = 0
  revenue = 0.0
  for group in groups:
    rows = group['rows']
    for row in rows:
      #print row
      # YYYY-MM-DD -> mx.DateTime -> ticks
      # and then compare in ticks.
      syear, smonth, sday = row['date'].split('-')
      rowts = mx.DateTime.DateTime(int(syear), int(smonth), int(sday)).ticks()
      tdiff = timestamp - rowts
      if (tdiff == 0) or (tdiff < _ONE_DAY):
        impressions = impressions + row['impressions']
        clicks = clicks + row['clicks']
        revenue = revenue + float(row['revenue'])
  print '%s -> i: %d,  c: %d,  r: %f' % (date, impressions, clicks, revenue)
  #print '{\"date" : \"%s\", \"impressions\" : %d}' % (date, impressions)


def get_rubi_row(dates):
  # See: http://www.voidspace.org.uk/python/articles/authentication.shtml
  # for authentication.  Some comment copied verbetim
  # this creates a password manager
  passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
  passman.add_password(None, _RUBI_URI, _RUBI_USER, _RUBI_PASSWD)

  # create the AuthHandler
  authhandler = urllib2.HTTPDigestAuthHandler(passman)

  opener = urllib2.build_opener(authhandler)

  urllib2.install_opener(opener)
  # All calls to urllib2.urlopen will now use our handler
  # Make sure not to include the protocol in with the URL, or
  # HTTPPasswordMgrWithDefaultRealm will be very confused.
  # You must (of course) use it when fetching the page though.

  # authentication is now handled automatically for us
  request = urllib2.Request(_RUBI_URI, headers = {'Accept' : 'application/json'})
  rubih = urllib2.urlopen(request)

  # OK, read the data into a dictionary
  json_data = rubih.read()
  #print json_data
  #return
  group_data = json.loads(json_data)

  groups = group_data['group']

  if len(dates) == 0:
    dt_now = mx.DateTime.now()
    y, m, d = dt_now.tuple()[0:3]
    dates = ['%4d-%02d-%02d' % (y, m, d)]
    print dates
  for date in dates:
    y, m, d = date.split('-')
    ts = mx.DateTime.DateTime(int(y), int(m), int(d)).ticks()
    impressions_on_date(date, ts, groups)

  # take the dictionary down a notch
  impressions = 0
  for group in groups:
    rows = group['rows']
    for row in rows:
      #print row
      impressions = impressions + row['impressions']
  print '{\"total-impressions\" : %d}' % impressions


if __name__ == '__main__':
  get_rubi_row(sys.argv[1:])

