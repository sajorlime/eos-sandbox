#!/usr/bin/env python3


"""
    Some code to test decorators
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

def report(f):
    print f

def service(f):
    def sf(*args, **kw):
        report(f)
        args[0].foo()
        return f(*args, **kw);
    return sf

class dec_test:
    def __init__(self_, a, b, c):
        self_.a = a
        self_.b = b
        self_.c = c

    def foo(self_):
        print "I'm not a foo bar"

    @service
    def job1(self_):
        print self_.a

    @service
    def job2(self_):
        print self_.b

    @service
    def job3(self_):
        print self_.c

d = {'a' : 1, 'b' : 2}

t = dec_test('Z', 377, d)
print t

t.job1()
t.job2()
t.job3()


