#!/usr/bin/env python3
#

""" source -- a data source
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import json
import time

class Source(object):
    def __init__(self, identifier):
        self._identifier = identifier
    def readline(self):
        pass
    def __iter__(self):
        pass
    def read(self):
        pass
    def close(self):
        pass
    def id(self):
        return self._identifier

class FileSource(Source):
    def __init__(self, identifier):
        Source.__init__(self, identifier)
        self._fh = codecs.open(identifier, encoding='utf-8', mode='r')

    def readline(self):
        return self._fh.readline()

    def __iter__(self):
        return self._fh

    def read(self):
        return self._fh.read()

    def close(self):
        self._fh.close()

