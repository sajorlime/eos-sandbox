#!/usr/bin/env python3
#

""" remote source -- a data source
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import codecs
import socket
import sys

_running = True

def tcp_read(store, encapsulate, kind, vdict, host, port):
    """
    Create a socket TCP/IP SOCK_STREAM
    Args:
        store: function that stores received data
        vdict: a dictionary that contains tag identification information
        kind: string that identifies the kind of data we expect
        host: host address to connect too
        port: port to connect too
    """
    global _running
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        # Connect to server and send data
        sock.connect((host, port))
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mfh = sock.makefile()

        while _running:
            received = mfh.readline()
            if not received:
                print('%s rcvd NULL' % kind, file=sys.stderr)
                #break
            rtj = encapsulate(received, vdict)
            store(kind, rtj)


from utils.source import Source

class RemoteSource(Source):
    def __init__(self, identifier, is_two_way=False):
        Source.__init__(self, identifier)
        host, port = identifier.split(':')
        port = int(port)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((host, port))
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._is_two_way = is_two_way
        if is_two_way:
            self._handle = sock
        else:
            self._handle = sock.makefile()

    def readline(self):
        #assert(not self._is_two_way)
        return self._handle.readline()

    def __iter__(self):
        assert(not self._is_two_way)
        return self._handle

    def read(self):
        rlen = int(str(self._handle.recv(6), 'utf-8'))
        #print('RRL:', rlen, file=sys.stderr)
        return str(self._handle.recv(rlen), 'utf-8')

    def request(self, msg):
        assert(self._is_two_way)
        # requests for data are limited to configuration types
        #print('RQL:', len(msg), file=sys.stderr)
        return self._handle.sendall(bytes(msg, 'utf-8'))

    def close(self):
        self._handle.close()

