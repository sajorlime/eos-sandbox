#!/usr/bin/env python3
#

""" cmp-opr
    compares two OPR files by creating dictionaries for both and noting where
    they are different.
    cmp-opr text1.opr text2.opr
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'



import os
import os.path
import shutil
import stat
import re
import getopt
import sys
import datetime
import time
import pickle
import copy

import opr
import args

class OPRInfo:
    """
    """
    def __init__(self, v, s):
        self._v = v
        self._s = s

    def show(self):
        return "%2.4f:%s" % (self._v, self._s)

    def v(self):
        return self._v

    def s(self):
        return self._s

def sort_opr_info(a, b):
    f = a.v() - b.v()
    if f < 0:
        return 1
    if f > 0:
        return -1
    return 0


args = args.Args()

threashold = float(args.get_arg('threashold'))
#print "threashold: %f " % threashold

show_ratio = args.get_arg('ratio')
exclude = args.get_arg('exclude')
exclude_list = []
if exclude:
    exclude_list = exclude.split(':')
#print "threashold: %f" % threashold 
if not threashold:
    threashold = 0.10

opr_data0 = opr.ReadOPRFile(args.get_value(0), exclude_list)
opr_data1 = opr.ReadOPRFile(args.get_value(1), exclude_list)
#opr.SortBySymbol(opr_data0)
#opr.SortBySymbol(opr_data1)

d0dict = opr.GenerateDict(opr_data0)
d1dict = opr.GenerateDict(opr_data1)


infos = []

for item in list(d1dict.values()):
    key = item.key()
    if key in d0dict:
        item0 = d0dict[key]
        pdiff = item.percent() - item0.percent()
        adiff = abs(pdiff)
        if adiff >= threashold:
            if not show_ratio:
                infos.append(OPRInfo(pdiff, item0.show("ref:")))
            if show_ratio:
                ratio = item.percent() / item0.percent()
                infos.append(OPRInfo(ratio, item.show("cmp:")))
            else:
                infos.append(OPRInfo(pdiff, item.show("cmp:")))
    else:
        if  item.percent() >= threashold:
            if not show_ratio:
                infos.append(OPRInfo(item.percent(), item.show("umd:")))
            else:
                ratio = item.percent() / 0.01
                infos.append(OPRInfo(ratio, item.show("umd:")))

infos.sort(sort_opr_info)

for info in infos:
    print(info.show())

