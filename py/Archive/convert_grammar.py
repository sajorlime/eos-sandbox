#!/usr/bin/env python3
#

""" extract-songs
    Reads a songs grammar file and generates string for speech to text
"""

"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import codecs
import copy
import os
import os.path
import shutil
import sys

import args
import filefilter
import generate_wav

def phrase_input_xform(line, n):
    """
        Take the song line from the grammar file and string extraneous info
        Pameters:
            line -- an input line read form a grammar file of songs
        Returns:
            a simplified line.
    """
    return line.split(' :: ')
    #line = line.split(' :: ')[0]
    #return line;


def make_phrase_lists(elements):
    """
    Takes list of phrase elements and creates a list
    representation.
    Parameters:
        elements:
            An input line of the form: A B [ [ ( C | sea | see ) ] D d ] E [F] G 
            is split one spaces and passed here.
    Returns:
        A list of strings and lists. For input above
            [A, B, [[C], [D, d]], E, [F], G], i.e. a list with six
            elements, two of which are sub-lists, one of which in
            turn contains two sub-lists.

    Note: recursive function.  When a sub list is encontered it dives in.
    """

    # elements iterator, used in sub-funtions and where needed.
    el_iter = iter(elements)

    def make_sublist(deliminator, terminator):
        """
        Helper function that compiles a sub list.
        Parameters:
            deliminator: the starting item, if encountered
                deepens search for terminator.
            terminator: the character we are looking for
                to terminates the sub list.
        Returns:
            The list of strings that are contained within the
            delimitors.

        Note: uses iterator global to function of accessing elements.
        """
        items = []
        cnt = 1
        while cnt > 0:
            element = next(el_iter)
            if element == deliminator:
                cnt = cnt + 1
            elif element != terminator:
                items.append(element)
            else:
                cnt = cnt - 1;
        return items

    phrases = []


    # So the idea here is to interate over all elements.
    # When we encounter a list marker we construct a sub-list
    # of thoese elements and call ourselves recursively.
    # Sub-lists are formed either by optional or "OR" clauses,
    # indicated by "[]" or "()".
    for element in el_iter:
        if element[0] != '[' and element[0] != '(':
            phrases.append(element)
            continue
        if element[0] == '[':

            bitems = []
            bitems = make_sublist('[', ']')
            phrases.append(['['] + make_phrase_lists(bitems))
        else:
            pitems = []
            pitems = make_sublist('(', ')')
            while True:
                try:
                    # TODO(epr): make this a simple loop that checks
                    # checks all elements for '|'
                    # We have a list so we just remove the or clauses
                    pitems.remove('|')
                except ValueError:
                    break

            phrases.append(['|'] + make_phrase_lists( pitems))
    #print phrases
    return phrases


def expand_phrases(phrase_list, ret_phrases):
    """
    Takes a list, initially created by make_phrase_lists(), and
    expands all paths to explicit strings.  When if generates
    an explicit output it appends to ret_phrases.
    Parameters:
        phrase_list: the input list to expand
        ret_phrases: the outlist when simple strings are found.
            Usually, but not necessarily null on initial entry.
    Returns:
        False when it is done and the ret_phrases list has had
        all found simple phrases appened. 

    Note: A each step in the expansion process it simplifies the
    leading elelments to a simple string.  When if finds a list
    element it calls it self for each element in the list with 
    the remiander of the unexpanded data appened.
    """
    front = ''
    pos = 0
    #print
    #print 'ep enter: %s ' % phrase_list
    #print 'ep type: %s ' % type(phrase_list[pos])
    while type(phrase_list[pos]).__name__ != 'list':
        #print 'ep type: %s' % type(phrase_list[pos])
        front = '%s %s' % (front.lstrip(), phrase_list[pos])
        pos = pos + 1
        if pos >= len(phrase_list):
            #print 'ep append: %s ' % front
            ret_phrases.append(front.lstrip())
            return False
    #print 'ep front: %s llen:%d' % (front, len(ret_phrases))

    #print 'ep exp list: %s' % phrase_list[pos]
    list_type = phrase_list[pos][0]
    #print 'ep lt: %s' % list_type
    # place the empty optional phrase
    if list_type == '[':
        # this phase is optional so do one without it.
        expand_phrases([front.lstrip()] + phrase_list[pos + 1:], ret_phrases)

        expand_phrases([front.lstrip()] + phrase_list[pos][1:] + phrase_list[pos + 1:], ret_phrases)
    else:
        for phrase in phrase_list[pos][1:]:
            expand_phrases([front.lstrip(), phrase] + phrase_list[pos + 1:], ret_phrases)

        
    return True


def all_phrases(line, longest_only = False):
    """
    Expects a line to be spoken with optional substrings, and returns
    a list with all possible strings.

    Parameters:
        line -- a line of the form: A B [[C] ( D | d) ] E [F] G
            I.e. there can be nested optional substring inside
            of optional substrings.
    """
    if line == '' or line == '\n' or line[0] == '#':
        print('line:%s' % line, end=' ')
        return None
    #print 'as in: %s ' % line
    phrase_lists = make_phrase_lists(line.strip().replace('  ', ' ').split(' '))
    #print 'as all: %s ' % phrase_lists
    phrases = []
    expand_phrases(phrase_lists, phrases)
            
    #print 'as ret: %s' % phrases
    if longest_only:
        phrases.sort(lambda x, y : len(y) - len(x))
        return [phrases[0]]

    return phrases


def read_grammar_file(grammar_file):
    return filefilter.ReadFile(grammar_file, xform = phrase_input_xform)


if __name__ == '__main__':

    usage = """
    convert-grammar grammar.txt [outfile]

    takes the grammar.txt file and write to string for each possible way to say
    the grammar line.
    """

    args = args.Args(usage)
    verbose = bool(args.get_arg('verbose', False))

    grammar_file = args.get_value()
    item_file_name = args.get_value(default = '-')
    if item_file_name == '-':
        item_file = sys.stdout
    else:
        item_file = codecs.open(item_file_name, encoding='utf-8', mode='w+')

    phrase_pairs = read_grammar_file(grammar_file)

    for phrase_pair in phrase_pairs:
        #print 'mpp: %s' % phrase_pair
        phrase_list = all_phrases(phrase_pair[0])
        if not phrase_list:
            #print 'skip'
            continue
        #print >> item_file, 'beg ',
        phrase_list.sort()
        last_phrase = ''
        for phrase in phrase_list:
            if phrase == None:
                continue
            if phrase != last_phrase:
                print('%s' % phrase, file=item_file)
            last_phrase = phrase
        #print >> item_file, ' end',
        print(file=item_file)


