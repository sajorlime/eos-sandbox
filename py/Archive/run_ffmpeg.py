#!/usr/bin/env python3

"""
    run_flac.py -- runs the match command for a list of flac files (could be wave also)
    Outputs:
    filename:
    json result
    ...
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import codecs
import datetime
import os
import re
import sys
import time

# If you get an error here you need to upgrade your python version to
# 2.6
import json

#import wsgiapp as indexers
#from wsgiapp import sample_indexers
import cli

#myindexers = ['peak_d20w, 'peak_d20w_s', 'peak_d40w', 'peak_d20w_lpf25']
myindexers = ['peak_d20w', 'peak_d2020w']

def secs_to_time(secs):
    frac = secs - int(secs)
    secs = secs - frac
    h = int(secs / (60 * 60))
    secs = secs - (h * (60 * 60))
    m = int(secs / 60)
    secs = secs - (60 * m)
    return '%02d:%02d:%02d.%02d' % (h, m, secs, 100 * frac)

def run_flacs():
    global myindexers 

    parser = argparse.ArgumentParser(description='run a list of flac againt a server with match command',
                                     epilog='%(prog)s reprocesses a set of logged ID.flac files')
    parser.add_argument('files',
                        metavar='FILE',
                        nargs='+',
                        help='any number of audio files')
    parser.add_argument('--index',
                        dest='indexers',
                        action='append',
                        help='add an index to be processed')
    parser.add_argument('--server',
                        dest='servers',
                        action='append',
                        default=[os.getenv('MATCHING_URL', 'http://localhost:8005')],
                        help='add a server to be processed')
    parser.add_argument('--ofile',
                        type=str,
                        default='-',
                        help='the output file for the results, defaults to stdout')
    parser.add_argument('--log',
                        action='store_true',
                        help='log the data to the server, default %(default)s')
    parser.add_argument('--cmp_first',
                        type=int,
                        default=0,
                        help='only prints results differ by CMP (only first two indexes considered)')
    parser.add_argument('--cmp_id',
                        action='store_true',
                        help='only prints results the ids in the matches differ')
    parser.add_argument('--matches', type=int, default=3, help='matches to display')
    args = parser.parse_args()

    """
        I'm just going to iterate over the servers and indexers,
        but we could do this in parallel.
    """

    # treat cmp_id seperately from other cases. 
    # TODO(epr): see if you can clean this up with iteraters
    if args.cmp_id:
        if len(args.indexers) != 1:
            print('\nonly one indexer allowed with cmp_id', file=sys.stderr)
            parser.print_help()
            sys.exit(1)
        if len(args.servers) != 1:
            print('\nonly one server allowed with cmp_id', file=sys.stderr)
            parser.print_help()
            sys.exit(1)
        if args.cmp_first:
            print('\nonly one of cmp_first and cmp_id valid', file=sys.stderr)
            parser.print_help()
            sys.exit(1)
        fresults = []
        server = args.server = args.servers[0]
        index = args.indexers[0]
        print('files starting with %s,' % args.files[0], end=' ')
        sys.stdout.flush()
        for audio_file in args.files:
            #print audio_file
            params = {'--format': 'json',
                      '--index' : index}
            mout = cli.get_cmd_output('match', args, params, [audio_file])
            fresults.append([audio_file, mout, index, server])
            print('.', end=' ')
            sys.stdout.flush()

        id = fresults[0][1]['matches']['byscore'][0][1]
        print('first id=%s' % id)
        for result in fresults:
            #print result[1]['matches']['byscore'][0][1]
            if id != result[1]['matches']['byscore'][0][1]:
                print('    not first match in all files %s' % result[0])
        sys.exit(0)

    if args.ofile is '-':
        ofile = sys.stdout
    else:
        ofile = codecs.open(args.ofile, encoding='utf-8', mode='w')
    
    for audio_file in args.files:
        for server in args.servers:
            args.server = server
            if args.indexers:
                myindexers = args.indexers
            else:
                params = {'--format': 'json'}
                all_indexers = cli.get_cmd_output('get-indexes', args, params)
                sample_indexers = all_indexers['sample_indexers']
                myindexers = list(sample_indexers.keys())
            fresults = []
            for index in myindexers:
                tnow = datetime.datetime.now()

                params = {'--format': 'json',
                          '--index' : index}
                if args.log:
                    params['--log'] =  True

                mout = cli.get_cmd_output('match', args, params, [audio_file])
                try:
                    pass
                except:
                    print('server failed on %s' % audio_file, file=ofile)
                    continue

                fresults.append([audio_file, mout, index, server, datetime.datetime.now() - tnow])

            if args.cmp_first > 0:
                s0 = fresults[0][1]['matches']['byscore'][0][0]
                s1 = fresults[1][1]['matches']['byscore'][0][0]
                if abs(s0 - s1) < args.cmp_first:
                    print('==== %s ===== differs by %d ==================================' % (audio_file, s0 - s1), file=ofile)
                    continue
                
            print('===============================================================================', file=ofile)
            for fresult in fresults:
                print('%s, %s, %s, %s' % (audio_file.replace('.flac', ''),
                                                          fresult[2],
                                                          fresult[3],
                                                          fresult[4]), file=ofile)
                matches = fresult[1]['matches']
                byscore = matches['byscore']
                content = matches['content']
                for i in range(min(args.matches, len(byscore))):
                    score = byscore[i][0]
                    id = byscore[i][1]
                    offset = byscore[i][2]
                    info = content[id]
                    cid = info['_id']
                    try:
                        language = info['language']
                    except KeyError:
                        language = 'unk'
                    length = info['length']
                    title = info['title']
                    # score, offset, title, lang, length, cid
                    try: 
                        print(' %3d, %s, %s, %s, %s, %s' % (
                                                score,
                                                secs_to_time(offset),
                                                title,
                                                language,
                                                secs_to_time(length),
                                                cid), file=ofile)
                    except UnicodeEncodeError:
                        print('failed to print %s' % title, file=sys.stderr)
            print('\n', file=ofile)
            ofile.flush()


if __name__=='__main__':
    run_flacs()

