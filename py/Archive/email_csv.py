#!/usr/bin/env python3
"""
    address_csv.py -- class that reads in csv files that represent
    e-mail lists.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import os
import sys

# local
import enum

_Name = 'Name'
_Given_Name = 'Given_Name'
_Family_Name = 'Family_Name'
_E_mail1_Type = 'E_mail1_Type'
_E_mail1_Value = 'E_mail1_Value'
_E_mail2_Type = 'E_mail2_Type'
_E_mail2_Value = 'E_mail2_Value'
_Phone1_Type = 'Phone1_Type'
_Phone1_Value = 'Phone1_Value'
_Phone2_Type = 'Phone2_Type'
_Phone2_Value = 'Phone2_Value'
_Address1_Type = 'Address1_Type'
_Address1_Formatted = 'Address1_Formatted'
_Address1_Street = 'Address1_Street'
_Address1_City = 'Address1_City'
_Address1_PO_Box = 'Address1_PO_Box'
_Address1_Region = 'Address1_Region'
_Address1_Postal_Code = 'Address1_Postal_Code'
_Address1_Country = 'Address1_Country'
_Address1_Extended_Address = 'Address1_Extended_Address'

class email_entry:
    """
    Info for an address
    """
    def __init__(self, the_enum, address):
        self._enum = the_enum
        self._address = address

    def name(self):
        name = self._address[self._enum.value(_Name)]
        given = self._address[self._enum.value(_Given_Name)]
        family = self._address[self._enum.value(_Family_Name)]
        if given and family:
            return [given, family]
        if name and family:
            return [name, family]
        if not name:
            email = self._address[self._enum.value(_E_mail1_Value)]
            return [email[0], None]
        return None

    def given(self):
        return self._address[self._enum.value(_Given_Name)]

    def family(self):
        return self._address[self._enum.value(_Given_Name)]

    def email1(self):
        return [self._address[self._enum.value(_E_mail1_Value)],
                self._address[self._enum.value(_E_mail1_Type)]]

    def email2(self):
        return [self._address[self._enum.value(_E_mail2_Value)],
                self._address[self._enum.value(_E_mail2_Type)]]

    def phone1(self):
        return [self._address[self._enum.value(_Phone1_Value)],
                self._address[self._enum.value(_Phone1_Type)]]

    def phone2(self):
        return [self._address[self._enum.value(_Phone2_Value)],
                self._address[self._enum.value(_Phone2_Type)]]

    def formated_address(self):
        # TODO(epr): test for formated, and created if not preent.
        return self._address[self._enum.value(_Address1_Formatted)]

    def address(self):
        mail_box = self._address[self._enum.value(_Address1_Street)]
        if not mail_box:
            mail_box = self._address[self._enum.value(_Address1_PO_Box)]
        return [self._address[self._enum.value(_Address1_Type)],
                mail_box,
                self._address[self._enum.value(_Address1_City)],
                self._address[self._enum.value(_Address1_Region)],
                self._address[self._enum.value(_Address1_Postal_Code)],
                self._address[self._enum.value(_Address1_Country)]]

    def extended_address(self):
        return self._address[self._enum.value(_Address1_Extended_Address)]

    def __str__(self):
        str = ('%s' % self.name()) + ',' \
              + self.given() + ',' \
              + self.family() + ',' \
              + ('%s' % self.email1()) + ',' \
              + ('%s' % self.email2()) + ',' \
              + ('%s' % self.phone1()) + ',' \
              + ('%s' % self.phone2()) + ',' \
              + self.formated_address() + ',' \
              + ('%s' % self.address()) + ',' \
              + self.extended_address()
        return str

    def format(self, the_format):
        str = ''
        flist = the_format.split(',')
        for iformat in flist:
            item = self._enum.value(iformat)
            if type(item).__name__ == "list":
                for sub_item in item:
                    str += sub_item + ' '
                str = str.strip()
                str += ','
            else:
                str += item + ','
        return str.rstrip(',')
                



class email_csv:
    """
    encapsulates the contents of a email csv file.
    """
    def __init__(self, fields, addresses):
        self._af = enum.enum(fields)
        self._addresses = []
        for addr in  addresses:
            self._addresses.append(email_entry(self._af, addr.split(',')))

    def name(self, position):
        if position > len(self._addresses):
            return None
        return self._addresses[position].name()

    def email1(self, position):
        if position > len(self._addresses):
            return None
        return self._addresses[position].email1()

    def email2(self, position):
        if position > len(self._addresses):
            return None
        return self._addresses[position].email2()

    def phone1(self, position):
        if position > len(self._addresses):
            return None
        return self._addresses[position].phone1()

    def phone2(self, position):
        if position > len(self._addresses):
            return None
        return self._addresses[position].phone2()

    def address(self, position):
        if position > len(self._addresses):
            return None
        return self._addresses[position].address()

    def __iter__(self):
      for addr in self._addresses:
        yield addr


if __name__ == '__main__':
    import args
    import filefilter

    args = args.Args("email_csv (test only) email.csv-file")

    af = ['Name',
          'Given_Name',
          'Family_Name',
          'E_mail1_Type',
          'E_mail1_Value',
          'E_mail2_Type',
          'E_mail2_Value',
          'Phone1_Type',
          'Phone1_Value',
          'Phone2_Type',
          'Phone2_Value',
          'Address1_Type',
          'Address1_Formatted',
          'Address1_Street',
          'Address1_City',
          'Address1_PO_Box',
          'Address1_Region',
          'Address1_Postal_Code',
          'Address1_Country',
          'Address1_Extended_Address']

    addresses = [
        'a a,a,a,emt1,a@a.edu,emt1,a@a.com,pt11,401-555-1212,pt21,410-555-1212,a1,format,astreet,acity,,reg,acode,cuntry,ext-addr',
        'b b,b,b,emt1,b@b.edu,emt2,b@b.com,pt12,402-555-1212,pt22,420-555-1212,a2,format,bstreet,bcity,,reg,bcode,cuntry,ext-addr',
        'C C,C,C,emt1,C@C.edu,emt3,C@C.com,pt13,403-555-1212,pt23,430-555-1212,a1,format,cstreet,ccity,,reg,ccode,cuntry,ext-addr',
        'D D,D,D,emt1,D@D.edu,emt5,D@D.com,pt15,405-555-1212,pt25,450-555-1212,a3,format,,dcity,dbox,reg,dcode,cuntry,ext-addr',
        'E E,E,E,emt1,E@E.edu,emt6,E@E.com,pt16,406-555-1212,pt26,460-555-1212,at,format,estreet,ecity,,reg,ecode,cuntry,ext-addr',
        'F F,F,F,emt1,F@F.edu,emt7,F@F.com,pt17,407-555-1212,pt27,470-555-1212,ad,format,fstreet,fcity,,reg,fcode,cuntry,ext-addr',
        'G G,G,G,emt1,G@G.edu,emt8,G@G.com,pt18,408-555-1212,pt28,480-555-1212,at,format,,gcity,box,reg,gcode,cuntry,ext-addr',
        'H H,H,H,emt1,H@H.edu,emt9,H@H.com,pt19,409-555-1212,pt29,490-555-1212,at,format,hstreet,hcity,,reg,hcode,cuntry,ext-addr',
        'I I,I,I,emt1,I@I.edu,emtb,I@I.com,pt1b,411-555-1212,pt2b,421-555-1212,ax,format,istreet,icity,,reg,icode,cuntry,ext-addr',
        'J J,J,J,emt1,J@J.edu,emtc,J@J.com,pt1c,412-555-1212,pt2c,422-555-1212,ay,format,,jcity,jbox,reg,jcode,cuntry,ext-addr',
        ]

    csv = email_csv(af, addresses)

    for addr in csv:
        print(addr)


