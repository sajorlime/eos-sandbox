#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""
    # The GPIO map is created from SugarCubeMb r8_47_40pin_to_20pin_display.pdf, and
    # http://www.newhavendisplay.com/specs/NHD-2.4-240320CF-CSXN-F.pdf
    # Because we are using an 8-bit interface send data to DB-8 through DB-15.
    # CPU              ST7789S, 8-bit
    # name pin        GPIO -->  pin name
    # ------------------------------
    # reset,   37,    GPIO14 --> Reset, also pd disp9
    # /CS,     38,    GPIO26 --> /CS  -- chip select not
    # D/C,     32,    GPIO24 --> D/C  -- data/command
    # /WR,     28,    GPIO23 --> /WR  -- write not
    # /RD,     10,    GPIO16 --> /RD  -- read not
    # PD DISP1, 21,   GPIO07 --> DB08
    # PD DISP2, 22,   GPIO20 --> DB09
    # PD DISP3, 23,   GPIO08 --> DB10
    # PD DISP4, 24,   GPIO21 --> DB11
    # PD DISP5, 26,   GPIO22 --> DB12
    # PD DISP6, 29,   GPIO10 --> DB13
    # PD DISP7, 31,   GPIO11 --> DB14
    # PD DISP8, 36,   GPIO25 --> DB15
    # ? IMO,    8     GPIO15 --> IM0 set to 1 enable 8-bit. set at init time.

display_pin_names = [
    'reset', # disp 9/reset
    'cs'   ,
    'dc'   ,
    'wr'   ,
    'rd'   ,
    'db00' , # disp 1
    'db01' , # disp 2
    'db02' , # disp 3
    'db03' , # disp 4
    'db04' , # disp 5
    'db05' , # disp 6
    'db06' , # disp 7
    'db07' , # disp 8
]

display_to_cpu_map = {
    'reset' : 37, # disp 9/reset
    'cs'    : 38,
    'dc'    : 32,
    'wr'    : 28,
    'rd'    : 10,
    'db00'  : 21, # disp 1
    'db01'  : 22, # disp 2
    'db02'  : 23, # disp 3
    'db03'  : 24, # disp 4
    'db04'  : 26, # disp 5
    'db05'  : 29, # disp 6
    'db06'  : 31, # disp 7
    'db07'  : 36, # disp 8
}

pic_cpu_pins = [ 11, 13, 16, 16, 18 ]
i2c_cpu_pins = [ 3, 5 ]
panel_cpu_pins = [ 11, 13, 15, 16, 18 ]
im_cpu_pins = [ 8 ]

claimed_cup_cpu_pins = pic_cpu_pins + i2c_cpu_pins + panel_cpu_pins + im_cpu_pins
claimed_cup_cpu_pins.sort()
print claimed_cup_cpu_pins

for key in display_to_cpu_map.keys():
    if display_to_cpu_map[key] in claimed_cup_cpu_pins:
        pin = display_to_cpu_map[key]
        print key, 'has claimed gpio:', pin
        if pin in pic_cpu_pins:
            print '\t', pin, 'in pic_cpu_pins'
        if pin in i2c_cpu_pins:
            print '\t', pin, 'in i2c_cpu_pins'
        if pin in panel_cpu_pins:
            print '\t', pin, 'in panel_cpu_pins'
        if pin in im_cpu_pins:
            print '\t', pin, 'in im_cpu_pins'

pin_to_gpio_map = {
    1  : '3.3v',
    2  : '5v',
    3  : 2,
    4  : '5v',
    5  : 3,
    6  : 'grnd',
    7  : 4,
    8  : 14,
    9  : 'grnd',
    10 : 15,
    11 : 17,
    12 : 18,
    13 : 27,
    14 : 'grnd',
    15 : 22,
    16 : 23,
    17 : '3.3v',
    18 : 24,
    19 : 10,
    20 : 'grnd',
    21 : 9,
    22 : 25,
    23 : 11,
    24 : 8,
    25 : 'grnd',
    26 : 7,
    27 : 0,
    28 : 1,
    29 : 5,
    30 : 'grnd',
    31 : 6,
    32 : 12,
    33 : 13,
    34 : 'grnd',
    35 : 19,
    36 : 16,
    37 : 26,
    38 : 20,
    39 : 'grnd',
    40 : 21,
    }

#print 14, pin_to_gpio_map[14], '? grnd'
#print 40, pin_to_gpio_map[40], '? 21'
#print pin_to_gpio_map 
display_gpios = []
for pin in pin_to_gpio_map.keys():
    gpio = pin_to_gpio_map[pin]
    if isinstance(gpio, int):
        display_gpios.append(gpio)
    else:
        print 'reject %d which is %s' % (pin, gpio)

display_gpios.sort()
print display_gpios

fbtft_pin_map = ''
for disp_pin in display_pin_names:
    cpu_pin = display_to_cpu_map[disp_pin]
    gpio_pin = pin_to_gpio_map[cpu_pin]
    fbtft_pin_map += '%s:%d,' % (disp_pin, gpio_pin)
    print '%2d, GPIO%02d, %s' % (cpu_pin, gpio_pin, disp_pin)
print fbtft_pin_map
fbtft_pin_map = fbtft_pin_map[:len(fbtft_pin_map) - 1] + ' '
print fbtft_pin_map


