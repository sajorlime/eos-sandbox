#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#

"""
    extract_audio_match.py -- command line program to extract
    audio mataches from SurgarServre logs and write to a new
    file in order to test.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import json
import os
import sys


def parse_logs_for_audio_recognition(log_file, rec_file):
    wfd = open(options.recognition, 'w+')
    with open(log_file, 'r') as rfd:
        line = rfd.readline()
        if ' audio match: : {\n' in line:
        if 'audio match: : {' in line:
           



ap=argparse.ArgumentParser()
#sub=p.add_subparsers()

ap.add_argument('--log', '-a',
                type=str,
                dest='log',
                help='Sugar Server Log file')
ap.add_argument('--rec',
                type=str,
                dest='recognition',
                help='output recognition data')
ap.add_argument('--limit',
                type=int,
                dest='limit',
                default=None,
                help='limit the audio matches we want')

options = ap.parse_args()


parse_logs_for_audio_recognition(rfd, wfd)

