#!/usr/bin/env python3
"""
    PLAY_MUSICARTIST.py -- program for doing the HTTP part of a query for artist.
    called via HTTP from asr_action PLAY_MUSIC_ARTIST

    Note: this first version is a cgi program.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""


import os
import cgi
import codecs
import random
import sys

from playable_package import pp_exception
from m3uplaylist import m3uplaylist
from m3u8playlist import m3u8playlist
from html_page import start_page, end_page

from aviga import _TEST, \
                  _BROWSER, \
                  _AVIGA_SERVICE, \
                  _ARTIST_ROOT, \
                  _DOC_ROOT, \
                  _QUERY, \
                  _QUERY_ROOT, \
                  _AVIGA_SERVICE


# this line enables output off error to a webpage!
import cgitb; cgitb.enable()

class aviga_query:
    """
    encapsulates the query values
    TODO(epr): generalize move out of here
    """
    def __init__(self, query):
        try:
            self._qlist = query[query.find('?') + 1:].split(',')
        except ValueError:
            # queries must have at least three values, client, user, & type
            raise pp_exception('badly formed query: %s' % query_str)

        self._qdict = {}

        for qitem in self._qlist:
            item = qitem.split('=')
            self._qdict[item[0]] = item[1].replace('%20', ' ');

        try:
            self._artist = self._qdict['artist']
        except KeyError:
            self._artist = None
            raise pp_exception('ops failed, no artist in query')

        try:
            self._type =  self._qdict['type'] 
        except KeyError:
            raise pp_exception('oops failed, no type in query')

        try:
            self._client =  self._qdict['client'] 
            #print type
        except KeyError:
            raise pp_exception('oops failed, no client in query')

        try:
            self._user =  self._qdict['user'] 
        except KeyError:
            raise pp_exception('oops failed, no user in query')


    def artist(self):
        return self._artist

    def type(self):
        return self._artist

    def user(self):
        return self._artist

    def client(self):
        return self._artist

    def value(index):
        try:
            return self._qdict(index)
        except KeyError:
            return None



def main():
    """
    The entry point of the CGI script of handling an artist query.
    """

    start_page()
    print('<pre>')

    try:
        query = aviga_query(_QUERY)

        #print 'doc_root:%s' % _DOC_ROOT
        #print 'query:%s' % _QUERY
        #print 'script:%s' % _QUERY_ROOT
        #print 'browser:%s' % _BROWSER
        #print 'aviga_service:%s' % _AVIGA_SERVICE
        try:
            qlist = _QUERY[_QUERY.find('?') + 1:].split(',')
        except ValueError:
            qlist = [_QUERY]
        qdict = {}
        #print 'qlist:%s' % qlist
        for qitem in qlist:
            item = qitem.split('=')
            qdict[item[0]] = item[1].replace('%20', ' ');
            #print "%s->%s" % (item[0], qdict[item[0]])

        try:
            artist = qdict['artist']
            #print artist
        except:
            print('<error>oops failed, no artist in query</error>')
            sys.exit(0);
        try:
            type =  qdict['type'] 
            #print type
        except:
            print('<error>oops failed, no type in query</error>')
            sys.exit(0);

        #print 'make path from query'
        #print 'art_root:%s' % _ARTIST_ROOT 

        path = _DOC_ROOT + '/' + _ARTIST_ROOT + '/' + type
        #print 'path:%s' % path
        if not os.path.exists(path):
            print('<error>path does not exist \'%s\' failed for \'%s\'\"</error>' % (path, _QUERY))
            sys.exit(0)

        if 'album' in qdict:
            album = qdict['album']
            path = path \
                    + '/' \
                    + artist \
                    + '~' \
                    + album + '.m3u'
            if not os.path.exists(path):
                print('<error>album not available: \'%s\' failed for \'%s\'\"</error>' % (path, _QUERY))
                sys.exit(0)
            plist = m3uplaylist(path, album, artist)

        else:
        
            #print 'no playlist artist:%s' % artist
            # if no play list then we try to find one.
            dlist = os.listdir(path);
            alist = []
            artist_match = artist + '~'
            for file in dlist:
                file_lower = file.lower()
                if artist_match.lower() in file_lower:
                    if ((file_lower.rfind('~albums') < 0)
                       and (file_lower.rfind('allsongs') < 0)
                       and (file_lower.rfind('.m3u8') < 0)):
                        alist.append(file)
            #print alist
            if len(alist) == 0:
                print('<error>artist not available: \'%s\' failed for \'%s\'\"</error>' % (path, _QUERY))
                sys.exit(0)
            qdict['album'] = alist[int(len(alist) * random.random())]       
            album = qdict['album']
            path = path \
                    + '/' \
                    +  album
            #print 'm3uplaylist:%s' % path
            album = album.split('~')[1]
            album = album[0 : album.rfind('.m3u') ]
            plist = m3uplaylist(path, album, artist)


        print(plist)

        
    except NameError as v:
        print('<error>name error \'%s\' failed for \'%s\'\"</error>' % (v, _QUERY))
    except ValueError as v:
        print('<error>value error \'%s\' failed for \'%s\'\"</error>' % (v, _QUERY))
    except pp_exception as v:
        print('<error>pp error \'%s\' failed for \'%s\'\"</error>' % (v, _QUERY))
    except :
        print('<error>query \'%s\' failed\"</error>' % _QUERY)

    print('</pre>')
    end_page()

main()
