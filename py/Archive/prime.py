#!/usr/bin/env python3
#
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

"""
    prime.py return prime or not
    
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import argparse
import os
import os.path
import re
import sys
import math

_primes = [
    2, 3, 5, 7, 11, 13, 17, 19, 23, 29,
    31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
    73, 79, 83, 89, 97, 101, 103, 107, 109, 113,
    127, 131, 137, 139, 149, 151, 157, 163, 167, 173,
    179, 181, 191, 193, 197, 199]

_minprime = 2
_maxprime = 199

def prime(n):
    if n <= 0 or not type(n) == int:
        print('oops number must be greater than zero:', n)
        return
    if not (n & 1):
        return False
    plimit = int(math.sqrt(n))
    if plimit > _maxprime:
        print('too big for me')
        return None
    #print('plimit:', plimit)
    for pcan in _primes[1:]: # start with the 3
        if pcan > plimit:
            print('end:', end='')
            return True
        if n % pcan == 0:
            print('zero:', end='')
            return False;
    print('endend:', end='')
    return False

if __name__=='__main__':
    parser = argparse.ArgumentParser(description='factorial')

    parser.add_argument('nums',
                        nargs='+',
                        type=int,
                        default=[],
                        help='one or more numbers to calculate factorial')
    args = parser.parse_args()
    print(args.nums)
    for n  in args.nums:
        print('prime(%d) ==' % n,  prime(n))

