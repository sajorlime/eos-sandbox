#!/usr/bin/python
# test
#


class enum:
    def __init__(self, elist):
        i = 0
        self._dict = {}
        for item in elist:
            self._dict[item] = i
            i = i + 1


    def value(self, item):
        try:
            return self._dict[item]
        except KeyError:
            return -1

    def __str__(self):
        return 'self: %s' % self._dict


if __name__ == '__main__':
    mye = enum(['abc', 'efg', 'hij', 'lmn', 'opqr'])

    print(mye)

    print(mye.value('abc'))
    print(mye.value('efg'))
    print(mye.value('hij'))
    print(mye.value('lmn'))
    print(mye.value('opqr'))
    print(mye.value('stuv'))
    print(mye.value('xyz'))
            
