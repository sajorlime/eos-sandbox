#!/usr/bin/env python3
#

""" runACSTest.py
    take a seed and a file and output a randomized line list.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""


__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import os
import os.path
import random
import shutil
import sys
import time

import args
import filefilter


usage = """
runACSTest [-seed=0.m] [ip=ip-addr] [port=NUM] [atc=atc-path] [request=xml|bin] [err=quit|cont] pcm-audio-file-list

run ACSTestClient againt a list of files.
    ip default: localhost
    port default: 27111
    seed defult: None
    request defult: xml
    err defult: cont
    atc defult: /d/_dev/_c2/CommServer_Pro/test/bin/Debug/ACSTestClient.exe
"""


if __name__ == "__main__":
    args = args.Args(usage)

    seed = bool(args.get_arg('seed', None))
    if seed:
        random.seed(seed)
    ip = args.get_arg('ip', "localhost")
    port = int(args.get_arg('port', 27111))
    request = args.get_arg('request', "xml")
    err = args.get_arg('err', "cont")
    atc = args.get_arg('atc', "/d/_dev/_c2/CommServer_Pro/test/bin/Debug/ACSTestClient.exe")

    print(args.len())
    if args.len() < 1:
        print(usage)
        sys.exit(1)

    # get the values
    file_lines = filefilter.ReadFile(args.get_value())

    #print file_lines[0]
    # TODO(epr): while this was fun I could have used random.shuffle() if
    # I had read the definition first.
    random.shuffle(file_lines)
    for out_line in file_lines:
        command = "%s --host=%s --port=%s --request=%s --wav=%s" % (
                    atc, ip, port, request, out_line)
        print(command)
        r = os.system(command)
        print(r)
        if r == 0:
            time.sleep(60)
        else:
            print('err returned')
            if err == 'quit':
                break;
            if r > 65000:
                break;
    

