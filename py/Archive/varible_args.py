#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import sys

def test_var_args(farg, *args):
    print("formal arg:", farg, *args)
    for arg in args:
        print("another arg:", arg)

def test_var_kwargs(farg, **kwargs):
    print("formal arg:", farg)
    for key in kwargs:
        print("another keyword arg: %s = %s" % (key, kwargs[key]))

def test_var_args_call(arg1, arg2, arg3):
    print("arg1:", arg1)
    print("arg2:", arg2)
    print("arg3:", arg3)

# any number of args
#test_var_args('call', 1, 2, 3)
# named args
#test_var_kwargs(farg=1, myarg2='two', myarg3=3)
# pass a list, or sequence
args = ('two', 3)
#test_var_args_call('sequnce', *args)
a_kwargs = {'arg3' : 3, 'arg2': 'two'}
#test_var_args_call('dictorary', *args)
a_kwargs['arg4'] = None
#test_var_args_call('dictorary+', *args)

_default_payload = {'a' : 'eo', 'b' : 'paul', 'c' : 'rojaa'}
def build_payload(url_type, *params):
    """
    Args:
        url_type: string that is a key into _discogs_bash_urls
        params: a dictorarises of key value pairs to be added to URL, in the form &key=value
    Returns:
        a payload ditionary suitable for a call to reqeusts.get(url, params=payload)

    """
    payload = _default_payload.copy()
    print(url_type, 'l', len(params), payload)
    i = 0
    for p in params:
        print('bp', i, p)
        print('pay', i, payload)
        payload.update(p)
        print('pay+', i, payload)
        i += 1
    #print('ex', payload)
    return payload

_payload0 = {'d' : 'roger', 'e' : 'E', 'f' : 'binns'}
_payload1 = {'g' : 'kevin', 'h' : 'H', 'c' : 'Rojas'}
#print('p0', _payload0)
#print('p1', _payload1)
#p = build_payload('xxx', _payload0, _payload1)
#print('built-payload', p)

def use_payload(url_type, *params):
    i = 0
    #prin('default', _default_payload, len(params))
    #for p in params:
    #    print(i, p)
    #    i += 1
    payload = build_payload(url_type, *params)
    print(url_type, payload)

_payload2 = {'a' : 'Emilio', 'd' : 'Roger', 'g' : 'Kevin', 'c' : 'Rojas'}
print('p0', _payload0)
print('p1', _payload1)
print('p2', _payload2)
use_payload('USE', _payload0, _payload1, _payload2)

test_var_args('FXX', 1, 2, 3)


