#!/usr/bin/env python3
## -*- coding: utf-8 -*-
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""


""" Interface for the platform API.

    We query the players on the home and way teams, retrieve field infomation,
    and write location updates.
"""

__author__ = 'eo@aaf.com (Emilio Rojas)'

import asyncio
import base64
import datetime
import json
import os.path
import queue
import re
import sys
import time

import isolynx

import utils.csv_filter as csvf
from utils.req_service import ReqException
from utils.req_service import ReqService


class PlatformException(ReqException):
    pass

"""
The items below are combined to from the query.
gen_query_str transverses the structure to generate the query.
"""
_unique_id = 'str'
_time = 'str'
_jersey_number = 'str'
_platoon = 'str'
_position = 'str'
_telemetry_tags = list
_player_name_elements = {'familyName': 'str',
                         'givenName': 'str',
                         'middleName': 'str',
                         'suffix': 'str',
                        }
_player_node_elements = {'id': _unique_id,
                         'name': _player_name_elements,
                         'jerseyNumber': _jersey_number,
                         'platoon': _platoon,
                         'position': _position,
                         'telemetryTagIds': [_telemetry_tags],
                        }
_player_edges = {'node': _player_node_elements}
_player_connection_elements = {'edges': [_player_edges]}
_game_availability = {'shortName': 'str', 'url': 'str'}
_game_clock = {'seconds':'int'}
_game_status = {'phase': 'str', 'quarter': 'int'}
_team_elements = {'id': _unique_id,
                 'name': 'str',
                 'nickname': 'str',
                 'playersConnection': _player_connection_elements,
                }
_address = {'locality': 'str', 'postalCode': 'str'}
_ball_id = {'id': 'str'}
_ball_node = {'node': _ball_id}
_ball_edges = {'edges': [_ball_node]}
_stadium_elements = {'name': 'str',
                     'id': _unique_id,
                     'address': _address,
                    }
_game_node_elements = {'time': _time,
                       'id': _unique_id,
                       'stadium': _stadium_elements,
                       'ballsConnection(last: 10)': _ball_edges,
                       'homeTeam': _team_elements,
                       'awayTeam': _team_elements,
                       'status': _game_status,
                       'clock': _game_clock,
                       'availability': _game_availability,
                      }
_game_connection_elements = {'nodes': [_game_node_elements]}
_games_query_elements = {'gamesConnection(atOrAfterTime:"%sZ")': _game_connection_elements}

_quaternion = {'x': float,
               'y': float,
               'z': float,
               'w': float,
              }
_point3d = {'x': float,
            'y': float,
            'z': float,
           }
_createPersonTelemetrySamples = {'time': 'str',
                                 'positionMeters': _point3d,
                                 'orientation': _quaternion,
                                 'personId': 'str',
                                }
_telemetry_samples = {'createPersonTelemetrySamples': 'str',
                                             'samples': [_createPersonTelemetrySamples],
                                      }
_telemetry_params = {'gameid': 'str',
                     'samples': [_telemetry_samples],
                    }
_telemtry_mutation = {'createPersonTelemetrySamplesResult(%(args)s)': _telemetry_params}


_level = 0
_spaces = '                              '
def gen_query_str(root, qresult=''):
    """
    generate a query string based on nested dicts and str values
    Args:
        root: a dictionary with root query containing members that are the sub-elements.
        qresult: just the place to start building the query.
    Return:
        a string the represents a platform API request.
    """
    global _level, _spaces
    #print(root, qresult, _level)
    #print(_level, '\n', qresult)
    _level += 1
    qresult += '{\n'
    for key in root:
        qresult += '%s%s ' % (_spaces[0:2*_level], key)
        #print(qresult)
        next_el = root[key]
        if type(next_el) is list:
            # we could generalize by doing this for each member of list
            # but I don't think this applies to graphql
            next_el = next_el[0]
        if type(next_el) is dict:
            qresult = gen_query_str(next_el, qresult)
            #qresult += '%s}\n' % _spaces[0:2*_level]
        if type(next_el) is str:
            qresult += '\n'
            #print(qresult)
    _level -= 1
    qresult += '%s}\n' % _spaces[0:2*_level]
    #print('leave', _level, qresult)
    return qresult


def gen_mutation_str(root, *args):
    mutation = 'mutation {\n'
    for key in root.keys():
        print(key)
    return mutation

# person location mutation
# args: person sampels and game id
_person_location_mutation = """
mutation {createPersonTelemetrySamples(
gameId: "%s",
samples:[
  %s
])
{
samples {
id
time
}
}
}
"""

# ball location mutation.
# args: ball samples and a game id.
_ball_location_mutation = """
mutation {createBallTelemetrySamples(
gameId: "%s",
samples:[
  %s
])
{
samples {
id
time
}
}
}
"""

_loc_mutations = {
    'team1' : _person_location_mutation,
    'team2' : _person_location_mutation,
    'balls' : _ball_location_mutation,
}


# create ball mutation: returns id
_create_ball_mutation = 'mutation {createBall {id}}'

"""
 add game ball to game: returns true/false
 mutation {
   addGameBall(
     gameBall:{ballId:"UigPMX19TpYec1XIW1u6mPofQL9Y",
               gameId:"Ggp_Ggl1zsL7adGGL5V6xGOK7WQj"
              }
   )
 }
"""
_add_game_ball = 'mutation {addGameBall(gameBall: {ballId: "%s", gameId: "%s"})}'

"""
 create a team -- needed for test
 <example>
 Returns the Team Id
 Note: team name = regionName + nickname
"""
_create_team = """
mutation {createTeam(
    team:{
          abbreviation: "%s",
          stadiumId:"%s",
          regionName:"%s",
          nickname:"%s"}
          )
{id}
}"""


def create_team(abbreviation, stadium_id, region_name, nickname):
    tquery = _create_team % (abbreviation, stadium_id, region_name, nickname)
    ctdict = retrieve_query(tquery)
    return ctdict

def team_handler(args):
    res =  create_team(args.abbr, args.stadium_id, args.region, args.nickname)
    print(json.dumps(res, indent=2), file=sys.stderr)
    return res



#TODO(epr): put this stuff into a singleton class
_host = None
_port = None
_root_path = None
#_authentication = None

_query_headers = {
    'Content-Type': 'application/json',
    'User-Agent': 'isoclient',
    'connection': 'keep-alive',
    #TODO(epr): add accet encoding and look at header on receive to gzip.decompress()
    #'Accept-Encoding': 'gzip',
    #'Transfer-Encoding': 'gzip',
}

basicre = re.compile('.*@.*.com:.*')

# root of async service
_req_srvc = None
_req_pool = None

_request_queue = queue.Queue()
_reply_queue = queue.Queue()


def initialize(host, port, path, authentication, pool_size=1):
    global _host, _port, _root_path
    global _req_srvc, _req_pool
    global _request_queue, _reply_queue
    print('Set root query to:', host, port, path, file=sys.stderr)
    print('Set authentication to:', authentication, file=sys.stderr)
    protocol, _host = host.split(':')
    _host = _host.replace('//', '')
    _port = port
    _root_path = path
    #_authentication = authentication
    if basicre.match(authentication):
        encoded_creds = base64.b64encode(authentication.encode('ascii'))
        _query_headers['Authorization'] = 'Basic %s' % encoded_creds.decode('ascii')
    else:
        _query_headers['Authorization'] = 'token %s' % authentication

def terminate():
    _req_pool.close()
    _req_srvc.exit()


def hack_resp_file():
    today = datetime.datetime.today()
    stoday = '%04d-%02d-%02d.%02d' % (today.year, today.month, today.day, today.hour)
    return '/telemetry/record/replies-%s.txt' % stoday


def hack_req_file():
    today = datetime.datetime.today()
    stoday = '%04d-%02d-%02d.%02d' % (today.year, today.month, today.day, today.hour)
    return '/telemetry/record/reqs-%s.txt' % stoday


def deliver_telemetry_results(rs, gameid, loctype, apilocs):
    """
    Writes the telemetry location data to the store.
    Args:
        rs: a remote store that accepts API calls.
        gameid: the unique gameid for the game in question.
        apilocs: a list of location results in API ready format.
    """
    #if loctype == 'balls':
    #    print('DTR:', gameid, loctype, apilocs, file=sys.stderr)
    lstr = ''
    try:
        for loc in apilocs:
            lstr = '%s%s,\n' % (lstr, loc)
    except TypeError as te:
        print('DTRte:', te, file=sys.stderr)
        print('DTRte:', lstr, file=sys.stderr)
        print('DTRte:', loc, file=sys.stderr)
        print('DTRte:', apilocs, file=sys.stderr)
        sys.exit(-37)
        raise te
    mutype = _loc_mutations[loctype]
    #print('DTR:', mutype, gameid, lstr, file=sys.stderr)
    mutation = mutype % (gameid, lstr[0:len(lstr) - 1])
    try:
        # this shoudl be the only retrieve0query with wait false
        retrieve_query(mutation, wait=False)
    except PlatformException:
        print('PlatformException ... continue ...', file=sys.stderr)


def old_retrieve_query(query_uri, query):
    query_data = json.dumps({'query': query})
    req = urllib.request.Request(query_uri,
                                 query_data.encode('utf-8'),
                                 _query_headers
                                )
    #print('RQ:', query_uri, req, file=sys.stderr)
    try:
        resp = urllib.request.urlopen(req)
    except urllib.error.URLError as x:
        print('Unable to connect to: %s' % query_uri, file=sys.stderr)
        print(x, file=sys.stderr)
        sys.exit(-3)
    jquery_resp = resp.read()
    resp_dict = json.loads(jquery_resp)
    resp.close()
    with open(hack_req_file(), 'a') as rh:
        print(query, file=rh)
    try:
        if not resp_dict['data'] or resp_dict.get('errors', False):
            with open('/telemetry/record/error.out', 'a') as eoh:
                #print('RQquery(%s):' % time.time(), file=eoh)
                eoh.write(query)
                print('XQresp:', json.dumps(resp_dict, indent=2), file=eoh)
            print('XQquery:', query_data, file=sys.stderr)
            print('XQresp:', json.dumps(resp_dict, indent=2), file=sys.stderr)
            raise PlatformException(resp_dict)
    except AttributeError as x:
        print('AE:', resp_dict, file=sys.stderr)
        raise x
    with open(hack_resp_file(), 'a') as rh:
        print(json.dumps(resp_dict, indent=2), file=rh)
        #rh.write(json.dumps(resp_dict, indent=2))
    return resp_dict



def url_rcv():
    global _reply_queue
    return _reply_queue.get()

def retrieve_query(query, wait=True):
    """
    Do the work to query the URI.
    Args:
        query_uri: the root of the query
        query: the PUT value query: {query-json}
        wait: if true retrieve reqsponse
    """
    global _request_queue, _reply_queue
    _request_queue.put((wait, query, time.time(),), False)
    #print('RQ: put it in:', _request_queue, _request_queue.empty(), wait) 
    if wait:
        #print('RQ: wait:', _reply_queue, wait) 
        return url_rcv()

def create_game_ball(query_uri=None, game_id=None):
    """
    Create a game bal and attach it to the Game.
    Args:
        query_uri: override root, should be None when called from client
        game_id: an API game id not put this ball in, should not be None when called from client.
    Returns:
        the ball id, or hack for ball dict when used for testing
    """
    bdict = retrieve_query(_create_ball_mutation)
    if not game_id:
        return bdict
    ball_id = bdict['data']['createBall']['id']
    add_ball = _add_game_ball % (ball_id, game_id)
    #print('AGB ', 'B:', ball_id, '\nG:', game_id, '\nquery:', add_ball, '\n', file=sys.stderr)
    agdict = retrieve_query(add_ball)
    print('CGB ', 'B:', ball_id, 'G:', game_id, 'data:', agdict['data'], file=sys.stderr)
    return ball_id

# the player page needs and player id and a key:value pair. it will return
# {"data" { "id" : "Id passed"}}, if all is good.
_patch_player = 'mutation {patchPlayer(id: "%s", patch: {%s:%s}) {id}}'

def patch_player(player, key):
    """
    Talks back to the API updating player info
    Args:
        a player node dict with key value dirty
        key: must be 'telemetryTagIds'
    Returns:
        query result
    """
    if key == 'telemetryTagIds':
        tts = player[key]
        key_value = '["%s", "%s"]' % (tts[0], tts[1])
    else:
        raise KeyError(key)
    patch = _patch_player % (player['id'], key, key_value)
    #print('PP:', patch, file=sys.stderr)
    pdict = retrieve_query(patch)
    return pdict

_create_player = """
mutation {
    createPlayer (
    player:
        {commonName:
            {givenName: "%s",
             middleName: "%s",
             familyName: "%s",
             suffix: "%s"},
          jerseyNumber: %d,
          position: %s,
          teamId: "%s",
          biography: "To play in Telemetry trial"
         }
     )
{
id
}
}"""

def create_player(given, middle, family, suffix, jersey, position, team_id):
    pquery = _create_player % (given, middle, family, suffix, jersey, position, team_id)
    pdict = retrieve_query(pquery)
    return pdict

def player_handler(args):
    res =  create_player(args.given, args.middle, args.family, args.suffix, args.jersey, args.position, args.team_id)
    print(json.dumps(res, indent=2), file=sys.stderr)
    return res

def load_players_handler(args):
    retrieve_team_context()
    cols, reader = csvf.csv_open(args.csv)
    player_list = []
    for pline in reader:
        pmap = {cols[0]:pline[0],cols[1]:pline[1],cols[2]:pline[2], cols[3]:pline[3], cols[4]:pline[4],
                'suffix':'', 'middle':''}
        pmap['team_id'] = _team_id_map[pmap['team']]
        pmap['jersey'] = int(pmap['jersey'])
        pmap.pop('team')
        create_player(**pmap)


_patch_direction = 'mutation {patchGame(id: "%s", patch: {telemetryXDirection:%s}) {id}}'

def patch_xdirection(game_id, direction):
    patch = _patch_direction % (game_id, direction)
    pdict = retrieve_query(patch)
    return pdict


_cities = None
_stadiums = None
_regions = None
_team_abbrs = None
_team_ids = None
_team_names = None
_team_id_map = {}

#_stadium_list = [
#    'Alamodome',
#    'Georgia State Stadium',
#    'Legion Field',
#    'Liberty Bowl Memorial Stadium',
#    'Rice–Eccles Stadium',
#    'SDCCU Stadium',
#    'Spectrum Stadium',
#    'Sun Devil Stadium',
#]

_stadiums_map = {
    'georgia state': 'Georgia State Stadium',
    'georgia': 'Georgia State Stadium',
    'state': 'Georgia State Stadium',
    'liberty bowl': 'Liberty Bowl Memorial Stadium',
    'liberty ': 'Liberty Bowl Memorial Stadium',
    'memorial': 'Liberty Bowl Memorial Stadium',
    'rice–eccles': 'Rice–Eccles Stadium',
    'rice-eccles': 'Rice–Eccles Stadium',
    'sdccu': 'SDCCU Stadium',
    'county credit union': 'SDCCU Stadium',
    'spectrum': 'Spectrum Stadium',
    'sun devil': 'Sun Devil Stadium',
    'legion field': 'Legion Field',
    'legion': 'Legion Field',
}

#_city_list = [
#    "Atlanta",
#    "Birmingham",
#    "Memphis",
#    "Orlando",
#    "Salt Lake City",
#    "San Antonio",
#    "San Diego",
#    "Tempe",
#]

def list_cities():
    return list(_cities)

_cities_map = {
    "atlanta": "Atlanta",
    "birmingham": "Birmingham",
    "memphis": "Memphis",
    "orlando": "Orlando",
    "salt lake city": "Salt Lake City",
    "salt lake": "Salt Lake City",
    "san antonio": "San Antonio",
    "san diego": "San Diego",
    "tempe": "Tempe",
}

#_team_list = [
#    'Arizona Hotshots',
#    'Atlanta Legends',
#    'Birmingham Iron',
#    'Memphis Express',
#    'Orlando Apollos',
#    'Salt Lake Stallions',
#    'San Antonio Commanders',
#    'San Diego Fleet',
#]

def list_teams():
    return list(_team_names)

_teams_map = {
    'atlanta': 'Atlanta Legends',
    'birmingham': 'Birmingham Iron',
    'memphis': 'Memphis Express',
    'orlando': 'Orlando Apollos',
    'salt lake city': 'Salt Lake Stallions',
    'salt lake': 'Salt Lake Stallions',
    'san antonio': 'San Antonio Commanders',
    'san diego': 'San Diego Fleet',
    'arizona': 'Arizona Hotshots',
    'legends': 'Atlanta Legends',
    'iron': 'Birmingham Iron',
    'express': 'Memphis Express',
    'apollos': 'Orlando Apollos',
    'apollo': 'Orlando Apollos',
    'stallions': 'Salt Lake Stallions',
    'commanders': 'San Antonio Commanders',
    'commander': 'San Antonio Commanders',
    'fleet': 'San Diego Fleet',
    'hotshots': 'Arizona Hotshots',
    'hotshot': 'Arizona Hotshots',
}

def retrieve_games(game_day=None,
                   city=None,
                   home=None,
                   away=None,
                   stadium=None,
                   use_all=False,
                   date_only=False,
                   axis_games=False,
                   playing=False):
    """
    Retrieve the games on the game_day.
    Args:
        game_day: YYYY-MM-DD or none
        filters, city, home, away, stadium
        use_all: forces a return of all games
        date_only: returns a game on the game_day only
    Returns:
        a list of game day for this week
    """
    if not game_day:
        print('No game day set')
        # consider getting y-m-d and setting time to 00:00:00.0.
        game_day = datetime.datetime.now()
    if date_only:
        print('date only day set', game_day)
        monday = game_day
        game_day = game_day.isoformat()
    else:
        monday = game_day - datetime.timedelta(days=game_day.weekday())
        game_day = monday.isoformat()
    print('Game date:', game_day, file=sys.stderr)
    myquery = gen_query_str(_games_query_elements)
    games_query = myquery % game_day
    #print('FQ:', games_query, file=sys.stderr)
    full_schedule = retrieve_query(games_query)
    #print('FS:', full_schedule.keys(), file=sys.stderr)
    #print('FSD:', full_schedule['data'].keys(), file=sys.stderr)
    game_list = full_schedule['data']['gamesConnection']['nodes']
    print('Number of games in full schedule:', len(game_list), file=sys.stderr)

    # limit choices to this week
    next_monday = monday + datetime.timedelta(days=7)
    snmonday = '%s' % next_monday
    print('Retrieve games after:', snmonday, file=sys.stderr)
    # HACK(epr): doing this for the data that has funky local structure
    if snmonday < game_list[0]['time']:
        # force it to at least look at the first day.
        print('Game date %s < %s:', snmonday, game_list[0]['time'], "retrieve all", file=sys.stderr)
        use_all = True

    index = 0
    if use_all:
        index = len(game_list)
    else:
        for game in game_list:
            #print('CGT:', snmonday, game['time'], file=sys.stderr)
            if snmonday < game['time']:
                break
            index += 1
    if not (city or home or away or stadium or playing):
        print('Number of games retrieved:', index, len(game_list[0:index]), file=sys.stderr)
        return game_list[0:index]
    print('Reduce by:',
          'city is %s' % city if city else '',
          'home team is %s' % home if home else '',
          'away team is %s' % away if away else '',
          'stadium is %s' % stadium if stadium else '',
          'playing is %s' % playing if playing else '',
          file=sys.stderr)
    print('game_list len:', index, len(game_list[0:index]), file=sys.stderr)
    rgames = []
    for game in game_list[0:index]:
        status = game['status']
        if status:
            phase = status['phase']
            if phase == 'COMPLETE':
                continue # game has finished
            # HACK(epr): remove AXIS games
            availability = game['availability']
            reject = False
            for venue in availability:
                #print('VEN:', venue, file=sys.stderr)
                if not axis_games and venue['shortName'] == 'Axis':
                    #print('NO AXIS:', game['id'], game['homeTeam']['nickname'], status['quarter'], file=sys.stderr)
                    reject = True
                    break
            if reject:
                continue
        if city:
            if city.lower() in _cities_map:
                city = _cities_map[city.lower()]
            if (city == game['stadium']['address']['locality']):
                rgames.append(game)
            continue
        if stadium:
            if stadium == 'Rice-Eccles Stadium':
                stadium = 'Rice–Eccles Stadium'
            #print('SM:', stadium, stadium.lower() in _stadiums_map, file=sys.stderr)
            if stadium.lower() in _stadiums_map:
                stadium = _stadiums_map[stadium.lower()]
            if (stadium == game['stadium']['name']):
                rgames.append(game)
            continue
        if home:
            if home.lower() in _teams_map:
                home = _teams_map[home.lower()]
            if ((home == game['homeTeam']['name']) or (home == game['homeTeam']['nickname'])):
                rgames.append(game)
            continue
        if away:
            if away.lower() in _teams_map:
                away = _teams_map[away.lower()]
            if ((away == game['awayTeam']['name']) or (away == game['awayTeam']['nickname'])):
                rgames.append(game)
            continue
        if playing:
            status = game['status']
            if not status:
                #print('IGNORE:', game['id'], game['homeTeam']['nickname'], file=sys.stderr)
                continue # game hasn't started
            phase = status['phase']
            #print('PHASE:', len(rgames), phase, game['id'], file=sys.stderr)
            if phase == 'PLAYING':
                availability = game['availability']
                reject = False
                for venue in availability:
                    #print('VEN:', venue, file=sys.stderr)
                    if venue['shortName'] == 'Axis':
                        #print('NO AXIS:', game['id'], game['homeTeam']['nickname'], status['quarter'], file=sys.stderr)
                        reject = True
                if reject:
                    continue
                print('ADGAME: %s,' % game['id'],
                      'A: %s,' % game['awayTeam']['nickname'],
                      'H: %s,' % game['homeTeam']['nickname'],
                      'Stad: %s,' % game['stadium']['name'],
                      'dtime: %s,' % game['time'], '\n',
                      'Status:', status,
                      'seconds: %d' % game['clock']['seconds'] ,
                       file=sys.stderr)
                rgames.append(game)
    print('RER:', len(rgames), file=sys.stderr)
    return rgames


_teams_query = """
{
  teamsConnection(first: 100) {
    nodes {
      id
      abbreviation
      name
      nickname
      regionName
      stadium {
        name
        id
        address {locality}
      }
    }
  }
}
"""

def retrieve_team_context():
    global _cities, _stadiums, _regions, _team_abbrs, _team_ids, _team_names
    teams_result = retrieve_query(_teams_query)
    tlist = teams_result['data']['teamsConnection']['nodes']
    _cities = set()
    _stadiums = set()
    _regions = set()
    _team_abbrs = set()
    _team_ids = set()
    _team_names = set()
    for tinfo in tlist:
        abbr = tinfo['abbreviation']
        _team_abbrs.add(abbr)
        team_id = tinfo['id']
        _team_ids.add(team_id)
        tname = tinfo['name']
        _team_names.add(tname)
        tnname = tinfo['nickname']
        region = tinfo['regionName']
        _regions.add(region)
        stadium = tinfo['stadium']
        sname = stadium['name']
        _stadiums.add(sname)
        city = stadium['address']['locality']
        _cities.add(city)
        _cities_map[city.lower()] = city
        _teams_map[city.lower()] = tname
        _teams_map[tnname.lower()] = tname
        _teams_map[region.lower()] = tname
        _teams_map[abbr.lower()] = tname
        _stadiums_map[sname.lower()] = sname
        _stadiums_map[sname.lower().replace(' stadium', '')] = sname
        _stadiums_map[region.lower().replace(' stadium', '')] = sname
        _team_id_map[tname] = team_id
    #print('RTCa:', _team_abbrs)
    #print('RTCi:', _team_ids)
    #print('RTCn:', _regions)
    #print('RTCs:', _stadiums)
    #print('RTCc:', _cities)


def list_handler(args):
    retrieve_team_context()
    print()
    print('Cities:\n      %s' % '\n      '.join(sorted(_cities)), end='\n\n')
    print('Stadiums:\n      %s' % '\n      '.join(sorted(_stadiums)))
    print('You can drop word "stadium" in the stadium name\n')
    print('Teams:\n      %s' % '\n      '.join(sorted(_team_names)))
    print('You can use team abbrivations\n')
    print('Team Abbreviations:\n      %s' % '\n      '.join(sorted(_team_abbrs)))
    #print(json.dumps(_team_id_map, indent=2))
    #print(json.dumps(_teams_map, indent=2))
    #print(json.dumps(_stadiums_map, indent=2))


def ball_handler(args):
    ball_stuff = create_game_ball(query_uri=args.root_query, game_id=args.game_id)
    print('BS:', ball_stuff)

def games_handler(args):
    #print('GHd:', args.date, file=sys.stderr)
    if args.date:
        tyear, tmonth, tday = args.date.split('-')
        target_day = datetime.datetime(year=int(tyear), month=int(tmonth), day=int(tday), hour=1)
    else:
        target_day = None
    glist = retrieve_games(target_day,
                           use_all=True,
                           playing=args.playing,
                           axis_games=args.axis)
    out = {'ngames': len(glist), 'games': glist}
    with open(args.ojson, 'w') as fd:
        print('Write to ', args.ojson, file=sys.stderr)
        print(json.dumps(out, indent=2), file=fd)

#class DateHandler(argparse.Action):
#    def __init__(self, option_strings, datestr, *args, **kwargs):
#        self._dstr = datestr
#        super(CustomAction, self).__init__(option_strings=option_strings,
#                                           *args, **kwargs)
#    def __call__(self, parser, namespace, values, option_string=None):
#        if self._dstr == 'today':
#            setattr(args, 'date', datetime.datetime.today())
#            return
#        tyear, tmonth, tday = dstr.split('-')
#        setattr(args,  'date', datetime.datetime(year=int(tyear), month=int(tmonth), day=int(tday), hour=22))


if __name__ == "__main__":
    import argparse
    import pprint

    parser = argparse.ArgumentParser(description='test program for platform API access',
                                     epilog='only one of city, home, away, stadium are used')
    parser.add_argument('--date', '-d',
                        dest="date",
                        type=str,
                        default='today',
                        help='the target date, in "YYYY-MM-DD" format, or "today", default: %(default)s')
    parser.add_argument('--city', '-C',
                        dest='city',
                        type=str,
                        default=None,
                        help='the target city, e.g. San Antonio, default: %(default)s')
    parser.add_argument('--home', '-H',
                        dest='home',
                        type=str,
                        default=None,
                        help='the target home team, e.g. [San Antonio ]Commanders, default: %(default)s')
    parser.add_argument('--away', '-A',
                        dest='away',
                        type=str,
                        default=None,
                        help='the target away team, e.g. [San Diego ]Fleet, default: %(default)s')
    parser.add_argument('--stadium', '-S',
                        dest='stadium',
                        type=str,
                        default=None,
                        help='the target stadium , e.g. Alamodome, default: %(default)s')
    parser.add_argument('--playing', '-p',
                        dest='playing',
                        action='store_true',
                        help='Return games that are playing')
    parser.add_argument('--ojson', '-j',
                        dest='ojson',
                        type=str,
                        default='/telemetry/games/#(YYYY-MM-DD)-#(locality)-#(stadium)--#(home)-vs-#(away).json',
                        help='where to store data for the isolynx_client.py to read' \
                             'The #(params) are replaced, default: %(default)s')
    parser.add_argument('--host',
                        type=str,
                        default='https://api.platform.aaf.com',
                        help='Host of the URI for quaries, default: %(default)s')
    parser.add_argument('--port',
                        type=int,
                        default=80,
                        help='Port of the URI for quaries, default: %(default)s')
    parser.add_argument('--root_path',
                        type=str,
                        default='https://api.platform.aaf.com/v1/graphql',
                        help='Path of the URI for quaries, default: %(default)s')
    parser.add_argument('--config_file', '-c',
                        dest='config_file',
                        type=str,
                        default='/telemetry/config/locality.json',
                        help='the config file, set to empty to ignore, default: %(default)s')
    #parser.add_argument('--all', '-a',
    #                    dest='all',
    #                    action='store_true',
    #                    default=False,
    #                    help='return the entire schedule, can still filter by city,home,away,&stadium')
    #parser.add_argument('--ball', '-b',
    #                    dest='ball',
    #                    action='store_true',
    #                    default=False,
    #                    help='return the entire schedule, can still filter by city,home,away,&stadium')
    #parser.add_argument('--list', '-l',
    #                    dest='list',
    #                    action='store_true',
    #                    default=False,
    #                    help='show the list cities, stadiums,&  teams')

    subparsers = parser.add_subparsers(title='things to do',
                                      description='subcmds: all-games, list-local-info, create-ball, create-team',
                                      dest='spname',
                                      help='execute a sub command')

    game_parser = subparsers.add_parser('games', help='create the JSON file with all games')
    game_parser.add_argument('--ojson', '-o',
                             type=str,
                             default='/telemetry/games/all.json',
                             help='path of JSON file, default: %(default)s')
    game_parser.add_argument('--axis', '-a',
                             action='store_true',
                             help='also retrieve only Axis games')
    game_parser.set_defaults(func=games_handler)

    list_parser = subparsers.add_parser('list', help='list all the names')
    list_parser.set_defaults(func=list_handler)

    ball_parser = subparsers.add_parser('ball', help='create a ball')
    ball_parser.add_argument('--game_id', '-g', type=str, default=None, help='Add ball to game')
    ball_parser.set_defaults(func=ball_handler)

    team_parser = subparsers.add_parser('team', help='create a team')
    team_parser.add_argument('--stadium_id', '-s', type=str, required=True, default=None, help='pass a stadium id')
    team_parser.add_argument('--abbr', '-a', type=str, required=True, default=None, help='a team abbreviation')
    team_parser.add_argument('--region', '-r', type=str, required=True, default=None, help='a team region')
    team_parser.add_argument('--nickname', '-n', type=str, required=True, default=None, help='a team nickname')
    team_parser.set_defaults(func=team_handler)

    player_parser = subparsers.add_parser('player', help='create a player')
    player_parser.add_argument('--team_id', '-i', type=str, required=True, default=None, help='pass a team id')
    player_parser.add_argument('--given', '-g', type=str, required=True, default=None, help='a first/given name')
    player_parser.add_argument('--middle', '-m', type=str, default='', help='a middle name')
    player_parser.add_argument('--family', '-f', type=str, required=True, default=None, help='a last/family name')
    player_parser.add_argument('--suffix', '-s', type=str, default='', help='jr, sr, etc.')
    player_parser.add_argument('--jersey', '-j', type=int, default='', help='an integer jersey #')
    player_parser.add_argument('--position', '-p', type=str, default='', help='an position as defined in the API, see PlayerPosition')
    player_parser.set_defaults(func=player_handler)

    load_players_parser = subparsers.add_parser('load-players', help='create a player')
    load_players_parser.add_argument('--csv', '-c', type=str, required=True, default=None, help='csv file')
    load_players_parser.set_defaults(func=load_players_handler)

    args = isolynx.supplement_parser(parser)

    print('date:', args.date, file=sys.stderr)
    initialize(args.host, args.port, args.root_path, args.authentication)

    if args.date:
        if args.date == 'today':
            today = datetime.datetime.today()
            tyear = today.year
            tmonth = today.month
            tday = today.day
            args.date = '%4d-%2d-%2d' % (today.year, today.month, today.day)
            target_day = today
        else:
            tyear, tmonth, tday = args.date.split('-')
            target_day = datetime.datetime(year=int(tyear), month=int(tmonth), day=int(tday), hour=22)

    if args.spname:
        #print('SP:', args, file=sys.stderr)
        args.func(args)
        terminate()
        sys.exit(0)

    print('TD:', target_day, args.playing, file=sys.stderr)
    glist = retrieve_games(target_day,
                           city=args.city,
                           home=args.home,
                           away=args.away,
                           stadium=args.stadium,
                           playing=args.playing)
    #pprint.pprint(glist)
    #print('GL:', len(glist), file=sys.stderr)
    for game in glist:
        gid = game['id']
        stadium = game['stadium']
        locality = stadium['address']['locality']
        locality_id = stadium['id']
        sname = stadium['name']
        time = game['time']
        date = time[0:time.index('T')] # drop time of day
        path = args.ojson.replace('#(YYYY-MM-DD)', date)
        path = path.replace('#(locality)', locality)
        path = path.replace('#(stadium)', sname)
        home = game['homeTeam']['nickname']
        away = game['awayTeam']['nickname']
        path = path.replace('#(home)', home)
        path = path.replace('#(away)', away)
        print('Create:', path)
        with open(path, 'a') as fd:
            print(json.dumps(game, indent=2), file=fd)
    #print(json.dumps(glist, indent=2))
    terminate()

