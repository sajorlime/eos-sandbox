#!/usr/bin/env python3
#

""" log-time.py
    reads the copy and paste output from log output of the MediaPortal.
    cat log-file | log-time +normalize_only
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'



import os
import os.path
import shutil
import stat
import re
import getopt
import sys
import datetime
import time
import pickle
import copy
import Numeric

import opr
import args


# sure would be been good to have writen down what FI and LI mean.
# This indext the time, but I don't what 'F' or 'L' means (first, last?).
_FI = 1
_LI = 13

_THREAD_POS = 15
_THREAD_END = 18
# logger position is a position in the string after thread
_LOGGER_POS = 1
_LOGGER_END = 16
# level position is a position in the string after thread
_LEVEL_POS = _LOGGER_END + 2
_LEVEL_END = _LEVEL_POS + 3

usage = """
log-time [+/-lines] [+/-vector] [+/-average] [-min_threshold=x.xx] [-count_limit=nn] [-large_sum=y.yy]

   +/-lines   -- default +, print or suppress input lines to output.
   +/-vector  -- default +, print or suppress vector of results
     +/-threads -- default -, seperates the vector according to thread values.
   +/-average -- default +, print or suppress average event time for multiple events
   +/-percent -- default +, print or suppress percent or large
                            vs. small times as defined by threshold
   -min_threshold=x.xx, default 0.0001, the threashold at which a item if of interest.
   -count_limit=nn, defaut 0, interval when using threshold to skip insignificant items.
                    set to zero or just -count_limit to not skip.
   -large_sum=y.yy, default 0.1999, this is the lower limit for indentifying long log times.
                    These times are marked and summed and then reported as a percentage.
   +/-strip -- default -, when true strips non-time stamp from output.
   +/-normalize_only -- default -, when true supresses delta, thresholding, etc.
                        this option is exclusive of the options above.  It makes 
                        strip true.
log-time is run with direct input from std-in.
E.g.
 cat log-file | log-time +lines -min_threshold=0.001
"""

class LogTime:
    """
    LogTime is a pair of the normalized time and the string
    that was output in the log.
    Usage:
        lt = LogTime(mins, secs, offset, thread, log_string)
    """
    def __init__(self, mins, secs, offset, thread, log_string):
        self._mins = int(mins)
        self._secs = float(secs)
        self._offset = float(offset)
        self._thread = int(thread)
        self._logger = log_string[_LOGGER_POS:_LOGGER_END]
        self._level = log_string[_LEVEL_POS:_LEVEL_END]
        self._log_msg = log_string[_LEVEL_END + 1:]

    def time(self):
        return 60.0 * self._mins + self._secs

    def offset(self):
        return self._offset

    def thread(self):
        return self._thread

    def logger(self):
        return self._logger

    def level(self):
        return self._level

    def log_msg(self):
        return self._log_msg

    def show(self, normal = False, threshold = 0.01):
        if self._mins < 0:
            return self._log_msg
        if normal:
            # hack needed because format %02.6f not handled with zero padding.
            if self._secs < 10:
                tens = '0'
            else:
                tens = ''
            return ("[%02d:%s%2.6f]:%03d:%s:%s:%s"
                    % (self._mins,
                       tens,
                       self._secs,
                       self._thread,
                       self._logger,
                       self._level,
                       self._log_msg))
        else:
            if self._offset > threshold:
                atten = '!'
            else:
                atten = ''
            return "[%02d:%02.6f:+%1.3f%s]:%03d:%s:%s:%s" % (self._mins,
                                                 self._secs,
                                                 self._offset,
                                                 atten,
                                                 self._thread,
                                                 self._logger,
                                                 self._level,
                                                 self._log_msg)


class LogEvent:
    """
    """
    def __init__(self):
        self._log_times = []
        self._threads = {}

    def append(log_time):
        self._log_times.append(log_time)
        t = log_time.thread()
        if t != 0 and t not in self._threads:
            self._threads[t] = log_time.logger()

    def log_times(self):
        return self._log_times

# get and deal with command line arguments.
args = args.Args(usage)

normalize_only = bool(args.get_arg('normalize_only', False))
if not normalize_only:
    _PRINT_LINES = bool(args.get_arg('lines', True))
    _PRINT_VECTOR = args.get_arg('vector', False)
    _PRINT_PERCENT = args.get_arg('percent', True)
    _VECTOR_SKIP = int(args.get_arg('vector_skip', 5))
    _VECTOR_DT = float(args.get_arg('vector_dt', 0.25))
    _PRINT_AVERAGE = args.get_arg('average', True)
    _PRINT_STUFF = not args.get_arg('strip', True)
    #print 'strip: %s ' % not _PRINT_STUFF

    _MIN_THRESHOLD = float(args.get_arg('min_threshold', 0.0001))
    #print 'mt: %f ' % _MIN_THRESHOLD 
    _THRESHOLD_CNT_LIMIT = int(args.get_arg('count_limit', 0))
    #print 'tcl: %d ' % _THRESHOLD_CNT_LIMIT
    _LARGE_SUM_THRESHOLD = float(args.get_arg('large_sum', 0.0999))
    #print 'lst: %f ' % _LARGE_SUM_THRESHOLD
else:
    _PRINT_LINES = True
    _PRINT_VECTOR = False
    _VECTOR_SKIP = 5
    _VECTOR_DT = 0.25
    _PRINT_PERCENT = False
    _PRINT_AVERAGE = False
    _PRINT_STUFF = False
    _MIN_THRESHOLD = 0.00001
    _THRESHOLD_CNT_LIMIT = 0
    _LARGE_SUM_THRESHOLD = 100

def show_times(outputs, large_sum, total_sum):
    """
    This is a hack global function to print out a group of collected
    times.
    Args:
        outputs:
            A list of LogTime object to print.
        large_sum:
            A value that should be less then total sum that can
            show the % of time spent waiting on large delays as 
            opposed to the normal incremental delays.
        total_sum:
            the total time needed.
    Returns:
        nada
    Note:
        This function grew as a hack on some code that just did this
        in line without a LogTime object etc.  It would be better
        if one needs to extend this to do everything off of a
        a single list.
    """
    if not outputs:
        return
    if (_PRINT_LINES or _PRINT_VECTOR) and not normalize_only:
        print()

    if _PRINT_LINES:
        for oline in outputs:
            print(oline.show(normalize_only, _LARGE_SUM_THRESHOLD))
        if not normalize_only:
            print()

    if _PRINT_VECTOR:
        print()
        print("Vector:")
        tvec = "0.0"
        last_logger = ''
        event = 1
        last_event = -_VECTOR_SKIP
        time = 0.0
        last_time = -_VECTOR_DT
        for oline in outputs:
            #tvec = "%s,%1.4f" % (tvec, oline.time())
            omsg = oline.log_msg()
            logger = oline.logger()
            if (logger == last_logger 
                  or (event - last_event) < _VECTOR_SKIP
                  or (oline.time() - last_time) < _VECTOR_DT / 2.0
               ):
                omsg = ''
                logger = ''
            else:
                last_logger = logger
                last_event = event
                last_time = oline.time()
            comma_pos = omsg.find(',')
            if comma_pos > 0:
                omsg = omsg[0:comma_pos - 1]
            print('%s %s, %1.5f' % (logger, omsg, oline.time()))
            event += 1
            if (oline.time() - last_time) > _VECTOR_DT:
                last_omsg = ''
        print(tvec)

    if _PRINT_PERCENT:
        print()
        print("Large Sum: %1.3f" % large_sum)
        print("Total Sum: %1.3f" % total_sum)
        diff_sum = total_sum - large_sum 
        print("Diff: %1.3f, Percent: %0.2f" % (diff_sum, large_sum / total_sum))
        print()


def my_input():
    """
    Process the input data from stdin.

    Return:
        a string unless it is the end-of-file in which case we return None.
    """
    try:
        ins = input()
    except EOFError:
        return None
    if not ins:
        return " "
    return ins
        
# Note: all of the stuff below should be in a LogTimes ojbect.

# we are in the inner loop when True.
inner = False
# we are finished when True
finish = False

# the LogTime list we keep when reading input.
out_list = None
# the floating point time list we keep when reading input

# stats on the data
total = 0.0
min_secs = 1000.0
max_secs = 0.0
sum_sq = 0.0
items = 0

# make sure there where no unprocessed arguments.
if not args.empty():
    print("args len: %d" % args.len())
    print("unrecognized arguments")
    print(args.report())
    print(usage)
    sys.exit(3)

# the main loop!
# we may see one or more log sections.
# The first section must start on the first line (an unfortunate hack)
# but subsequent sections are started with an '!' as the first character
# of a line.
while True:
    if finish:
        break
    out_list = []

    first_line = my_input()
    if first_line[0:1] != '[':
        out_list.append(LogTime(-1, 0.0, 0.0, 0, first_line))
        continue
    #print "fl:%s" % first_line
    str_mins, str_secs = first_line[_FI:_LI].split(':')
    ref_mins = int(str_mins)
    ref_secs = 60.0 * float(ref_mins) + float(str_secs)
    llsecs = lsecs = ref_secs
    large_sum = 0.0

    thread = first_line[_THREAD_POS:_THREAD_END]
    out_list.append(LogTime(0, 0.0, 0.0, thread, first_line[_THREAD_END + 1:]))
    inner = True
    # get the first one that is below the threshold
    threshold_cnt = _THRESHOLD_CNT_LIMIT

    while True:
        if not inner:
            break
        line = my_input()
        if not line:
            # an empty line means we are at the end-of-file.
            finish = True
            break
        if line[0:1] != '[':
            # if we are at a new section print the last and restart.
            if line [0:1] == '!':
                show_times(out_list, large_sum, tsecs)
                # after showing data do some summary stats before resetting data.
                total += tsecs
                if min_secs > tsecs:
                    min_secs = tsecs
                if max_secs < tsecs:
                    max_secs = tsecs
                sum_sq += tsecs * tsecs
                items += 1
                inner = False
                continue
            if _PRINT_STUFF:
                out_list.append(LogTime(-1, 0.0, 0.0, 0, line))
            continue
        # process the line
        str_mins, str_secs = line[_FI:_LI].split(':')
        csecs = 60.0 * float(str_mins) + float(str_secs)
        tsecs = csecs - ref_secs
        mins = int(tsecs / 60.0)
        secs = tsecs - mins * 60.0
        dsecs = csecs - lsecs
        lsecs = csecs
        if dsecs < _MIN_THRESHOLD:
            threshold_cnt += 1
            if threshold_cnt < _THRESHOLD_CNT_LIMIT:
                continue;
        threshold_cnt = 0
        ddsecs = csecs - llsecs
        if ddsecs > _LARGE_SUM_THRESHOLD:
            large_sum += ddsecs
        thread = line[_THREAD_POS:_THREAD_END]
        out_list.append(LogTime(int(mins), secs, ddsecs, thread, line[_THREAD_END + 1:]))
        llsecs = lsecs

# OK we are all done with the input but we need to process 
# the last section (could be only section).
show_times(out_list, large_sum, tsecs)

if min_secs > tsecs:
    min_secs = tsecs
if max_secs < tsecs:
    max_secs = tsecs
total += tsecs
sum_sq += tsecs * tsecs
items += 1

# finally we we want to print the averages?  Only if there was more than one.
if _PRINT_AVERAGE and items > 1:
    print("mean: %1.4f for %d items" % (total / items, items))
    print("sum_sq/%d: %1.4f " % (items, sum_sq / items))
    print("min: %1.4f, max: %1.4f " % (min_secs, max_secs))

