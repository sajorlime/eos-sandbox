#!/usr/bin/env python3
#
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import copy


_d = {
    "alsa_input.pci-0000_00_1b.0.analog-stereo": {
        "bus": "pci", 
        "channels": 2, 
        "format": "s16le", 
        "module": "module-alsa-card", 
        "rate": 44100, 
        "sample_size": 16, 
        "state": "SUSPENDED", 
        "type": "source"
    }, 
    "alsa_input.usb-Burr-Brown_from_TI_USB_Audio_CODEC-00.analog-stereo": {
        "bus": "usb", 
        "channels": 2, 
        "format": "s16le", 
        "module": "module-alsa-card", 
        "rate": 44100, 
        "sample_size": 16, 
        "state": "RUNNING", 
        "type": "source"
    }, 
    'alsa_input.usb-SweetEo_SugarCube_ADDA_r1.5-class2-00-SugarCubeADDAr1.analog-stereo': {
        'added': 1513187566.366027,
        'format': 's32le',
        'bus': 'usb',
        'rate': 192000,
        'module': 'module-alsa-card',
        'channels': 2,
        'sample_size': 32,
        'state': 'IDLE',
        'type': 'source'
    }
}

def cmp_srcs(lsrc, rsrc):
    if 'sugarcube' in lsrc.lower():
        print 'lsrc:', lsrc, rsrc
        return -1
    if 'sugarcube' in rsrc.lower():
        print 'rsrc:', lsrc, rsrc
        return 1
    if lsrc.lower() == rsrc.lower():
        return 0
    if lsrc.lower() < rsrc.lower():
        return -1
    return 0

_l = copy.copy(_d.keys())
print 'L:', _l, '\n'
for l in _l:
    print l
print
print
#_s = sorted(_l, cmp=cmp_srcs)
_l.sort(cmp=cmp_srcs)
_s = _l
#_s = sorted(_l, cmp=cmp_srcs)
#_s = sorted(_l)
print
print 'S:', _s, '\n'
for s in _s:
    print s
print
print





