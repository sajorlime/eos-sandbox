#!/usr/bin/env python3
#

""" extract-songs
    Reads a songs grammar file and generates string for speech to text
"""

"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""


__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import codecs
import copy
import os
import os.path
import shutil
import sys

import args
import filefilter

usage = """
convert-songs song-grammar.txt

takes the songs-grammar.txt file and write to string for each possible way to say
the grammar line.
"""

args = args.Args(usage)
verbose = bool(args.get_arg('verbose', False))

def song_xform(line, n):
    """
        Take the song line from the grammar file and string extraneous info
        Pameters:
            line -- an input line read form a grammar file of songs
        Returns:
            a simplified line.
    """
    line = line.split(' :: ')[0]
    return line;


def all_lists(lines):
    """
    """

    def grow_list(list, deliminator, terminator):
        items = []
        cnt = 1
        while cnt > 0:
            element = next(lines)
            if element == deliminator:
                cnt = cnt + 1
            elif element != terminator:
                items.append(element)
            else:
                cnt = cnt - 1;
        return items

    phrases = []

    for element in lines:
        if element[0] != '[' and element[0] != '(':
            phrases.append(el)
            continue
        if element[0] == '[':

            bitems = []
            bitems = grow_list(bitems, '[', ']')
            phrases.append(all_lists(bitems))
        else:
            pitems = []
            pitems = grow_list(pitems, '(', ')')
            while True:
                try:
                    ptiems.remove('|') 
                except ValueError:
                    break

            phrases.append(all_lists(pitems))
        element = next_element()
    return phrases


def all_strings(line):
    """
        Expects a spoken line with optional substrings, and returns
        a list with all possible strings with and without substrings.

        Parameters:
            line -- a line of the form: A B [[C] D] E [F] G
                I.e. there can be nested optional substring inside
                of optional substrings.
    """
    if line == '' or line == '\n' or line[0] == '#':
        #print 'line:%s' % line,
        return None
    phrases = all_lists(line.split(' '))
    print(phrases)

    return phrases

if __name__ == '__main__':

    grammar_file = args.get_value()
    item_file_list = grammar_file.split('.')
    item_file_name = args.get_value(default = '-')
    if item_file_name == '-':
        item_file = sys.stdout
    else:
        item_file = codecs.open(item_file_name, encoding='utf-8', mode='w+')

    lines = filefilter.ReadFile(grammar_file, xform = song_xform)

    for line in lines:
        llist = all_strings(line)
        if not llist:
            #print 'skip'
            continue
        #print >> item_file, 'beg ',
        for phrase in llist:
            if phrase == None:
                continue
            print(phrase, end=' ', file=item_file)
        #print >> item_file, ' end',
        print(file=item_file)


