#!/usr/bin/env python3
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""
""" recognition_response.py
Handles the response from the Novauirs recognition server.

The result from the Novauris server looks like:
<result>
    <list>
    <item conf="100"><trackname>A</trackname><artistname>Rolling Stones</artistname></item>
    </list>
</result>

or 

<result>
    <list>
    <item conf="24"><trackname>A</trackname><artistname>Rolling Stones</artistname></item>
    <item conf="16"><trackname>B</trackname><artistname>Aretha Franklin</artistname></item>
    <item conf="16"><trackname>C</trackname><artistname>Duke Ellington</artistname></item>
    <item conf="8"><trackname>D</trackname><artistname>Blondie</artistname></item>
    <item conf="8"><trackname>E</trackname><artistname>Talking Heads</artistname></item>
    <item conf="5"><trackname>F</trackname><artistname>American Public Media</artistname></item>
    <item conf="3"><trackname>G</trackname><artistname>Duke Ellington</artistname></item>
    <item conf="3"><trackname>H</trackname><artistname>Talking Heads</artistname></item>
    <item conf="2"><trackname>I</trackname><artistname>Duke Ellington</artistname></item>
    <item conf="1"><trackname>J</trackname><artistname>KC & the Sunshine Band</artistname></item>
    </list>
</result>

or

some unexpected error behavior.  Maybe we add try blocks later!

"""

import sys
import os
import time
import urllib.request, urllib.parse, urllib.error
import wave
import xml.dom
from xml.dom import minidom
from xml.dom.minidom import parse, parseString

import args
import client

import args

class recognition_item:
    """
    Encapsulates a single item returned from the Novauris server
    as defined by the test grammar.
    """
    def __init__(self, conf, title, artist):
        self._conf = int(conf)
        self._title = title.lower()
        self._artist = artist.lower()

    def conf(self):
        return self._conf

    def title(self):
        return self._title

    def artist(self):
        return self._artist

    def __str__(self):
        return '<%d, %s, %s>' % (self._conf, self._title, self._artist)


class recognition_response:
    """
    Holds the XMl result string.
    """
    def __init__(self, response):
        self._response = response
        self._recognition_items = []

        try:
            xmlStr =  minidom.parseString(response)
        except xml.parsers.expat.ExpatError:
            print('response failed to parse:\n%s' % response, file=sys.stderr)
            raise ValueError
            

        result_node = xmlStr.getElementsByTagName('result')
        if not result_node:
            raise ValueError

        for item_node in xmlStr.getElementsByTagName('item'):
            conf = item_node.getAttribute('conf')
            title = item_node.firstChild.firstChild.data
            artist = item_node.childNodes[1].firstChild.data
            ri = recognition_item(conf, title, artist)
            self._recognition_items.append(ri)
            #print '<%s, %s, %s>' % (conf, title, artist)
            #print '<%s, %s, %s>' % (ri, self._recognition_items, len(self._recognition_items))


    def find_artist(self, artist):
        #print self._recognition_items[0]
        al =  [x for x in self._recognition_items if x.artist() == artist]
        #print al
        return al
        #return filter(lambda x : x.artist() == artist, self._recognition_items)

    def find_title(self, title):
        return [x for x in self._recognition_items if x.title() == title]

    def find_conf(self, conf):
        return [x for x in self._recognition_items if x.conf() > conf]

    def items(self):
        return self._recognition_items

"""
"""
_SUCCESS = 0
_CONCERN = 1
_FAILURE = 2
_STATUS_STR = ['SUCCESS', 'CONCERN', 'FAILURE']

class recognition_value:
    """
    """

    def __init__(self, status, track, artist, resp_items):
        self._status = int(status)
        self._track = track
        self._artist = artist
        self._resp_items = resp_items

    def success(self):
        return _SUCCESS == self._status

    def concern(self):
        return _CONCERN == self._status

    def failure(self):
        return _FAILURE == self._status

    def status(self):
        return self._status

    def track(self):
        return self._track

    def artist(self):
        return self._artist

    def resp_items(self):
        return self._resp_items

    def __str__(self):
        status = _STATUS_STR[self._status]
        resp_list_str = '(%d)[%s' % (len(self._resp_items), self._resp_items[0])
        for resp in self._resp_items[1:]:
            resp_list_str = '%s, %s' % (resp_list_str, resp)
        resp_list_str += ']'
            
        return '<%s, %s, %s, %s>' % (status, self._track, self._artist, resp_list_str)


def validate_response(response,
                      search_str,
                      longest):
    """
    takes a response class and a match string and returns the a
    recognition value, that has level of match, and the list of
    items that qualified it for this level.
    """
    # make it a legal xml structure.
    search_str.replace('(', '').replace(')', '').replace('  ', ' ')
    search_str= '<search>' + search_str.lower() + '</search>'
    #print search_str
    xmlStr =  minidom.parseString(search_str)
    search_el = xmlStr.documentElement
    #print 'sel type: %s' % type(search_el)
    #print 'sel: %s' % search_el
    artist = ''
    track = ''
    for se_child in search_el.childNodes:
        #print 'schild: %s' % se_child
        for se_grandchild in se_child.childNodes:
            if se_grandchild.nodeType == xml.dom.Node.TEXT_NODE:
                #print 'parentchild nn: |%s|' % se_child.nodeName
                if se_child.nodeName == 'trackname':
                    #print 'found nn: %s' % se_child.nodeName
                    track = se_grandchild.data
                elif se_child.nodeName == 'artistname': 
                    #print 'found nn: %s' % se_child.nodeName
                    artist = se_grandchild.data

    if args.Verbose(): print('looking for <a: %s, t:%s>' % (artist, track))
    #print 'ri: %s' % response.items()
    if args.Verbose(): print('ti: %s, %s' % (top_item.artist(), top_item.title()))
    match_list = [x for x in response.find_artist(artist) if x.title() == track]
    if match_list:
        return recognition_value(_SUCCESS, track, artist, match_list)

    ret = 0
    title_list = response.find_title(track)

    artist_list = response.find_artist(artist)

    at_list = [x for x in title_list if x.artist() == artist]
 
    if len(at_list) > 0:
        return recognition_value(_CONCERN, track, artist, at_list)
    if len(title_list) > 0:
        return recognition_value(_CONCERN, track, '<no-match/>' + artist + '</no-match>', title_list)
    if len(artist_list) > 0:
        return recognition_value(_CONCERN, '<no-match>' + track + '</no-match>', artist, artist_list)

    return recognition_value(_FAILURE,
                             '<no-match>' + track + '</no-match>',
                             '<no-match>' + artist + '</no-match>',
                             list(response.items()))



if __name__=='__main__':

    usage = """
    recognition_response test

    takes the grammar.txt file and write to string for each possible way to say
    the grammar line.
    """

    args = args.Args(usage)
    verbose = bool(args.get_arg('verbose', False))
    taskname = args.get_arg('taskname', 'aviga01')
    default_match = '<trackname>God Save The Queen</trackname><artistname>Sex Pistols</artistname>'
    match = args.get_arg('match', default_match)
    def_path = '/home/aviga//dev/avigacoder/python-tools/test_data/godsavethequeen.wav'
    wavefile = args.get_value(default = def_path);

    print(wavefile)
    response_str = client.nsAvigaClient(taskname, wavefile)
    print(response_str)
    try:
        rec_resp = recognition_response(response_str)
    except ValueError:
        sys.exit(-1)
    vresp = validate_response(rec_resp, match)
    print('resp: %s' % vresp)

