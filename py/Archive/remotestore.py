#!/usr/bin/env python3
#

"""remote store
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import time
import websocket

from utils.record import Record

class RemoteStore(Record):
    def __init__(self, address, gametime=time.time()):
        wsock = websocket.WebSocket()
        wsock.connect(address)
        Record.__init__(self, wsock, gametime)

    def send(self, stuff):
        self._handle.send(stuff)

    def start(self, stime=None):
        """
        start sending data, possibily overriding game time
        """
        if not stime:
            stime = self._gametime
        return

    def send_chunk(self, name, json_chunk):
        """
        Send a chunk of JSON to the websocket end point.  Each chunk is seen separately, not
        as part of a lager JSON object.
        Args:
            name: ignore here, used in file sub-class
            json_chunk: a JSON blob
        """
        if name:
            self.send('{"%s":' % name)
        self.send(json_chunk)
        if name:
            self.send('}\n')
        else:
            self.send('\n')

    def end(self):
        self.send(json.dumps({'end time' : time.time()}))

