#!/usr/bin/env python3
#
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""


class enum:
    def __init__(self, elist):
        i = 0
        self._dict = {}
        for item in elist:
            self._dict[item] = i
            i = i + 1


    def value(self, item):
        try:
            return self._dict[item]
        except KeyError:
            return -1

    def __str__(self):
        return 'self: %s' % self._dict


if __name__ == '__main__':
    mye = enum(['abc', 'efg', 'hij', 'lmn', 'opqr'])

    print(mye)

    print(mye.value('abc'))
    print(mye.value('efg'))
    print(mye.value('hij'))
    print(mye.value('lmn'))
    print(mye.value('opqr'))
    print(mye.value('stuv'))
    print(mye.value('xyz'))
            
