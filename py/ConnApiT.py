#!/usr/bin/env python3

"""
HTTP connector
"""
import argparse
import json
import requests
import sys

class Connector(object):
    """
    Http connector object for simple local http handling to buffering agent.
    We open two adjacent ports to acommodate the two
    connection-agent servers.
    """
    def __init__(self,
                 port=3773,
                 host='localhost',
                 directory='outfiles'):
        self._session = requests.Session()
        self._port = port
        self._host = host
        self._directory = directory
        self._req_path = f'http://{self._host}:{self._port}/{self._directory}'

    def get(self):
        reply = self._session.get(self._req_path)
        return json.loads(reply.text)

    def post(self, data):
        self._session.post(self._req_path, data)


if __name__ == "__main__":
    # Test code for clients and connection_agent.py
    of_num = 1 # create outfile numbers, inc below.
    parser = argparse.ArgumentParser(description='test the connector')
    args = parser.parse_args()
    connector = Connector(port=3773)
    onum = 0
    while True:
        print('P/G<enter>', end='', flush=True)
        inline = sys.stdin.readline().strip()
        if 'p' == inline[0] or 'P' == inline[0]:
            opath = 'outfile_%d.txt' % onum
            onum += 1
            print('post:', opath)
            res = connector.post(opath)
            print('P:', res)
        if 'g' == inline[0] or 'G' == inline[0]:
            res = connector.get()
            print('G:', res)
        if 'q' == inline[0] or 'Q' == inline[0]:
            break

