#!/usr/bin/env python3
#

""" gflFilter.py
    Takes three files, a file with a list of filters, a source file lines
    to be filtered, and a target file to write filtered output.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""


__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import os
import os.path
import shutil
import sys

import args
import filefilter


usage = """
gflFilter filter-file to-filter-list-file output-file

Takes a list of filters for a list and output the result to the output-file.
The filters are one line entries (i.e. seperated by <NL> in the filter-file.
The things to filter are the same, single lines in the to-filter-list.
If any of line of the filter file appear in a line of the to-filter-list 
the to-filter line is removed (not writen) to the output-file.
"""

def line_filter(filters, line):
    """ the line filter checks the list of all filters against the line.
    Args:
        filters: a list of strings that to test for.
        line: the object being accepted or rejected.
    Returns:
        line if it is accepted an empty string otherwise
    """
    for filter in filters:
        #print "line:%s filter:%s in:%s" % (line, filter, filter in line)
        if filter in line:
            return False
    return True

def accept_filters(fval, n):
    """ filter filter-lines
    Args:
        fval: a input line from the filter file (.gflfilter)
        n: the linke number, which is ignored
    Returns:
        if line is commented returns None
        ottherwise returns line with whitespace remvoed from EOL.
    """
    if fval[0] == '#':
        return None
    return fval.rstring()


if __name__ == "__main__":
    args = args.Args(usage)

    if args.len() != 3:
        print(usage)
        sys.exit(1)

    # get the values
    #filters = filefilter.ReadFile(args.get_value(), xform=lambda fval, n : fval.rstrip())
    filters = filefilter.ReadFile(args.get_value(), accept_filters)
    filtered = filefilter.ReadFile(args.get_value(),
                                   interest = lambda line: line_filter(filters, line))

    wf = open(args.get_value(), 'wc');
    for out_line in filtered:
        wf.write(out_line)
    

