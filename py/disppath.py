#!/usr/bin/env python3
#

""" disppath.py -- OBSOLETE description
    by default does base name of single arguement.
    When there is more than one argument it looks for them in the last
    arg in the order upto the last arg, and returns the first
    found:
        Found...basename, where the number of dots are the directory
        levels between found and basename
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import os
import sys

def process_name():
    parser = argparse.ArgumentParser(description='manipulate the base name')
    parser.add_argument('--home',
                        action='store_true',
                        help='exclude home dir')
    parser.add_argument('--dev',
                        action='store_true',
                        help='exclude dev dir')
    parser.add_argument('--debug',
                        action='store_true',
                        help=argparse.SUPPRESS)
    parser.add_argument('--levels', '-l',
                        type=int,
                        default=7,
                        help='levels to compress')
    parser.add_argument('bases',
                        type=str,
                        nargs='?',
                        action='append',
                        help='bases paths to remove, replace with ~[0]')
    args = parser.parse_args()
    pwd = os.getenv('PWD')
    dev = os.getenv('DEV_BASE')
    home = os.getenv('HOME')
    #print(args.bases, len(args.bases), file=sys.stderr)
    #print(pwd, dev, home, file=sys.stderr)
    for base in args.bases:
        if base is None:
            break;
        #print('B:', base, file=sys.stderr)
        pwd = pwd.replace(base, base[0].upper())
    if not args.dev:
        tail = os.path.basename(dev)
        if args.debug:
            print('T:', tail, dev)
            print('R:',  pwd.replace(dev, tail))
        pwd = pwd.replace(dev, tail)
    if not args.home:
        pwd = pwd.replace(home, 'H')
    print(pwd)

if __name__=='__main__':
    process_name()

