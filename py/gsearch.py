#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import argparse
import google
import google.generativeai as genai
import json
import os
import pathlib as pl
import sys


from googlesearch import search

def google_search(query):
  """Searches Google for the given query and prints the top results."""
  for result in search(query, num_results=100):  # Get the top 10 results
    print(result)

# Example usage
_query = "conferences for board members in California 2025"

if __name__ == '__main__':
    google_search(_query)

    sys.exit(0)


