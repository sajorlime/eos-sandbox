#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import argparse
import google
import google.generativeai as genai
import google.ai.generativelanguage as gaigl
#from google.ai.generativelanguage import GenerativeLanguageService
#from google.ai.generativelanguage import TextPrompt
import json
import os
import pathlib as pl
import sys

# Import the Gemini API client library (you'll need to install it first)
#  pip install google-ai-generativelanguage

api_key = os.environ['APIKEY']
#genai.configure(api_key=api_key)


# Replace with your actual API key

client = gaigl.GenerativeLanguageService(api_key=api_key)

def analyze_code(code_snippet, prompt_question):
  '''Analyzes a code snippet using the Gemini API.'''

  prompt = gaigi.TextPrompt(
      text=f'''
      {code_snippet}

      {prompt_question}
      '''
  )
  response = client.generate_text(prompt=prompt)
  return response.result

def process_directory(directory_path):
  '''Processes a single directory, analyzing code files within it.'''
  for filename in os.listdir(directory_path):
    if filename.endswith('.py'):  # Adjust file extension as needed
      filepath = os.path.join(directory_path, filename)
      with open(filepath, 'r') as f:
        code_content = f.read()

      # Example prompt - customize as needed
      prompt = 'Explain the purpose of this code and any key functions or classes.'
      response = analyze_code(code_content, prompt)
      print(f'Analysis for {filename}:\n{response}\n')

# Get the base directory path of your codebase
#base_directory = '/home/eo/src/zainar/df'

# Iterate through the directories
'''
for directory_name in os.listdir(base_directory):
  directory_path = os.path.join(base_directory, directory_name)
  if os.path.isdir(directory_path):
    print(f'Analyzing directory: {directory_name}')
    process_directory(directory_path)
'''

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='gaigl python file analyze')

