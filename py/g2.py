#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser()

# Create the group
group = parser.add_argument_group("groups", nargs="+")

# Add arguments to the group
group.add_argument("--group", choices=["A", "B", "C"])

# Parse the command line arguments
args = parser.parse_args()

# Print the values of the --group options
print(args.group)
