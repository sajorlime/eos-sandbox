#!/usr/bin/env python3
# utils/medfilt.py 
# 06/07/2022
# median filter
'''
    implenets some simple numpy filtering and matplotlib.
'''

import csv
import numpy as np
import matplotlib.pyplot as plt
import os
import pathlib as pl
#import scipy as sci
import sys

#import utils.plot_utils as pu

def medfilt(x, k):
    """Apply a length-k median filter to a 1D array x.
        Boundaries are extended by repeating endpoints.
        This was cribed from the inter-webs
    Args:
        x: a one dimensional array
        k: the median filter width
    Returns:
        the median filtred result
    """
    assert k % 2 == 1, "Median filter length must be odd."
    assert x.ndim == 1, "Input must be one-dimensional."
    k2 = (k - 1) // 2 # floor division
    y = np.zeros((len (x), k), dtype=x.dtype)
    y[:,k2] = x
    for i in range(k2):
        j = k2 - i
        y[j:,i] = x[:-j]
        y[:j,i] = x[0]
        y[:-j,-(i+1)] = x[j:]
        y[-j:,-(i+1)] = x[-1]
    return np.median(y, axis=1)

def ccsv2np(csvpath):
    ''' CSV to numpy 2-d array
    Args:
        csvpath: pl.Path for CSV file, can have comments
    Returns:
        (col_names, numeric data array, list of extractions)
        col_names is the first non-comment row.
        data array is the non-comment data, col_names width
    '''
    def comment_skip(csv_data):
        for csv_line in csv_data:
            if csv_line.startswith('#'):
                '''
                #print(f'IN:{csv_line=}')
                m = exre.match(csv_line)
                if m:
                    print(f'{m.group()=}')
                    vals = m.group(0).split(':')
                    print(f'{vals=}')
                    extractions[vals[1]] = [vals[2], eval(vals[3])]
                '''
                continue
            if csv_line.startswith('\n'):
                continue
            #print(f'OUT:{csv_line=}')
            yield csv_line

    with csvpath.open('r') as csvf:
        reader = csv.reader(comment_skip(csvf), delimiter=',')
        col_names = next(reader)
        ldata = list(reader)
        data = np.array(ldata).astype(float)
    #print(f'{col_names=}, {data.size=}, {extractions=}')
    return data


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='test median filter')
    parser.add_argument('csv',
                        type=pl.Path,
                        help='csv file path')
    parser.add_argument('--med', '-m',
                        type=int,
                        default=15,
                        help='width of median filter')
    parser.add_argument('--col', '-c',
                        type=int,
                        default=0,
                        help='col to plot against row')
    args = parser.parse_args()

    #print('FP:', args.csv)
    if not os.path.exists(args.csv):
        print('fp does not exist:', args.csv)
        sys.exit(1)
        
    csv_data = ccsv2np(args.csv)
    yvals = csv_data[:,args.col]
    if args.col == 0:
        y0 = yvals[0]
        yvals = yvals - y0
    xvals = np.array(range(len(yvals)))
    avg_vals = np.full(yvals.shape, np.mean(yvals))
    ymedian = medfilt(yvals, args.med)
    # smooth envals
    plt.plot(xvals, yvals, 'red')
    plt.plot(xvals, ymedian, 'blue')
    plt.plot(xvals, avg_vals, 'green')
    plt.show()

