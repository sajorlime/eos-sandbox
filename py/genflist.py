#!/usr/bin/env python3
#
#

"""
  genflist.py creates a filtered tree with .ext and writes
  it to the file .ext-files
  Filtering excludes anything in the local or global .gflfiler
  file.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import os
import os.path
import re
import sys


import filefilter as ff
import regexfilter
import tree

def dirsort(a, b):
    """
    a sort for creating a directory order

    Args:
        a: left
        b: right
    """
    #import pdb ; pdb.set_trace()
    al = a.split('/')
    ia = len(al)
    bl = b.split('/')
    ib = len(bl)
    bi = iter(bl)
    for apart in al:
        bpart = next(bi)
        if not bpart:
            return 1
        if apart < bpart:
            return -1
        if apart > bpart:
            return 1
    if ia != ib:
        return ia - ib
    return 0

def cmp_to_key(mycmp):
    """
    Creates a comparison object that supports <, >, ==
    Args:
        mycmp: a method the returns < 0, > 0, or == 0
            see dirsort above
    """
    class DirSort(object):
        def __init__(self, obj, *args):
            self._obj = obj
        def __lt__(self, other):
            return mycmp(self._obj, other._obj) < 0
        def __gt__(self, other):
            return mycmp(self._obj, other._obj) > 0
        def __eq__(self, other):
            return mycmp(self._obj, other._obj) == 0
        def __le__(self, other):
            return mycmp(self._obj, other._obj) <- 0
        def __ge__(self, other):
            return mycmp(self._obj, other._obj) >- 0
        def __ne__(self, other):
            return mycmp(self._obj, other._obj) != 0
    return DirSort

class Pair:
    """
    Defines a pair of of items
    """
    def __init__(self, pair_str):
        self._paira = pair_str.split(',')

    def __str__(self):
        return '(%s,%s)' % (self._paira[0], self._paira[1])

    def key(self):
        return self._paira[0]

    def value(self):
        return self._paira[1]


def _xform(fval:str, n: int) -> str:
    return fval.rstrip()

def _interest(l: str) -> bool:
    return l[0] != '#'

def add_filters(gfl: str, filters: list) -> list:
    filters += ff.ReadFile(gfl, xform=_xform, interest=_interest)
    return filters

root_gfl = os.path.join(os.getenv('HOME'), '.gflfilter')
_epilog=f'''
see tree.py --help for example filters
by default uses local .gflfiler and {root_gfl}
'''

def genflist():
    ''' generate a file list based on extentions
    TODO(epr): this is way too slow for deep directory structures.
        One thing to be explored would be to filter early on trees.
    '''
    parser = argparse.ArgumentParser(description='generate a file list called '
                                                 '.<ext>-files for each '
                                                 'extension passed',
                                     epilog=_epilog)
    parser.add_argument('extensions',
                        metavar='EXT',
                        nargs='+',
                        help='list of extensions for which to create file '
                              'lists use all for getting all files '
                              'in the tree')
    parser.add_argument('--pair',
                        dest='pairs',
                        action='append',
                        default=['h,\.h$|\.hpp$',
                                 'c,\.c$|\.cc$|\.cpp$|\.c\+\+$|\.cxx$,',
                                 's,\.S$,',
                                 'mk,Makefile',
                                 'conf,\.conf',
                                 'inc,\.inc',
                                 'bb,\.bb|\.bbapend|\.bbclass',
                                 'dts,\.dts|\.dtsi',
                                 'all,any'], # what do these mean?
                        help='equivilance pairs, %(default)s')
    parser.add_argument('--gfl',
                        type=str,
                        default='.gflfilter',
                        help=f'location of list of file filters, '
                             f'if does not exist uses '
                             f'{root_gfl}')
    parser.add_argument('-root',
                        action='store_true',
                        help=f'exclude .gflfilter in '
                             f'{root_gfl} directory')
    parser.add_argument('--ofile',
                        type=str,
                        default=None,
                        help='override output file')
    parser.add_argument('--filter', '-f', 
                        dest='filters',
                        action='append',
                        type=regexfilter.ArgRegexFilter,
                        default=[],
                        help='add filter using tree.py syntax')
    args = parser.parse_args()

    gfl = args.gfl # defaults to local directory
    gflfilters = []
    if not os.path.exists(gfl):
        gfl = os.path.join(os.getenv('HOME'), '.gflfilter')
    if os.path.exists(gfl):
        add_filters(gfl, gflfilters)
    #print(f'0{gflfilters}')
    if not args.root:
        add_filters(root_gfl, gflfilters)
    #print(f'1{gflfilters}')
    if not gflfilters:
        print('genflist: warning no filter file', file=sys.stderr)
    filters = args.filters

    for f in gflfilters:
        if not f:
            continue
        if f[0] == '+':
            filters.append(regexfilter.RegexFilter(True, f'{f[1:]}'))
        else:
            filters.append(regexfilter.RegexFilter(False, '%s' % f))

    pairs = {}
    for spair in args.pairs:
        pair = Pair(spair)
        #print(f'{pair=}')
        pairs[pair.key()] = pair.value()
        #print(f'{pairs=}')

    def process_pair(ext, pat, ofile=None):
        #print(f'PP: {pat=}')
        filters.append(regexfilter.RegexFilter(True, pat))
        #print(f'{filters=}')

        # TODO(epr): py wierdness here, if don't pass empty list
        # next call has old list.
        tih = tree.TreeItemHandler('.', filters, [])
        tree.tree_files('.', tih)
        if not ofile:
            wf = open('.%s-files' % ext, 'w+');
        else:
            wf = open(ofile, 'w+');
        #print(f'{list(tih.items())=}')
        files = sorted(list(tih.items()), key=cmp_to_key(dirsort))

        for file in files:
            print(file, file=wf)
        wf.close()
        return

    if args.extensions == ['all']:
        process_pair('all', '.')
        return 0

    for ext in args.extensions:
        pat = ext
        if ext in list(pairs.keys()):
            pat = pairs[ext]
        else:
            pat = '\.%s$' % ext
        process_pair(ext, pat, args.ofile)
        filters.pop()
    return 0

if __name__=='__main__':
    genflist()

