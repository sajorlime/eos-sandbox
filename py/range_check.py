#!/usr/bin/env python
import argparse
# pip3 install argparse_range
from argparse_range import range_action as ract


parser = argparse.ArgumentParser('Range Test')
parser.add_argument('-r',
                    action=ract(5.5, 17.3),
                    type=float,
                    help='R help')

try:
    args = parser.parse_args()
except Exception as re:
    print(f'{re=}')
print(f'{args=}')
