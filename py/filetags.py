#!/usr/bin/env python3
#

""" filetags.py
    Takes a file which should be a list of file paths.
    It extracts the basename of the file and creates
    a tag entry for the name, and its lower case equivalent
    and a tag for the name without an extension.

    E.g. we get /foo/bar/file.cpp
    we return file^I/foo/bar/file.cpp^I1
    if we get /foo/bar/File.cpp
    we return file^I/foo/bar/File.cpp^I1
    and we return file^I/foo/bar/file.cpp^I1

"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import os
import os.path
import shutil
import sys

import filefilter


usage = """
filetags file-list-file

Takes a file which should be a list of file paths.
It extracts the basename of the file and creates
a tag entry for the name, and it lower case equivalent
and tag for the name without an extension.

E.g. we get /foo/bar/file.cpp
we return file^I/foo/bar/file.cpp^I1
if .g. we get /foo/bar/File.cpp
we return file^I/foo/bar/File.cpp^I1
and we return file^I/foo/bar/file.cpp^I1

It writes the result to stdout
"""

def line_filter(filters, line):
    """ the line filter checks the list of all filters against the line.
    Args:
        filters: a list of strings to test for.
        line: the object being accepted or rejected.
    Returns:
        line if it is accepted an empty string otherwise
    """
    for filter in filters:
        #print "line:%s filter:%s in:%s" % (line, filter, filter in line)
        if filter in line:
            return False

    return True


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=usage)
    parser.add_argument('flfile',
                        type=str,
                        nargs='+',
                        help='file that contains a list of files, see genflist.py')
    parser.add_argument('--ofile', '-o',
                        type=str,
                        default='-',
                        help='output file, %(default)s')
    parser.add_argument('--use_base',
                        action='store_true',
                        help='include base name, e.g. "file.c" also get "file"')
    args = parser.parse_args()

    # get the values
    flist = filefilter.ReadFile(args.flfile[0], xform = lambda fval, n : fval.rstrip())

    for fpath in flist:
        pos = fpath.rfind('/');
        fp_lower = None
        if pos >= 0:
            fp_identity = fpath[pos + 1:]
        else:
            fp_identity = fpath

        print("%s\t%s\t1" % (fp_identity, fpath))

        # get a version w/o extension
        if args.use_base:
            fp_identity_no_ext = None
            pos = fp_identity.rfind('.')
            if pos > 0:
                fp_identity_no_ext = fp_identity[0:pos]

            if fp_identity_no_ext:
                print("%s\t%s\t1" % (fp_identity_no_ext, fpath))
            else:
                print("%s\t%s\t1" % (fp_identity_no_ext, fpath))
                continue

            fp_lower = fp_identity_no_ext.lower()
            if fp_lower != fp_identity_no_ext:
                print("%s\t%s\t1" % (fp_lower, fpath))
    

