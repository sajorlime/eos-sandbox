#!/usr/bin/env python3
#

""" ahist.py
    Takes an user history file and does things.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""




__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import os
import os.path
import shutil
import sys

import args
import filefilter


usage = """
ahist [+/-v] [-o=outputfile] hist-file

"""


euv_any = -1
euv_artist = 0
euv_album = 1
euv_genre = 2
euv_title = 3
euv_releaseDate = 4
euv_comment = 5
euv_state = 6
euv_date_time_ref = 7
euv_date_time_event = 8
Last_UserVector = 9

def hist_filter(line):
    """ the line filter takes any line that does not start with white
        space for #.
    Args:
        line: the object being accepted or rejected.
    Returns:
        true if the lines first charactar is 0-9, A-Z, or a-z.
    """
    return line[0].isalnum()

def hist_xform(line, num):
    return line.strip().split('|');


if __name__ == "__main__":
    args = args.Args(usage)
    opath = args.get_arg('o', '-');
    validate = args.get_arg('v');

    if opath == '-':
        ofile = sys.stdout
    else:
        ofile = open(opath, 'w')

    hist_file = args.get_value()

    hist_list = filefilter.ReadFile(hist_file,
                                    interest=hist_filter,
                                    xform=hist_xform)
    for hist_item in hist_list:
        if validate:
            if len(hist_item) != Last_UserVector:
                print('BAD H-ITEM: %s' % hist_item)
                continue
            event_file = hist_item[euv_date_time_event] + '.json'
            event_path = os.path.join('history', event_file)
            if not os.path.exists(event_path):
                ref_file = hist_item[euv_date_time_ref] + '.json'
                ref_path = os.path.join('history', ref_file)
                if not os.path.exists(ref_path):
                    print('BAD H-PATH: %s => %s' % (event_file, hist_item))
                else:
                    print('HASREF: (r:%s, e:%s) => %s' % (ref_file, event_file, hist_item))
                continue
            else:
                print('GOOD H-PATH: %s => %s' % (event_file, hist_item))
                continue
        else:
            print(hist_item)
    

