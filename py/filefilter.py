#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

""" filefilter.py
    provides functions that filter a file and deliver it as a list 
    of objects.  The type is determined by the caller of ReadFile.
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import codecs

import sys

import regexfilter

def _exclude(tstr, exclude):
    """ Local function to exclude lines from the list.
        A string is excluded when an item in the exclusion
        list appears in the string.
    Args:
        tstr: the string value which is being tested.
        exclude: a list of exclusion values
    Returns:
        True (exluded it) if the exclusion is found in the string.
    """
    for exclusion in exclude:
        if tstr.find(exclusion) > 0:
            return True
    return False

def _true_interest(line):
    """ default function that indicates if there is interest
        in a particular string.
        This function is overridden by the caller of ReadFile
        when the user wants to indicated that she is interested
        in a subset of the lines in a file.
    Args:
        line: the string of interest.
    Returns:
        True
    """
    return True


def _identity_xform(line, line_number):
    """ the identity transfor is the default transformation function for reading
        lines from a file.  It just returns the input line.
    Args:
        line: the line to be transformed.
        line_number: the line number form the file to be transformed.
    Returns
        line as passed, i.e. it is a identity transform 
    """
    return line


def _interest(interest, line, echo):
    """ local interest fucntion that enables printing
    Args:
        interest: function object that takes one 
        echo: true to echo the string
    """
    if echo:
        print(line)
    return interest(line)


def ReadFile(obj_file_path, xform=_identity_xform,
             interest=_true_interest,
             echo_line=False,
             echo_number=False,
             strip=None,
             file_encoding='utf-8'):
    """ read a file and return a list of object provided by interest and xform
    Args:
        obj_file_path: file path to a list of lines to turn into a list,
            possibly with transform and filters.
        xform: is a function object that takes an input line and
            returns an object interesting to the caller.  The default value
            simply returns the input value.
        interest: is a function object returns a boolean value to indicate
            that this line of of interest.  I.e. it allows the caller to ignore lines.
            The default value always returns True.
        echo_line: prints the line if true
        echo_number: count line numbers
        strip: strips characters in string
    Returns:
        a list of objects as returned by xform, by default a list of strings read
        from the file.
    """
    if not obj_file_path or obj_file_path == '-':
        obj_file = sys.stdin
    else:
        obj_file = codecs.open(obj_file_path, encoding=file_encoding, mode='r')
    nline = 1
    if echo_line:
        if echo_number:
            def el_func(x):
                # echo line
                ostr = '%d\t%s' % (nline, x)
                sys.stdout.buffer.write(ostr.encode('utf-8'))
        else:
            def el_func(x):
                try:
                    print(x, end='')
                except UnicodeEncodeError as ex:
                    print("UnicodeEncodeError  from file:", obj_file_path, nline, file=sys.stderr)
                    print(ex, file=sys.stderr)
                except UnicodeDecodeError as ude:
                    print("UnicodeDecodeError  from file:", obj_file_path, nline, file=sys.stderr)
                    print(ude, file=sys.stderr)
    else:
        def el_func(x):
            pass

    obj_data = []
    # TODO(epr): make this a list comprehension
    try:
        #print('OF:', obj_file)
        for line in obj_file:
            if strip:
                line = line.strip(strip)
            if interest(line):
                obj_data.append(xform(line, nline))
                #print(len(obj_data))
                el_func(line)
            nline = nline + 1
    except UnicodeDecodeError as ude:
        print("UnicodeDecodeError  from file:", obj_file_path, nline, file=sys.stderr)
        print(nline)
        print(line)
        print(ude)
        raise ude
            

    obj_file.close()
    #print(len(obj_data), type(obj_data))
    return obj_data

# test case
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='filefilter (test only) filter-val filtered-file ...')
    parser.add_argument('testfile',
                        type=str,
                        help='file to filter')
    parser.add_argument('-filter', '-f',
                        type=str,
                        dest='filters',
                        action='append',
                        required=True,
                        help='filter file with thesse strings, like grep, see regexfilter.py -h')
    #parser.add_argument('--not',
    #                    dest='notin',
    #                    action='store_true',
    #                    help='anti filter')
    args = parser.parse_args()
    flist = []
    for f in args.filters:
        flist.append(regexfilter.SRegexFilter(f))
    print(flist)

    def interest(line):
        return regexfilter.ftest(line, flist)

    thelist = ReadFile(args.testfile,
                       interest=interest,
                       echo_line=True,
                       echo_number=True)
    print('lines:', len(thelist))


