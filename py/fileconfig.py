#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

""" Retrieve configuration from a FileSource
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import json
import os.path
import sys

from utils.filesource import FileSource

# TODO(epr): create root class for configs

class FileConfig(object):
    """
    Encapsulate a configuration
    """
    def __init__(self, config_path, config_type, config_name=None):
        """
        Args:
            iso_lines: a list of lines taken from a IsoData or .txt configuration
            config_type: str one of the configration types defined by IsoLynx
            config_name: str whatever
        """
        self._config_name = config_name
        self._source = FileSource(config_path)
        root = sys.path[0]
        jroot =  os.path.join(root, 'json')
        self._config_type = config_type
        json_fh = open(os.path.join(jroot, '%s.json' % config_type), encoding='utf-8', mode='r')
        self._jdata = json.loads(json_fh.read())

    def config_type(self):
        return self._config_type

    def config_name(self):
        return self._config_name

    #def config_fh(self):
    #    return self._config_fh
    def source(self):
        return self._source

    def jdata(self):
        return self._jdata

    def readline(self):
        self._config_fh.readline()

    def __str__(self):
        return '<cname: %s, src: %s, ctype: %s, jdata:%s>' % (self._config_name,
                                                              self._source,
                                                              self._config_type,
                                                              self._jdata)

