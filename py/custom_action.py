#!/usr/bin/env python
import argparse
# possible ref
# https://stackoverflow.com/questions/33301000/custom-parsing-function-for-any-number-of-arguments-in-python-argparse

class CustomAction(argparse.Action):
    def __init__(self,option_strings,
                 additional_arg1,additional_arg2,
                 dest=None,
                 nargs=0,
                 default=None,
                 required=False,
                 type=None,
                 metavar=None,
                 help=None):
        self._a1=additional_arg1
        self._a2=additional_arg2
        super(CustomAction, self).__init__(
            option_strings=option_strings,
            dest=dest,
            nargs=nargs,
            default=default,
            required=required,
            metavar=metavar,
            type=type,
            help=help)
    def __call__(self, parser, namespace, values, option_string=None):
        print(self._a1)
        print(self._a2)
        setattr(namespace, self.dest, values)

#........
parser = argparse.ArgumentParser('xxx')
parser.add_argument('-e', '--example', action=CustomAction, additional_arg1='--your_arg', additional_arg2=42)
args = parser.parse_args()
print(f'{args=}')
