#!/usr/bin/env python3
#
""" adjust_path
    adjusts a path with environment variable info
    e.g. DEV_SRC = /d/dev-x/foo/_src, and path is
                  /d/dev-x/foo/_src/asr_action
    returns       ${DEV_SRC}/asr_action

    usage: adjust_path target_path ENV_VAR [ENV_VAR1, ENV_VAR2, ...]

    returns value for first environment value that is a substring of
        target_path, if no environment value is a substring of
        target_path then target_path is returned
        
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""





import os
import os.path
import shutil
import sys

import args


usage = """
adjust_path [-f=file]
defaults to stderr and stdout

write the stdin to stdout and stderr, when file is not specified
"""

def adjust_path(path, *env):
    for envar in env:
        env_value = os.getenv(envar)
        if env_value:
            env_token = '${%s}' % envar
            #print envar, env_value, env_token
            if env_token != env_value:
                path = path.replace(env_value, env_token, 1)
    return path


def ap_main():
    myargs = args.Args(usage)
    target_path = myargs.get_value()
    print(adjust_path(target_path, *myargs.get_values()))
    #for envar in args.get_values():
    #    env_value = os.getenv(envar)
    #    if not env_value:
    #        continue
    #    env_pos = target_path.find(env_value)
    #    if env_pos == 0:
    #        print '${%s}%s' % (envar, target_path[len(env_value):])
    #        return 0
    # we didn't find it.
    # print target_path
    return 0

if __name__ == "__main__":
    ap_main()

