#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

""" filtertags.py
    provides functions that filter a file and deliver it as a list 
    of objects.  The type is determined by the caller of ReadFile.
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import codecs

import sys

import filefilter
import regexfilter

_epi = """
"""

# test case
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(
                description='filtertags filter-val filtered-file ...',
                epilog=_epi)
    parser.add_argument('tfile',
                        type=str,
                        help='tag to filter')
    parser.add_argument('--ofile', "-o",
                        type=str,
                        default="-",
                        help='output file, %(default)s')
    parser.add_argument('--filter',
                        type=str,
                        action='append',
                        default=['-\^struct [A-z].+;\$',
                                 '-\^class [A-z].+;\$',
                                 '-\# +define +[A-z].+\$',
                                 '-\# +define +__declspec',
                                ],
                        help='filter file with this string, %(default)s, '
                             'see regexfilter.py -h')
    args = parser.parse_args()
    flist = []
    for f in args.filter:
        flist.append(regexfilter.SRegexFilter(f))
    def interest(line):
        return regexfilter.ptest(line, flist)
 
    #print('FTf:', args.tfile)
    olines = filefilter.ReadFile(args.tfile, interest=interest, echo_line=False)
    if args.ofile == '-':
        print("what?")
        ofile = sys.stdout
    else:
        ofile = codecs.open(args.ofile, encoding='utf-8', mode='w')
    for line in olines:
        print(line, file=ofile, end='')


