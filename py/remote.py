#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import os
import subprocess
import sys

HOST=os.getenv('ZH_02_TEST_MACHINE')
# Ports are handled in ~/.ssh/config since we use OpenSSH
COMMAND="ls -a"

ssh = subprocess.Popen(["ssh", "%s" % HOST, COMMAND],
                       shell=False,
                       stdout=subprocess.PIPE,
                       stderr=subprocess.PIPE)
result = ssh.stdout.readlines()
if result == []:
    error = ssh.stderr.readlines()
    print("ERROR: %s" % error)
else:
    print(result)
