#!/usr/bin/env python3
"""
test logging, with stream
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""
import argparse
import logging
import sys

class Tee(object):
    ''' define a class that overrides stdout,
        allowing write to stdout and file.
        Usage:
            pass a list of file to constructor
            and assign instance to sys.stdout.
    '''
    def __init__(self, *files):
        '''
        Args:
            list of files to be writen together
        '''
        self.files = files
    def write(self, obj):
        ''' write to all files '''
        for f in self.files:
            f.write(obj)
            f.flush()  # output to be visible immediately
    def flush(self):
        ''' flush all files '''
        for f in self.files:
            f.flush()



if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='tlog')
    parser.add_argument('--log', '-l',
                        default='tlog.log',
                        help='log file name')
    parser.add_argument('--level', '-ll',
                        default=logging.INFO,
                        help='log level')
    args = parser.parse_args()

    _sys_stdout = sys.stdout # remember sys.stdout
    _log_fd = open(args.log, 'w')
    sys.stdout = Tee(sys.stdout, _log_fd)

    logging.basicConfig(level=logging.INFO, stream=sys.stdout)

    print('ABC')
    logging.info('DEF')
    print('HIJ')
    logging.warning('KLM')
    print('NOP')
    logging.error('QRS')
