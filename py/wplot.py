#!/usr/bin/python3
#

""" wplot.py
    plot wordly 
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import os
import os.path
import random
import re
import sys



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='play workle, or solve today\'s puzzle')
    parser.add_argument('--find', '-f', 
                        action='store_true',
                        help = 'Play a puzzle, if not create one')
    parser.add_argument('--words', '-w', 
                        dest='words',
                        type=str,
                        default=os.getenv('WORDS'),
                        help = 'override the $WORDS enviroment variable, default: %(default)s')
    myargs = parser.parse_args()
    def ihelp():
        print('?: gives this message')
        print('-X says that the X is not in workd')
        print('-Xn says that the X is not in position n')
        print('+Xn says that the X is in position n')
        print('where X in (a, z), and')
        print( '      n in (0, 4)')
        print('N: new word')
        print('R: random word from current list')
        print('q: quits')
        return None

    allwords = open(myargs.words).readlines()
    words = [ w.strip() for w in allwords if w[0] != w[0].upper()]
    refwords = [ w for w in words if len(w) == 5 and "'" not in w ]
    inwords = refwords
    awords = [ w for w in inwords if 'a' in w ]
    ewords = [ w for w in inwords if 'e' in w ]
    iwords = [ w for w in inwords if 'i' in w ]
    owords = [ w for w in inwords if 'o' in w ]
    uwords = [ w for w in inwords if 'o' in w ]
    aewords = [ w for w in awords if 'e' in w ]
    eiwords = [ w for w in ewords if 'i' in w ]
    iuwords = [ w for w in iwords if 'u' in w ]
    aeiwords = [ w for w in eiwords if 'a' in w ]
    print(len(aewords), len(eiwords), len(iuwords))
    print(len(aeiwords))
    inpicks = ''
    target = ''
    ihelp()
    while True:
        print('?[-Xn]|+Xn]NQq', inpicks, len(inwords), target)
        inbuf = sys.stdin.readline().strip()
        if '?' in inbuf:
            ihelp()
            continue
        if '-' == inbuf[0]:
           exletter = inbuf[1].lower()
           if len(inbuf) == 2:
               inwords = [ w for w in inwords if exletter not in w ]
               print(exletter, len(inwords))
           else:
               expos = int(inbuf[2])
               print(expos)
               inwords = [ w for w in inwords if exletter != w[expos] ]
               inwords = [ w for w in inwords if exletter in w ]
               print(exletter, len(inwords))
           inpicks = '%s|%s' % (inpicks, inbuf)
        elif '+' == inbuf[0]:
           inletter = inbuf[1].lower()
           if len(inbuf) == 2:
               inwords = [ w for w in inwords if inletter in w ]
           else:
               inpos = int(inbuf[2])
               inwords = [ w for w in inwords if inletter == w[inpos].lower() ]
           inpicks = '%s|%s' % (inpicks, inbuf)
        elif 'N' == inbuf[0]:
            inwords = refwords
            inpicks = ''
        elif 'R' == inbuf[0]:
            wi = int(random.random() * len(inwords))
            print('Try:', inwords[wi])
            continue
        elif 'S' == inbuf[0]:
            print(aeiwords)
            wi = int(random.random() * len(aeiwords))
            print('Try:', aeiwords[wi])
            continue
        elif 'W' == inbuf[0]:
            target = inbuf[1:6]
            continue
        elif 'q' == inbuf[0]:
            exit(0)
        else:
            ihelp()
            continue
            sys.exit(0)
        inlen =  len(inwords)
        print('Len inwords:', inlen)
        if inlen < 10:
            for w in inwords:
                print(w)

        


