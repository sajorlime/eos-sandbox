#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#


import argparse
import google
import google.generativeai as genai
import os
import pathlib as pl
import re

import ast
import os

def analyze_imports(file_path):
    """
    Analyzes a Python file and extracts import information.

    Args:
        file_path (str): The path to the Python file.

    Returns:
        list: A list of imported modules.
    """
    with open(file_path, "r") as f:
        tree = ast.parse(f.read())

    imports = []
    for node in ast.walk(tree):
        if isinstance(node, (ast.Import, ast.ImportFrom)):
            for alias in node.names:
                if isinstance(node, ast.Import):
                    imports.append(alias.name)
                else:
                    imports.append(f"{node.module}.{alias.name}")
    return imports

def analyze_directory(directory):
    """
    Analyzes all Python files in a directory and its subdirectories.

    Args:
        directory (str): The path to the directory.

    Returns:
        dict: A dictionary mapping file paths to lists of imported modules.
    """
    import_map = {}
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith(".py"):
                file_path = os.path.join(root, file)
                import_map[file_path] = analyze_imports(file_path)
    return import_map

# Example usage
directory = "/path/to/your/directory"  # Replace with your directory
import_map = analyze_directory(directory)

# Print the results
for file_path, imports in import_map.items():
    print(f"File: {file_path}")
    for module in imports:
        print(f"  - Imports: {module}")


