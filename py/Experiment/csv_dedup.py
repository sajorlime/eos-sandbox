#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#

""" csv_to_json.py
    A tool for creating a JSON representation of the a  CSV file.
    File must have a meaningful header line.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import sys
import argparse
import json

import csv_reader as cr


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='write CSV data as a list '
                                                  'of JSON data rooted in '
                                                  'some key')
    cr.reader_options(parser)

    parser.add_argument('--jkey', '-k',
                        help='when json output selected use key '
                             'defaults to column zero header '
                             'allows number or string')
    args = parser.parse_args()

    ofh = cr.write_fh(args)

    csv_cols, csv_reader = cr.csv_open_with_args(args)

    if args.jkey:
        jkey = args.jkey
    else:
        jkey = csv_cols[0]

    lines_dict = {}
    jrow = 0
    for csv_line in csv_reader:
        # JSON output
        # first create a jictionary indexed by rows
        # the row info embedded
        # and the column headers as dict keys in each row.
        lout = {'jndex' : 0}
        lines_dict[jrow] = lout
        col = 0
        for index, cname in enumerate(csv_cols):
            lout[csv_cols[index]] = csv_line[col]
            col += 1
        jrow += 1
        lout['jndex'] = jrow
        dout = {}
        # now, take the lines dict and create a dict indexed by jkeys, col
        for rk in lines_dict:
            row_name = lines_dict[rk][jkey]
            dout[row_name] = lines_dict[rk]
            #lout.append(lines_dict[rk])
        print(json.dumps(dout, indent=2), file=ofh)


