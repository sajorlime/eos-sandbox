#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#

""" test_match.py
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import traceback
import codecs
import csv
import itertools
import json
import math
import operator
import re
import sys


def test_match(csv_cols:list, req_cols:list) -> dict:
    '''
    Takes the csv column names and a list of desired colums to create
        a list of indicies.
    Args:
        csv_cols: list of column names
        req_cols: list of names, we want to find
            a req_col be preceeded by, +, -, or ~, which means
            e.g., +n1, -n2, ~n3.
            + implies that the name starts with n1, - implies ends with n2,
            and ~ implies that n3 is anywhere in the name
    Returns:
        A dictionary with a two-way map of name:col, col:name
        raises a ValueError if a req_cols value is not in csv_cols.
    '''
    col_map = {}
    for rc in req_cols:
        #print(f'RC: {rc}')
        match rc[0]:
            case '+':
                regx = f'^{rc[1:]}'
            case ',':
                regx = f'.*{rc[1:]}'
                regx = regx + '$'
                print(f', {regx}')
            case '~':
                regx = f'.*{rc[1:]}.*'
                print(f'~ {regx}')
            case _:
                regx = '^{}$'.format(rc)
                print(f'_ {regx}')
        #print(f'RX: {regx}')
        for cn, col in enumerate(csv_cols):
            rmatch = re.match(regx, col)
            if rmatch is None:
                continue
            rm = rmatch.string
            col_map[col] = cn
            col_map[cn] = col
    return col_map

cols = 'Secs,meas-volts,meas-amps,meas-watts,meas-S(VA),meas-Q(VARS),meas-Hz,inv-volts,inv-amps,inv-watts,inv-VA(S),inv-VARS(Q),inv-Hz,s/n: 512312000004'
clist = cols.split(',')


if __name__ == "__main__":
    #print(sys.version_info)
    parser = argparse.ArgumentParser(description='test match ')
    parser.add_argument('--col', '-c',
                        dest='columns',
                        type=str,
                        action='append',
                        help='CSV columns to accept, either names or indexes. '
                             '+col matchings all columns '
                                'that begin with "col". '
                             ',col  matchings all columns '
                                'that ned with "col".')
    args = parser.parse_args()
    print(f'0: {args.columns=}')

    cmap = test_match(clist, args.columns)
    print(cmap)

