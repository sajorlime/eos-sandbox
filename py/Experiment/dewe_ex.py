#!/usr/bin/env python3
# dewetron.py
# 10/04/2022
# define dewetron meter.
# Encapsulates access to the Dewetron multi-meter.
'''
Access to the meter and its operations is done in a private process.
Messages are sent the via Send to request actions.
The class _Dewetron is private to the module and there are module level
methods to request actions.
Provides a process that reads and stores meter data.
See SimpleMeter for programatic reading.
'''

import atexit
import enum
import multiprocessing as mp
import os
import posixpath as os_path # replase os.path with os_path
import socket
import sys
import telnetlib
import time
import traceback

_running = False
_lfd = None

class MItem(enum.IntEnum):
    ''' indices for Measurement Items
    '''
    T = 0
    V = 1
    I = 2
    W = 3
    S = 4
    Q = 5
    Hz = 6
    Items = 6

_transfer_channels = [ 'U', 'I', 'P', 'S', 'Q', 'F', ]
_transfer_rate = 100  # MAX: 10 kHz, for higher rates: use DATASTREAM
_transfer_duration = 0.1  # MIN: 1 sec

class MMC(object):
    ''' hold current and min/max values
    '''
    def __init__(self, sval):
        self._current = sval;
        self._min = sval
        self._max = sval
    @property
    def value(self):
        return (self._min, self._current, self._max)
    @property
    def current(self):
        return self._current
    @property
    def min(self):
        return self._min
    @property
    def max(self):
        return self._max
    def update(self, current):
        ''' update the current value and min/max.
            #Note(epr): would it be faster to use min and max functions?
        '''
        if current < self._min:
            self._min = current
        elif current > self._max:
            self._max = current
        self._current = current


class _Dewetron(object):
    ''' Encapsulate the Dewetron Meter
        Only called from the _dewe_proc
        Using py-visa to get instrument access.
    '''
    #def __init__(self, ipaddr, port, dque, rque, lfd):
    def __init__(self, ipaddr, port, dque, rque):
        self._ipaddr = ipaddr
        self._port = port
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 0)
        try:
            #print(f'connect to {ipaddr}:{port}', file=lfd)
            self._sock.connect((ipaddr, port))
        except Exception as toe:
            #print(f'_D:{toe =}, @ {ipaddr}:{port}', file=lfd)
            print(f'_D:{toe =}, @ {ipaddr}:{port}')
            raise toe
        print(f'{self._sock} connected', file=sys.stderr)
        self._dque = dque
        self._rque = rque
        self._dt = MMC(0.0)
        self._volts = MMC(120.0)
        self._amps = MMC(0.0)
        self._hz = MMC(0.0)
        self._watts = MMC(0.0)
        self._svolts = MMC(60.0)
        self._qvolts = MMC(60.0)
        self._csv_data = []
        self._t0 = 0.0
        self._outer_run = True
        self._inner_run = False
        self._tfetched = 0
        #self._lfd = lfd
        # make use we can talk w/o error, and set HEAD off, whateve?
        #self.error_query()
        self._send(':COMM:HEAD OFF')

    def sock_close(self):
        self._sock.close()
        print(f'closed? {self._sock}', file=sys.stderr)

    def data_init(self):
        ''' Setup the meter for elog data gathering, check that there
            are no errors.
        Returns:
           Returns elog-items as returned from meter
        '''
        schannel_list = '"'+'","'.join(_transfer_channels)+'"'
        self._send(f':ELOG:ITEMS {format(schannel_list)}')
        time.sleep(0.1)
        elog_items = self._query(':ELOG:ITEMS?')
        #print(f'DI:{elog_items=}', file=self._lfd)
        per_param = 1.0 / _transfer_rate
        #print('DI :ELOG:PERIOD {:f}'.format(per_param), file=self._lfd)
        self._send(':ELOG:PERIOD {:f}'.format(per_param))
        period = self._query(':ELOG:PERiod?')
        #print(f'DI: {period}', file=self._lfd)
        self._send(':ELOG:TIM REL')
        #time_stamp = self._query(':ELOG:TIMestamp?')
        #print(f'DI: {time_stamp}')
        #qeret = self.error_query()
        #if qeret:
        #    raise Exception(f'Dewe parameters error, {qeret})')
        return elog_items

    def read_elog(self):
        global _running
        #print(f'REL:send fetch', file=self._lfd)
        elog_data = self._query(':ELOG:FETCH?')
        if not elog_data or 'NONE' in elog_data:
            #print(f'REL-BAD-READ: {elog_list}', file=self._lfd)
            print(f'REL-BAD-READ: {elog_list}', file=sys.stdout)
            _running = False
            return []
        elog_list = elog_data.split(',')
        #print(f'REL: {elog_list}', file=self._lfd)
        eld = len(elog_list)
        num_ch = len(_transfer_channels) + 1
        num_items = int(eld / num_ch)
        eldata = [elog_list[i : i + num_ch] for i in range(0, num_ch * num_items, num_ch)]
        return eldata

    def save_results(self, dT):
        ''' get the current values from the meter and store them to csv_data.
        Args:
            dT: the delta time reference to use.
        Returns:
            None
        '''
        def show_val(mmc, pos, tstr):
            mmcs = f'{mmc.min:.1f} <= {mmc.current:.2f}{tstr} <= {mmc.max:.1f}'
            #print(pos, f'{mmcs}', file=self._lfd)
        fetched = self.read_elog()
        #print(f'save resutls: {dT=}, {fetched}', file=_lfd)
        if not fetched:
            print(f'SR: NONE {fetched}', file=sys.stderr)
            self._inner_run = self._outer_run = False
            return None
        dres = fetched[0]
        if 'NAN' in dres:
            fetched.pop(0)
            if not fetched:
                return None
            dres = fetched[0]
        if not self._csv_data:
            self._t0 = float(dres[0])
        self._dt.update(float(dT))
        self._volts.update(float(dres[MItem.V]))
        self._amps.update(float(dres[MItem.I]))
        self._watts.update(float(dres[MItem.W]))
        self._svolts.update(float(dres[MItem.S]))
        self._qvolts.update(float(dres[MItem.Q]))
        self._hz.update(float(dres[MItem.Hz]))
        show_val(self._dt, 1, 's')
        show_val(self._amps, 2, 'V')
        show_val(self._volts, 3, 'A')
        show_val(self._hz, 4, 'Hz')
        show_val(self._watts, 5, 'W')
        show_val(self._svolts, 6, 'S')
        show_val(self._qvolts, 7, 'Q')
        self._csv_data += fetched
        self._tfetched += len(fetched)
        return None

    def write_csv(self, filename):
        with open(filename, 'w', newline='') as ddfile:
            #TODO(epr): Print header based on data
            print('Secs,Volts,Amps,Watts,S,Q,Hz', file=ddfile)
            ldt = 0.0
            for litem in self._csv_data:
                try:
                    dt = float(litem[0]) - self._t0
                    if abs(dt) > 100:
                        print(f'#bad {dt=} @ {ldt=}', file=ddfile)
                        print(f'#bad {dt=} @ {ldt=}', file=sys.stderr)
                        break
                    ldt = dt
                    litem[0] = f'{dt}'
                except ValueError as ve:
                    print(f'#got: {ve} @ {litem[0]=}', file=ddfile)
                    print(f'#got: {ve} @ {litem[0]=}', file=sys.stderr)
                    continue
                slitem = ','.join(litem)
                print(slitem.strip(), file=ddfile)
        r = len(self._csv_data)
        self._csv_data = []
        return r

    def start_save(self):
        ''' sends ELOG info to meter and moves process to inner loop
        '''
        state = self._query(':ELOG:STATE?')
        if state == 'RUNNING':
            print('STATE IS RUNNING, bad!', file=sys.stderr)
        time.sleep(0.1)
        self._send(':ELOG:START')
        self._inner_run = True
        return True

    def stop_save(self):
        ''' stops the ELOG processed and terminates the inner loop
        '''
        global _transfer_duration
        self._inner_run = False
        # NOTE(epr): The actual stop needs to be in the process.
        # We need to wait for the process to wake, so 
        # transfer-duration +0.1 secs.
        #print('SS sleep for it')
        time.sleep(_transfer_duration + 0.1)
        return False

    def dewe_stop(self, check_inner=True):
        ''' stops the dewe-proc by setting outer and inner run to false.
        '''
        #print(f'DS: {self._outer_run=}, {self._inner_run=}')
        #print(f'DS: {self._outer_run=}, {self._inner_run=}', file=self._lfd)
        if _dm_que:
            _dm_que.put(None)
        if self._inner_run and check_inner:
            # in case stop is called without first stop data collection.
            self.stop_save()
        else:
            self._inner_run = False
        self._outer_run = False
        return False

    def _msg_handler(self, msg):
        method = msg[0]
        #print(f'{time.time():.1f}  MH: self.{method}(*{msg[1:]})', file=self._lfd)
        r = eval(f'self.{method}(*{msg[1:]})')
        #print('MHR:', r)
        # ? should we always put reply
        self._rque.put(r)

    def _send(self, msg):
        tcp_msg = f'{msg}\n'
        try:
            self._sock.sendall(tcp_msg.encode())
        except BrokenPipeError as bpe:
            self.dewe_stop(False)
            traceback.print_exc(file=sys.stderr)
            print('S: try exit')
            #print('S: try exit', file=self._lfd)
            sys.exit(1)

    def recv(self):
        try:
            raw_reply = self._sock.recv(4 * 4096)
        except ConnectionAbortedError as cae:
            self.dewe_stop(False)
            traceback.print_exc(file=sys.stderr)
            sys.exit(1)
        return raw_reply.decode().strip()

    def _query(self, msg):
        self._send(f'{msg}')
        return self.recv()

    def error_query(self):
        #print('EQ called')
        self._send(':SYST:ERR:ALL?')
        err_reply = self.recv()
        #print(f'EQ {err_reply=}')
        #lsp.logit(f'sys err status: {err_reply}')
        if 'No error' in err_reply:
            return False
        #print(f'EQ error:{err_reply=}')
        return True

def _dewe_proc(addr, port, msg_que, reply_que, lfile='Dproc.log'):
    ''' a process that runs the continously, but waits on messages
        from the world.
        The other loop runs all the time, the inner loop runs between
        starts and stop.
    '''
    global _transfer_duration
    global _running
    dewe = _Dewetron(addr, port, msg_que, reply_que)
    try:
        while dewe._outer_run:
            dewe_data = []
            dmsg = msg_que.get() # blocks here until message sent
            dewe._msg_handler(dmsg)
            #print(f'OUTER:, {dmsg}, {dewe._inner_run=}')
            sT = time.time()
            lT = sT
            while dewe._inner_run:
                #print(f'INNER:, {sT:.1f}, {dewe._inner_run=}')
                try:
                    time.sleep(_transfer_duration)
                    #print(f'INNER WAKE:, {_transfer_duration}, {dewe._inner_run=}', file=_lfd)
                    if not msg_que.empty():
                        #print('get inner NOT EMPTY')
                        try:
                            dmsg = msg_que.get_nowait()
                            if dmsg == None:
                                print('INNER got NONE, bad!')
                                #print('INNER got NONE, bad!', file=dewe._lfd)
                                dewe._inner_run = dewe._outer_run = False
                                break
                        except Exception as ex:
                            dewe._inner_run = dewe._outer_run = False
                            print('dewe-inner', ex)
                            #print('dewe-inner', ex, file=dewe._lfd)
                        #print(f'got inner:{dmsg=}')
                        dewe._msg_handler(dmsg)
                    else:
                        nT = time.time()
                        dT = nT - sT
                        dlT = nT - lT
                        lT = nT
                        if dlT > (2 * _transfer_duration): 
                            print(f'LONG sleep {dlT=} @{nT}', file=sys.stderr)
                        #print('inner get-results', dT, file=dewe._lfd)
                        dewe.save_results(dlT)
                    continue
                except Exception as ex:
                    print('inner exception')
                    print(ex)
                    traceback.print_exc(file=sys.stderr)
                    dewe._inner_run = dewe._outer_run = False
    except Exception as ex:
        print('outer exception')
        print(ex)
        traceback.print_exc(file=sys.stderr)
        sys.exit(1)
    print('DPL: SENDING STOP')
    dewe._send(':ELOG:STOP')
    dewe.sock_close()
    while not msg_que.empty():
        dmsg = msg_que.get_nowait()
        print(f'_dewe-dump {msg} @ exit')
    print(f'_dewe-proc {_d_proc} EXIT')
    #print(f'_dewe-proc {_d_proc} EXIT', file=dewe._lfd)
    return 0

# the dewe cmd queue
_dm_que = None
# the dewe reply queue
_dr_que = None
_d_proc = None

'''
The methods below all SEND messages to the process (_dewe_proc) that handles
actually talking to the meter.
The idea is to not burden the rest of the system with polling.
Note(epr): the concern about burdening the system is no longer valid when
    the ELOG protocol.
'''

def _dsend(args):
    _dm_que.put(args)
    #print(f'dsend: {args=}')
    r =  _dr_que.get()
    #print(f'dsent: {r=}')
    return r

def _dewe_stop():
    global _dm_que
    global _d_proc
    #print(f'DStop {_dm_que=}, {_d_proc=}')
    if _dm_que:
        _dsend(('dewe_stop',))
    print(f'DStopProc {_d_proc=}')
    if _d_proc:
        #print(f'D-proc {_d_proc=} joining')
        _d_proc.join(1.0)
        if _d_proc.is_alive():
            print(f'D-proc {_d_proc=} terminate')
            _d_proc.terminate()
            
    _d_proc = None

def startup(address, port=10001, lfd=None):
    ''' start-up the Dewetron process.
    Args:
        address: ip address of the dewetron meter.
        port:  port number of the ip address.
        lfd: log-file-descriptor
    '''
    global _d_proc
    global _dm_que, _dr_que
    print(f'dewe-startup:{type(port)=}, {port=}, {lfd=}')
    #log_que = lsp.get().acquire
    _dm_que = mp.Queue(1)
    _dr_que = mp.Queue(1)
    try:
        _d_proc = mp.Process(target=_dewe_proc,
                            args=(address,
                                  port,
                                  _dm_que,
                                  _dr_que,))
    except Exception as ex:
        print(f'{ex}')
        sys.exit(1)
    #print('d_proc.start()', _d_proc)
    _d_proc.start()
    atexit.register(_dewe_stop)

def data_init():
    print('msg G data_init', file=_lfd)
    return _dsend(('data_init',))

def write_csv(filename):
    #print('WRITE_CSV:', filename)
    _dsend(('write_csv', filename,))

def start_save(debug=False):
    return _dsend(('start_save',))

def stop_save():
    #print('GSS_dsend STOP_SAVE')
    _dsend(('stop_save',))


_epilog = '''
Provides a process that reads and stores meter data.
See SimpleMeter for programatic reading.
'''
if __name__ == '__main__':
    # test code
    import argparse
    import signal

    def sig_handler(sig, frame):
        sys.exit(1)
    signal.signal(signal.SIGINT, sig_handler)

    parser = argparse.ArgumentParser(description='test dewe objects',
                                     epilog=_epilog)
    parser.add_argument('--ipaddr', '-a',
                        default=os.getenv('MOCK_IP_ADDR', 'localhost'),
                        help='IP addrs of the Dewetron')
    parser.add_argument('--port', '-p',
                        default=10001,
                        help='port# of the Dewetron')
    parser.add_argument('--secs', '-s',
                        type=int,
                        default=10,
                        help='secs to gather data in full monty test')
    #parser.add_argument('--logfile', '-l',
    #                    type=argparse.FileType('w'),
    #                    default='Dex.log',
    #                    help='log file: %(default)s')
    args = parser.parse_args()
    #_lfd  = args.logfile
    #print(f'MP mode: {args.ipaddr=}:{args.port=}, {_lfd}')
    stime = time.time()
    #print(f'start @ {stime:.1f}, {args.ipaddr}:{args.port}')
    startup(args.ipaddr, args.port, _lfd)
    items = data_init()
    start_save()
    #lgr.debug(f'started {args.ipaddr=}:{args.port=} w/{items}')
    _running = True
    for _ in range(args.secs):
        time.sleep(1)
        dtime = time.time() - stime
        print(f'wl:{dtime:.1f}               ', end='\r')
        #print(f'wl:{dtime:.1f}', end='\n', file=_lfd)
        if not _running:
            #print(f'Early termination:{dtime:.1f}', end='\n', file=_lfd)
            print(f'Early termination:{dtime:.1f}', end='\n', file=sys.stderr)
            break
    print(f'M:save @ {dtime:.1f}')
    print(f'M:save @ {dtime:.1f}', file=_lfd)
    stop_save()
    write_csv('Mock.csv')
    print('\nM:see Mock.csv')
    #_dewe_stop()
    dtime = time.time() - stime
    print(f'M:Exit @ {dtime:.1f}')
    sys.exit(0)

