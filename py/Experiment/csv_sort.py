#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#

""" csv_filter.py
    A tool for dealng with CSV files.
    It allows one to read, filter, and write results.
    Also allows number operations on columns.
    Colums can be retrieved by header names or column indicies.
    Output is typically CSV format, but JSON is also an option.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import traceback
import codecs
import csv
import itertools
import json
import math
import operator
import re
import sys

import csv_io as cio


def _identity():
    def cidentity(x, row, i):
        return x
    return cidentity


def _output_lines(csv_res):
    for i, line in enumerate(csv_res):
        sline = [str(v) for v in line]
        print(','.join(sline))


def _nofilter_csv(csv_handler,
                  col_map: dict,
                  col_xforms) -> list:
    ''' handle the case where there are no filtes
    Args:
        csvrhandler: csv.handler
        col_map: two-way map of {cname <-> index} in the CSV file
        col_xforms: a list of operations
    '''
    out_rows = []
    
    end_len = len(col_xforms)
    #print(f'NFC0: {end_len=}')
    # in the common case there there are no row filters,
    # just convert the columns.
    for raw_row in csv_handler:
        if not raw_row:
            continue
        out_row = []
        rrlen = len(raw_row)
        for rindex, xform in enumerate(col_xforms):
            if  rindex >= rrlen:
                out_row.append(xform(0, raw_row, rindex))
            else:
                out_row.append(xform(raw_row[rindex], raw_row, rindex))
        out_rows.append(out_row)
    return out_rows


def filter_csv(csv_handler,
               columns: list,
               col_map: dict,
               row_filters: list,
               col_xforms: list):
    """
    Filter a CSV file based on the input parameters.
    Args:
        csv_handler: an open CSV file reader. If there is a column definition
            row at the top, it has been read, or it will be treated as
            an ordinary row.
        columns: a list of colums of interest
        row_filters: a list of pairs that filter the rows.
        col_xforms: a list of transforms by column
    Returns:
        A list of lists for the columns passed in that order, if any,
        of the column values passed.  There should max_line or fewer rows.
    """
    if not col_xforms: # shouldn't happen unless called externally
        col_xforms = itertools.repeat(_identity(), len(columns))
    raw_rows =_nofilter_csv(csv_handler, col_map, col_xforms)
    if not row_filters:
        return raw_rows
    filtered_rows = []

    for rindex, raw_row in enumerate(raw_rows):
        b = True
        for rfilter in row_filters:
            #print(f'RF: {rfilter}')
            v = raw_row[rfilter[0]]
            #print(f'RFrv: {rindex=}: {col_map[rfilter[0]]=},'
            #      f'{rfilter[0]=},{rfilter[1](v)=}, {v=}')
            if not rfilter[1](v):
                #print(f'RF=: {rindex=}, {b}')
                b = False
                break
        #print(f'RFb: {rindex=}, {b}')
        if b:
            print(f'RFadd: {rindex=}, {b}, {columns}')
            filtered_rows.append([raw_row[col_map[col]] for col in columns])
            #filtered_rows.append(raw_row)
    return filtered_rows

def get_csv_col(csv_array, col):
    """
    Take in a csv result like array and return a column as a row.
    Args:
        csv_array: a csv result like array.
        col: the desired column.
    Returns:
        a list containing the data in col.
    """
    return [ x[col] for x in csv_array ]


def sort_csv(csv_list, sort_cols, columns):
    """
    Sort the input list based on the sort cols
    Args:
        csv_list: a list of CSV lines index by indicies, and columns
        sort_cols: the column we want to sort on.
        columns: the reference columns
    Returns:
        sorted list according to sort_cols.
    TODO(epr): there is an order issue using the itemgetter, fine for one
               sort column, but it looks like the reverse for multiple
    """
    if not sort_cols:
        return csv_list
    #index = columns.index(col)
    print('SC0:', sort_cols)
    indicies = gen_csv_column_indicies(columns, sort_cols)
    #def csv_cmp(a, b):
    #    """
    #    """
    #    for index in indicies:
    #        d = cmp(a[index], b[index])
    #        if d:
    #            return d;
    #    return 0
    #print('LIN:', csv_list, indicies)
    csv_list.sort(key=operator.itemgetter(*indicies))
    #print('LOUT:', csv_list, '\n')
    return csv_list


def create_col_map(csv_cols):
    ''' create a two-way map of names and indicies
    Args:
        csv_cols: the column names we care about
        indices: a set of matching indicies in the same order csv_cols
    '''
    col_map = {}
    for cindex, col  in enumerate(csv_cols):
        #print('Col %s %d:' % (col, cindex))
        col_map[col] = cindex
        col_map[cindex] = col
    return col_map



def add_new_cols(new_cols: list, csv_cols: list, col_map: dict, xforms: list):
    ''' a list of new_cols is passed that represent operations on other
        columns.
        csv_cols, col_map, and xforms are modified.
        to the csv_cols list that is passed.  New columns are alwyas
        append to the list.
    Args:
        new_cols a list of new-columuns in the form
          :new-name:colx, coly: operation.
          the oeration must be a python method that can be operated by "eval".
          E.g., :new-col-namen:src-col0:src_col1: src-col0 + scr-col1',
            or :new-col-namen:cx:src-col0 add scr-col1',
        csv_cols: this header names filterd by args.columns
        col_map: a two way map of column ands and indicies
            E.g: {'N1':3, 'N2': 8, 'N3': 15, 'N4': 2}
        xforms: this current list of column transformers
    Return:
        returns True if new-cols, else Flase
    '''
    if not args.new_cols:
        return False
    csv_col_ops = []
    if args.new_cols:
        #print('NCs:', col_map)
        for new_col in args.new_cols:
            # we want to check for correct syntax
            sep = new_col[0]
            #print(f'{sep=}:{new_col=}')
            if new_col.count(sep) != 4:
                print('\nCheck new col delimiter, check help for new_col\n',
                      file=sys.stderr)
                print(f'With delimiter as ":", {sep} passed', file=sys.stderr)
                print('E.g.-> ', file=sys.stderr)
                print(':new-col-namen:src-col0:src_col1: src-col0 + scr-col1',
                      file=sys.stderr)
                print('Where "+" is the operator ', file=sys.stderr)
                parser.print_help()
                # exit here because we may of modified params.
                sys.exit(1)
            _new_col_adder(new_col, csv_cols, col_map, xforms)
    #print(f'CO: {csv_cols=} after new_col')
    #print(f'CO: {col_map=} after new_col')
    #print(f'COindicies: {[col_map[x] for x in col_map if x in csv_cols]}')
    return True


def gen_csv_column_map(csv_cols:list, req_cols:list=None) -> list:
    '''
    Takes the csv column names and a list of desired colums to create
        a list of indicies.
    Args:
        csv_cols: list of column names
        req_cols: list of names, we want to find
            a req_col be preceeded by, +, -, or ~, which means
            e.g., +n1, -n2, ~n3.
            + implies that the name starts with n1, - implies ends with n2,
            and ~ implies that n3 is anywhere in the name
    Returns:
        A dictionary with a two-way map of name:col, col:name
        raises a ValueError if a req_cols value is not in csv_cols.
    '''
    col_map = {}
    if not req_cols:
        for ci, cn in enumerate(csv_cols):
            col_map[ci] = cn
            col_map[cn] = ci
        return col_map
    for rc in req_cols:
        #print(f'RC: {rc}')
        match rc[0]:
            case '+':
                regx = f'^{rc[1:]}'
            case ',':
                regx = f'.*{rc[1:]}'
                regx = regx + '$'
                #print(f', {regx}')
            case '~':
                regx = f'.*{rc[1:]}.*'
                #print(f'~ {regx}')
            case _:
                regx = '^{}$'.format(rc)
                #print(f'_ {regx}')
        print(f'RX: {regx}')
        for ci, cn in enumerate(csv_cols):
            rmatch = re.match(regx, col)
            if rmatch is None:
                continue
            rm = rmatch.string
            print(f'{rm=}')
            col_map[ci] = cn
            col_map[cn] = ci
        return col_map


def _new_col_adder(new_col: str,
                   csv_cols: list,
                   col_map: dict,
                   xforms: list):
    """
    Return function that will return a new value to append to row.
    Args:
        new_col: the coded new column value
            a python operation in column name values.
            e.g. --new_col ":cname:c1,c2:operation
            we substitute naively.
        csv_cols: the current list of columns which will be appended
        col_maps: the current two-way column map 
        xforms: this current list of column transformers
    Modifies:
        modifies csv_cols, col_map, and xforms
    Returns:
        None
    """
    def sub_op(ca, cb, op, cmap):
        ''' substitute operation
        '''
        #print('OS:', op, cmap)
        ai = cmap[ca]
        bi = cmap[cb]
        op = op.replace(ca, f'float(row[{ai}])')
        op = op.replace(cb, f'float(row[{bi}])')
        #print(f'OSe: {op=}')
        return op
    #print(f'{new_col=}')
    sep = new_col[0]
    new_col, ca, cb, cop = new_col[1:].split(sep)
    #print(f'NCA0:, {new_col=}, {ca=}, {cb=}, {cop=}')
    csv_cols.append(new_col)
    l = len(csv_cols) - 1
    col_map[new_col] = l
    col_map[l] = new_col
    #print(f'NCA1: {col_map[new_col]=}')
    operation = sub_op(ca, cb, cop, col_map)
    #print(f'NCA: {operation=}')
    def cadder(x, row, rindex):
        #traceback.print_exc()
        #print(f'CA: {operation=}, ... {row=}, {rindex=}')
        return eval(operation)
    xforms.append(cadder)

    #print(f'NCA: {xforms[-1]=}, {len(xforms)=}')
    return None


def _make_col_filter(expression):
    #print(f'mcf: {expression}')
    def col_filter(_VAR_):
        #print(f'CF: {_VAR_=}, {type(_VAR_)=}, {expression=}, {eval(expression)}')
        return eval(expression)
    return col_filter


def gen_csv_row_filters(col_map, csv_row_filters):
    """
    Args:
        col_map: two-way dictiony map
        csv_row_filters: a list of column names, regular expresion pairs
    Returns:
        A list of integer indices regx pairs
        raises a ValueError if index_cols value is not in csv_cols.
    """
    if not csv_row_filters:
        return []
    row_filters = []
    for row_filter in csv_row_filters:
        tindex, rfilter = row_filter.split(':')
        rfilter = rfilter.strip().strip('"')
        if rfilter.find('E ') != 0:
            print(f'Bad filter: {rfilter}')
            sys.exit(1)
        if tindex.isnumeric():
            tindex = col_map[int(tindex)]
        else:
            index = col_map[tindex]
        xfilter = rfilter[2:].replace(tindex, '_VAR_')
        row_filters.append((index, _make_col_filter(xfilter)))
    return row_filters


def gen_csv_col_xforms(csv_cols, tcols, col_map):
    """ generate a list of column transforms, i.e. create a function
        for each column value when dealing with the value in/for that column.
        Typically, this is the identity method.
    Args:
        csv_cols: the column names in column order for CSV file.
        tcols: list of column name or index and a 'python' type, seperated
            by ":"
        col_map: the column map -- this is added to.
    Returns:
        a list of transform methods, in this case either _identity or a type
        method.
    Note: 
    """
    xforms = [_identity() for _ in csv_cols]
    #print(f'GCCX: {len(xforms)=}, {len(csv_cols)=}')
    if not tcols:
        return xforms
    for col in tcols:
        cindex, xform = col.split(':')
        print(f'XF: {cindex=}, {xform=}')
        # cindex is text, but it may be a col number
        index = col_map[cindex]
        def _type(x, row, index):
            #print(f'_T: {x=}, {xform=}, {index=}')
            yform = f'{xform}({x})'
            #print(f'_Tx: {yform})')
            y = eval(yform)
            #print(f'_Ty: {y}')
            return eval(f'{yform}')
        xforms[index] = _type
    #print(f'GCCX: {xforms=}')
    return xforms


if __name__ == "__main__":
    #print(sys.version_info)
    parser = argparse.ArgumentParser(description='filter rows and columns '
                                                 'from a CSV file and write '
                                                 'a new CSV file')
    cio.reader_options(parser)
    parser.add_argument('--col', '-c',
                        dest='columns',
                        type=str,
                        action='append',
                        help='CSV columns to accept, either names or indexes. '
                             '+col matchings all columns '
                                'that begin with "col". '
                             '-col  matchings all columns '
                                'that ned with "col".')
    parser.add_argument('--ctype', '-t',
                        dest='col_types',
                        type=str,
                        action='append',
                        default=None,
                        help='from the possible  columns, the column type, '
                             'default col type is str. ' \
                             'e.g. product_type:int. '
                             'Use to override the defaults '
                             'of string or float.'
                             )
    parser.add_argument('--filter', '-f',
                        type=str,
                        dest='filters',
                        action='append',
                        default=[],
                        help='from the possible columns, criteria by column '
                             'for accepting rows, which can be a regular '
                             'expression. e.g. product_type:shoe.'
                             'Numeric expressions must start with "E expr",'
                             'e.g. "cx:E cx > 5.3". Expression must be in '
                             'one variable only.'
                             'Expressions may require a type specification.')
    parser.add_argument('--new_col', '-n',
                        type=str,
                        dest='new_cols',
                        action='append',
                        default=[],
                        help='add a new column based on operations '
                             'with other columns '
                             'e.g. --new_col ":cname:c1 op c2 [op c3 ...]" '
                             'or, --new_col "cname:func(c1, c2) op cN", '
                             'etc.  '
                             'First value, ":", is the sepearator, '
                             'but it can be replaced by any character '
                             'func must be a python operator '
                             'if resulting type is not float '
                             'include type in func)')
    # TODO(epr): should be csv_sort.py?
    parser.add_argument('--sort', '-s',
                        dest='sort_cols',
                        type=str,
                        action='append',
                        default=[],
                        help='from the selected columns, sort the output '
                             'in the order presented. e.g. --sort=cname ')
    # TODO(epr): should be csv_dedup.py?
    parser.add_argument('--dedup', '-D',
                        type=int,
                        help='remove dupicate lines ignoring column dedup '
                             'and after filtering. Zero removes no columns, '
                             'default: "%(default)s"')
    # TODO(epr): should be csv_rotate.py?
    parser.add_argument('--rotate', '-r',
                        action='store_true',
                        help = 'rotate output: column: [values]')
    parser.add_argument('--index', '-I',
                        action='store_true',
                        help='print the index ')
    parser.add_argument('--count',
                        action='store_true',
                        help='count and print the number of returned lines')
    parser.add_argument('--json',
                        action='store_true',
                        help='output data as json')
    parser.add_argument('--jkey',
                        type=str,
                        help='when json output selected use key'
                             ' allows number or string')
    args = parser.parse_args()
    #print(f'0: {args.columns=}')

    csv_cols, csv_reader = cio.csv_open_with_args(args)
    col_map = gen_csv_column_map(csv_cols)
    #print(f'MCM: {col_map}')
    #col_map = create_col_map(csv_cols)
    #if args.columns:
    #    csv_cols = [col for col in args.columns]
    #else:
    #    columns = csv_cols

    csv_col_xforms = gen_csv_col_xforms(csv_cols,
                                        args.col_types,
                                        col_map)
    # Note(epr): csv_cols, col_map and xforms are all modified if new-cols
    # are passed
    add_new_cols(args.new_cols, csv_cols, col_map, csv_col_xforms)
    # required params
    csv_row_filters = gen_csv_row_filters(col_map, args.filters)

    #print('filter')
    csv_filtered = filter_csv(csv_reader,
                              columns=args.columns,
                              col_map=col_map,
                              row_filters=csv_row_filters,
                              col_xforms=csv_col_xforms,
                             )
    #_output_lines(csv_filtered)
    #sys.exit(0)
    csv_res = sort_csv(csv_filtered,
                       sort_cols=args.sort_cols,
                       columns=[ csv_cols[i] for i in csv_indicies ])

    ofh = cio.write_fh(args)

    for index in csv_indicies[:-1]:
        print(f'{csv_cols[index]}{args.delimiter}', file=ofh, end='')
        print(f'{csv_cols[csv_indicies[len(csv_indicies) - 1]].strip()}',
              file=ofh)
    last_cmp_line = ''
    for csv_line in csv_res:
        # CSV output
        for column in csv_line[:-1]:
            print('%s%s' % (column.strip()
                    if type(column) is str
                    else column, args.delimiter),
                  file=ofh, end='')
        fv = csv_line[len(csv_line) - 1]
        print('%s' % fv.strip() if type(fv) is str else fv, file=ofh)
    if args.count:
        print('\nLines:', len(csv_res), file=ofh)


