#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

"""
   Taken from eo's tcp_logger.py hence keeping the open source comment.
   proxy_meter.py is a meter proxy logger.
   It runs on localhost and proxies an address of a server on the network.
   Logs all the interactions to some file.

"""

__author__ = 'emilio.rojas@apparent.com (Emilio Rojas)'

import argparse
import os
import os.path
import threading
import time
import shutil
import socket
import socketserver
import sys


PROXY_HOST = "localhost"
PROXY_PORT = 10001
_dewetron = "localhost"

_running = True

def proxy_server(host, port, rhost, rport, lfd):
    ''' create a server that opens a client port and a server port.
        When it recieve something on either something client port
        it is the man in the middle
    Args:
        host: string name of host, should be localhost
        port: int port# on host 
        rhost: string name of host, could be localhost for testing
        rport: int port# on rhost, can't be port for testing
        lfd: is the open logfile descriptor
    '''
    global _running
    # create the client socket
    cs = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    cs.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 0)
    #cs.settimeout(1)
    print(f'P-con: ({rhost}:{rport})', file=lfd)
    try:
        cs.connect((rhost, rport))
    except ConnectionRefusedError as cre:
        print(f'{cre}', file=lfd)
        return
    cs.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # now act as a server the forwards and reads response
    # problem: not all commands have a response. So, first approach
    # make it smart, just like the dewetron code where only queries have
    # resposes. i.e., message in in '?'
    # The communication with the proxy endpoint is always inititated
    # by the client
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            print(f'P-srv: ({host}:{port})', file=lfd)
            s.bind((host, port))
            s.listen()
            conn, addr = s.accept()
            with conn:
                print(f'{addr} connected')
                while _running:
                    try :
                        # get client command
                        cdata = conn.recv(4 * 4096)
                        if not cdata:
                            print(f'server: {cdata}', file=lfd)
                            print(f'SERVER: {cdata}')
                            #break
                        print(f'client: {cdata}', file=lfd)
                    except ConnectionAbortedError as cae:
                        print(f'{cae}')
                        break;
                    #b = (cdata == b':ELOG:STOP\n')
                    #print(f'ps-rcvd: {cdata} ==stop? {b}', file=lfd)
                    if cdata == b':ELOG:STOP\n':
                        print(f'ps-rcvd: {cdata}, leaving')
                        _running = False
                    # OK we got something, so send it to the endpoint
                    cs.sendall(cdata)
                    if b'?' in cdata:
                        # if it is a query we wait for the reply
                        rdata = cs.recv(4 * 4096)
                        print(f'server: {rdata}', file=lfd)
                        conn.sendall(rdata)
                        continue
                    # otherwise we back for the next mesage
                    if not cdata:
                        print(f'CLIENT: {cdata}')
                        break;
                    continue
    print('proxy_server leaving')

def echoy_server(host, port):
    ''' echos input in upper case.
        It is here for testing only, otherwise the meter is the target.
    '''
    global _running
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        print(f'E: ({host}:{port})')
        try:
            s.bind((host, port))
        except OSError as ose:
            print(f'{ose}, quiting')
            return
        s.listen()
        conn, addr = s.accept()
        with conn:
            print(f'{addr} connected')
            while _running:
                try :
                    rdata = conn.recv(4 * 4096)
                except ConnectionAbortedError as cae:
                    print(f'{cae}')
                    break;
                print(f'es-rcvd: {rdata}')
                if not rdata:
                    break;
                if b'?' in rdata:
                    conn.sendall(rdata.upper())
    print('echoy_server leaving')


def tcp_send(host, port):
    """ Connect to a server at (host, port) and get CLI and send to the
        sever and read the response.
    Args:
    """
    global _running
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        # Connect to server and send data
        try:
            sock.connect((host, port))
        except ConnectionRefusedError as cre:
            print(f'{cre} w/ ({host}:{port})')
            print(f'tcp_send quiting')
            return
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        #mfh = sock.makefile()

        while _running:
            to_send = sys.stdin.readline().strip()
            if not to_send:
                print('NULL rcv')
                break
            print(f'send: {to_send}')
            sock.sendall(to_send.encode('utf-8'))
            if '?' in to_send:
                rcv_back = sock.recv(4 * 4096)
                print(f'rback: {rcv_back}')
            #break
    print(f'tcp_send leaving')


if __name__ == "__main__":
    import signal
    def sig_handler(sig, frame):
        _running = False
        sys.exit(1)

    signal.signal(signal.SIGINT, sig_handler)

    parser = argparse.ArgumentParser(description='test TCP program')
    parser.add_argument('--ipaddr', '-a',
                        default=os.getenv('DEWE_IP_ADDR', '192.168.1.236'),
                        help='IP addrs of the Dewetron, default: %(default)s')
    parser.add_argument('--port', '-p',
                        default=PROXY_PORT,
                        help='port# of the Dewetron, default: %(default)s')
    parser.add_argument('--host', '-H',
                        default=PROXY_HOST,
                        help = 'provide host, default: %(default)s')
    parser.add_argument('--rport', '-r',
                        default=PROXY_PORT,
                        help='port# of the Dewetron, default: %(default)s')
    parser.add_argument('--log_file',
                        type=argparse.FileType('w'),
                        default=sys.stdout,
                        help='set log filename, defaults to stdout')
    parser.add_argument('--test', '-T',
                        action='store_true',
                        help='Run test tread that echos with upper case')
    #parser.add_argument('--rhost', '-R',
    #                    default=PROXY_HOST,
    #                    help = 'provide host, default: %(default)s')
    opts = parser.parse_args()

    print('make threads')

    if opts.test:
        opts.rport += 1 # adjust the port since we are on same machine
        opts.ipaddr = 'localhost' # use local host
        echoyt = threading.Thread(target=echoy_server,
                                 name='echoy',
                                 args=(opts.ipaddr, opts.rport,))
        print(f'{echoyt}:')
        print('start echoyt and sleep 0.1 sec')
        echoyt.start()
        time.sleep(0.1)
    proxyt = threading.Thread(target=proxy_server,
                              name='proxy',
                              args=(opts.host,
                                    opts.port,
                                    opts.ipaddr,
                                    opts.rport,
                                    opts.log_file))
    if opts.test:
        sendt = threading.Thread(target=tcp_send,
                                 name='send-it',
                                 args=(opts.host, opts.port,))
        print(f'{sendt}:')
    print(f'{proxyt}:')
    print('start proxyt and sleep 0.1 sec')
    proxyt.start()
    time.sleep(0.1)
    if opts.test:
        print('start sendt ')
        sendt.start()
    print('waiting for completion')
    #line = sys.stdin.read()
    #if '~' in line.upper():
    #    _running = False
    proxyt.join()
    if opts.test:
        echoyt.join()
        sendt.join()
    
