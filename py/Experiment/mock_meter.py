#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

"""
   A mock-meter server
   accepts dewetrom messages and responds with resonable reply

"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import os
import os.path
import threading
import time
import random
import shutil
import signal
import socket
import socketserver
import sys


PROXY_HOST = "localhost"
PROXY_PORT = 10001
_dewetron = "localhost"
_period = None
_start_time = None
_samples_sent = 0

_lfd = None

_running = True


# start in CONFIG state!
_current_state = 'CONFIG'

_meter_states = {
    'CONFIG',
    'RUNNING',
    'ERROR',
}

def _reply(ssock, msg):
    #print(f'R: {ssock} <- {msg}', file=_lfd)
    ssock.sendall(f'{msg}\n'.encode())
    return 

def _get_state(ssock, msg):
    global _current_state
    #print(f'GS:{_current_state=}')
    _reply(ssock, _current_state)
    return

def _start_state(ssock, msg):
    global _current_state
    global _start_time
    print(f'SS: {msg=}')
    if _current_state == 'CONFIG':
        _current_state = 'RUNNING'
        _start_time = time.time()
        #print(f'SSret:{_current_state=}, {_start_time:.2f}')
        return
    print(f'StS:{_current_state=}')
    if _current_state == 'RUNNING':
        _current_state == 'ERROR'
        return
    _current_state == 'ERROR'
    return

def _null(ssock, msg):
    return

def _set_period(ssock, msg):
    global _period
    #print(f'SP: {msg}')
    _, secs = msg.split(' ')
    _period = float(secs)

def _get_period(ssock, msg):
    _reply(ssock, f'{_period:.5f}')

_items = None

def _set_items(ssock, msg):
    global _items
    #print(f'si:{msg}')
    mname, chans = msg.split(' ', 1)
    #print(f'si:{mname=}, {chans=}')
    _items = ','.join(chans.split(','))
    #print(f'si:{_items=}')

def _get_items(ssock, msg):
    global _items
    _reply(ssock, _items)

# sometime send a NAN l
#_sfetch_nan = [NAN,NAN,NAN,NAN,NAN,NAN,]
# some server fetech responses.
# code will replace time values as it loops
_server_fetch = [
    [
        1.2042825E+2,2.4228318E+0,2.9156006E+2,2.9160065E+2,4.8647552E+0,5.999292E+1,
    ],
    [
        1.2044936E+2,2.4165547E+0,2.9087909E+2,2.9088049E+2,9.0828234E-1,6.000182E+1,
    ],
    [
        1.2049153E+2,2.4159598E+0,2.9093515E+2,2.9094119E+2,1.8733422E+0,6.0001911E+1,
    ],
    [
        1.204513E+2,2.4200408E+0,2.9105832E+2,2.9129501E+2,1.1740126E+1,6.0001911E+1,
    ],
    [
        1.2039048E+2,2.4119701E+0,2.9020078E+2,2.9020639E+2,1.8072727E+0,6.0001911E+1,
    ],
    [
        1.2037218E+2,2.4147391E+0,2.9048117E+2,2.9048471E+2,1.4285738E+0,6.0001911E+1,
    ],
    [
        1.2030326E+2,2.4161005E+0,2.904863E+2,2.9049832E+2,2.6422925E+0,6.0001911E+1,
    ],
    [
        1.2041285E+2,2.4185686E+0,2.9105209E+2,2.910816E+2,4.1452761E+0,6.0001911E+1,
    ],
    [
        1.2044958E+2,2.41524E+0,2.9075092E+2,2.9075638E+2,1.7841974E+0,6.0001911E+1,
    ],
]

_last_nan = False;
_fetch_index = 0;
_fetch_len = len(_server_fetch)


def _next_elog(ssock, msg):
    ''' send the next sends of ELog samples.
    Time zero is the time START is received.
    Each sample time increments by the <_period>a.
    We send enough same to reach the current period since START
    '''
    global _fetch_index
    global _fetch_len
    global _last_nan
    global _ltime
    global _samples_sent

    print(f'{msg=}', file=_lfd)
    #print(f'{msg=}', file=sys.stderr)
    if _current_state != 'RUNNING':
        print(f'NE bad state:{_current_state =}', file=sys.stderr)
        ssock.sendall('ERROR'.encode())
        return
        
    '''
    if _fetch_index == 0 and not _last_nan:
        _ltime = _start_time
        dt = 0.0
        ctime = time.time()
        dtime = ctime - _start_time
        # Randomly send a NAN group at the start
        r = 5 * random.random()      
        if r % 5 == 0:
            ssock.sendall(snan_out.encode())
            _last_nan = True
            return
    '''
    ctime = time.time()
    dtime = ctime - _start_time
    print(f'NEL:{dtime=}', file=_lfd)
    #print(f'NEL:{dtime=}')
    # round to next samples
    samples_to_send = int((dtime / _period) + 0.5)
    n = samples_to_send - _samples_sent
    # calc samples to send 
    ol = []
    tto_send = ctime
    for i in range(n):
        sample = _server_fetch[_fetch_index]
        samp_strs = [ f'{v:.5e}' for v in sample]
        samp_strs.insert(0, f'{tto_send:.2f}')
        ol.append(','.join(samp_strs))
        _fetch_index += 1
        if _fetch_index >= _fetch_len:
            _fetch_index = 0
            #_last_nan = False
        tto_send += _period
    _samples_sent += n
    # Now we need single string
    ostr = ','.join(ol)
    print(f'NEG:{len(ol)=}, {_samples_sent=}, {len(ostr)=}, {len(ostr.split(","))=}',
            file=_lfd)
    print(f'NEG-send: {ostr} ', file=_lfd)
    #print(f'NEG:{_samples_sent=}')
    #print(f'NEG-send:{ostr}')
    _reply(ssock, ostr)
    return

def _stop(ssock, msg):
    global _running
    print(f'RCV STOP, {msg}')
    ssock.close()
    _running = False

def _idn(ssock, msg):
    _reply(ssock, 'DEWETRON,OXYGEN,0,6.3.1')

def _version(ssock, msg):
    _reply(ssock, 'SCPI,"1999.0",RC_SCPI,"1.21",OXYGEN,"6.3.1"')


_client_cmds = {
    ':COMM:HEAD': _null, # OFF
    ':ELOG:ITEMS' : _set_items,
    ':ELOG:ITEMS?' : _get_items,
    ':ELOG:PERIOD': _set_period,
    ':ELOG:PERIOD?': _get_period,
    ':ELOG:STATE?' : _get_state,
    ':ELOG:TIM': _null, # REL
    ':ELOG:START': _start_state,
    ':ELOG:FETCH?' : _next_elog,
    ':ELOG:STOP' : _stop,
    '*VER?' : _version,
    '*IDN?' : _idn,
}

def _bad_cmd(scock, msg):
    print(f'BCmd:{msg=}', file=_lfd)
    raise Exception(f'Bad Cmd:|{msg}|')
    

def _get_handler(msg):
    m = msg.split(' ', 1)
    print(f'{m=}, {msg=}', file=_lfd)
    k = m[0].upper()
    h = _client_cmds.get(k, _bad_cmd)
    print(f'{h=}, {m=}, {k=}', file=_lfd)
    return h


def mock_meter(host, port, lfd):
    ''' create a server that opens a server port.
        When it recieves a commad it responds with an appropriate
        response form above.
    Args:
        host: string name of host, should be localhost
        port: int port# on host 
        lfd: is the open logfile descriptor
    '''
    global _running
    global _lfd
    _lfd = lfd
    # now act as a server that reads response
    # problem: not all commands have a response. So, first approach
    # make it smart, just like the dewetron code where only queries have
    # resposes. i.e., message in in '?'
    # The communication with the proxy endpoint is always inititated
    # by the client
    empty_cnt = 0
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            print(f'P-srv: ({host}:{port})', file=lfd)
            s.bind((host, port))
            s.listen()
            conn, addr = s.accept()
            with conn:
                print(f'{addr} connected', file=lfd)
                print(f'{addr} connected')
                while _running:
                    try :
                        # get client command
                        cdata = conn.recv(256)
                        if not cdata:
                            print(f'meter rcvd: {cdata}', file=lfd)
                            print(f'meter rcvd: {cdata}')
                            if empty_cnt > 5:
                                break
                            empty_cnt += 1
                        print(f'meter rcvd: {cdata}', file=lfd)
                    except ConnectionAbortedError as cae:
                        print(f'{cae}')
                        return;
                    msg = cdata.decode().strip()
                    msgs = msg.split('\n', 1)
                    for m in msgs:
                        hfunc = _get_handler(m)
                        hfunc(conn, m)
                    continue
    print('mock-meter leaving')

if __name__ == '__main__':
    def sig_handler(sig, frame):
        sys.exit(1)
    signal.signal(signal.SIGINT, sig_handler)

    parser = argparse.ArgumentParser(description='dewe meter mock')
    parser.add_argument('--ipaddr', '-a',
                        default=os.getenv('MOCK_IP_ADDR', 'localhost'),
                        help='IP addrs of the Dewetron')
    parser.add_argument('--port', '-p',
                        default=10001,
                        help='port# of the Dewetron')
    parser.add_argument('--logfile', '-l',
                        type=argparse.FileType('w'),
                        default='mm.log',
                        help='log files for mock-meter: %(default)s')
    args = parser.parse_args()
    mock_meter(args.ipaddr, args.port, args.logfile)
    sys.exit(0)


