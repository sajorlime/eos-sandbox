#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#

""" test_new_arg.py
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import traceback
import codecs
import csv
import itertools
import json
import math
import operator
import re
import sys


def test_match(csv_cols:list, req_cols:list) -> dict:
    '''
    Takes the csv column names and a list of desired colums to create
        a list of indicies.
    Args:
        csv_cols: list of column names
        req_cols: list of names, we want to find
            a req_col be preceeded by, +, -, or ~, which means
            e.g., +n1, -n2, ~n3.
            + implies that the name starts with n1, - implies ends with n2,
            and ~ implies that n3 is anywhere in the name
    Returns:
        A dictionary with a two-way map of name:col, col:name
        raises a ValueError if a req_cols value is not in csv_cols.
    '''
    col_map = {}
    for rc in req_cols:
        #print(f'RC: {rc}')
        match rc[0]:
            case '+':
                regx = f'^{rc[1:]}'
            case ',':
                regx = f'.*{rc[1:]}'
                regx = regx + '$'
                #print(f', {regx}')
            case '~':
                regx = f'.*{rc[1:]}.*'
                #print(f'~ {regx}')
            case _:
                regx = '^{}$'.format(rc)
                #print(f'_ {regx}')
        #print(f'RX: {regx}')
        for cn, col in enumerate(csv_cols):
            rmatch = re.match(regx, col)
            if rmatch is None:
                continue
            rm = rmatch.string
            col_map[col] = cn
            col_map[cn] = col
    return col_map


def _new_col_adder(new_col: str,
                   csv_cols: list,
                   col_map: dict,
                   xforms: list):
    """
    Return function that will return a new value to append to row.
    Args:
        new_col: the coded new column value
            a python operation in column name values.
            e.g. --new_col ":cname:c1,c2:operation
            we substitute naively.
        csv_cols: the current list of columns which will be appended
        col_maps: the current two-way column map 
        xforms: this current list of column transformers
    Modifies:
        modifies csv_cols, col_map, and xforms
    Returns:
        None
    """
    def sub_op(ca, cb, op, cmap):
        ''' substitute operation
        '''
        print('OS:', op, cmap)
        ai = cmap[ca]
        bi = cmap[cb]
        op = op.replace(ca, f'row[{ai}]')
        op = op.replace(cb, f'row[{bi}]')
        print(f'OSe: {op=}')
        return op
    #print(f'{new_col=}')
    sep = new_col[0]
    new_col, ca, cb, cop = new_col[1:].split(sep)
    print(f'NCA0:, {new_col=}, {ca=}, {cb=}, {cop=}')
    csv_cols.append(new_col)
    l = len(csv_cols) - 1
    col_map[new_col] = l
    col_map[l] = new_col
    print(f'NCA1: {col_map=}')
    operation = sub_op(ca, cb, cop, col_map)
    print(f'NCA: {operation=}')
    def cadder(row):
        #traceback.print_exc()
        print(f'CA: {operation=}, ... {row[1]=}, ... {row=}')
        return eval(operation)
    xforms.append(cadder)
    return None


def add_new_cols(new_cols: list, csv_cols: list, col_map: dict, xforms: list):
    ''' a list of new_cols is passed that represent operations on other
        columns.
        csv_cols, col_map, and xforms are modified.
        to the csv_cols list that is passed.  New columns are alwyas
        append to the list.
    Args:
        new_cols a list of new-columuns in the form
          :new-name:colx, coly: operation.
          the oeration must be a python method that can be operated by "eval".
          E.g., :new-col-namen:src-col0:src_col1: src-col0 + scr-col1',
            or :new-col-namen:cx:src-col0 add scr-col1',
        csv_cols: this header names filterd by args.columns
        col_map: a two way map of column ands and indicies
            E.g: {'N1':3, 'N2': 8, 'N3': 15, 'N4': 2}
        xforms: this current list of column transformers
    Return:
        returns True if new-cols, else Flase
    '''
    if not args.new_cols:
        return False
    csv_col_ops = []
    if args.new_cols:
        #print('NCs:', col_map)
        for new_col in args.new_cols:
            # we want to check for correct syntax
            sep = new_col[0]
            print(f'{sep=}:{new_col=}')
            if new_col.count(sep) != 4:
                print('\nCheck new col delimiter, check help for new_col\n',
                      file=sys.stderr)
                print(f'With delimiter as ":", {sep} passed', file=sys.stderr)
                print('E.g.-> ', file=sys.stderr)
                print(':new-col-namen:src-col0:src_col1: src-col0 + scr-col1',
                      file=sys.stderr)
                print('Where "+" is the operator ', file=sys.stderr)
                parser.print_help()
                # exit here because we may of modified params.
                sys.exit(1)
            _new_col_adder(new_col, csv_cols, col_map, xforms)
    print(f'CO: {csv_cols=} after new_col')
    print(f'CO: {col_map=} after new_col')
    print(f'COindicies: {[col_map[x] for x in col_map if x in csv_cols]}')
    return True


def _identity(rindex):
    def cidentity(row):
        return row[rindex]
    return cidentity

cols = 'Secs,meas-volts,meas-amps,meas-watts,meas-S(VA),meas-Q(VARS),meas-Hz,inv-volts,inv-amps,inv-watts,inv-VA(S),inv-VARS(Q),inv-Hz,s/n: 512312000004'
clist = cols.split(',')

xforms = [_identity(ci) for ci, _ in enumerate(clist)]


if __name__ == "__main__":
    #print(sys.version_info)
    parser = argparse.ArgumentParser(description='test match ')
    parser.add_argument('--col', '-c',
                        dest='columns',
                        type=str,
                        action='append',
                        help='CSV columns names. '
                             '+col matchings all columns '
                                'that begin with "col". '
                             '-col  matchings all columns '
                                'that ned with "col".')
    '''
    parser.add_argument('--ncol', '-nc',
                        dest='ncols',
                        type=int,
                        action='append',
                        help='CSV columns indexes. '
                             '+col matchings all columns '
                                'that begin with "col". '
                             '-col  matchings all columns '
                                'that ned with "col".')
    '''
    parser.add_argument('--new_col', '-n',
                        type=str,
                        dest='new_cols',
                        action='append',
                        default=[],
                        help='add a new column based on operations '
                             'with other columns '
                             'e.g. --new_col ":cname:c1 op c2 [op c3 ...]" '
                             'or, --new_col "cname:func(c1, c2) op cN", '
                             'etc.  '
                             'First value, ":", is the sepearator, '
                             'but it can be replaced by any character '
                             'func must be a python operator '
                             'if resulting type is not float '
                             'include type in func')
    args = parser.parse_args()
    #print(f'0: {args.columns=}')

    if args.columns:
        columns = args.columns
    else:
        columns = clist
    cmap = test_match(clist, clist)

    add_new_cols(args.new_cols, clist, cmap, xforms)
    print(f'{columns=}, {len(columns)=}\n')
    print(f'{cmap=}\n')
    print(f'{xforms=}, {len(xforms)=}\n')

