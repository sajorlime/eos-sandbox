#!/usr/bin/env python3
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""
import os
import sys
import datetime

todayPos = 0
tomorrowPos = 1
dayPos = 0
monthPos = 1
yearPos = 2

def _get_date(aDate=datetime.date.today()):
    print(aDate)
    aDateStr = "%s" % aDate
    aDateList = aDateStr.split('-')
    day = aDateList.pop()[0:2]; # we don't want the time if it is present
    month = aDateList.pop()
    if month == '12':
        month = 'C'
    elif month == '11':
        month = 'B'
    elif month == '10':
        month = 'A'
    else:
        month = month[1:2]
        year = aDateList.pop()
    return [day, month, year]

def getDates(today=datetime.date.today()):
    todayList = _get_date(today)
    tomorrow = today + datetime.timedelta(1)
    tomorrowList = _get_date(tomorrow)
    return [todayList, tomorrowList]

