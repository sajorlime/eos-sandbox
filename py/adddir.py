#!/usr/bin/env python3
#
""" adddir.py 
    is passed dir alias file, and an optional target name
    e.g.:
        adddir.py /home/eo/bash/bash_xdirs.sh [thisname]
    it adds to the file the new alias using either thisname or the
    basename of the directory.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""


__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import os
import os.path
import shutil
import sys

import adjust_path
import args
import filefilter


usage = """
adddir.py alias-file-path [aliasname]

adds an alias at the end of the file to change directory to
the current directory with the name aliasname or basename.
If the basename has spaces these are changed to underscores ('_').
"""


if __name__ == '__main__':
    myargs = args.Args(usage)

    name = myargs.get_arg('name', None)
    absolute = myargs.get_arg('absolute', None)

    afpath = myargs.get_value()
    mpathv = myargs.get_values()
    #print >> sys.stderr, mpathv
    mpath = mpathv[0]
    fpath = '%s' % mpath
    if len(mpathv) > 1:
        for mpart in mpathv[1:]:
            mpath = '%s %s' % (mpath, mpart)
            fpath = '%s_%s' % (fpath, mpart)

    if not name:
        lslash_pos = fpath.rfind('/')
        if lslash_pos < 0:
            name = fpath
        else:
            name = fpath[lslash_pos + 1:]

    name = name.replace(' ', '_')
    mpath = mpath.replace(' ', '\ ')

    if absolute:
        alias = 'alias %s=\"cd \\\"%s\\\";is_devbase;showtitle\"\n' % (name, mpath)
    else:
        adpath = adjust_path.adjust_path(mpath, *('DEV_LIB', 'DEV_SRC', 'DEV_BASE', 'HOME'))
        alias = 'alias %s=\"cd \\\"%s\\\";showtitle\"\n' % (name, adpath)
    afh = open(afpath, 'a')
    #afh = sys.stdout
    #alias = 'alias %s=\"cd \\\"%s\\\"\; showtitle"\n' % (name, adpath)
    # we want alias cv6.1="cd \"/home/cv6.1\";showtitle"
    # we want alias cv6.1="cd \"/home/cv6.1\""
    # alias = 'alias %s=\"cd \\\"%s\\\"\"\n' % (name, adpath)
    #print >> sys.stderr, alias
    afh.write(alias)
    #print('A:', alias)

