#!/usr/bin/env python3

"""
    fixhist.py -- fix history files up.
        -- remove dupicates and meaningless items.
           e.g. 'ls' by itself
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import time

import filefilter
import uniqueList


def fixhist():
    parser = argparse.ArgumentParser(description='Fix history files',
                                     epilog='see tree.py --help for example filters')
    parser.add_argument('--src', '-s',
                        dest='src',
                        type=str,
                        default='',
                        help='the source history file')
    parser.add_argument('--dst', '-d',
                        dest='dst',
                        type=str,
                        default='history.filt',
                        help='the destination directory, default %(default)s')
    parser.add_argument('--filter', '-f', 
                        dest='filters',
                        action='append',
                        type=str,
                        default='',
                        help='add a filter')
    myargs = parser.parse_args()

    if not os.path.exists(myargs.src):
        #print(sys.stderr >> 'The file %s must exist' % myargs.src)
        sys.exit(1)

    hitems = uniqueList.uniqueList(filefilter.ReadFile(myargs.src))
    wfd = open(myargs.dst, 'w')
    for item in hitems:
      print(item, file=wfd, end='')

if __name__=='__main__':
    fixhist()

