#!/usr/bin/env python3
#
""" groups.py 
    test program for loading Groups class
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""


__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import argparse
import os
import os.path
import shutil
import sys

import adjust_path
import args
import filefilter


usage = """
adddir.py alias-file-path [aliasname]

adds an alias at the end of the file to change directory to
the current directory with the name aliasname or basename.
If the basename has spaces these are changed to underscores ('_').
"""

class Group(object):
    def __init__(self, glist: str) -> None:
        self._glist = glist.split(',')
        return None
    
    @property
    def cols(self) -> list:
        return tuple([ int(col) for col in self._glist ])

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='groups test code')
    parser.add_argument('--glist', '-g',
                        type=Group,
                        action='append',
                        help='e.g "0, 2, 3"')
    args = parser.parse_args()
    for g in args.glist:
        print(g.cols)


