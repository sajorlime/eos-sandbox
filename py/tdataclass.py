#!/usr/bin/env python3
# tdataclass
# 11/23/2022
# experiment with data class

import argparse
import csv
import enum
import json
import os
import math
import re
import struct
import sys
import time
import traceback

from dataclasses import dataclass, asdict, field
import dataclasses

def _alimit(lo, lmin, lmax):
    limit = float(lo)
    if (limit >= lmin) and (limit <= lmax):
        return limit
    raise argparse.ArgumentTypeError(f'{limit} not in [{lmin}, {lmax}]')


def _a_limit(pfs):
    pf = float(pfs)
    if pf > -74.0 and pf < 74.0:
        return f'{pf=}'
    raise argparse.ArgumentTypeError(f'{pfs} not in +/-74degs range')

@dataclass
class Member(object):
    _desc = 'I am Member'
    _default = None
    _help = f'I am Member, default: {None}'
    #def __init__(self, t=float, n='M', v=0, m=0, x=0, d='I am a M'):
    #    self._type = t
    #    self._name = n
    #    self._value = v
    #    self._vmin = m
    #    self._vmax = x
    #    self._desc = d
    _args: list[str]
    _name: str = 'member'
    _value: float = 0.0
    _vmin: float = 0.0
    _vmax: float = 0.0
    _desc: str = 'I am a member'

    def add_arg(self, parser):
        print(f'{self._args=}')
        parser.add_argument(*self._args,
                            type=self.getlf(),
                            #default=100.0,
                            default=self._value,
                            help=self._help)
    @staticmethod
    def limit(vs, vmin, vmax):
        v = float(vs)
        if (v >= vmin) and (v <= vmax):
            return v
        raise argparse.ArgumentTypeError(f'{v} not in [{vmin}, {vmax}]')
    def getlf(self):
        def blimit(vs):
            print(f'G:{self=}')
            return Member.limit(vs, self._vmin, self._vmax)
        return blimit

@dataclass
class BMember(Member):
    _args: list[str] = field(default_factory=list)
    _name: str = 'bmember'
    _value: float = 0.5
    _vmin: float = 0.0
    _vmax: float = 1.0
    _desc: str = 'I am a Bmember'
    def __init__(self, v=0.5):
        self._args = ['--b', '-b']
        super().__init__(*asdict(self))
        print(f'B:{self=}')

@dataclass
class CMember(Member):
    _args: list[str]
    _name: str = 'c'
    _value: float = 10.0
    _vmin: float = 0.0
    _vmax: float = 100.0
    _desc: float = 'I am a C'
    def __init__(self, v=0.5):
        super().__init__(['--cc', '-c'])
        print(f'C:{self=}')


#def bmember(arg):
#    print(f'bm {arg}')
#    b = BMember(float(arg))
#    print(f'xb {b}')
#    return b

@dataclass
class Data:
    bin_format: str = '!BBfff'
    version: int = 3
    flag: bool = False
    a: Member = (float, 'd0', 1.0, 0.0, 2.0, 'a member')
    b: Member = (float, 'd1', 1.5, 0.1, 2.0, 'b member')
    c: float = 100.0

def add_data(data_parser):
    data_parser.add_argument('-f',
                               action='store_true',
                               help='set flag on')
    data_parser.add_argument('-a',
                               type=_a_limit,
                               default=100.0,
                               help='set a, default: %(default)s')
    #data_parser.add_argument(*['--b', '-b'],
    #                           type=bmember,
    #                           default=1.0,
    #                           help='set b, default: %(default)s')
    #data_parser.add_argument('-c',
    #                           #type=_limit(-23.7, 100.0),
    #                           default=100.0,
    #                           help='set a, default: %(default)s')
    data_parser.set_defaults(func=data_handler)

def set_data(data):
    buf += struct.pack(data.bin_format,
                       5,
                       data.version,
                       int(data.flag),
                       data.a,
                       data.b,
                       data.c)
    print(f'{data} = > {buf}')

def data_handler(args):
    if args.f:
        print(args.f)
    if args.a:
        print(args.a)
    if args.b:
        
        print(args.b)

@dataclass
class CData(Data):
    y: int = 377

d = Data()
cd = CData()
#print(f'{type(d.a)=}')
ddict = asdict(d)
#print(f'{ddict=}')
cddict = asdict(cd)
#print(f'{cddict=}')
#sys.exit()
parser = argparse.ArgumentParser()
add_data(parser)
b = BMember()
b._args = ['--b', '-b']
print(f'{b=}')
bdict = asdict(b)
print(f'{bdict=}')
b.add_arg(parser)
c = CMember(float)
print(f'c:{asdict(c)}=')
c.add_arg(parser)
args = parser.parse_args()
args.func(args)
x = Data()
print(f'{x=}')

