#!/usr/bin/env python3
#

""" editing-bakup
    Reads file list from stdin and copies files to backup location.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import os
import os.path
import shutil
import sys

import filefilter as ff



'''
_PAST_DEPOT = 8

in_file_path = args.get_arg('f', '-')
#print in_file_path
depot = bool(args.get_arg('depot', False))
verbose = bool(args.get_arg('verbose', False))
cwd_is_dev_base = bool(args.get_arg('dev_base', False))
storage_root = args.get_arg('storage_path', None)
if not storage_root:
    tpath = os.getenv('EDIT_BACKUP_PATH')
    #print "tpath: %s" % tpath
    if not tpath:
        tpath = os.getenv('TMP')
    if not tpath:
        tpath = '/tmp'
    storage_root = tpath

if in_file_path == '-':
    in_file = sys.stdin
else:
    in_file = open(in_file_path)

if cwd_is_dev_base:
    dev_base = os.getcwd()
else:
    dev_base = os.getenv('DEV_BASE')
    if not dev_base:
        print('Error: DEV_BASE must be set in environment or must use -dev_base option')
        print(usage)
        sys.exit(2)

#print "verbose: %s" % verbose
if verbose:
    print("dev_base: %s" % dev_base)
    print("depot: %s" % depot)

path_list = []
for path in in_file:
    if path and len(path) > 0:
        #print "pre-path: %s" % path.strip()
        if depot:
            path = path[_PAST_DEPOT:path.find('#')]
        path_list.append(path.strip())
        #print "post-path: %s" % path.strip()


print("storage-root: %s" % storage_root)
if verbose:
    print("path list:%s" % path_list)
for path in path_list:
    storage_path = os.path.join(storage_root, path)
    #src_path = os.path.join('..', path)
    src_path = path
    storage_dir = os.path.dirname(storage_path)
    if verbose:
        print("storage_dir:%s" % storage_dir)
    if not os.path.exists(storage_dir):
        os.makedirs(storage_dir)
    if verbose:
        print("cp:%s to %s" % (src_path, storage_path))
    if os.path.exists(src_path):
        try:
            shutil.copyfile(src_path, storage_path)
        except IOError:
            if not os.path.isfile(src_path):
                continue
            raise
    else:
        print("%s: does not exist" % src_path)
'''

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='backup files from a file list')
    parser.add_argument('file_list',
                        type=str,
                        nargs='+',
                        help=f'gets a file-list file')
    home = os.getenv('HOME')
    parser.add_argument('--dest_dir', '-d',
                        type=str,
                        default=os.getenv('EBACKUP_DIR', f'{home}/tmp/eback'),
                        help=f'destination directory, defaults: %(default)s')
    parser.add_argument('--verbose', '-v',
                        action='store_true')

    args = parser.parse_args()

    def vprint(*params):
        if args.verbose:
            print(*params)

    flist = []
    for fl in args.file_list:
        flist += ff.ReadFile(fl, xform = lambda fval, n : fval.rstrip())

    cwd = os.getcwd()
    bcwd = os.path.relpath(cwd, '..')
    for fp in flist:
        vprint(f'{fp=}')
        frp = os.path.relpath(fp, cwd)
        vprint(f'{frp=}')
        dd = f'{args.dest_dir}/{bcwd}'
        dest_file = os.path.join(dd, frp)
        vprint(f'{dest_file=}')
        storage_dir = os.path.dirname(dest_file)
        vprint(f'{storage_dir=}')
        os.makedirs(storage_dir, exist_ok=True)
        vprint(f'copy {frp} -> {dest_file=}')
        shutil.copyfile(frp, dest_file)
    
    


