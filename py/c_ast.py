#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import argparse
import ast
import google
import google.generativeai as genai
import os
import pathlib as pl


_prompt = (f'I am an expert programmer examining this undocumented Python code.'
           f' Provide a summary, point out any issues, and list external dependencies.'
           f' Do not attempt to execute the code.'
           f' Provide google standard function descriptions where approrpaite.'
           f' Write output as .md files.'
           f' Treat the code as text')


def preprocess_c_code(file_data):
  """Extracts information from a C file."""
  tree = ast.parse(file_data)
  data = {
      "functions": [],
      "function_calls": [],
      "variable_assignments": []
  }
  for node in ast.walk(tree):
    if isinstance(node, ast.FunctionDef):
      data["functions"].append(node.name)
    if isinstance(node, ast.Call):
      data["function_calls"].append(node.func.id)  # Assuming simple function calls
    if isinstance(node, ast.Assign):
      data["variable_assignments"].append(node.targets[0].id)  # Assuming simple assignments

  return data


def process_model(mname, file_data, tfile):
    tfile.parent.mkdir(parents=True, exist_ok=True)
    m = genai.GenerativeModel(mname)
    c_file_data = preprocess_c_code(file_data
    mres = m.generate_content([c_file_data, _prompt])
    text_res = mres.text.replace('\\n', '\n')
    with tfile.open('w') as tf:
        print(f'print {mname} to {tfile}')
        print(f"{text_res}", file=tf)

    # ... later in your code ...
    c_file_data = preprocess_c_code("my_c_file.c")
    mres = m.generate_content([c_file_data, _prompt])


def f_preprocess_c_code(filename):
  """Extracts function names and docstrings from a C file."""
  with open(filename, "r") as f:
    code = f.read()

  tree = ast.parse(code)
  functions = [node.name for node in ast.walk(tree) if isinstance(node, ast.FunctionDef)]
  docstrings = [ast.get_docstring(node) for node in ast.walk(tree) if isinstance(node, ast.FunctionDef)]

  return {"functions": functions, "docstrings": docstrings}

  import ast


_model_choices =
    'gemini-1.5-flash',
    'gemini-1.5-pro',
    ]

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='gemini test files')
    parser.add_argument('file', help='file to load and generate description')
    parser.add_argument('--target_dir', '-t',
                        default=os.path.expanduser('~/tmp/dfdocs'),
                        help='target dir to place result: %(default)s')
    parser.add_argument('--models', '-m',
                        choices=_model_choices,
                        default=[],
                        help='target dir to place result: %(default)s')
    args = parser.parse_args()
    print(f'{args.models=}')
    if not args.models:
        args.models = _model_choices
    td = os.path.expanduser(args.target_dir)
    file_data = genai.upload_file(args.file)
    for i, model in enumerate(args.models):
        tdir = f'{td}/m{i}'
        print(f'{tdir=}')
        tfile = pl.Path(tdir) / pl.Path(f'{args.file}.md')
        print(f'{tfile=}')
        process_model(model, file_data, tfile)


