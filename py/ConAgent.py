#!/usr/bin/env python3
"""
Very simple HTTP server as connection-agent
Usage::
    connection_agent.py [<port>]
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""
import argparse
import json
import logging
import queue
import time
import threading
from http.server import BaseHTTPRequestHandler, HTTPServer

"""
Overview: the purpose of the connection agent to connect two programs,
e.,g., MR and DC, or DC and LE, with data and buffering to allow the 
two programs to having jitter in their run rates and as long as the 
average consumption rate exceeds the projection rate.
The two programs access the server on the sending and receiving sides.
This program, connection_agent.py, is responsible for receiving data in
(only in the form of file-paths*) and handing them to the GET interface
when called.
* we should be able to extend this to arbitrary data, but initially we 
use file paths.
"""

_root = '/tmp'
_max_post_items = -1 # set positive to limit posts
_seq = 1
_postq = queue.Queue(_max_post_items)

"""
class S(erver) implements a simple HTTP model that allows GETs and POSTs.
TODO(epr): Extend the class the allow custom extention of JSON data.
TODO(epr): extend to allow independence of location of data.
"""
class S(BaseHTTPRequestHandler):
    def _set_response(self):
        """ handles any boiler plate
        """
        self.send_response(200)
        self.send_header('Content-type', 'text/json')
        self.end_headers()

    def do_GET(self):
        """
        Called from the handler.
        It has been extended to have a sequence number, and to package
        system in JSON.
        """
        global _seq
        #logging.info('GET', _postq.empty())
        opath = _postq.get()
        of_path = '%s/%s' % (_root, opath)
        logging.info("%d: GET request,\nPath: %s\nHeaders:\n%s\n",
                     _seq,
                     str(self.path),
                     str(self.headers))
        self._set_response()
        ofile_content = {'seq' : _seq, 'outfile' : of_path}
        _seq += 1
        ofile_json = json.dumps(ofile_content)
        logging.info('Msg: %s' % ofile_json)
        self.wfile.write(ofile_json.encode('utf-8'))

    def do_POST(self):
        logging.info('POST')
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        body = post_data.decode('utf-8')
        logging.info("Path: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                str(self.path), str(self.headers), body)
        ##TODO(epr): handle full error here.
        ##if _postq.full():
        ##    # send error response
        _postq.put(body)
        self._set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))

# define the default port here, but if code is called from another module
# this might not be useful.
_default_port = 3773

def run(server_class=HTTPServer, handler_class=S, port=_default_port):
    """
    run launches the logging and the http-server
    """
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping httpd...\n')

def runget(port):
    # run the get services
    run(port=port)

def runpost(port):
    # run the post services
    run(port=port)

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='connection-agent')
    parser.add_argument('--port',
                        type=int,
                        default=_default_port,
                        help='GET port, POST port @ +1,'
                             'default: %(default)s')
    parser.add_argument('--root',
                        type=str,
                        default=None,
                        help='The root of all outfiles')
    args = parser.parse_args()

    if args.root:
        _root = args.root

    # We create two theads, one each for POST and GET.
    # This is required so that the POST and GET don't block
    # each other. The concurncy is secondary.
    # The _postq interfaces between these two threads.
    get_thread = threading.Thread(target=runget,
                                  name='get',
                                  daemon=True,
                                  args=(args.port,))
    post_thread = threading.Thread(target=runpost,
                                   name='post',
                                   daemon=True,
                                   args=(args.port + 1,))
    get_thread.start()
    post_thread.start()
    post_thread.join()
    get_thread.join()

