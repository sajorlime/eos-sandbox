#!/usr/bin/env python3
#
#

"""
    tree.py prints the tree, and provides methods of executingg
    and filtering on the tree.
    
    See usage.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import argparse
import os
import os.path
import re
import sys
import subprocess
import textwrap

import regexfilter

class AbstractList:
    """
        class for holding instances of lists, queues, etc.
        that have the same semantics
    """
    def __init__(self, alist):
        self._alist = alist

    def put(self, item):
        self._alist.append(item)

    def get(self):
        return None

    def iterate(self):
        return iter(self._alist)

    def __str__(self):
        return f'<AbstractList:{self._alist}>'

class AList(AbstractList):
    def __init__(self, alist=None):
        if alist == None:
            alist = []
        AbstractList.__init__(self, alist)

class ItemHandler(AbstractList):
    """
    Base class for passing to get_flist
    """
    def __init__(self, root, alist=None):
        AList.__init__(self, alist)
        self._root = root

    def accept(self, item):
        return True

    def append(self, item):
        self.put(item)

    def items(self):
        return self.iterate()

    def terminate(self):
        pass

    def __str__(self):
        return '<ItemHandler:{root:%s, %s}>' % (self._root, AbstractList.__str__(self))

class TreeItemHandler(ItemHandler):
    def __init__(self, root, filters, alist=[]):
        ItemHandler.__init__(self, root, alist)
        self._filters = filters
        self._iiter = None

    def accept(self, item):
        #print('at:', regexfilter.ftest(item, self._filters))
        if regexfilter.ftest(item, self._filters):
            #print('APPEND')
            self.append(item)

    def append(self, item):
        self.put(item)

    def ptest(self, path):
        """
        tests if path is in the rejection filters.
        Args:
            path: a string, but typically a file path
        Returns:
            True iff path is in the "False" filter
        """
        return regexfilter.ptest(path, self._filters)

    def __str__(self):
        fl = ', '.join([ '%s' % f for f in self._filters ])
        return 'TreeItemHandler:{\n\t[%s], \n\t%s, \n\t%s}' % (fl,
                                                               self._iiter,
                                                               ItemHandler.__str__(self))


def tree_files(tree_root, handler=None, accept_list=None, reject_list=None):
    """
    Generate a file list of files in the tree_root
    Args:
        tree_root: the starting directory 
        filters: a RegexFilter list
        handler: object has accept method, if not null ignore accept/reject
        accept_list: a list of strings that select files in subdirectory
        reject_list: a list of strings that select reject in subdirectory
    Returns:
        a filtered list of files in the tree
    """ 
    if not handler:
        afilters = [ regexfilter.SRegexFilter('+%s' % accept) for accept in accept_list ]
        rfilters = [ regexfilter.SRegexFilter('-%s' % reject) for reject in reject_list ]
        handler = TreeItemHandler(tree_root, afilters + rfilters)
        #print(handler)
    for root, dirs, files in os.walk(tree_root, topdown=True):
        #print('root: %s ' % root, 'len(files): %s' % len(files), 'in dirs: %s' % dirs)
        #for d in dirs:
        #   #print('T:', d,handler.ptest('/' + d), d)
        #    if not handler.ptest('/' + d):
        #       #print('X', d,handler.ptest('/' + d))
        dirs[:] = [d for d in dirs if handler.ptest('/' + d)]
        #print('root: %s ' % root, 'out dirs: %s' % dirs, files)
        for file in files:
            full_path = os.path.join(root, file)
            handler.accept(full_path)
    handler.terminate()
    return handler


def tree_dirs(tree_root, handler):
    '''
    Generate a file list of files in the tree_root
    Args:
        tree_root: the starting directory 
        handler: object has accept method
    Returns:
        a filtered list of dirs in the tree
    Note(epr): this does not work correctly
    '''
    for root, dirs, files in os.walk(tree_root, topdown=True):
        print(f'walk: {dirs=}')
        dirs = [d for d in dirs if not handler.ptest(d)]
        print(f'ptest: {dirs=}')
        if len(files) > 0:
            handler.accept([root, files])
    handler.terminate()
    return handler


def tree_print(directory, indent='', ichar=' '):
    """
    """
    #print '%s%s' % (indent, directory)
    for root, dirs, files in os.walk(directory):
        indent = indent + ichar
        for file in files:
            print('%s  %s' % (indent, file))
        for dir in dirs:
            print('%s- %s/' % (indent, dir))


#            '-s', myargs.vsize,           # output video size
def tree_action(action, dpath):
    args = [action, dpath]
    #print('%s %s' % (args[0], args[1]))
    mif = subprocess.Popen(args,
                         stdout = subprocess.PIPE,
                         stderr = subprocess.PIPE)
    out, err = mif.communicate()
    if mif.returncode:
        raise Exception("ffmpeg failed code %d out\n%s\nerr\n%s" % (mif.returncode, out, err))
    return out.decode('utf-8')


if __name__=='__main__':
    _epilog = textwrap.dedent("""\
        Example filter specs
        --filter=+\\.txt\\\\Z -- requires that all matches end in .txt.
        --filter=-Foo -- excludes file paths that contain 'Foo'
        --filter=+Albums --filter=-m3u8 -- includes files that
                                           have "Albums" and excludes
                                           any with "m3u8"
    """)

    parser = argparse.ArgumentParser(description='display a directory tree', 
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog=_epilog)

    parser.add_argument('--filter', '-f',
                        dest='filters',
                        action='append',
                        type=regexfilter.SRegexFilter,
                        default=[],
                        help='[+-]regx +=accept/-=reject entry that matches this regex')
    parser.add_argument(dest='dir',
                        nargs='?',
                        type=str, default='.',
                        help='the directory that we will operate on, default %(default)s')
    parser.add_argument('--show_tree',
                        action='store_true',
                        help='show the tree, default %(default)s')
    parser.add_argument('--show_dirs', '-d',
                        action='store_true',
                        help='show the dirs, default %(default)s')
    parser.add_argument('--action', '-a',
                        type=str,
                        help='execute an action for each file file')
    parser.add_argument('--match', '-m',
                        dest="match",
                        action='append',
                        type=str,
                        default=[],
                        help='match string for finding config files, default: %(default)s')
    parser.add_argument('--reject', '-r',
                        dest="reject",
                        action='append',
                        type=str,
                        default=[],
                        help='reject string for finding config files, default: %(default)s')
    args = parser.parse_args()

    if args.show_tree:
        tree_print(args.dir)
        sys.exit(0)

    #for f in args.filters:
    #    print(f._mode, f.pattern())

    #if not args.filters:
    #    tih =  tree_files(args.dir, accept_list=args.match, reject_list=args.reject)
    #    #print(tih)
    #    tfl = [ f for f in tih.items() ]
    #    #print(tfl)
    #    sys.exit(0)

    tih = TreeItemHandler(args.dir, args.filters)
    #print(tih)

    #print(args.show_dirs, args.dir, args.filters, args.action)
    if args.show_dirs:
        tree_dirs(args.dir, tih)
        sys.exit(0)

    #tih = TreeItemHandler(args.dir, args.filters)
    tree_files(args.dir, handler=tih)
    if args.action:
        for file in list(tih.items()):
            print(tree_action(args.action, file), end='')
    else:
        for file in list(tih.items()):
            print(file)

