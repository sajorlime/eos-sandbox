#!/usr/bin/env python3
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

""" hreduce.py
    Takes a list of *-history files and removes the shit I know I don't want
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import os
import sys

import filefilter as ff
import regexfilter

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='remove string from history')
    parser.add_argument('hfile',
                        nargs='*',
                        help='history files to filter')
    parser.add_argument('--remove', '-r',
                        #nargs='+',
                        action='append',
                        help='remove from files this string')
    parser.add_argument('--hrtargets', '-t',
                        default=os.getenv('HRTARGETS',
                                           '~/bash/history/hrtargets.txt'),
                        help=('list of removal targets default: %(default)s'
                              ' use $HRTARGETS to set'))
    args = parser.parse_args()
    #print(f'{args.remove=}')
    hrtl = ff.ReadFile(args.hrtargets, strip='\n')\
            + (args.remove if args.remove else [])
    #print(f'{hrtl=}')
    fval = '|'.join(hrtl)
    #print(f'{fval=}')
    filterex = regexfilter.SRegexFilter(fval)
    def interest(line):
        return fcomp.find(line)
    for hfile in args.hfile:
        #print(hfile)
        flines = ff.ReadFile(hfile, interest=filterex.accept, strip='\n')
        for l in flines:
            print(l.strip())


