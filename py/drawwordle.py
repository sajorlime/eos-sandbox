#!/usr/bin/python3

import sys

wordle_matrix = []
def buildwordle():
    print('[0;0H', flush=True)
    print('[2k', end='', flush=True)
    print('[2J', end='', flush=True)
    print('[8;14r', end='', flush=True)
    for i in range(6):
        wline = [ c for c in '......' ]
        wordle_matrix.append(wline)

def drawwordle():
    row = 1
    print('[%s;0H' % row, end='', flush=True)
    for l in wordle_matrix:
        for c in l:
            print(' %1s' % c, end='', flush=True)
        row += 1
        print('[%s;0H' % row, end='', flush=True)

    print('[14;2H', end='', flush=True)
    print('enter: n m x: ', end='', flush=True)

nocolor = '[0m'
green = '[32;1m'
yellow = '[33;1m'

if __name__ == "__main__":
    buildwordle()
    while True:
        drawwordle()
        sin = sys.stdin.readline()
        sinl = sin.split()
        if len(sinl) < 3:
            if sinl[0] == 'q':
                print('[r', end='', flush=True)
                print('[15;0H', end='', flush=True)
                break;
        color = nocolor
        if len(sinl) == 4:
            if sinl[3] == 'y':
                color = yellow
            elif sinl[3] == 'g':
                color = green

        #print(sinl, type(sinl))
        #sys.exit(1)
        row = int(sinl[0])
        col = int(sinl[1])
        x = sinl[2]
        wordle_matrix[row][col] = '%s%s%s' % (color, x, nocolor)
        #print(wordle_matrix)
        #sys.stdin.meadline()
        

