#!/usr/bin/env python3
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import os
import args

import string
import cgi
import time
import os

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from CGIHTTPServer  import CGIHTTPRequestHandler

#os.chdir("/home/httpd/html")                           # run in html root dir
os.chdir("/home/emilio/tmp/http")                           # run in html root dir
#srvraddr = ("", 8000)                                    # my hostname, portnumber
#srvrobj  = HTTPServer(srvraddr, CGIHTTPRequestHandler)
#srvrobj.serve_forever()                                # run as perpetual demon



class MyRequestHandler(CGIHTTPRequestHandler):
    pass



#class MyServer(HTTPServer):
class MyServer(BaseHTTPRequestHandler):

    def do_GET(self_):
        try:
            print('client_address: %s, %d' % (self_.client_address[0], self_.client_address[1]))
            print('command: %s' % self_.command)
            print('path: %s' % self_.path)
            print('request_version: %s' % self_.request_version)
            print('headers: %s' % self_.headers)
            print('rfile: %s' % self_.rfile)
            print('wfile: %s' % self_.wfile)
            ct =  self_.headers.getheader('content-type')
            if ct:
                print('content-type: %s' % ct)
            else:
                print('content-type is missing')
            ka =  self_.headers.getheader('keep-alive')
            if ka:
                print('keep-alive: %s' % ka)
            else:
                print('keep-alive is missing')
            if self_.path.endswith(".html"):
                #super(MyServer, self_).doGet()
                html_handler.do_GET(self_)
                #HTTPServer.do_GET(self_)
                #f = open(os.curdir + os.sep + self_.path) #self_.path has /test.html

                #self_.send_response(200)
                #self_.send_header('Content-type',	'text/html')
                #self_.end_headers()
                #self_.wfile.write(f.read())
                #f.close()
                return
            self_.send_response(200)
            self_.send_header('Content-type',	'text/html')
            self_.end_headers()
            self_.wfile.write("hey, today is the" + str(time.localtime()[7]))
            self_.wfile.write(" day in the year " + str(time.localtime()[0]))
            self_.wfile.write("hey, today is the" + str(time.localtime()[7]))
                
            return
                
        except IOError:
            self_.send_error(404,'File Not Found: %s' % self_.path)
     

    def do_POST(self_):
        global rootnode
        try:
            print('client_address: %s, %d' % (self_.client_address[0], self_.client_address[1]))
            print('command: %s' % self_.command)
            print('path: %s' % self_.path)
            print('request_version: %s' % self_.request_version)
            print('headers: %s' % self_.headers)
            print('rfile: %s' % self_.rfile)
            print('wfile: %s' % self_.wfile)
            ct =  self_.headers.getheader('content-type')
            if ct:
                print('content-type: %s' % ct)
            else:
                print('content-type is missing')
            ka =  self_.headers.getheader('keep-alive')
            if ka:
                print('keep-alive: %s' % ka)
            else:
                print('keep-alive is missing')

            #ctype, pdict = cgi.parse_header(self_.headers.getheader('content-type'))
            #if ctype == 'multipart/form-data':
            #    query=cgi.parse_multipart(self_.rfile, pdict)
            self_.send_response(301)
            
            self_.end_headers()
            #upfilecontent = query.get('upfile')
            #print("filecontent", upfilecontent[0])
            #self_.wfile.write("<HTML>POST OK.<BR><BR>");
            #self_.wfile.write(upfilecontent[0]);
            print(' read contents')
            for contents in self_.rfile:
                print('contents: %s' % contents)
            
        except :
            pass


    @staticmethod
    def serve_forever(port):
        HTTPServer(('', port), MyServer).serve_forever()

if __name__ == "__main__":
    args = args.Args()
    PORT = args.get_arg('port', 3773)
    MyServer.serve_forever(PORT)

