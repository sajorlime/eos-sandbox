#!/usr/bin/env python3

from common import *

if __name__ == '__main__':
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.bind((socket.gethostname(), PORT))
    serversocket.listen(5)

    # accept connections from outside
    (clientsocket, address) = serversocket.accept()
    frame_counter = 0

    while True:

        # get frame and the buffer frame, 8 bytes in float64
        data = recvall(clientsocket, 2 * size_temp * 8)

        recv_signal = np.frombuffer(data, dtype=np.float64)
        if (is_server_processing):
            processed_signal = np.convolve(recv_signal, matched_filter)
            peak_value = np.amax(np.absolute(processed_signal))
            print("Frame counter: ", frame_counter, ", peak: ", peak_value)
            if (peak_value > THRES):
                print("Transmission detected in frame counter: ", frame_counter)
        frame_counter = frame_counter + 2

