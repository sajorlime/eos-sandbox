#!/usr/bin/env python
#

"""test program
   UDP client/server
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'erojas@gmail.com (Emilio Rojas)'

import argparse
import os
import os.path
import threading
import shutil
import socket
import sys


UDP_IP = "127.0.0.1"
UDP_PORT = 5005
MESSAGE = "Hello, World!"

_msglist = []
def putmsg(msg):
    global _msglist
    _msglist.append(msg)


def udp_send(arg=None):
    #print 'SEND'
    sock = socket.socket(socket.AF_INET, # Internet
                         socket.SOCK_DGRAM) # UDP
    if not arg:
        arg = sys.stdin
    with arg as src:
        while True:
            #print 'input:',
            msg = src.readline()
            sock.sendto(msg, (UDP_IP, UDP_PORT))
            if 'quit' in msg:
                sock.close()
                sys.exit(0)


def udp_recv(arg=None):
    sock = socket.socket(socket.AF_INET, # Internet
                         socket.SOCK_DGRAM) # UDP
    sock.bind((UDP_IP, UDP_PORT))

    putmsg('top')
    while True:
        putmsg('get')
        msg, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
        putmsg(msg)
        if 'quit' in msg:
            putmsg('quit')
            sock.close()
            sys.exit()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='explore a compact dictionary')
    parser.add_argument('--receiver', '-r',
                        dest='recv',
                        action='store_true',
                        default=False,
                        help='I seek to receive')
    parser.add_argument('--threads', '-t',
                        dest='threads',
                        action='store_true',
                        default=False,
                        help='use threads')
    opts = parser.parse_args()

    if  opts.threads:
        print 'make threads'
        recvt = threading.Thread(target=udp_recv, name='rcvt', args=())
        sendt = threading.Thread(target=udp_send, name='send', args=())

        print "UDP target IP:", UDP_IP
        print "UDP target port:", UDP_PORT
        print 'start thread recvt '
        recvt.start()
        print 'start thread sendt '
        sendt.start()
        recvt.join()
        sendt.join()
        
        for m in _msglist:
            print m,
        sys.exit(0)
    if  opts.recv:
        print "UDP target IP:", UDP_IP
        print "UDP target port:", UDP_PORT
        udp_recv()

    udp_send()

