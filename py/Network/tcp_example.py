#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

"""test program
   TCP client/server
"""

__author__ = 'erojas@gmail.com (Emilio Rojas)'

import argparse
import os
import os.path
import threading
import time
import shutil
import socket
import socketserver
import sys


TCP_HOST = "localhost"
TCP_PORT = 3773

class TCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """
    def handle(self):
        # self.request is the TCP socket connected to the client
        self.data = self.request.recv(1024).strip()
        print("{} wrote:".format(self.client_address[0]))
        print(self.data)
        # just send back the same data, but upper-cased
        self.request.sendall(self.data.upper())

ext_fileh = sys.stdin

class FileTCPHandler(socketserver.StreamRequestHandler):
    #def __init__(self, istream):
    #    socketserver.StreamRequestHandler(self)
    #    self._istream = istream

    def handle(self):
        # self.rfile is a file-like object created by the handler;
        # we can now use e.g. readline() instead of raw recv() calls
        self.connection.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        print(self.connection)
        while True:
            #aline = ext_fileh.readline()
            aline = "abcdefg"
            if not aline:
                aline = 'quit'
            print("{} wrote:".format(self.client_address[0]))
            print(aline)
            # Likewise, self.wfile is a file-like object used to write back
            # to the client
            self.wfile.write(aline.encode())
            if aline == 'quit':
                sys.exit(0)
            time.sleep(0.100)

def tcp_send(istream, host=TCP_HOST, port=TCP_PORT):
    # Create the server, binding to localhost on port 9999
    with socketserver.TCPServer((host, port), FileTCPHandler) as server:
        # Activate the server; this will keep running until you
        # interrupt the program with Ctrl-C
        server.serve_forever()

_msglist = []
def putmsg(msg):
    global _msglist
    _msglist.append(msg)


# not using
def tcp_client(arg=None):
    hostname, sld, tld, port = 'www', 'integralist', 'co.uk', 80
    target = '{}.{}.{}'.format(hostname, sld, tld)
    # create an ipv4 (AF_INET) socket object using the tcp protocol (SOCK_STREAM)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # connect the client
    # client.connect((target, port))
    client.connect(('0.0.0.0', 9999))
    # send some data (in this case a HTTP GET request)
    client.send('GET /index.html HTTP/1.1\r\nHost: {}.{}\r\n\r\n'.format(sld, tld))
    # recieve the response data (4096 is recommended buffer size)
    response = client.recv(4096)
    print(response)

def tcp_recv(host=TCP_HOST, port=TCP_PORT):
    """
    Create a socket TCP/IP SOCK_STREAM
    Args:
    """
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        # Connect to server and send data
        sock.connect((host, port))
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        #mfh = sock.makefile()

        while True:
            #recieved = mfh.readline()
            recieved = sock.recv(4096)
            if not recieved:
                print('NULL rcv')
                break
            print("rcvd: {}".format(str(recieved)))
            #break

def tcp_read(host=TCP_HOST, port=TCP_PORT):
    """
    Create a socket TCP/IP SOCK_STREAM
    Args:
    """
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        # Connect to server and send data
        sock.connect((host, port))
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mfh = sock.makefile()

        while True:
            recieved = mfh.readline()
            #recieved = sock.recv(4096)
            if not recieved:
                print('NULL rcv')
                break
            print("rcvd: {}".format(str(recieved)))
            #break


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='test TCP program')
    parser.add_argument('--reciever', '-r',
                        dest='recv',
                        action='store_true',
                        default=False,
                        help='I seek to recieve')
    parser.add_argument('--host', '-H',
                        dest='host',
                        type=str,
                        default=TCP_HOST,
                        help = 'provide host, default: %(default)s')
    parser.add_argument('--port', '-p',
                        dest='port',
                        type=int,
                        default=TCP_PORT,
                        help='provide port, default: %(default)s')
    parser.add_argument('--threads', '-t',
                        dest='threads',
                        action='store_true',
                        default=False,
                        help='use threads')
    opts = parser.parse_args()

    if  opts.threads and False:
        print('make threads')
        recvt = threading.Thread(target=tcp_read, name='readt', args=())
        sendt = threading.Thread(target=tcp_send, name='send', args=())

        print("TCP target IP:", TCP_HOST)
        print("TCP target port:", TCP_PORT)
        print('start thread recvt ')
        recvt.start()
        print('start thread sendt ')
        sendt.start()
        recvt.join()
        sendt.join()
        
        for m in _msglist:
            print(m,)
        sys.exit(0)

    if  opts.recv:
        print("TCP target IP:", TCP_HOST)
        print("TCP target port:", TCP_PORT)
        tcp_read()
        sys.exit(0)

    tcp_send(sys.stdin)
    sys.exit(0)

