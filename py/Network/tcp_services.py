#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

""" TCP/IP services

"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import asyncio
import datetime
import json
from json import JSONEncoder
import queue
import socket
import sys
import time
import zmq
from zmq.asyncio import Context

#TODO(epr): move other tcp/ip services here, should this be called comm_services?

_loc_queue = None

def location_put(loc):
    """
    Send simple locations into the ether
    Args:
        loc: a dictionary wth {'t': <src>, 'n':<id>, 'x': <float>, 'y':<float>}
    """
    global _loc_queue
    if _loc_queue:
        _loc_queue.put(json.dumps(loc))
    with open('TeamPos.txt', 'a') as ph:
        print(json.dumps(loc), file=ph)
    # drop it on the floor if no reader.

def locations_service(port):
    """
    A signleton location service (TODO(epr): consider making in a handler
    Args:
        port: the port number to use
    """
    global _loc_queue
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        print('TLS:', port, file=sys.stderr)
        s.bind(('localhost', port))
        s.listen()
        conn, addr = s.accept()
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        print('TLS Connected:', port, file=sys.stderr)
        _loc_queue = queue.Queue()
        print('TLS qis:', _loc_queue, file=sys.stderr)
        with conn:
            while True:
                data = _loc_queue.get()
                #print('data:', data)
                conn.sendall(data.encode('utf-8'))
                if data == 'QUIT':
                    break


