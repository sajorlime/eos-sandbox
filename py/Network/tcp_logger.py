#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

"""
   tcp_logger.py is a proxy logger.
   It runs on localhost and proxies an address of a server on the network.

"""

__author__ = 'erojas@gmail.com (Emilio Rojas)'

import argparse
import os
import os.path
import threading
import time
import shutil
import socket
import socketserver
import sys


PROXY_HOST = "localhost"
PROXY_PORT = 10001
_dewetron = "localhost"


def proxy_server(host, port, rhost, rport):
    ''' create a server that opens a client port and a server port.
        When it recieve something on either something client port
        it is the man in the middle
    Args:
        host: string name of host, should be localhost
        port: int port# on host 
        rhost: string name of host, could be localhost for testing
        rport: int port# on rhost, can't be port for testing
    '''
    # create the client socket
    cs = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    cs.connect((rhost, rport))
    cs.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # now act as a server the forwards and reads response
    # problem: not all commands have a response. So, first approach
    # make it smart, just like the dewetron code where only queries have
    # resposes. i.e., message in in '?'
    # The communication with the proxy endpoint is always inititated
    # by the client
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((host, port))
            s.listen()
            conn, addr = s.accept()
            with conn:
                print(f'{addr} connected')
                while True:
                    try :
                        # get client command
                        print(f'recv from client')
                        cdata = conn.recv(4096)
                    except ConnectionAbortedError as cae:
                        print(f'{cae}')
                        break;
                    print(f'ps-rcvd: {cdata}')
                    # OK we got something, so send it to the endpoint
                    cs.sendall(cdata)
                    if b'?' in cdata:
                        # if it is a query we wait for the reply
                        print(f'{cdata} is a query')
                        rdata = cs.recv(4096)
                        conn.sendall(rdata)
                        continue
                    # otherwise we back for the next mesage
                    if not cdata:
                        # exit when we send empty data
                        break;
                    continue
    print('proxy_server leaving')

def echoy_server(host, port):
    ''' echos input in upper case.
    '''
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((host, port))
        s.listen()
        conn, addr = s.accept()
        with conn:
            print(f'{addr} connected')
            while True:
                try :
                    rdata = conn.recv(4096)
                except ConnectionAbortedError as cae:
                    print(f'{cae}')
                    break;
                print(f'es-rcvd: {rdata}')
                if not rdata:
                    break;
                if b'?' in rdata:
                    conn.sendall(rdata.upper())
    print('echoy_server leaving')


def tcp_send(host, port):
    """ Connect to a server at (host, port) and get CLI and send to the
        sever and read the response.
    Args:
    """
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        # Connect to server and send data
        sock.connect((host, port))
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        #mfh = sock.makefile()

        while True:
            to_send = sys.stdin.readline().strip()
            if not to_send:
                print('NULL rcv')
                break
            print(f'send: {to_send}')
            sock.sendall(to_send.encode('utf-8'))
            if '?' in to_send:
                rcv_back = sock.recv(4096)
                print(f'rback: {rcv_back}')
            #break


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='test TCP program')
    #parser.add_argument('--ipaddr', '-a',
    #                    default=os.getenv('DEWE_IP_ADDR', _dewetron),
    #                    help='IP addrs of the Dewetron, default: %(default)s')
    parser.add_argument('--port', '-p',
                        default=PROXY_PORT,
                        help='port# of the Dewetron, default: %(default)s')
    parser.add_argument('--host', '-H',
                        default=PROXY_HOST,
                        help = 'provide host, default: %(default)s')
    parser.add_argument('--rport', '-r',
                        default=PROXY_PORT + 1,
                        help='port# of the Dewetron, default: %(default)s')
    parser.add_argument('--rhost', '-R',
                        default=PROXY_HOST,
                        help = 'provide host, default: %(default)s')
    opts = parser.parse_args()

    print('make threads')

    echoyt = threading.Thread(target=echoy_server,
                             name='echoy',
                             args=(opts.rhost, opts.rport,))
    proxyt = threading.Thread(target=proxy_server,
                             name='proxy',
                             args=(opts.host, opts.port, opts.rhost, opts.rport,))
    sendt = threading.Thread(target=tcp_send,
                             name='send-it',
                             args=(opts.host, opts.port,))
    print(f'{echoyt}:')
    print(f'{proxyt}:')
    print(f'{sendt}:')
    print('start echoyt and sleep 1sec')
    echoyt.start()
    print('start proxyt and sleep 1sec')
    proxyt.start()
    time.sleep(1)
    print('start sendt ')
    sendt.start()
    print('waiting for competion')
    echoyt.join()
    proxyt.join()
    sendt.join()
    
