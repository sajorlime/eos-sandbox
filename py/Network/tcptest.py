#!/usr/bin/env python3
############################################
############################################
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""
__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import os
import args

import string
import cgi
import time
import os
import sys

import socket


HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 6000        # Port to listen on (non-privileged ports are > 1023)

def dummy():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()
        conn, addr = s.accept()
        with conn:
            print('Connected by', addr)
            while True:
                data = conn.recv(1024)
                if not data:
                    break
                conn.sendall(data)

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Connect to server and send data
try:
    sock.connect((HOST, PORT))
except ConnectionRefusedError:
    print('CONNECT FAILED:', HOST, PORT, file=sys.stderr)
    sys.exit(-1)
print('CONNECTED:', 'localhost', PORT, file=sys.stderr)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#sock.settimeout(10.0)
_rh = sock.makefile()

#_rh.write('This is a TEST')
while True:
    print('abcdef')
    sock.sendall(bytes('\01\06'.encode('utf-8')))
    sock.sendall(bytes("abcdef".encode('utf-8')))
    time.sleep(5)
    #if not msg:
    #    break

_rh.close()
    
sys.exit(0)

