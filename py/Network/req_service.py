#!/usr/bin/env python3
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import atexit
import queue
import sys
import threading
import time


class ReqException(Exception):
    def __init__(self, error_msg):
        """
        Args:
            errored_dict: is the platform API error return from a query.
        """
        if error_msg is dict:
            Exception.__init__(self, json.dumps(error_msg, indent=2))
        if error_msg is str:
            Exception.__init__(self, error_msg)
        Exception.__init__(self, 'unknown')

class ReqWorker(threading.Thread):
    """
    A class specialized for handling http requests to the AAF API.
    """
    def __init__(self, name, reqf, req_q, rcv_q):
        """
        Encapsulates a service to allow the requests to be handled asynchonously.
        Args:
            name: a string name for the thread
            reqf: a funtion used to make the request.
            req_q: a queue that delivers requests in the form (reply-flag, query)
            rcv_q: detination of query results
        """
        threading.Thread.__init__(self)
        self._name = name
        self._reqf = reqf
        self._req_q = req_q
        self._rcv_q = rcv_q
        self._ttime = 0
        self._treqs = 0
        self.start()

    def run(self):
        """
        A method defined by the Thread class.
        """
        while True:
            #if self._req_q.empty():
            #    print('%s found req-q full' % self._name, file=sys.stderr)
            reply, req, ts = self._req_q.get() # wait here for requests
            #if self._req_q.empty():
            #    print('%s found req-q empty' % self._name, file=sys.stderr)
            if req == 'exit':
                if self._treqs > 0:
                    print('%s happily exiting w/avg %1.5f over %d calls' % (self._name, self._ttime / self._treqs, self._treqs),
                          file=sys.stderr)
                return
            try:
               r = self._reqf(req)
            except ReqException:
                # this should have been handled by the req function so we just continue
                continue
            self._ttime += time.time() - ts
            self._treqs += 1
            if reply:
                self._rcv_q.put(r)


class ReqService(object):
    """
    Excapsaltes the req service so that all requests can use by creating and object
    """
    def __init__(self, name, reqf, req_q, rcv_q, threads):
        self._name = name
        self._reqf = reqf
        self._req_q = req_q
        self._rcv_q = rcv_q
        self._threads = threads
        for th in range(threads):
            ReqWorker('%s-%d' % (name, th), reqf, req_q, rcv_q)

    def exit(self):
        for th in range(self._threads):
            self._req_q.put((False, 'exit', 'now'))
            time.sleep(0.01)

if __name__ == "__main__":
    """
    Code to test the ReqServic
    """
    import random

    reqq = queue.Queue()
    rcvq = queue.Queue()

    def mreqf(req):
        #print('REQF:', req)
        t = random.random()
        #print('sleep:', req, t)
        time.sleep(t)
        #print('woke:', req, t)
        return req

    def rcvf():
        global rcvq
        try:
            return rcvq.get(timeout=10.0)
        except queue.Empty:
            return -377.0


    threads = 10
    rs = ReqService('test', mreqf, reqq, rcvq, threads)

    reqq.put((True, 'abc'))
    print('got from:', rcvq, rcvq.get())

    for i in range(1, threads):
        print('P:', i)
        reqq.put((True, i))

    for i in range(1, threads):
        j = rcvf()
        print('J:', j)
        if j == -377.0:
            break
    rs.exit()

