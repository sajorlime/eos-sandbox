#!/usr/bin/python3

import argparse
import os
import sys
import time

_size = None

nocolor = '[0m'
green = '[32;1m'
yellow = '[33;1m'

def build_status():
    global _size
    _size = os.get_terminal_size()
    #print(_size, 'wait')
    #sin = sys.stdin.readline()
    #rinfo = '[4;{_size.lines}r', end='', flush=True)
    print(f'[4;{_size.lines}r', end='', flush=True)

def reset_status():
    global _size
    _size = os.get_terminal_size()
    print(f'[0;{_size.lines}r', end='', flush=True)

def show_status(n, status):
    c = 31 + (n % 7)
    print(f'[{c};1m', end='')
    print(f'[0;0H{status}', end='')
    print(f'[{_size.lines};0H{nocolor}')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='test status line')
    parser.add_argument('--file', '-f', help="file to scroll")
    parser.add_argument('--reset', '-r',
                        action='store_true',
                        help="reset scroll region")
    args = parser.parse_args()
    if args.reset:
        reset_status()
        if not args.file:
            sys.exit(0)
    build_status()
    n = 0
    while True:
        data = open(args.file).read().split('\n')
        for line in data:
            if (n % (13)) == 0:
                if n > len(data):
                    n = 0
                show_status(n, f'{n}, got ya {data[n]}')
            n += 1
            print(line)
            time.sleep(0.1)

