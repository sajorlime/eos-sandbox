﻿import matplotlib.pyplot as plt
import mpld3
import numpy as np

# Create a figure
fig = plt.figure()

# Plot some data
x = range(10)
y = x ** 2
plt.plot(x, y)

# Export the data
html = mpld3.save_html(fig, 'graph.html', width=640, height=480)

# Save the HTML file
with open('graph.html', 'w') as f:
    f.write(html)
