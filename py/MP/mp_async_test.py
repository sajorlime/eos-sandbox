#!/usr/bin/env python3
#
from multiprocessing import Pool, JoinableQueue, Queue
import os
import time

def work(c):
    print('F:', c)
    return c.lower()

stop_value = 'quit'
#jobq = JoinableQueue(4)
jobq = Queue(4)
resq = Queue()
def put_result(item):
    resq.put(item)
    return None
    print('PR:', items)
    for ritem in items:
        if ritem == stop_value:
            print('PRstop:', ritem)
            break
        resq.put(ritem)
        print('PR:', ritem)

def consume(jobq, resq):
    print('Consume started:')
    for work_item in iter(jobq.get, stop_value):
        try:
            print('WORK:', work_item)
            resq.put(work(work_item))
        except Exception as e:
            print('SE:', e)
        #finally:
        #    jobq.task_done()

def async_consume_error(e):
    print('AME:', e)

def async_map_error(e):
    print('AME:', e)

def pass_it(x):
    print('PI:', x)
    return x

def push_it(x):
    print('PU:', x, jobq.qsize())
    jobq.put(x)
    #return x

if __name__ == '__main__':
    work_items = [ chr(c) for c in range(ord('A'), ord('Z') + 1)]
    batch_size = 4
    with Pool(processes=batch_size) as pool:         # start 4 worker processes
        #result = pool.apply_async(f, (10,), callback=g) # evaluate "f(10)" asynchronously in a single process
        #pool.apply_async(consume, jobq) # evaluate "f(10)" asynchronously in a single process
        for _ in range(batch_size):
            pool.apply_async(consume, (jobq, resq,)).get()
            
        for j in range(0, len(work_items), batch_size):
            wgroup = work_items[j:j + batch_size]
            print('WG:', wgroup)
            for witem in wgroup:
                pool.apply_async(pass_it,
                                 witem,
                                 callback=push_it,
                                 error_callback=async_consume_error) 
            #jobq.join()
            print(' results', resq.empty())
            expected_results = len(wgroup)
            while expected_results > 0:
                while not resq.empty():
                    item = resq.get(False, 0)
                    print('from resq:', item)
                    expected_results -= 1
            print('LE:', expected_results)
        for _ in range(batch_size):
            jobq.put(stop_value)

        #print(pool.map(f, range(10)))       # prints "[0, 1, 4,..., 81]"

        #it = pool.imap(f, range(10))
        #print(next(it))                     # prints "0"
        #print(next(it))                     # prints "1"
        #print(it.next(timeout=1))           # prints "4" unless your computer is *very* slow

        #result = pool.apply_async(time.sleep, (10,))
        #print(result.get(timeout=1))        # raises multiprocessing.TimeoutError
