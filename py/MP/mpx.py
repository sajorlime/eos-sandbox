#!/usr/bin/env python3

import argparse
import copy
import json
import os
import random
import readchar
import sys
import time
import multiprocessing as mp
from multiprocessing import Process, Queue, JoinableQueue, cpu_count, Lock

def serve(pmman):
    for work_item in iter(q.get, 'quit'):
        try:
            pmman.work(work_item)
        except Exception as e:
            print('SE:', e)
        finally:
            pmman._job_queue.task_done()

class PManager:
    def __init__(self, work_it, processes=None):
        if processes:
            self.PROCESSES = processes
        else:
            self.PROCESSES = cpu_count() - 1
        #print('CPUs:', self.PROCESSES)
        self._job_queue = mp.JoinableQueue(maxsize=self.PROCESSES)
        self._job_lock = mp.Lock()
        self._result_queue = mp.Queue()
        self._result_lock = mp.Lock()
        self._work_it = work_it
        self._pool = mp.Pool()

    def start(self):
        for _ in range(mp.cpu_count() - 1):
            self._pool.apply_async(serve, self._job_queue)

        #serve(self._job_queue, self._result_queue)

    def put_result(self, item):
        print('put-result')
        res = self._result_queue.put(item)

    def get_result(self):
        res = self._result_queue.get()
        return res

    def result_empty(self):
        return self._result_queue.empty()

    def result_close(self):
        self._result_queue.close()

    def stop(self):
        for worker in self.workers:
            #print('stop')
            self._job_queue.put('quit')
        for worker in self.workers:
            worker.join()
        self._job_queue.close()

    def run_workers(self, witems, rlist):
        """
        run the works and pass work items
        Args:
            self: a PManger object
            witems: a list of work items
            rlsit: place results in this list
        """
        self.start()
        print('stared')
        #time.sleep(0.01) # let process run
        ritems = 0
        for i in range(0, self.PROCESSES + 1):
            # Put `BATCH_SIZE` items in queue asynchronously.
            self._pool.map_async(self._work_it,
                                 witems[i:i + self.PROCESSES + 1],
                                 callback=self.put_result)
            print('job running')
            self._job_queue.join()
        for _ in range(self.PROCESSES):
            self._job_queue.put('quit')
            print('job quit')
        #self._job_queue.join()

        # get any outsganding results
        while True:
            #self._result_lock.acquire()
            print('get result')
            res = self.get_result()
            #self._result_lock.release()
            #print('wtr:', res)
            if res == None:
                #print('Rcv quits')
                break
            else:
                if 'quit' not in res:
                    ritems += 1
            if 'quit' not in res:
                ritems += 1
                rlist.append(res)
        self.stop()
        while not self.result_empty():
            res = self.get_result()
            #print('wnr:', res)
            if not res:
                break
            if 'quit' not in res:
                ritems += 1
                rlist.append(res)
        self.result_close()

    def work(self, item):
        # do some work on item
        #print('W:', item)
        if type(item) == dict:
            params = {'wdict' : item}
            #rlock.acquire()
            self._result_queue.put(self._work_it(**params))
            #rlock.release()
            #time.sleep(0.001)
        elif type(item) == str:
            #rlock.acquire()
            self._result_queue.put(None) # item is None
            #rlock.release()
        else:
            #rlock.acquire()
            self._result_queue.put(item) # item is None
            #rlock.release()

if __name__ == "__main__":
    # simple test for this module
    def work_it(wdict):
        print('work-it')
        rdict = {}
        for k in wdict.keys():
            print('K:', k, pid)
            rdict[k] = os.getpid()
            #print('Wi:', rdict)
        return json.dumps(rdict)


    manager = PManager(work_it)
    res_list = []
    work_items = [{chr(s) : s} for s in range(ord('A'), ord('Z') + 1)]
    stime = time.perf_counter()
    #for s in range(ord('A'), ord('z') + 1):
    #    work_times.append({s : ord(s)})
    #work_dict = {chr(s) : s for s in range(ord('a'), ord('z') + 1)}
    #work_items = [{chr(s) : s} for s in range(ord('A'), ord('z') + 1)]
    manager.run_workers(work_items, res_list)
    ttime = time.perf_counter() - stime
    #sres = set(res_list)
    print('ttime: %1.5f' % ttime, 'len(%d)' % len(res_list), end='\r')
    #print('ritems:', ritems)
    #sres = set(rlist)
    #assert(len(rlist) == len(work_items))
    #print('ttime: %1.5f' % ttime, 'len(%d)' % len(res_list), end='\r')
    #print('ritems:', json.dumps(res_list, indent=2))
    #print('\n', list(work_items[0])[0])
    keys = [ list(k)[0] for k in work_items]
    #print(keys)
    #print(res_list)
    if len(res_list) != len(work_items):
        print()
        res_list.sort()
        for p in res_list:
            #print(type(p[0]))
            x = list(json.loads(p).keys())[0]
            #print(x, x in keys, len(res_list))
            #readchar.readchar()
            keys.remove(x)
        inds = [ord(k) - ord('A') for k in keys]
        print('no:', keys, inds)
        #print('\nritems:', json.dumps(res_list))
    #print('\nDone')
