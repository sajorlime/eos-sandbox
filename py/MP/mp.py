#!/usr/bin/env python3

import argparse
import copy
import json
import os
import random
import readchar
import sys
import time
from multiprocessing import Process, Queue, cpu_count, Lock

def serve(pmman):
    while True:
        pmman._job_lock.acquire()
        work_item = pmman._job_queue.get()
        pmman._job_lock.release()
        # Note(epr): must quit first, otherwise loses some elements
        if work_item == 'quit':
            #print('serve quits', os.getpid())
            break
        pmman.work(work_item)

class PManager:
    def __init__(self, work_it, max_procs=10):
        self.NUMBER_OF_PROCESSES = min(cpu_count() - 1, max_procs)
        #print('CPUs:', self.NUMBER_OF_PROCESSES)
        self._job_queue = Queue(maxsize=self.NUMBER_OF_PROCESSES)
        self._job_lock = Lock()
        self._result_queue = Queue()
        self._result_lock = Lock()
        self._work_it = work_it

    def start(self):
        self.workers = [Process(target=serve, args=(self,))
                        for i in range(self.NUMBER_OF_PROCESSES)]
        for w in self.workers:
            w.start()

        #serve(self._job_queue, self._result_queue)

    def send_work(self, work_item):
        self._job_queue.put(work_item)

    def get_result(self):
        #self._result_lock.acquire()
        res = self._result_queue.get()
        #self._result_lock.release()
        return res

    def result_empty(self):
        return self._result_queue.empty()

    def result_close(self):
        self._result_queue.close()

    def stop(self):
        for worker in self.workers:
            #print('stop')
            self._job_queue.put('quit')
        for worker in self.workers:
            worker.join()
        self._job_queue.close()

    def run_workers(self, witems, rlist):
        """
        run the works and pass work items
        Args:
            self: a PManger object
            witems: a list of work items
            rlsit: place results in this list
        """
        time.sleep(0.01) # let process run
        ritems = 0
        for wi in witems:
            if not self.result_empty():
                self._result_lock.acquire()
                res = self.get_result()
                self._result_lock.release()
                if 'quit' not in res:
                    ritems += 1
                    #print('ewtr:', res)
                    rlist.append(res)
            self.send_work(wi)
        self.send_work(None)

        # get any outsganding results
        while True:
            #self._result_lock.acquire()
            res = self.get_result()
            #self._result_lock.release()
            #print('wtr:', res)
            if res == None:
                #print('Rcv quits')
                break
            else:
                if 'quit' not in res:
                    ritems += 1
            if 'quit' not in res:
                ritems += 1
                rlist.append(res)
        self.stop()
        while not self.result_empty():
            res = self.get_result()
            #print('wnr:', res)
            if not res:
                break
            if 'quit' not in res:
                ritems += 1
                rlist.append(res)
        self.result_close()

    def work(self, item):
        # do some work on item
        #print('W:', item)
        if type(item) == dict:
            params = {'wdict' : item}
            #rlock.acquire()
            self._result_queue.put(self._work_it(**params))
            #rlock.release()
            #time.sleep(0.001)
        elif type(item) == str:
            #rlock.acquire()
            self._result_queue.put(None) # item is None
            #rlock.release()
        else:
            #rlock.acquire()
            self._result_queue.put(item) # item is None
            #rlock.release()

if __name__ == "__main__":
    # simple test for this module
    import argparse
    import mp_globals
    import numpy as np
    def work_it(wdict):
        print('WIid:', mp_globals._dict)
        print('WIad:', mp_globals._args_dict)
        print('WIl:', mp_globals._list)
        print('W-%dI:' % os.getpid())
        mp_globals.show_globals('wi-%d' % os.getpid())
        rdict = {}
        for k in wdict.keys():
            #print('K:', k, pid)
            rdict[k] = os.getpid()
            #print('Wi:', rdict)
        return json.dumps(rdict)

    class foo(object):
        def __init__(self, a, b):
            self._a = a
            self._b = b
        def __str__(self):
            print('sfoo:', self._a, self._b)
            return '{%s, %s}' % (self._a, self._b)

    parser = argparse.ArgumentParser(description="mp test")
    parser.add_argument('--last','-l', type=int,
                        default=ord('Z'))
    args = parser.parse_args()
    npa = np.array([1, 2, 3, 4, 5])
    npl = np.linspace(1, 1000000, 1000000000)
    #print('mpl size:', npl.size)
    #sys.exit()
    mp_globals._args_dict = {'eo': 999,  'rojas' : 377}
    mp_globals._dict = {'EO': np.multiply(npa, 2),
                        'rojas' : np.multiply(npa, 3),
                        'large' : npl}
    mp_globals._list = [100, 200, 300, npa, np.multiply(npl, 1.7)]
    mp_globals._object = foo(1, np.multiply(npl, 7.7777))
    manager = PManager(work_it)
    manager.start()
    res_list = []
    work_items = [{chr(s) : s} for s in range(ord('A'), ord('A') + args.last + 1)]
    stime = time.perf_counter()
    #for s in range(ord('A'), ord('z') + 1):
    #    work_times.append({s : ord(s)})
    #work_dict = {chr(s) : s for s in range(ord('a'), ord('z') + 1)}
    #work_items = [{chr(s) : s} for s in range(ord('A'), ord('z') + 1)]
    manager.run_workers(work_items, res_list)
    ttime = time.perf_counter() - stime
    #sres = set(res_list)
    print('ttime: %1.5f' % ttime,
            'len(%d)' % len(res_list), end='\n')
    #print('ritems:', ritems)
    #sres = set(rlist)
    #assert(len(rlist) == len(work_items))
    #print('ttime: %1.5f' % ttime, 'len(%d)' % len(res_list), end='\r')
    #print('ritems:', json.dumps(res_list, indent=2))
    #print('\n', list(work_items[0])[0])
    keys = [ list(k)[0] for k in work_items]
    #print(keys)
    #print(res_list)
    if len(res_list) != len(work_items):
        print()
        res_list.sort()
        for p in res_list:
            #print(type(p[0]))
            x = list(json.loads(p).keys())[0]
            #print(x, x in keys, len(res_list))
            #readchar.readchar()
            keys.remove(x)
        inds = [ord(k) - ord('A') for k in keys]
        print('no:', keys, inds)
        #print('\nritems:', json.dumps(res_list))
    #print('\nDone')
    mp_globals.show_globals('end')

