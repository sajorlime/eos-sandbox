#
# define globals used between dc_calc and dcm.main

_args_dict = {}
_object = None
_dict = {}
_list = []
_G = 'g'

def show_globals(pre):
    print('%s-ad:' % pre, _args_dict.keys())
    print('%s-icd:' % pre, _dict)
    print('%s-icl:' % pre, _list)
    i = 0
    for il in _list:
        if type(il) != int:
            print('il:', i, len(il))
        else:
            print('il:', i, il)
        i += 1
    print('%s-g:' % pre, _G)
    print('%s-o:%s' % (pre, _object))

