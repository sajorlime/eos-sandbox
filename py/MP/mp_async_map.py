#!/usr/bin/env python3
#
from multiprocessing import Pool
import json
import os
import time

def work(c):
    rdict = {}
    print('W:', type(wdict)
    for k in wdict.keys():
        #print('K:', k, pid)
        rdict[k] = os.getpid()
        #print('Wi:', rdict)
        return json.dumps(rdict)

def work_wrap(wdict):
    work()
    

def async_apply_error(e):
    print('AAE:', e)


if __name__ == '__main__':
    rlist = []
    work_items = [ {chr(c):c}  for c in range(ord('A'), ord('Z') + 1) ]
    batch_size = os.cpu_count() - 1
    with Pool(processes=batch_size) as pool:   # start 4 worker processes
        j = 0
        #for j in range(0, len(work_items), batch_size):
        wgroup = work_items[j:j + batch_size]
        total_jobs = len(work_items)
        j = batch_size
            #if len(wgroup) != batch_size:
            #    print('last:', wgroup, batch_size)
            #print('WG:', wgroup)
        ritems = []
        # start batch size jobs
        for witem in wgroup:
            r = pool.apply_async(work,
                                 ({'wdict':witem},),
                                 error_callback=async_apply_error) 
            ritems.append(r)
        nresults = len(work_items)
        max_j = 0
        while nresults > 0:
            time.sleep(0.001)
            jobs_complete = 0
            clist = []
            # this loop completes jobs
            for r in ritems:
                if r.ready():
                    rlist.append(r.get())
                    nresults -= 1
                    clist.append(r)
                    #j += 1
                    jobs_complete += 1
            for r in clist:
                ritems.remove(r)
            while jobs_complete > 0 and j < total_jobs:
                r = pool.apply_async(work,
                                     work_items[j],
                                     error_callback=async_apply_error) 
                ritems.append(r)
                j += 1
    pids = [r[1] for r in rlist ]
    print('RL:', rlist, len(rlist))
    print('PIDS:', set(pids))

