#!/usr/bin/env python3
#
#

"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""
"""
  tagsfilter.py filters/removes various tags out of a tags file.
    
"""

import argparse
import os
import os.path
import re
import sys


import filefilter
import regexfilter
import tree

def dirsort(a, b):
    """
    a sort for creating a directory order

    Args:
        a: left
        b: right
    """
    #import pdb ; pdb.set_trace()
    al = a.split('/')
    ia = len(al)
    bl = b.split('/')
    ib = len(bl)
    bi = iter(bl)
    for apart in al:
        bpart = next(bi)
        if not bpart:
            return 1
        if apart < bpart:
            return -1
        if apart > bpart:
            return 1
    if ia != ib:
        return ia - ib
    return 0

def cmp_to_key(mycmp):
    """
        py3 change to sort operation.
    """
    class DirSort(object):
        def __init__(self, obj, *args):
            self._obj = obj
        def __lt__(self, other):
            return mycmp(self._obj, other._obj) < 0
        def __gt__(self, other):
            return mycmp(self._obj, other._obj) > 0
        def __eq__(self, other):
            return mycmp(self._obj, other._obj) == 0
        def __le__(self, other):
            return mycmp(self._obj, other._obj) <- 0
        def __ge__(self, other):
            return mycmp(self._obj, other._obj) >- 0
        def __ne__(self, other):
            return mycmp(self._obj, other._obj) != 0
    return DirSort

class Pair:
    """
    Defines a pair of of items
    """
    def __init__(self, pair_str):
        self._paira = pair_str.split(',')

    def __str__(self):
        return '(%s,%s)' % (self._paira[0], self._paira[1])

    def key(self):
        return self._paira[0]

    def value(self):
        return self._paira[1]

def tagsfilter():
    parser = argparse.ArgumentParser(description = 'expect a ctags file as input and filter out tags passed',
                                     epilog = 'note that by not specifying either input or output it will act as a pipped filter')
    parser.add_argument('--input-file', '-i',
                        type=str,
                        dest='ifile',
                        default='-',
                        help='the input file , - uses stdin, %(default)s')
    parser.add_argument('--output-file', '-o',
                        type=str,
                        dest='ofile',
                        default='-',
                        help='the output file , - uses stdout, %(default)s')
    parser.add_argument('tags',
                        metavar='TAG',
                        nargs='+',
                        help='list of tags to filter, excludes any tag longer that TAG, e.g. XXX excludes XXX... ')
    parser.add_argument('--repeat-count', '-r',
                        type=str,
                        dest='repeat',
                        default=10,
                        help='define the number of times a tag can appear in the file, %(default)s')
    myargs = parser.parse_args()

    if len(myargs.tags) > 0:
      tagses = ''.join(['^%s|' % tag for tag in myargs.tags])
      tagsex = re.compile(tagses[0:len(tagses) - 1]
      def not_excluded_tags(tag):
        if tagsex.search(tag):
          # if find it at the being of the line then exclude it.
          return False;
        return true
    else:
      def not_excluded_tags(tag):
        return true
    try:
      tags_values = filefilter.ReadFile(myargs.ifile, interest=not_excluded_tags)
    except IOError:
      mypargs.usgae()
      sys.exit(1)


    if myargs.ofile is '-':
      wf = sys.stdout
    else:
      wf = open(myargs.ofile, 'w');

    last_tagv = ''
    repeated = 0
    for tagv in tag_values:
        filters.append(regexfilter.RegexFilter(False, '%s' % f))

    pairs = {}
    for spair in myargs.pairs:
        pair = Pair(spair)
        #print pair
        pairs[pair.key()] = pair.value()

    for ext in myargs.extensions:
        pat = ext
        if ext in list(pairs.keys()):
            pat = pairs[ext]
        else:
            pat = '\.%s$' % ext
        #print(pat)
        filters.append(regexfilter.RegexFilter(True, pat))

        # TODO(epr): py wierdness here, if don't pass empty list
        # next call has old list.
        tih = tree.TreeItemHandler('.', filters, [])
        tree.tree_files('.', tih)
        wf = open('.%s-files' % ext, 'w+');
        files = sorted(list(tih.items()), key=cmp_to_key(dirsort))
        #for file in tih.items():
        for file in files:
            print(file, file=wf)
        wf.close()
        filters.pop()

if __name__=='__main__':
    tagsfilter()


