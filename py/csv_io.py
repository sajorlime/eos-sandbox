#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#

""" csv_io.py
    open a CSV file, managed reading and writing CSV file.s
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import sys
import argparse
import codecs
import csv

# CSV library doesn't like unicode, so we need to wrap it.
# code cribbed from
# http://stackoverflow.com/questions/904041/reading-a-utf8-csv-file-with-python
def comment_skip(utf8_csv_data):
    ''' skip comments in csv-data
    Args:
        uft8_csv_data: a list of csv input lines
    Yeilds::
        yeilds all lines that do not start with a hash, "#"
    '''
    for line in utf8_csv_data:
        if line.startswith('#'):
            continue
        yield line


# used to test against max lines and for debug.
_row = 0

def utf8_csv_reader(utf8_csv_data, max_lines, delimiter=','):
    """
    Reader returns reader that allows reading one line at a time.
    Args:
        utf8_csv_data: raw data
        delimiter: how to parse lines.
    Returns:
        one line at a time
    """
    global _row
    _row = 0
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(comment_skip(utf8_csv_data), delimiter=delimiter)
    try:
        for row in csv_reader:
            # decode UTF-8 back to Unicode, cell by cell:
            _row += 1
            if max_lines and (_row > max_lines):
                return # exists iteration
            yield [cell for cell in row]
    except UnicodeDecodeError as e:
        print('exception in utf8-csv-reader @ row', _row)
        raise e


def _identity():
    def cidentity(x, row, i):
        return x
    return cidentity


def csv_open(csv_path,
             delimiter=',',
             header_line=1,
             trim=True,
             max_lines=None,
             max_cols=None):
    """
    Open a CSV file and return the first line and a csv_reader for the file.
    Args:
        csv_path: a file path to a CSV file.
        delimiter: the delimiter for the CSV columns
        header_line: pick the header line, skip lines before
    Returns:
        this list of csv_columns, and a csv_reader for the rest of the file.
    """
    csv_fh = codecs.open(csv_path, encoding='utf-8')
    line = 1
    while line < header_line: # normally does nada
        burn = csv_fh.readline()
        line += 1
    raw_cols = csv_fh.readline().strip().split(delimiter)
    if max_cols != None:
        raw_cols = raw_cols[0:max_cols]
    csv_cols = []
    if trim:
        for col in raw_cols:
            csv_cols.append(col.strip())
    else:
        csv_cols = raw_cols
    csv_reader = utf8_csv_reader(csv_fh, max_lines, delimiter)
    if len(csv_cols) <= 1:
        print(f'csv cols length is <=  1\n{csv_cols}\n',
              file=sys.stderr)
        print('check delimiter', file=sys.stderr)
        parser.print_help()
        sys.exit(1)
    return csv_cols, csv_reader


def csv_open_with_args(args):
    delimiter = args.delimiter
    if delimiter == '\\t':
        delimiter = '\t'
    return csv_open(args.icsv,
                    delimiter=delimiter,
                    header_line=args.header_line,
                    trim=args.trim,
                    max_lines=args.max_lines,
                    max_cols=args.max_cols)


def io_options(parser):
    parser.add_argument('icsv',
                        help='the input CSV file path')
    parser.add_argument('--ocsv', '-o',
                        type=str,
                        default='-',
                        help='the output CSV file path (needed to write '
                             'utf-8 characters, "-" indicates stdout which '
                             'is the default')
    parser.add_argument('--notrim',
                        dest='trim',
                        action='store_false',
                        default=True,
                        help='Do not trim spaces from column headers')
    parser.add_argument('--delimiter', '-d',
                        type=str,
                        default=',',
                        help='the CSV column delimiter, default:' \
                             '"%(default)s", use "\\t" for tab')
    parser.add_argument('--max-cols', '-C',
                        dest='max_cols',
                        type=int,
                        help='the header row, one implies top of file '
                             'If greater than 1 skips rows before. '
                             'default: %(default)s')
    parser.add_argument('--header_line', '-hl',
                        type=int,
                        default=1,
                        help='process')
    parser.add_argument('--max-lines', '-m',
                        dest='max_lines',
                        type=int,
                        default=sys.maxsize,
                        help='the maximum lines to output, default: '
                              '%(default)s')

def write_fh(args):
    if args.ocsv == '-':
        return sys.stdout
    return codecs.open(args.ocsv, encoding='utf-8', mode='w')


def _output_lines(csv_res):
    for i, line in enumerate(csv_res):
        sline = [str(v) for v in line]
        print(','.join(sline))


def csv_write(o_name, rows, cols, indicies, delimiter=','):
    """
    Write the a CSV file.
    Args:
        o_name: output file name, '-' gives stdout
        row: the output data rows, only indices are printed.
        cols: is the column headers
        delimiter: delimit columns with this char/string
    """
    if o_name == '-':
        ofh = sys.stdout
    else:
        ofh = codecs.open(o_name, encoding='utf-8', mode='w')
    header = delimiter.join([f'{cols[i]}' for i in indicies])
    print(header, file=ofh)

    for row in rows:
        rval = delimiter.join([f'{row[i]}' for i in indicies])
        print(rval, file=ofh)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='test code a CSV reader')
    io_options(parser)
    args = parser.parse_args()

    delimiter = args.delimiter
    if delimiter == '\\t':
        delimiter = '\t'
    csv_cols, csv_reader = csv_open_with_args(args)
    print(','.join(csv_cols))
    for csv_line in csv_reader:
        print(','.join(csv_line))


