#!/usr/bin/env python3

import logging
import sys

class SingleLevelFilter(logging.Filter):
    def __init__(self, passlevel, reject):
        self.passlevel = passlevel
        self.reject = reject
    def filter(self, record):
        if self.reject:
            return (record.levelno != self.passlevel)
        else:
            return (record.levelno == self.passlevel)

class LogFilter(logging.Filter):
    def __init__(self, passlevel):
        self._passlevel = passlevel
    def filter(self, record):
        return (record.levelno >= self._passlevel)


if False:
    h1 = logging.StreamHandler(sys.stdout)
    f1 = SingleLevelFilter(logging.INFO, False)
    h1.addFilter(f1)
    rootLogger = logging.getLogger()
    rootLogger.addHandler(h1)
    h2 = logging.StreamHandler(sys.stderr)
    f2 = SingleLevelFilter(logging.INFO, True)
    h2.addFilter(f2)
    rootLogger.addHandler(h2)
    logger = logging.getLogger("my.logger")
    logger.setLevel(logging.DEBUG)
    logger.debug("A DEBUG message")
    logger.info("An INFO message")
    logger.warning("A WARNING message")
    logger.error("An ERROR message")
    logger.critical("A CRITICAL message")
else:
    #logging.basicConfig(filename='flog.log', level=logging.DEBUG)
    soh = logging.StreamHandler(sys.stdout)
    sof = LogFilter(logging.DEBUG)
    soh.addFilter(sof)
    rootLogger = logging.getLogger()
    rootLogger.addHandler(soh)
    fd = open('flog.log', 'w')
    foh = logging.StreamHandler(fd)
    fof = LogFilter(logging.INFO)
    foh.addFilter(fof)
    rootLogger.addHandler(foh)
    logger = logging.getLogger("my.logger")
    logger.setLevel(logging.DEBUG)
    logger.debug("A DEBUG message")
    logger.info("An INFO message")
    logger.warning("A WARNING message")
    logger.error("An ERROR message")
    logger.critical("A CRITICAL message")
