#!/usr/bin/env python3
#

"""test program
   Makes an new directory of schedules from an old one.
   makeToday -ver dir-path-to-new dir-path-to-old MM DD NN HE
   only files with the ending .sch are accepted unless the -ver options is specified
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import os
import os.path
import shutil
import stat
import re
import getopt
import sys
import datetime
import time
import pickle
import copy

import schedDate

ext = '.[Ss][Cc][Hh]'

prog = sys.argv.pop(0)

extraDays = 0

try:
  if sys.argv == []:
    print("Ops parameters")
    print("makeToday [-ver] +days srcDir dstDir MM DD NN HE")
    sys.exit(1)

  if sys.argv[0] == '-ver':
    ext = '.[Vv][Ee][Rr]'
    sys.argv.pop(0)
  if sys.argv[0][0:1] == '+':
    extraDays = int(sys.argv[0][1:])
    sys.argv.pop(0)
except IndexError:
  print("Ops parameters")


mydate = datetime.date.today()
mydate += datetime.timedelta(extraDays)
print(str(mydate))

todayList = schedDate.get(mydate)
day = todayList[schedDate.dayPos]
month = todayList[schedDate.monthPos]
year = todayList[schedDate.yearPos]
print(month + ":" + day + ":" + year)

M = "[0-9,A-C]"
DD = "[0-3][0-9]"
NN = "[0-9,A-F][0-9]"
HE = "[0-9][0-9][0-9]"

try:
  srcDir = sys.argv.pop(0)
  dstDir = sys.argv.pop(0)
except IndexError:
  print("Ops need at least srcdir and dstdir directory ")
  print("usage: makeToday.py [-ver] dstdir srcdir [M [[DD] [[NN] [HeHeHe]]]]")
  sys.exit(2)

if not os.path.exists(srcDir):
  print("Ops srcdir(" + srcDir + "must exist")
  print("usage: makeToday.py dstdir srcdir [M [[DD] [[NN] [HeHeHe]]]]")
  sys.exit(3)

if not os.path.exists(dstDir):
  print("making directory: " + dstDir)
  os.mkdir(dstDir)


try:
  if sys.argv[0]:
    M = sys.argv.pop(0)

  if sys.argv[0]:
    DD = sys.argv.pop(0)

  if sys.argv[0]:
    NN = sys.argv.pop(0)

  if sys.argv[0]:
    HE = sys.argv.pop(0)

except IndexError:
  pass


print("gettting a file list from: ", srcDir)

srcDirList = os.listdir(srcDir)

#srcDirList.sort()
#print srcDirList

pattern = "%s%s%s%s%s" % (M, DD, NN, HE, ext)
legalName = re.compile(pattern)
#print pattern

#print legalName

#targetList = copy.copy(srcDirList)
targetList = []
#print targetList
for srcFile in srcDirList:
  #print "test file: " + srcFile
  if legalName.match(srcFile):
    #print "src file: " + srcFile + " is legal"
    targetList.append(srcFile)
    continue


#print targetList
for srcFile in targetList:
  #dstFile = ''.join([month, day, srcFile[3:]])
  dstFile = "%s%s%s" % (month, day, srcFile[3:])
  print(dstDir + '/' + dstFile)
  shutil.copyfile("%s/%s" % (srcDir, srcFile), "%s/%s" % (dstDir, dstFile))


