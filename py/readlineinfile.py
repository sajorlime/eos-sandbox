#!/usr/bin/env python3
#

"""
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import os
import os.path
import sys

import args
import filefilter


"""
rf = open(sys.argv[1], 'r')
lines = rf.readlines()

print(lines)

def RstripList(list):
  for item in list:
    yield item.rstrip()

wf = open(sys.argv[2], 'w')
for line in RstripList(lines):
  wf.write("%s\n" % line)
"""

if __name__ == "__main__":
    args = args.Args("readlineinfile.py  file line")
    if args.len() > 2 or args.len() == 0:
      args.usage()
      sys.exit(1)
    elif args.len() == 1:
      try:
        tline = int(args.get_value())
        file = '-'
      except TypeError:
        args.usage()
        sys.exit(2)
    else:
      file = args.get_value()
      tline = int(args.get_value())

    cline = 0
    def filter(fline):
      global cline, tline
      cline = cline + 1
      return cline == tline

    lines = filefilter.ReadFile(file, interest = filter)
    if lines:
      print(lines[0])

