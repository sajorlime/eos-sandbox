#!/usr/bin/env python3
#
''' program uses regular expersion to validate string
'''

import re

def extract_values_and_check(text):
    '''
    Extracts the values of hPa and degC from the input text and verifies
    the presence of 'PASSED'.

    Args:
        text (str): The input string containing the data.

    Returns:
        tuple: A tuple containing the extracted hPa and degC values as floats.

    Raises:
        ValueError: If the expected format is not found
            or "PASSED" is not present.
    '''

    pattern = r"hPa:\s+(\d+\.\d+)\ndegC:\s+(\d+\.\d+)\nPASSED"

    match = re.search(pattern, text)

    if not match:
        raise ValueError("Invalid format or 'PASSED' not found")

    hpa = float(match.group(1))
    degC = float(match.group(2))

    return hpa, degC

# Example usage
text = '''
hPa: 973.54
degC: 21.73
PASSED
'''

try:
    hpa_value, degc_value = extract_values_and_check(text)
    print(f"hPa: {hpa_value}, degC: {degc_value}")

    # Perform further tests with the extracted values
    if hpa_value < 1000:
        print("Low pressure detected!")
    # ... other tests ...

except ValueError as e:
    print(e)
