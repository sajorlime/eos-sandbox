#!/usr/bin/env python3
#
#

"""
    regexfilter.py implements a class that provides filters based
    on regular expressions.
    The filter can be an accept or a reject.
    There are two constructors:
        RegexFilter(sized_match)
        RegexFilter(match_type, match)
    
    Examples:
        +\.txt\\Z -- requires that all matches end in .txt.
        -Foo -- excludes file paths that contain 'Foo'

    TODO(epr): look at compiling filters together into single regex.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import os
import os.path
import stat
import re
import sys

#TODO(epr): sort out these tests, something is broken.

def ftest(item, filters, reject=False):
    """
    Filter test, tests item against all filters in filters list.
    Args:
        item: is an kind of the type the filter is expecting
        filters: a list of filters.
        reject: inverts returned value when True
    Returns:
       Returns True iff none of the filters reject the item.
    """
    if reject:
        for filter in filters:
            if filter.reject(item):
                return True;
        return False

    for filter in filters:
        if not filter.accept(item):
            return False
    return True

def ptest(path, filters):
    """
    path test, tests item against all _mode=False, filters in filters list.
    Args:
        path: is a kind of the type the filter is expecting
            filters: a list of filters.
    Returns:
       Returns True iff none of the False filters rejects the item.
    """
    for ffilter in filters:
        #print(f'PT:, {ffilter._mode=}, {ffilter.accept(path)=}, {path}')
        if not ffilter._mode and not ffilter.accept(path):
            return False
    #print('PTT')
    return True

class RegexFilter:

    def __init__(self, mode, regex):
        """
        RegexFilter(mode, regex)
        Args:
            mode: if true accept otherwise reject filter type
            regex: the regular expression that will act as a filter
        """
        #print mode, regex
        self._mode = mode
        self._rex = re.compile(regex)
        #print(mode, self._rex.pattern)
        
    def search(self, item):
        return self._rex.search(item)

    def accept(self, item):
        #print(self._mode, item)
        if self._mode:
            if self.search(item):
                return True
            return False
        # Not!
        #print('Not:', self.search(item))
        if self.search(item):
            #print('not: return false')
            return False
        #print('not: return true')
        return True
        
    def reject(self, item):
        if self._mode:
            return False
        return self.search(item)
        
    def pattern(self):
        return self._rex.pattern

    def __str__(self):
        return 'RegexFilter:{mode: %s, rex:%s}' % (self._mode, self._rex)


class SRegexFilter(RegexFilter):
    def __init__(self, srex):
        """
        RegexFilter(signed_regex)
        """
        #print(f"srex[0] is '+'[0], {srex.find('+')=}, {srex[0]}, {srex=}")
        #RegexFilter.__init__(self, srex[0] is '+'[0], srex[1:])
        RegexFilter.__init__(self, not srex.find('+'), srex[1:])


class ArgRegexFilter(RegexFilter):
    def __init__(self, srex):
        """
        ArgRegexFilter(arg_signed_regex)
        """
        if srex[0] == '+'[0]:
          index = 1
          include_it = True
        elif srex[0] == '-'[0]:
          index = 1
          include_it = False
        else:
          index = 0
          include_it = True
        #print(srex[0] == '+'[0], srex.find('+'), srex[0], srex)
        #RegexFilter.__init__(self, srex[0] == '+'[0], srex[1:])
        RegexFilter.__init__(self, include_it, srex[index:])

if __name__=='__main__':
    f1 = SRegexFilter('+ABC|zyz')
    f2 = SRegexFilter('-ABC')

    print('+?abc', f1.accept('abc'))
    print('+?ABC', f1.accept('ABC'))
    print('+?zyz', f1.accept('zyz'))
    print('+?xyz', f1.accept('xyz'))
    print('-?abc', f2.accept('abc'))
    print('-?ABC', f2.accept('ABC'))
    print('-?zyz', f2.accept('zyz'))
    print('-?xyz', f2.accept('xyz'))

    f3 = RegexFilter(True, 'ABC')
    f4 = RegexFilter(False, 'ABC')
    print('T abc', f3.accept('abc'))
    print('T ABC', f3.accept('ABC'))
    print('F abc', f4.accept('abc'))
    print('F ABC', f4.accept('ABC'))

