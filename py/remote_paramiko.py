#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import os
import paramiko
import time

# SSH connection details
#hostname = 'your_remote_server'
hostname=os.getenv('IMX8IP')
username = 'root'
#password = 'your_password'  # Or use SSH keys for better security
key_filename = os.path.expanduser('~/.ssh/id_rsa')
#key_filename = '/home/eo/.ssh/id_rsa'

# Establish the SSH connection
ssh_client = paramiko.SSHClient()
# Automatically add new host keys to the known_hosts file
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client.connect(hostname,
                   username=username,
                   password='')

def run_cmd(cmd):
# Execute a command
    stdin, stdout, stderr = ssh_client.exec_command(cmd)
    # Process the output
    output = stdout.read().decode()
    error = stderr.read().decode()

    print("Output:")
    print(output)

    if error:
        print("Error:")
        print(error)

run_cmd('./pressure')
time.sleep(1)
run_cmd('./accelerometer')
time.sleep(1)
run_cmd('gpioset gpio2 19=1')
time.sleep(2)
run_cmd('gpioset gpio2 19=0')
# Close the SSH connection
ssh_client.close()
