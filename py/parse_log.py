#!/usr/bin/env python3
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

""" parse_log.py
    Special purpose program to parse the proxy_meter.py log file
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import os
import os.path
import shutil
import sys

import filefilter as ff
import regexfilter as rxf


usage = """
initags [-o=out-tags-file] list of ini files.

If no -o option, writes to stdout.
"""

def key_filter(line):
    """ the line filter takes any line that does not start with white
        space or #.
    Args:
        line: the object being accepted or rejected.
    Returns:
        true if the lines first charactar is 0-9, A-Z, or a-z.
    """
    return line[0].isalnum()

def key_xform(line, num):
    i_tab = line.find('\t');
    i_space = line.find(' ');

    if i_space ==  -1:
        i_space = i_tab
    if i_tab ==  -1:
        i_tab = i_space
    i_white = i_tab
    if i_white == -1:
        return ["", "", -1]
    if i_white > i_space:
        i_white = i_space
    return [line[0:i_white], line[i_white:].strip(), num]

def gen_tags(files, interest, nointerest, must, keywords):
    """
    Generate tags for for the list of files.
    Args:
        files: a list of files
        interest: a string reg ex for chosing lines in the file.
        noitnerest: expressions to skip line
        must: expressions that must be present in list that are not keywords
        keywords: keywords are skipped when creating a tag.
    """
    def my_interest(line):
        """
        returns True iff interest and not nointerest
        """
        if interest.accept(line):
            if nointerest.accept(line):
                return False
            if keywords.accept(line):
                return True
            if must.accept(line):
                return True
            return False
        return False

    tagables = []
    for file in files:
        tagables += filefilter.ReadFile(file, interest = my_interest, xform = lambda l, n : (file, l.rstrip()))

    taglines = {}
    for tagpair in tagables:
        #print(tagline.rstrip())
        tagline = tagpair[1]
        if keywords.accept(tagline):
            if '=' in tagline:
                sline = tagline.split(' ')
                tag = sline[1].split('=')[0]
            else:
                sline = tagline.split(' ')
                #print(sline[1].strip().split('('))
                tag = sline[1].strip().split('(')[0]
            taglines[tag] = tagpair
        else:
            taglines[tagline.split('=')[0]] = tagpair
    return taglines

def gentag(key, line_info):
    spat = line_info[1].find('${')
    if spat > 1:
        # bash substitutions are problamacit so remvoe them.
        # TODO(epr): should be command line option
        return '%s\t%s\t/^%s/' % (key, line_info[0], line_info[1][0:spat])
    return '%s\t%s\t/^%s$/' % (key, line_info[0], line_info[1])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='create tags from a list of files')
    parser.add_argument('log',
                        type=str,
                        help='log file file, output is name log.csv')
    parser.add_argument('--outfile', '-o',
                        type=str,
                        default=None,
                        help='overide outfile name')
    parser.add_argument('--stride', '-s',
                        type=int,
                        default=7,
                        help='the stride groups the data')
    parser.add_argument('--raw', '-r',
                        action='store_true',
                        help='raw fetched data (ascii)')
    args = parser.parse_args()

    def _interest(l):
        if l.startswith('server: b\''):
            if l[10] in '123456789':
                return True
        return False
        
    def _xform(l0, n):
        l1 = l0[10:].strip().strip("\\n'")
        return l1
        
    if args.raw:
        elogs = ff.ReadFile(args.log,
                            interest=lambda l : l[0].isnumeric(),
                            xform=lambda l, n : l.strip()
                            )
    else:
        elogs = ff.ReadFile(args.log,
                            interest=_interest,
                            xform=_xform,
                            strip="\\n")
    '''
    # useing only RAW mode!
    elogs = ff.ReadFile(args.log,
                        interest=lambda l : l[0].isnumeric(),
                        xform=lambda l, n : l.strip()
                        )
    '''
    elog_list = []
    print(f'{len(elogs)=}')
    for elog in elogs:
        print(f'{len(elog)=}')
        elog_list += elog.split(',')
    if args.outfile:
        ofile = open(args.outfile, 'w')
    else:
        ofname = args.log.replace('.log', '.csv')
        print(f'{ofname=}', file=sys.stderr)
        ofile = open(ofname, 'w')
    stride = args.stride
    for i in range(2 * stride):
        print(f'{i=}')
        print(elog_list[i], file=sys.stderr)
    nitems =  len(elog_list)
    el_data = [elog_list[i : i + stride] for i in range(0, nitems, stride)]
    print(f'{len(el_data)=}', file=sys.stderr)
    t0 = float(el_data[0][0])
    print(f'{t0=}', file=sys.stderr)
    lt0 = t0
    print('Secs,Volts,Amps,Watts,S,Q,Hz', file=ofile)
    for eld in el_data:
        t = float(eld[0])
        dt = t - lt0
        #print(f'{dt=}, {t=}, {lt0=}', file=sys.stderr)
        if abs(dt) > 1:
            print(f'dt to big {dt}', file=sys.stderr)
            break
        new_t = t - t0
        lt0 = t
        eld[0] = f'{new_t}'
        if 'NAN' in eld:
            print(f'SKIP NAN', file=sys.stderr)
            continue
        oline = ','.join(eld)
        print(oline, file=ofile)

