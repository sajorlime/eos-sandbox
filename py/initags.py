#!/usr/local/env bin/python3
#

""" initags.py
    Takes a file which is an aviga ini file.
    I.e. it contains keys, value paris, with
    comment and blank lines.  Keys always start on the
    at the first character or the line and can be any alpha
    numeric value.  The value starts after the first white
    space following the string.

    This program creates tags for all in values in a list
    of ini files.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""


__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import os
import os.path
import shutil
import sys

import args
import filefilter

usage = """
initags [-o=out-tags-file] list of ini files.

If no -o option, writes to stdout.
"""

def key_filter(line):
    """ the line filter takes any line that does not start with white
        space for #.
    Args:
        line: the object being accepted or rejected.
    Returns:
        true if the lines first charactar is 0-9, A-Z, or a-z.
    """
    return line[0].isalnum()

def key_xform(line, num):
    i_tab = line.find('\t');
    i_space = line.find(' ');

    if i_space ==  -1:
        i_space = i_tab
    if i_tab ==  -1:
        i_tab = i_space
    i_white = i_tab
    if i_white == -1:
        return ["", "", -1]
    if i_white > i_space:
        i_white = i_space
    return [line[0:i_white], line[i_white:].strip(), num]


if __name__ == "__main__":
    args = args.Args(usage)
    opath = args.get_arg('o', '-');

    if opath == '-':
        ofile = sys.stdout
    else:
        ofile = open(opath, 'w')

    ini_files = args.get_values()

    for ini_path in ini_files:
    # get the values
        key_value_list = filefilter.ReadFile(ini_path, interest = key_filter, xform = key_xform)
        key_value_list.sort()
        for item in key_value_list:
            print("%s\t%s\t%d" % (item[0], ini_path, item[2]), file=ofile)

