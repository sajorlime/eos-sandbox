#!/usr/bin/python3
#


""" Program to take a list of execution paths and return a unique
    set in the order of appearance so as not to change the meaning.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

#from sets import Set

import os
import sys
import re

# local import
#import args
import argparse
import filefilter
 
Usage = """
usage: uniquePath path
       use quotes if spaces in path
       takes any unix list path list and makes each element unique.
"""

def make_path(paths, sep=':', verbose=False):
    # if the path had spaces in it, the value might not be passed correctly to 
    # uniquePath as a single argument, so attempt to reconstruct.

    if len(paths) == 1:
      if verbose: print("path len was 1", file=sys.stderr)
      print(paths[0])
      sys.exit(0)

    path = sep.join(['%s' % p for p in paths])
    return path

if __name__=='__main__':
    parser = argparse.ArgumentParser(
             description='take a PATH as input and output unique path in input order ')
    parser.add_argument('path',
                        nargs='*',
                        help='path files')
    parser.add_argument('--verbose', '-v', 
                        action='store_true')
    parser.add_argument('--nl',
                        action='store_true',
                        help='use newlines rather then ":" as separator')
    args = parser.parse_args()

    if args.nl:
        sep = '\n'
    else:
        sep = ':'


    #print('path:', args.path)
    path_set = set()
    def add_to_set(path, plist):
        global path_set
        if path not in path_set:
            path_set.add(path)
            plist.append(path)

    def expand_from_env(path):
        m = re.search('\${.*}', path)
        if m:
            var = m.group(0)
            if not var:
                return '/bin'
            evar = os.getenv(var[2:-1], None)
            if not evar:
                return '/bin'
            #print('P:', path, var[2:-1], evar)
            return path.replace(var, evar)
        m = re.search('%.*%', pt)
        if m:
            var = m.group(0)
            evar = os.getenv(var[2:-1], '')
            return path.replace(var, evar)
        return path

    fpath = []
    for path in args.path:
        path_things = filefilter.ReadFile(path, strip='\n')
        for pt in path_things:
            if ':' in pt:
                ptl = pt.split(':')
                #print('yes:', len(ptl))
                for pti in ptl:
                    pti = expand_from_env(pti)
                    add_to_set(pti, fpath)
            else:
                pt = expand_from_env(pt)
                add_to_set(pt, fpath)
    #for item in fpath:
    #    print(item)
    #print(path_set)
    print(make_path(fpath))
    sys.exit(0)

