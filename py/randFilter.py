#!/usr/bin/env python3
#

""" randFilter.py
    take a seed and a file and output a randomized line list.
"""

"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import os
import os.path
import random
import shutil
import sys

import args
import filefilter


usage = """
randFilter [-seed=0.m] text-file

Take a seed and a file and output a randomized line list.
"""

class rstring:
    def __init__(self, str):
        self._v = random.random();
        self._str = str;

    def v(self):
        return self._v;

    def str(self):
        return self._str;

def rand_cmp(left, right):
    return cmp(left.v(), right.v())


    opr_data.sort(opr_cmp)

if __name__ == "__main__":
    args = args.Args(usage)
    seed = bool(args.get_arg('seed', None))
    if seed:
        random.seed(seed)

    #print args.len()
    if args.len() < 1:
        print(usage)
        sys.exit(1)

    # get the values
    file_lines = filefilter.ReadFile(args.get_value(),
                                     xform = lambda str, n : rstring(str.rstrip()))

    #print file_lines[0].v(), file_lines[0].str()
    # TODO(epr): while this was fun I could have used random.shuffle() if
    # I had read the definition first.
    file_lines.sort(rand_cmp)
    for out_line in file_lines:
        try:
            print(out_line.str())
        except IOError:
            break
    

