#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import os
import paramiko
import select
import sys
import time

# SSH connection details
#hostname = 'your_remote_server'
hostname=os.getenv('ZH_02_TEST_MACHINE')
username = 'dev'
#password = 'your_password'  # Or use SSH keys for better security
key_filename = os.path.expanduser('~/.ssh/id_rsa')

# Establish the SSH connection
ssh_client = paramiko.SSHClient()
# Automatically add new host keys to the known_hosts file
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client.connect(hostname,
                   username=username,
                   key_filename=key_filename)

# Create an interactive shell channel with pty
#channel = ssh_client.invoke_shell(term='xterm', width=80, height=24)
channel = ssh_client.invoke_shell(term='xterm', width=80, height=12400)

# Send the command to start the interactive program
channel.send('.\accelerometer\n')
#channel.send('sudo -S minicom -D /dev/ttyUSB0 -b 115200\n')
#channel.send('sudo -S minicom -D /dev/ttyUSB0 -b 115200\n')

# Wait for the program to start (adjust as needed)
time.sleep(2)

while True:
    # Check for available data on stdout/stderr or if stdin is ready for input
    readable, writable, exceptional = select.select([channel, sys.stdin], [], [])

    if channel in readable:
        # Read output from the channel
        output = channel.recv(1024).decode()
        print(output, file=sys.stdout, flush=True)
        #sys.stdout.flush()  # Ensure immediate output

        # Check for prompts or other conditions indicating input is needed
        if "Enter your command:" in output:  # Adjust prompt detection as needed
            user_input = input()  # Get input from the user
            channel.send(user_input + '\n')  # Send input to the channel

    if sys.stdin in readable:
        # Read input from the user (if not already handled above)
        user_input = sys.stdin.readline()
        channel.send(user_input)

# Close the channel and SSH connection
channel.close()
ssh_client.close()

'''
# Execute a command
stdin, stdout, stderr = ssh_client.exec_command('ls -l')

# Process the output
output = stdout.read().decode()
error = stderr.read().decode()

print("Output:")
print(output)

if error:
    print("Error:")
    print(error)

# Close the SSH connection
ssh_client.close()
'''
