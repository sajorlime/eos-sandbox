#!/usr/bin/env python3
#
#

"""
    test_control_program.py
    Program has several stages.
        *. See uboot_control_program.py
            *. Connect to U-Boot over serial after boot.
                *. there should be a manual sanity connection before running
                    TCP?  Then we know the system is in u-boot when we run.
            *. Program the cc1310. Requires JTAG connection to board.
            *. Get MAC address assignment
            *. Program MAC.
            *. Generate HWID?
            *. Generate QR Code label.
            *. Apply label and validate with barcode.py
            *. Connect and download binary objects via tftp.
                *.  Linux binaries and M7 binary
            *. Are there any tests before booting Linux?
            *. Boot Linux
            *. Wait for login prompt and login.
                *. This implicitly validates memories and processor.
        *. Connects using SSH
        *. Copy pressure and accelerameter to target
        *. gpio_detect, should capture and validate expected output.
        *. Balidate GPIO with LEDs ON/OFF sequence.
        *. i2c_detect, should capture and validate expected output.
        *. Down via SSH accelerator pressure programs
        *. over i2c validate accelerator.
        *. over i2c validate pressure and temperature.
        *. Run pingpong validating M7.
        *. program the ESP32
            *. Manipulate GPIO to allow programming
"""
"""
    TODO(epr): Insert Zainar License info.
"""
"""
    TODO(epr): start by connecting the u-boot over serial.
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import argparse
import datetime
import enum
import paramiko
import pathlib as pl
import os
import re
import select
import serial
import sys
import textwrap
import time
import typing as ty

_debug = False
_min_hpa = 900
_max_hpa = 1100
_min_degC = 17
_max_degC = 27
_min_Va = 8.8
_max_Va = 10.8

#_hostip=os.getenv('ZH_02_TEST_MACHINE')
_hostip=os.getenv('IMX8IP')
if not _hostip:
    raise OSError('IMX8IP not set')
#_uname = 'dev'
_uname = 'root'

_tty = '/dev/ttyACM%d'

_lgr = None

def logit(lmsg: str, file=_lgr) -> None:
    '''
    file is defined inorder to ingore converted prints
    TODO(epr): remote the extra parameters
    '''
    print(lmsg, flush=True, file=_lgr)
    print(lmsg, flush=True)
    return None


def prompt(pmsg: str, file=sys.stdout) -> None:
    print(lmsg, flush=True)
    return None

_ssh_client = None
def setup_ssh():
    global _hostip, _uname
    global _ssh_client
    # SSH connection details
    #password = 'your_password'  # Or use SSH keys for better security
    #key_filename = os.path.expanduser('~/.ssh/id_rsa')

    # Establish the SSH connection
    _ssh_client = paramiko.SSHClient()
    # Automatically add new host keys to the known_hosts file
    _ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    _ssh_client.connect(_hostip,
                        username=_uname,
                        password='')



def run_cmd(cmd: str, *args: ty.Any) -> (str, str):
    '''
    Executes a command using subprocess.run.

    Args:
        cmd: The name of the executable or function to run.
        *args: Variable number of arguments to pass to the executable.
    Returns:
        output
        CompletedProcess object containing the output and return code.
    '''
    global _ssh_client
    if not isinstance(cmd, str):
        raise TypeError("The 'cmd' argument must be a string.")
    #args = args[:-1] # removing verbose, but I wonder if there is a better way.
    cmd_list = [cmd] + [str(a) for a in args]  # Create the command list
    cmd = ' '.join(cmd_list)
    if _debug:
        logit(f'RC: {cmd_list=}, {cmd=}')
    stdin, stdout, stderr = _ssh_client.exec_command(f'{cmd}')
    out_read = stdout.read().decode()
    err_read = stderr.read().decode()
    if err_read and None:
        logit(f'SE: {err_read}')
    return out_read, err_read


class Color(enum.Enum):
    RESET = 0
    BLACK = 30
    RED = 31
    GREEN = 32
    YELLOW = 33
    BLUE = 34
    MAGENTA = 35
    CYAN = 36
    WHITE = 37
    # Extended colors
    ORANGE = 208

    def __str__(self):
        if self.value < 200:
            return f"\033[{self.value}m"
        else:
            return f"\033[38;5;{self.value}m"

_black = f'{Color.RESET}'


class GPIO_LED(object):
    _color_map = {
        'red' : f'{Color.RED}',
        'green' : f'{Color.GREEN}',
        'yellow' : f'{Color.YELLOW}',
        'blue' : f'{Color.BLUE}',
        'orange' : f'{Color.ORANGE}',
    }

    def __init__(self, dev, pin, color):
        self._dev = dev
        self._pin = pin
        self._color = color
        self._ansi_color = GPIO_LED._color_map[color]

    def set(self, v:int):
        #logit(f'set {self._color} to {v}')
        run_cmd('gpioset', self._dev, f'{self._pin}={v}')

    def turn_on(self):
        self.set(1)

    def turn_off(self):
        self.set(0)

    @property
    def color(self):
        return self._color

    @property
    def ansi_color(self):
        return self._ansi_color

    @property
    def show_color(self):
        return f'{self._ansi_color}{self._color}'

_dvk_led_list = [GPIO_LED('gpio2', 19, 'red'),
                 GPIO_LED('mca-gpio', 12, 'green'),
                 GPIO_LED('mca-gpio', 18, 'yellow')]
_zh02_led_list = [GPIO_LED('gpio4', 24, 'orange'),
                  GPIO_LED('gpio4', 25, 'green'),
                  GPIO_LED('gpio4', 26, 'yellow'),
                  GPIO_LED('gpio4', 21, 'blue')]

def validate_gpio(led_list) -> str:
    ''' iterate through the led_list with the user validating that LEDS turn
        on and then off.
    '''
    uresp = 'LED sequence'
    input('testing GPIO with LED on/off, <enter>')
    for led in led_list:
        led.turn_on()
        uin = input(f'Y/n <enter> to validate that the'
                    f' {led.show_color} LED is ON{_black}');
        if 'n' in uin:
            return f'{uresp} -- {led.color} on rejected'
        uresp += f' -- {led.color} ON'
        led.turn_off()
        uin = input(f'Y/n <enter>{led.ansi_color} to validate that the'
                    f'{_black} {led.color} LED is OFF');
        if 'n' in uin:
            return f'{uresp} -- {led.color} off rejected'
        uresp += f' -- {led.color} OFF'
    return uresp

def validate_i2c() -> str:
    ''' Calls i2c methods and captuers their stdout to validate
        excpeted operation.
    '''
    return ''




def validate_accelerometer() -> str:
    ''' Calls accelerometer method and captuers its stdout to validate
        excpeted operation.
        See Lis2de12 device documentation.
    '''
    input('testing accelerometer, <enter>')
    sout, serr = run_cmd('./accelerometer', [])
    accel_pat = r'Va:\s+(\d+\.\d+) PASSED'
    match = re.search(accel_pat, sout)
    if not match:
        out = sout.replace("\n", "- - -")
        err = serr.replace("\n", "- - -")
        rstr = f'{Color.RED}Unexpected result --{out}--{err}--{_black}'
        logit(rstr)
        return rstr
    Va = float(match.group(1))
    if (Va < _min_Va) or (Va > _max_Va):
        rstr = (f'{Color.YELLOW}Warning acceleration ({Va=})'
                f'out of range{_black}')
        logit(rstr)
        return rstr
    rstr =  f'{Color.GREEN}accelerometer {Va=} PASSED{_black}'
    logit(rstr)
    return rstr


def validate_pressure() -> str:
    ''' Calls pressure method and captuers it stdout to validate
        excpeted operation.
        Example expected results:
            hPa: 973.54
            degC: 21.73
            PASSED
    '''
    input('testing pressure and temperature, <enter>')
    sout, serr = run_cmd('./pressure', [])
    rstr = ''
    pt_pat = r"hPa:\s+(\d+\.\d+) degC:\s+(\d+\.\d+) PASSED"
    match = re.search(pt_pat, sout)
    if not match:
        out = sout.replace("\n", "- - -")
        err = serr.replace("\n", "- - -")
        return (f'{Color.RED}Unexpected result'
                f'-- {out} -- {err} -- {_black}')
    hpa = float(match.group(1))
    degC = float(match.group(2))

    if (hpa < _min_hpa) or (hpa > _max_hpa):
        rstr = (f'{Color.YELLOW}Warning pressure ({hpa})'
                f'out of range{_black}')
    else:
        rstr = f'{Color.GREEN}pressure {hpa=}, PASSED{_black}'
    #logit(f'F:{rstr=}')
    if (degC < _min_degC) or (degC > _max_degC):
        rstr = (f'{rstr} - ' 
                f'{Color.YELLOW}'
                f'Warning temperature ({degC}) out of range'
                f'{_black}')
    else:
        rstr = f'{rstr} -- {Color.GREEN}temperature {degC=}, PASSED{_black}'
    logit(f'{rstr}')
    return rstr

   



if __name__=='__main__':
    _epilog = textwrap.dedent("""\
        The test has several steps:
            *. Read bardcode stcker to capture S/N and ?
               This requires that the barcode reader is in the a USB port.
               When prompted the user will scan the barcode.
               The scaned info, e.g, S/N, egc.  will be logged.
               It needs to validate the Barcode written
               by uboot_test_control.py
            *: Download test programs.
            *: Log gpio detect
            *: Manually versify the color LEDs.
               Verify other LEDs. Uses builtin gpio code.
            *: Log i2c detect
            *: Runs Pressure test, logs results.
            *: Runs Accelerameter test, logs results.
            *: TBD install released code.
    """)

    parser = argparse.ArgumentParser(description='ZH-02 H/W validation test '
                                                 'control program', 
                                     formatter_class\
                                        =argparse.RawDescriptionHelpFormatter,
                                     epilog=_epilog)

    parser.add_argument('--board',
                        choices=['zh-02', 'dvk'],
                        default='zh-02',
                        help='test the zh-02 or the dvk for test developmet')
    parser.add_argument('--debug',
                        action='store_true',
                        help='_debug flag set when selected')
    expars = parser.add_mutually_exclusive_group()
    expars.add_argument('--pressure', '-p',
                        action='store_true',
                        help='if set do only pressure')
    expars.add_argument('--accelerometer', '-a',
                        action='store_true',
                        help='if set do only accelerometer')
    expars.add_argument('--leds', '-l',
                        action='store_true',
                        help='if set do only gpio-leds')
    parser.add_argument('--min_hpa',
                        type=float,
                        default=_min_hpa,
                        help=argparse.SUPPRESS)
    parser.add_argument('--max_hpa',
                        type=float,
                        default=_max_hpa,
                        help=argparse.SUPPRESS)
    parser.add_argument('--min_degC',
                        type=float,
                        default=_min_degC,
                        help=argparse.SUPPRESS)
    parser.add_argument('--max_degC',
                        type=float,
                        default=_max_degC,
                        help=argparse.SUPPRESS)
    parser.add_argument('--min_Va',
                        type=float,
                        default=_min_Va,
                        help=argparse.SUPPRESS)
    parser.add_argument('--max_Va',
                        type=float,
                        default=_max_Va,
                        help=argparse.SUPPRESS)
    parser.add_argument('--port',
                        type=int,
                        default=0,
                        help=argparse.SUPPRESS)

    class Dir(object):
        """ class for argparse get a valid directory path.
        """
        def __init__(self, dpath: str):
            logit(f'Dir init({dpath}')
            self._dpath = pl.Path(dpath)
            logit(f'{self._dpath=}')
            if not self._dpath.exists():
                return
            if not self._dpath.is_dir():
                errmsg = (f'Error creating directory'
                          f'"{self._dpath} is not a valid dir path"')
                raise argparse.ArgumentTypeError(errmsg)
            return

        def instantiate(self):
            if not self._dpath.exists():
                try:
                    # Attempt to create the directory
                    os.makedirs(self._dpath, exist_ok=True) 
                except OSError as e:
                    raise argparse.ArgumentTypeError(f'Error creating directory'
                                                     f'"{self._dpath}": {e}')
            return self._dpath

    # TODO(epr): add a remote service or file system
    parser.add_argument('--logdir',
                        type=Dir,
                        default=os.path.expanduser(f'~/log/zh-02_validation'),
                        help='Path to the log file directory, %(default)s,')
    args = parser.parse_args()
    _debug = args.debug
    single_op = args.leds or args.pressure or args.accelerometer
    # TODO(epr): consider doing this w/in a class
    _min_hpa = args.min_hpa
    _max_hpa = args.max_hpa
    _min_degC = args.min_degC
    _max_degC = args.max_degC
    _min_Va = args.min_Va
    _max_Va = args.max_Va

    today = datetime.date.today()
    tdystr = today.strftime("%Y%m%d")
    stty = _tty % args.port

    setup_ssh()
    logdir = args.logdir.instantiate()
    lpath = logdir / f'{tdystr}.log'

    led_list  = _zh02_led_list if args.board == 'zh-02' else _dvk_led_list
    with lpath.open('a') as lfd:
        ''' Get the barcode before testing.
        And example barcode:
        ZH-02 r1,Batch:09042402001,HWID:D4F1FB20004B1200,MAC:C4AC59A619B6,Rev: 2.0
        '''
        _lgr = lfd
        barcode = None
        prompt(f'Scan ZH-02 barcode:')
        with serial.Serial(stty, 115200, timeout=0) as ser:
            while True:
                # Use select to wait for data on the serial port
                # Timeout of 1 second
                ready_to_read, _, _ = select.select([ser], [], [], 1)
                if ser in ready_to_read:
                    # Data available, read it
                    data = ser.read(ser.in_waiting)
                    if len(data) == 0:
                        continue
                    barcode = data.decode()
                    logit(f'Received barcode: {barcode}')
                    break
                else:
                    # No data within the timeout, do other work
                    print('    waiting for scan \007', end='\r')

        bcpat = r'(Z[AHT]-[0-9][0-9] .+),Batch:(\d+),HWID:([A-F0-9]+),MAC:([A-F0-9:]+),Rev: (\d+\.\d+)'
        m = re.match(bcpat, barcode)
        if not m:
            logit(f'Barcode: {barcode}\n did not match {bcpat}')
            sys.exit()
        model = m.group(1)
        batch = m.group(2)
        hwid = m.group(3)
        mac = m.group(4)
        hwrev = m.group(5)
        logit(f'\n----\nTCP Log entry: @ {time.asctime()}', file=lfd)
        logit(f'{lpath}: ', file=lfd)
        logit(f'ID: {barcode}: ', file=lfd)
        logit(f'{model=}: ', file=lfd)
        logit(f'{batch=}: ', file=lfd)
        logit(f'{hwid=}: ', file=lfd)
        logit(f'{mac=}: ', file=lfd)
        logit(f'{hwrev=}: ', file=lfd)
        # TODO(epr): decode the barcode
        alist = [f"{arg}: {getattr(args, arg)}" for arg in vars(args)]
        logit(f'Args: {alist}\n', file=lfd)
        if single_op:
            if args.leds:
                tr = validate_gpio(led_list)
                logit(tr, file=lfd)
            elif args.pressure:
                tr = validate_pressure()
                logit(tr, file=lfd)
            elif args.accelerometer:
                tr = validate_accelerometer()
                logit(tr, file=lfd)
        else:
            logit(validate_gpio(led_list), file=lfd)
            logit(validate_i2c(), file=lfd)
            logit(validate_accelerometer(), file=lfd)
            logit(validate_pressure(), file=lfd)
            logit('\n', file=lfd)
    logit(f'ZH-02 data logged in: {lpath}')
    sys.exit(0)

