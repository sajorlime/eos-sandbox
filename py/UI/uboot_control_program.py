#!/usr/bin/env python3
#
#

"""
    uboot_control_program.py
    See: https://docs.google.com/document/d/1Ix8FrxZuwXYE5D0vfDA9evxaG0RkIc04ymPBLPvvLCk/edit
    Program has several stages.
        *. Connect to U-Boot over serial after boot.
            *. there should be a manual sanity connection before running
                TCP?  Then we know the system is in u-boot when we run.
        *. Program the cc1310. Requires JTAG connection to board.
        *. Get MAC address assignment
        *. Program MAC.
        *. Generate HWID?
        *. Generate QR Code label.
        *. Run ZH_O2_TARGET=<ASSIGNED-ADDR> uboot_control_program.py
        *. Set an IP address that works on your network.
            *. DHCP can be used on first board, but after it should be
               assigned statically. Maybe take the address at the top
               of the available ramge from the router.
               @Zainar 172.16.2.75?
               @EO 10.0.0.142
        *. Connect and download binary objects via tftp.
            *.  Linux binaries and M7 binary
        *. Are there any tests before booting Linux?
        *. Boot Linux
"""
"""
    TODO(epr): Insert Zainar License info.
"""
"""
    TODO(epr): Big change, managing u-boot through each step has proved
        problamatic.  It seems like we are sometimes dropping characters.
        I'm changing tacktics.  Probably should have firgured this out
        before.  But everthing the information is sparse and asking the 
        correct questions the hard part.
        Now we will load tftp.scr.img and source it.
        I need to figure out if I can successfully use pythong to launch
        this process, reply yes at the right time, and notice the we've
        booted and have a login prompt.

        Still need to validate/generate pingpong.
        Do TCP from here, or exit and run TCP
        We still do not copy objects from S3 (or other) storage.
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import argparse
import codecs
import datetime
import enum
import pathlib as pl
import os
import select
import serial
import sys
import textwrap
import time
import typing as ty


_wait = True
_lgr = None

@ty.no_type_check
def logit(lmsg: str) -> None:
    print(f'{lmsg}', end='', flush=True, file=_lgr)
    if not lmsg.endswith('\n'):
        print(flush=True, file=_lgr)
    if lmsg.startswith('>>'):
        return None
    print(lmsg, end='', flush=True)
    return None


_hostip = os.getenv('ZH_02_TEST_MACHINE')
if not _hostip:
    raise OSError('ZH_02_TEST_MACHINE not set')
_zh_02_ip = os.getenv('ZH_02_TARGET')
if not _zh_02_ip:
    raise OSError('ZH_02_TARGET not set')
_dev = os.getenv('ZH_02_TEST_USER')
if not _dev:
    _dev = 'dev'
_root = os.getenv('ZH_02_TARGET_USER')
if not _root:
    _root = 'root'


'''
Values to be delivered inside u-boot.
TODO(epr): condsider steps with validation: (cmd: check, result)
'''
# if this needs to change read from environment
_net_mask = '255.255.255.0'
# root name for the image dvk, we could change
_image_name = 'ccimx8mn-dvk'
# The linux name using components, according to build
_linux_name = f'dey-image-qt-xwayland-{_image_name}'
#################################################################


_really = 'Do you really want to program the boot loader? <y/N>'
def wait_for_really(ser: serial.Serial, empty_tries=100) -> None:
    """
    Reads characters from a serial port until a terminal string is matched.

    Args:
        ser: The serial port object.
        terminal: The string that signals the end of the data to be read.

    Returns:
        Logs strings recieved
    """
    data = ''
    tries = 0
    stime = 0.1
    #logit(f'\nRUR top: {ser.in_waiting=}, {_really=}')
    rtr, _, _ = select.select([ser], [], [], stime)
    rval = ser.read(1).decode('utf-8', errors='ignore')
    while tries < empty_tries:
        # sleep stime up to empty_tries, reset tries if we have data.
        rval = ser.read(1).decode('utf-8', errors='ignore')
        print(f'{rval}', end='')
        if not rval:
            tries += 1
            #logit(f'RUR: empty rval:{data}, len:{len(data)} {ser.in_waiting=}')
            rtr, _, _ = select.select([ser], [], [], stime)
            if tries > (empty_tries / 3):
                print(f'{tries} / {empty_tries} ', end='')
            continue
        else:
            tries = 0
        data += rval
        if data.endswith(_really):
            logit(data)
            return None
    logit(f'RUS error: {data=}, {ser.in_waiting=}')
    raise serial.SerialTimeoutException(f'RUS timeout, {ser.in_waiting}')



def read_until_string(ser: serial.Serial,
                      terminal:str,
                      empty_tries=10,
                      no_error=False,
                      term2=None) -> None:
    """
    Reads characters from a serial port until a terminal string is matched.

    Args:
        ser: The serial port object.
        terminal: The string that signals the end of the data to be read.

    Returns:
        Logs strings recieved
    """
    data = ''
    tries = 0
    stime = 0.1
    #logit(f'RUS top: {ser.in_waiting=}, {terminal=}')
    rtr, _, _ = select.select([ser], [], [], stime)
    rval = ser.read(1).decode('utf-8', errors='ignore')
    if rval and rval != ' ':
        data = rval
    while tries < empty_tries:
        # sleep stime up to empty_tries, reset tries if we have data.
        rval = ser.read(1).decode('utf-8', errors='ignore')
        if not rval:
            tries += 1
            rtr, _, _ = select.select([ser], [], [], stime)
            continue
        data += rval
        if data.endswith(terminal):
            if tries > (empty_tries / 2):
                logit(f'dtries:{empty_tries=}:{empty_tries - tries}:{data}')
            else:
                logit(f'{data}')
            return None
        if term2:
            if term2 in data:
                logit(f'RUS T2: {term2=}:{data}')
                return None
    if terminal in data:
        logit(f'RUS: multi: {data}, {tries=})/{empty_tries}')
        return None
    if no_error:
        return None

    logit(f'RUS error: {data=}')
    raise serial.SerialTimeoutException(f'RUS timeout, {ser.in_waiting}')


def read_until_prompt(ser: serial.Serial) -> None:
    read_until_string(ser, '=>')


# example mmc part respose
'''
Partition Map for MMC device 0  --   Partition Type: EFI

Part    Start LBA       End LBA         Name
        Attributes
        Type GUID
        Partition GUID
  1     0x00001000      0x00020fff      "linux_a"
        attrs:  0x0000000000000000
        type:   ebd0a0a2-b9e5-4433-87c0-68b6b72699c7
        type:   data
        guid:   43f1961b-ce4c-4e6c-8f22-2230c5d532bd
  2     0x00021000      0x00040fff      "linux_b"
        attrs:  0x0000000000000000
        type:   ebd0a0a2-b9e5-4433-87c0-68b6b72699c7
        type:   data
        guid:   f241b915-4241-47fd-b4de-ab5af832a0f6
  3     0x00041000      0x00640fff      "rootfs_a"
        attrs:  0x0000000000000000
        type:   ebd0a0a2-b9e5-4433-87c0-68b6b72699c7
        type:   data
        guid:   1c606ef5-f1ac-43b9-9bb5-d5c578580b6b
  4     0x00641000      0x00c40fff      "rootfs_b"
        attrs:  0x0000000000000000
        type:   ebd0a0a2-b9e5-4433-87c0-68b6b72699c7
        type:   data
        guid:   c7d8648b-76f7-4e2b-b829-e95a83cc7b32
  5     0x00c41000      0x00c48fff      "safe"
        attrs:  0x0000000000000000
        type:   ebd0a0a2-b9e5-4433-87c0-68b6b72699c7
        type:   data
        guid:   ebae5694-6e56-497c-83c6-c4455e12d727
  6     0x00c49000      0x00c50fff      "safe2"
        attrs:  0x0000000000000000
        type:   ebd0a0a2-b9e5-4433-87c0-68b6b72699c7
        type:   data
        guid:   3845c9fc-e581-49f3-999f-86c9bab515ef
  7     0x00c51000      0x00e2ffde      "data"
        attrs:  0x0000000000000000
        type:   ebd0a0a2-b9e5-4433-87c0-68b6b72699c7
        type:   data
        guid:   3fcf7bf1-b6fe-419d-9a14-f87950727bc0
=> 
'''
def mmc_part_hander(ser: serial.Serial) -> None:
    ''' capture mmc part response, approx
        lines   words  chars
         43     106    1645
        The characters come immediately
    '''
    lines = 0
    while lines < 42:
        read_until_string(ser, '\n' , empty_tries=42)
        #input(f'{lines}')
        lines += 1
    read_until_prompt(ser)
    return None

# calculate an approximate number of expected lines
_pre = 33
_vfat = 30
_ext = 930
_boot = 740
_reset_lines = 2 * _vfat + 2 * _ext + _boot

def reset_handler(ser: serial.Serial) -> None:
    ''' Almost 800 lines
        About 20 seconds
    '''
    lines = 0
    time.sleep(2)
    logit(f'{_reset_lines=}')
    while lines < _reset_lines:
        read_until_string(ser,
                          '\n' ,
                          empty_tries=int(30/0.1),
                          no_error=True,
                          term2='login:')
        lines += 1
        if (lines & ((1 << 6) - 1)) == 0:
            logit(f'{lines=} of {_reset_lines}\n')
    logit(f'{_reset_lines} READ')
    for _ in range(10):
        ser.write('\n'.encode('utf-8'))
        time.sleep(1)
        rval = ser.read(ser.in_waiting)
        if 'login:' in rval.decode('utf-8', errors='ignore'):
            logit('FOUND LOGIN')
            return None
    return None

import subprocess

def check_serial_port(port):
    ''' adding this bacuse if serial port is open, e.g. but minicom
    The code sits silently ignoring that it can't really access the port.
    But thinks it has opened it.
    '''
    # prints stuff to stderr we don't care about.
    try:
        output = subprocess.check_output(['lsof', port],
                                         stderr=subprocess.DEVNULL)
        if output:
            err_msg = f'Error: Serial port:{port} is already in use!'
            logit(f'CSP: {output}') # this will tell us who the culprit is
            logit(err_msg)
            raise serial.SerialException(err_msg)
    except subprocess.CalledProcessError as e:
        # except this error is the port is not accessed.
        pass


def run_uboot(lapth):
    global _lgr
    with lpath.open('w') as lfd:
        '''
        '''
        _lgr = lfd
        logit(f'\n----\nUCP Log entry: @ {time.asctime()}\n')
        check_serial_port(stty)
        with serial.Serial(stty, 115200, timeout=0) as ser:
            '''
            Value, handler pairs
            '''
            # if this needs to change read from environment
            #_net_mask = '255.255.255.0'
            # root name for the image dvk, we could change
            #_image_name = 'ccimx8mn-dvk'
            # The linux name using components, according to build
            #_linux_name = f'dey-image-qt-xwayland-{_image_name}'
            cmd_resp_steps = [
                #tuple((f'mmc part', mmc_part_hander)),
                #None,
                # see https://www.digi.com/resources/documentation/digidocs/embedded/dey/4.0/cc8mnano/yocto-change-partition-table_t_emmc.html
                #' ', # send a character and validate that we are in uboot.
                tuple(('run parts_linux_dualboot', read_until_prompt)),
                # Set network settings
                # edit to match assumes environment variables are set!
                tuple((f'setenv serverip {_hostip}', read_until_prompt)),
                #tuple(('print serverip', read_until_prompt)),
                tuple((f'setenv ipaddr {_zh_02_ip}', read_until_prompt)),
                #tuple(('print ipaddr', read_until_prompt)),
                tuple((f'setenv netmask {_net_mask}', read_until_prompt)),
                #tuple(('print netmask', read_until_prompt)),
                # if a gateway is being used, but the netmask might change
                #CmdResp(f'setenv gatewayip {_gateway_ip}'
                tuple(('tftpboot ${loadaddr} tftp.scr.img', read_until_prompt)),
                tuple(('source ${loadaddr}', wait_for_really)),
                #tuple(('yes', reset_handler)),
                ]


            clear_serial(ser)
            for cmd_resp in cmd_resp_steps :
                if not cmd_resp:
                    break
                print(cmd_resp[0])
                send_and_handle(cmd_resp[0], cmd_resp[1], ser)
                if _wait:
                    input(f'\n for next <enter>')
            send_cmd(ser, 'yes', no_assert=True)
            reset_handler(ser)
        logit(f'ZH-02 data logged in: {lpath}\n')


def wait_for_reply(ser: serial.Serial,
                   rlen: int,
                   raise_ex=True,
                   tout=0.1,
                   max_tries=5) -> str:
    ''' Waits for a reply of rlen and returns it.
        A key assumption here is that we are not in a hurry to process replies.
    Args:
        ser: the serial connection to the /dev/ttyUSB0 from the ZH-02
        rlen: the bytes to read
        raise_ex: if true raises an exception if we timeout before
            that many characters are returned.
        tout: seconds before timeout.
        max_tries: tentative max tries
    Returns:
        return read string
    '''
    rstr = ''
    read = 0
    while True:
        # sleeps here for characters
        rtr, _, _ = select.select([ser], [], [], tout)
        if ser in rtr:
            if ser.in_waiting >= rlen:
                #print(f'read {rlen=} of {ser.in_waiting=} ? {rlen=}')
                rstr = ser.read(rlen).decode('utf-8', errors='ignore')
                #print(f'after read {rstr=} of {ser.in_waiting=} ? {rlen=}')
                return rstr
        if max_tries <= 0:
            # We've timed out, but we what to return what has been received.
            #logit(f'WFR: {ser.in_waiting}\n')
            rstr = ser.read(ser.in_waiting).decode('utf-8', errors='ignore')
            #logit(f'WFR short: {rlen=}, {rstr}, {ser.in_waiting=}\n')
            return  rstr
        max_tries -= 1
        continue


def read_available(ser: serial.Serial, tout=1, rlen=80, echo=True) -> str:
    ''' used to read the anything in the buffer and return it.
        caller should look for termination condition.
    '''
    ra_tries = 5
    ret_str = ''
    while True:
        av_str = wait_for_reply(ser,
                                rlen,
                                raise_ex=False,
                                tout=tout,
                                max_tries=2)
        ret_str += av_str
        if echo:
            logit(ret_str)
        if  not av_str:
            return ret_str
        continue


def clear_serial(ser: serial.Serial) -> None:
    ''' clear the serial input from uboot.
    '''
    # pushing space at uboot stops the last commnd from repeating.
    push_str = ' \n'
    ser.write(push_str.encode('utf-8'))
    #rtr, _, _ = select.select([ser], [], [], 0.1)
    #ser.read(len(push_str))
    #read_until_prompt(ser)
    read_available(ser, echo=False)


def send_cmd(ser: serial.Serial, uboot_cmd: str, no_assert=False) -> None:
    ''' Send a command string
        Note: u-boot echos the characters sent before replying.
            So the code needs to consume and ignore the echo.
            If the string is not returned it is an error.
            Any additional reply from u-boot is left in the buffer.
    Args:
        ser: the serial connection to the /dev/ttyUSB0 from the ZH-02
    Return:
        returns None else asserts error
    '''
    # we append CRLF because that is what u-boot will reply with.
    logit(f'{uboot_cmd=}')
    scmd = f'{uboot_cmd}\r\n'
    slen = len(scmd)
    rtr, _, _ = select.select([ser], [], [], 0.1)
    if rtr:
        dead_chars = ser.read(ser.in_waiting)
        #print(f'throw away:{dead_chars=}')
    sent = 0
    while sent < slen:
        c = scmd[sent]
        ser.write(c.encode('utf-8'))
        time.sleep(0.1)
        r = ser.read(1).decode('utf-8', errors='ignore')
        if not r:
            #print('sc rcv empty, cont')
            time.sleep(0.1)
            continue
        if r == ' ' and c != ' ':
            continue
        if c != r and no_assert:
            return None
        assert c == r, f'SC: scmd:{c=} != {r=}, {scmd=}'
        sent += 1
    return None
    srep = wait_for_reply(ser, slen, tout=3, max_tries=10)
    # check for command w/o the CRLF
    if uboot_cmd in srep:
        return None
    # TODO(epr): this feels like a hack to be removed
    logit(f'send_cmd: {uboot_cmd=} not in {srep}')
    if not no_assert:
        logit(f'send_cmd: {uboot_cmd=} not recieved')
        assert scmd == srep, f'SC: scmd:{scmd=} != {srep=}'
    time.sleep(1)
    return None


def send_and_handle(uboot_cmd: str,
                    handler: ty.Callable[[serial.Serial], bool],
                    ser: serial.Serial) -> bool:
    ''' send the command and wait and handle the expected response.
    Args:
        uboot_cr: CmdResp
        handler: fuction that expects cmd response
    '''
    send_cmd(ser, uboot_cmd)
    return handler(ser)


if __name__=='__main__':
    _epilog = textwrap.dedent("""\
        The test has several steps:
            *: Connects to new ZH-02 board using serial module..
               Connects to /dev/ttyUSB0, If minicode was used it
               must be closed.
            *: Assumes TFTP has been setup on linux dev machine.
            *: Validate u-boot prompt.
            *: Setup TFTP in u-boot, sets IP addr
            *: Setup dual boot partions.
            *: Using TFTP install all partions and pingpog.
            *: Boot linux, log, and wait for prompt.
            *: Exit
    """)

    parser = argparse.ArgumentParser(description='ZH-02 uboot installation ',
                                     formatter_class\
                                      =argparse.RawDescriptionHelpFormatter,
                                     epilog=_epilog)

    parser.add_argument('--wait',
                        action='store_true',
                        help='wait between commands')

    class Dir(object):
        """ class for argparse to get a valid directory path.
        """
        def __init__(self, dpath: str):
            #print(f'Dir init({dpath}')
            self._dpath = pl.Path(dpath)
            #print(f'{self._dpath}')
            if not self._dpath.exists():
                return
            if not self._dpath.is_dir():
                errmsg = (f'Error creating directory'
                          f'"{self._dpath} is not a valid dir path"')
                raise argparse.ArgumentTypeError(errmsg)
            return

        def instantiate(self):
            if not self._dpath.exists():
                try:
                    # Attempt to create the directory
                    os.makedirs(self._dpath, exist_ok=True)
                except OSError as e:
                    raise argparse.ArgumentTypeError(f'Error creating directory'
                                                     f'"{self._dpath}": {e}')
            return self._dpath

    # TODO(epr): add a remote service or file system
    parser.add_argument('--logdir',
                        type=Dir,
                        default=os.path.expanduser(f'~/log/zh-02_validation'),
                        help='Path to the log file directory, %(default)s,')
    args = parser.parse_args()
    _wait = args.wait

    today = datetime.date.today()
    tdystr = today.strftime("%Y%m%d")
    stty = '/dev/ttyUSB0'

    logdir = args.logdir.instantiate()
    lpath = logdir / f'{tdystr}.log'

    run_uboot(lpath)
    sys.exit(0)

