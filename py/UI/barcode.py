#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

''' program to test barcode reading from the serial port via 
    /dev/ttyACM0
'''

import re
import serial
import select
import sys


#device_str = 'ZH-02 r1,Batch:09042402001,HWID:D4F1FB20004B1200,MAC:C4AC59A619B6,Rev: 2.0'

# Define the regular expression pattern
_bc_pat = r'(Z[AHT]-[0-9][0-9] .+),Batch:(\d+),HWID:([A-F0-9]+),MAC:([A-F0-9:]+),Rev: (\d+\.\d+)'

class BarCodeReader(object):
    def __init__(self, tty: str='ACM0', baud: int =11520, timeout: int =0):
        self._ttydev = f'/dev/tty{tty}'
        # Configure your serial port (adjust as needed)
        # Set timeout to 0 for non-blocking
        self._ser = serial.Serial(self._ttydev, baud, timeout=timeout)
    
    def read(self):
        while True:
            # Use select to wait for data on the serial port
            # Timeout of 1 second
            ready_to_read, _, _ = select.select([self._ser], [], [], 1)

            if self._ser in ready_to_read:
                # Data available, read it
                data = self._ser.read(self._ser.in_waiting)
                barcode = data.decode()
                print(f'barcode: {barcode}')
                #print(f'dotcode: {barcode.replace(' ', '.')}')
                return barcode
            else:
                # No data within the timeout, do other work
                print('    waiting for scan\007', end='\r')

    def validate(self, barcode:str):
        # Match the pattern and extract values
        match = re.match(_bc_pat, barcode)

        if match:
            model = match.group(1)
            batch_number = match.group(2)
            hwid = match.group(3)
            mac = match.group(4)
            revision = match.group(5)

            res_dict = {
                'found' : True,
                'Model' : model,
                'Batch' : batch_number,
                'HWID' : hwid,
                'MAC' : mac,
                'Revision' : revision}
            #print(f"Model: {model}")
            #print(f"Batch: {batch_number}")
            #print(f"HWID: {hwid}")
            #print(f"MAC: {mac}")
            #print(f"Revision: {revision}")
        else:
            res_dict = { 'found' : False}
            #print("No match found.")
        return res_dict




if __name__=='__main__':
    import argparse
    bcr = BarCodeReader()
    barcode = bcr.read()
    print(f'barcode: {barcode}')
    rdict = bcr.validate(barcode)
    print(rdict)

