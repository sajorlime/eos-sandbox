#!/usr/bin/env python3
#

""" if-extract.py
    exttracts the IP address from the output of ifconfig, 
    it only finds the first such value.
    ifconfig eth0 | if-extract.py
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import os
import os.path
import sys


def my_input():
    try:
        ins = input()
    except EOFError:
        print("Not Found")
        sys.exit(1)
    if not ins:
        return " "
    return ins
        

while True:
    line = my_input()
    inet_pos = line.find('inet addr:')
    if inet_pos < 0:
        continue
    end_pos = line.find(' ', inet_pos + 8);
    if end_pos < 0:
        print("Space Not Found")
        sys.exit(2)
    print(line[inet_pos + 10:end_pos])
    break;

sys.exit(0)

