#!/usr/bin/env python3

#

""" mergertags.py
    hack program to merge tag values into test iso team files
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""


__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import glob
import os
import os.path
import re
import sys

import filefilter


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = 'pass tags and Team file')
    parser.add_argument('tags',
                        help = 'tags file')
    parser.add_argument('team',
                        help = 'team file')
    myargs = parser.parse_args()

    if os.path.exists(myargs.team):
        team = myargs.team
    else:
        mteams = glob.glob('%s*' % myargs.team)
        print('glob:', mteams, file=sys.stderr)
        if len(mteams) == 1:
            team = mteams[0]
        else:
            print('File: %s does not exist' % myargs.team, file=sys.stderr)
            sys.exit(-1)
    if not os.path.exists(myargs.tags):
        print('File: %s does not exist' % myargs.tags, file=sys.stderr)
        sys.exit(-1)

    tags = filefilter.ReadFile(myargs.tags, interest=lambda l : l, strip='\n')
    team = filefilter.ReadFile(team, strip='\n')
    teamout = []
    tindex = 0
    for tline in team:
        if tline and tline[0].isdigit():
            break
        teamout.append(tline)
        tindex += 1
    tplayers = team[tindex:]

    if len(tags) < len(tplayers):
        t1 = 1
        t2 = 257
        while len(tags) < len(tplayers):
            tag = '%08X, %08X' %(t1, t2)
            tags.append(tag)
            t1 += 1
            t2 +=1
    itags = 0
    for player in tplayers:
        player = player.replace('\t', ' ')
        tag = tags[itags].replace('\t', ' ')
        nplayer = player.replace('00000000, 00000000', tag)
        if nplayer == player:
            print('TAG replacement failed for:', player, file=sys.stderr)
        itags += 1
        teamout.append(nplayer)
    with open(myargs.team, 'w') as th:
        for tline in teamout:
            print(tline.strip(), file=sys.stdout)
            print(tline.strip(), file=th)

