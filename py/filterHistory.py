#!/usr/bin/env python3
#
""" 
    Read a list and returns a list with unique items.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import os
import sys

import filefilter
import uniqueList

if __name__=='__main__':
    X = lambda l, n : l.strip()
    I = lambda l : len(l) > 3 and l[0] not in '0123456789\[\] ;^$'
    hsize = int(os.getenv('HISTSIZE', '1000'))
    ignore_list = [
                    'Sta',
                    'Pul',
                    'Pus',
                    'eal',
                    'Get',
                    'Put',
                    'vfu',
                    'venv',
                    'vlo',
                    'pwd',
                    'vi',
                    'vt',
                    'ls',
                    'mv',
                    'cp',
                    'cat',
                    'dif',
                    'Dif',
                    'Mak',
                    'mak',
                    'Bra',
                    ]
    i_list = []
    for rfile in sys.argv[1:]:
        i_list += filefilter.ReadFile(rfile, xform=X, interest=I)
    o_list = uniqueList.uniqueList(i_list, ifilter=ignore_list, any_match=True)
    for item in o_list:
        print(item)
   

