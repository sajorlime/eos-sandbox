#!/usr/bin/env python3
#


""" Program to take a list of execution paths and return a unique
    set in the order of appearance so as not to change the meaning.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

#from sets import Set

import os
import sys

# local import
#import args
import argparse
 
Usage = """
usage: uniquePath path
       use quotes if spaces in path
       takes any unix list path list and makes each element unique.
"""

def unique_path(paths, sep=':', verbose=False):
    if len(paths) == 0:
      usage ("Oops you need a path ")

    # if the path had spaces in it, the value might not be passed correctly to 
    # uniquePath as a single argument, so attempt to reconstruct.

    if len(paths) == 1:
      if verbose: print("path len was 1", file=sys.stderr)
      print(paths[0])
      sys.exit(0)

    path_list = paths.split(':')
    path_set = set()
    if verbose: print(path_set, file=sys.stderr)

    if verbose: print(path_list, file=sys.stderr)

    new_path = []

    for path_item in path_list:
      if path_item:
        if not path_item in path_set:
          path_set.add(path_item)
          new_path.append(path_item)
        elif verbose:
            print("dup:%s" % path_item, file=sys.stderr)


    #print pathSet
    #for path_item in new_path:
    #  path=path + "%s:" % path_item
    path = sep.join(['%s' % p for p in new_path])
    return path

if __name__=='__main__':
    parser = argparse.ArgumentParser(
             description='take a PATH as input and output unique path in input order ')
    parser.add_argument('path',
                        nargs='?',
                        help='store_true')
    parser.add_argument('--verbose', '-v', 
                        action='store_true')
    parser.add_argument('--nl',
                        action='store_true',
                        help='use newlines rather then ":" as separator')
    args = parser.parse_args()

    if args.nl:
        sep = '\n'
    else:
        sep = ':'

    path = args.path
    if not path:
        path = os.getenv('PATH')

    print(unique_path(path, sep=sep, verbose=args.verbose))

