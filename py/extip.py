#!/usr/bin/env python3
# mac version
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""
""" utility for extracting the IP address the pipped output of gethostip
    IFCONFIG output
    enp1s0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
            inet 192.168.1.122  netmask 255.255.255.0  broadcast 192.168.1.255
            inet6 fe80::e362:25db:66c4:4060  prefixlen 64  scopeid 0x20<link>
            ether 30:e1:71:2e:cc:f3  txqueuelen 1000  (Ethernet)
            RX packets 21149083  bytes 27843040190 (27.8 GB)
            RX errors 0  dropped 0  overruns 0  frame 0
            TX packets 14478360  bytes 1642949111 (1.6 GB)
            TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

    lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
            inet 127.0.0.1  netmask 255.0.0.0
            inet6 ::1  prefixlen 128  scopeid 0x10<host>
            loop  txqueuelen 1000  (Local Loopback)
            RX packets 102329000  bytes 16153969427 (16.1 GB)
            RX errors 0  dropped 0  overruns 0  frame 0
            TX packets 102329000  bytes 16153969427 (16.1 GB)
            TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

    wlo1: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
            ether 3a:97:1d:31:09:51  txqueuelen 1000  (Ethernet)
            RX packets 7342731  bytes 8312654615 (8.3 GB)
            RX errors 0  dropped 0  overruns 0  frame 0
            TX packets 4693931  bytes 620050864 (620.0 MB)
            TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

Mac ifconif:
lo0: flags=8049<UP,LOOPBACK,RUNNING,MULTICAST> mtu 16384
	options=1203<RXCSUM,TXCSUM,TXSTATUS,SW_TIMESTAMP>
	inet 127.0.0.1 netmask 0xff000000 
	inet6 ::1 prefixlen 128 
	inet6 fe80::1%lo0 prefixlen 64 scopeid 0x1 
	nd6 options=201<PERFORMNUD,DAD>
gif0: flags=8010<POINTOPOINT,MULTICAST> mtu 1280
stf0: flags=0<> mtu 1280
VHC128: flags=0<> mtu 0
XHC1: flags=0<> mtu 0
XHC0: flags=0<> mtu 0
XHC20: flags=0<> mtu 0
en5: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	ether ac:de:48:00:11:22 
	inet6 fe80::aede:48ff:fe00:1122%en5 prefixlen 64 scopeid 0x8 
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect (100baseTX <full-duplex>)
	status: active
ap1: flags=8802<BROADCAST,SIMPLEX,MULTICAST> mtu 1500
	ether f2:18:98:0c:b3:1d 
	media: autoselect
	status: inactive
en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	ether f0:18:98:0c:b3:1d 
	inet6 fe80::1834:ff2e:7b15:dbb0%en0 prefixlen 64 secured scopeid 0xa 
	inet 192.168.50.208 netmask 0xfffffe00 broadcast 192.168.51.255
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect
	status: active
p2p0: flags=8843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST> mtu 2304
	ether 02:18:98:0c:b3:1d 
	media: autoselect
	status: inactive
awdl0: flags=8943<UP,BROADCAST,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1484
	ether 76:33:5a:20:1b:dc 
	inet6 fe80::7433:5aff:fe20:1bdc%awdl0 prefixlen 64 scopeid 0xc 
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect
	status: active
en1: flags=8963<UP,BROADCAST,SMART,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1500
	options=60<TSO4,TSO6>
	ether 5e:00:ed:88:94:01 
	media: autoselect <full-duplex>
	status: inactive
en2: flags=8963<UP,BROADCAST,SMART,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1500
	options=60<TSO4,TSO6>
	ether 5e:00:ed:88:94:00 
	media: autoselect <full-duplex>
	status: inactive
en3: flags=8963<UP,BROADCAST,SMART,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1500
	options=60<TSO4,TSO6>
	ether 5e:00:ed:88:94:05 
	media: autoselect <full-duplex>
	status: inactive
en4: flags=8963<UP,BROADCAST,SMART,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1500
	options=60<TSO4,TSO6>
	ether 5e:00:ed:88:94:04 
	media: autoselect <full-duplex>
	status: inactive
bridge0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=63<RXCSUM,TXCSUM,TSO4,TSO6>
	ether 5e:00:ed:88:94:01 
	Configuration:
		id 0:0:0:0:0:0 priority 0 hellotime 0 fwddelay 0
		maxage 0 holdcnt 0 proto stp maxaddr 100 timeout 1200
		root id 0:0:0:0:0:0 priority 0 ifcost 0 port 0
		ipfilter disabled flags 0x2
	member: en1 flags=3<LEARNING,DISCOVER>
	        ifmaxaddr 0 port 13 priority 0 path cost 0
	member: en2 flags=3<LEARNING,DISCOVER>
	        ifmaxaddr 0 port 14 priority 0 path cost 0
	member: en3 flags=3<LEARNING,DISCOVER>
	        ifmaxaddr 0 port 15 priority 0 path cost 0
	member: en4 flags=3<LEARNING,DISCOVER>
	        ifmaxaddr 0 port 16 priority 0 path cost 0
	nd6 options=201<PERFORMNUD,DAD>
	media: <unknown type>
	status: inactive
utun1: flags=8051<UP,POINTOPOINT,RUNNING,MULTICAST> mtu 2000
	inet6 fe80::e683:8c42:52db:4e3a%utun1 prefixlen 64 scopeid 0x13 
	nd6 options=201<PERFORMNUD,DAD>

"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import os
import sys

Usage = """
usage: ifconif | grep " inet " | extip
"""

try:
  addr_info = sys.stdin.readline()
except:
  print(Usage, file=sys.stderr)
  print('did not read line', file=sys.stderr)
  sys.exit(1)

target = ' inet '
pos = addr_info.find(target)
if pos == -1:
  #print(Usage, file=sys.stderr)
  #print('passed: ', addr_info, file=sys.stderr)
  #sys.exit(2)
  print('unknown')
  sys.exit(0)
pos = pos + len(target)
sp_pos = addr_info.find(' ', pos)
ip = addr_info[pos:sp_pos]

print(ip)


