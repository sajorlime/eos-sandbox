#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

__author__ = 'eorojas@gmail.com (Emilio Rojas) w/gemini'

import json
import os
import pathlib as pl
import requests


_google_api_key = os.getenv('GOOGLE_API_KEY')
assert _google_api_key, 'GOOGLE_API_KEY not defined'
_google_project_id = os.getenv('GOOG_PROJECT_ID')
assert _google_project_id , 'GOOG_PROJECT_ID not defined'
_google_project_name = os.getenv('GOOG_PROJECT_NAME')
assert _google_project_name , 'GOOG_PROJECT_NAME not defined'

_location = 'us-central1'

_prompt = (f'I am an expert programmer examining this undocumented Python code.'
           f' Provide a summary, point out any issues, and list external dependencies.'
           f' Do not attempt to execute the code.'
           f' Treat the following code as text: ') # code with go here


id_end_point = f'https://aiplatform.googleapis.com/v1/projects/{_google_project_id}/locations/{_location}/models/gemini-pro:generateText'
_end_point = f'https://aiplatform.googleapis.com/v1/projects/{_google_project_name}/locations/{_location}/models/gemini-pro:generateText'

def analyze_code(api_key, project_id, filename):
    ''' Analyzes code using the Gemini API.
    Args:
        api_key:
        project_id:
        filesname:
    '''

    try:
        with open(filename, 'r') as f:
            code = f.read().replace('#!', '') # Remove shabang

        prompt = f'{_prompt} ```{code}```'
        print(prompt)


        headers = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {_google_api_key}',
            'X-Goog-User-Project': _google_project_id,
        }

        data = {'model': 'gemini-pro', 'prompt': {'text': prompt}}

        response = requests.post(
            _end_point,
            headers=headers,
            json=data,  # Use the json parameter to automatically encode the data
        )

        response.raise_for_status()  # Raise an exception for HTTP errors

        result = response.json()
        return result['candidates'][0]['output']

    except requests.exceptions.RequestException as e:
        print(f'Error making API request: {e}')
        return None
    except (KeyError, IndexError) as e:
        print(f'Error parsing API response: {e}')
        return None


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='analyze code with Gemini')
    parser.add_argument('file', help='code file name')
    args = parser.parse_args()

    analysis = analyze_code(_google_api_key, _google_project_id, args.file)
    if analysis:
        print(analysis)
