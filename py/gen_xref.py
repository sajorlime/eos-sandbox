#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import argparse
import ast
import google
import google.generativeai as genai
import json
import os
import pathlib as pl
import sys

def append_mod_name(mod_name: str, fpath: str, xdict: dict):
        if mod_name not in xdict:
            xdict[mod_name] = list()
        xdict[mod_name].append(fpath)


def analyze_imports(file_path, xref_dict):
    '''
    Analyzes a Python file and extracts import information,
    updating the xref_dict.

    Args:
        file_path (str): The path to the Python file.
        xref_dict (dict): A dictionary to store cross-reference data.
    Returns:
        None, update xref_dict
    '''
    with open(file_path, 'r') as f:
        try:
            tree = ast.parse(f.read())
        except SyntaxError as e:
            xref_dict['syntax_error'].append(f'{file_path} - {e}')
            print(f'Skipping file due to SyntaxError: {file_path} - {e}')
            return

    for node in ast.walk(tree):
        if isinstance(node, (ast.Import, ast.ImportFrom)):
            for alias in node.names:
                mod_name = alias.name if isinstance(node, ast.Import) \
                                         else node.module
                if mod_name in sys.builtin_module_names:
                    append_mod_name(mod_name, file_path, xref_dict['builtin'])
                    xref_dict['builtin'][mod_name].append(file_path)
                elif mod_name in sys.modules:
                    append_mod_name(mod_name, file_path, xref_dict['sys'])
                else:
                    append_mod_name(mod_name, file_path, xref_dict['local'])

def gen_xref(directory):
    '''
    Generates a cross-reference of modules and the files that import them.

    Args:
        directory (str): The path to the directory.

    Returns:
        dict: A dictionary mapping module names to sets of files that
              import them.
    '''
    xref_dict = {'builtin':{}, 'sys':{}, 'local':{}, 'syntax_error': []}
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith('.py'):
                file_path = os.path.join(root, file)
                analyze_imports(file_path, xref_dict)
    return xref_dict


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='gen xref')
    parser.add_argument('target_dir',
                        default=None,
                        help='starting directory')
    args = parser.parse_args()
    xref_dict = gen_xref(args.target_dir)

    # Append cross-reference to each file
    for root, dirs, files in os.walk(args.target_dir):
        for file in files:
            if file.endswith('.py'):
                file_path = os.path.join(root, file)
    xref_json = json.dumps(xref_dict, indent=4)
    xrjp = pl.Path(args.target_dir) / pl.Path('xref.json')
    with xrjp.open('w') as xrjfd:
        xrjfd.write(xref_json)
