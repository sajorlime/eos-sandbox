#!/usr/bin/python3
#

""" wordllefind.py
    prograram to quess wordly
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import os
import os.path
import random
import re
import sys

def filter_doubles(w):
    wsl = sorted(w)
    l = None
    for c in wsl:
        if c == l:
            return None
        l = c
    return w

def pick_one(plist):
    #print('PO:', len(plist))
    pi = int(random.random() * len(plist))
    return (plist[pi], len(plist))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='play workle, or solve today\'s puzzle')
    parser.add_argument('--ref', '-r', 
                        default=None,
                        help = 'write ref words to param')
    parser.add_argument('--freq', '-f', 
                        default=None,
                        help = 'calculate frequncies')
    parser.add_argument('--words', '-w', 
                        dest='words',
                        type=str,
                        default=os.getenv('WREF'),
                        help = 'override the $WORDS enviroment variable, default: %(default)s')
    myargs = parser.parse_args()
    def ihelp():
        print('?: gives this message')
        print('-X says that the X is not in workd')
        print('-Xn says that the X is not in position n')
        print('+Xn says that the X is in position n')
        print('where X in (a, z), and')
        print( '      n in (0, 4)')
        print( '= set last try')
        print( '% set match + . -, yes, maybe, no')
        print('N: new word')
        print('R: random word from current list')
        print('S: pick a word to start')
        print('T: define a target word to print')
        print('Q: quits')
        return None

    target_file = '/home/eo/tmp/target'
    try:
        target = open(target_file).read().strip()
    except IOError:
        target = ''
    allwords = open(myargs.words).readlines()
    #words = [ w.strip() for w in allwords if w[0] != w[0].upper()]
    refwords = [ w.strip().lower() for w in allwords ]
    #refwords = [ w for w in words if len(w) == 5 and "'" not in w ]
    if myargs.ref:
        with open(myargs.ref, 'w') as rd:
            for w in refwords:
                print(w, file=rd)
        sys.exit()
    cnt_table = [ 0 for i in range(26) ]
    if myargs.freq:
        for w in refwords:
            for c in w:
                i = ord(c) - ord('a')
                cnt_table[i] = cnt_table[i] + 1
        print(cnt_table)
        sys.exit()
    inwords = refwords
    xwords = [ w for w in inwords if filter_doubles(w) ]
    # keep most frequent
    xwords = [ w for w in xwords if 'r' in w 
                                 or 't' in w
                                 or 's' in w
                                 or 'c' in w
                                 or 'h' in w]
    awords = [ w for w in xwords if 'a' in w ]
    ewords = [ w for w in xwords if 'e' in w ]
    iwords = [ w for w in xwords if 'i' in w ]
    owords = [ w for w in xwords if 'o' in w ]
    uwords = [ w for w in xwords if 'u' in w ]
    aewords = [ w for w in awords if 'e' in w ]
    aiwords = [ w for w in awords if 'e' in w ]
    aowords = [ w for w in awords if 'e' in w ]
    auwords = [ w for w in awords if 'e' in w ]
    eiwords = [ w for w in ewords if 'i' in w ]
    eowords = [ w for w in ewords if 'o' in w ]
    euwords = [ w for w in ewords if 'u' in w ]
    iowords = [ w for w in iwords if 'o' in w ]
    iuwords = [ w for w in iwords if 'u' in w ]
    eowords = [ w for w in ewords if 'o' in w ]
    aeiwords = [ w for w in eiwords if 'a' in w ]
    aiuwords = [ w for w in aiwords if 'u' in w ]
    aouwords = [ w for w in aowords if 'u' in w ]
    aeowords = [ w for w in aowords if 'e' in w ]
    eiuwords = [ w for w in eiwords if 'u' in w ]
    iouwords = [ w for w in iowords if 'u' in w ]
    vowels_list =(aeiwords, aiuwords, aeowords, iouwords, eiuwords)
    pick_list = [ w for w in aeiwords if 'r' in w
                                        or 't' in w
                                        or 's' in w
                                        or 'n' in w ]
    inpicks = []
    found = 5 * ['_']
    try_word = 'xxxxx'
    letters = []
    ihelp()
    while True:
        try:
            sletters = set(letters)
            letters = list(sletters)
            print('?[-Xn]|+Xn]NQq',
                  '|'.join(inpicks),
                  letters,
                  len(inwords),
                  target,
                  ''.join(found),
                  try_word)
            inbuf = sys.stdin.readline().strip()
            if '?' in inbuf:
                ihelp()
                continue
            if inbuf[0].isupper():
                if 'L' == inbuf[0]:
                    print(inwords)
                    continue
                if 'N' == inbuf[0]:
                    inwords = refwords
                    inpicks = []
                    letters = []
                    found = 5 * ['_']
                elif 'R' == inbuf[0]:
                    if len(inwords) > 20:
                        rwords = [ w for w in inwords if filter_doubles(w) ]
                    else:
                        rwords = inwords
                    try_word = pick_one(rwords)[0]
                    print('Try:', try_word)
                    continue
                elif 'S' == inbuf[0]:
                    vi = 0
                    print('S:', inbuf, len(inbuf))
                    if len(inbuf) == 1:
                        print(pick_list)
                        try_word = pick_one(pick_list)[0]
                        print('Try:', len(pick_list), try_word)
                        continue
                    elif len(inbuf) > 1:
                        if not inbuf[1].isdigit():
                            continue
                        vi = int(inbuf[1])
                        if vi >= len(vowels_list):
                            vi = 0
                    vlist = vowels_list[vi]
                    if len(inbuf) > 2:
                        if ' ' in inbuf:
                            sword = inbuf.split()[1]
                        if sword in vlist:
                            print(sword)
                        else:
                            print(vlist)
                    #print(aeiwords)
                    #wi = int(random.random() * len(vlist))
                    try_word = pick_one(vlist)[0]
                    print(vlist)
                    print('Try:', len(vlist), vi, try_word)
                    continue
                elif 'T' == inbuf[0]:
                    target = inbuf[1:].strip()
                    open(target_file, 'w').write(target)
                    continue
                elif 'Q' == inbuf[0]:
                    exit(0)
                ihelp()
                continue
            if '-' == inbuf[0]:
                exletter = inbuf[1].lower()
                if len(inbuf) == 2:
                    inwords = [ w for w in inwords if exletter not in w ]
                    print(exletter, len(inwords))
                else:
                    expos = int(inbuf[2])
                    if expos > 4:
                        ihelp()
                        continue
                    print(expos, 'X')
                    inwords = [ w for w in inwords if exletter != w[expos] ]
                    inwords = [ w for w in inwords if exletter in w ]
                    letters.append(exletter)
                    print(exletter, len(inwords))
                inpicks.append(inbuf)
            elif '+' == inbuf[0]:
                inletter = inbuf[1].lower()
                letters.append(inletter)
                if len(inbuf) == 2:
                    inwords = [ w for w in inwords if inletter in w ]
                else:
                    inpos = int(inbuf[2])
                    if inpos > 4:
                        ihelp()
                        continue
                    found[inpos] = inletter
                    inwords = [ w for w in inwords if inletter == w[inpos] ]
                inpicks.append(inbuf)
            elif '%' == inbuf[0]:
                if len(inbuf) != 6:
                    ihelp()
                    continue
                for i in range(1, 6):
                    j = i - 1
                    letter = try_word[j]
                    if inbuf[i] == '+':
                        found[j] = try_word[j]
                        print('IL', inbuf, letter, letters, j)
                        try:
                            inwords = [ w for w in inwords if letter == w[j] ]
                            #newwords = []
                            #for w in inwords:
                            #    if len(w) != 5:
                            #        print(w, 'is len', len(w))
                            #    if letter == w[j]:
                            #        newwords.append(w)
                            #inwords = newwords
                        except IndexError as ie:
                            print(ie)
                            print('j:', j)
                            
                        letters.append(letter)
                        print('ILs', inbuf, letter, letters, j)
                    elif inbuf[i] == '-' and letter not in letters:
                        inwords = [ w for w in inwords if letter not in w ]
                    elif inbuf[i] == '.':
                        letters.append(letter)
                        inwords = [ w for w in inwords if letter != w[j] and letter in w ]
                    else:
                        print('ELSE', inbuf, i, j, letter)
                        ihelp()
                #continue
            elif '=' == inbuf[0]:
                try_word = inbuf[1:6]
                continue
            elif '/' == inbuf[0]:
                sword = inbuf[1:]
                if sword in inwords:
                    print(sword, 'in inwords')
                else:
                    print(sword, 'not in inwords')
                continue
            else:
                print('else', inbuf, i, j, letter)
                ihelp()
                continue
                sys.exit(0)
            inlen =  len(inwords)
            print('Len inwords:', inlen)
            if inlen < 10:
                for w in inwords:
                    print(w)
        except IndexError as ie:
            print('index error')
            ihelp()
            continue
        except ValueError as ve:
            print('value error')
            ihelp()
            continue

        


