#!/usr/bin/env python3
#
""" 
    Read a list and returns a list with unique items.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import os
import sys

import regexfilter

def matches(item, flist):
    for f in flist:
        if f in item:
            return True
    return False
    

def uniqueList(inlist, ifilter=None, print_last=False, any_match=False):
    """
    Returns a list from the inlist that is unique and in the same oder.
    Args:
        inlist: any list
        ifilter: has in operator, if true skip item
        path: the inList is a path and we want to return a ':' concatenated list.
    Returns:
        the elements in the list in order with dups removed.
    """
    outlist = []
    indict = {}
    pos = 0
    if not ifilter:
        ifilter = []
    print(ifilter)
    ifilter.sort()
    sfilter = set(ifilter)
    match_dict = {}
    for item in inlist:
        # TODO(epr): use regular expression, use partial matches
        if any_match:
            if matches(item, sfilter):
                continue
        elif item in sfilter:
            print("I:", item)
            continue
        if item in indict:
            if print_last:
                indict[item] = pos
                pos += 1
                continue
        outlist.append(item)
        indict[item] = pos
        pos += 1
    #print(indict)
    pl_items = sorted(indict, key=indict.__getitem__)
        
    return pl_items


if __name__=='__main__':
    import filefilter
    parser = argparse.ArgumentParser(description='input a list of text lines and output a unique list in order ')
    parser.add_argument('--filter', '-F', 
                        dest='ifilter',
                        action='append',
                        default=[],
                        help='add filter text')
    parser.add_argument('--file', '-f', 
                        dest='file',
                        default='-',
                        help='file to filter')
    parser.add_argument('--print_last', '-l', 
                        dest='print_last',
                        action='store_true',
                        default=False,
                        help='print at end, otherwise print in order')
    myargs = parser.parse_args()
    olist = uniqueList(filefilter.ReadFile(myargs.file, strip='\n'), myargs.ifilter, myargs.print_last)
    for item in olist:
        print(item)
   

