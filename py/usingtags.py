#!/usr/bin/env python3
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

""" usingtags.py
    takes the output of gfiles -w ^using | grep =
    and turn this into tags:
        ./src/zainar_private_utilities_lite.hpp:37:using i16 = int16_t;
    ->
        i16	./src/zainar_private_utilities_lite.hpp	37
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import os
import os.path
import shutil
import sys

import filefilter
import regexfilter



def gen_tags(files, interest, nointerest, must, keywords):
    """
    Generate tags for for the list of files.
    Args:
        files: a list of files
        interest: a string reg ex for chosing lines in the file.
        noitnerest: expressions to skip line
        must: expressions that must be present in list that are not keywords
        keywords: keywords are skipped when creating a tag.
    """
    def my_interest(line):
        """
        returns True iff interest and not nointerest
        """
        if interest.accept(line):
            if nointerest.accept(line):
                return False
            if keywords.accept(line):
                return True
            if must.accept(line):
                return True
            return False
        return False

    tagables = []
    for file in files:
        tagables += filefilter.ReadFile(file, interest = my_interest, xform = lambda l, n : (file, l.rstrip()))

    taglines = {}
    for tagpair in tagables:
        #print(tagline.rstrip())
        tagline = tagpair[1]
        if keywords.accept(tagline):
            if '=' in tagline:
                sline = tagline.split(' ')
                tag = sline[1].split('=')[0]
            else:
                sline = tagline.split(' ')
                #print(sline[1].strip().split('('))
                tag = sline[1].strip().split('(')[0]
            taglines[tag] = tagpair
        else:
            taglines[tagline.split('=')[0]] = tagpair
    return taglines

def gentag(key, line_info):
    spat = line_info[1].find('${')
    if spat > 1:
        # bash substitutions are problamacit so remvoe them.
        # TODO(epr): should be command line option
        return '%s\t%s\t/^%s/' % (key, line_info[0], line_info[1][0:spat])
    return '%s\t%s\t/^%s$/' % (key, line_info[0], line_info[1])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='create tags from using directives, in C++')
    parser.add_argument('--ifile',
                        type=str,
                        default='using-lines',
                        help='using lines file to convert: %(default)s')
    parser.add_argument('--ofile',
                        type=str,
                        default='using-tags',
                        help='using tags file out')
    args = parser.parse_args()
    lines = filefilter.ReadFile(args.ifile, strip='\n')
    btags = []
    for l in lines:
        ldata = l.split(':')
        btags.append(ldata)
        #print(ldata)
    tags = []
    for bt in btags:
        print(bt)
        uloc = bt[2].find('using ')
        tstart = bt[2][uloc + 6:]
        sloc = tstart.find(' ')
        tline = '%s\t%s\t%s' % (bt[2][uloc + 6 : uloc + sloc + 6], bt[1], bt[2])
        #print(bt[2], uloc, sloc)
        tags.append(tline)
    tags.sort()
    #print(tags)
    with open(args.ofile, 'w') as of:
        for et in tags:
            print(et, file=of)
    sys.exit()
    keys = sorted(ldict.keys())
    for key in keys:
        linfo = ldict[key]
        tag = gentag(key, linfo)
        #print('%s\t%s\t/^%s$/' % (key, linfo[0], linfo[1]), file=ofd)
        print(tag, file=ofd)

