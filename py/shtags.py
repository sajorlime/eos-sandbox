#!/usr/bin/env python3
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

""" shtags.py
    Takes a list of files and creates tags for the elements
    in files with simple.  By default these are bash shell elements,
    but by passing regular expressions this can be extended.

    This program creates tags for all values in a list
    of ini files.
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import os
import os.path
import shutil
import sys

import filefilter
import regexfilter


usage = """
initags [-o=out-tags-file] list of ini files.

If no -o option, writes to stdout.
"""

def key_filter(line):
    """ the line filter takes any line that does not start with white
        space or #.
    Args:
        line: the object being accepted or rejected.
    Returns:
        true if the lines first charactar is 0-9, A-Z, or a-z.
    """
    return line[0].isalnum()

def key_xform(line, num):
    i_tab = line.find('\t');
    i_space = line.find(' ');

    if i_space ==  -1:
        i_space = i_tab
    if i_tab ==  -1:
        i_tab = i_space
    i_white = i_tab
    if i_white == -1:
        return ["", "", -1]
    if i_white > i_space:
        i_white = i_space
    return [line[0:i_white], line[i_white:].strip(), num]

def gen_tags(files, interest, nointerest, must, keywords):
    """
    Generate tags for for the list of files.
    Args:
        files: a list of files
        interest: a string reg ex for chosing lines in the file.
        noitnerest: expressions to skip line
        must: expressions that must be present in list that are not keywords
        keywords: keywords are skipped when creating a tag.
    """
    def my_interest(line):
        """
        returns True iff interest and not nointerest
        """
        if interest.accept(line):
            if nointerest.accept(line):
                return False
            if keywords.accept(line):
                return True
            if must.accept(line):
                return True
            return False
        return False

    tagables = []
    for file in files:
        tagables += filefilter.ReadFile(file,
                                        interest=my_interest,
                                        xform=lambda l, n : (file, l.rstrip()))

    taglines = {}
    for tagpair in tagables:
        #print(tagline.rstrip())
        tagline = tagpair[1]
        if keywords.accept(tagline):
            if '=' in tagline:
                sline = tagline.split(' ')
                tag = sline[1].split('=')[0]
            else:
                sline = tagline.split(' ')
                #print(sline[1].strip().split('('))
                tag = sline[1].strip().split('(')[0]
            taglines[tag] = tagpair
        else:
            taglines[tagline.split('=')[0]] = tagpair
    return taglines

def gentag(key, line_info):
    spat = line_info[1].find('${')
    if spat > 1:
        # bash substitutions are problamacit so remvoe them.
        # TODO(epr): should be command line option
        return '%s\t%s\t/^%s/' % (key, line_info[0], line_info[1][0:spat])
    return '%s\t%s\t/^%s$/' % (key, line_info[0], line_info[1])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='create tags from a list of files')
    parser.add_argument(dest='src',
                        metavar='SRC',
                        nargs='*',
                        type=str,
                        help='a source file')
    parser.add_argument('--interest', '-i',
                        dest='interest',
                        type=regexfilter.RegexFilter,
                        default=regexfilter.SRegexFilter('+^[A-z]'),
                        help='set the interest, , defaults to lines with \"+^[A-z]\"')
    parser.add_argument('--nointerest', '-n',
                        dest='no_interest',
                        type=regexfilter.RegexFilter,
                        default=regexfilter.SRegexFilter('+do |do\\n|done |done\\n|for | while |if |set'),
                        help='set the no interest, , defaults to lines with \"+^[A-z]\"')
    parser.add_argument('--keyword', '-k',
                        dest='keywords',
                        type=regexfilter.RegexFilter,
                        default=regexfilter.SRegexFilter('+function|export|alias'),
                        help='set the keywords, , defaults to lines with \"+function|export|alias\"')
    parser.add_argument('--must', '-m',
                        dest='must',
                        type=regexfilter.RegexFilter,
                        default=regexfilter.SRegexFilter('+='),
                        help='lines not containing a keyword must have this string, defaults to "="')
    parser.add_argument('--out', '-o',
                        dest='out',
                        metavar='OUT',
                        type=str,
                        default='-',
                        help = 'the destination tags file, defaults to stdout')
    myargs = parser.parse_args()

    #print(myargs.src)
    #print(myargs.keywords)
    #print(myargs.interest)
    #print(myargs.out)
    ldict = gen_tags(myargs.src, myargs.interest, myargs.no_interest, myargs.must, myargs.keywords)

    if myargs.out == '-':
        ofd = sys.stdout
    else:
        ofd = open(myargs.out, 'w')

    #print ('!_TAG_FILE_SORTED   1   /0=unsorted, 1=sorted, 2=foldcase/', file = ofd)

    # example output
    # Add svn.sh  /^function Add()$/;"    f

    keys = sorted(ldict.keys())
    for key in keys:
        linfo = ldict[key]
        tag = gentag(key, linfo)
        #print('%s\t%s\t/^%s$/' % (key, linfo[0], linfo[1]), file=ofd)
        print(tag, file=ofd)

