#!/usr/bin/env python3
# aif/dmember.py
# 11/18/2022
# based class for loading data members

import argparse

from dataclasses import dataclass

@dataclass
class DMember(object):
    ''' Base class for Data Members
        Note(epr): I settled on this after playing wiht dataclass definitions.
        DMembers are a class that we can use for argument parsing and data
        value holding.
    '''
    def __init__(self,
                 dmargs,
                 dmhelp,
                 dmdefault,
                 dmtype,
                 dmname,
                 dmvalue,
                 dmmin,
                 dmmax,
                 dmdesc):
        self._args = dmargs
        self._help = dmhelp
        self._default = dmdefault
        self._type = dmtype
        self._name = dmname
        self._value = dmvalue
        self._min = dmmin
        self._max = dmmax
        self._desc = dmdesc
    def __str__(self):
        return f'{self._args}, ' \
               f'{self._help}, ' \
               f'{self._default}, '\
               f'{self._type} '\
               f'{self._name}, '\
               f'{self._value}, '\
               f'{self._min}, '\
               f'{self._max}, '\
               f'{self._desc}'

    @property
    def dmargs(self):
        return self._args
    @property
    def dmhelp(self):
        return self._help
    @property
    def dmdefault(self):
        return self._default
    @property
    def dmtype(self):
        return self._type
    @property
    def dmtype(self):
        return self._name
    @property
    def dmvalue(self):
        return self._value
    @property
    def dmmin(self):
        return self._min
    @property
    def dmmax(self):
        return self._max
    @property
    def desc(self):
        return self._desc
    @staticmethod
    def check_limits(vs, vmin, vmax):
        ''' limit checker, called via get_lcheck
            Allows parameter values to  validate according to class members
        '''
        #print(f'{vs=}, {type(vs)=}')
        v = float(vs)
        if (v >= vmin) and (v <= vmax):
            return v
        raise argparse.ArgumentTypeError(f'{v} not [{vmin}, {vmax}]')
    def get_lcheck(self):
        '''
        Returns:
            climits limit checker with min and max wrapped in method
        '''
        def climits(vs):
            return DMember.check_limits(vs, self._min, self._max)
        return climits
    def dm_add_arg(self, parser):
        ''' add an argument the parser.
        Args:
            parser: is the local parser we want to add a parameter too.
        Returns:
            side effect on the parser
        '''
        #print(f'{self._help}')
        #print(f'{self._args=}')
        parser.add_argument(*self._args,
                           type=self.get_lcheck(),
                           default=self._default,
                           dest=self._name,
                           help=self._help)

def build_members(dmargs,
                 hbase,
                 dmdefault,
                 dmtype,
                 dmname,
                 dmvalue,
                 dmmin,
                 dmmax,
                 dmdesc):
    ''' takes all argument parameters and constructs a dictionary to allow
        construction of dmember with the required values and limit cheecks
    Args:
        dmnargs: the parameter line argument(s)
        hbase: the base info for help,
        remaining arguments should be clear.
    '''
    dmhelp = f'{hbase}, [{dmmin}, {dmmax}], default: %(default)s'
    #print(f'{type(dmhelp)}')
    return {
         'dmargs' : dmargs,
         'dmhelp' : dmhelp,
         'dmdefault' : dmdefault,
         'dmtype' : dmtype,
         'dmname' : dmname,
         'dmvalue' : dmvalue,
         'dmmin' : dmmin,
         'dmmax' : dmmax,
         'dmdesc' : dmdesc,
    }



