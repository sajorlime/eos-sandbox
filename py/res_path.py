#!/usr/bin/env python3
# rpath.py
'''
    code that finds result files. 
    Useful geting CSV files for ploting or otherwise examining.
'''

import os
import pathlib
import sys

result_root = os.getenv('COMP_RESULT_ROOT', 'results')

_epilog = '''
use --days (possibly by default) or --date to pick the day
use --tslots or --time in hhmmss to pick the particular run.
'''


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='find compliance results',
                                    epilog=_epilog)
    parser.add_argument('--results_dir', '-r',
                        default='results',
                        help='override path to result dir, default: %(default)s')
    parser.add_argument('--days', '-y',
                        type=int,
                        default=1,
                        help='days in the past, default: %(default)s')
    parser.add_argument('--date', '-d',
                        help='date as MMDD or MMDDYYYY')
    parser.add_argument('--time_slot', '-s',
                        type=int,
                        default=1,
                        help='hhmmss slot in the past, default: %(default)s')
    parser.add_argument('--time', '-t',
                        help='time as HHMM or HHMMSS')
    parser.add_argument('--show_dates', '-D',
                        action='store_true',
                        help='show the contents of dates selected')
    parser.add_argument('--show_hours', '-H',
                        action='store_true',
                        help='show the contents of HHMMSS selected')
    parser.add_argument('--show_files', '-F',
                        action='store_true',
                        help='show the files selected by of date and time')
    parser.add_argument('--show_all_files', '-A',
                        action='store_true',
                        help='show the files selected by of date and time')
    parser.add_argument('--show_wait', '-w',
                        action='store_true',
                        help='show the wait csv files')
    parser.add_argument('--ext', '-e',
                        help='w/show filters the context with *.ext')
    parser.add_argument('--base', '-b',
                        action='append',
                        help='w/show filters the context with *base*')
    parser.add_argument('--ignore',
                        action='append',
                        default=None,
                        help='ignore these directories: %(default)s')
    parser.add_argument('--debug',
                        action='store_true',
                        help='debug print')
    args = parser.parse_args()

    def drop_dirs(dir_list, drop_list):
        drops = []
        #print(dir_list)
        for n in drop_list:
            drops += [ d for d in dir_list if n in d.parts ]
        drops = set(drops)
        for d in drops:
            dir_list.remove(d)

    def res_dirs(rpath, ignores):
        ''' find the directories of interest
        Args:
            rpath: the result path
            ignores: list of names to ignore
        '''
        remonths = '[01][0-9]'
        redays = '[0-3][0-9]'
        reyears = '20[23][0-9]'
        mstr = f'{rpath}/{remonths}{redays}{reyears}'
        rdir = pathlib.Path(rpath)
        ddirs = [ d for d in rdir.iterdir() if d.match(mstr) ]
        #print(ddirs, type(ddirs), type(ignores))
        drop_dirs(ddirs, ignores)
        return ddirs

    # result dir
    rdirs = res_dirs(args.results_dir, args.ignore)
    #print(rdirs)
    # we will date or days
    if args.date:
        ddirs = [ d for d in rdirs if d.match(f'*{args.date}*') ]
        if not ddirs:
            print(f'{args.date} not found', file=sys.stderr)
            sys.exit(1)
        if len(ddirs) > 1:
            print(f'multiple directories for "{args.date}" found:')
            for dd in ddirs:
                print(f'  {dd}')
            print(f'Refine dates')
            sys.exit(0)
        ddir = ddirs[0]
    else:
        ddir = rdirs[-args.days]

    if args.show_dates:
        print('  possible dates:', file=sys.stderr)
        for dd in ddirs:
            print(f'    {dd}', file=sys.stderr)
        print(f'{ddir}')
        sys.exit(0)

    # time dirs
    tdirs = pathlib.Path(ddir)
    if args.time:
        ttdirs = [ t for t in tdirs.iterdir() if t.match(f'*{args.time}*') ]
        if not ttdirs:
            print(f'{args.time} not found', file=sys.stderr)
            sys.exit(1)
        if len(ttdirs) > 1:
            print(f'multiple directories for "{args.time}" found:')
            for tt in ttdirs:
                print(f'  {tt}')
            print(f'Refine time')
            sys.exit(0)
        ttdir = ttdirs[0]
    else:
        ttdirs = [ d for d in tdirs.iterdir() ]
        ttdir = ttdirs[-args.days]
    if args.show_hours:
        for td in ttdirs:
            print(f'    {td}', file=sys.stderr)
            ttdir = ttdirs[-args.time_slot]
        print(f'ttdir: {ttdir}')
        sys.exit(0)

    # files dir
    fdir = pathlib.Path(ttdir)
    fresults = [ f for f in ttdir.iterdir() ]
    if not args.show_wait:
        fresults = [ f for f in fresults if not f.match('*wait-*') ] 
    if (args.show_files and not (args.ext or args.base)) or args.debug:
        print('file results:', file=sys.stderr)
        for f in fresults:
            print(f'    {f}', file=sys.stderr)
        if not args.debug:
            sys.exit(0)

    if args.ext:
        if args.debug:
            print(f'ext: {args.ext}')
        eresults = [ e for e in fresults if e.match(f'*.{args.ext}') ]
        if args.debug:
            print(f'eresults: {eresults}')
    else:
        eresults = fresults

    if args.base:
        if args.debug:
            print(f'args.base: {args.base}')
        results = eresults
        for base in args.base:
            if args.debug:
                print(f'test {base}')
            results = [ b for b in results if b.match(f'*{base}*') ]
        if args.debug:
            print(f'results {results}')

    else:
        results = eresults
    if args.show_files or args.debug:
        for r in results:
            print(f'    {r}', file=sys.stderr)
    if args.debug:
        print(results)
    if not results:
        print('NO RESULTS FOUND')
        sys.exit(1)
    print(results[-1])

