#!/usr/bin/python3
#

""" wordllefind.py
    prograram to quess wordly
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import os
import os.path
import random
import re
import sys

def filter_doubles(w):
    wsl = sorted(w)
    l = None
    for c in wsl:
        if c == l:
            return None
        l = c
    return w

def pick_one(plist):
    #print('PO:', len(plist))
    pi = int(random.random() * len(plist))
    return (plist[pi], len(plist))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='play workle, or solve today\'s puzzle')
    parser.add_argument('--rm',
                        type=str,
                        default=None,
                        help = 'word to remove')
    parser.add_argument('--words', '-w', 
                        dest='words',
                        type=str,
                        default=os.getenv('WREF'),
                        help = 'override the $WORDS enviroment variable, default: %(default)s')
    myargs = parser.parse_args()

    allwords = open(myargs.words).readlines()
    #words = [ w.strip() for w in allwords if w[0] != w[0].upper()]
    refwords = [ w.strip().lower() for w in allwords ]
    refwords.pop(refwords.index(myargs.rm))
    with open('x.file', 'w') as xfd:
        for w in refwords:
            print(w, file=xfd)

        


