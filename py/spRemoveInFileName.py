#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import os
import os.path
import sys

import args

usage="""
    rmSpacesInFileName [-space=C] [-replace=C] list-of-files

    -space=C is the character/string to to be replace.
        it defaults to the space ' '.
    -replace=C is the character/string to replace the 'space' with.
        it defaults to the dash '-'.
"""

args = args.Args(usage)
_REPLACE = args.get_arg('replace', '-')
_SPACE = args.get_arg('space', ' ')

#print 'R:%s' % _REPLACE
#print 'S:%s' % _SPACE


def gen_new_name(old_name):
  return old_name.replace(_SPACE, _REPLACE)


if __name__ == '__main__':
    files = args.get_values()

    for file in files:
      base = os.path.basename(file)
      dir = os.path.dirname(file)
      new_base = gen_new_name(base)
      new_file = os.path.join(dir, new_base)
      if new_file != file:
        print("rename %s to %s" % (file, new_file))
        if not os.path.exists(new_file):
          os.rename(file, new_file)
        else:
          print("%s exists, remove to complete operation" % new_file)

