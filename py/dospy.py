#!/usr/bin/env python3
# dospy.py -- runs a python program in a dos cmd enviroment.
#   Useful for  when some tools are only available in Windows
#   Can easily extened to any Windows EXE

import argparse
import pathlib
import os
import sys

parser = argparse.ArgumentParser(description='run python program in dos')

homepath=os.getenv('HOMEPATH')
#print(homepath)
if not pathlib.Path(homepath).is_dir():
    print(f'{homepath} not found')
    sys.exit(1)
pyexe = os.getenv('DOSPYEXE', f'{homepath}/AppData/Local/Programs/Python/Python311/python.exe').replace('\\', '/')
#print(f'pyexe:{pyexe}')
parser.add_argument('--exe',
                    help='override dos python.exe path',
                    default=pyexe)
pypy = os.getenv('DOSPYSCRIPT', './<py>.py')
parser.add_argument('py',
                    help=f'example py: {pypy}, also can use env DOSPYSCRIPT',
                    default=pypy)
pyargs = os.getenv('DOSPYARGS', '')
parser.add_argument('--HELP', '-H',
                    action='store_true',
                    help=f'get help for {pypy}')
parser.add_argument('--foreground', '-F',
                    action='store_true',
                    help=f'run in foreground, otherwise runs in background')
parser.add_argument('--pip_install',
                    help=f'run pip command and install argument, need dummy py')
args, ukargs = parser.parse_known_args()
#print(f'{args=}')
#print(f'{ukargs=}')
pyargs += ' '.join(ukargs)
#print(f'{pyargs=}')
if not pathlib.Path(args.py).is_file() and not args.pip_install:
    print(f'{args.py} not found')
    sys.exit(1)
#print(f'args:{args}')
if args.foreground:
    BG = ''
else:
    BG = '&'
if args.HELP:
    doscmd = f'cmd /c {args.exe} {args.py} --help'
elif args.pip_install:
    doscmd = f'cmd /c {args.exe} -m pip {args.pip_install}' 
else:
    doscmd = f'cmd /c {args.exe} {args.py} {pyargs} {BG}'
print(f'{doscmd=}')
os.system(doscmd)
print('\n')

