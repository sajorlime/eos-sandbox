#!/usr/bin/env python3
#

"""args.py
   handle command line arguments in a consistent way.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import sys

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

__args = None


class Args:
    """ class Args gives a way of handling command line arguments.

    usage:
        Args()
    """

    def __init__(self, usage=None, verbose=False, save=False):
        """ Creates an object from the system argv array and removes
            all values from that arrays.
            The result is an object with a flagged argument dictionary
            and an array of values not associated with flags.
        Args:
            usage: a value to print when -?, or -h is passed as the first
                   argument.
            flags: if not empty it is used to filter legal flags
            save: is boolean that when true leaves the args in sys.agrv.
        Returns:
            the Args objects with sys.argv empty.
        """
        #print(sys.argv)
        length = len(sys.argv)
        if length > 1:
            if (sys.argv[1] == '-?'
                or sys.argv[1] == '--?'
                or sys.argv[1] == '-h'
                or sys.argv[1] == '--h'
                or sys.argv[1] == '-help'
                or sys.argv[1] == '--help'
               ):
                if usage:
                    print("%s: %s" % (sys.argv[0], usage))
                else:
                    print("%s: unknown or no options" % sys.argv[0])
                sys.exit(1)

        self._verbose = verbose
        self._usage = usage
        self._argv = sys.argv
        if not save:
            sys.argv = None
        self._argdict = {}
        self._values = []
        argn = 1
        # OK, for each of the args try to decide its type and record it.
        while argn < length:
            arg = self._argv[argn]
            # print('a(%d): %s' % (argn, arg))
            if arg[0:1] == '-':
                eq_pos = arg.find('=')
                if eq_pos > 0:
                    # print("arg %s=%s" % (arg[1:eq_pos:], arg[eq_pos + 1:]))
                    self._argdict[arg[1:eq_pos:]] = arg[eq_pos + 1:]
                else:
                    self._argdict[arg[1:]] = False
            elif arg[0:1] == '+':
                self._argdict[arg[1:]] = True
            else:
                self._values.append(arg)

            argn += 1
        # print(self._argdict.keys())
        # print(self._argdict.values())
        __args = self

    def len(self):
        # returns the number of values that can be retrieved from args.
        return len(self._values) + len(self._argdict)

    def empty(self):
        # returns true if there are no values in args.
        return self.len() == 0

    def get_arg(self, flag_value, default=False):
        """ get the argument associated with flag_value and remove it
            from the args.  A default value can be provided when the flag
            value does not exist.
        Args:
            flag_value: the string value of a flag parameter, e.g. -myflag=foo,
            then you pass 'myflag'
            default: the default object to return when the flag values was
            not passed on the command line or the values has already been
            removed.
        Note:
            It seems to me the be a bug that multiple calls to a value with a
            default will just keep returning the default.  This behavior is
            differet then when one gets with multiple calls with out a
            default.
        """
        # print("ga: %s, %s: with %s" % (flag_value, default, self._argdict))
        return self._argdict.pop(flag_value, default)
        # try:
        #   value = self._argdict.pop(flag_value, False)
        #   return value
        # except KeyError:
        #   return False

    def get_value(self, pos=0, default=None):
        """ return and remove a value that was not passed as a flag option.
        """
        # print("gv: %d: with %s" % (pos, self._values))
        try:
            rv = self._values[pos:pos + 1][0]
            self._values.remove(rv)
        except IndexError:
            return default
        return rv

    def get_bool(self, val, rval=False):
        """ return and remove a value that was not passed as a flag option.
        """
        try:
            print(self._argdict)
            rval = self._argdict[val]
        except IndexError:
            print(f'gb-ret not-found rval: {rval}')
            return False
        print(f'gb-ret: found {rval}')
        return rval

    def get_values(self):
        """ return and remove all of the values that were not passed as flags.
        """
        rvs = self._values
        self._values = []
        return rvs

    def report(self):
        """ report the arguments that are currently available.
        """
        return "flags: %s, values: %s" % (self._argdict, self._values)

    def error_if_unused_args(self):
        """ Print the args and usage before exiting if arguments
            remain in args.
        """
        if not self.empty():
            print("args len: %d" % self.len())
            print("unrecognized arguments")
            print(self.report())
            print(self._usage)
            sys.exit(-1)

    def verbose(self):
        """ set a global values for the Verbose call to return.
        """
        if not self._verbose:
            # print('check verbose')
            self._verbose = bool(self.get_arg('verbose', False))
            # print('found %s' % self._verbose)
        return self._verbose

    def help(self, reason, exit=True, exv=-1):
        print('\nargument error:', file=sys.stderr)
        print(reason, file=sys.stderr)
        print('usage:', file=sys.stderr)
        print(self._usage, file=sys.stderr)
        if exit:
            sys.exit(exv)


def GetArgs():
    return __args


def Verbose():
    return __args.verbose()


def Help(reason, exit):
    return __args.help(reason, exit)
# end
