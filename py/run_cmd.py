#!/usr/bin/env python3
#

"""
    run_cmd.py runs a command from passed arguments
"""
"""
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import argparse
import os
import re
import sys
import subprocess as sp
import textwrap
import typing as ty


def run_cmd(cmd: str, *args: ty.Any) -> sp.CompletedProcess :
    """
    Executes a command using subprocess.runsp..

    Args:
        cmd: The name of the executable or function to run.
        *args: Variable number of arguments to pass to the executable.

    Returns:
        CompletedProcess object containing the output and return code.
    """
    if not isinstance(cmd, str):
        raise TypeError("The 'cmd' argument must be a string.")
    print(f'{(args[0])=}')
    cmd_list = [cmd] + [str(a) for a in list(args[0])]  # Create the command list
    print(f'{cmd_list=}')
    return sp.run(cmd_list, capture_output=True, text=True)
    #return result


if __name__=='__main__':
    _epilog = textwrap.dedent("""\
        Example run_cmd ls -l -a -r
        Note: ls depends on the shell to expand wild cards, so wild cards
            can not be passed.
    """)

    parser = argparse.ArgumentParser(description='run command test program',
                                     epilog=_epilog)

    parser.add_argument('cmd', help="name of the command line command")
    parser.add_argument('args',
                        nargs='*',
                        help="legal arguments to cmd")
    parser.add_argument('--board',
                        choices=['zh-02', 'dvk'],
                        default='zh-02',
                        help='a dummy parameter for testing')
    args = parser.parse_args()

    cp_res = run_cmd(args.cmd, args.args)
    if cp_res.returncode != 0:
        print(cp_res.stderr)
        cp_res.check_returncode()
    print(f'ret: {cp_res.returncode=}')
    print(f'Output:\n{cp_res.stdout}')
    print(f'{args.board=}')

