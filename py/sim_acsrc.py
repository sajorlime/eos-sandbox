#!/usr/bin/env python3
# utils/plot_utils.py 
# 09/10/2022
# plot_utils
'''
    routines for ploting CSV data.
'''

import csv
import os
import pathlib as pl
import sys

# step_duration, volts, vslew, freq, fslew
uv2 = \
[
  [
    # sdur, volts, vslew, freq, fslew
    2,      120.0,  10.0,  60.0, 1.0
  ],
  [
    2,      60.5,  10.0,  60.0, 1.0
  ],
  [
    0.033,   59.5,  10.0,  60.0, 1.0
  ],
  [
    2,      59.5,  2.0,  60.0, 1.0
  ],
  [
    2,      120.0,  2.0,  60.0, 1.0
  ],
]

# step_duration, volts, vslew, freq, fslew
ov2 = \
[
  [
    # sdur, volts, vslew, freq, fslew
    2,      120.0,  10.0,  60.0, 1.0
  ],
  [
    2,      143.5,  10.0,  60.0, 1.0
  ],
  [
    0.033,   144.5,  10.0,  60.0, 1.0
  ],
  [
    2,      144.5,  10.0,  60.0, 1.0
  ],
  [
    2,      120.0,  10.0,  60.0, 1.0
  ],
]


mfsize = 3
#// store the last mfsize values, in rotating buffer
lsize = mfsize + 1
m_last = [120.0, 120.0, 120.0, 120.0]
ml_pos = 0

def m3pt(t0, t1, t2):
    b0_le_1 = t0 <= t1
    b1_le_2 = t1 <= t2
    if (b0_le_1 & b1_le_2):
        return t1;
    if (b0_le_1 & b1_le_2):
        return t2
    if (b0_le_1):
        return t0
    b0_le_2 = t0 <= t2;
    if (b0_le_2 & b1_le_2):
        return t0
    if (b1_le_2):
        return t2
    return t1;

def median(ynew):
    global ml_pos
    m_last[ml_pos] = ynew
    ml_pos += 1
    ml_pos &= mfsize
    medium = m3pt(m_last[(ml_pos + 1) & mfsize],
                  m_last[(ml_pos + 2) & mfsize],
                  ynew)
    return medium


def gen_step_data(ref_secs, step_data, ref_step, sample_rate=60.0):
    ''' generate an list step data based on the data and sample rate
    Args:
        ref_secs: where we start in time
        step_data: list of duration, in secs, volts, the volts slew from the
            last step, freq, freq slew
        ref_step: the lasto reference step data
        sample_rate: samples/sec to denerate, so 2 secs give 120 sample
    '''
    #print(f'gsd: {step_data=}, {ref_step=}')
    secs = step_data[0]
    end_volts = step_data[1]
    vslew = step_data[2]
    end_freq = step_data[3]
    fslew = step_data[4]
    volts_out = ref_step[1]
    freq_out = ref_step[3]
    tstep_size = 1.0 / sample_rate
    tsteps = secs * sample_rate
    dvolts = end_volts - volts_out
    vstep = dvolts / tsteps
    dfreq = end_freq - freq_out
    fstep = dfreq / tsteps
    sd_out = []
    tout = 0.0
    #print(f'{tstep_size=}, {vstep=}, {fstep=}')
    while (tout < secs):
        #print(secs - tout, volts_out, freq_out)
        sd_out.append([ref_secs + tout, volts_out, freq_out])
        tout += tstep_size
        volts_out += vstep
        freq_out += fstep
    #print(f'{sd_out[0]=}, {sd_out[-1]=}')
    return sd_out

def write_data(fd, header, odata):
    print(header, file=fd)
    for step in odata[1:]:
        sout = ','.join([f'{s}' for s in step])
        print(sout, file=fd)
        #for element in step:
        #    print(f'{element},', end='', file=fd)
        #print(file=fd)


def add_1_4filter(data, col):
    y0 = data[0][col]
    for row in data[1:]:
        ynew  = row[col]
        y0 = ((3 * y0) + ynew) / 4.0
        row.append(y0)

def add_1_2filter(data, col):
    y0 = data[0][col]
    for row in data[1:]:
        ynew  = row[col]
        y0 = (y0 + ynew) / 2.0
        row.append(y0)

def add_3_4filter(data, col):
    y0 = data[0][col]
    for row in data[1:]:
        ynew  = row[col]
        y0 = (y0 + (3 * ynew)) / 4.0
        row.append(y0)

def add_m3_filter(data, col):
    y0 = data[0][col]
    for row in data[1:]:
        ynew  = row[col]
        y0 = median(ynew)
        row.append(y0)
    


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='sim ac source ')
    parser.add_argument('csv',
                        type=pl.Path,
                        help='csv file path')
    parser.add_argument('--ov2',
                        action='store_true',
                        help='get ov2 data')
    parser.add_argument('--uv2',
                        action='store_true',
                        help='get ov2 data')
    args = parser.parse_args()
    #print(csvfd)
    # first line references itself
    if args.ov2:
        steps_src = ov2
    else:
        steps_src = uv2
    sim_data = gen_step_data(0, steps_src[0], steps_src[0])
    ref_secs = steps_src[0][0]
    #print(f'{len(sim_data)=}')
    for onum, ostep in enumerate(steps_src[1:]):
        #print(f'{onum=}, {ostep}')
        if ostep[0] == 0:
            print(f'end {ostep=}')
            break;
        sim_data += gen_step_data(ref_secs, steps_src[onum], steps_src[onum - 1])
        ref_secs += steps_src[onum][0]
        #print(f'{len(sim_data)=}')
    add_1_2filter(sim_data, 1)
    add_3_4filter(sim_data, 1)
    add_1_4filter(sim_data, 1)
    add_m3_filter(sim_data, 1)
    with args.csv.open('w') as csvfd:
        write_data(csvfd, 'Secs,Volts,Hz,f1/2,f3/4,f1/4,m3f', sim_data)
            

