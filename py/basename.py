#!/usr/bin/env python3
#

""" basename.py -- by default does base name of single arguement.
    When there is more than one argument it looks for them in the last
    arg in the order upto the last arg, and returns for the first
    found: Found...basename, where the number of dots are the directory
    levels between found and basename
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import os
import sys


Usage = """
usage: basename.py [base0 ... baseN] path
"""

def basename(path):
  rslash_pos = path.rfind('/')
  #print(path, rslash_pos, file=sys.stderr)
  return path[rslash_pos + 1:]

argn = len(sys.argv)
if argn < 2:
  print(Usage, file=sys.stderr)
  sys.exit(1)

path = sys.argv[argn - 1]
last_slash_pos = path.rfind('/')
if last_slash_pos == -1:
  print(path)
  sys.exit(0)

thebase = path[last_slash_pos + 1:]

argi = 1
bases = sys.argv[1:argn - 1]
#print(bases, file=sys.stderr)

found = ''
levels = '..........'
slashes = 0
for base in bases:
  findit = basename(base)
  if base[0:2] == '//':
    findit = base[1:]
    base = base[1:]
  base_pos = path.find(base)
  if (base_pos != -1) and (base_pos < last_slash_pos):
    found = findit
    slashes = path[base_pos:].count('/')
    break

print(found, levels[len(levels) - slashes:], thebase, sep = '')
#nt = os.getenv('NOW_TESTING')
#vn = os.getenv('VNF_TRIP_PARAMETERS')
#print(f'windows {nt=}')
#print(f'windows {vn=}')
#print(found + levels[len(levels) - slashes:] + thebase,

