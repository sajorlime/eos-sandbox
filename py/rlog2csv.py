#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

""" rlog2csv.py
    Filter the rlog file.
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import codecs

import sys
import time
from signal import signal, SIGINT

import filefilter
import regexfilter

_epi = """
"""

red = '[31;1m'
green = '[32;1m'
nc = '[0m'

def get_trip_times(ttil):
    ''' the trip times based on a list of a type
    Args:
        ttil: list of trip info dicts, of the same type
    Returns:
        [trip_times]
    '''
    ttl = []
    for td in ttil:
        cttr = td.get('cttr(dt, v/h, t1, t0)', (0, 0, 0, 0))
        try:
            t1 = cttr[2]
            t0 = cttr[3]
        except IndexError as ie:
            print(f'index error {cttr=}')
            t1 = 0
            t0 = 0
        calc_tt = t1 - t0
        ttl.append(calc_tt)
    return ttl
 

def calc_trip_stats(ttl: list, verbose: bool = False) -> tuple:
    ''' calculate the trip statics based on a list of a type
    Args:
        ttl: list of trip info dicts, of the same type
        verbose: print some results for each
    Returns:
        avg_trip, min_trip, max_trip, n, passes, fails
    '''
    sum_dtrips = 0
    sum_ctt = 0
    min_dtrip = 10 ** 10
    max_dtrip = -min_dtrip
    fails = 0
    passes = 0
    exp = ttl[0]['expected'] # just use 160 ms
    n = len(ttl)
    for td in ttl:
        cttr = td.get('cttr(dt, v/h, t1, t0)', (0, 0, 0, 0))
        try:
            t1 = cttr[2]
            t0 = cttr[3]
        except IndexError as ie:
            print(f'IndexError, set {cttr=}')
            t1 = 0
            t0 = 0
        calc_tt = t1 - t0
        sum_ctt += 1000 * calc_tt # to msecs
        try:
            this_dtrip = int(td['trip'] - td['tmin'])
        except TypeError as te:
            calc_tt = 0
            print(f'{td}')
            sys.exit(1)
        if td['result'] == 'PASS!':
            passes += 1
        else:
            fails += 1
        sum_dtrips += this_dtrip
        min_dtrip = int(min(min_dtrip, this_dtrip))
        max_dtrip = int(max(max_dtrip, this_dtrip))
        if verbose:
            print(f"{this_dtrip}, {int(calc_tt)}, {td['result']}, {td['run']}")
    avg_sum_dtrips = int(round(sum_dtrips / n, 3))
    avg_sum_ctt = int(round(sum_ctt / n, 3))
    return avg_sum_dtrips, min_dtrip, max_dtrip, avg_sum_ctt, n, passes, fails
 

def get_data(args):
    ''' read the rlog.log file (twice) to get counts of no-trips, passes
    and fails. Prints results.
    '''
    nt_in_lines = filefilter.ReadFile(args.rlfile,
                                      interest=nt_interest,
                                      strip='\n',
                                      echo_line=False)
    in_lines = filefilter.ReadFile(args.rlfile,
                                   interest=interest,
                                   strip='\n',
                                   echo_line=False)
    if args.ofile == '-':
        ofile = sys.stdout
    else:
        ofile = codecs.open(args.ofile, encoding='utf-8', mode='w')
    # read in the lines that match the filter
    # and convert the line to a dictionary
    nt_data = [eval(line) for line in nt_in_lines]
    fdata = [eval(line) for line in in_lines]
    
    if args.hw_version:
        fdata = [ d for d in fdata if d.get('hw_version', 0) == args.hw_version ]
        nt_fdata = [ d for d in nt_data if d.get('hw_version', 0) == args.hw_version ]
        print(f'hwver cnt: {len(fdata)=}')
    if args.serial_number:
        fdata = [d for d in fdata
                    if args.serial_number
                        in d.get('serial_number', 'not found')]
        nt_data = [d for d in nt_data
                    if args.serial_number
                        in d.get('serial_number', 'not found')]

    def get_rdict(file_data):
        x = len(file_data)
        ddict = {}
        i = 0
        for d in file_data:
            key = d['test']
            dl = ddict.get(key)
            if not dl:
                ddict[key] = [d]
            else:
                dl.append(d)
            i += 1
            x -= 1
            if x <= 0:
                break
        return ddict

    nt_ddict = get_rdict(nt_data)
    ddict = get_rdict(fdata)
    print(f'NT cnt: {nt_ddict=}')
    print(f'p/f cnt: {len(ddict)=}')
    for k in nt_ddict:
        tl = nt_ddict[k]
        print(f'{k}: {red}{len(tl)} NO TRIPS{nc}', file=ofile)
    for k in ddict:
        tl = ddict[k]
        if args.list:
            ttl = get_trip_times(tl)
            ttlstr = ','.join([ f'{t:.3f}' for t in ttl ])
            print(f'{ttlstr}\n{min(ttl):.3f} .. {max(ttl):.3f}\n')
            continue
        avgdt, mindt, maxdt, avg_ctt, n, passes, fails \
            = calc_trip_stats(tl, args.verbose)
        target = 3 * (160 / 4)
        cor = target - maxdt
        if passes > 0:
            passes = f'{green}passes={passes}/{n}{nc}'
        if fails > 0:
            fails = f'{red}fails={fails}/{n}{nc}'
        else:
            fails = ''
        print(f'{k}: {avgdt=}, '
              #f'{cor=}/{160 - cor}, '
              f'{mindt=}, '
              f'{maxdt=}, '
              #f'cmin:{mindt + cor}, '
              #f'cmax:{maxdt + cor}, '
              f'{avg_ctt=}, '
              f'{passes},',
              f'{fails}',
              file=ofile)

        


def sig_handler(signal_received, frame):
    sys.exit(0)


# test case
if __name__ == "__main__":
    signal(SIGINT, sig_handler)
    parser = argparse.ArgumentParser(
                description='filter rlog, outputs a CSV file with trip correction',
                epilog=_epi)
    parser.add_argument('rlfile',
                        type=str,
                        help='rlog file')
    parser.add_argument('--ofile', "-o",
                        type=str,
                        default="-",
                        help='output file, %(default)s')
    parser.add_argument('--filter', '-f',
                        type=str,
                        action='append',
                        help='filter file with these strings')
    #needs a better definition
    #parser.add_argument('--replace', '-r',
    #                    type=str,
    #                    help='replace substrings in the filters')
    parser.add_argument('--hw_version', '-hw',
                        type=int,
                        help='filter file with with this hw_version')
    parser.add_argument('--serial_number', '-sn',
                        help='filter file with with this the serial number')
    parser.add_argument('--repeat_time', '-rt',
                        type=int,
                        help='repeat time in secs')
    parser.add_argument('--today', '-T',
                        action='store_true',
                        help='use todays date')
    parser.add_argument('--hours',
                        type=int,
                        default=1,
                        help='use todays date')
    parser.add_argument('--no_trip', '-N',
                        action='store_true',
                        help='count the no trips')
    parser.add_argument('--list', '-L',
                        action='store_true',
                        help='list the margins, instead of the stats')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='list the margins, instead of the stats')
    args = parser.parse_args()
    nt_flist = [regexfilter.SRegexFilter('+NO-TRIP')]
    flist = [regexfilter.SRegexFilter('-NO-TRIP'),
             regexfilter.SRegexFilter('-None, None'),
             regexfilter.SRegexFilter('-nan'),
            ]
    if args.today:
        datestr = time.strftime('%m%d%Y', time.localtime())
        timestr = time.strftime('%H', time.localtime())
        hour = int(timestr)
        if hour >= 24:
            hour = 0
        timestr = f'{int(timestr) + 8}'
        f = f'+{datestr}:{timestr}'
        print(f'{f=}')
        print(f'{timestr=}')
        print(f)
        nt_flist.append(regexfilter.SRegexFilter(f))
        flist.append(regexfilter.SRegexFilter(f))
    if args.filter:
        for f in args.filter:
            flist.append(regexfilter.SRegexFilter(f'+{f}'))
            nt_flist.append(regexfilter.SRegexFilter(f'+{f}'))

    if args.ofile == '-':
        ofile = sys.stdout
    else:
        ofile = codecs.open(args.ofile, encoding='utf-8', mode='w')
    if args.filter:
        for f in args.filter:
            flist.append(regexfilter.SRegexFilter(f'+{f}'))

    def nt_interest(line):
        # define interest based on the filter-list (flist)
        return regexfilter.ftest(line, nt_flist)

    def interest(line):
        # define interest based on the filter-list (flist)
        return regexfilter.ftest(line, flist)

    while True:
        get_data(args)
        if not args.repeat_time:
            break
        time.sleep(args.repeat_time)
        print()

