#!/usr/bin/env python3
#
#
"""
  difflist.py does the diffs based on file lists from two roots.
    difflist.py refdir devdir [--type=ext]
                | --list=file-list-path0] --list=file-list-path1
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import os
import os.path
import re
import sys


import filefilter
import regexfilter

def get_file_list(path, extensions):
    """
    get a list of files with from ext-files filtered by the extensions list.
    """
    flist = os.listdir(path)
    #print('extensions', extensions, len(extensions))
    if extensions:
        exts = ['\.%s' % x for x in extensions]
        #print('exts', exts)
        pre = regexfilter.RegexFilter(exts, '-files$')
        #print('pre', pre.pattern())
        return [os.path.join(path, x) for x in filter(pre.accept, flist)]
    #print('posting', len(flist))
    post = regexfilter.RegexFilter(['\..+-files$'])
    #print('post', post)
    candidates = filter(post.accept, flist)
    #print('candidates', candidates)
    return [os.path.join(path, x) for x in candidates]

        

_usage = """
do the diffs based on file lists from two roots.
    either refdir devdir [--type=ext, ...],
        specifies the directoy locations and the file type you want to examine
    or --list=list-file-path0 --list=list-file-path1
"""
_epilog = """
leaving extensions empty causes it to look for all files
of the form .ext-files in the root dirs
"""

def difflist():
    parser = argparse.ArgumentParser(description = _usage, epilog = _epilog)
    parser.add_argument('dirs',
                        nargs='*',
                        type=str,
                        default=[],
                        help='specify dir names when specifying root directories')
    parser.add_argument('--ext', '-e',
                        dest='extensions',
                        metavar='EXT',
                        action='append',
                        default=[],
                        help='list all extensions you are interested in when sepecifying root dirs')
    parser.add_argument('--list', '-l', 
                        dest='flists',
                        metavar='FILE-LIST',
                        action='append',
                        type=str,
                        default=[],
                        help='specify two file-list paths')
    parser.add_argument('--quiet', '-q', 
                        dest='not_quiet',
                        action='store_true',
                        default=False,
                        help='pass a quiet flag to diff')
    # TODO(epr): need more code to handle operation when more is true
    parser.add_argument('--more', '-m', 
                        action='store_true',
                        default=False,
                        help='user less')
    myargs = parser.parse_args()

    #print(myargs.dirs)
    #print(myargs.extensions)
    #print(myargs.flists)
    if myargs.dirs:
        if myargs.flists or len(myargs.dirs) != 2:
            parser.print_help()
            sys.exit(1)
    elif not myargs.flists or len(myargs.flists) != 2:
        parser.print_help()
        sys.exit(1)
    if myargs.dirs:
        flists_ref = get_file_list(myargs.dirs[0], myargs.extensions)
        flists_dev = get_file_list(myargs.dirs[1], myargs.extensions)
    else:
        flists_ref = [myargs.flists[0]]
        flists_dev = [myargs.flists[1]]
    #flists_ref.sort()
    #flists_dev.sort()
    #print(flists_ref)
    #print(flists_dev)
    quiet = ''
    if myargs.not_quiet:
        quiet = ' -q '
    more = ''
    if myargs.more:
        more = ' | less '
    while flists_ref and flists_dev:
        fl_ref = flists_ref.pop(0)
        bdir_ref = os.path.dirname(fl_ref)
        fl_dev = flists_dev.pop(0)
        bdir_dev = os.path.dirname(fl_dev)
        #print('fls:', bdir_ref, fl_ref, bdir_dev, fl_dev)
        lof_ref = filefilter.ReadFile(fl_ref)
        lof_dev = filefilter.ReadFile(fl_dev)
        #print('lofs-len:', len(lof_ref), len(lof_dev))
        while lof_ref and lof_dev:
            fref = os.path.join(bdir_ref, lof_ref.pop(0).strip())
            fdev = os.path.join(bdir_dev, lof_dev.pop(0).strip())
            dcmd = 'diff %s %s %s %s' % (quiet, fref, fdev, more)
            #print('dcmd:', dcmd)
            os.system(dcmd)
            #if os.system(dcmd) and not myargs.not_quiet:
            #    print(fdev)
    # TODO(epr): deal with list of different sizes
    return 0

    """
    if myargs.dirs:
    if not os.path.exists(gfl):
        gfl = os.path.join(os.getenv('HOME'), '.gflfilter')
    if os.path.exists(gfl):
        gflfilters = filefilter.ReadFile(gfl, xform = lambda fval, n : fval.rstrip())
    else:
        gflfilters = []
        print('genflist: warning no filter file', file=sys.stderr)
    filters = myargs.filters

    for f in gflfilters:
        filters.append(regexfilter.RegexFilter(False, '%s' % f))

    pairs = {}
    for spair in myargs.pairs:
        pair = Pair(spair)
        pairs[pair.key()] = pair.value()

    for ext in myargs.extensions:
        pat = ext
        if ext in list(pairs.keys()):
            pat = pairs[ext]
        else:
            pat = '\.%s$' % ext
        #print(pat)
        filters.append(regexfilter.RegexFilter(True, pat))

        # TODO(epr): py wierdness here, if don't pass empty list
        # next call has old list.
        tih = tree.TreeItemHandler('.', filters, [])
        tree.tree_files('.', tih)
        wf = open('.%s-files' % ext, 'w+');
        files = sorted(list(tih.items()), key = cmp_to_key(dirsort))
        #for file in tih.items():
        for file in files:
            print(file, file=wf)
        wf.close()
        filters.pop()
    """

if __name__=='__main__':
    difflist()


