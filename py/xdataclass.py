#!/usr/bin/env python3
# tdataclass
# 11/23/2022
# experiment with data class

import argparse
import csv
import enum
import json
import os
import math
import re
import struct
import sys
import time
import traceback

from dataclasses import dataclass, asdict, field
import dataclasses

def _alimit(lo, lmin, lmax):
    limit = float(lo)
    if (limit >= lmin) and (limit <= lmax):
        return limit
    raise argparse.ArgumentTypeError(f'{limit} not in [{lmin}, {lmax}]')


def _a_limit(pfs):
    pf = float(pfs)
    if pf > -74.0 and pf < 74.0:
        return f'{pf=}'
    raise argparse.ArgumentTypeError(f'{pfs} not in +/-74degs range')

@dataclass
class Member(object):
    _desc = 'I am Member'
    _default = None
    _help = f'I am Member, default: {None}'
    def __init__(self, a='', t=float, n='M', v=0, m=0, x=0, d='I am a M', h='help'):
        self._args = a
        self._type = t
        self._name = n
        self._value = v
        self._vmin = m
        self._vmax = x
        self._desc = d
        self._help = h

    def __str__(self):
        return f'{self._args=}, {self._type=}, {self._name=}, {self._value=}, {self._vmin=}, {self._vmax=}, {self._desc=}, {self._help=}'

    def add_arg(self, parser):
        print(f'add argd: {self}')
        #print(f'{self._args=}')
        parser.add_argument(*self._args,
                            type=self.getlf(),
                            default=self._value,
                            help=self._help)
    @staticmethod
    def limit(vs, vmin, vmax):
        v = float(vs)
        if (v >= vmin) and (v <= vmax):
            return v
        raise argparse.ArgumentTypeError(f'{v} not in [{vmin}, {vmax}]')
    def getlf(self):
        def blimit(vs):
            print(f'G:{self=}')
            return Member.limit(vs, self._vmin, self._vmax)
        return blimit

_bm_dict = {
    'a' : ['--b', '-b'],
    't' : float,
    'n' : 'b-name',
    'v' : 0.5,
    'm' : 0.0,
    'x' : 1.0,
    'd' : 'I am a B member',
    'h' : 'pass the B value, default: %(default)s',
}

@dataclass
class BMember(Member):
    def __init__(self, v=0.5):
        print(f'{_bm_dict["a"]=}')
        self._args = _bm_dict['a']
        super().__init__(**_bm_dict)

@dataclass
class CMember(Member):
    _dict = {
        'a' : ['--chug', '-c'],
        't' : float,
        'n' : 'c-name',
        'v' : 5.5,
        'm' : 1.0,
        'x' : 10.0,
        'd' : 'I am a C member',
        'h' : 'pass the C value, default: %(default)s',
    }
    def __init__(self, v=0.5):
        super().__init__(**CMember._dict)
        print(f'C:{self=}')

def add_params(data_parser):
    data_parser.add_argument('-f',
                               action='store_true',
                               help='set flag on')
    data_parser.add_argument('-a',
                               type=_a_limit,
                               default=100.0,
                               help='set a, default: %(default)s')
    data_parser.set_defaults(func=data_handler)

def data_handler(args):
    if args.f:
        print(args.f)
    if args.a:
        print(args.a)
    if args.b:
        print(args.b)


parser = argparse.ArgumentParser()

m = Member(h='xhelp')
#print(m)
#print(m.__str__())
print(f'{m}, {m._help}')

add_params(parser)
b = BMember()
b.add_arg(parser)
c = CMember()
c.add_arg(parser)
args = parser.parse_args()
print(f'{args=}')
args.func(args)

