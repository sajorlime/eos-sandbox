#!/usr/bin/env python3
"""
    email_csv.py -- class that reads in csv files that represent
    e-mail lists.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import os
import sys

# local
import enum



class email_csv:
    """
    encapsulates the contents of a email csv file.
    """
    def __init__(self, fields, addresses):
        self._af = enum.enum(address_fields)
        self._addresses = []
        for addr in  addresses:
            self._addresses.append(addr.split(','))

    def name(self, position):
        if position > len(self._addresses):
            return None
        addr = self._addresses[position]
        print(af.value('Name'))
        return addr[af.value('Name')]

    def email1(self, position):
        if position > len(self._addresses):
            return None
        addr = self._addresses[position]
        return addr[af.value('E_mail1_Value')]

    def email2(self, position):
        if position > len(self._addresses):
            return None
        addr = self._addresses[position]
        return addr[af.value('E_mail2_Value')]



if __name__ == '__main__':
    import args
    import filefilter

    args = args.Args("email_csv (test only) email.csv-file")
    addresses = filefilter.ReadFile(args.get_value())

    csv = email_csv(addresses)

    print(csv)

    i = 0; print('%d: name: %s, %s' % (i, csv.name(i), csv.email1(i)))
    i = 1; print('%d: name: %s, %s' % (i, csv.name(i), csv.email1(i)))
    i = 3; print('%d: name: %s, %s' % (i, csv.name(i), csv.email1(i)))
    i = 8; print('%d: name: %s, %s' % (i, csv.name(i), csv.email1(i)))

