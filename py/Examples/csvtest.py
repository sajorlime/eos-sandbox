#!/usr/bin/env python3
#
#

"""
  csvtest.py accept and reject csv columns
    
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import csv
import os
import os.path
import datetime
import re
import sys

import regexfilter


def csvtest():
  parser = argparse.ArgumentParser(description = 'reads CSV file',
                                   epilog = 'note that by not specifying either input or output it will act as a pipped filter')
  parser.add_argument('--input-file', '-i',
                      type=str,
                      dest='ifile',
                      default='-',
                      help='the input file , - uses stdin, %(default)s')
  parser.add_argument('--output-file', '-o',
                      type=str,
                      dest='ofile',
                      default='-',
                      help='the output file , - uses stdout, %(default)s')
  parser.add_argument('--column', '-c',
                       dest='columns',
                       action='append',
                       #type=regexfilter.SRegexFilter,
                       type=str,
                       default=[],
                       help='[+-]regx +=accept/-=reject columns when column header that matches this regex')
  myargs = parser.parse_args()

  if myargs.ofile is '-':
    wf = sys.stdout
  else:
    wf = open(myargs.ofile, 'w');

  if myargs.ifile is '-':
    rf = sys.stdin
  else:
    rf = open(myargs.ifile, 'r');

  reader = csv.reader(rf)
  fields = next(reader)
  fdict = {}
  findex = 0
  for field in fields:
    fdict[field.replace(' ', '-')] = findex
    findex = findex + 1
    
  thisday = datetime.date.today()
  today = datetime.datetime(thisday.year, thisday.month, thisday.day)
  now = datetime.datetime.now()
  print(now, today, now - today)
  print('-----')
  try:
    for row in reader:
      month, day, year = row[0].split('/')
      row_date = datetime.datetime(int(year), int(month), int(day))
      if myargs.columns:
        interesting = []
        for interest in myargs.columns:
          if interest in fields:
            #interesting.append('%s:%s' % (interest, row[fdict[interest]]))
            interesting.append('%s:%s' % (interest, row[fields.index(interest)]))
          else:
            interesting.append('%s:%s' % (interest, 'no'))
      else:
        interesting = row[1:]
      print('%s-%s-%s: ' % (year, month, day), today - row_date, interesting, file = wf)
  except csv.Error as e:
    sys.exit('file %s, line %d: %s' % (myargs.ifile, reader.line_num, e))

  #for row in csvdict:
  #  print(row, file = wf)

if __name__=='__main__':
  csvtest()


