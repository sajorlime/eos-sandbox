#!/usr/bin/env python3
# aif/immediate.py
# 11/15/2022
# Encapsulates the immediate data TLVs, was part of access_aif

import csv
import enum
import json
import logging as lg
import os
import math
import posixpath as os_path # replase os.path with os_path, use / not \
import re
import signal
import socket
import struct
import sys
import time
import traceback

from dataclasses import dataclass, asdict

if __name__ == "__main__":
    #print(f'immed:{sys.path=}')
    me = sys.path
    tlv = me.replace('/aif', '/tlv')
    sys.path.insert(0, tlv)
    utils = me.replace('/aif', '/utils')
    sys.path.insert(0, utils)
    #print(f'mimmed: {sys.path=}')
    #sys.path.insert(0, '..')
    #print(f'{sys.path=}')

import primative as prim
import aif_utils as autils
import extract as aext
import dmember as dm
import power_status as pwrs
import reactive_power as rpwr
import volt_watt as vw
import volt_var as vv

import InverterConstants as invc

class OutputPowerLimit(dm.DMember):
    _members \
        = dm.build_members(
            ['--output_power_limit', '-opl'], # args
            'set the output power limit set point', # help base
            100.0, # default
            float,  # type
            'outputPowerLimit', # name
            100.0, # set value, should be the same a default
            100.0, # min
            0.0, # max
            'Output Power Limit Set Point')
    def __init__(self):
        super().__init__(**OutputPowerLimit._members)

class OutputPowerFactor(dm.DMember):
    _members = dm.build_members(
          ['--output_power_factor', '-opf'],
          'set the output power factor',
          1.0,
          float, 
          'outputPowerFactor',
          1.0, 
          0.45, 
          1.0,
          'Output Power Limit Set Point')
    def __init__(self):
        super().__init__(**OutputPowerFactor._members)

class OutputConstantReactivePowerPct(dm.DMember):
    _members = dm.build_members(
          ['--output_constant_reactive_power_pct', '-ocr'],
          'set the input power limit set point',
          100.0,
          float, 
          'outputConstantReactivePowerPct',
          0.0, 
          0.0, 
          100.0,
          'Input Power Limit Set Point')
    def __init__(self):
        super().__init__(**OutputConstantReactivePowerPct._members)

class InputPowerLimit(dm.DMember):
    _members = dm.build_members(
          ['--input_power_limit', '-ipl'],
          'Set the input power limit',
          100.0,
          float, 
          'inputPowerLimit',
          100.0, 
          0.0, 
          100.0,
          'Input Power Limit Set Point')
    def __init__(self):
        super().__init__(**InputPowerLimit._members)

class InputPowerFactor(dm.DMember):
    _members = dm.build_members(
          ['--input_power_factor', '-ipf'],
          'Set the input power factor',
          1.0,
          float, 
          'inputPowerFactor',
          1.0, 
          0.45, 
          1.0,
          'Input Power Limit Set Point')
    def __init__(self):
        super().__init__(**InputPowerFactor._members)

class InputConstantReactivePowerPct(dm.DMember):
    _members = dm.build_members(
          ['--input_constant_reactive_power_pct', '-icr'],
          'Set the input constant reactive power pct',
          1.0,
          float, 
          'inputConstantReactivePowerPct',
          100.0, 
          0.0, 
          100.0,
          'Input Power Limit Set Point')
    def __init__(self):
        super().__init__(**InputConstantReactivePowerPct._members)

IMMED_AIF_DATA_STRUCT_VERSION = 3
IMMED_AIF_DATA_STRUCT_SIZE = (4 * 1) + (2 * 4) + (2 * 1) + 4 + (2 * 1) + (3 * 4)
'''
from gatway.h:1580 ish

typedef struct __attribute__((__packed__)) s_immedAifData
{
  U8   version;
  U8   dynamicDataFlag;
  U8   inputPowerFactorLeading;
  U8   outputPowerFactorLeading;
  F32  inputPowerFactor;
  F32  outputPowerFactor;
  U8   inputConstantReactivePowerFactorLeading;
  U8   outputPowerLimitEnable;
  F32  outputPowerLimit;
  U8   outputConstantReactivePowerFactorLeading;
  U8   inputPowerLimitEnable;
  F32  inputPowerLimit;
  F32  inputConstantReactivePowerPct;
  F32  outputConstantReactivePowerPct;
} t_immedAifData;

From aif.h:190 ish
 // AIF Immediate Controls Group (eeprom configuration):
#define AIF_IMMED_POWER_FACTOR_ENABLE_IN_EEPROM         0x0001
#define AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_IN_EEPROM  0x0002
#define AIF_IMMED_INPUT_POWER_FACTOR_ENABLE_IN_EEPROM   0x0004
#define AIF_IMMED_CONSTANT_REACTIVE_POWER_ENABLE_IN_EEPROM  0x0010
#define AIF_IMMED_DYNAMIC_POWER_FACTOR_ENABLE_IN_EEPROM 0x0020


#define AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_IN_EEPROM            0x0001
#define AIF_IMMED_INPUT_POWER_FACTOR_LEADING_IN_EEPROM             0x0002
#define AIF_IMMED_OUTPUT_CONSTANT_REACTIVE_POWER_LEADING_IN_EEPROM 0x0004
#define AIF_IMMED_INPUT_CONSTANT_REACTIVE_POWER_LEADING_IN_EEPROM  0x0008

#define AIF_IMMED_OUTPUT_POWER_LIMIT_ENABLE             0x0001
#define AIF_IMMED_INPUT_POWER_LIMIT_ENABLE              0x0002


typedef struct s_aifImmedControlsConfig
{                                 //  SET    GET   <- TLV MANAGEMENT
  BOOL Unlocked     ;             //  128   1035
  F32  outputPowerLimitPct;       //  149   1036
  BOOL outputPowerLimitEnable;    //  130   1037
  F32  outputPowerFactor;         //  150   1038
  BOOL outputPowerFactorLeading;  //  132   1039
  BOOL outputPowerFactorEnable;   //
  F32  inputPowerFactor;          //
  BOOL inputPowerFactorLeading;   //
  BOOL inputPowerFactorEnable;    //
  BOOL immedPowerFactorEnable;    //
  BOOL inputPowerLimitEnable;     //
  F32  inputPowerLimitPct;        //
  F32  inputConstantReactivePowerPct;       //
  F32  outputConstantReactivePowerPct;      //
  BOOL constantReactivePowerEnable;         //
  BOOL inputConstantReactivePowerLeading;
  BOOL outputConstantReactivePowerLeading;
  BOOL dynamicPfEnabled;
} t_aifImmedControlsConfig;


reactive power in/out bits
#define REACTIVE_POWER_CTRL_IMMED_PF_MASK             0x03
#define REACTIVE_POWER_CTRL_IMMED_PF_OUTPUT_ENABLED   0x01
#define REACTIVE_POWER_CTRL_IMMED_PF_INPUT_ENABLED    0x02

reactive power mode bits, only one mode allowed
#define REACTIVE_POWER_CTRL_PF_MODE_MASK                0x1F
#define AIF_CMD_IMMED_POWER_FACTOR_MODE_ENABLE_BIT_0    0x01
#define AIF_CMD_VOLT_VAR_MODE_ENABLE_BIT_1              0x02
#define AIF_CMD_WATT_VAR_MODE_ENABLE_BIT_2              0x04
#define AIF_CMD_DYNAMIC_POWER_FACTOR_MODE_ENABLE_BIT_3  0x08
#define AIF_CMD_CONSTANT_REACTIVE_PWR_MODE_ENABLE_BIT_4 0x10

#define TLV_AIF_IMMED_CONTROL_CONFIG_ELEMENT_VERSION  3

#define  AIF_STATUS_CONNECT_TO_GRID_ENABLED_BIT_0            0x01
#define  AIF_STATUS_OUTPUT_POWER_LIMIT_ENABLE_BIT_1          0x02
#define  AIF_STATUS_OUTPUT_POWER_FACTOR_LEADING_ENABLE_BIT_2 0x04
#define  AIF_STATUS_OUTPUT_VAR_LIMIT_ENABLE_BIT_3            0x08
#define  AIF_STATUS_INPUT_POWER_FACTOR_LEADING_ENABLE_BIT_4  0x10
#define  AIF_STATUS_INPUT_POWER_LIMIT_ENABLE_BIT_5           0x20
#define  AIF_STATUS_OUTPUT_CONSTANT_REACTIVE_POWER_LEADING_ENABLE_BIT_6 0x40
#define  AIF_STATUS_INPUT_CONSTANT_REACTIVE_POWER_LEADING_ENABLE_BIT_7  0x80

typedef struct __attribute__((__packed__)) s_tlvAifImmedControlConfigElement
{
  U8   version;
  U8   immediatePowerFactorEnableFlags;   // same as in t_aifReactivePowerControlMode
  U8   powerFactorModeFlags;              // same as in t_aifReactivePowerControlMode
  U8   aifStatusFlags;
  F32  outputPowerLimitPct;               // from GTW_TO_SG424_SET_AIF_IMMED_OUTPUT_POWER_LIMIT_PCT_IN_EEPROM_F32_TLV_TYPE
  F32  outputPowerFactor;                 // same as t_powerFactor
  F32  spare;                             //
  F32  inputPowerFactor;                  // same as t_powerFactor
  F32  inputPowerLimitPct;                // from GTW_TO_SG424_SET_AIF_IMMED_INPUT_POWER_LIMIT_PCT_IN_EEPROM_F32_TLV_TYPE
  F32  inputConstantReactivePowerPct;     //
  F32  outputConstantReactivePowerPct;    //
} t_tlvAifImmedControlConfigElement;


'''

_idata_read_map = {
    # if GTW_TO_SG424_GET_AIF_IMMED_GROUP_DATA_U16_TLV_TYPE 
    invc.SG424_TO_GTW_AIF_IMMED_OUTPUT_CONNECT_TO_GRID_FROM_EEPROM_U16_TLV_TYPE: 'Unlock', # 1035
    invc.SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_LIMIT_PCT_FROM_EEPROM_F32_TLV_TYPE: 'outputPowerLimit', # = 1036
    invc.SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_LIMIT_ENABLE_FROM_EEPROM_U16_TLV_TYPE: 'outputPowerLimitEnable', #= 1037
    invc.SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_FROM_EEPROM_F32_TLV_TYPE: 'outputPowerFactor', #= 1038
    invc.SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_LEADING_FROM_EEPROM_U16_TLV_TYPE: 'outputPowerFactorLeading', #= 1039
    invc.SG424_TO_GTW_AIF_IMMED_OUTPUT_POWER_FACTOR_ENABLE_FROM_EEPROM_U16_TLV_TYPE: '', #= 1040
    invc.SG424_TO_GTW_AIF_IMMED_OUTPUT_VAR_LIMIT_PCT_FROM_EEPROM_F32_TLV_TYPE: '', #= 1041
    invc.SG424_TO_GTW_AIF_IMMED_OUTPUT_VAR_LIMIT_ENABLE_FROM_EEPROM_U16_TLV_TYPE: '', #= 1042
    # not finished
}

@dataclass
class ImmediateData:
    header_format: str = '!HH'
    data_format: str = '!BBBBffBBfBBfff'
    version: int = IMMED_AIF_DATA_STRUCT_VERSION
    size: int = IMMED_AIF_DATA_STRUCT_SIZE
    #print(f'{struct.calcsize(data_format)=}')
    assert(size == struct.calcsize(data_format)), 'mismatched sizes'
    # define in structure order, set-immed-data will have same order. However,
    # argument parser will have the same order as MGInsight dialog box
    dynamicDataFlag: bool = False
    inputPowerFactorLeading: bool = False
    outputPowerFactorLeading: bool = False
    inputPowerFactor: float = 1.0
    outputPowerFactor: float = 1.0
    inputConstantReactivePowerFactorLeading: bool = False
    outputPowerLimitEnable: bool = False
    outputPowerLimit: float = 100.0
    outputConstantReactivePowerFactorLeading: bool = False
    inputPowerLimitEnable: bool = False
    inputPowerLimit: float = 100.0
    inputConstantReactivePowerPct: float = 0.0
    outputConstantReactivePowerPct: float = 0.0

@dataclass
class ImmedConfig(object):
    ''' Class for importing binary data from inverter
        See aif/extract.extract_tlvs.
    '''
    read_format: str = '!BBBBfffffff'
    version: int = -1
    immediatePowerFactorEnableFlags: int = 0#   // same as in t_aifReactivePowerControlMode
    powerFactorModeFlags: int = 0#              // same as in t_aifReactivePowerControlMode
    aifStatusFlags: int = 0#
    outputPowerLimitPct: float = 0.0 #               // from GTW_TO_SG424_SET_AIF_IMMED_OUTPUT_POWER_LIMIT_PCT_IN_EEPROM_F32_TLV_TYPE
    outputPowerFactor: float = 0.0 #                 // same as t_powerFactor
    spare: float = 0.0 #                             //
    inputPowerFactor: float = 0.0 #                  // same as t_powerFactor
    inputPowerLimitPct: float = 0.0 #                // from GTW_TO_SG424_SET_AIF_IMMED_INPUT_POWER_LIMIT_PCT_IN_EEPROM_F32_TLV_TYPE
    inputConstantReactivePowerPct: float = 0.0 #     //
    outputConstantReactivePowerPct: float = 0.0 #    //
    def __init__(self, idata, dlen):
        '''
        Args:
            idata: is the rcv'd binary buffer from the inverter
            dlen: the data leght.
        '''
        self._ibuf = idata,
        self._blen = dlen
        self._id = ImmediateData()
        assert dlen == IMMED_AIF_DATA_STRUCT_SIZE, 'data size incorrect'
    @property
    def value(self):
        return self
    def set_immed(self):
        #print(f'F {len(self._ibuf[0])=}')
        itup = struct.unpack(self.read_format, self._ibuf[0])
        #print(f'G {itup}, {len(itup)}')
        cdict = asdict(self)
        #print(f'H {cdict}, {len(cdict)}')
        assert(len(itup) == len(cdict) - 1), 'incorect number of elements returned'
        iti = 0
        mlist = [ m  for m in (cdict.keys()) if m not in {'read_format'} ]
        for mname in mlist:
            #print(f'self.{mname} = itup[iti]')
            exec(f'self.{mname} = itup[iti]'); iti += 1

def add_immediate_params(immed_parser):
    immed_parser.add_argument('--get_immed_data', '-gid',
                               action='store_true',
                               dest='getImmedData',
                               help='get immediated data from the inverter')
    OutputPowerLimit().dm_add_arg(immed_parser)
    immed_parser.add_argument('--output_power_limit_enable', '-ople',
                               action='store_true',
                               dest='outputPowerLimitEnable',
                               help='set the output power limit enable')
    OutputPowerFactor().dm_add_arg(immed_parser)
    immed_parser.add_argument('--output_power_factor_leading_enable', '-opfle',
                               action='store_true',
                               dest='outputPowerFactorLeading',
                               help='set the output power factor enable')
    immed_parser.add_argument('--output_power_factor_enable', '-opfe',
                               action='store_true',
                               dest='outputPowerFactor',
                               help='set the output power factor enable')
    OutputConstantReactivePowerPct().dm_add_arg(immed_parser)
    immed_parser.add_argument('--outputConstantReactivePowerFactorLeading', '-ocrpml',
                               action='store_true',
                               dest='outputConstantReactivePowerFactorLeading',
                               help='enable output power factor leading')

    InputPowerLimit().dm_add_arg(immed_parser)
    immed_parser.add_argument('--input_power_limit_enable', '-iple',
                               action='store_true',
                               dest='inputPowerLimitEnable',
                               help='set the input power limit enable')
    InputPowerFactor().dm_add_arg(immed_parser)
    immed_parser.add_argument('--input_power_factor_leading_enable', '-ipfle',
                               action='store_true',
                               dest='inputPowerFactorLeading',
                               help='set the input power factor enable')
    immed_parser.add_argument('--input_power_factor_enable', '-ipfe',
                               action='store_true',
                               dest='input_power_factor_enable',
                               help='set the input power factor enable')
    InputConstantReactivePowerPct().dm_add_arg(immed_parser)
    immed_parser.add_argument('--input_constant_reactive_power_factor_leading', '-icrpml',
                               action='store_true',
                               dest='inputConstantReactivePowerFactorLeading',
                               help='enable input power factor leading')

    immed_parser.set_defaults(func=immed_handler)


def set_immed_data(ip_addr, immed_data):
    buf = struct.pack('!HHH',
                      invc.GTW_TO_SG424_MASTER_U16_TLV_TYPE,
                      invc.MASTER_TLV_DATA_FIELD_LENGTH,
                      1)
    buf += struct.pack(immed_data.header_format,
                       invc.GTW_TO_SG424_SET_IMMED_DATA_STRUCT_TLV_TYPE,
                       immed_data.size)
    buf += struct.pack(immed_data.data_format,
                       immed_data.version,
                       int(immed_data.dynamicDataFlag),
                       int(immed_data.inputPowerFactorLeading),
                       int(immed_data.outputPowerFactorLeading),
                       immed_data.inputPowerFactor,
                       immed_data.outputPowerFactor,
                       int(immed_data.inputConstantReactivePowerFactorLeading),
                       int(immed_data.outputPowerLimitEnable),
                       immed_data.outputPowerLimit,
                       int(immed_data.outputConstantReactivePowerFactorLeading),
                       int(immed_data.inputPowerLimitEnable),
                       immed_data.inputPowerLimit,
                       immed_data.inputConstantReactivePowerPct,
                       immed_data.outputConstantReactivePowerPct)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(buf, (ip_addr, invc.SG424_PRIVATE_UDP_PORT))
    sock.close()

def get_immed_data(ip_addr):
    #tlvid = invc.GTW_TO_SG424_GET_AIF_IMMED_GROUP_DATA_U16_TLV_TYPE # 140
    tlvid = invc.GTW_TO_SG424_GET_AIF_IMMED_CONFIG_CONTROL_GROUP_DATA_U16_TLV_TYPE # 258
    idbuf = autils.get_aif_data(ip_addr, tlvid)
    tlv_l = aext.extract_tlvs(idbuf)
    idata = tlv_l[0][1]
    idata.set_immed()
    return idata


def immed_handler(args):
    ''' This is the parameter handler. For the most part the handler is only
        useful testging. Other programs, e.g. access_aif, compliance, etc.
        will set immediagte-data and then call set_immediate data.
    '''
    idata = ImmediateData()
    if args.getImmedData:
        cdata = get_immed_data(args.ipaddr)
        print(f'{cdata}')
        return

    idata.outputPowerFactor = args.outputPowerFactor
    idata.outputPowerFactorLeading = args.outputPowerFactorLeading
    idata.outputPowerLimit = args.outputPowerLimit
    idata.outputPowerLimitEnable = args.outputPowerLimitEnable
    idata.outputConstantReactivePowerPct = args.outputConstantReactivePowerPct
    idata.outputConstantReactivePowerFactorLeading = args.outputConstantReactivePowerFactorLeading

    idata.inputPowerFactor = args.inputPowerFactor
    idata.inputPowerFactorLeading = args.inputPowerFactorLeading
    idata.inputPowerLimit = args.inputPowerLimit
    idata.inputPowerLimitEnable = args.inputPowerLimitEnable
    idata.inputConstantReactivePowerPct = args.inputConstantReactivePowerPct
    idata.inputConstantReactivePowerFactorLeading = args.inputConstantReactivePowerFactorLeading
    set_immed_data(args.ipaddr, idata)

