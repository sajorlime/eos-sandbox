#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import os
import os.path
import sys

import args

def ms2hrms(unadj):
    ms = int(unadj % 1000)
    print('ms1:', ms)
    secs = (unadj - ms) / 1000
    print('secs1:', secs)
    mins = int(int(secs) / 60)
    print('mins1:', mins)
    secs -= int(60 * mins)
    print('secs2:', secs)
    hrs = int(mins / 60)
    print('hrs:', hrs)
    mins -= 60 * hrs
    print('mins2:', mins)
    return ('ut: %08x'  % unadj, '%d:%02d:%02d:%03d' % (hrs, mins, secs, ms))

args = args.Args('give me a number darn it!')

if __name__ == '__main__':
    """
    """
    nlist = args.get_values()
    for n in nlist:
        print('n: %d -> %s' % (int(n), ms2hrms(int(n))))


