#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""


""" program to filter paths using the .gflfilter file as the source of
    filters.
"""



import os
import os.path
import shutil
import stat
import re
import getopt
import sys
import datetime
import time
import pickle
import copy


prog = sys.argv.pop(0)

_filter_file = ".gflfilter";

try:
  if sys.argv == []:
    print("Oops parameters")
    print("removePath [-f filter] path-list")
    sys.exit(1)

  if sys.argv[0] == '-filter':
    sys.argv.pop(0)
    _filter_file = sys.argv.pop(0)
except IndexError:
  print("Ops parameters")


filters = []
try:
  ff = open(_filter_file, mode="r")
except IOError:
  home = os.path.expandvars("${HOME}")
  try:
    ff = open(os.path.join(home, _filter_file), mode="r")
  except IOError:
    ff = none

if ff:
  for rline in ff:
    rline = rline.rstrip()
    if rline != "":
      filters.append(rline)

r_path = []
for a_path in sys.argv:
  ok = True
  for filter in filters:
    if filter and a_path.find(filter) !=  -1:
      ok = False
  if ok:
    r_path.append(a_path)

for a_path in r_path:
  print(a_path)

