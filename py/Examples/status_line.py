#!/usr/bin/python3
'''
    Implements the "windowed" status update mechanism.
    Status line prints are receive throuh a mp.queue.
'''

import enum
import multiprocessing as mp
import os
import sys
import time
import traceback

# allow imports assuming executing from automation dir.
sys.path.insert(0, os.getcwd())
import setup_path

import global_common as gc

_sl_proc = None
_sl_que = None
_mp_lock = None
_dummy = False

def dbg_print(*msg, end='\n', flush=False, file=sys.stderr):
    _mp_lock.acquire()
    print(*msg, end=end, file=file, flush=flush)
    _mp_lock.release()


_STATUS_SIZE = 20

# indices for show-status
@enum.unique
class SIndx(enum.IntEnum):
    CMD_LINE = 1
    RESULTS = 2
    METER = 3
    VOLTS = 5
    AMPS = 6
    HZ = 7
    WATTS = 8
    SVOLTS = 9
    QVOLTS = 10
    VLINE = 12
    TXSTATUS = 13
    T0STATUS = 14
    INV_INFO = 15
    MISC = 16
    STATUS = _STATUS_SIZE - 2
    _ITYPE = _STATUS_SIZE - 1
    _LAST_LINE = _STATUS_SIZE
    # regular messsages above here
    EMSG = 33
    DUMMY = 34
    QUIT = 64

def _get_term_info():
    #print('TI')
    try:
        tinfo = os.get_terminal_size()
    except OSError as ose:
        tinfo = None
    return tinfo

def _status_proc(slq, lock):
    ''' This is the worker process top-level loop,
    Args:
        queue: an mp.queue
    '''
    global _dummy

    tinfo = _get_term_info()
    if tinfo:
        gc.term_info = tinfo
        def update(color, line, status):
            # set the color
            print(f'[{color};1m', end='')
            # write the status to line in in status section, clear line first
            print(f'[{line};0H[2K{status}', end='')
            # go back to the bottom of the screen and clear the color
            print(f'[{tinfo.lines};0H{gc.nc}', end='', flush=True)
    else:
        gc.term_info = None
        def update(color, line, status):
            print(f'[{color};1m', end='')
            print(f'{line}:{status}', end='')
            print(f'{gc.nc}', end='\n', flush=True)

    while True:
        try:
            slmsg = slq.get()
            if slmsg is None:  # We send this as a sentinel to tell the listener to quit.
                break
            line = slmsg[0]
            color = 31 + (line % 6)
            status = slmsg[1]
            if _dummy:
                print(f'{line}:{status}', end='\r')
            if line > _STATUS_SIZE:
                if line >= SIndx.QUIT:
                    #print('SR: QUIT rcvd')
                    break
                if line >= SIndx.DUMMY:
                    _dummy = True
                    print(f'{line}:{status}')
                    continue
                # TODO(epr): this turns into something useful!
                line = SIndx._ITYPE
            #########
            lock.acquire()
            update(color, line, status)
            ## set the color
            #print(f'[{color};1m', end='')
            ## write the status to line in in status section, clear line first
            #print(f'[{line};0H[2K{status}', end='')
            ## go back to the bottom of the screen and clear the color
            #print(f'[{tinfo.lines};0H{gc.nc}', end='', flush=True)
            lock.release()
            #########
            continue
        except Exception:
            traceback.print_exc(file=sys.stderr)
            sys.exit(1)

def build_status(dummy=False):
    global _sl_proc
    global _sl_que
    global _mp_lock
    global _dummy

    _dummy = dummy

    _mp_lock = mp.Lock()
    _sl_que = mp.Queue()
    _sl_proc = mp.Process(target=_status_proc,
                          name='status',
                          args=(_sl_que, _mp_lock))
    _sl_proc.start()
    #print(f'BStatus: {_sl_proc=}')
    if _dummy:
        print('SL in dummy mode')
        _msg_status(SIndx.DUMMY, 'SL in dummy mode')
        return
    gc.term_info = _get_term_info()
    if gc.term_info:
        print(f'[{_STATUS_SIZE};{gc.term_info.lines}r', end='', flush=True)
        # go back to the bottom of the screen and clear the color
        print(f'[{gc.term_info.lines};0H{gc.nc}', end='', flush=True)
    else:
        print(f'INVALID TERMINAL SIZE', file=sys.stderr)

def reset_status():
    global _sl_proc
    global _dummy
    #print(f'RESET Status_{_sl_proc=}')
    if _sl_proc:
        #print('Reset Status, send QUIT')
        _msg_status(SIndx.QUIT, 'reset status')
        _sl_proc.join()
        #print('STATUS JOINED, reset terminal')
        _sl_proc = None
    term_info = _get_term_info()
    if term_info:
        print(f'[0;{gc.term_info.lines}r', end='', flush=True)
        print(f'[{gc.term_info.lines};0H{gc.nc}')


def _msg_status(n, status):
    global _sl_que
    if n < _STATUS_SIZE:
        show_status(n, f'Bad status line: {n}, {status}')
        return
    # set a color based on n
    _sl_que.put((n, status))

def show_status(n, status):
    global _dummy
    global _sl_que
    if _dummy:
        print(f'DM: {n}:{status}')
        return
    if n > SIndx._LAST_LINE:
        _msg_status(n, f'Bad status line: {n}, {status}')
        return
    # set a color based on n
    _sl_que.put((n, status))

def acquire():
    return _sl_que

def connect(que, dummy=False):
    global _sl_que
    global _dummy
    _dummy = dummy
    _sl_que = que


# code below here is test code.
if __name__ == '__main__':
    import argparse
    import signal

    def sig_handler(sig, frame):
        sys.exit(1)

    parser = argparse.ArgumentParser(description='test status line')
    parser.add_argument('--file', '-f', help="file to scroll")
    parser.add_argument('--RESET', '-R',
                        action='store_true',
                        help="hard reset scroll region")
    parser.add_argument('--reset', '-r',
                        action='store_true',
                        help="reset scroll region")
    args = parser.parse_args()
    if args.RESET:
        tsize = _get_term_info()
        print(f'[0;{tsize.lines}r', end='', flush=True)
        print(f'[{tsize.lines};0H{gc.nc}')
        sys.exit(0)
    build_status()
    for si in range(1, _STATUS_SIZE):
        show_status(si, 30 * '-')
    if args.reset:
        sys.exit(0)
    n = 0
    if args.file:
        signal.signal(signal.SIGINT, sig_handler)
        while True:
            data = open(args.file).read().split('\n')
            for line in data:
                n %= _STATUS_SIZE
                show_status(n, f'{n}, got ya {line}')
                n += 1
                time.sleep(0.1)
                dbg_print(line)
    else:
        show_status(SIndx._ITYPE, 'interactive')
        while True:
            _mp_lock.acquire()
            print('\rn: info, <enter> to quit', end='')
            _mp_lock.release()
            inl = sys.stdin.readline()
            dbg_print(inl)
            if inl.find(':') > 0:
                try:
                    l, msg = inl.split(':', 2)
                    if int(l) >= _STATUS_SIZE:
                        dbg_print('too big')
                        continue
                    show_status(int(l), msg)
                except Exception as ex:
                    print(ex)
            else:
                show_status(SIndx.QUIT, 'bye')
                break
        reset_status()

