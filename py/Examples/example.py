
# gen args
try:
  if sys.argv == []:
    print("Oops parameters")
    print("removePath [-f filter] path-list")
    sys.exit(1)
  if sys.argv[0] == '-filter':
    sys.argv.pop(0)
    _filter_file = sys.argv.pop(0)
# exception
except IndexError:
  print("Ops parameters")

# open
  ff = open(_filter_file, mode="r")

isclass
  if isinstance(a, str):
    print 'str'
  
# function def
def insert_item(d, i):
    ...
    return


# argparse
    parser = argparse.ArgumentParser(description='explore a compact dictionary',
                                     epilog='some string after the help')
    parser.add_argument('file', help-"required positional")
    parser.add_argument('--col', '-c',
                        dest='columns',
                        type=str,
                        action='append',
                        default=[],
                        help='CSV ...')
    parser.add_argument('--ctype', '-t',
                        dest='col_types',
                        type=str,
                        action='append',
                        default=[],
                        help='from ...')
    parser.add_argument('--filter', '-f',
                        dest='filters',
                        type=str,
                        action='append',
                        default=[],
                        help='from ...')
    parser.add_argument('--sort', '-s',
                        dest='sort_cols',
                        type=str,
                        action='append',
                        default=[],
                        help='from ...')
    parser.add_argument('--infile', '-i',
                        dest='infile',
                        type=str,
                        default=None,
                        help='the ...')
    parser.add_argument('--ocsv', '-o',
                        dest='ocsv',
                        type=str,
                        default='-',
                        help='the out...')
    parser.add_argument('--delimiter', '-d',
                        dest='delimiter',
                        type=str,
                        default=',',
                        required=True
                        help='the CSV co...')
    parser.add_argument('--max-lines', '-m',
                        dest='max_lines',
                        type=int,
                        default=sys.maxsize,
                        help='the ...')
    parser.add_argument('--rotate', '-r',
                        dest='rotate',
                        action='store_true',
                        default=False,
                        help='rotate ...')
    parser.add_argument('--index', '-I',
                        dest='index',
                        action='store_true',
                        default=False,
                        help='print the ...')
    parser.add_argument('--count',
                        dest='count',
                        action='store_true',
                        default=False,
                        help='count and pri...')
    choices=['CaDefaults','HiDefaults','MfgDefaults',],
    s=sub.add_parser('updatesystem', help='Ru...')
    s.set_defaults(func=updatesystem.run)


    options = parser.parse_args()

# variable args
def vargs(arg1, *args, **kwargs):
    print(arg1)
    print(args)
    print(kwargs)

# list comprehension
def list_comprehension():
    pass
convert_grammar.py:        for phrase in phrase_list[pos][1:]:
difflist.py:        return [os.path.join(path, x) for x in filter(pre.accept, flist)]
gglck.py:            clist = [primes[x] * ord(cookie[x]) for x in range(gindex - myargs.hwidth, gindex)]
recognition_response.py:    match_list = [x for x in response.find_artist(artist) if x.title() == track]
recognition_response.py:    at_list = [x for x in title_list if x.artist() == artist]
 # else 
 names = [ cxn if not cxn.isnumeric() else tnames[int(cxn)] for cxn in cnames ]
myList = ['a','b','c','d']
myString = ','.join(myList )
print hex string: ":".join("{:02x}".format(ord(c)) for c in s)
# input hex
t = int('3FFF', 16)

# dict comprehension
discogs.py: args = {arg[0]: arg[1] for arg in [ av for av in arg_vals if av[1] ] }
tps = {k:v for k,v in pd.items() if v['jndex'] < 800 }


# inheritence
class AbstractList:
    """
        class for holding instances of lists, queues, etc.
        that have the same semantics
    """
    def __init__(self, alist):
        self._alist = alist

    def put(self, item):
        self._alist.append(item)

    def get(self):
        return None

    def iterate(self):
        return iter(self._alist)

class AList(AbstractList):
    def __init__(self, alist=None):
        if alist == None:
            super().__init__([])
            return
        super().__init__(alist)

class ItemHandler(AbstractList):
    """
    Base class for passing to get_flist
    """
    def __init__(self, root, alist=None):
        AbstractList.__init__(self, alist)
        self._root = root

    def accept(self, item):
        return True

    def append(self, item):
        self.put(item)

    def items(self):
        return self.iterate()

    def terminate(self):
        pass

class TreeItemHandler(ItemHandler):
    def __init__(self, root, filters, alist=None):
        ItemHandler.__init__(self, root, alist)
        self._filters = filters
        self._iiter = None

    def accept(self, item):
        if regexfilter.ftest(item, self._filters):
            self.append(item)

    def append(self, item):
        self.put(item)

    def ptest(self, path):
        """
        tests if path is in the rejection filters.
        Args:
            path: a string, but typically a file path
        Returns:
            True iff path is in the "False" filter
        """
        return regexfilter.ptest(path, self._filters)



# return function
In [74]: def adder(row):
    ...:     def f(i):
    ...:         if i >= len(row):
    ...:             return -1
    ...:         return row[i]
    ...:     return f
    ...: 
    ...: 
    ...: 

# sub argument handler subparser
def make_sub_handler(name):
    def sub_handler(theargs):
        jfh = open(os.path.join(theargs.dir, '%s.json' % name), 'r')
        jdata  = json.loads(jfh.read())
        isofh = open(os.path.join(theargs.dir, theargs.iso), 'r')
        lout = gather_entries(isofh, jdata)
        jout = {'%s' % (name) : lout}
        jout['filename'] = theargs.iso
        print(json.dumps(jout, separators=(',',':')), theargs.out)
    h = sub_handler
    hname = '%s_handler' % name
    globals()[hname] = h

if __name__ == "__main__":
    #           to reusing it.
    for name in _subparser_names.keys():
        make_sub_handler(name)

    parser = argparse.ArgumentParser(description='converts iso data file to JSON')
    parser.add_argument('iso',
                        type=str,
                        help='iso data file nate to convert')
    parser.add_argument('dir',
                        nargs='?',
                        dest='dir',
                        type=str,
                        default='',
                        help='directory path where iso and json files live, %(default)s')
    parser.add_argument('dir',
                        nargs='+',
                        dest='dir',
                        type=str,
                        default='',
                        help='directory path where iso and json files live, %(default)s')
    parser.add_argument('--out', '-o',
                        type=argparse.FileType('w'),
                        default=sys.stdout,
                        help='JSON output file representation of iso')
    parser.add_argument('--section', '-s',
                        action='store_true',
                        default=None,
                        # hide HIDE/HIDDEN suppress or show SHOW noshow NOSHOW
                        help=argparse.SUPPRESS)
    subparser = parser.add_subparsers(title='configuration file handlers',
                                      description='subcmds: facility, field, view, team, ball, referee, marker',
                                      dest='spname',
                                      help='convert a facility, field, view, or team configuration file to JSON format')


    def make_subparser(name):
        """
            create the subparser with different functions handlers
        """
        phelp = 'pass a IsoLynx %s.txt file as the iso file, you need to have %s.json reference file' % (name, name)
        make_parser = """subparser.add_parser('%s', help='%s')""" % (name, phelp)
        #print('MP:', make_parser)
        sparser = eval(make_parser)
        phandler = '%s_handler' % name
        set_handler = """sparser.set_defaults(func=%s)""" % phandler
        #print(set_handler)
        eval(set_handler)


    for name in _subparser_names:
        make_subparser(name)

    args = parser.parse_args()

    if args.spname:
        args.func(args)
        sys.exit(0)

def add_ints():
    parser = argparse.ArgumentParser(description='Add the integers form the command line')
    parser.add_argument('ints',
                        metavar='N',
                        type=int,
                        nargs='+',
                        help='an int for the accumulator')
    '''
    parser.add_argument('--diff', '-d', dest='accumulate', action='store_const',
                        const=lambda (x, y) : x - y,
                        default=lambda x : x,
                        help='diff the ints (def: find the max)')
    '''
    parser.add_argument('--sum', '-s',
                        dest='accumulate',
                        action='store_const',
                        const=sum,
                        default=0,
                        help='sum the ints (def: find the max)')
    args = parser.parse_args()
    print(args)
    print(sum(args.ints))
    if args.accumulate:
        print(args.accumulate(args.ints))

fruits = ['Apple', 'Mango', 'Banana', 'Peach']
 
# iterate with enumerate or sequence
for index, element in enumerate(fruits, start=2):
  print(index, ":", element)

# continue f-string
        sl.show_status(sl.SIndx.RESULTS,
                       f'{_rsave_dir.as_posix()}:{run_number}'
                       f', {_tests_remaining=}, of {_total_tests=}')

# propery
class Foo:
    foo = 'foo'
    @propery
    def foo():
        return self.foo
