#!/usr/bin/env python3.6

from common import *

if __name__ == '__main__':
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
    	s.connect((socket.gethostname(), PORT))
    except:
        print("Please start server first. Exiting!")

    # send an infinite stream of complex reals
    # divided into active frames and buffer frames

    frame_counter = 0
    new_temp = 0 * template + noise_std * sp.randn(size_temp)

    while (True):

        # only transmit 10 % of the time
        if (frame_counter % 2 == 0 and sp.random.rand() > 0.9):
            print("Transmitted!")
            new_temp = new_temp + template
        else:
            new_temp = noise_std * sp.randn(size_temp)

        frame_counter = frame_counter + 1
        print("Frame counter: ", frame_counter)
        s.sendall(new_temp.tobytes())
