#!/usr/bin/env python3
# utils/fft.py 
# 03/20/2023
# fft
# cribed from:
#   https://pythonnumericalmethods.berkeley.edu/notebooks/chapter24.04-FFT-in-Python.html
'''
    routine for taking an FFT of CSV data.
'''
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pathlib as pl
import sys

from scipy.fftpack import fft, ifft


import plot_utils as pu

col_names, meter_data, _ = pu.ccsv2np(pl.Path('oxy.csv'))

tindex = col_names.index('Secs')
vindex = col_names.index('Volts')
plt.figure(figsize = (12, 6))
T = meter_data[:,tindex]
V = meter_data[:,vindex]
N = len(T)
dt = (T[N - 1] - T[0]) / N
print(f'{dt=}')
freq = T / dt
F = fft(V)
magF = np.abs(F)
#print(f'{T.ndim=}, {magF.ndim=}, {len(magF)=}, {type(F[0])=}')
#print(f'{T[10]=}, {V[10]=}, {freq[10]=}, {magF[10]=}')
plt.suptitle('FFT of Dewe Sampled AC Source volts w/ inverter @ 120V 60hz')

#for i in range(0, 100, 20):
#    print(f'{i=}, {freq[i]=}, {magF[i]=}')
plt.plot(freq[1:], magF[1:])
plt.xlabel('Freq (Hz)')
plt.ylabel('Freq Amplitude')
plt.xticks(rotation=25) 
plt.show()

'''

F = fft(df['Volts'])
N = len(F)
#n = np.arange(N)
## get the sampling rate
#sr = 1 / (60*60)
dt = (T[N - 1] - T[0]) / N
#T = N/sr
freq = 1 / dt

# Get the one-sided specturm
n_oneside = N//2
# get the one side frequency
f_oneside = freq[:n_oneside]

plt.figure(figsize = (12, 6))
plt.plot(f_oneside, np.abs(F[:n_oneside]), 'b')
plt.xlabel('Freq (Hz)')
plt.ylabel('FFT Amplitude |F(freq)|')
plt.show()

sys.exit(0)

plt.style.use('seaborn-poster')
#%matplotlib inline
# sampling rate
sr = 2000
# sampling interval
ts = 1.0/sr
t = np.arange(0,1,ts)

freq = 1.
x = 3*np.sin(2*np.pi*freq*t)

freq = 4
x += np.sin(2*np.pi*freq*t)

freq = 7
x += 0.5* np.sin(2*np.pi*freq*t)

plt.figure(figsize = (8, 6))
plt.plot(t, x, 'r')
plt.ylabel('Amplitude')

plt.show()

F = fft(x)

plt.figure(figsize = (12, 6))
plt.subplot(121)

plt.stem(freq, np.abs(F), 'b', \
         markerfmt=" ", basefmt="-b")
plt.xlabel('Freq (Hz)')
plt.ylabel('FFT Amplitude |F(freq)|')
plt.xlim(0, 10)

plt.subplot(122)
plt.plot(t, ifft(F), 'r')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.tight_layout()
plt.show()
'''

