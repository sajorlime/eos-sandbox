#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#

""" csv_filter.py
    A tool for dealng with CSV files.
    It allows one to read, filter, and write results.
    Also allows number operations on columns.
    Colums can be retrieved by header names or column indicies.
    Output is typically CSV format, but JSON is also an option.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import sys
import argparse
import codecs
import csv
import itertools
import json
import math
import operator
import re

# convert a CSV float string to float. Removes comma separators.
# Args: s is a string
# Return: float of string
def Float(s):
    if s.find(',') < 0:
        xs = s.replace(',', '')
        return float(xs)
    return float(s)


# CSV library doesn't like unicode, so we need to wrap it. #TODO(epr) check with py3.
# code cribbed from
# http://stackoverflow.com/questions/904041/reading-a-utf8-csv-file-with-python
def comment_skip(utf8_csv_data):
    for line in utf8_csv_data:
        #yield line.encode('utf-8')
        if line.startswith('#'):
            #print(f'{line=}')
            continue
        yield line


_row = 0
def utf8_csv_reader(utf8_csv_data, delimiter=','):
    """
    Reader returns reader that allows reading one line at a time.
    Args:
        utf8_csv_data: raw data
        delimiter: how to parse lines.
    Returns:
        one line at a time
    """
    global _row
    _row = 0
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(comment_skip(utf8_csv_data), delimiter=delimiter)
    try:
        for row in csv_reader:
            #print('row', row)
            # decode UTF-8 back to Unicode, cell by cell:
            _row += 1
            yield [cell for cell in row]
    except UnicodeDecodeError as e:
        print('exception in utf8-csv-reader @ row', _row)
        raise e

def _identity(x):
    """ default filter for no filter """
    return x

def filter_csv(csv_handler,
               columns,
               row_filters,
               col_xforms,
               col_adders=None,
               max_lines=sys.maxsize):
    """
    Filter a CSV file based on the input parameters.
    Args:
        csv_handler: an open CSV file reader. If there is a column definition
            row at the top, it has been read, or it will be treated as
            an ordinary row.
        max_lines: the maximum lines to return
        columns: a list of columns indices, to be returned in this order
        row_filters: a list of pairs that filter the rows.
        col_xforms: a list of transforms by column
        col_adder: a list of column adders by column
    Returns:
        A list of lists for the columns passed in that order, if any,
        of the column values passed.  There should max_line or fewer rows.
    """
    if not col_xforms: # shouldn't happen unless called externally
        col_xforms = itertools.repeat(_identity, len(columns))
    #print('filter-csv-cols', columns)
    #print ('rowfs', row_filters)
    filtered_rows = []
    if not row_filters:
        # in the common case there there are no row filters,
        # just convert the columns.
        for raw_row in csv_handler:
            #print('RR:', raw_row, len(raw_row))
            if not raw_row:
                #print('RR:', raw_row, len(raw_row))
                continue
            rindex = 0
            #print('RR:', raw_row, len(raw_row))
            xraw_row = []
            rlen = len(raw_row)
            for xform in col_xforms:
                #print('RI:', rindex, raw_row[rindex])
                if rindex >= rlen:
                    print('ROW too short (%d):' % rindex, raw_row)
                    sys.exit(1)
                xraw_row.append(xform(raw_row[rindex]))
                rindex += 1
            #print('XF:', xraw_row, rindex)
            if col_adders:
                for col_adder in col_adders:
                    xraw_row.append(col_adder(xraw_row))
            #print('raw_row:%s' % raw_row)
            #print('XR:', xraw_row, len(xraw_row))
            if not xraw_row:
                continue
            try:
                filtered_rows.append([ xraw_row[col] for col in columns ])
            except IndexError as e:
                print('exception', e, xraw_row)
                sys.exit(-1)
            max_lines -= 1
            if max_lines <= 0:
                break
        return filtered_rows

    #print ('col_xforms', col_xforms)
    for raw_row in csv_handler:
        b = True
        #print('RRX:', col_xforms[6], len(col_xforms))
        cindex = 0
        #print('RR:', raw_row)
        xraw_row = []
        for xform in col_xforms:
            xraw_row.append(xform(raw_row[cindex]))
            #print('XRRA:', xraw_row, cindex)
            cindex += 1
            if cindex >= len(raw_row):
                break
        #print('XR:', raw_row[6], type(raw_row[6]), xraw_row[6], type(xraw_row[6]))
        if col_adders:
            for col_adder in col_adders:
                xraw_row.append(col_adder(xraw_row))
        if not xraw_row:
            continue
        #print('XR:', xraw_row)
        #print('RFT:', type(row_filters), row_filters)
        #def row_filter(filter, rows):
            
        for rfilter in row_filters:
            #print('rf:', rfilter)
            #print('rfm:', rfilter[0], rfilter[1])
            #print(xraw_row)
            v = xraw_row[rfilter[0]]
            #print('V:', v)
            if not rfilter[1](v):
                b = False
                break
        if b:
            filtered_rows.append([ xraw_row[col] for col in columns ])
            max_lines -= 1
            if max_lines <= 0:
                break
    return filtered_rows

def get_csv_col(csv_array, col):
    """
    Take in a csv result like array and return a column as a row.
    Args:
        csv_array: a csv result like array.
        col: the desired column.
    Returns:
        a list containing the data in col.
    """
    return [ x[col] for x in csv_array ]


def sort_csv(csv_list, sort_cols, columns):
    """
    Sort the input list based on the sort cols
    Args:
        csv_list: a list of CSV lines index by indicies, and columns
        sort_cols: the column we want to sort on.
        columns: the reference columns
    Returns:
        sorted list according to sort_cols.
    TODO(epr): there is an order issue using the itemgetter, fine for one
               sort column, but it looks like the reverse for multiple
    """
    if not sort_cols:
        return csv_list
    #index = columns.index(col)
    print('SC0:', sort_cols)
    indicies = gen_csv_column_indicies(columns, sort_cols)
    #def csv_cmp(a, b):
    #    """
    #    """
    #    for index in indicies:
    #        d = cmp(a[index], b[index])
    #        if d:
    #            return d;
    #    return 0
    #print('LIN:', csv_list, indicies)
    csv_list.sort(key=operator.itemgetter(*indicies))
    #print('LOUT:', csv_list, '\n')
    return csv_list


def gen_csv_column_indicies(csv_cols, index_cols=None):
    """
    Takes the csv column names and a list of desired colums to create
        a list of indicies.
    Args:
        csv_cols: the column names in column order for CSV file.
        index_cols: a list of column names,
                   that is a proper subset of the csv_cols.
            They can be an any order.
            members of index_cols may start with "+" or "-" which indicates that
            we need to match all that + start or - end with the "col" value.
    Returns:
        A list of integer indicies for the desired columns
        raises a ValueError if index_cols value is not in csv_cols.
    """
    if not index_cols:
        return range(len(csv_cols))
    wildplus = [ col for col in index_cols if col.startswith('+') ]
    #print('wp:', wildplus)
    for iplus in wildplus:
        ipos = index_cols.index(iplus) # where to insert
        index_cols.pop(ipos) # remove the wild card
        # now find matching columns
        match = iplus[1:]
        found = False
        for col in csv_cols:
            if col.startswith(match):
                index_cols.insert(ipos, col)
                ipos += 1
                found = True
        if not found:
            raise ValueError('No col: %s found' % iplus)
    wildminus = [ col for col in index_cols if col.startswith('\-') ]
    for iminus in wildminus:
        ipos = index_cols.index(iminus) # where to insert
        index_cols.pop(ipos) # remove the wild card
        # now find matching columns
        match = iminus[2:]
        found = False
        for col in csv_cols:
            if col.endswith(match):
                index_cols.insert(ipos, col)
                ipos += 1
                found = True
        if not found:
            raise ValueError('No col: %s found' % match)
    #print('gcc:', index_cols)
    return [ csv_cols.index(index) for index in index_cols ]

def new_col_adder(new_col_arg, csv_cols, col_map):
    """
    Return function that will return a new value to append to row.
    Args:
        new_col_arg: the coded new column value
            a python operation in column name values.
            e.g. --new_col ":cname:c1,c2,c3:c1 op c2 [op c3 ...]
            or, --new_col "cname:func(c1, c2, ..) op cN", etc.
            we substitute naively.
        csv_cols: the current list of columns which will be appended
        col_map: a two way map of column names to indices
    Returns: a function, f(raw_row) that returns a row value to append to raw_row
    """
    def op_sub(cmap, op):
        #print('OS:', op, cmap)
        for col in cmap.keys():
            #print('OSm:', col, type(col), op)
            if type(col) is not str:
                continue
            #print('OSn:', col, col in op)
            op = op.replace(col, 'row[%d]' % cmap[col])
        #print('OSe:', op)
        return op

    sep = new_col_arg[0]
    _, col, cols, coded_op = new_col.split(sep)
    csv_cols.append(col)
    l = len(csv_cols) - 1
    col_map[col] = l
    col_map[l] = col
    operation = op_sub(col_map, coded_op)
    def cadder(row):
        #print('CA:', operation, row[6], type(row[6]), eval(operation))
        return eval(operation)
    return cadder

def _make_col_filter(expression):
    #print('MCF:', expression)
    def col_filter(_VAR_):
        #print('CCF:', expression)
        return eval(expression)
    return col_filter


def gen_csv_row_filters(csv_cols, csv_row_filters):
    """
    Args:
        csv_cols: the column names in column order for CSV file.
        csv_row_filters: a list of column names, regular expresion pairs
    Returns:
        A list of integer indices regx pairs
        raises a ValueError if index_cols value is not in csv_cols.
    """
    if not csv_row_filters:
        return []
    row_filters = []
    for row_filter in csv_row_filters:
        text_index, rfilter = row_filter.split(':')
        index = csv_cols.index(text_index)
        rfilter = rfilter.strip().strip('"')
        if rfilter.find('E ') == 0:
            xfilter = rfilter[2:].replace(text_index, '_VAR_')
            #print('xF:', index, text_index, xfilter)
            row_filters.append((index, _make_col_filter(xfilter)))
            #print('GCF:', index, text_index, rfilter, row_filters)
        else:
            refilter = re.compile(rfilter)
            def sfilter(x):
                #print('SF:', x)
                return refilter.search(x)
            #print('XF:', text_index, rfilter)
            row_filters.append((index, sfilter))
    return row_filters


def gen_csv_col_xforms(csv_cols, tcols):
    """
    Args:
        csv_cols: the column names in column order for CSV file.
        tcols: list of column name and 'python' operations
    Returns:
        A list of integer indicies regx pairs
        raises a ValueError if index_cols value is not in csv_cols.
    """
    xforms = [ _identity for x in csv_cols ]
    if not tcols:
        return xforms
    for col in tcols:
        text_index, xform = col.split(':')
        index = csv_cols.index(text_index)
        xforms[index] = eval(xform)
        #print('CX:', col, xforms[index])
    return xforms


def csv_open(csv_path, delimiter=',', header_line=1, trim=True, max_cols=None):
    """
    Open a CSV file and return the first line and a csv_reader for the file.
    Args:
        csv_path: a file path to a CSV file.
        delimiter: the delimiter for the CSV columns
        header_line: pick the header line
    Returns:
        this list of csv_columns, and a csv_reader for the rest of the file.
    """
    csv_fh = codecs.open(csv_path, encoding='utf-8')
    line = 1
    while line < header_line: # normally does nada
        burn = csv_fh.readline()
        line += 1
    raw_cols = csv_fh.readline().strip().split(delimiter)
    if max_cols != None:
        raw_cols = raw_cols[0:max_cols]
    #print(raw_cols)
    csv_cols = []
    if trim:
        for col in raw_cols:
            csv_cols.append(col.strip())
    else:
        csv_cols = raw_cols
    csv_reader = utf8_csv_reader(csv_fh, delimiter)

    #print(csv_cols, csv_reader)
    return csv_cols, csv_reader

def csv_write(o_name, rows, indicies=None, cols=None, delimiter=','):
    """
    Write the a CSV file.
    Args:
        o_name: output file name, '-' gives stdout
        row: the output data rows, only indices are printed.
        indicies: an list of indexes into the column headers
                  if indicies is empty then print all columns
        cols: is the columnet headers, if None no header
    """
    if o_name == '-':
        ofh = sys.stdout
    else:
        ofh = codecs.open(o_name, encoding='utf-8', mode='w')
    for index in indicies[:-1]:
        print('%s%s' % (cols[index], delimiter), file=ofh, end='')
    print('%s' % cols[indicies[len(indicies) - 1]], file=ofh)
    for row in rows:
        for column in row[:-1]:
            print('%s%s' % (column, delimiter), file=ofh, end='')
        print('%s' % row[len(row) - 1], file=ofh)
    print('\nLines\n', len(rows), file=ofh)



if __name__ == "__main__":
    #print(sys.version_info)
    parser = argparse.ArgumentParser(description='filter rows and columns from a CSV file and write a new CSV file')
    parser.add_argument('icsv',
                        help='the input CSV file path')
    parser.add_argument('--ocsv', '-o',
                        type=str,
                        default='-',
                        help='the output CSV file path (needed to write ' \
                             'utf-8 characters, "-" indicates stdout which ' \
                             'is the default')
    parser.add_argument('--col', '-c',
                        dest='columns',
                        type=str,
                        action='append',
                        default=[],
                        help='CSV columns to accept, either names or indexes. ' \
                             '+col matchings all columns that begin with "col". ' \
                             '-col  matchings all columns that ned with "col".')
    parser.add_argument('--notrim',
                        dest='trim',
                        action='store_false',
                        default=True,
                        help='Do not trim spaces from column headers')
    parser.add_argument('--ctype', '-t',
                        dest='col_types',
                        type=str,
                        action='append',
                        default=[],
                        help='from the possible  columns, the column type, '
                             'default col type is str. ' \
                             'e.g. product_type:float.')
    parser.add_argument('--filter', '-f',
                        type=str,
                        dest='filters',
                        action='append',
                        default=[],
                        help='from the possible columns, criteria by column ' \
                             'for accepting rows, which can be a regular ' \
                             'expression. e.g. product_type:shoe.' \
                             'Numeric expressions must start with "E expr",' \
                             'e.g. "cx:E cx > 5.3". Expression must be in one variable only.' \
                             'Expressions may require a type specification.')
    parser.add_argument('--new_col', '-n',
                        type=str,
                        dest='new_cols',
                        action='append',
                        default=[],
                        help='add a new column based on operations with other columns' \
                             'e.g. --new_col ":cname:c1 op c2 [op c3 ...]" ' \
                             'or, --new_col "cname:func(c1, c2, ..) op cN", etc.' \
                             '":" is the default sepearator, but it can be replaced by any character')
    parser.add_argument('--sort', '-s',
                        dest='sort_cols',
                        type=str,
                        action='append',
                        default=[],
                        help='from the selected columns, sort the output ' \
                             'in the order presented. e.g. --sort=cname ')
    parser.add_argument('--delimiter', '-d',
                        type=str,
                        default=',',
                        help='the CSV column delimiter, default:' \
                             '"%(default)s", use "\\t" for tab')
    parser.add_argument('--max-cols', '-C',
                        dest='max_cols',
                        type=int,
                        help='the maximum cols to process')
    parser.add_argument('--max-lines', '-m',
                        dest='max_lines',
                        type=int,
                        default=sys.maxsize,
                        help='the maximum lines to output, default: '
                              '%(default)s')
    parser.add_argument('--dedup', '-D',
                        type=int,
                        help='remove dupicate lines ignoring column dedup ' \
                             'and after filtering. Zero removes no columns, ' \
                             'default: "%(default)s"')
    parser.add_argument('--rotate', '-r',
                        action='store_true',
                        help = 'rotate output: column: [values]')
    parser.add_argument('--index', '-I',
                        action='store_true',
                        help='print the index ')
    parser.add_argument('--count',
                        action='store_true',
                        help='count and print the number of returned lines')
    parser.add_argument('--json',
                        action='store_true',
                        help='output data as json')
    parser.add_argument('--jkey',
                        type=str,
                        help='when json output selected use key'
                             ' allows number or string')
    options = parser.parse_args()

    delimiter = options.delimiter
    if delimiter == '\\t':
        delimiter = '\t'
    csv_cols, csv_reader = csv_open(options.icsv,
                                    delimiter,
                                    options.trim,
                                    max_cols=options.max_cols)
    #print('CF:', csv_cols)
    if len(csv_cols) == 1:
        print('csv cols length is 1\n', csv_cols, '\n', file=sys.stderr)
        print('check delimiter', file=sys.stderr)
        parser.print_help()
        sys.exit(1)

    csv_col_ops = None
    if options.new_cols:
        col_map = {}
        cindex = 0
        for col in csv_cols:
            print('Col %s %d:' % (col, cindex))
            col_map[col] = cindex
            col_map[cindex] = col
            cindex += 1
        csv_col_ops = []
        print('NCs:', col_map)
        for new_col in options.new_cols:
            sep = new_col
            if new_col.count(sep) < 2:
                print('\nCheck new col delimiter, check help for new_col\n', file=sys.stderr)
                parser.print_help()
                sys.exit(1)
            csv_col_ops.append(new_col_adder(new_col, csv_cols, col_map))

    #print('CO:', options.columns)

    if options.columns:
        columns = options.columns
    else:
        columns = csv_cols
    try:
        csv_indicies = gen_csv_column_indicies(csv_cols, options.columns)
        #print('CIs:', csv_indicies)
    except ValueError as x:
        print('%s', x, file=sys.stderr)
        print(csv_cols, file=sys.stderr)
        print(options.columns, file=sys.stderr)
        sys.exit(1)

    #print('OF:', options.filters)
    # required params
    csv_row_filters = gen_csv_row_filters(csv_cols, options.filters)
    csv_col_xforms = gen_csv_col_xforms(csv_cols, options.col_types)

    #print('filter')
    csv_filtered = filter_csv(csv_reader,
                              columns=csv_indicies,
                              row_filters=csv_row_filters,
                              col_xforms=csv_col_xforms,
                              col_adders=csv_col_ops,
                              max_lines=options.max_lines,
                             )
    #print('sort')
    #print('CF:', csv_filtered, '\n')
    #print('SC:', options.sort_cols, '\n')
    #print('CC:', [ csv_cols[i] for i in csv_indicies ], '\n')
    csv_res = sort_csv(csv_filtered,
                       sort_cols=options.sort_cols,
                       columns=[ csv_cols[i] for i in csv_indicies ])

    if options.ocsv == '-':
        ofh = sys.stdout
    else:
        ofh = codecs.open(options.ocsv, encoding='utf-8', mode='w')

    if options.rotate:
        print('', file=ofh)
        col = 0
        for index in csv_indicies:
            print(index, csv_cols[index], options.delimiter, file=ofh)
            for csv_line in csv_res:
                print(csv_line[col], options.delimiter, file=ofh)
            col += 1
            print(file=ofh)
        print('---------------', file=ofh)
        print(file=ofh)
    else:
        #print('write')
        #print(csv_indicies[:-1])
        #print(csv_cols)
        if options.json:
            lines_dict = {}
            jrow = 0
            if options.jkey:
                jkey = options.jkey
            else:
                jkey = csv_cols[0]
        else:
            print('NJ:', end=' ')
            for index in csv_indicies[:-1]:
                print('{}{}'.format(csv_cols[index], options.delimiter),
                      file=ofh, end='')
            print('{}'.format(csv_cols[csv_indicies[len(csv_indicies)
                                                    - 1]].strip()),
                  file=ofh)
        last_cmp_line = ''
        for csv_line in csv_res:
            # do we de-dupicate
            if options.dedup != None:
                if options.dedup == 0:
                    cmp_line = csv_line
                else:
                    cmp_line = csv_line.pop(options.dedup - 1)
                if cmp_line == last_cmp_line:
                    continue
                last_cmp_line = cmp_line
            if options.json:
                # JSON output
                # first create a jictionary indexed by rows
                # the row info embedded
                # and the column headers as dict keys in each row.
                lout = {'jndex' : 0}
                lines_dict[jrow] = lout
                col = 0
                for index in csv_indicies[:]:
                    lout[csv_cols[index]] = csv_line[col]
                    col += 1
                jrow += 1
                lout['jndex'] = jrow
            else:
                # CSV output
                for column in csv_line[:-1]:
                    print('{}{}'.format(column.strip()
                                           if type(column) is str
                                           else column,
                                        options.delimiter),
                          file=ofh,
                          end='')
                    #print('%s%s' % (column.strip()
                    #                    if type(column) is str else column,
                    #                 options.delimiter),
                    #      file=ofh,
                    #      end='')
                fv = csv_line[len(csv_line) - 1]
                print('{}'.format(fv.strip() if type(fv) is str else fv),
                      file=ofh)
                #print('%s' % fv.strip() if type(fv) is str else fv, file=ofh)
        if options.json:
            dout = {}
            # now, take the lines dict and create a dict indexed by jkeys, col
            for rk in lines_dict:
                row_name = lines_dict[rk][jkey]
                dout[row_name] = lines_dict[rk]
                #lout.append(lines_dict[rk])
            print(json.dumps(dout, indent=2), file=ofh)
    if options.count:
        print('\nLines:', len(csv_res), file=ofh)


