#!/usr/bin/env python3

"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import os
import args

import string
import cgi
import time
import os

class Base:
    def __init__(self_):
        return

    def foo(self_):
        print('Hi, I am base-foo')


class SimpleChild(Base):
    def __init__(self_):
        Base.__init__(self_)
        return

    def foo(self_):
        print('Hi, I am the simple child')

class TrickChild(Base):
    def __init__(self_):
        Base.__init__(self_)
        return

    def foo(self_):
        print('Hi, I am the trick child')
        Base.foo(self_)

class SuperChild(SimpleChild):
    def __init__(self_):
        SimpleChild.__init__(self_)
        return

    def foo(self_):
        print('Hi, I am the super child')
        SimpleChild.foo(self_)


print('base')
base = Base()
base.foo()
print('simple')
simple_child = SimpleChild()
simple_child.foo()
print('trick')
trick_child = TrickChild()
trick_child.foo()
print('super')
super_child = SuperChild()
super_child.foo()

