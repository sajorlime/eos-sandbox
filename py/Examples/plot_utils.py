#!/usr/bin/env python3
# utils/plot_utils.py 
# 09/10/2022
# plot_utils
'''
    routines for ploting CSV data.
'''

import csv
import matplotlib.pyplot as plt
import mpld3
import numpy as np
import os
import pandas as pd
import pathlib as pl
import re
import scipy as sci
#import statsmodels.api as sm
import sys


_color_list = ['blue', 'green', 'red', 'cyan', 'magenta', 'yellow', 'black']

def npfromcsv(csvpath):
    return np.genfromtxt(csvpath)


def ccsv2np(csvpath):
    ''' CSV w/comments to numpy 2-d array
    Args:
        csvpath: pl.Path for CSV file, can have comments
    Returns:
        (col_names, numeric data array, list of extractions)
        col_names is the first non-comment row.
        data array is the non-comment data, col_names width
        extractions: list of comment lines
    '''
    extractions = []
    def comment_skip(csv_data):
        for csv_line in csv_data:
            if csv_line.startswith('#'):
                extractions.append(csv_line)
                continue
            if csv_line.startswith('\n'):
                continue
            yield csv_line

    with csvpath.open('r') as csvf:
        reader = csv.reader(comment_skip(csvf), delimiter=',')
        col_names = next(reader)
        ldata = list(reader)
        data = np.array(ldata).astype(float)
    #print(f'{col_names=}, {data.size=}, {extractions=}')
    return col_names, data, extractions

def plot_tvsy(t_col, y_col, title, tname, yname, show=False):
    ''' plot time vs. voltage of a loaded 2-d array
    '''
    figure, axis = plt.subplots()
    axis.plot(t_col, y_col)
    axis.set_title(title)
    axis.set_ylabel(tname)
    axis.set_xlabel(yname)
    if show:
        plt.show()
    return None

def plot_x_vs_data(data, title, header, show=False):
    ''' plot time vs. voltage, amps, watts, w/header of a loaded 2-d array
    '''
    subps = len(header) - 1
    rows = 2
    cols = int((subps / rows) + 0.5)
    #print(subps, header)
    figure, axis = plt.subplots(rows, cols)
    figure.suptitle(title)
    # we assume that the x-axis is col zero
    dTs = data[:,0]
    li = 1
    for row in range(rows):
        for col in range(cols):
            name = header[li]
            #print(f'{li=}, {name=}, {row=}, {col=}')
            axis[row, col].plot(dTs, data[:,li])
            axis[row, col].set_title(name)
            li += 1
    if show:
        plt.show()
    return None

def plot_tvawh(dTs, volts, amps, watts, hzs,
               title, header, trip_level=None, label=None, show=False):
    ''' plot time vs. voltage, amps, watts, w/header of a loaded 2-d array
    '''
    figure, axis = plt.subplots(2, 2)
    figure.suptitle(title)
    #print(f'{dTs.shape=}, {volts.shape=}')
    axis[0, 0].plot(dTs, volts, label=label)
    if label:
        #axis[0, 0].legend(loc='upper right')
        axis[0, 0].legend()
    axis[0, 0].set_title(header[1])
    axis[0, 1].plot(dTs, amps)
    axis[0, 1].set_title(header[2])
    axis[1, 0].plot(dTs, watts)
    axis[1, 0].set_title(header[3])
    axis[1, 1].plot(dTs, hzs)
    axis[1, 1].set_title(header[4])
    if show:
        plt.show()
    return None

def _linregress(x, y):
    res = sci.stats.linregress(x, y)
    return res

def plot_xvsdata(data, title, header, clist=None, sub=False, show=False):
    ''' plot time vs. watt, q, s 
    '''
    plt.suptitle(title)
    dXs = data[:,0]
    #print(f'{title=}, {header=}, {clist=}')
    if clist is None:
        clist = [ h.strip() for h in header[1:] ]
    #print(f'{header=}, {clist=}')
    if sub:
        zero_line = np.full(dXs.shape, 0.0)
    else:
        zero_line = np.full(dXs.shape, 120.0)
    plt.plot(dXs, zero_line, 'green', label=f'zero-level: {0.0:.1f}')
    y0df = pd.DataFrame({'x': dXs, 'y0': data[:,1]})
    y0res = _linregress(y0df['x'], y0df['y0'])
    y0slope = y0res.slope
    y1df = pd.DataFrame({'x': dXs, 'y1': data[:,2]})
    y1res = _linregress(y1df['x'], y1df['y1'])
    y1slope = y1res.slope
    #print(f'{y0slope=}, {y1slope=}')
    for li, name in enumerate(clist):
        #print(f'{name=}, {li=}')
        col = header.index(name)
        #print(f'{col=}, {name=}, {li=}')
        if sub:
            ydata = data[:,col] - dXs
            ylabel = ' - req'
        else:
            ylabel = ''
            ydata = data[:,col]
        if li ==  0:
            label = f'{header[col]}{ylabel}, m0={y0slope}'
            #print(f'0:{col=}, {name=}, {li=}, {y0slope}, "{label}"')
        elif li ==  1:
            label = f'{header[col]}{ylabel}, m1={y1slope}'
            #print(f'1:{col=}, {name=}, {li=}, {y1slope}, "{label}"')
        else:
            label = f'X:{header[col]=}, {li=}'
        #print(f'x:{col=}, {name=}, {li=}, "{label}"')
        plt.plot(dXs, ydata, '-', label=label)
    plt.legend(loc='lower left')
    if show:
        plt.show()
    return None

def plot_t_vs_wqs(mdata, title, header, clist, show=False):
    ''' plot time vs. watt, q, s 
    '''
    plt.suptitle(title)
    dTs = mdata[:,0]
    #print(f'{header=}, {clist=}')
    for li, name in enumerate(clist):
        col = header.index(name)
        #print(f'{col=}, {name=}, {li=}')
        plt.plot(dTs, mdata[:,col], '-', label=header[col])
    plt.legend(loc='upper right')
    if show:
        plt.show()

def plot_4cols(col_data, title, col_names,
               trip_level=None, labels=None, pcols=None,
               show=False,
               lgr=print):
    ''' plots four cols against col zero
    Args:
        col_data: numpy N x samp array, N >= 5
        title: plot tilte string
        col_names: a list of column names,len >= 4
        labels: a list of pairs.
               can be [(Col, info), ...} or 
                      [ 'Col:info', ...
        trip_level: a trip level to plot on the target trip plot
        pcols: the integer list of cols to plot, if None [1, 2, 3, 4]
    '''
    figure, axis = plt.subplots(2, 2, figsize=(10, 8))
    figure.suptitle(title)
    if not pcols:
        pcols = [ i for i in range(1,5) ]
    if not labels:
        labels = [ None for _ in col_names ]
    elif type(labels) == str:
        lpairs = labels.split(',')
        labels - [ lpair.split(':') for lpair in lpairs ]
    while len(labels) < len(pcols):
        labels.append(None)
    #lgr(f'PC:{trip_level=}, {labels=}')
    x = col_data[:,0]
    lgr(f'CLx: {col_names=}')
    def plot_col(y, col, label):
        nonlocal x, col_data, col_names, trip_level
        r = int(col / 2)
        c = int(col % 2)
        lstr = None
        color = 'blue'
        if label:
            csel = label[0]
            lgr(f'PCl: {col=}, {csel=}, {label=}, {trip_level=}')
            if csel == col_names[col]:
                lstr = f'{label[1]}'
            else:
                lstr = None
            trip_line = np.full(x.shape, trip_level)
            axis[r, c].plot(x, trip_line, 'green', label=f'tl: {trip_level}')
        lgr(f'PC: {col=}, {label=}, {lstr=}') 
        axis[r, c].plot(x, y, color, label=lstr)
        if label:
            axis[r, c].legend(loc='lower left')
        axis[r, c].set_title(col_names[col])

    for col in range(4):
        ci = pcols[col]
        lgr(f'CL: {ci=} <- {col=}, {labels[col]}')
        lgr(f'CLd: {col_data[:,ci][0:3]=}')
        plot_col(col_data[:,ci], col, labels[col])
    if show:
        plt.show()
    #return figure
    return None

def plot_meter(meter_data,
               title,
               col_names,
               clist=None,
               trip_level=None,
               label=None,
               block=False,
               show=False,
               lgr=print):
    ''' plot time vs. voltage, amps, watts, w/header of a loaded 2-d array
    Args:
        meter_data: numpy N x samp array
        tilte: string to place on plot
        col_names: the complete col-names
        clist: an ordered list of names to look up
        label: an optional pair {col-name, label) to put on the col-name plot
    Returns:
        matplotlib.pyplot figure

    '''
    def get_col(names, name, dcol):
        try:
            return names.index(name)
        except ValueError as ie:
            lgr(f'pu-gc:warning {name} not found in {names}')
            return dcol
    col_names = [ n.strip() for n in col_names ]
    lil = []
    if clist:
        for li, cname in enumerate(clist):
            lil.append(get_col(col_names, cname, li))
    else:
        lil = [ i for i in range(1, 5) ]
    cnames = [ col_names[i].strip() for i in lil ]
    labels = []
    for cn in cnames:
        if label and (cn == label[0]):
            labels.append(label)
        else:
            labels.append(None)
    if True:
        fig = plot_4cols(meter_data, title, cnames,
                         trip_level, labels, pcols=lil, show=show,
                         lgr=lgr)
    else:
        #print(f'PM-tvawh:{cnames=}')
        fig = plot_tvawh(meter_data[:,lil[0]],
                         meter_data[:,lil[1]],
                         meter_data[:,lil[2]],
                         meter_data[:,lil[3]],
                         meter_data[:,lil[4]],
                         title,
                         cnames,
                         trip_level,
                         label)
    if show:
        plt.show(block=block)
    return None


def plot_norm_data(data,
                   indices,
                   col_names,
                   title,
                   colors=None,
                   block=False,
                   show=False,
                   lgr=print):
    ''' plot data that is assumed to be normalized.
    Args:
        data: all columns present and time is in column zero;
        indices: the list of columns to plot against col zero
        col_names: names of columns corresponding to indices
        title: plot title
        colors: a list of colors to override default color-list
        show: show the plot, usually done outside of call
    Returns:
        matplotlib.pyplot figure
    '''
    global _color_list
    if not colors:
        colors = _color_list
    for ci, xi in enumerate(indices):
        plt.plot(data[:,0], data[:,xi], colors[ci], label=col_names[xi])

    plt.xlabel('secs')
    plt.ylabel('normized data')
    plt.title(f'Normalized \n {title}')
    plt.legend(loc='lower left')
    if show:
        plt.show(block=block)
    #return plt.figure()
    return None


def plot_vnorm(dTs, volts, amps, tvolts, title, header, trip_time, show=False):
    ''' A routine indended to plot normalize volts, and amps against time.
    Args:
    Returns:
        plt.figure()
    '''
    def norm(ydata):
        ymax = max(ydata)
        ymin = min(ydata)
        dy = ymax - ymin
        return (ydata - ymin)/dy
    norm_volts = norm(volts)
    norm_amps = norm(amps)

    if trip_time == None:
        trip_time = 0.0
    plt.figsize=(20, 16)
    trip_loc = ratio_location(norm_amps, 0.5)[0][0]
    trip_amps = np.full(dTs.shape, norm_amps[trip_loc])
    plt.plot(dTs, norm_amps, 'red', label='norm-amps')

    vmin = min(volts)
    vmax = max(volts)
    #print(f'PTN: {vmin=}, {vmax=}')
    norm_tvolts = (tvolts - vmin)/(vmax - vmin)
    trip_line = np.full(dTs.shape, norm_tvolts)
    plt.plot(dTs, norm_volts, 'blue', label='norm-volts')
    plt.plot(dTs, trip_line, 'green', label=f'trip-level tt: {trip_time:.4f}')

    plt.xlabel('secs')
    plt.ylabel('v/i')
    plt.title(f'Normalized \n {title}')
    plt.legend(loc='lower left')
    if show:
        plt.show()
    return None


def plot_hnorm(dTs, hz, amps, thz, title, header, trip_time, show=False):
    ''' A routine indended to plot normalize freq, and amps against time.
    Args:
    Returns:
        plt.figure()
    '''
    def norm(ydata):
        ymax = max(ydata)
        ymin = min(ydata)
        dy = ymax - ymin
        return (ydata - ymin)/dy
    norm_hz = norm(hz)
    norm_amps = norm(amps)

    if trip_time == None:
        trip_time = 0.0
    plt.figsize=(20, 16)
    trip_loc = ratio_location(norm_amps, 0.5)[0][0]
    trip_amps = np.full(dTs.shape, norm_amps[trip_loc])
    plt.plot(dTs, norm_amps, 'red', label='norm-amps')
    hzmin = min(hz)
    hzmax = max(hz)
    #print(f'PTN: {hzmin=}, {hzmax=}')
    norm_thz = (thz - hzmin)/(hzmax - hzmin)
    trip_line = np.full(dTs.shape, norm_thz)
    plt.plot(dTs, norm_hz, 'blue', label='norm-hz')
    plt.plot(dTs, trip_line, 'green', label=f'trip-level tt: {trip_time:.4f}')

    plt.xlabel('secs')
    plt.ylabel('v/i')
    plt.title(f'Normalized \n {title}')
    plt.legend(loc='lower left')
    if show:
        plt.show()
    #return plt.figure()
    return None

def plot_ppp(data,
             title,
             header,
             annotations=None,
             xlabel='SAS power in',
             ylabel='measured power out',
             p1=True,
             p2=True,
             p3=True,
             p4=True,
             show=False):
    ''' plot power
    '''
    pwr_in = data[1:,0]
    inv_pwr = data[1:,1]
    meter_pwr = data[1:,2]
    meter_s = data[1:,3]
    meter_q = data[1:,4]
    dx = pwr_in[-1] - pwr_in[0]
    miny = inv_pwr[0]
    maxy = inv_pwr[-1]
    for yarray in [inv_pwr, meter_pwr, meter_s, meter_q]:
        miny = min(min(yarray), miny)
        maxy = max(max(yarray), maxy)
        dy = maxy - miny
    #print(f'{miny=}, {maxy=}, {dy=}')
    from scipy import stats
    Mslope, Mintercept, Mr_value, Mp_value, Mstd_err \
        = stats.linregress(pwr_in, meter_pwr)
    MSslope, MSintercept, MSr_value, MSp_value, MSstd_err \
        = stats.linregress(pwr_in, meter_s)
    MQslope, MQintercept, MQr_value, MQp_value, MQstd_err \
        = stats.linregress(pwr_in, meter_q)
    dip = inv_pwr[-1] - inv_pwr[0]
    dmp = meter_pwr[-1] - meter_pwr[0]
    ayp_scale = 0.95 # annotation y position scale
    axp = 0.05 * dx
    if p1:
        plt.plot(pwr_in, inv_pwr, 'blue', label='inv watts')
        plt.annotate(f'inv-pwr m:{dip / dx:.2f}',
                     (axp, (ayp_scale * dy) + miny),
                     color='blue')
        ayp_scale -= 0.05
    if p2:
        plt.plot(pwr_in, meter_pwr, 'red', label='meter watts')
        plt.annotate(f'meter-pwr m:{Mslope:.2f}',
                     (axp, (ayp_scale * dy) + miny),
                     color='red')
        ayp_scale -= 0.05
    if p3:
        plt.plot(pwr_in, meter_s, 'green', label='meter S')
        plt.annotate(f'meter S m:{MSslope:.2f}',
                     (axp, (ayp_scale * dy) + miny),
                     color='green')
        ayp_scale -= 0.05
    if p4:
        plt.plot(pwr_in, meter_q, 'orange', label='meter Q')
        plt.annotate(f'meter Q m:{MQslope:.2f}',
                     (axp, (ayp_scale * dy) + miny),
                     color='orange')
        ayp_scale -= 0.05
    for ano in annotations:
        anoi =  annotations[ano]
        print(f'{ano=}, {anoi[1]=}')
        xpos = axp
        if anoi[1] != None:
            xpos = anoi[1][0]
            ypos = anoi[1][1]
        else:
            ypos = (ayp_scale * dy) + miny
            ayp_scale -= 0.05
        print(f'{ano}: {anoi[0]} {xpos=}, {ypos=}')
        plt.annotate(f'{ano}: {anoi[0]}',
                     (xpos, ypos),
                     color='black')
        
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(f'{title}')

    #plt.annotate(f'invM:{dip / dx:.2f}', (0.1 * dx, 0.95 * dy))
    if show:
        plt.show()
    #return plt.figure()
    return None


def plot_vpp(data, title, use_meter=True, show=False):
    ''' plot power in vs inv-pwr and meter-pwr
    Not sure this works
    '''
    if use_meter:
        volts = data[1:,1]
    else:
        volts = data[1:,2]
    inv_pwr = data[1:,3]
    meter_pwr = data[1:,4]
    meter_s = data[1:,5]
    meter_q = data[1:,6]
    #print(meter_pwr - meter_s)
    plt.plot(volts, inv_pwr, 'blue', label='inv watts')
    plt.plot(volts, meter_pwr, 'red', label='meter watts')
    #plt.plot(volts, meter_s, 'green', label='meter S')
    #plt.plot(volts, meter_q, 'orange', label='meter Q')
    if use_meter:
        plt.xlabel('meter volts out')
    else:
        plt.xlabel('inverter volts out')
    plt.ylabel('measured power out')
    plt.title(f'{title}')
    if show:
        plt.show()

def plot1_2d(x1, y1, title='', show=True):
    plt.plot(x1, y1)
    plt.title(title)
    if show:
        plt.show()
    # return plt.figure()
    return None

def plot2Y_2d(x1, y1, y2, title='', show=True):
    plt.plot(x1, y1, x1, y2)
    plt.title(title)
    if show:
        plt.show()

def plot2X_2d(x1, y1, x2, y2, title='', show=True):
    plt.plot(x1, y1, x2, y2)
    plt.title(title)
    if show:
        plt.show()
    #return plt.figure()
    return None


def show_figures():
    plt.show()


def get_index_gt_target(tvec, target):
    ''' get the index of first value gte vtarget, assuming positive slope
    Args:
        tvec: a 1-d array of voltage values
        target: the target voltage we seek
    Returns:
        An offset into the target vector
    '''
    return np.argmax(tvec > target)


def get_index_lt_target(tvec, target):
    ''' get the index of first value gte vtarget, assuming positive slope
    Args:
        tvec: a 1-d array of target values
        target: the target value we seek
    Returns:
        An offset into the  target vector
    '''
    return np.argmax(tvec < target)


def get_range_from_times(tvals, tmin, tmax):
    ''' get the range indices from time range.
    Args:
        tvals: a 1-d array of time values
        tmin: the min time value
        tmax: the max time value
    Returns:
        A pair of (minindex, maxindex) based on time values
    '''
    tvlt_t0 = tvals[tvals < tmin]
    # can I used tvlt_t0, and take shape[1]?
    tvlt_t1 = tvals[tvals < tmax]
    tmini =  tvlt_t0.shape[0]
    tmaxi =  tvlt_t1.shape[0]
    return (tmini, tmaxi)


def reduce_to_range(csv_data, data_range):
    #print(csv_data.shape)
    #print(data_range[0], data_range[1])
    return csv_data[data_range[0] : data_range[1], :]


def ratio_location(y, ratio=0.5):
    """ find the ratio (50%?) location, and use that point to guess index
    Args:
        y: a 1-d array
        ratio: the target ratio to find
    """
    assert y.ndim == 1, "Input must be one-dimensional."
    ymin = np.amin(y)
    ymax = np.amax(y)
    #ymid = int((ymax - ymin) * ratio)
    ymid = (ymax - ymin) * ratio
    upmid = (y > ymid)  # a little python magic here.
    # find the first location where y[n] < ymid
    index = np.where(upmid==False)
    return index


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='plot tv data test')
    parser.add_argument('csv',
                        nargs='+',
                        type=pl.Path,
                        help='csv file path')
    parser.add_argument('--show',
                        action='store_true',
                        help='show each file, in turn, otherwise all together')
    parser.add_argument('--test',
                        action='store_true',
                        help='debug in test section')
    parser.add_argument('--t0',
                        type=float,
                        help='set t0/min time')
    parser.add_argument('--t1',
                        type=float,
                        help='set t1/max time')
    parser.add_argument('--col',
                        type=int,
                        default=1,
                        help='select column to plot, default:%(default)s')
    exparser = parser.add_mutually_exclusive_group()
    exparser.add_argument('--meter',
                        action='store_true',
                        help='plot dewe meter data')
    exparser.add_argument('--power',
                        action='store_true',
                        help='plot inverter power data')
    parser.add_argument('--xvdata', '-x',
                        action='store_true',
                        help='plot data[0] vs data[1:]')
    parser.add_argument('--xvsub',
                        action='store_true',
                        help='plot data[0] vs data[1:]')
    args = parser.parse_args()

    if args.test and args.t0 and args.t1:
        col_names, meter_data, _ = ccsv2np(pl.Path(args.csv[0]))
        timevals = meter_data[:,_def_ti] # the time stamp array
        data_range = get_range_from_times(timevals, args.t0, args.t1)
        print(f'range: {data_range}')
        data = reduce_to_range(meter_data, data_range)
        print(f'shape data: {data.shape}')
        sys.exit(0)
    if args.meter:
        # we want to test what compliance does
        for csvfpath in args.csv:
            if not csvfpath.is_file():
                print(f'{csvfpath} does not exist')
                break
            m_names = ['Secs', 'Volts', 'Amps', 'Watts', 'Hz']
            header, mdata, _ = ccsv2np(csvfpath)
            plot_meter(mdata, csvfpath, header, m_names,
                       label='mtest', show=args.show)
        if not args.show:
            show_figures()
        sys.exit(0)
    if args.power:
        # we want to test what compliance does
        for csvfpath in args.csv:
            if not csvfpath.is_file():
                print(f'{csvfpath} does not exist')
                break
            p_names = ['dT', 'vline', 'iline', 'watts', 'hz']
            header, mdata, _ = ccsv2np(csvfpath)
            plot_meter(mdata, csvfpath, header, p_names,
                        label='ptest', show=args.show)
        if not args.show:
            show_figures()
        sys.exit(0)
    # if not tests plot the column 
    for csvfpath in args.csv:
        if not csvfpath.is_file():
            print(f'{csvfpath} does not exist')
            break
        col_names, data, _ = ccsv2np(csvfpath)
        if args.xvdata:
            max_volts = data[:,0][-1]
            start_volts = data[:,0][0]
            max_steps = len(data[:,0])
            fields = [ f.split(':') for f in col_names[3:]]
            fdict = {f[0].strip():f[1].strip() for f in fields if len(f) == 2}
            dts = ''
            if 'dT' in fdict:
                dts = f'dT:{fdict["dT"]} secs, '
            title = f'{csvfpath.as_posix()}\n' \
                    f'volts[{start_volts}:{max_volts}], ' \
                    f'samps:{np.shape(data)[0]}, {dts}' \
                    f'hw:{fdict["hwVersion"]}, S/N:{fdict["serialNumber"]}'
            plot_xvsdata(data,
                         title,
                         col_names[:3],
                         sub=args.xvsub,
                         show=True)
            sys.exit(0)
        tname = col_names[0]
        pname = col_names[args.col]
        plot_col = data[:,args.col]
        time_col = data[:,_def_ti] # the time stamp array
        if args.t0 and args.t1:
            data_range = get_range_from_times(time_col, args.t0, args.t1)
            time_col = time_col[data_range[0]:data_range[1]]
            plot_col = plot_col[data_range[0]:data_range[1]]
        plot_tvsy(time_col, plot_col, csvfpath, tname, pname, show=args.show)
    if not args.show:
        show_figures()

