#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

""" interleave.py
    Read two files and output an interleaved file with labeled lines.
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import os
import os.path
import shutil
import stat
import re
import getopt
import sys
import datetime
import time
import pickle
import copy
import string

import args
import filefilter

ff = filefilter


usage = """
interleave textfile1 textfile2

Read in two files and write output to stdout.

"""



if __name__ == "__main__":
    #print 'in main'
    args = args.Args(usage)
    file1, file2 = args.get_values();
    args.error_if_unused_args()

    f1l = ff.ReadFile(file1)
    f2l = ff.ReadFile(file2)

    f1len = len(f1l)
    f2pos = 0
    f2len = len(f2l)
    print("%s:%d" % (file1, f1len))
    print("%s:%d" % (file2, f2len))
    if f2len > f1len:
        f2compl = f2l[f1len:]
    else:
        f2compl = []

    for l1 in f1l:
        print("1> :%s" % l1, end=' ')
        f2next = f2pos + 1
        if (f2pos < f2len):
            l2 = f2l[f2pos:f2next][0]
        else:
            l2 = "\n"
        print("2> :%s" % l2)
        f2pos = f2next

    for l2 in f2compl:
        print("1> :")
        print("2> :%s" % l2)

