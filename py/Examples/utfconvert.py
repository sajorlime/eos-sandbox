#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

""" utfconvert.py
    Takes a file and two conversion arguments to convert file from
    on format to another.
"""

"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import codecs
import os
import os.path
import shutil
import sys

import args
import filefilter


usage = """
utfconvert input [output] [-utfin=utf-id] [-utfout=utf-id]
    input: path to input file with encoding utfin
    output: path to output file that will have encoding utfout
            writes to stdout if no output is specifiied.
    utfin: default to utf-16, can appear in any order.
           Can take any supported codec.
    utfout: default to utf-8, can appear in any order
            Can take any supported codec.

The default behavior is to take a utf-16 file and covert it to a utf-8.
If no output write to stdout.
Shortcut for utf-8 to utf-16: utfconvert input -utfin=utf-8, not meaningful
if no output file.
"""

if __name__ == "__main__":
    args = args.Args(usage)

    utf_in = args.get_arg('utfin', 'utf-16')
    utf_out = 'utf-8'
    if utf_in == utf_out:
        utf_out = 'utf-16'
    utf_out = args.get_arg('utfout', utf_out)
    in_path = args.get_value()
    out_path = args.get_value(default = '-')

    args.error_if_unused_args()

    #print in_path
    #print out_path
    #print utf_in
    #print utf_out

    #lines = filefilter.ReadFile(args.get_value(), file_encoding = utf_in)
    inh = codecs.open(in_path, encoding=utf_in, mode='r')
    #lines = []
    #for line in inh:
    #        lines.append(line)
    #print lines
    lines = inh.readlines()
    inh.close() # close incase we want to write the file.

    if out_path == '-':
        outh = sys.stdout
    else:
        outh = codecs.open(out_path, encoding=utf_out, mode='w')

    for out_line in lines:
        print(out_line, end=' ', file=outh)
    
