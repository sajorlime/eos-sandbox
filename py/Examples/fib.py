#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import os
import os.path
import sys

import args

fib_calls = 0
def fib(n):
    global fib_calls 
    fib_calls += 1
    if n <= 1:
        return 1
    return fib(n - 1) + fib(n - 2)


fast_fib_calls = 1
def fast_fib(n, memo):
    global fast_fib_calls 
    fast_fib_calls += 1
    if n <= 1:
        return 1
    #print n in memo
    if n in memo:
        return memo[n]
    #if n >= 3:
    #   return
    m = fast_fib(n - 1, memo) + fast_fib(n - 2, memo)
    memo[n] = m
    return m;

def fib1(n):
    memo = {0:1, 1:1}
    return fast_fib(n, memo)


def safe_fib(n):
    if n <= 1:
        return 1
    rfib0 = 1
    rfib1 = 2
    i = 2
    rfibi = 2
    while i < n:
       rfibi = rfib1 + rfib0
       rfib0 = rfib1
       rfib1 = rfibi
       i = i + 1
    return rfibi

args = args.Args('give me a number darn it!')

if __name__ == '__main__':
    """
    """
    nlist = args.get_values()
    for n in nlist:
        fib_call = 0
        print('n: %d safe_fib:%d' % (int(n), safe_fib(int(n))))
        fib_call = 0
        print('fib1(%d) = %s' % (int(n), fib1(int(n))))
        print('fast_fib_calls: %d' % fast_fib_calls)
        print('fib(%d) = %d' % (int(n), fib(int(n))))
        fast_fib_call = 1
        print('fib_calls: %d' % fib_calls)
        print('fib_calls/fast_fib_calls: %d' % int(fib_calls / fast_fib_calls))


