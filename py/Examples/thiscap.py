#!/usr/bin/env python3
#
__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import os
import os.path
import shutil
import sys

import args


usage = """
mytee [-f=file]
defaults to stderr and stdout

write the stdin to stdout and stderr, when file is not specified
"""


args = args.Args(usage)

class A:
    def __init__(self):
        self.f = None

class C:
    def __init__(self, s, a):
        self._s = s;
        a.f = lambda : self._s

    
if __name__ == "__main__":
    a = A()
    c = C('this is c', a)
    print(a.f())


