#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#

"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'erojas@gmail.com (Emilio Rojas)'

import sys
import argparse


parser = argparse.ArgumentParser(prog='TestArgs')
parser.add_argument('--foo', action='store_true', help='foo help')
subpars = parser.add_subparsers(dest='spname', help='sub-foo help')

def field_handler(args):
    print(args.bar)
    print(args)

def team_handler(args):
    print(args.x)
    print(args)


par_field = subpars.add_parser('field', help='field help')
par_field.add_argument('bar', type=float, help='give me a float')
par_field.set_defaults(func=field_handler)

par_team = subpars.add_parser('team', help='x help')
par_team.add_argument('-x', type=str, help='give me a string')
par_team.set_defaults(func=team_handler)

parser.parse_args(['team', '-x', '12.1'])



parser.parse_args(['--foo'])
parser.parse_args(['--foo', 'field', '37.73'])
parser.parse_args(['team', '-x', 'Z'])


