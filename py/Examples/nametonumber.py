#!/usr/bin/env python3
#

""" nametonumber.py
    Convert and name to phone number.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

from __future__ import generators

import os
import os.path
import shutil
import sys

import args


usage = """
n2n [name name name ...]
"""

_num_dict = {}
def load_some(index, v, len):
    while len > 0:
        _num_dict[index] = v;
        index = (int) index + 1
    return index

index = load_some('A', 2, 3)
index = load_some('D', 3, 3)
index = load_some('G', 4, 3)
index = load_some('J', 5, 3)
index = load_some('N', 6, 3)
index = load_some('P', 7, 4)
index = load_some('T', 8, 3)
index = load_some('W', 9, 4)


def name2num(name):
    """ hack a name into phone number
    Args:
        name: any alpha string
    Returns:
        a string that has only the digits 1-9.
    """
    num = ''
    for c in name:
        num += _num_dict(c)
    return num


if __name__ == "__main__":
    args = args.Args(usage)

    arg = args.next()
    while arg:
        print '%s => %s' % (arg, name2num(arg))
        arg = args.next()


