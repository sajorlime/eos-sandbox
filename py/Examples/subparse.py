#!/usr/bin/env python3

"""
    subparse.py -- example program for using argparse.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import sys

def show_subparse():
    test_choices = ['meas', 'find']
    parser = argparse.ArgumentParser()
    parser.add_argument('--test',                                                                                                                                                                                                  default='meas',
                        choices=test_choices,
                        help=('which test(s) are we doing option: '
                             'default:%(default)s'))
                             #'default:%(default)s' % test_choices)

    sub = parser.add_subparsers()

    sp1 = sub.add_parser('find', 'find the outfiles in a directory')
    sp1.add_argument('--ofpath',
                    type='str',
                    default='/tmp/outfiles/outfile_(.*).txt',
                    help='path to outfiles: default: %(default)s')
    sp2 = sub.add_parser('meas', 'find the outfiles in a directory')
    sp2.add_argument('-of',
                    '--outfile_path',
                    default=None,
                    requried=True,
                    help='path to an outfile parse')
    sp2.add_argument('-ncsv',
                        '--plot_nodes_csv',
                        type=str,
                        default=None,
                        help="node_csv")
    args = parser.parse_args()
    print(args)
    sys.exit(0)
    for p in args.choice:
        print(p)
    print("that's all folks")


if __name__ == '__main__':
    show_subparse()



