#!/usr/bin/env python3
#
#
""" utility for taking bash-environment variable or a string and a python
    string operation  and executing that operation and returning the result.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import os
import sys


def amethod(x, y, z):
    print(x, y, z)

def bmethod(*args):
    print(*args)

def estring(cmd, env, x):
    """
    execute string command on env and x
    Params:
        cmd: a string comamnd, eg.  mystring.split()
        env: the name of an evironment variable, may be None.
        x: a string, may be None
        Target string is env value if not None, otherwise x.
    Returns:
        result of the string cmd on the target
    """
    if env:
        target = os.getenv(env)
    else:
        target = x
    excmd = '"%s".%s' % (target, cmd)
    r = eval(excmd)
    if isinstance(r, list):
        return ' '.join(r)
    return r
        


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='operating on a string and return result')
    parser.add_argument('-x', default='X')
    parser.add_argument('-y', default='Y')
    parser.add_argument('-z', default='Z')
    args = parser.parse_args()
    eval(f'amethod(args.x, args.y, args.z)')
    p = ('amethod', args.z, args.y, args.x)
    print(f'{p[0]}, *{p[1:]}')
    eval(f'{p[0]}(*{p[1:]})')
    p = ('bmethod', args.y, args.x, args.z)
    eval(f'{p[0]}({p[1:]})')


