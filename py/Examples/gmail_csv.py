#!/usr/bin/env python3
"""
    gmail_csv.py -- class that reads in a google gmail 
    cvs file.

"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import os
import os
import cgi
import codecs
import random
import sys

# local
import email_csv

address_fields = [email_csv._Name,
                  email_csv._Given_Name,
                  'Additional_Name',
                  email_csv._Family_Name,
                  'Yomi_Name',
                   'Given_Name_Yomi',
                  'Additional_Name_Yomi',
                  'Family_Name_Yomi',
                  'Name_Prefix',
                  'Name_Suffix',
                  'Initials',
                  'Nickname',
                  'Short_Name',
                  'Maiden_Name',
                  'Birthday',
                  'Gender',
                  'Location',
                  'Billing_Information',
                  'Directory_Server',
                  'Mileage',
                  'Occupation',
                  'Hobby',
                  'Sensitivity',
                  'Priority',
                  'Subject',
                  'Notes',
                  'Group_Membership',
                  email_csv._E_mail1_Type,
                  email_csv._E_mail1_Value,
                  email_csv._E_mail2_Type,
                  email_csv._E_mail2_Value,
                  email_csv._Phone1_Type,
                  email_csv._Phone1_Value,
                  email_csv._Phone2_Type,
                  email_csv._Phone2_Value,
                  email_csv._Address1_Type,
                  email_csv._Address1_Formatted,
                  email_csv._Address1_Street,
                  email_csv._Address1_City,
                  email_csv._Address1_PO_Box,
                  email_csv._Address1_Region,
                  email_csv._Address1_Postal_Code,
                  email_csv._Address1_Country,
                  email_csv._Address1_Extended_Address]


class gmail_csv(email_csv.email_csv):
    """
    encapsulates the contents of a gmail csv file.
    """
    def __init__(self, addresses):
        email_csv.email_csv.__init__(self, address_fields, addresses)


if __name__ == '__main__':
    import args
    import filefilter

    args = args.Args("gmail_csv (test only) gmail.csv-file")
    addresses = filefilter.ReadFile(args.get_value())

    csv = gmail_csv(addresses)

    print(csv)

    i = 0; print('%d: name: %s, %s' % (i, csv.name(i), csv.email1(i)))
    i = 1; print('%d: name: %s, %s' % (i, csv.name(i), csv.email1(i)))
    i = 3; print('%d: name: %s, %s' % (i, csv.name(i), csv.email1(i)))
    i = 8; print('%d: name: %s, %s' % (i, csv.name(i), csv.email1(i)))

