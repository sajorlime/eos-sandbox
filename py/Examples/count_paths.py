#!/usr/bin/env python3
#
#
'''
    count_path.py

    Question 2: Consider a 2D array of size (m,n).
    Write a program in C, C++, or Python to find the total number of paths
    to get from element (0,0) to (m-1,n-1) considering moving to the right
    or down only at each time.
    Start by writing pseudo code below.
    What is the time and space complexity of your algorithm?

    I worked on wrting a program that walked the paths, but that proved
    to be ugly.

    There is a obvious recurance relationship where the paths available
    is related to the paths the sub-matrix.

    If rows or cols is 1 then there is one path.

    In essance going right is asking how many paths are in the rest matrix with on
    lese cols and adding it.
    Likewise moving down a row.

    Example input: 
    Input: 3,2
    Output: 3
    
'''

__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import argparse

def recurse_path(rows, cols):
    if rows <= 1 or cols <= 1:
        return 1
    return recurse_path(rows - 1, cols) + recurse_path(rows, cols - 1)


if __name__=='__main__':
    parser = argparse.ArgumentParser(description='find_path')

    parser.add_argument('--rows',
                        type=int,
                        default=3,
                        help='the number of rows')
    parser.add_argument('--cols',
                        type=int,
                        default=2,
                        help='the number of cols')
    args = parser.parse_args()

    for row in range(args.rows):
        for col in range(args.cols):
            print((row + 1, col + 1), recurse_path(row + 1, col + 1))

