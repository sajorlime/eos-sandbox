#!/usr/bin/env python3

import json
import sys

_sunSpecXmlStart = '<sunSpecModels v="%d">'
_sunSpecXmlEnd = '</sunSpecModels>'

_sunSpechRefComment = '<!-- %d: %s -->'

_modelStart = '<model id="%d" len="%d" name="%s">'
_modelEnd = '</model>'

_blockStart = '<block len="%d">'
_blockEnd = '</block>'


_base_indent_width = 2
_spaces = '                                          ' # big enough

def xml_line(xline, indent):
    """ return line with indent """
    return '%s%s' % (_spaces[0:_base_indent_width * indent], xline)

# global list of strings describing the XML model
class Model:
    def __init__(self):
        self._xml_lines = []
        self._indent = 0
        self._id = None
        self._label = None
        self._len = None

    def add_line(self, xline, indent=None):
        if xline.index('<') != 0:
            raise RuntimeError('Expected "<" at start of XML statment')
        lgtp = xline.rfind('>')
        if lgtp ==  -1:
            raise RuntimeError('Expected ">" at end of XML statment')
        indent = self._indent
        if xline.find('<!--') == 0:
            #print('com:', xline, self._indent)
            self._xml_lines.append(xml_line(xline, indent))
            return
        lsgt = xline.rfind('/>')
        if (lgtp - lsgt) == 1: # if 1, completed statement, indent unchanged
            #print('citem:', xline, self._indent)
            self._xml_lines.append(xml_line(xline, indent))
            return
        #print('??:', lgtp, lsgt, (lgtp - lsgt), xline, self._indent)
        if xline.find('</') == 0: # end of block, so outdent
            self._indent -= 1
            #print('OD:', xline, self._indent)
            self._xml_lines.append(xml_line(xline, self._indent))
            return
        endgt = xline.find('>')
        if endgt == lgtp:
            # only one tag, so indent
            self._indent += 1 # then indent
            #print('in:', endgt, lgtp, self._indent)
            self._xml_lines.append(xml_line(xline, indent))
            return
        # extract <tag>, set to </tag> and find
        tag = xline[0:endgt + 1]
        #print('t:', endgt, tag, self._indent)
        tag = tag.replace('<', '</') # <tag> -> </tag>
        etag = xline.rfind(tag)
        if etag == -1: # not end tag found
            self._indent += 1 # then indent
        #print('IN:', tag, etag, self._indent)
        self._xml_lines.append(xml_line(xline, indent))

    def set_name(self, name):
        self._name = name

    def set_id(self, model):
        self._id = model

    def set_label(self, label):
        self._label = label

    def set_len(self, mlen):
        self._len = mlen

    def __str__(self):
        return '\n'.join(self._xml_lines)

_Model = Model()

def spec_start(h0, model_name, ver=1):
    if h0['Field Type'] != 'Header':
        raise RuntimeError('Expected "Field Type" == "Header"')
    name = h0['Name']
    if name != 'ID':
        raise RuntimeError('Expected header name "ID"')
    model = int(h0['Value'])
    label = h0['Label']
    _Model.set_name(model_name)
    _Model.set_id(model)
    _Model.set_label(label)
    _Model.add_line(_sunSpecXmlStart % ver)
    _Model.add_line(_sunSpechRefComment % (model, label))

def spec_end():
    _Model.add_line(_sunSpecXmlEnd)
        

def model_start(h1):
    if h1['Field Type'] != 'Header':
        raise RuntimeError('Expected "Field Type" == "Header"')
    name = h1['Name']
    if name != 'L':
        raise RuntimeError('Expected header name "L"')
    _Model.set_len(int(h1['Value']))
    _Model.add_line(_modelStart % (_Model._id, _Model._len, _Model._name))

def model_end():
    _Model.add_line(_modelEnd)
        
def block_start():
    _Model.add_line(_blockStart % _Model._len)
        
def block_end():
    _Model.add_line(_blockEnd)
        

_block_point = '<point id="%s" offset="%d" type="%s" sf="%s" units="%s" acces="%s" mandatory="%s" />'

def blk_point(bp_json):
    """
    block-point string
    Args:
        bp_json: JSON element for the CSV row.
    Returns:
        return the XML string for a point w/in the XML block element.
    """
    name = bp_json['Name']
    offset = bp_json['"Block Offset"']
    if offset is None:
        offset = -1
    type_name = bp_json['Type']
    sf = bp_json['SF']
    units = bp_json['Units']
    access = bp_json['R/W']
    mandatory = 'true' if bp_json['"Mandatory M/O"'] == 'M' else 'false'
    _Model.add_line(_block_point % (name, int(offset), type_name, sf, units, access, mandatory))

#def dpoint(js, file=sys.stdout):
#    js = js.replace('""', '"')
#    djs = json.loads(js)
#    print('<point id="%s" offset="%d" type="%s" sf="%s" units="%s" acces="%s" mandatory="%s" />' % xdjs(djs), file=file)
     
#def djpoint(djs, file=sys.stdout):
#    print('<point id="%s" offset="%d" type="%s" sf="%s" units="%s" acces="%s" mandatory="%s" />' % xdjs(djs), file=file)

#point_json_path = '/home/eorojas/tmp/point.json'

#def read_point_json(jpath):
#    jf = open(jpath)
#    return json.loads(jf.read())

#jdata = read_point_json(point_json_path)

_pointModelStart = '<model>'
_pointModelEnd = '</model>'
_pointLabel = '<label>%s</label>'
_pointDesc = '<description>%s</description>'
_pointNotes = '<notes>%s</notes>'

def str_point(jrow):
    """
    string-point XLM producer
    Args:
        jrow: the JSON row for the CSV data.
    Returns:
        adds strings to th _Model
    """
    label = jrow["Label"]
    desc = '' if jrow['Description'] is None else jrow['Description']
    notes = '' if jrow['Notes'] is None else jrow['Notes']
    _Model.add_line(_pointModelStart)
    _Model.add_line(_pointLabel % label)
    _Model.add_line(_pointDesc % desc)
    _Model.add_line(_pointNotes % notes)
    _Model.add_line(_pointModelEnd)
    

def strings_start(locale='en'):
    _Model.add_line('<strings id="%s" locale="%s">' % (_Model._id, locale))

def strings_end():
    _Model.add_line('</strings>')

if __name__=='__main__':
    import argparse
    parser = argparse.ArgumentParser(description='takes in JSON description of model and outputs SunSpec XML',
                                     epilog='The JSON description can be generated from the CSV file via "csv_filter.py --json"')
    parser.add_argument('--json-file', '-j',
                        type=str,
                        dest='ifile',
                        default = '-',
                        help='the input file , "-" uses stdin, default: %(default)s')
    parser.add_argument('--xml-file', '-x',
                        type=str,
                        dest='ofile',
                        default='-',
                        help='the output file , "-" uses stdout, default: %(default)s')
    parser.add_argument('--model-name', '-m',
                        type=str,
                        dest='model_name',
                        required=True,
                        help='the model name for this model. e.g., equinox_ess')
    parser.add_argument('--version', '-v',
                        type=int,
                        default=1,
                        help='version of the model, default: %(default)s')
    args = parser.parse_args()
    if args.ifile == '-':
        jddict = json.loads(sys.stdin.read())
    else:
        with open(args.ifile, 'r') as ifile:
            jdict = json.loads(ifile.read())
    # the jdict must be a list of JSON rows describing the CSV file.
    # typically csv_filter.py --json model.csv | make_smdx_xml.py
    h0 = jdict[0]
    spec_start(h0, args.model_name)
    h1 = jdict[1]
    model_start(h1)
    block_start()
    for jrow in jdict[2:]:
        if jrow['Field Type'] == '':
            continue
        #print('bp:', jrow['Name'])
        blk_point(jrow)
    block_end()
    model_end()
    strings_start()
    for jrow in jdict[2:]:
        if jrow['Field Type'] == '':
            continue
        #print('sp:', jrow['Name'])
        str_point(jrow)
    strings_end()
    spec_end()
    print(len(_Model._xml_lines))
    if args.ofile == '-':
        ofile = sys.stdout
    else:
        ofile = open(args.ofile, 'w')
    print(_Model, file=ofile)


