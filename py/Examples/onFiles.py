#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

""" onFiles.py file-list-file command and arguments
    Takes each of the names in a file list and executes "command and
    arguments" file-name.
    If and any argument is %F, the file name is substitued in place of
    the %F and remove from the line.
"""


__author__ = 'eorojas@gmail.com (Emilio Rojas)'


import os
import os.path
import shutil
import sys

import args
import filefilter


usage = """
onFiles [-exit-on-error] [-noisy] file-list-file command and arguments

Takes each of the names in a file list and executes "command and
arguments" file-name.
If and any argument is %F, the file name is substitued in place of
the %F and removed from the end of the line.
"""

_ignore_errors = True
_noisy = False

def command_handler(command, command_arg):
    """ call command with file path.  Command must have exactly one %s in it.
    Args:
        command:  the command, it must have one %s in it to substitute
        the command_arg.
        command_arg:  the path extracted from the file names extracted
        from the list.
    Returns:
        Always returns False, i.e. never put anything it the list
    """
    command_arg.rstrip()
    if _noisy:
        print(command % command_arg)
    ret = os.system(command % command_arg)
    if ret:
        if not _ignore_errors:
            sys.exit(ret)
    return False


if __name__ == '__main__':
    #args = args.Args(usage)
    if (len(sys.argv) < 3
        or sys.argv[1] == '-?'
        or sys.argv[1] == '-h'
        or sys.argv[1] == '--h'):
        print(usage)
        sys.exit(1)

    # get the values
    #file_list_file = args.get_value()
    argi = 1
    if sys.argv[1] == "-exit-on-errors":
        _ignore_errors = False
        argi = argi + 1
    if sys.argv[argi] == "-noisy":
        _noisy = True
        argi = argi + 1
    file_list_file = sys.argv[argi];
    argi = argi + 1;
    # compile the rest of the arguments into command
    pre_command = ''
    # TODO(epr): this should be in args and use a generator
    #cmd_args = args.get_values()
    cmd_args = sys.argv[argi:];
    #print 'CA: %s' % cmd_args
    for arg in cmd_args:
        pre_command = '%s %s' % (pre_command, arg)
    #print 'PC: %s' % pre_command
    fpos = pre_command.find('%F')
    if fpos >= 0:
        command = '%s%s%s' % (pre_command[0:fpos], '%s', pre_command[fpos + 3:])
    else:
        command = pre_command + ' %s'
    #print 'flf: %s' % file_list_file
    filefilter.ReadFile(file_list_file,
                        interest
                            = lambda argument: command_handler(command,
                                                               argument))


