#!/usr/bin/env python3

"""
    addparams.py -- example program for using argparse.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse

def show_params():
    parser = argparse.ArgumentParser()

    parser.add_argument('params',
                        type=str,
                        nargs='*',
                        help='an value for the accumulator')
    """
    parser.add_argument('--diff', '-d', dest='accumulate', action='store_const',
                        const=lambda (x, y) : x - y,
                        default=lambda x : x,
                        help='diff the ints (def: find the max)')
    """
    parser.add_argument('--sum',
                        '-s',
                        dest='accumulate',
                        action='store_const',
                        const=sum,
                        default=0,
                        help='sum the ints (def: find the max)')
    args = parser.parse_args()
    for p in args.params:
        print(p)
    print("that's all folks")


if __name__ == '__main__':
    show_params()

