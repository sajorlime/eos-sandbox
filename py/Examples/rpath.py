#!/usr/bin/env python3
# utils/rpath.py 
# 08/11/2022
# rpath.py
'''
    code that finds result files. 
    Useful geting CSV files for ploting or otherwise examining.
'''

import os
import pathlib as pl
import sys

result_root = os.getenv('COMP_RESULT_ROOT', 'results')

_epilog = '''
use --days (possibly by default) or --date to pick the day
use --tslots or --time in hhmmss to pick the particular run.
pick meter or power, will ignore the other.
'''


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='find compliance results',
                                    epilog=_epilog)
    parser.add_argument('--results_dir', '-r',
                        default='results',
                        help='override path to result dir, default: %(default)s')
    parser.add_argument('--days', '-y',
                        type=int,
                        default=1,
                        help='days in the past, default: %(default)s')
    parser.add_argument('--date', '-d',
                        help='date as MMDD or MMDDYYYY')
    parser.add_argument('--time_slot', '-s',
                        type=int,
                        default=1,
                        help='hhmmss slot in the past, default: %(default)s')
    parser.add_argument('--time', '-t',
                        help='time as HHMM or HHMMSS')
    parser.add_argument('--show_dates', '-D',
                        action='store_true',
                        help='show the contents of dates selected')
    parser.add_argument('--show_hours', '-H',
                        action='store_true',
                        help='show the contents of HHMMSS selected')
    parser.add_argument('--show_files', '-F',
                        action='store_true',
                        help='show the files selected by of date and time')
    parser.add_argument('--ext', '-e',
                        default='csv',
                        help='w/show filters the context with *.ext, default:%(default)s')
    parser.add_argument('--mtype', '-m',
                        default='meter',
                        choices=['meter', 'power'],
                        help='w/show filters the context with *base*, default:%(default)s')
    parser.add_argument('--base', '-b',
                        action='append',
                        help='w/show filters the context with *base*, default:%(default)s')
    parser.add_argument('--ignore',
                        action='append',
                        default=['daily', 'csv_bak', 'summary.csv', 'wait-'],
                        help='ignore these directories: %(default)s')
    parser.add_argument('--show_dir',
                        action='store_true',
                        help='show the resulting dir print, and exit')
    args = parser.parse_args()

    def drop_dirs(dir_list, drop_list):
        drops = []
        for n in drop_list:
            drops += [ d for d in dir_list if n in d.parts ]
        drops = set(drops)
        for d in drops:
            dir_list.remove(d)

    def res_dirs(rpath, ignores):
        ''' find the directories of interest
        Args:
            rpath: the result path
            ignores: list of names to ignore
        '''
        remonths = '[01][0-9]'
        redays = '[0-3][0-9]'
        reyears = '20[23][0-9]'
        mstr = f'{rpath}/{remonths}{redays}{reyears}'
        rdir = pl.Path(rpath)
        ddirs = [ d for d in rdir.iterdir() if d.match(mstr) ]
        #print(f'{ddirs=} {ignores=}')
        drop_dirs(ddirs, ignores)
        return ddirs

    # result dir
    rdirs = res_dirs(args.results_dir, args.ignore)
    #print(f'{rdirs=}')
    # we will date or days
    if args.date:
        ddirs = [ d for d in rdirs if d.match(f'*{args.date}*') ]
        if not ddirs:
            print(f'{args.date} not found', file=sys.stderr)
            sys.exit(1)
        if len(ddirs) > 1:
            print(f'multiple directories for "{args.date}" found:')
            for dd in ddirs:
                print(f'  {dd}', file.sys.stderr)
            print(f'Refine dates', file.sys.stderr)
            sys.exit(0)
        ddir = ddirs[0]
    else:
        ddir = rdirs[-args.days]

    if args.show_dates:
        print('Possible dates:', file=sys.stderr)
        for dd in rdirs:
            print(f'  {dd}', file=sys.stderr)

        print(f'{ddir}')
        sys.exit(0)

    if args.show_dir:
        print(f'{ddir.as_posix()}')
        ddirs = [ d for d in ddir.iterdir() ]
        for d in ddirs:
            print(f'{d.as_posix()}')
        sys.exit(0)

    # time dirs
    tdirs = pl.Path(ddir)
    if args.time:
        ttdirs = [ t for t in tdirs.iterdir() if t.match(f'*{args.time}*') ]
        if not ttdirs:
            print(f'{args.time} not found', file=sys.stderr)
            sys.exit(1)
        if len(ttdirs) > 1:
            print(f'multiple directories for "{args.time}" found:')
            for tt in ttdirs:
                print(f'  {tt}')
            print(f'Refine time')
            sys.exit(0)
        ttdir = ttdirs[0]
    else:
        ttdirs = [ d for d in tdirs.iterdir() ]
        ttdir = ttdirs[-args.days]
    if args.show_hours:
        for td in ttdirs:
            print(f'    {td}', file=sys.stderr)
            ttdir = ttdirs[-args.time_slot]
        sys.exit(0)

    # files dir
    fdir = pl.Path(ttdir)
    fresults = [ f for f in ttdir.iterdir() ]
    drop_dirs(fresults, args.ignore)

    if args.ext:
        eresults = [ e for e in fresults if e.match(f'*.{args.ext}') ]
    else:
        eresults = fresults

    #print(f'MTYPE:{eresults=}')
    #sys.stdin.readline()
    if args.mtype:
        mtresults = [ mt for mt in eresults if mt.match(f'*{args.mtype}*') ]
        #print(f'{mtresults=}')
        #sys.stdin.readline()
    else:
        mtresults = eresults
    #print(f'BASE:{eresults=}')
    #print(f'BASE:{mtresults=}')
    #sys.stdin.readline()
    if args.base:
        results = []
        for base in args.base:
            m = f'*{base}*'
            #print(f'INBASE:{mtresults}')
            #print(f'|{m}|')
            results += [ b for b in mtresults if b.match(m) ]
            #print(f'{results=}')
            #for b in mtresults:
            #    print(f'ITBASE:{b}, {type(b)}, {m}: {b.match(m)}')
    else:
        results = mtresults
    #sys.stdin.readline()
    #print(f'FILES:{results=}')
    if args.show_files:
        for r in results:
            print(f'    {r.as_posix()}', file=sys.stdout)
        sys.exit(0)
    if not results:
        print('NO RESULTS FOUND')
        sys.exit(1)

    print(results[-1].as_posix(), file=sys.stderr)
    print(results[-1].as_posix())
    sys.exit(0)

