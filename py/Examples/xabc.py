#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#

""" 
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import sys
import argparse
import codecs
import copy
import csv
import re

import filefilter

def insert_item(d, i):
    """
    Insert an item into our tree at dictionary 'd'
    Args:
        d: is a dictionary that ???
        i: is the string to insert.
    """
    return

def xtest():
    dev_name = "fbtft_device"
    fb_device_params = ""\
        "%(dev_name)s.custom=%(custom)s " \
        "%(dev_name)s.dma=%(dma)s " \
        "%(dev_name)s.verbose=%(verbose) " \
        "%(dev_name)s.name=%(name)s " \
        "%(dev_name)s.gpios=%(gpios)s " \
        "%(dev_name)s.buswidth=%(buswidth)s " \
        "%(dev_name)s.debug=%(debug)s " % \
       {
        "dev_name" : dev_name,
        "dma" : 1,
        "verbose" :  3,
        "name" : "fb_ili9341",
        "gpios" : "reset:26,cs:20,dc:12,wr:1,rd:15,db00:9,db01:25,db02:11,db03:8,db04:7,db05:5,db06:6,db07:16",
        "buswidth" : 8,
        "debug" : 7,
        "custom": 1, 
        }
    print fb_device_params


def match_len(ref, new, hint):
    """
    get the match lenth of new to ref
    Args:
        ref: the referecne sting to match
        new: the string we are matching up
        hint: the guessed length
    """
    i = hint
    if ref[0:hint] == new[0:hint]:
        if ref[hint:hint + 1] == new[hint:hint+1]:
            return match_len(ref, new, hint + 1)
        return hint
    # we need to move back.
    if i == 1:
        return 0
    return match_len(ref, new, hint - 1)

if __name__ == "__main__":
    xtest()
    sys.exit(0)
    parser = argparse.ArgumentParser(description='explore a compact dictionary')
    parser.add_argument('--col', '-c',
                        dest='columns',
                        type=str,
                        action='append',
                        default=[],
                        help='CSV columns to accept, either names or indexes')
    parser.add_argument('--ctype', '-t',
                        dest='col_types',
                        type=str,
                        action='append',
                        default=[],
                        help='from the possible  columns, the column type, defualt col type is str.  e.g. product_type:float. ')
    parser.add_argument('--filter', '-f',
                        dest='filters',
                        type=str,
                        action='append',
                        default=[],
                        help='from the possible  columns, criteria by column for accepting rows, which can be a regular expression.  e.g. product_type:shoe. ')
    parser.add_argument('--sort', '-s',
                        dest='sort_cols',
                        type=str,
                        action='append',
                        default=[],
                        help='from the selected columns,  sort the output in the order presented.  e.g. --sort=cname ')
    parser.add_argument('--infile', '-i',
                        dest='infile',
                        type=str,
                        default=None,
                        help='the input CSV file path')
    parser.add_argument('--ocsv', '-o',
                        dest='ocsv',
                        type=str,
                        default='-',
                        help='the output CSV file path (needed to write utf-8 characters, "-" indicates stdout which is the default')
    parser.add_argument('--delimiter', '-d',
                        dest='delimiter',
                        type=str,
                        default=',',
                        help='the CSV column delimiter, default: "%(default)s", use "\\t" for tab')
    parser.add_argument('--max-lines', '-m',
                        dest='max_lines',
                        type=int,
                        default=sys.maxsize,
                        help='the maximum lines to output, default: "%(default)s"')
    parser.add_argument('--rotate', '-r',
                        dest='rotate',
                        action='store_true',
                        default=False,
                        help='rotate output: column: [values]')
    parser.add_argument('--index', '-I',
                        dest='index',
                        action='store_true',
                        default=False,
                        help='print the index ')
    parser.add_argument('--count',
                        dest='count',
                        action='store_true',
                        default=False,
                        help='count and print the number of returned lines')
    options = parser.parse_args()

    abcl = filefilter.ReadFile(options.infile)

    abcl.sort()
    print(len(abcl))
    abc = abcl[0]
    first = copy.copy(abc)
    print(first)
    root = {}
    root[abc[0]] = {abc}

