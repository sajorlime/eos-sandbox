#!/usr/bin/env python3
# xplot.py
# xplot
'''
    routines for ploting CSV data.
'''

import csv
import matplotlib.pyplot as plt
import numpy as np
import os
import sys
#import medfilt as mf
import plot_utils as pu

if __name__ == "__main__":
    sys.path.insert(0, '../utils')

    import argparse
    parser = argparse.ArgumentParser(description='plot CSV data')
    parser.add_argument('csv',
                        nargs='+',
                        help='csv file path')
    parser.add_argument('--vi',
                        action='store_true',
                        help='plot voltage and current together')
    parser.add_argument('--mf',
                        action='store_true',
                        help='run a median filter over volts and amps')
    parser.add_argument('--normalize', '-n',
                        action='store_true',
                        help='run a median filter over volts and amps')
    args = parser.parse_args()

    for csvfile in args.csv:
        if not os.path.exists(csvfile):
            print(f'{csvfile} does not exist')
            sys.exit(1)
        col_names, csv_data, _ = pu.ccsv2np(csvfile)
        if args.mf:
            csv_data[0] = mf.medfilt(csv_data[0], 7)
            csv_data[1] = mf.medfilt(csv_data[1], 7)
            csv_data[2] = mf.medfilt(csv_data[2], 7)
            csv_data[3] = mf.medfilt(csv_data[3], 7)
        if args.vi:
            pu.plot_tva(csv_data, csvfile, col_names)
        else:
            pu.plot_tvawh(csv_data, csvfile, col_names)
    pu.show_figures()

