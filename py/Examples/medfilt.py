#!/usr/bin/env python3
# utils/medfilt.py 
# 06/07/2022
# median filter
'''
    implenets some simple numpy filtering and matplotlib.
'''

import csv
import numpy as np
import matplotlib.pyplot as plt
import os
import pathlib as pl
#import scipy as sci
import sys

#import utils.plot_utils as pu

def medfilt(x, k):
    """Apply a length-k median filter to a 1D array x.
        Boundaries are extended by repeating endpoints.
        This was cribed from the inter-webs
    Args:
        x: a one dimensional array
        k: the median filter width
    Returns:
        the median filtred result
    """
    assert k % 2 == 1, "Median filter length must be odd."
    assert x.ndim == 1, "Input must be one-dimensional."
    k2 = (k - 1) // 2 # floor division
    y = np.zeros((len (x), k), dtype=x.dtype)
    y[:,k2] = x
    for i in range(k2):
        j = k2 - i
        y[j:,i] = x[:-j]
        y[:j,i] = x[0]
        y[:-j,-(i+1)] = x[j:]
        y[-j:,-(i+1)] = x[-1]
    return np.median(y, axis=1)

# TODO(epr): replace with plot-utils version
def mf_ratio_location(y, ratio=0.5):
    """ find the ratio (50%?) location, and use that point to guess index
    Args:
        y: a 1-d array
        ratio: the target ratio to find
    """
    assert False, 'obsolete mf-ration-location'
    assert y.ndim == 1, "Input must be one-dimensional."
    ymin = np.amin(y)
    ymax = np.amax(y)
    ymid = int((ymax - ymin) * ratio)
    print(f'RL: {ymin=}, {ymid=}, {ymax=}')
    upmid = (y > ymid)
    # find the first location where y[n] < ymid
    index = np.where(upmid==False)
    #print(f'RL: {index}')
    return index

def calc_env_time(env_csv, trip_time_est=-0.150, fwidth=13, plotit=False):
    ''' calculate the trip time from the envelop
    Args:
        env_csv: pl.Path the CSV envelope data.
        trip_time_est: trip time estimate in secs.
        fwidth: the median filter width
        plotit: a flag to stop and plot the median filter result
    Returns:
        a triple of (index, time-statmp, value) at index
    '''
    headers, csv_data, _ = pu.ccsv2np(env_csv)
    envals = csv_data[:,1] # get the envelope volts array
    timevals = csv_data[:,0] # the time stamp array
    tvs0 =  timevals.shape
    # take only those values after the trip estimate
    timevals1 = timevals[timevals > trip_time_est]
    tvs1 =  timevals1.shape
    # the shapes diff pick the index 
    ind0 = tvs0[0] - tvs1[0]
    # smoth the values with a 1-d median filter
    senvals = medfilt(envals[ind0:], fwidth)
    # fify % location
    loc_info = mf_ratio_location(senvals)
    index = loc_info[0][0] + ind0
    if plotit:
        pu.plot1_2d(timevals1, senvals, 'med-flit')
    return (index, 1000.0 * timevals[index], envals[index])


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='test median filter')
    parser.add_argument('csv',
                        type=pl.Path,
                        help='csv file path')
    parser.add_argument('--env', '-e',
                        action='store_true',
                        help='plot envelope result')
    parser.add_argument('--med', '-m',
                        action='store_true',
                        help='plot median filter result')
    parser.add_argument('--trip_time', '-t',
                        type=float,
                        default=None,
                        help='pass a trip time and use to calc env tt')
    parser.add_argument('--fwidth', '-w',
                        type=int,
                        default=7,
                        help='set the filter width')
    args = parser.parse_args()

    if args.trip_time:
        ceinfo = calc_env_time(args.csv, args.trip_time, args.fwidth)
        print(f'CET: {ceinfo}')
        sys.exit(0)

    print('FP:', args.csv)
    if not os.path.exists(args.csv):
        print('fp does not exist:', args.csv)
        sys.exit(1)
        
    headers, csv_data, _ = ccsv2np(args.csv)
    envals = csv_data[:,1]
    timevals = csv_data[:,0]
    senvals = medfilt(envals, args.trip_time, args.fwidth)

    if args.env:
        #print('plot 2d')
        plot1_2d(timevals, envals, 'evelope')

    loc_info = mf_ratio_location(senvals)
    #print(loc_info)
    index = loc_info[0][0]
    #print('IT:', index)
    #print(index, timevals[index], envals[index], senvals[index])

    # smooth envals
    if args.med:
        title = f'edge @ {index}-> ({timevals[index]}:{envals[index]})'
        plot2_2d(timevals, envals, timevals, senvals, title)

