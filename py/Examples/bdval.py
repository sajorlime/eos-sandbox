#!/usr/bin/env python3
#
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import copy
import os
import os.path
import sys
import argparse
import filefilter
import regexfilter

import args

def swap8(n):
    return (((n >> (64 - 64)) & 0xff) << (64 - 8)
           | ((n >> (64 - 56)) & 0xff) << (64 - 16)
           | ((n >> (64 - 48)) & 0xff) << (64 - 24)
           | ((n >> (64 - 40)) & 0xff) << (64 - 32)
           | ((n >> (64 - 32)) & 0xff) << (64 - 40)
           | ((n >> (64 - 24)) & 0xff) << (64 - 48)
           | ((n >> (64 - 16)) & 0xff) << (64 - 56)
           | ((n >> (64 - 8)) & 0xff) << (64 - 64)
           )

if __name__ == '__main__':
    """
    """
    tfd = open(sys.argv[1])
    hex_lines = filefilter.ReadFile(sys.argv[1], strip='\n')
    hex_bytes = ' '.join(hex_lines)
    #print(hex_bytes)
    eatan = 0;
    num_list = []
    while eatan < len(hex_bytes):
        # take the next 8 hex bytes
        bytes8 = hex_bytes[eatan : eatan + 3 * 8]
        #print('b8:', bytes8, len(bytes8))
        bytes8 = bytes8.replace(' ', '')
        #print('B8:', bytes8, len(bytes8))
        b8 = int(bytes8, 16)
        i8 = swap8(b8)
        y8 = swap8(i8)
        assert(b8 == y8)
        #print('i8: %016X' % i8)
        num_list.append(i8)
        eatan += 3 * 8
        #break
    pos = 0;
    last = 0
    print('len list:', len(num_list))

    for n in num_list:
        #print('n:%016X' % n)
        seq = (n >> (64 - 24)) & ((1 << 24) - 1)
        #print('seq:%016X' % seq)
        if seq != last:
            print('unexp(%d): 0x%X != 0x%X' % (pos, seq, last))
            print('%X' % n)
            break
        #break
        last = seq + 2
        if seq == ((1 << 24) - 2):
            last = 0;
        pos = pos + 1
    print('list checks out for %d 64-bit samples' % pos)

