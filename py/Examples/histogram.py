#!/usr/bin/env python3
#

""" histogram.py
    Take a file, or stdin and does a histogram of all the lines received.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

import argparse
import hashlib
import os
import os.path
import shutil
import subprocess
import sys

import filefilter


def histogram():
    parser = argparse.ArgumentParser(description='take a histogram file and generate histogram count')
    parser.add_argument('--file', '-f',
                        type=str,
                        default='-',
                        help='input file, the default, %(default)s, uses stdin ')
    parser.add_argument('--hwidth', '-w',
                        type=int,
                        default=4,
                        help='characters that are considered %(default)s')
    parser.add_argument('--offset', '-o',
                        type=int,
                        default=0,
                        help='characters to start from begining %(default)s')
    parser.add_argument('--limit', '-l',
                        type=int,
                        default=1 << 32,
                        help='characters to start from end %(default)s')
    myargs = parser.parse_args()

    hist = {}

    lines = filefilter.ReadFile(myargs.file, xform=lambda x, y : x.strip())
    limit = myargs.limit
    offset = myargs.offset
    width = myargs.hwidth

    print('0 limit ', limit)
    for line in lines:
        li = line[offset:offset + width]
        if li in hist:
            hist[li] = hist[li] + 1
        else:
            hist[li] = 1
        limit -= 1
        if limit <= 0:
            print('limit reached')
            break
    #print('limit ', limit)
    #print(hist.keys())
    hsorted = sorted(hist.items(), key=lambda x : (x[1], x[0]), reverse=True)
    index = 0
    outcount = 10
    while index < outcount:
        print(hsorted[index])
        index += 1
    #print(hist)

if __name__=='__main__':
    histogram()


