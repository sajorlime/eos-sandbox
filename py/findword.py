#!/usr/bin/python3
#

""" findword.py
    Use the file in $WORDS to find words.
    in files with simple.  By default these are bash shell elements,
    but by passing regular expressions this can be extended.
  
    This program creates tags for all in values in a list
    of ini files.
"""
"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""

__author__ = 'eorojas@gmail.com (Emilio Rojas)'

import argparse
import os
import os.path
import re
import sys

import filefilter
import regexfilter


usage = """
findword.py [options]
"""

def filter_words(template, nottemplate, words):
    rex = re.compile('^' + ''.join(template) + '$')
    print(rex.pattern)
    nts = ''
    for nt in nottemplate:
        if not nt:
            nts += '.'
        else:
            nts += '[^%s]' % ''.join([x for x in nt])
    nrex = re.compile('^' + nts + '$')
    print(nts, nrex.pattern)
    # 3x return iterater
    posl =  list(filter(lambda x : rex.search(x), words))
    print(len(posl))
    l = list(filter(lambda x : nrex.search(x), posl))
    print(len(l))
    return l
    #return filter(lambda x : rex.search(x), words)

def update_letters(letters, notletters, comma_poss, letter):
    if letter not in 'abcdefghijklmnopqrstuvwxyz':
        print('letter before pos')
        return
    if comma_poss.strip() == '0':
        p = 0
        for nll in _notletters:
            if '.' in nll:
                notletters[p] = [letter.strip()]
            else:
                nll.append(letter.strip())
            p += 1
        print('0:', notletters)
        return
    try:
        poss = [ int(x) - 1 for x in comma_poss.split(',')]
    except ValueError:
        print('could not convert to ints', comma_poss)
        return
    for pos in poss:
        if pos == -1:
            print('zero not allowed list')
            return;
        elif pos < 0:
            pos = -pos
            if pos >= ll:
                continue
            _notletters[pos].append(letter.strip())
        else:
            if pos >= ll:
                continue
            letters[pos] = letter.strip()
            for p in range(ll):
                if p not in poss:
                    notletters[p].append(letter.strip())
    print('up:', letters, ' : ',  notletters)

def get_maybes(letters, words):
    cdict = {}
    vdict = {}
    vowles =  'aeiou'
    for x in vowles:
        vdict[x] = 0
    for x in 'bcdfghjklmnpqrstvwxyz':
        cdict[x] = 0
    max_pos = 0
    lwords = len(words)
    for word in words:
        for l in word:
            if l not in letters:
                if l in vowles:
                    vdict[l] = vdict[l] + 1
                else:
                    cdict[l] = cdict[l] + 1
    mc = max(cdict.values())
    mv = max(vdict.values())
    for key in cdict.keys():
        if cdict[key] == mc:
            print('m:c', mc, key, end = ', ')
            break
    if mv > 0:
        for key in vdict.keys():
            if vdict[key] == mv:
                print('m:v', mv, key, end = ', ')
                break
    print('')

_def_letters = [ '.' for x in range(40) ]
_letters = []

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = 'use the words file to find words interactively')
    parser.add_argument('--words', '-w', 
                        dest='words',
                        type=str,
                        default=os.getenv('WORDS'),
                        help = 'override the $WORDS enviroment variable, default: %(default)s')
    myargs = parser.parse_args()

    #print(myargs.words)
    iwords = filefilter.ReadFile(myargs.words, xform = lambda l, n : (l.rstrip().lower()))
    words = iwords
    while True:
        print('len/letter/pos(,s)')
        input = sys.stdin.readline().strip()
        #input = readline.get_buffer()
        try:
            lls, letter, comma_poss = input.split('/')
        except ValueError:
            if input:
                continue
            sys.exit(0)
        try:
            ll = int(lls)
        except ValueError:
            sys.exit(0)
        if not _letters:
            print('reset letters')
            _letters = _def_letters[0:ll]
            _notletters = [[] for x in range(ll) ]
            words = iwords
        else:
            print('continue letters')
        update_letters(_letters, _notletters, comma_poss, letter)
        fwords = list(filter_words(_letters, _notletters, words))
        # TODO(epr):  can one rewind an iterator?
        fwords = list(fwords)
        get_maybes(_letters, fwords)
        print(len(fwords))
        print(_letters, _notletters)

        while True:
            print('letter/pos(,s)')
            input = sys.stdin.readline().strip()
            #input = readline.get_buffer()
            try:
                letter, comma_poss = input.split('/')
            except ValueError:
                if input.strip():
                    continue
                _letters = []
                break;
            update_letters(_letters, _notletters, comma_poss, letter)
            owords = filter_words(_letters, _notletters, fwords)
            get_maybes(_letters, owords)
            #print(fwords)
            l = len(owords)
            if l < 64:
                for word in owords:
                    print(word)
            print(l)
            print(_letters, _notletters)

        


