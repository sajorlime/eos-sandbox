#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import argparse
import google
import google.generativeai as genai
import json
import os
import pathlib as pl
import sys


_prompt = (f'I am an expert programmer examining this undocumented Python code.'
           f' Provide a robust summary of module purpose,'
           f' Point out any issues, e.g. style, possible errors, etc.'
           f' List external dependencies in two groups standard python modules.'
           f' and modules that are likely locally defined'
           f' For each function defined in the file make a guess even'
           f' if there is no doc string for what it does,'
           f' if there is a doc string assume it as a starting place.'
           f' Use google doc string format, include function name'
           f' For each class defined in the file make a guess even'
           f' if there is no doc string for what it does,'
           f' if there is a doc string assume it as a starting place'
           f' Use format google doc string format, include class name'
           f' Include class and funciton bodies in ouput'
           f' Do not attempt to execute the code.'
           f' Write output as .md files.'
           f' Treat the code as text')

_used_in = ''

def process_model(mname:str, file_data:str, tfile:pl.Path, force=False) -> None:
    ''' process the file-data by the modle mname, and write to tfile
    Args:
        mname: I genai model name
        file_data: the programming lanuage file contents
        tfile: the target file to write the results too.
    Returns:
        None, but writes doc to tfile
    '''
    tfile.parent.mkdir(parents=True, exist_ok=True)
    try:
        m = genai.GenerativeModel(mname)
        mres = m.generate_content([file_data, _prompt + _used_in])
    except google.api_core.exceptions.ResourceExhausted as ex:
        print(f'{ex}\nexiting')
        sys.exit(1)
    text_res = mres.text.replace('\\n', '\n')
    with tfile.open('w') as tf:
        #print(f'Print {mname} to {tfile}')
        print(f'## {mname} generated', file=tf)
        print(f'{text_res}', file=tf)
    return None


_model_choices = [
    'gemini-1.5-flash',
    'gemini-1.5-pro',
    ]


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='gemini python file analyze')
    parser.add_argument('file', help='file to load and generate description')
    parser.add_argument('--target_dir', '-t',
                        required=True,
                        default=None,
                        help='target dir to place result: %(default)s')
    parser.add_argument('--models', '-m',
                        choices=_model_choices,
                        action='append',
                        default=[],
                        help='target dir to place result: %(default)s')
    parser.add_argument('--xref', '-x',
                        action='store_true',
                        help='look for a xref.json file, then check for this'
                             ' module')
    parser.add_argument('--force', '-f',
                        action='store_true',
                        help='force the update')
    parser.add_argument('--use_model_in_path', '-M',
                        action='store_true',
                        help='use the module name in path')
    args = parser.parse_args()
    models = args.models
    if not models:
        #print(f'{models=}')
        #print(f'{_model_choices[1]=}')
        models.append(_model_choices[1])
    xref_list = []
    if args.xref:
        fpath = pl.Path(args.file)
        rootp = pl.Path(fpath.parts[0])
        xrfile = rootp / pl.Path('xref.json')
        if xrfile.exists():
            mod_name = pl.Path(args.file).name.replace('.py', '')
            with xrfile.open('r') as xrfd:
                xrfd = json.loads(xrfd.read())
                xrmod_dict = xrfd['local']
            xref_list = xrmod_dict.get(mod_name)
            if xref_list:
                xmods = [ pl.Path(xp).parts[-1].replace('.py', '')
                            for xp in xref_list ]
                xrs = ', '.join(xmods)
                _used_in = f'Create a USED BY section which states it is used in {xrs} modules'
            
    #if not args.models:
    #    args.models = _model_choices
    td = os.path.expanduser(args.target_dir)
    file_data = genai.upload_file(args.file)
    for i, model in enumerate(args.models):
        if args.use_model_in_path:
            tdir = f'{td}/m-{model}'
        else:
            tdir = td
        #print(f'{tdir=}')
        tfile = pl.Path(tdir) / pl.Path(f'{args.file}.md')
        if (not tfile.exists() 
            or (pl.Path(args.file).stat().st_mtime > tfile.stat().st_mtime)
            or args.force):
            process_model(model, file_data, tfile)
            print(f'Updated {tfile}')
        else:
            print(f'Target newer: {args.file}')
    sys.exit(0)


