#!/usr/bin/env python3
#
#

"""
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
"""
"""
  tagsfilter.py filters/removes various tags out of a tags file.
    
"""

import argparse
import os
import os.path
import re
import sys


import filefilter
import regexfilter
import tree

def ofkey(of):
    """
    sort outfile names of the form outfile_[0-9]+.txt or outfiles_NM.txt
    where N and M are numeric [0, 9]

    Args:
        of: the outfile name
    """
    s = of.replace('outfile_', '').replace('.txt', '')
    print(s, end=',')
    return int(s)

def offilter():
    parser = argparse.ArgumentParser(description='file ofiles test')
    parser.add_argument('ofiles',
                        nargs='+',
                        help='oout_files_*.txt')
    myargs = parser.parse_args()

    #print(myargs.ofiles)
    #ofiles_sorted = myargs.ofiles.sort(key=ofkey)
    ofiles_sorted = sorted(myargs.ofiles,
                           key=lambda s : int(s.replace('outfile_', '').replace('.txt', '')))
    print(ofiles_sorted)

if __name__=='__main__':
    offilter()


