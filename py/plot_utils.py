#!/usr/bin/env python3
# utils/plot_utils.py 
# 09/10/2022
# plot_utils
'''
    routines for ploting CSV data.
'''

import csv
import matplotlib.pyplot as plt
import numpy as np
import os
import pathlib as pl
import re
import scipy as sci
import sys
#import xrange

def npfromcsv(csvpath):
    return np.genfromtxt(csvpath)

def ccsv2np(csvpath, extractl=None):
    ''' CSV to numpy 2-d array
    Args:
        csvpath: pl.Path for CSV file, can have comments
        extractl: list of values to extract
    Returns:
        (col_names, numeric data array, list of extractions)
        col_names is the first non-comment row.
        data array is the non-comment data, col_names width
        extractions: dict of (extraction-name, value)
    '''
    extractions = {}
    '''
    TODO(epr): I don't remember what this code was suppose to do!
        Except to was suppose to return lines marked for display,
        Howver, this option does not appear anywere in the code!
    if extractl:
        exre = re.compile('|'.join([f'#DISP:{s}:.*' for s in extractl]))
        #print(f'E:{exre=}')
        assert exre.match('#DISP:meter: EPR,')
    '''
    def comment_skip(csv_data):
        for csv_line in csv_data:
            if csv_line.startswith('#'):
                '''
                #print(f'IN:{csv_line=}')
                m = exre.match(csv_line)
                if m:
                    print(f'{m.group()=}')
                    vals = m.group(0).split(':')
                    print(f'{vals=}')
                    extractions[vals[1]] = [vals[2], eval(vals[3])]
                '''
                continue
            if csv_line.startswith('\n'):
                continue
            #print(f'OUT:{csv_line=}')
            yield csv_line

    with csvpath.open('r') as csvf:
        reader = csv.reader(comment_skip(csvf), delimiter=',')
        col_names = next(reader)
        ldata = list(reader)
        data = np.array(ldata).astype(float)
    #print(f'{col_names=}, {data.size=}, {extractions=}')
    return col_names, data, extractions

def plot_tvsy(t_col, y_col, title, tname, yname, show=False):
    ''' plot time vs. voltage of a loaded 2-d array
    '''
    figure, axis = plt.subplots()
    axis.plot(t_col, y_col)
    axis.set_title(title)
    axis.set_ylabel(tname)
    axis.set_xlabel(yname)
    if show:
        plt.show()

def plot_t_vs_xyz(data, cols, trip_level, title, col_names, show=False):
    ''' plot time (col0)  vs. xyz list of cols together
        data: numpy data in cols, where col 0 is time
        cols: list of columns to plot against time
        col_names: list of column names
    '''
    plt.figsize=(20, 16)

    colors = ['azure', 'orange', 'red', 'blue', 'aqua', 'hotpink', 'honeydew', 'fuchsia']
    assert len(cols) < len(colors), f'{len(cols)=} > {len(colors)=}'
    xyz_axis = []
    dTs = data[:,0]
    for i, col in enumerate(cols):
        plotd = data[:,col]
        plt.plot(dTs, plotd, color=colors[col], label=f'{col_names[col]}')
        xyz_axis.append(col_names[col])
    #dTs = data[:,0]
    trip_data = np.full(dTs.shape, trip_level)
    plt.plot(dTs, trip_data, color='green')
    plt.xlabel('-'.join(xyz_axis))
    plt.ylabel(col_names[0])
    plt.legend(loc='lower left')
    if show:
        plt.show()

def plot_x_vs_data(data, title, header, show=False):
    ''' plot time vs. voltage, amps, watts, w/header of a loaded 2-d array
    '''
    subps = len(header) - 1
    rows = 2
    cols = int((subps / rows) + 0.5)
    print(subps, header)
    figure, axis = plt.subplots(rows, cols)
    figure.suptitle(title)
    # we assume that the x-axis is col zero
    dTs = data[:,0]
    li = 1
    for row in range(rows):
        for col in range(cols):
            name = header[li]
            print(f'{li=}, {name=}, {row=}, {col=}')
            axis[row, col].plot(dTs, data[:,li])
            axis[row, col].set_title(name)
            li += 1
    if show:
        plt.show()

def plot_tvawh(dTs, volts, amps, watts, hzs,
               title, header, label=None, show=False):
    ''' plot time vs. voltage, amps, watts, w/header of a loaded 2-d array
    '''
    figure, axis = plt.subplots(2, 2)
    figure.suptitle(title)
    #print(f'{dTs.shape=}, {volts.shape=}')
    axis[0, 0].plot(dTs, volts, label=label)
    if label:
        #axis[0, 0].legend(loc='upper right')
        axis[0, 0].legend()
    axis[0, 0].set_title(header[1])
    axis[0, 1].plot(dTs, amps)
    axis[0, 1].set_title(header[2])
    axis[1, 0].plot(dTs, watts)
    axis[1, 0].set_title(header[3])
    axis[1, 1].plot(dTs, hzs)
    axis[1, 1].set_title(header[4])
    if show:
        plt.show()

def plot_t_vs_wqs(mdata, title, header, clist, show=False):
    ''' plot time vs. watt, q, s 
    '''
    plt.suptitle(title)
    dTs = mdata[:,0]
    #print(f'{header=}, {clist=}')
    for li, name in enumerate(clist):
        col = header.index(name)
        #print(f'{col=}, {name=}, {li=}')
        plt.plot(dTs, mdata[:,col], '-', label=header[col])
    plt.legend(loc='upper right')
    if show:
        plt.show()

def plot_4cols(col_data, title, col_names,
               labels=None, pcols=None,
               show=False):
    ''' plots four cols against col zero
    Args:
        col_data: numpy N x samp array, N >= 5
        title: plot tilte string
        col_names: a list of column names,len >= 4
        labels: a list of pairs.
               can be [(Col, info), ...} or 
                      [ 'Col:info', ...
        pcols: the integer list of cols to plot, if None [1, 2, 3, 4]
    '''
    figure, axis = plt.subplots(2, 2, figsize=(10, 8))
    figure.suptitle(title)
    if not pcols:
        pcols = [ i for i in range(1,5) ]
    if not labels:
        labels = [ None for _ in col_names ]
    elif type(labels) == str:
        lpairs = labels.split(',')
        labels - [ lpair.split(':') for lpair in lpairs ]
    while len(labels) < len(pcols):
        labels.append(None)
    #print(f'PC:{labels=}')
    x = col_data[:,0]
    def plot_col(y, col, label):
        nonlocal x, col_data, col_names
        r = int(col / 2)
        c = int(col % 2)
        lstr = None
        if label:
            csel = label[0]
            #print(f'PCl: {col=}, {csel=}, {label=}')
            if csel == col_names[col]:
                lstr = label[1]
            else:
                lstr = None
        #print(f'PC: {col=}, {label=}, {lstr=}') 
        axis[r, c].plot(x, y, label=lstr)
        if label:
            axis[r, c].legend(loc='lower left')
        axis[r, c].set_title(col_names[col])

    for col in range(4):
        ci = pcols[col]
        #print(f'CL: {ci=} <- {col=}')
        plot_col(col_data[:,ci], col, labels[col])
    if show:
        plt.show()

def plot_meter(meter_data,
               title,
               col_names,
               clist=None,
               label=None,
               show=False):
    ''' plot time vs. voltage, amps, watts, w/header of a loaded 2-d array
    Args:
        meter_data: numpy N x samp array
        tilte: string to place on plot
        col_names: the complete col-names
        clist: an ordered list of names to look up
        label: an optional pair {col-name, label) to put on the col-name plot
    '''
    def get_col(names, name, dcol):
        try:
            return names.index(name)
        except ValueError as ie:
            print(f'warning {name} not found in {names}', file=sys.stderr)
            return dcol
    col_names = [ n.strip() for n in col_names ]
    if clist:
        lil = []
        for li, cname in enumerate(clist):
            lil.append(get_col(col_names, cname, li))
    else:
        lil = [ i for i in range(1, 5) ]
    cnames = [ col_names[i].strip() for i in lil ]
    labels = []
    for cn in cnames:
        if cn == label[0]:
            labels.append(label)
        else:
            labels.append(None)
    if True:
        #print(f'PM: {labels=}')
        plot_4cols(meter_data, title, cnames,
                   labels, pcols=lil, show=show)
    else:
        #print(f'PM:{cnames=}')
        plot_tvawh(meter_data[:,lil[0]],
                   meter_data[:,lil[1]],
                   meter_data[:,lil[2]],
                   meter_data[:,lil[3]],
                   meter_data[:,lil[4]],
                   title,
                   cnames,
                   label)
    if show:
        plt.show()

def plot_vnorm(dTs, volts, amps, tvolts, title, header, trip_time, show=False):
    def norm(ydata):
        ymax = max(ydata)
        ymin = min(ydata)
        dy = ymax - ymin
        return (ydata - ymin)/dy
    norm_volts = norm(volts)
    norm_amps = norm(amps)

    if trip_time == None:
        trip_time = 0.0
    plt.figsize=(20, 16)
    trip_loc = ratio_location(norm_amps, 0.5)[0][0]
    trip_amps = np.full(dTs.shape, norm_amps[trip_loc])
    plt.plot(dTs, norm_amps, 'red', label='norm-amps')

    vmin = min(volts)
    vmax = max(volts)
    #print(f'PTN: {vmin=}, {vmax=}')
    norm_tvolts = (tvolts - vmin)/(vmax - vmin)
    trip_line = np.full(dTs.shape, norm_tvolts)
    plt.plot(dTs, norm_volts, 'blue', label='norm-volts')
    plt.plot(dTs, trip_line, 'green', label=f'trip-level tt:{trip_time:.3f}')

    plt.xlabel('secs')
    plt.ylabel('v/i')
    plt.title(f'Normalized \n {title}')
    plt.legend(loc='lower left')
    if show:
        plt.show()

def plot_ppp(data,
             title,
             header,
             annotations=None,
             xlabel='SAS power in',
             ylabel='measured power out',
             p1=True,
             p2=True,
             p3=True,
             p4=True,
             show=False):
    ''' plot power in vs inv-pwr and meter-pwr
    '''
    pwr_in = data[1:,0]
    inv_pwr = data[1:,1]
    meter_pwr = data[1:,2]
    meter_s = data[1:,3]
    meter_q = data[1:,4]
    dx = pwr_in[-1] - pwr_in[0]
    miny = inv_pwr[0]
    maxy = inv_pwr[-1]
    for yarray in [inv_pwr, meter_pwr, meter_s, meter_q]:
        miny = min(min(yarray), miny)
        maxy = max(max(yarray), maxy)
        dy = maxy - miny
    #print(f'{miny=}, {maxy=}, {dy=}')
    from scipy import stats
    Mslope, Mintercept, Mr_value, Mp_value, Mstd_err \
        = stats.linregress(pwr_in, meter_pwr)
    MSslope, MSintercept, MSr_value, MSp_value, MSstd_err \
        = stats.linregress(pwr_in, meter_s)
    MQslope, MQintercept, MQr_value, MQp_value, MQstd_err \
        = stats.linregress(pwr_in, meter_q)
    dip = inv_pwr[-1] - inv_pwr[0]
    dmp = meter_pwr[-1] - meter_pwr[0]
    ayp_scale = 0.95 # annotation y position scale
    axp = 0.05 * dx
    if p1:
        plt.plot(pwr_in, inv_pwr, 'blue', label='inv watts')
        plt.annotate(f'inv-pwr m:{dip / dx:.2f}',
                     (axp, (ayp_scale * dy) + miny),
                     color='blue')
        ayp_scale -= 0.05
    if p2:
        plt.plot(pwr_in, meter_pwr, 'red', label='meter watts')
        plt.annotate(f'meter-pwr m:{Mslope:.2f}',
                     (axp, (ayp_scale * dy) + miny),
                     color='red')
        ayp_scale -= 0.05
    if p3:
        plt.plot(pwr_in, meter_s, 'green', label='meter S')
        plt.annotate(f'meter S m:{MSslope:.2f}',
                     (axp, (ayp_scale * dy) + miny),
                     color='green')
        ayp_scale -= 0.05
    if p4:
        plt.plot(pwr_in, meter_q, 'orange', label='meter Q')
        plt.annotate(f'meter Q m:{MQslope:.2f}',
                     (axp, (ayp_scale * dy) + miny),
                     color='orange')
        ayp_scale -= 0.05
    for ano in annotations:
        anoi =  annotations[ano]
        print(f'{ano=}, {anoi[1]=}')
        xpos = axp
        if anoi[1] != None:
            xpos = anoi[1][0]
            ypos = anoi[1][1]
        else:
            ypos = (ayp_scale * dy) + miny
            ayp_scale -= 0.05
        print(f'{ano}: {anoi[0]} {xpos=}, {ypos=}')
        plt.annotate(f'{ano}: {anoi[0]}',
                     (xpos, ypos),
                     color='black')
        
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(f'{title}')

    #plt.annotate(f'invM:{dip / dx:.2f}', (0.1 * dx, 0.95 * dy))
    if show:
        plt.show()


def plot_vpp(data, title, header, use_meter=True, show=False):
    ''' plot power in vs inv-pwr and meter-pwr
    '''
    if use_meter:
        volts = data[1:,1]
    else:
        volts = data[1:,2]
    inv_pwr = data[1:,3]
    meter_pwr = data[1:,4]
    meter_s = data[1:,5]
    meter_q = data[1:,6]
    #print(meter_pwr - meter_s)
    plt.plot(volts, inv_pwr, 'blue', label='inv watts')
    plt.plot(volts, meter_pwr, 'red', label='meter watts')
    #plt.plot(volts, meter_s, 'green', label='meter S')
    #plt.plot(volts, meter_q, 'orange', label='meter Q')
    if use_meter:
        plt.xlabel('meter volts out')
    else:
        plt.xlabel('inverter volts out')
    plt.ylabel('measured power out')
    plt.title(f'{title}')
    if show:
        plt.show()

def plot1_2d(x1, y1, title='', show=True):
    plt.plot(x1, y1)
    plt.title(title)
    if show:
        plt.show()

def plot2Y_2d(x1, y1, y2, title='', show=True):
    plt.plot(x1, y1, x1, y2)
    plt.title(title)
    if show:
        plt.show()

def plot2X_2d(x1, y1, x2, y2, title='', show=True):
    plt.plot(x1, y1, x2, y2)
    plt.title(title)
    if show:
        plt.show()

def show_figures():
    plt.show()

def get_index_gt_target(tvec, target):
    ''' get the index of first value gte vtarget, assuming positive slope
    Args:
        tvec: a 1-d array of voltage values
        target: the target voltage we seek
    Returns:
        An offset into the target vector
    '''
    return np.argmax(tvec > target)

def get_index_lt_target(tvec, target):
    ''' get the index of first value gte vtarget, assuming positive slope
    Args:
        tvec: a 1-d array of target values
        target: the target value we seek
    Returns:
        An offset into the  target vector
    '''
    return np.argmax(tvec < target)

def get_range_from_times(tvals, tmin, tmax):
    ''' get the range indicies from time range.
    Args:
        tvals: a 1-d array of time values
        tmin: the min time value
        tmax: the max time value
    Returns:
        A pair of (minindex, maxindex) based on time values
    '''
    tvlt_t0 = tvals[tvals < tmin]
    # can I used tvlt_t0, and take shape[1]?
    tvlt_t1 = tvals[tvals < tmax]
    tmini =  tvlt_t0.shape[0]
    tmaxi =  tvlt_t1.shape[0]
    return (tmini, tmaxi)

def reduce_to_range(csv_data, data_range):
    #print(csv_data.shape)
    #print(data_range[0], data_range[1])
    return csv_data[data_range[0] : data_range[1], :]

def ratio_location(y, ratio=0.5):
    """ find the ratio (50%?) location, and use that point to guess index
    Args:
        y: a 1-d array
        ratio: the target ratio to find
    """
    assert y.ndim == 1, "Input must be one-dimensional."
    ymin = np.amin(y)
    ymax = np.amax(y)
    #ymid = int((ymax - ymin) * ratio)
    ymid = (ymax - ymin) * ratio
    #print(f'RL: {ymin=}, {ymid=}, {ymax=}')
    upmid = (y > ymid)
    # find the first location where y[n] < ymid
    index = np.where(upmid==False)
    #print(f'RL: {len(index[0])}')
    return index

_def_header = 'Secs,Volts,Amps,Watts,Hz'.split(',')
_secs = 'Secs'
_def_ti = _def_header.index(_secs)
_volts = 'Volts'
_def_vi = _def_header.index('Volts')
_amps = 'Amps'
_def_ai = _def_header.index('Amps')
_watts = 'Watts'
_def_wi = _def_header.index('Watts')
_freq = 'Hz'
_def_fi = _def_header.index('Hz')

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='plot tv data test')
    parser.add_argument('csv',
                        nargs='+',
                        type=pl.Path,
                        help='csv file path')
    parser.add_argument('--show',
                        action='store_true',
                        help='show each file, in turn, otherwise all together')
    parser.add_argument('--test',
                        action='store_true',
                        help='debug in test section')
    parser.add_argument('--t0',
                        type=float,
                        help='set t0/min time')
    parser.add_argument('--t1',
                        type=float,
                        help='set t1/max time')
    parser.add_argument('--col',
                        type=int,
                        default=1,
                        help='select column to plot, default:%(default)s')
    parser.add_argument('--tl',
                        type=float,
                        help='plot trip level at <tl>')
    exparser = parser.add_mutually_exclusive_group()
    exparser.add_argument('--xyz',
                        action='store_true',
                        help='plot y and z against x')
    exparser.add_argument('--meter',
                        action='store_true',
                        help='plot dewe meter data')
    exparser.add_argument('--median',
                        type=int,
                        default=None,
                        help='plot a median filter of col data')
    exparser.add_argument('--power',
                        action='store_true',
                        help='plot inverter power data')
    args = parser.parse_args()

    if args.xyz:
        col_names, xyz_data, _ = ccsv2np(pl.Path(args.csv[0]))
        #print(f'{col_names=}, {xyz_data.ndim=}')
        plot_t_vs_xyz(xyz_data, [1, 3, 4, 5, 6], args.tl,
                      args.csv[0].name,
                      col_names, show=False)
        '''
        plot_t_vs_xyz(xyz_data, [1, 4], args.tl,
                      args.csv[0].name,
                      col_names, show=False)
        plot_t_vs_xyz(xyz_data, [1, 5], args.tl,
                      args.csv[0].name,
                      col_names, show=False)
        plot_t_vs_xyz(xyz_data, [1, 6], args.tl,
                      args.csv[0].name,
                      col_names, show=False)
        '''
        show_figures()
        sys.exit(0)
    if args.test and args.t0 and args.t1:
        col_names, meter_data, _ = ccsv2np(pl.Path(args.csv[0]))
        sys.exit(0)
    if args.median:
        # we want to test what compliance does
        for csvfpath in args.csv:
            if not csvfpath.is_file():
                print(f'{csvfpath} does not exist')
                break
            m_names = ['Secs', 'Volts', 'Amps', 'Watts', 'Hz']
            header, mdata, _ = ccsv2np(csvfpath)
            plot_meter(mdata, csvfpath, header, m_names,
                        label='mtest', show=args.show)
        if not args.show:
            show_figures()
        sys.exit(0)
    if args.meter:
        # we want to test what compliance does
        for csvfpath in args.csv:
            if not csvfpath.is_file():
                print(f'{csvfpath} does not exist')
                break
            m_names = ['Secs', 'Volts', 'Amps', 'Watts', 'Hz']
            header, mdata, _ = ccsv2np(csvfpath)
            plot_meter(mdata, csvfpath, header, m_names,
                        label='mtest', show=args.show)
        if not args.show:
            show_figures()
        sys.exit(0)
    if args.power:
        # we want to test what compliance does
        for csvfpath in args.csv:
            if not csvfpath.is_file():
                print(f'{csvfpath} does not exist')
                break
            p_names = ['dT', 'vline', 'iline', 'watts', 'hz']
            header, mdata, _ = ccsv2np(csvfpath)
            plot_meter(mdata, csvfpath, header, p_names,
                        label='ptest', show=args.show)
        if not args.show:
            show_figures()
        sys.exit(0)
    # if not tests plot the column 
    for csvfpath in args.csv:
        if not csvfpath.is_file():
            print(f'{csvfpath} does not exist')
            break
        col_names, meter_data, _ = ccsv2np(csvfpath)
        tname = col_names[0]
        pname = col_names[args.col]
        plot_col = meter_data[:,args.col]
        time_col = meter_data[:,_def_ti] # the time stamp array
        if args.t0 and args.t1:
            data_range = get_range_from_times(time_col, args.t0, args.t1)
            time_col = time_col[data_range[0]:data_range[1]]
            plot_col = plot_col[data_range[0]:data_range[1]]
        plot_tvsy(time_col, plot_col, csvfpath, tname, pname, show=args.show)
    if not args.show:
        show_figures()

