#!/bin/bash
ln -s ${HOME}/eos-sandbox/.vimrc ${HOME}/.vimrc
ln -s ${HOME}/eos-sandbox/.gflfilter ${HOME}/.gflfilter
mkdir ${HOME}/src
mkdir ${HOME}/tmp
mkdir ${HOME}/bin
ln -s ${HOME}/eos-sandbox/bash ${HOME}/bash
ln -s ${HOME}/eos-sandbox/py ${HOME}/src/py
ln -s ${HOME}/eos-sandbox/csrc ${HOME}/src/work
cp ${HOME}/bash/bash_xdirs ${HOME}/bash/bash_xdirs-${HOSTNAME}
cp ${HOME}/bash/bash_xall ${HOME}/bash/bash_xall-${HOSTNAME}
#echo export HOME=/home/eo > .bashrc
#echo export HOMEPATH=/home/eo >> .bashrc
#echo export USER=eo >> .bashrc
#echo export USERNAME=eo >> .bashrc
echo source bash/bashrc >> .bashrc
