#!/usr/bin/env bash
# script to run distanceComputerModule.py with parameters

export OMP_NUM_THREADS=1
export OPENBLAS_NUM_THREADS=1
export EMP=1
# set the base name for the output file
OUT_ROOT=/tmp/outdata
OUTBASE=dcmrun
PROFILE=''
typeset -i run
typeset -i repeat
run=0
repeat=1
DATA_DATE=20210818
PROFILE_ARG=''

for arg in $*
do
    case $arg in
    -d) DATA_DATE=$2
        shift
        shift
        ;;
    -n) 
        unset OMP_NUM_THREADS
        unset OPENBLAS_NUM_THREADS
        EMP=0
        shift
        ;;
    -o) OUTBASE=$2
        shift
        shift
        ;;
    -p) PROFILE="-m cProfile -s cumtime "
        shift
        ;;
    -P) PROFILE_ARG='--profile'
        shift
        ;;
    -r) repeat=$2
        echo set repeat, $repeat
        shift
        shift
        ;;
    -t) 
        OMP_NUM_THREADS=1
        OPENBLAS_NUM_THREADS=1
        shift
        ;;
    -?) 
        echo rundcm.sh [-d datedir] [-n] [-o basename]  [-p] [-r R] [-t] [-?]
        echo "    -n disable multi-threading"
        echo "    -o set basename of log file, /tmp/outdata/BASENAME"
        echo "       file gets /tmp/outdata/BASENAME-R.txt"
        echo "       BASENAME defaults to: \"${OUTBASE}\""
        echo "    -p call python with: -m cProfile -s cumtime"
        echo "    -P profile DC_calc"
        echo "    -r repeat the run R times"
        echo "    -? print this message"
        shift
        exit 1
        ;;
    esac
done

OUT_PATH=${OUT_ROOT}/${DATA_DATE}
echo ${OUT_PATH}
mkdir -p ${OUT_PATH}
OUTFILE_ROOT=${OUT_PATH}/${OUTBASE}

echo outbase: ${OUTFILE_ROOT}
echo OMP_NUM_THREADS: $OMP_NUM_THREADS
echo OPENBLAS_NUM_THREADS: $OPENBLAS_NUM_THREADS
echo EMP: $EMP

DATA_ROOT=/home/emilio/src/data
pushd ${DATA_ROOT} > /dev/null
DD=`ls ${DATA_DATE}`
popd > /dev/null
DATA_PATH=/home/emilio/src/data/${DATA_DATE}/${DD}

while [ $run -lt $repeat ]
do
    OUTFILE=${OUTFILE_ROOT}-${run}.txt
    echo outfile: ${OUTFILE}
    python3 ${PROFILE} \
        distanceComputationModule.py \
        --node_path=${DATA_PATH}/node_characteristics.csv \
        --outfile_pattern_path=${DATA_PATH}/outfile_\(.*\).txt \
        --tagTDOAOnly=-1 \
        --verboseMode=0 \
        --distComputeMethod=0\
        --do_kpi=0 \
        --outfile_pattern_max_num=500 \
        --plot_metrics=0 \
        --output_dist_format=1 \
        --nodePermuationDist=0 \
        --out_tdoa_type=2  \
        --use_TDOA_geom=0 \
        --mvdn=1 \
        --enable_multi_process=${EMP} \
        ${PROFILE_ARG} \
        --default_yaml_file_path=/home/emilio/tmp/dc_config_templates/DC_defaults.yml \
        #> ${OUTFILE}
    run=run+1
done

